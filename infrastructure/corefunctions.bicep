param senderAppName string
param reporterAppName string
param vorpaAppName string
param prepareAppName string
param storageAccountType string = 'Standard_LRS'
param location string
param runtime string
param senderAspName string
param aspName string
param sharedResourceGroup string
param resourceGroup string
param senderConnectivitySubnet string
param connectivitySubnet string
param privateDnsZoneName string
param subnetName string
param vnetName string
param storageAccountName string


resource storageAccount 'Microsoft.Storage/storageAccounts@2022-05-01' = {
  name: storageAccountName
  location: location
  sku: {
    name: storageAccountType
  }
  kind: 'Storage'
  properties: {
    supportsHttpsTrafficOnly: true
    defaultToOAuthAuthentication: true
  }
}

resource senderHostingPlan 'Microsoft.Web/serverfarms@2022-03-01' = {
  name: senderAspName
  location: location
  sku: {
    name: 'EP1'
    tier: 'ElasticPremium'
    family: 'EP'
    capacity: 1
  }
  kind: 'elastic'
  properties: {
    maximumElasticWorkerCount: 20
  }
}

resource hostingPlan 'Microsoft.Web/serverfarms@2022-03-01' = {
  name: aspName
  location: location
  sku: {
    name: 'EP1'
    tier: 'ElasticPremium'
    family: 'EP'
    capacity: 1
  }
  kind: 'elastic'
  properties: {
    maximumElasticWorkerCount: 20
  }
}

module sender 'function.bicep' = {
  name: 'func-sender'
  params: {
    appName: senderAppName
    hostingPlanId: senderHostingPlan.id
    location: location
    runtime: runtime
    storageAccountName: storageAccountName
    appResourceGroup: resourceGroup
    connectivitySubnet: senderConnectivitySubnet
    privateDnsZoneName: privateDnsZoneName
    rgSharedResources: sharedResourceGroup
    subnetName: subnetName
    vnetName: vnetName
  }
  dependsOn: [storageAccount]
}

module reporter 'function.bicep' = {
  name: 'func-reporter'
  params: {
    appName: reporterAppName
    hostingPlanId: hostingPlan.id
    location: location
    runtime: runtime
    storageAccountName: storageAccountName
    appResourceGroup: resourceGroup
    connectivitySubnet: connectivitySubnet
    privateDnsZoneName: privateDnsZoneName
    rgSharedResources: sharedResourceGroup
    subnetName: subnetName
    vnetName: vnetName
  }
  dependsOn: [storageAccount]
}

module vorpa 'function.bicep' = {
  name: 'func-vorpa'
  params: {
    appName: vorpaAppName
    hostingPlanId: hostingPlan.id
    location: location
    runtime: runtime
    storageAccountName: storageAccountName
    appResourceGroup: resourceGroup
    connectivitySubnet: connectivitySubnet
    privateDnsZoneName: privateDnsZoneName
    rgSharedResources: sharedResourceGroup
    subnetName: subnetName
    vnetName: vnetName
  }
  dependsOn: [storageAccount]
}

module prepare 'function.bicep' = {
  name: 'func-prepare'
  params: {
    appName: prepareAppName
    hostingPlanId: hostingPlan.id
    location: location
    runtime: runtime
    storageAccountName: storageAccountName
    appResourceGroup: resourceGroup
    connectivitySubnet: connectivitySubnet
    privateDnsZoneName: privateDnsZoneName
    rgSharedResources: sharedResourceGroup
    subnetName: subnetName
    vnetName: vnetName
  }
  dependsOn: [storageAccount]
}
