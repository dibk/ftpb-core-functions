﻿using Dibk.Ftpb.Api.Clients.HttpClients;
using Dibk.Ftpb.Api.Models;
using Ftb_Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ftb_Repositories
{
    public class FormAttachmentMetadataRepository : IFormAttachmentMetadataRepository
    {
        private string _archiveReference;
        private readonly ILogger<FormAttachmentMetadataRepository> _logger;
        private readonly FormMetadataHttpClient _formMetadataClient;

        public FormAttachmentMetadataRepository(ILogger<FormAttachmentMetadataRepository> logger, FormMetadataHttpClient formMetadataClient)
        {
            _logger = logger;
            _formMetadataClient = formMetadataClient;
        }

        public async Task AddAttachmentsAsync(List<FormAttachmentMetadataApiModel> formAttachments)
        {
            await _formMetadataClient.AddAttachmentMetadata(_archiveReference, formAttachments);
        }

        public void SetArchiveReference(string archiveReference)
        {
            _archiveReference = archiveReference;
        }
    }
}