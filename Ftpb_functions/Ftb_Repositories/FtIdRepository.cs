﻿using Dibk.Ftpb.Api.Clients.HttpClients;
using Dibk.Ftpb.Api.Models;
using Ftb_Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Threading.Tasks;

namespace Ftb_Repositories
{
    public class FtIdRepository : IFtIdRepository
    {
        private string _archiveReference;
        private readonly ILogger<FtIdRepository> _logger;
        private readonly FtIdHttpClient _ftIdHttpClient;
        private FormMetadataApiModel _formMetadata;

        public FtIdRepository(ILogger<FtIdRepository> logger, FtIdHttpClient ftIdHttpClient)
        {
            _logger = logger;
            _ftIdHttpClient = ftIdHttpClient;
        }

        public async Task Add(string archiveReference, string ftid)
        {
            try
            {
                await _ftIdHttpClient.AddFtId(archiveReference, ftid);
            }
            catch (HttpRequestException hre)
            {
                if(hre.GetStatusCode() == System.Net.HttpStatusCode.BadRequest)
                {
                    _logger.LogError(hre, "Unable to add FtId: {FtId} to archiveReference: {archiveReference}", ftid, archiveReference);
                    throw;
                }
                else
                {
                    _logger.LogError(hre, "Error when adding FtId: {FtId} to archiveReference: {archiveReference}", ftid, archiveReference);
                    throw;
                }
            }
            
        }
    }
}