﻿using Ftb_DbModels;
using Ftb_Repositories.HttpClients;
using Ftb_Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Ftb_Repositories
{
    public class DistributionFormsRepository : IDistributionFormRepository
    {
        private ConcurrentBag<DistributionForm> _distributionForms = new ConcurrentBag<DistributionForm>();

        private string _archiveReference;
        private readonly ILogger<DistributionFormsRepository> _logger;
        private readonly DistributionFormsHttpClient _distributionFormsClient;

        public DistributionFormsRepository(ILogger<DistributionFormsRepository> logger, DistributionFormsHttpClient distributionFormsClient)
        {
            _logger = logger;
            _distributionFormsClient = distributionFormsClient;
        }
        public void SetArchiveReference(string archiveReference)
        {
            _archiveReference = archiveReference;
        }
        //public async Task<IEnumerable<DistributionForm>> GetAll(Guid mainDistributionFormReference)
        //{
        //    if (_distributionForms.Count == 0)
        //    {
        //        var dfs = await _distributionFormsClient.GetAll(_archiveReference);

        //        var df = dfs.FirstOrDefault(df => df.Id == mainDistributionFormReference);
        //        if(df != null)
        //            _distributionForms.Add(df);
        //    }



        //    return _distributionForms;
        //}

        public async Task<DistributionForm> Get(string id)
        {
            var result = _distributionForms.Where(d => d.Id.ToString().Equals(id)).FirstOrDefault();

            if (result == null)
            {
                result = await _distributionFormsClient.Get(id);
            }

            return result;
        }

        public async Task<DistributionForm> Get(Guid id)
        {
            var result = _distributionForms.FirstOrDefault(d => d.Id == id);

            if (result == null)
            {
                result = await _distributionFormsClient.Get(id.ToString());
                if (result != null)
                    _distributionForms.Add(result);
            }

            return result;
        }

        public async Task<bool> Update(string archiveReference, Guid id, DistributionForm updatedDistributionForm)
        {
            HttpResponseMessage result;
            result = await _distributionFormsClient.Put(archiveReference, id, updatedDistributionForm);
            if (!result.IsSuccessStatusCode)
            {
                _logger.LogError("Updating distribution form returned httpStatusCode {HttpStatusCode}", result.StatusCode);
            }
            return result.IsSuccessStatusCode;
        }

        public void Add(DistributionForm distributionForm)
        {
            if (_distributionForms.Where(d => d.Id == distributionForm.Id).Count() > 0)
                throw new ArgumentException($"Distribution form {distributionForm.Id} already exist");

            distributionForm.InitialArchiveReference = _archiveReference;
            _distributionForms.Add(distributionForm);
        }

        public async Task<bool> Save()
        {
            var sw = new Stopwatch();
            if (_distributionForms?.Count > 0)
            {
                HttpResponseMessage result;

                sw.Start();
                SynchronizeDistributionData();
                _logger.LogDebug("|DistributionFormsRepository.Save|: Elapsed time for synchronizing distribution data: {elapsedMilliseconds}", sw.ElapsedMilliseconds);
                var dfForUpdate = new List<DistributionForm>();
                var dfForPost = new List<DistributionForm>();

                sw.Restart();
                foreach (var df in _distributionForms)
                {
                    var dfInRepo = await _distributionFormsClient.Get(df.Id.ToString());
                    if (dfInRepo != null)
                        dfForUpdate.Add(df);
                    else
                        dfForPost.Add(df);
                }
                _logger.LogDebug("|DistributionFormsRepository.Save|: Elapsed time for distributing distribution forms to add and update lists: {elapsedMilliseconds}", sw.ElapsedMilliseconds);


                if (dfForUpdate.Count > 0)
                {
                    sw.Restart();
                    List<HttpResponseMessage> putResults = new List<HttpResponseMessage>();
                    foreach (var dfUpdate in dfForUpdate)
                    {
                        var putResult = await _distributionFormsClient.Put(dfUpdate.InitialArchiveReference, dfUpdate.Id, dfUpdate);
                        putResults.Add(putResult);
                    }

                    foreach (var res in putResults.Where(p => !p.IsSuccessStatusCode))
                    {
                        _logger.LogError("Persisting distribution form returned httpStatusCode {HttpStatusCode} from {RequestUri}", res.StatusCode, res.RequestMessage.RequestUri);
                    }

                    var failed = putResults.FirstOrDefault(p => !p.IsSuccessStatusCode);
                    if (failed != null)
                        result = failed;
                    else
                        result = putResults.First();
                    _logger.LogDebug("|DistributionFormsRepository.Save|: Elapsed time for updating distribution forms: {elapsedMilliseconds}", sw.ElapsedMilliseconds);
                }
                else
                {
                    sw.Restart();
                    result = await _distributionFormsClient.Post(_archiveReference, dfForPost);
                    _logger.LogDebug("|DistributionFormsRepository.Save|: Elapsed time for posting distribution forms: {elapsedMilliseconds}", sw.ElapsedMilliseconds);
                }

                if (!result.IsSuccessStatusCode)
                {
                    _logger.LogError("Persisting distribution form returned httpStatusCode {HttpStatusCode}", result.StatusCode);
                }
                return result.IsSuccessStatusCode;
            }
            else
            {
                _logger.LogWarning("No forms to persist!");
                return true;
            }

        }

        private void SynchronizeDistributionData()
        {
            foreach (var item in _distributionForms.Where(d => d.DistributionReference == Guid.Empty))
            {
                var children = _distributionForms.Where(d => d.DistributionReference == item.Id);
                if (children.Count() > 0)
                {
                    foreach (var child in children)
                    {
                        child.DistributionStatus = item.DistributionStatus;
                        child.DistributionType = item.DistributionType;
                        child.SubmitAndInstantiatePrefilled = item.SubmitAndInstantiatePrefilled;
                        child.SubmitAndInstantiatePrefilledFormTaskReceiptId = item.SubmitAndInstantiatePrefilledFormTaskReceiptId;
                        child.SignedArchiveReference = item.SignedArchiveReference;
                        child.Signed = item.Signed;
                        child.DistributionStatus = item.DistributionStatus;
                        child.RecieptSentArchiveReference = item.RecieptSentArchiveReference;
                        child.RecieptSent = item.RecieptSent;
                        child.ErrorMessage = item.ErrorMessage;
                        child.InitialExternalSystemReference = item.InitialExternalSystemReference;
                        child.Printed = item.Printed;
                    }
                }
            }
        }
    }
}
