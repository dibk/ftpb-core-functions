﻿using Dibk.Ftpb.Api.Clients.HttpClients;
using Dibk.Ftpb.Api.Models;
using Ftb_Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Threading.Tasks;

namespace Ftb_Repositories
{
    public class FormMetadataRepository : IFormMetadataRepository
    {
        private string _archiveReference;
        private readonly ILogger<FormMetadataRepository> _logger;
        private readonly FormMetadataHttpClient _formMetadataClient;
        private FormMetadataApiModel _formMetadata;

        public FormMetadataRepository(ILogger<FormMetadataRepository> logger, FormMetadataHttpClient formMetadataClient)
        {
            _logger = logger;
            _formMetadataClient = formMetadataClient;
        }

        public async Task<FormMetadataApiModel> Get()
        {
            var result = await _formMetadataClient.Get(_archiveReference);
            _formMetadata = result;
            return result;
        }

        public async Task<FormMetadataApiModel> Get(string archiveReference)
        {
            var result = await _formMetadataClient.Get(archiveReference);
            _formMetadata = result;
            return result;
        }

        public void Update(FormMetadataApiModel formMetadata)
        {
            _formMetadata = formMetadata;
        }

        public async void Create(FormMetadataApiModel formMetadata)
        {
            if (formMetadata != null)
            {
                try
                {
                    await _formMetadataClient.Create(formMetadata);
                }
                catch (HttpRequestException ex)
                {
                    if (ex.StatusCode == System.Net.HttpStatusCode.Conflict)
                    {
                        _logger.LogWarning("FormMetadata already exists");
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }

        public async Task Save()
        {
            if (_formMetadata != null)
                await _formMetadataClient.Update(_formMetadata);
        }

        public void SetArchiveReference(string archiveReference)
        {
            _archiveReference = archiveReference;
        }
    }
}