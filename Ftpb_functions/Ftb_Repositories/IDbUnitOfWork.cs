﻿using Ftb_Repositories.Interfaces;
using System.Threading.Tasks;

namespace Ftb_Repositories
{
    public interface IDbUnitOfWork
    {
        IDistributionFormRepository DistributionForms { get; }
        IFormMetadataRepository FormMetadata { get; }
        ILogEntryRepository LogEntries { get; }
        IFormAttachmentMetadataRepository FormAttachmentMetadata { get; }
        IFtIdRepository FtId { get; }

        Task<bool> SaveDistributionForms();

        Task SaveFormMetadata();

        Task SaveLogEntries();

        void SetArchiveReference(string archiveReference);
    }
}