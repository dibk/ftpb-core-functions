using Ftb_Repositories.HttpClients;
using Ftb_Repositories.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Text;
using System;

namespace Ftb_Repositories
{
    public static class ServicesConfiguration
    {
        public static IServiceCollection AddFtbRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            // -- Skal byttes ut med klienter fra Dibk.Ftpb.Api.Clients.HttpClients - START
            services.Configure<FormProcessAPISettings>(configuration.GetSection("FormProcessAPISettings"));
            services.AddHttpClient<DistributionFormsHttpClient>();
            services.AddHttpClient<LogEntryHttpClient>();
            services.AddHttpClient<IFileDownloadStatusHttpClient, FileDownloadStatusHttpClient>();
            // -- Skal byttes ut med klienter fra Dibk.Ftpb.Api.Clients.HttpClients - END

            services.AddScoped<IDbUnitOfWork, DbUnitOfWork>();
            services.AddScoped<ILogEntryRepository, LogEntryRepository>();
            services.AddScoped<IDistributionFormRepository, DistributionFormsRepository>();
            services.AddScoped<IFormMetadataRepository, FormMetadataRepository>();

            // -- Registrering av klienter fra Dibk.Ftpb.Api.Clients.HttpClients - START
            var userName = configuration["FormProcessAPISettings:BasicAuthUserName"];
            var password = configuration["FormProcessAPISettings:BasicAuthPassword"];
            var baseUri = new Uri(configuration["FormProcessAPISettings:Uri"]);

            var auth = Encoding.ASCII.GetBytes($"{userName}:{password}");
            var authHeader = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(auth));

            var t = services.AddHttpClient("ftpbApiClient", opt =>
            {
                opt.BaseAddress = baseUri;
                opt.DefaultRequestHeaders.Authorization = authHeader;
                opt.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            });

            services.AddScoped<IFormAttachmentMetadataRepository, FormAttachmentMetadataRepository>();
            services.AddScoped<IFtIdRepository, FtIdRepository>();
            services.AddScoped<Dibk.Ftpb.Api.Clients.HttpClients.FormMetadataHttpClient>();
            services.AddScoped<Dibk.Ftpb.Api.Clients.HttpClients.FtIdHttpClient>();
            services.AddScoped<Dibk.Ftpb.Api.Clients.HttpClients.MunicipalityHttpClient>();
            services.AddScoped<MunicipalityService>();
            // -- Registrering av klienter fra Dibk.Ftpb.Api.Clients.HttpClients - END

            return services;
        }
    }
}
