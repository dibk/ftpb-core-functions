﻿using Dibk.Ftpb.Api.Clients.HttpClients;
using Dibk.Ftpb.Api.Models;
using System.Threading.Tasks;

namespace Ftb_Repositories
{
    public enum FtPBContext
    {
        Bygg,
        Plan
    }

    public class MunicipalityService
    {
        private readonly MunicipalityHttpClient _httpClient;

        public MunicipalityService(MunicipalityHttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<(string Name, string OrganizationNumber)> GetMunicipalityOrgnr(string municipalityCode, FtPBContext ftPBContext = FtPBContext.Bygg)
        {
            var municipality = await _httpClient.GetMunicipality(municipalityCode);
            return (municipality.Name, GetOrgnr(municipality, ftPBContext));
        }

        private string GetOrgnr(MunicipalityApiModel municipality, FtPBContext ftPBContextEnum)
        {
            if (ftPBContextEnum == FtPBContext.Bygg)
                return municipality.OrganizationNumber;
            else
            {
                return !string.IsNullOrEmpty(municipality.PlanningDepartmentSpecificOrganizationNumber)
                    ? municipality.PlanningDepartmentSpecificOrganizationNumber
                    : municipality.OrganizationNumber;
            }
        }

        public async Task<MunicipalityApiModel> GetMunicipality(string municipalityCode)
        {
            return await _httpClient.GetMunicipality(municipalityCode);
        }
    }
}