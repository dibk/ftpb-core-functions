﻿using Ftb_DbModels;
using System;
using System.Threading.Tasks;

namespace Ftb_Repositories.Interfaces
{
    public interface IDistributionFormRepository
    {
        void Add(DistributionForm distributionForm);
        //Task<IEnumerable<DistributionForm>> GetAll(Guid mainDistributionFormReference);
        Task<DistributionForm> Get(string id);
        Task<DistributionForm> Get(Guid id);
        Task<bool> Update(string archiveReference, Guid id, DistributionForm updatedDistributionForm);
        void SetArchiveReference(string archiveReference);
        Task<bool> Save();
    }
}