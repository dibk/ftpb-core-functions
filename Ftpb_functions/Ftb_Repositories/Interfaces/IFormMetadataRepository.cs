﻿using Dibk.Ftpb.Api.Models;
using System.Threading.Tasks;

namespace Ftb_Repositories.Interfaces
{
    public interface IFormMetadataRepository
    {
        Task<FormMetadataApiModel> Get();
        Task<FormMetadataApiModel> Get(string archiveReference);
        void Update(FormMetadataApiModel formMetadata);
        Task Save();
        void SetArchiveReference(string archiveReference);
        void Create(FormMetadataApiModel formMetadata);
    }
}