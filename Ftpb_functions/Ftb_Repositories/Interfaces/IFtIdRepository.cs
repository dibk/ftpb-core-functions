﻿using System.Threading.Tasks;

namespace Ftb_Repositories.Interfaces
{
    public interface IFtIdRepository
    {
        Task Add(string archiveReference, string ftid);
    }
}