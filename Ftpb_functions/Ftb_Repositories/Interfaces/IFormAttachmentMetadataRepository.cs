﻿using Dibk.Ftpb.Api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ftb_Repositories.Interfaces
{
    public interface IFormAttachmentMetadataRepository
    {
        Task AddAttachmentsAsync(List<FormAttachmentMetadataApiModel> formAttachments);
        void SetArchiveReference(string archiveReference);
    }
}