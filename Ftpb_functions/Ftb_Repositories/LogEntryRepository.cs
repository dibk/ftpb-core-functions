﻿using Ftb_DbModels;
using Ftb_Repositories.HttpClients;
using Ftb_Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace Ftb_Repositories
{
    public class LogEntryRepository : ILogEntryRepository
    {
        private readonly ILogger<LogEntryRepository> _logger;
        private readonly LogEntryHttpClient _logEntryClient;
        private ConcurrentBag<LogEntry> _logEntries;
        private string _archiveReference;
        public LogEntryRepository(ILogger<LogEntryRepository> logger, LogEntryHttpClient logEntryClient)
        {
            _logEntries = new ConcurrentBag<LogEntry>();
            _logger = logger;
            _logEntryClient = logEntryClient;
        }
        public void SetArchiveReference(string archiveReference)
        {
            _archiveReference = archiveReference;
        }

        public async Task Save()
        {
            if (_logEntries?.Count > 0)
            {
                _logger.LogDebug($"#{_logEntries.Count} to save");
                foreach (var entry in _logEntries)
                    entry.ArchiveReference = _archiveReference;

                await _logEntryClient.Post(_archiveReference, _logEntries);

                _logEntries.Clear();
            }
            else _logger.LogDebug("No logentries to save");
        }

        public void AddInfo(string message)
        {
            _logEntries.Add(new LogEntry(_archiveReference, message, LogEntry.Info));
        }

        public void AddInfo(string message, string eventId)
        {
            _logEntries.Add(new LogEntry(_archiveReference, message, LogEntry.Info) { EventId = eventId });
        }
        public void AddInfoInternal(string message, string eventId)
        {
            _logEntries.Add(new LogEntry(_archiveReference, message, LogEntry.Info, LogEntry.InternalMsg) { EventId = eventId, });
        }

        public void AddErrorInternal(string message, string eventId)
        {
            _logEntries.Add(new LogEntry(_archiveReference, message, LogEntry.Error, LogEntry.InternalMsg) { EventId = eventId });
        }

        public void AddError(string message)
        {
            _logEntries.Add(new LogEntry(_archiveReference, message, LogEntry.Error));
        }

        public void AddNewError(string message, string eventId)
        {
            _logEntries.Add(new LogEntry(_archiveReference, message, LogEntry.Error) { EventId = eventId, });
        }
    }
}