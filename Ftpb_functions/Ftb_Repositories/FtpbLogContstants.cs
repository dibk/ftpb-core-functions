﻿namespace Ftb_Repositories.LogContstants
{
    public class FormMetadataStatus
    {
        public const string IKø = "I kø";
        public const string Ok = "Ok";
        public const string Feil = "Feil";
        public const string FeilDatavaliering = "Feil - datavalidering";
        public const string UnderBehandling = "Under behandling";
    }
}