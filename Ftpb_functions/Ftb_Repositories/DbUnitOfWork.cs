﻿using Ftb_Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Ftb_Repositories
{
    public class DbUnitOfWork : IDbUnitOfWork
    {
        private readonly ILogger<DbUnitOfWork> _logger;

        public DbUnitOfWork(ILogger<DbUnitOfWork> logger,
                            ILogEntryRepository logEntryRepository,
                            IFormMetadataRepository formMetadataRepository,
                            IDistributionFormRepository distributionFormRepository,
                            IFormAttachmentMetadataRepository formAttachmentMetadataRepository,
                            IFtIdRepository ftIdRepository
            )
        {
            _logger = logger;
            LogEntries = logEntryRepository;
            FormMetadata = formMetadataRepository;
            DistributionForms = distributionFormRepository;
            FormAttachmentMetadata = formAttachmentMetadataRepository;
            FtId = ftIdRepository;
        }

        private string _archiveReference;

        public void SetArchiveReference(string archiveReference)
        {
            _archiveReference = archiveReference;
            FormMetadata.SetArchiveReference(_archiveReference);
            DistributionForms.SetArchiveReference(_archiveReference);
            LogEntries.SetArchiveReference(_archiveReference);
            FormAttachmentMetadata.SetArchiveReference(_archiveReference);
        }

        public IDistributionFormRepository DistributionForms { get; }
        public IFormMetadataRepository FormMetadata { get; }
        public ILogEntryRepository LogEntries { get; }
        public IFormAttachmentMetadataRepository FormAttachmentMetadata { get; }
        public IFtIdRepository FtId { get; }

        public async Task SaveFormMetadata()
        {
            await FormMetadata.Save();
        }

        public async Task<bool> SaveDistributionForms()
        {
            return await DistributionForms.Save();
        }

        public async Task SaveLogEntries()
        {
            await LogEntries.Save();
        }
    }
}