﻿using Ftb_DbModels;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Ftb_Repositories.HttpClients
{
    public interface IFileDownloadStatusHttpClient
    {
        HttpClient Client { get; }

        Task<IEnumerable<FileDownloadStatus>> GetAllAsync(string archiveReference);
        Task<bool> PostAsync(string archiveReference, FileDownloadStatus fileDownload);
        Task<bool> PutAsync(string archiveReference, FileDownloadStatus fileDownload);
    }
}