﻿namespace Ftb_Repositories.HttpClients
{
    public class FormProcessAPISettings
    {
        public string Uri { get; set; }
        public string BasicAuthUserName { get; set; }
        public string BasicAuthPassword { get; set; }
    }
}
