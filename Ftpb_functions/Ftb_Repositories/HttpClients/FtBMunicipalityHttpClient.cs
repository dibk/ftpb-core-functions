﻿//using Ftb_DbModels;
//using Microsoft.Extensions.Logging;
//using Microsoft.Extensions.Options;
//using System;
//using System.Net.Http;
//using System.Net.Http.Headers;
//using System.Text;
//using System.Text.Json;
//using System.Threading.Tasks;

//namespace Ftb_Repositories.HttpClients
//{
//    public class FtBMunicipalityHttpClient
//    {
//        private readonly ILogger<FtBMunicipalityHttpClient> _logger;

//        public HttpClient Client { get; }

//        public FtBMunicipalityHttpClient(ILogger<FtBMunicipalityHttpClient> logger, HttpClient httpClient, IOptions<FtBMunicipalityAPISettings> settings)
//        {
//            _logger = logger;
//            Client = httpClient;
//            Client.BaseAddress = new Uri(settings.Value.Uri);
//            var authToken = Encoding.ASCII.GetBytes($"{settings.Value.BasicAuthUserName}:{settings.Value.BasicAuthPassword}");
//            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(authToken));
//        }

//        public async Task<MunicipalityApiModel> GetMunicipality(string municipalityCode)
//        {
//            try
//            {
//                var requestUri = $"{municipalityCode}";
//                var result = await Client.GetAsync(requestUri);

//                result.EnsureSuccessStatusCode();

//                var stringResult = await result.Content.ReadAsStringAsync();

//                return JsonSerializer.Deserialize<MunicipalityApiModel>(stringResult);
//            }
//            catch (HttpRequestException ex)
//            {
//                _logger.LogError(ex, "HttpRequestException occurred while trying to get municipality with code {MunicipalityCode}", municipalityCode);
//                throw new HttpRequestException("Error while trying to get municipality", ex);
//            }
//            catch (Exception ex)
//            {
//                _logger.LogError(ex, "An error occurred while trying to get municipality with code {MunicipalityCode}", municipalityCode);
//                throw;
//            }
//        }
//    }
//}