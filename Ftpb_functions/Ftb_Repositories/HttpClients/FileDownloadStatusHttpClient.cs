﻿using Ftb_DbModels;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Ftb_Repositories.HttpClients
{
    public class FileDownloadStatusHttpClient : IFileDownloadStatusHttpClient
    {
        public HttpClient Client { get; }
        private readonly ILogger _log;

        public FileDownloadStatusHttpClient(HttpClient httpClient, IOptions<FormProcessAPISettings> settings, ILogger<DistributionFormsHttpClient> log)
        {
            _log = log;
            Client = httpClient;
            Client.BaseAddress = new Uri(settings.Value.Uri);
            Client.DefaultRequestHeaders.Authorization = BasicAuthenticationHelper.GetAuthenticationHeader(settings.Value);
        }

        public async Task<bool> PostAsync(string archiveReference, FileDownloadStatus fileDownload)
        {
            var requestUri = $"api/formlogdata/{archiveReference}/filedownloads";
            var json = JsonSerializer.Serialize(fileDownload);
            var stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            var returnCode = await Client.PostAsync(requestUri, stringContent);

            if(!returnCode.IsSuccessStatusCode)
                _log.LogError("Adding FiledownloadStatus failed for {RequestUri}", requestUri);

            return returnCode.IsSuccessStatusCode;
        }

        public async Task<bool> PutAsync(string archiveReference, FileDownloadStatus fileDownload)
        {
            var requestUri = $"api/formlogdata/{archiveReference}/filedownloads";
            var json = JsonSerializer.Serialize(fileDownload);
            var stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            var returnCode = await Client.PutAsync(requestUri, stringContent);

            if (!returnCode.IsSuccessStatusCode)
                _log.LogError("Updating FiledownloadStatus failed for {RequestUri}", requestUri);

            return returnCode.IsSuccessStatusCode;
        }

        public async Task<IEnumerable<FileDownloadStatus>> GetAllAsync(string archiveReference)
        {
            if (archiveReference == null)
            {
                throw new ArgumentNullException("Id cannot be null");
            }

            var requestUri = $"api/formlogdata/{archiveReference}/filedownloads";

            var result = await Client.GetAsync(requestUri);

            IEnumerable<FileDownloadStatus> retVal = null;
            if (result.IsSuccessStatusCode)
            {
                var content = await result.Content.ReadAsStringAsync();
                retVal = JsonSerializer.Deserialize<IEnumerable<FileDownloadStatus>>(content);
            }
            else if (result.StatusCode != System.Net.HttpStatusCode.NotFound)
                _log.LogError("Unable to find FiledownloadStatus for {RequestUri}", requestUri);

            return retVal;
        }
    }
}
