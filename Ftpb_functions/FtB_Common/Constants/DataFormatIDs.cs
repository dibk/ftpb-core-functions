﻿namespace FtB_Common.Constants
{
    public static class DataFormatIDs
    {
        public const string InnsendingReguleringsplanforslag = "InnsendingAvReguleringsplanforslag";
        public const string VarselPlanoppstart = "VarselPlanoppstart";
        public const string VarselHoeringOgOffentligEttersyn = "VarselHoeringOgOffentligEttersyn";
        public const string UttalelseHoeringOgOffentligEttersyn = "UttalelseHoeringOgOffentligEttersyn";
        public const string UttalelseVarselPlanoppstart = "UttalelseVarselPlanoppstart";
        public const string IgangsettingstillatelseV3 = "IgangsettingstillatelseV3";
    }
}
