namespace FtB_Common.Constants
{
    public static class Tags
    {
        /// <summary>
        /// Liste med tillatte typer av vedleggsopplysninger, som legges som ekstrametadata på dokumenter i forsendelsen til svarut.
        /// Listen inneholder både systemdefinerte og brukerdefinerte metadata.
        /// </summary>
        public enum Vedleggsopplysninger
        {
            VedleggId,
            VedleggFilnavn,
            VedleggIfcValideringId,
            VedleggVersjonsnr,
            VedleggVersjonsdato,
            VedleggKategori,
            VedleggFulgtNabovarsel,
            VedleggKommentar,
            DatatypeVersjon,
            SkjemaVersjon,
            ReferanseXML
        }

        /// <summary>
        /// Liste med tillatte typer av vedleggsopplysninger som inneholder brukerdefinert metadata.
        /// Disse blir blant annet brukt for å kontrollere key verdien i userDefinedMetadata på dataelementer i altinn3 instanser.
        /// </summary>
        public enum VedleggsopplysningerFraSøker
        {
            VedleggId,
            VedleggIfcValideringId,
            VedleggVersjonsnr,
            VedleggVersjonsdato,
            VedleggFulgtNabovarsel,
            VedleggKommentar,
        }
        
        public const string DevelopmentTagValue = "DIBKDevelopment";

        public const string DataFormatId = "DataFormatId";
        public const string DataFormatVersion = "DataFormatVersion";
        public const string SubformName = "SubformName";
    }
}