﻿using System;
using System.Runtime.Serialization;

namespace FtB_Common.Exceptions
{
    [Serializable]
    public class WrongEnvironmentException : Exception
    {
        public WrongEnvironmentException()
        {
        }

        public WrongEnvironmentException(string message) : base(message)
        {
        }

        public WrongEnvironmentException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected WrongEnvironmentException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}