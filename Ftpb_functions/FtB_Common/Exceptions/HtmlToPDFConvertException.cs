﻿using System;

namespace FtB_Common.Exceptions
{
    public class HtmlToPDFConvertException : Exception
    {
        public string Text { get; set; }
        public int HTTPStatusCode { get; set; }
        public HtmlToPDFConvertException(string text, int hTTPStatusCode)
        {
            Text = text;
            HTTPStatusCode = hTTPStatusCode;
        }
    }
}
