﻿using System;

namespace FtB_Common.Exceptions
{
    public class DistributionSendExeception : Exception
    {
        public string Text { get; set; }
        public string ExternalSystemReference { get; set; }
        public Guid DistributionFormReferenceId { get; set; }

        public DistributionSendExeception(string externalSystemReference, Guid distributionFormReferenceId, string text)
        {
            Text = text;
            ExternalSystemReference = externalSystemReference;
            DistributionFormReferenceId = distributionFormReferenceId;
        }

        public override string ToString()
        {            
            return $"{base.Message} - {Text} - ExternalSystemReference: {ExternalSystemReference} - DistributionFormReferenceId: {DistributionFormReferenceId}";
        }
    }
}
