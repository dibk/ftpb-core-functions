﻿using System;

namespace FtB_Common.Exceptions
{
    public class FileDownloadStatusException : Exception
    {
        public string Text { get; set; }
        public FileDownloadStatusException(string text)
        {
            Text = text;
        }
    }
}
