﻿using System;
using System.Runtime.Serialization;

namespace FtB_Common.Exceptions
{
    [Serializable]
    public class CancelProcessException : Exception
    {
        public CancelProcessException()
        {
        }

        public CancelProcessException(string message) : base(message)
        {
        }

        public CancelProcessException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}