﻿using System;
using System.Runtime.Serialization;

namespace FtB_Common.Exceptions
{
    [Serializable]
    public class InvalidDistributionSenderException : Exception
    {
        public InvalidDistributionSenderException()
        {
        }

        public InvalidDistributionSenderException(string message) : base(message)
        {
        }

        public InvalidDistributionSenderException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}