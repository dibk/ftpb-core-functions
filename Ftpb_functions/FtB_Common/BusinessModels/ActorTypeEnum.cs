﻿using System.ComponentModel;

namespace FtB_Common.BusinessModels
{
    public enum ActorTypeEnum
    {
        [Description("Privatperson")]
        Privatperson,
        Foretak,
        [Description("Offentlig myndighet")]
        OffentligMyndighet,
        Organisasjon,
        Plankonsulent
    }
}
