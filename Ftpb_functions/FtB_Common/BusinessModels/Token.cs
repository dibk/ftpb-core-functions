﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using FtB_Common.Enums;

namespace FtB_Common.BusinessModels;

internal class Token(string token, TokenTypeEnum type, IEnumerable<string> scopes, string customIdentifier, DateTime? expiryTime) : IEquatable<Token>
{
    public string GetToken() => token;

    /// <returns>The expiry time in UTC</returns>
    public DateTime? GetExpiryTime() => expiryTime;

    private readonly TokenTypeEnum _type = type;
    private readonly HashSet<string> _scopes = scopes?.ToHashSet() ?? new HashSet<string>();
    private readonly string _customIdentifier  = customIdentifier;

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((Token)obj);
    }

    public bool Equals(Token other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return _scopes.SetEquals(other._scopes) && _customIdentifier == other._customIdentifier && _type == other._type;
    }

    public override int GetHashCode()
    {
        var scopesSorted = _scopes.ToImmutableSortedSet();
        return HashCode.Combine(string.Join(';', scopesSorted), _customIdentifier, (int)_type);
    }
}