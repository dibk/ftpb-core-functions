namespace FtB_Common.BusinessModels
{
    public interface IActor
    {
        string Id { get; set; }
        string Name { get; set; }
        string PresentationId { get; }
        ActorTypeEnum Type { get; set; }
    }

    public class Actor : IActor
    {
        public ActorTypeEnum Type { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }

        public string PresentationId
        {
            get
            {
                if (Id.Length > 11)
                {
                    //Most likely an encrypted SSN
                    return "anonymous actor";
                }
                return Id;
            }
        }
    }

    public class ActorInternal
    {
        public ActorTypeEnum Type { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string DecryptedId { get; set; }

        public ActorInternal(Actor actor)
        {
            Id = actor.Id;
            Type = actor.Type;
        }
    }

    public class ShipmentActor : Actor
    {
        public ShipmentActor(Actor actor)
        {
            base.Id = actor.Id;
            base.Name = actor.Name;
            base.Type = actor.Type;
        }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string Poststed { get; set; }

    }
}
