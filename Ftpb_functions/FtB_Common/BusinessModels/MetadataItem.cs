﻿namespace FtB_Common
{
    public class MetadataItem
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
