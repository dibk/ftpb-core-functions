﻿namespace FtB_Common.BusinessModels
{
    public class ReceiverDistinctKeyItem
    {
        public string decryptedId { get; }
        private bool isHoeringsmyndighet { get; }

        public ReceiverDistinctKeyItem(string decryptedId, bool receiverType)
        {
            this.decryptedId = decryptedId;
            this.isHoeringsmyndighet = receiverType;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj is not ReceiverDistinctKeyItem other)
                return false;

            return decryptedId == other.decryptedId && isHoeringsmyndighet == other.isHoeringsmyndighet;
        }

        public override int GetHashCode()
        {
            return System.HashCode.Combine(decryptedId, isHoeringsmyndighet);
        }
    }
}