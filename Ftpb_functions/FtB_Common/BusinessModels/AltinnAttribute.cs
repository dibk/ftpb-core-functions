﻿using System;

namespace FtB_Common.BusinessModels
{
    [AttributeUsage(AttributeTargets.Class)]
    public class AltinnAttribute : Attribute
    {
        public string Resource { get; set; }
        public string Type { get; set; }
    }

    public class AltinnEventTypes
    {
        public const string InstanceCompleted = "app.instance.process.completed";
        public const string InstanceCreated = "app.instance.created";
        public const string ValidateSubscription = "platform.events.validatesubscription";
    }
    public class AltinnEventResources
    {
        public const string InnsendingPlanforslag = "urn:altinn:resource:app_dibk_innsending-planforslag";
        public const string VarselHoffe = "urn:altinn:resource:app_dibk_varsel-hoffe";
        public const string VarselPlanoppstart = "urn:altinn:resource:app_dibk_varselplanoppstart";
        public const string UttalelseHoffe = "urn:altinn:resource:app_dibk_uttalelse-hoffe";
        public const string UttalelseVarselPlanoppstart = "urn:altinn:resource:app_dibk_uttalelse-varselplanoppstart";
        public const string IgangsettingstillatelseV3 = "urn:altinn:resource:app_dibk_ig-v3";
    }
}
