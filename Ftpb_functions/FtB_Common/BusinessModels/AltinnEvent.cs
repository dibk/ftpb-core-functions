﻿using System;
using System.Text.Json.Serialization;

namespace FtB_Common.BusinessModels
{
    public class AltinnEvent
    {
        public string Specversion { get; set; }
        public string Id { get; set; }
        public DateTime Time { get; set; }
        public string Source { get; set; }
        public string Type { get; set; }
        public string Resource { get; set; }
        public string ResourceInstance { get; set; }
        public string AlternativeSubject { get; set; }

        [JsonIgnore]
        public string ArchiveReference
        {
            get
            {
                if (string.IsNullOrEmpty(this.ResourceInstance))
                    return null;
                else return this.ResourceInstance[^12..];
            }
        }
    }
}