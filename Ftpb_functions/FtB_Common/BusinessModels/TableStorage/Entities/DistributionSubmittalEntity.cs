﻿using FtB_Common.Enums;
using System;

namespace FtB_Common.BusinessModels.TableStorage.Entities
{

    public class DistributionSubmittalEntity : FtpbTableEntity
    {
        public string SenderId { get; set; }
        public DateTime ReplyDeadline { get; set; }
        public int ReceiverCount { get; set; }
        public DateTime CreatedTimeStamp { get; set; }
        public string Status { get; set; }
        public Guid ReceiptsContainerName { get; set; }
        public string Type { get; set; }
    }
}