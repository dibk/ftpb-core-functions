﻿using FtB_Common.Enums;
using System;

namespace FtB_Common.BusinessModels.TableStorage.Entities
{
    public class NotificationSenderEntity : FtpbTableEntity
    {
        public string InitialExternalSystemReference { get; set; }
        public string SenderId { get; set; }
        public string SenderName { get; set; }
        public string SenderPhone { get; set; }
        public string SenderEmail { get; set; }
        public string ProcessStage { get; set; }
        public string ProcessOutcome { get; set; }
        public DateTime CreatedTimeStamp { get; set; }
        public string ReceiverId { get; set; }
        public string PlanId { get; set; }
        public string PlanNavn { get; set; }
        public string Reply { get; set; }
        public string Objection { get; set; }
    }
}