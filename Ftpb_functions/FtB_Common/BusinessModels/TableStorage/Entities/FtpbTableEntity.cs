﻿using Azure;
using Azure.Data.Tables;
using System;

namespace FtB_Common.BusinessModels.TableStorage.Entities
{
    public class FtpbTableEntity : ITableEntity
    {
        public string PartitionKey { get; set; }
        public string RowKey { get; set; }
        public DateTimeOffset? Timestamp { get; set; }
        public ETag ETag { get; set; }
    }
}