﻿using FtB_Common.Enums;

namespace FtB_Common.BusinessModels.TableStorage.Entities
{
    public class DistributionReceiverLogEntity : FtpbTableEntity
    {
        public string ReceiverId { get; set; }
        public string ReceiverName { get; set; }
        public string Status { get; set; }
    }
}