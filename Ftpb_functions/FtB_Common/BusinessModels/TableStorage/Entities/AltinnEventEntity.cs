﻿using FtB_Common.BusinessModels.TableStorage.Dtos;

namespace FtB_Common.BusinessModels.TableStorage.Entities
{
    public class AltinnEventEntity : FtpbTableEntity
    {
        public string EventStatus { get; set; }
        public string AltinnEventJson { get; set; }
    }
}