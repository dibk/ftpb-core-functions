﻿using FtB_Common.Enums;

namespace FtB_Common.BusinessModels.TableStorage.Entities
{
    public class NotificationSenderLogEntity : FtpbTableEntity
    {
        public string SenderId { get; set; }
        public string Status { get; set; }
    }
}