﻿using FtB_Common.Enums;
using System;

namespace FtB_Common.BusinessModels.TableStorage.Entities
{
    public class DistributionReceiverEntity : FtpbTableEntity
    {
        public string ReceiverId { get; set; }
        public string ReceiverName { get; set; }
        public string ProcessStage { get; set; }
        public string ProcessOutcome { get; set; }
        public DateTime CreatedTimeStamp { get; set; }
        public string ReceiverLogPartitionKey { get; set; }
        public string DistributionFormReferenceId { get; set; }
        public string SubmitPrefillTaskReceiptId { get; set; }
        public string PrefillReferenceId { get; set; }
    }
}