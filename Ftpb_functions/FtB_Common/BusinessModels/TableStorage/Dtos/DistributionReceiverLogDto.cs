﻿using FtB_Common.Enums;

namespace FtB_Common.BusinessModels.TableStorage.Dtos
{
    public class DistributionReceiverLogDto : BaseDto
    {
        public string ReceiverId { get; set; }
        public string ReceiverName { get; set; }
        public DistributionReceiverStatusLogEnum? Status { get; set; }

        public DistributionReceiverLogDto()
        { }

        public DistributionReceiverLogDto(string partitionKey, string rowKey) : base(partitionKey, rowKey)
        { }

        public DistributionReceiverLogDto(string partitionKey, string rowKey, string receiverId, string receiverName, DistributionReceiverStatusLogEnum status) : base(partitionKey, rowKey)
        {
            ReceiverId = receiverId;
            ReceiverName = receiverName;
            Status = status;
        }
    }
}