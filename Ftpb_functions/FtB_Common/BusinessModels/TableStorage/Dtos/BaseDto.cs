﻿namespace FtB_Common.BusinessModels.TableStorage.Dtos
{
    public class BaseDto : IBaseDto
    {
        public BaseDto()
        {}

        public BaseDto(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }

        public string PartitionKey { get; set; }
        public string RowKey { get; set; }
    }
}