﻿namespace FtB_Common.BusinessModels.TableStorage.Dtos
{
    public interface IBaseDto
    {
        string PartitionKey { get; set; }
        string RowKey { get; set; }
    }
}