﻿using FtB_Common.Enums;
using System;

namespace FtB_Common.BusinessModels.TableStorage.Dtos
{
    public class NotificationSenderDto : BaseDto
    {
        public string InitialExternalSystemReference { get; set; }
        public string SenderId { get; set; }
        public string SenderName { get; set; }
        public string SenderPhone { get; set; }
        public string SenderEmail { get; set; }
        public NotificationSenderProcessStageEnum? ProcessStage { get; set; }
        public string ProcessOutcome { get; set; }
        public DateTime CreatedTimeStamp { get; set; }
        public string ReceiverId { get; set; }
        public string PlanId { get; set; }
        public string PlanNavn { get; set; }
        public string Reply { get; set; }
        public string Objection { get; set; }

        public NotificationSenderDto()
        { }

        public NotificationSenderDto(string partitionKey, string rowKey) : base(partitionKey, rowKey)
        { }

        public NotificationSenderDto(string partitionKey, string rowKey, string senderId, NotificationSenderProcessStageEnum processStage, DateTime createdTimestamp) : base(partitionKey, rowKey)
        {
            SenderId = senderId;
            ProcessStage = processStage;
            CreatedTimeStamp = createdTimestamp;
        }
    }
}