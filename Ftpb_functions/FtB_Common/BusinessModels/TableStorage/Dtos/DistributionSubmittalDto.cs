﻿using FtB_Common.Enums;
using System;

namespace FtB_Common.BusinessModels.TableStorage.Dtos
{
    public class DistributionSubmittalDto : BaseDto
    {
        public string SenderId { get; set; }
        public DateTime ReplyDeadline { get; set; }
        public int ReceiverCount { get; set; }
        public DateTime CreatedTimeStamp { get; set; }
        public DistributionSubmittalStatusEnum? Status { get; set; }
        public Guid ReceiptsContainerName { get; set; }
        public string Type { get; set; }

        public DistributionSubmittalDto()
        { }

        public DistributionSubmittalDto(string archiveReference) : base(archiveReference, archiveReference)
        { }

        public DistributionSubmittalDto(string archiveReference, DistributionSubmittalStatusEnum status) : base(archiveReference, archiveReference)
        {
            Status = status;
        }

        public DistributionSubmittalDto(string archiveReference, string senderId, int receiverCount, string type, DateTime createdTimestamp) : base(archiveReference, archiveReference)
        {
            SenderId = senderId;
            ReceiverCount = receiverCount;
            Status = DistributionSubmittalStatusEnum.Created;
            Type = type;
            CreatedTimeStamp = createdTimestamp;
        }

        public override string ToString()
        {
            return $"PartitionKey: {PartitionKey} RowKey: {RowKey} SenderId: {SenderId} ReplyDeadline: {ReplyDeadline} ReceiverCount: {ReceiverCount} CreatedTimeStamp: {CreatedTimeStamp} Status: {Status}";
        }
    }
}