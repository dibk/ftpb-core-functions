﻿using FtB_Common.Enums;

namespace FtB_Common.BusinessModels.TableStorage.Dtos
{
    public class NotificationSenderLogDto : BaseDto
    {
        public string SenderId { get; set; }
        public NotificationSenderStatusLogEnum? Status { get; set; }

        public NotificationSenderLogDto()
        { }

        public NotificationSenderLogDto(string partitionKey, string rowKey) : base(partitionKey, rowKey)
        { }

        public NotificationSenderLogDto(string partitionKey, string rowKey, string senderId, NotificationSenderStatusLogEnum status) : base(partitionKey, rowKey)
        {
            SenderId = senderId;
            Status = status;
        }
    }
}