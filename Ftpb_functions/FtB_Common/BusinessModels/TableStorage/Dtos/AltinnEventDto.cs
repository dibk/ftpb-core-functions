﻿namespace FtB_Common.BusinessModels.TableStorage.Dtos
{
    public enum AltinnEventStatusEnum
    {
        Received,
        Processing,
        Completed,
        Failed
    }

    public class AltinnEventDto : BaseDto
    {
        public AltinnEventStatusEnum? EventStatus { get; set; }
        public string AltinnEventJson { get; set; }

        public AltinnEventDto()
        { }

        public AltinnEventDto(string archiveReference, string eventType) : base(archiveReference, eventType)
        { }

        public AltinnEventDto(string archiveReference, string eventType, string altinnEventJson) : base(archiveReference, eventType)
        {
            EventStatus = AltinnEventStatusEnum.Received;
            AltinnEventJson = altinnEventJson;
        }
    }
}