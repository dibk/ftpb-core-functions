﻿namespace FtB_Common.BusinessModels.TableStorage.Dtos
{
    public static class DistributionSubmittalTypes
    {
        public static string VORPA => "VORPA";
        public static string VORPAH => "VORPAH";
        public static string HOFFE => "HOFFE";
    }
}