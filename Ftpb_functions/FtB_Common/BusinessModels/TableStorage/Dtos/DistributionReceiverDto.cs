﻿using FtB_Common.Enums;
using System;

namespace FtB_Common.BusinessModels.TableStorage.Dtos
{
    public class DistributionReceiverDto : BaseDto
    {
        public string ReceiverId { get; set; }
        public string ReceiverName { get; set; }
        public DistributionReceiverProcessStageEnum? ProcessStage { get; set; }
        public ReceiverProcessOutcomeEnum? ProcessOutcome { get; set; }
        public DateTime CreatedTimeStamp { get; set; }
        public string ReceiverLogPartitionKey { get; set; }
        public string DistributionFormReferenceId { get; set; }
        public string SubmitPrefillTaskReceiptId { get; set; }
        public string PrefillReferenceId { get; set; }

        public DistributionReceiverDto()
        { }

        public DistributionReceiverDto(string partitionKey, string rowKey) : base(partitionKey, rowKey)
        { }

        public DistributionReceiverDto(string partitionKey, string rowKey, string receiverId, string receiverName, DistributionReceiverProcessStageEnum status, DateTime createdTimestamp, string receiverLogPartitionKey) : base(partitionKey, rowKey)
        {
            ReceiverId = receiverId;
            ReceiverName = receiverName;
            ProcessStage = status;
            CreatedTimeStamp = createdTimestamp;
            ReceiverLogPartitionKey = receiverLogPartitionKey;
        }
    }
}