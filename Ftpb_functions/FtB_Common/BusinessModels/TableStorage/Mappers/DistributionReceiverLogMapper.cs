﻿using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Entities;
using FtB_Common.Enums;
using System;

namespace FtB_Common.BusinessModels.TableStorage.Mappers
{
    public class DistributionReceiverLogMapper : IDtoEntityMapper<DistributionReceiverLogDto, DistributionReceiverLogEntity>
    {
        public DistributionReceiverLogDto MapToDto(DistributionReceiverLogEntity entity)
        {
            ArgumentNullException.ThrowIfNull(entity, nameof(entity));

            var dto = new DistributionReceiverLogDto(entity.PartitionKey, entity.RowKey)
            {
                ReceiverId = entity.ReceiverId,
                ReceiverName = entity.ReceiverName
            };

            if (Enum.TryParse<DistributionReceiverStatusLogEnum>(entity.Status, out var status))
                dto.Status = status;

            return dto;
        }

        public DistributionReceiverLogEntity MapToTableEntity(DistributionReceiverLogDto dto)
        {
            ArgumentNullException.ThrowIfNull(dto, nameof(dto));

            return new DistributionReceiverLogEntity
            {
                PartitionKey = dto.PartitionKey,
                RowKey = dto.RowKey,
                ReceiverId = dto.ReceiverId,
                ReceiverName = dto.ReceiverName,
                Status = dto.Status.ToString()
            };
        }
    }
}