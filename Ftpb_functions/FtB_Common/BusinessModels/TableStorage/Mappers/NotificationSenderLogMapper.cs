﻿using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Entities;
using FtB_Common.Enums;
using System;

namespace FtB_Common.BusinessModels.TableStorage.Mappers
{
    public class NotificationSenderLogMapper : IDtoEntityMapper<NotificationSenderLogDto, NotificationSenderLogEntity>
    {
        public NotificationSenderLogDto MapToDto(NotificationSenderLogEntity entity)
        {
            ArgumentNullException.ThrowIfNull(entity, nameof(entity));

            var dto = new NotificationSenderLogDto(entity.PartitionKey, entity.RowKey)
            {
                SenderId = entity.SenderId
            };

            if (Enum.TryParse<NotificationSenderStatusLogEnum>(entity.Status, out var status))
                dto.Status = status;

            return dto;
        }

        public NotificationSenderLogEntity MapToTableEntity(NotificationSenderLogDto dto)
        {
            ArgumentNullException.ThrowIfNull(dto, nameof(dto));

            return new NotificationSenderLogEntity
            {
                PartitionKey = dto.PartitionKey,
                RowKey = dto.RowKey,
                SenderId = dto.SenderId,
                Status = dto.Status.ToString()
            };
        }
    }
}