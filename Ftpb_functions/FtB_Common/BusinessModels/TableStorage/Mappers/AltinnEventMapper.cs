﻿using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Entities;
using System;

namespace FtB_Common.BusinessModels.TableStorage.Mappers
{
    public class AltinnEventMapper : IDtoEntityMapper<AltinnEventDto, AltinnEventEntity>
    {
        public AltinnEventDto MapToDto(AltinnEventEntity entity)
        {
            ArgumentNullException.ThrowIfNull(entity, nameof(entity));

            var dto = new AltinnEventDto(entity.PartitionKey, entity.RowKey)
            {
                PartitionKey = entity.PartitionKey,
                RowKey = entity.RowKey,
                AltinnEventJson = entity.AltinnEventJson
            };

            if (Enum.TryParse<AltinnEventStatusEnum>(entity.EventStatus, out var status))
                dto.EventStatus = status;

            return dto;
        }

        public AltinnEventEntity MapToTableEntity(AltinnEventDto dto)
        {
            ArgumentNullException.ThrowIfNull(dto, nameof(dto));

            return new AltinnEventEntity()
            {
                PartitionKey = dto.PartitionKey,
                RowKey = dto.RowKey,
                AltinnEventJson = dto.AltinnEventJson,
                EventStatus = dto.EventStatus.ToString()
            };
        }
    }
}