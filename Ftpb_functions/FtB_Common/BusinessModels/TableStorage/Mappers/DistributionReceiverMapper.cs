﻿using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Entities;
using FtB_Common.Enums;
using System;

namespace FtB_Common.BusinessModels.TableStorage.Mappers
{
    public class DistributionReceiverMapper : IDtoEntityMapper<DistributionReceiverDto, DistributionReceiverEntity>
    {
        public DistributionReceiverDto MapToDto(DistributionReceiverEntity entity)
        {
            ArgumentNullException.ThrowIfNull(entity, nameof(entity));

            var dto = new DistributionReceiverDto(entity.PartitionKey, entity.RowKey)
            {
                CreatedTimeStamp = entity.CreatedTimeStamp.ToLocalTime(),
                DistributionFormReferenceId = entity.DistributionFormReferenceId,
                PrefillReferenceId = entity.PrefillReferenceId,
                ReceiverId = entity.ReceiverId,
                ReceiverLogPartitionKey = entity.ReceiverLogPartitionKey,
                ReceiverName = entity.ReceiverName,
                SubmitPrefillTaskReceiptId = entity.SubmitPrefillTaskReceiptId
            };

            if (Enum.TryParse<DistributionReceiverProcessStageEnum>(entity.ProcessStage, out var processStage))
                dto.ProcessStage = processStage;

            if (Enum.TryParse<ReceiverProcessOutcomeEnum>(entity.ProcessOutcome, out var processOutcome))
                dto.ProcessOutcome = processOutcome;

            return dto;
        }

        public DistributionReceiverEntity MapToTableEntity(DistributionReceiverDto dto)
        {
            ArgumentNullException.ThrowIfNull(dto, nameof(dto));

            return new DistributionReceiverEntity
            {
                PartitionKey = dto.PartitionKey,
                RowKey = dto.RowKey,

                CreatedTimeStamp = dto.CreatedTimeStamp.ToUniversalTime(),
                DistributionFormReferenceId = dto.DistributionFormReferenceId,
                PrefillReferenceId = dto.PrefillReferenceId,
                ProcessOutcome = dto.ProcessOutcome?.ToString(),
                ProcessStage = dto.ProcessStage?.ToString(),
                ReceiverId = dto.ReceiverId,
                ReceiverLogPartitionKey = dto.ReceiverLogPartitionKey,
                ReceiverName = dto.ReceiverName,
                SubmitPrefillTaskReceiptId = dto.SubmitPrefillTaskReceiptId,
            };
        }
    }
}