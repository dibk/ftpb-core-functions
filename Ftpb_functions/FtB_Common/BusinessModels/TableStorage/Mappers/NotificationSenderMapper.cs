﻿using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Entities;
using FtB_Common.Enums;
using System;

namespace FtB_Common.BusinessModels.TableStorage.Mappers
{
    public class NotificationSenderMapper : IDtoEntityMapper<NotificationSenderDto, NotificationSenderEntity>
    {
        public NotificationSenderDto MapToDto(NotificationSenderEntity entity)
        {
            ArgumentNullException.ThrowIfNull(entity, nameof(entity));

            var dto = new NotificationSenderDto(entity.PartitionKey, entity.RowKey)
            {
                CreatedTimeStamp = entity.CreatedTimeStamp.ToLocalTime(),
                InitialExternalSystemReference = entity.InitialExternalSystemReference,
                Objection = entity.Objection,
                PlanId = entity.PlanId,
                PlanNavn = entity.PlanNavn,
                ProcessOutcome = entity.ProcessOutcome,
                ReceiverId = entity.ReceiverId,
                Reply = entity.Reply,
                SenderEmail = entity.SenderEmail,
                SenderId = entity.SenderId,
                SenderName = entity.SenderName,
                SenderPhone = entity.SenderPhone
            };

            if (Enum.TryParse<NotificationSenderProcessStageEnum>(entity.ProcessStage, out var processStage))
                dto.ProcessStage = processStage;

            return dto;
        }

        public NotificationSenderEntity MapToTableEntity(NotificationSenderDto dto)
        {
            ArgumentNullException.ThrowIfNull(dto, nameof(dto));

            return new NotificationSenderEntity
            {
                CreatedTimeStamp = dto.CreatedTimeStamp.ToUniversalTime(),
                InitialExternalSystemReference = dto.InitialExternalSystemReference,
                Objection = dto.Objection,
                PartitionKey = dto.PartitionKey,
                PlanId = dto.PlanId,
                PlanNavn = dto.PlanNavn,
                ProcessOutcome = dto.ProcessOutcome,
                ProcessStage = dto.ProcessStage.ToString(),
                ReceiverId = dto.ReceiverId,
                Reply = dto.Reply,
                RowKey = dto.RowKey,
                SenderEmail = dto.SenderEmail,
                SenderId = dto.SenderId,
                SenderName = dto.SenderName,
                SenderPhone = dto.SenderPhone
            };
        }
    }
}