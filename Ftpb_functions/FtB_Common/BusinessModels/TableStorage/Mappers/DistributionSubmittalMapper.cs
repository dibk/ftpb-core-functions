﻿using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Entities;
using FtB_Common.Enums;
using System;

namespace FtB_Common.BusinessModels.TableStorage.Mappers
{
    public class DistributionSubmittalMapper : IDtoEntityMapper<DistributionSubmittalDto, DistributionSubmittalEntity>
    {
        public DistributionSubmittalDto MapToDto(DistributionSubmittalEntity entity)
        {
            ArgumentNullException.ThrowIfNull(entity, nameof(entity));

            var dto = new DistributionSubmittalDto(entity.PartitionKey)
            {
                CreatedTimeStamp = entity.CreatedTimeStamp.ToLocalTime(),
                ReceiverCount = entity.ReceiverCount,
                ReplyDeadline = entity.ReplyDeadline.ToLocalTime(),
                SenderId = entity.SenderId,
                Type = entity.Type,
                ReceiptsContainerName = entity.ReceiptsContainerName
            };

            if (Enum.TryParse<DistributionSubmittalStatusEnum>(entity.Status, out var status))
                dto.Status = status;

            return dto;
        }

        public DistributionSubmittalEntity MapToTableEntity(DistributionSubmittalDto dto)
        {
            ArgumentNullException.ThrowIfNull(dto, nameof(dto));

            return new DistributionSubmittalEntity()
            {
                PartitionKey = dto.PartitionKey,
                RowKey = dto.RowKey,
                CreatedTimeStamp = dto.CreatedTimeStamp.ToUniversalTime(),
                ReceiverCount = dto.ReceiverCount,
                ReceiptsContainerName = dto.ReceiptsContainerName,
                SenderId = dto.SenderId,
                Type = dto.Type,
                ReplyDeadline = dto.ReplyDeadline.ToUniversalTime(),
                Status = dto.Status.ToString()
            };
        }
    }
}