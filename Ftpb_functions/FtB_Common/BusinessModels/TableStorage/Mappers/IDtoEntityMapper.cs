﻿using Azure.Data.Tables;
using FtB_Common.BusinessModels.TableStorage.Dtos;

namespace FtB_Common.BusinessModels.TableStorage.Mappers
{
    public interface IDtoEntityMapper<TDto, TEntity> where TDto : IBaseDto where TEntity : ITableEntity
    {
        TEntity MapToTableEntity(TDto dto);

        TDto MapToDto(TEntity entity);
    }
}