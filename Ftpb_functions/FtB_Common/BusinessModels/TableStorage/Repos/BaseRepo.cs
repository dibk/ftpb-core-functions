using Azure.Data.Tables;
using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Mappers;
using FtB_Common.Interfaces;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtB_Common.BusinessModels.TableStorage.Repos
{
    public class BaseRepo<TDto, TEntity> : IBaseRepo<TDto> where TDto : class, IBaseDto, new() where TEntity : class, ITableEntity, new()
    {
        private readonly ILogger _log;
        private readonly ITableStorage _tableStorage;
        private readonly IDtoEntityMapper<TDto, TEntity> _mapper;

        public BaseRepo(ILogger logger, ITableStorage tableStorage, IDtoEntityMapper<TDto, TEntity> mapper)
        {
            _log = logger;
            _tableStorage = tableStorage;
            _mapper = mapper;
        }

        public async Task<TDto> GetAsync(string partitionKey, string rowKey)
        {
            var entity = await _tableStorage.GetTableEntityAsync<TEntity>(partitionKey, rowKey);

            if (entity == null)
                return null;

            return _mapper.MapToDto(entity);
        }

        public async Task<IEnumerable<TDto>> GetAsync(string partitionKey)
        {
            var entities = await _tableStorage.GetTableEntitiesAsync<TEntity>(partitionKey);

            if (entities == null)
                return null;

            return entities.Select(e => _mapper.MapToDto(e)).ToList();
        }

        public IEnumerable<TDto> Get(IEnumerable<KeyValuePair<string, string>> filter)
        {
            var entities = _tableStorage.GetTableEntities<TEntity>(filter);

            if (entities == null)
                return null;

            return entities.Select(e => _mapper.MapToDto(e)).ToList();
        }

        public void Add(TDto dto)
        {
            var entity = _mapper.MapToTableEntity(dto);

            _tableStorage.InsertEntityRecord<TEntity>(entity);
        }

        public async Task AddAsync(TDto dto)
        {
            var entity = _mapper.MapToTableEntity(dto);

            await _tableStorage.InsertEntityRecordAsync<TEntity>(entity);
        }

        public async Task AddAsync(IEnumerable<TDto> dtos)
        {
            var entities = dtos.Select(_mapper.MapToTableEntity);

            await _tableStorage.InsertEntityRecordsAsync<TEntity>(entities);
        }

        public async Task<TDto> UpdateAsync(TDto dto)
        {
            var entity = _mapper.MapToTableEntity(dto);

            var updatedEntity = await _tableStorage.UpdateEntityRecordAsync<TEntity>(entity);

            return _mapper.MapToDto(updatedEntity);
        }

        public async Task<bool> UpdateAsync(IEnumerable<TDto> dtos)
        {
            var entities = dtos.Select(_mapper.MapToTableEntity);

            return await _tableStorage.UpdateEntitiesAsync(entities);
        }
    }
}