﻿using FtB_Common.BusinessModels.TableStorage.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FtB_Common.BusinessModels.TableStorage.Repos
{
    public interface IBaseRepo<TDto> where TDto : class, IBaseDto, new()
    {
        void Add(TDto dto);

        Task AddAsync(IEnumerable<TDto> dtos);

        Task AddAsync(TDto dto);

        IEnumerable<TDto> Get(IEnumerable<KeyValuePair<string, string>> filter);

        Task<IEnumerable<TDto>> GetAsync(string partitionKey);

        Task<TDto> GetAsync(string partitionKey, string rowKey);

        Task<bool> UpdateAsync(IEnumerable<TDto> dtos);

        Task<TDto> UpdateAsync(TDto dto);
    }
}