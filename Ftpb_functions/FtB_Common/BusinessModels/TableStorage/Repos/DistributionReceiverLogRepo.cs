﻿using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Entities;
using FtB_Common.BusinessModels.TableStorage.Mappers;
using FtB_Common.Interfaces;
using Microsoft.Extensions.Logging;

namespace FtB_Common.BusinessModels.TableStorage.Repos
{
    public class DistributionReceiverLogRepo : BaseRepo<DistributionReceiverLogDto, DistributionReceiverLogEntity>
    {
        public DistributionReceiverLogRepo(ILogger<DistributionReceiverLogRepo> logger,
                                           ITableStorage tableStorage,
                                           IDtoEntityMapper<DistributionReceiverLogDto, DistributionReceiverLogEntity> mapper) : base(logger, tableStorage, mapper)
        {
        }
    }
}