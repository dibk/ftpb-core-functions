﻿using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Entities;
using FtB_Common.BusinessModels.TableStorage.Mappers;
using FtB_Common.Interfaces;
using Microsoft.Extensions.Logging;

namespace FtB_Common.BusinessModels.TableStorage.Repos
{
    public class NotificationSenderRepo : BaseRepo<NotificationSenderDto, NotificationSenderEntity>
    {
        public NotificationSenderRepo(ILogger<NotificationSenderRepo> logger,
                                         ITableStorage tableStorage,
                                         IDtoEntityMapper<NotificationSenderDto, NotificationSenderEntity> mapper) : base(logger, tableStorage, mapper)
        {
        }
    }
}