﻿using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Entities;
using FtB_Common.BusinessModels.TableStorage.Mappers;
using FtB_Common.Interfaces;
using Microsoft.Extensions.Logging;

namespace FtB_Common.BusinessModels.TableStorage.Repos
{
    public class DistributionReceiverRepo : BaseRepo<DistributionReceiverDto, DistributionReceiverEntity>
    {
        public DistributionReceiverRepo(ILogger<DistributionReceiverRepo> logger,
                                        ITableStorage tableStorage,
                                        IDtoEntityMapper<DistributionReceiverDto, DistributionReceiverEntity> mapper) : base(logger, tableStorage, mapper)
        {
        }
    }
}