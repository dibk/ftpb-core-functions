﻿using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Entities;
using FtB_Common.BusinessModels.TableStorage.Mappers;
using FtB_Common.Interfaces;
using Microsoft.Extensions.Logging;

namespace FtB_Common.BusinessModels.TableStorage.Repos
{
    public class NotificationSenderLogRepo : BaseRepo<NotificationSenderLogDto, NotificationSenderLogEntity>
    {
        public NotificationSenderLogRepo(ILogger<NotificationSenderLogRepo> logger,
                                         ITableStorage tableStorage,
                                         IDtoEntityMapper<NotificationSenderLogDto, NotificationSenderLogEntity> mapper) : base(logger, tableStorage, mapper)
        {
        }
    }
}