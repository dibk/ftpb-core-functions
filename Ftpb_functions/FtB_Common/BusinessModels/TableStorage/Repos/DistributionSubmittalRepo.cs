﻿using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Entities;
using FtB_Common.BusinessModels.TableStorage.Mappers;
using FtB_Common.Interfaces;
using Microsoft.Extensions.Logging;

namespace FtB_Common.BusinessModels.TableStorage.Repos
{
    public class DistributionSubmittalRepo : BaseRepo<DistributionSubmittalDto, DistributionSubmittalEntity>
    {
        public DistributionSubmittalRepo(ILogger<DistributionSubmittalRepo> logger,
                                         ITableStorage tableStorage,
                                         IDtoEntityMapper<DistributionSubmittalDto, DistributionSubmittalEntity> mapper) : base(logger, tableStorage, mapper)
        {
        }
    }
}