﻿using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Entities;
using FtB_Common.BusinessModels.TableStorage.Mappers;
using FtB_Common.Interfaces;
using Microsoft.Extensions.Logging;

namespace FtB_Common.BusinessModels.TableStorage.Repos
{
    public class AltinnEventRepo : BaseRepo<AltinnEventDto, AltinnEventEntity>
    {
        public AltinnEventRepo(ILogger<AltinnEventRepo> logger,
                               ITableStorage tableStorage,
                               IDtoEntityMapper<AltinnEventDto, AltinnEventEntity> mapper) : base(logger, tableStorage, mapper)
        {
        }
    }
}