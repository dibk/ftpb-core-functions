﻿using FtB_Common.Enums;

namespace FtB_Common.BusinessModels;

public class StatusApiFileInfo
{
    public string Name { get; set; }
    public string FileName { get; set; }
    public FileTypesForDownloadEnum FileType { get; set; }
    public string MimeType { get; set; }
}