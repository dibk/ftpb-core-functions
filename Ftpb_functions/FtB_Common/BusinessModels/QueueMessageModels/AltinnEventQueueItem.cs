﻿using FtB_Common.Interfaces;

namespace FtB_Common.BusinessModels.QueueMessageModels
{
    public class AltinnEventQueueItem : IQueueItem
    {
        public string ArchiveReference { get; set; }        
        public string AltinnEventType { get; set; }
    }
}
