﻿using FtB_Common.Interfaces;

namespace FtB_Common.BusinessModels.QueueMessageModels
{
    public class ReportQueueItem : IQueueItem
    {
        public string ArchiveReference { get; set; }
        public string ReceiverSequenceNumber { get; set; }
        public string ReceiverLogPartitionKey { get; set; }
        public Receiver Receiver { get; set; }
        public Actor Sender { get; set; }
    }
}
