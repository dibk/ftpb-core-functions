﻿using FtB_Common.Interfaces;

namespace FtB_Common.BusinessModels.QueueMessageModels
{
    public class SubmittalQueueItem : IQueueItem
    {
        public string ArchiveReference { get; set; }
    }
}
