﻿using FtB_Common.Enums;

namespace FtB_Common.BusinessModels
{
    public class DistributionSender
    {
        public DistributionSenderRoleEnum Role { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

    }
}
