﻿namespace FtB_Common.BusinessModels
{
    public class Receiver : Actor
    {
        public string BlobItemName { get; set; }
        public bool DigitallyReserved { get; set; }
        public bool? IsHoeringsmyndighet { get; set; }
    }
}
