﻿using System;
using System.Collections.Generic;

namespace FtB_Common
{
    public static class ListExtensions
    {
        public static List<List<T>> Batch<T>(this List<T> collection, int batchSize)
        {
            var batches = new List<List<T>>();

            var counter = 0;

            while (counter < collection.Count)
            {
                var currentBatchSize = Math.Min(batchSize, collection.Count - counter);
                batches.Add(collection.GetRange(counter, currentBatchSize));
                counter += batchSize;
            }
            
            return batches; 
        }

    }
}
