﻿using System;

namespace FtB_Common.Extensions
{
    public static class LongExtensions
    {
        public static int ToMegabytes(this long numberOfBytes)
        {
            var mb = (numberOfBytes / 1024f) / 1024f;
            var roundedMb = (int)Math.Round(mb);
            return roundedMb;
        }
    }
}