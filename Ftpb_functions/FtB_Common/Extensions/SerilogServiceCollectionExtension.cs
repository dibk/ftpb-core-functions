using Altinn.Common;
using Elastic.Apm.SerilogEnricher;
using Elastic.Serilog.Sinks;
using Elastic.Transport;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;
using Serilog.Filters;
using System;

namespace FtB_Common
{
    public static class SerilogServiceCollectionExtension
    {
        public static LoggerConfiguration ConfigureSerilog(IConfiguration configuration)
        {
            var elasticDeploymenthUrl = configuration.GetValue<string>("Elasticsearch:DeploymentUrl");
            var elasticDataStreamName = configuration.GetValue<string>("Elasticsearch:DataStreamName");
            var elasticUsername = configuration.GetValue<string>("Elasticsearch:Username");
            var elasticPassword = configuration.GetValue<string>("Elasticsearch:Password");
            var logLevel = (LogEventLevel)Enum.Parse(typeof(LogEventLevel), configuration.GetValue<string>("Serilog:MinimumLevel:Default"));

            var config = new LoggerConfiguration()
                .MinimumLevel.Is(logLevel)
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("Host", LogEventLevel.Warning)
                .MinimumLevel.Override("Dibk.Ftpb.Integration.SvarUt.SvarUtClient", LogEventLevel.Verbose)
                .Enrich.FromLogContext()
                .Enrich.WithMachineName()
                .Enrich.WithElasticApmCorrelationInfo()                
                .WriteTo.Console();

            if (!string.IsNullOrEmpty(elasticDeploymenthUrl) && !string.IsNullOrEmpty(elasticUsername) && !string.IsNullOrEmpty(elasticPassword))
            {
                //Sink for all logs
                config.WriteTo.Elasticsearch([new Uri(elasticDeploymenthUrl)],
                       opts => { 
                           opts.DataStream = new Elastic.Ingest.Elasticsearch.DataStreams.DataStreamName(elasticDataStreamName);
                           opts.ConfigureChannel = channelOpts => {
                               channelOpts.BufferOptions = new Elastic.Channels.BufferOptions()
                               {
                                   OutboundBufferMaxSize = 200,
                               }; 
                           };
                       },
                       tr => { tr.Authentication(new BasicAuthentication(elasticUsername, elasticPassword)); });

                //Sink for altinn usage telemetry logs
                config.WriteTo.Logger(lc => lc.Filter.ByIncludingOnly(Matching.FromSource<AltinnTelemetryLogger>())
                                .WriteTo.Elasticsearch(new Uri[] { new Uri(elasticDeploymenthUrl) },
                                                       opts => { opts.DataStream = new Elastic.Ingest.Elasticsearch.DataStreams.DataStreamName("altinntelemetry"); },
                                                       tr => { tr.Authentication(new BasicAuthentication(elasticUsername, elasticPassword)); }));

                //Sink for SvarUt forsendelse payload logs
                config.WriteTo.Logger(lc => lc.Filter.ByIncludingOnly(Matching.WithProperty<string>("ForsendelsePayload", p => !string.IsNullOrEmpty(p)))
                                                .WriteTo.Elasticsearch(new Uri[] { new Uri(elasticDeploymenthUrl) },
                                                       opts => { opts.DataStream = new Elastic.Ingest.Elasticsearch.DataStreams.DataStreamName("forsendelsesvarut"); },
                                                       tr => { tr.Authentication(new BasicAuthentication(elasticUsername, elasticPassword)); }))
                                                .MinimumLevel.Verbose()
                                            .WriteTo.Console();
            }
            else
                Console.WriteLine("ERROR IN SERILOG CONFIGURATION - Unable to register elastic sink. URL is missing in config");


            return config;
        }
    }
}