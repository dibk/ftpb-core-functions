﻿using System.Collections.Generic;

namespace FtB_Common.Extensions;

public static class HashSetExtensions
{
    public static void Replace<T>(this HashSet<T> hashSet, T item)
    {
        hashSet.Remove(item);
        hashSet.Add(item);
    }
}