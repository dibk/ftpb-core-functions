﻿using FtB_Common.Constants;
using FtB_Common.Storage.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FtB_Common.Extensions
{
    public static class MetadataExtensions
    {
        public static string Type(this IEnumerable<KeyValuePair<string, string>> metadata)
        { return GetValue(metadata, BlobStorageMetadataKeys.Type); }
        public static string AttachmentTypeName(this IEnumerable<KeyValuePair<string, string>> metadata)
        { return GetValue(metadata, BlobStorageMetadataKeys.AttachmentTypeName); }
        public static string DataFormatId(this IEnumerable<KeyValuePair<string, string>> metadata)
        { return GetValue(metadata, Tags.DataFormatId); }
        public static string? DataFormatVersion(this IEnumerable<KeyValuePair<string, string>> metadata)
        { return GetValue(metadata, Tags.DataFormatVersion); }
        public static string? SubformName(this IEnumerable<KeyValuePair<string, string>> metadata)
        { return GetValue(metadata, Tags.SubformName); }
        public static string? SvarUtFilename(this IEnumerable<KeyValuePair<string, string>> metadata)
        { return GetValue(metadata, "SvarUtFilename"); }
        public static string? BlobNamePrefix(this IEnumerable<KeyValuePair<string, string>> metadata)
        { return GetValue(metadata, BlobStorageMetadataKeys.BlobNamePrefix); }
        public static List<KeyValuePair<string, string>>? Vedleggsopplysninger(this IEnumerable<KeyValuePair<string, string>> metadata)
        { return GetVedleggsopplysninger(metadata); }

        public static string ToString(this IEnumerable<KeyValuePair<string, string>> metadata, string separator)
        {
            return string.Join(separator, metadata.Select(p => $"'{p.Key}': '{p.Value}'"));
        }

        private static string? GetValue(IEnumerable<KeyValuePair<string, string>> metadata, string metadataKey)
        {
            if (metadata == null)
                return null;

            return metadata.SingleOrDefault(p => p.Key == metadataKey).Value;

        }

        private static List<KeyValuePair<string, string>>? GetVedleggsopplysninger(IEnumerable<KeyValuePair<string, string>> metadata)
        {
            var vedleggsopplysninger = new List<KeyValuePair<string, string>>();
            if (metadata == null)
                return null;

            // Finn DataFormatId og DataFormatVersion i metadata
            var dataFormatId = metadata.FirstOrDefault(m => m.Key == Tags.DataFormatId).Value;
            var dataFormatVersion = metadata.FirstOrDefault(m => m.Key == Tags.DataFormatVersion).Value;

            // Hvis begge verdier eksisterer, sett sammen DatatypeVersion og SkjemaVersjon
            if (!string.IsNullOrEmpty(dataFormatId) && !string.IsNullOrEmpty(dataFormatVersion))
            {
                var datatypeVersionValue = $"{dataFormatId}.{dataFormatVersion}";
                vedleggsopplysninger.Add(new KeyValuePair<string, string>(nameof(Tags.Vedleggsopplysninger.DatatypeVersjon), datatypeVersionValue));

                var skjemaVersionValue = $"{dataFormatId}.{dataFormatVersion}";
                vedleggsopplysninger.Add(new KeyValuePair<string, string>(nameof(Tags.Vedleggsopplysninger.SkjemaVersjon), skjemaVersionValue));
            }

            foreach (var item in metadata)
            {
                if (Enum.IsDefined(typeof(Tags.Vedleggsopplysninger), item.Key))
                {
                   vedleggsopplysninger.Add(item);
                }
            }

            return vedleggsopplysninger;

        }
    }
}
