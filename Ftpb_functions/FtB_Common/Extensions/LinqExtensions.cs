﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FtB_Common.Extensions
{
    public static class LinqExtensions
    {
        public static bool ContainsAll<T>(this IEnumerable<T> source, IEnumerable<T> values)
        {
            return values.All(value => source.Contains(value));
        }

        public static bool ContainsAll<T>(this IEnumerable<T> source, IEnumerable<T> values, IEqualityComparer<T> equalityComparer)
        {
            return values.All(value => source.Contains(value, equalityComparer));
        }
    }
}
