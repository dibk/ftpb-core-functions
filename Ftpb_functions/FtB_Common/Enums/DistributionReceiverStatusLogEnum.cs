﻿namespace FtB_Common.Enums
{
    public enum DistributionReceiverStatusLogEnum
    {
        Created,
        PrefillDataMapped,
        CorrespondenceSendingFailed,
        CorrespondenceSent,
        ReservedReportee,
        ReadyForReporting,
        Completed
    }
}
