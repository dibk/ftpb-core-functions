﻿namespace FtB_Common.Enums
{
    public enum DistributionSubmittalStatusEnum
    {
        Created,
        Processing,
        ReceiptSentToSubmitter,
        Distributed,
        ReportingInProgress,
        Reported
    }
}
