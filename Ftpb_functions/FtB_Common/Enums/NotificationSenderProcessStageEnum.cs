﻿namespace FtB_Common.Enums
{
    public enum NotificationSenderProcessStageEnum
    {
        Created,
        Reported
    }
}
