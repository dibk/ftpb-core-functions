﻿namespace FtB_Common.Enums
{
    public enum DistributionReceiverProcessStageEnum
    {
        Created,
        Distributing,
        PrefillSent,
        Distributed,
        ReadyForReporting,
        Reported,
    }
}
