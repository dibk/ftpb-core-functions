﻿namespace FtB_Common.Enums
{
    public enum ReceiverProcessOutcomeEnum
    {
        Sent,
        ReservedReportee,
        Failed
    }
}
