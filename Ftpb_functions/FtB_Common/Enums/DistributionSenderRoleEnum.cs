﻿namespace FtB_Common.Enums
{
    //TODO: Check if "AnsvarligSoker" is OK to use here
    public enum DistributionSenderRoleEnum
    {
        Forslagsstiller,
        Plankonsulent,
        AnsvarligSoker
    }
}
