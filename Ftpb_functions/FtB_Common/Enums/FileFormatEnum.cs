﻿using System.ComponentModel;

namespace FtB_Common.Enums;

public enum FileFormatEnum
{
    [Description("json")]
    Json = 1,
    [Description("xml")]
    Xml = 2,
}