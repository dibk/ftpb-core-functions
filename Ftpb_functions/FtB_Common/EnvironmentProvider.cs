﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Text;

namespace FtB_Common
{
    public enum EnvironmentType
    {
        Local,
        Development,
        Test,
        Production
    }

    public class EnvironmentProvider
    {
        /*
         Mulig workaround for å få riktig environment i Azure Functions. IWebHostEnvironment er ikke tilgjengelig i Azure Functions uten
         å benytte seg av ASPNET-CORE integrasjonen 'Microsoft.Azure.Functions.Worker.Extensions.Http.AspNetCore'. For enkelte funksjoner
         er det ei unødvendig pakke å ta i bruk. Bruker da IConfiguration for å hente ut environment variabelen.
         Mulig dette er for hacky og bør endres. :D
         */
        public EnvironmentType CurrentEnvironment { get; private set; } = EnvironmentType.Production;
        public bool IsDevelopment => CurrentEnvironment == EnvironmentType.Local || CurrentEnvironment == EnvironmentType.Development;
        public bool IsTest => CurrentEnvironment == EnvironmentType.Test;
        public bool IsProduction => CurrentEnvironment == EnvironmentType.Production;

        private readonly ILogger<EnvironmentProvider> _logger;
        private readonly IConfiguration _configuration;

        public EnvironmentProvider(ILogger<EnvironmentProvider> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            CurrentEnvironment = GetEnvironmentType();
        }

        private EnvironmentType GetEnvironmentType()
        {
            _logger.LogDebug("Retreives environment from configuration");
            TmpLogEnvironment();

            var environment = _configuration["Environment"];
            if (string.IsNullOrEmpty(environment))
            {
                _logger.LogDebug("Unable to find config for env variable 'Environment'. Defaults to prod..");
                return EnvironmentType.Production;
            }

            if (Enum.TryParse<EnvironmentType>(environment, true, out var result))
            { return result; }
            else
            {
                _logger.LogDebug("Unable to parse value '{ConfiguredEnvironment}' to enum value. Defaults to prod..", environment);
                return EnvironmentType.Production;
            }
        }

        //Kun for å se hva som er satt i environment variabler. Kan fjernes etterhvert.
        private void TmpLogEnvironment()
        {
            var sb = new StringBuilder();
            //Environment bestemmes slik:
            //https://learn.microsoft.com/en-us/aspnet/core/fundamentals/environments?view=aspnetcore-8.0
            var environment = _configuration["Environment"];
            sb.AppendLine($"Configuration 'Environment': {environment}");

            var dotnetEnvironment = _configuration["DOTNET_ENVIRONMENT"];
            sb.AppendLine($"Configuration 'DOTNET_ENVIRONMENT': {dotnetEnvironment}");

            var aspnetcoreEnvironment = _configuration["ASPNETCORE_ENVIRONMENT "];
            sb.AppendLine($"Configuration 'ASPNETCORE_ENVIRONMENT': {aspnetcoreEnvironment}");

            //MS sier at:
            //"Use this setting instead of ASPNETCORE_ENVIRONMENT when you need to change the runtime environment in Azure to something other than Production."
            //https://learn.microsoft.com/en-us/azure/azure-functions/functions-app-settings#azure_functions_environment
            var azureFunctionsEnvironment = _configuration["AZURE_FUNCTIONS_ENVIRONMENT"];
            sb.AppendLine($"Configuration 'AZURE_FUNCTIONS_ENVIRONMENT': {azureFunctionsEnvironment}");

            _logger.LogDebug(sb.ToString());
        }
    }
}