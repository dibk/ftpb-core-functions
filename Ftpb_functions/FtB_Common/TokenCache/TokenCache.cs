﻿using FtB_Common.BusinessModels;
using FtB_Common.Enums;
using FtB_Common.Extensions;
using Microsoft.IdentityModel.JsonWebTokens;
using System;
using System.Collections.Generic;
using System.Threading;

namespace FtB_Common.TokenCache;

/// <summary>
/// A cache for storing tokens. See <see cref="TokenTypeEnum"/> for the different types of tokens that can be stored. <br/>
/// </summary>
public sealed class TokenCache
{
    private readonly HashSet<Token> _tokens = new();
    private readonly SemaphoreSlim _semaphore = new(1);

    /// <summary>
    /// Stores the token in the cache. If a token with same <paramref name="tokenType"/>, <paramref name="scopes"/> and <paramref name="customIdentifier"/> already exists, it will be replaced.
    /// </summary>
    /// <param name="token">The new token value</param>
    /// <param name="tokenType">The type of the <paramref name="token"/></param>
    /// <param name="scopes">The scopes of the <paramref name="token"/></param>
    /// <param name="customIdentifier">A custom identifier for the <paramref name="token"/></param>
    /// <param name="expiryTime">The time of expiry for the <paramref name="token"/></param>
    public void SetToken(string token, TokenTypeEnum tokenType, IEnumerable<string> scopes, string customIdentifier, DateTime? expiryTime = null)
    {
        var jwtToken = new JsonWebToken(token);

        if (expiryTime == null || jwtToken.ValidTo < expiryTime)
            expiryTime = jwtToken.ValidTo;

        var tokenObject = new Token(token, tokenType, scopes, customIdentifier, expiryTime);

        _tokens.Replace(tokenObject);

        _semaphore.Release();
    }

    /// <summary>
    /// If this method returns <see langword="false"/>, it is very important to call <see cref="SetToken"/> as soon as possible. <br/>
    /// Failing to do so will result in a locked application, as the mutex will never be released.
    /// </summary>
    /// <param name="tokenType">The type of the <paramref name="tokenValue"/></param>
    /// <param name="scopes">The scopes of the <paramref name="tokenValue"/></param>
    /// <param name="customIdentifier">A custom identifier for the <paramref name="tokenValue"/></param>
    /// <param name="tokenValue">
    /// If the method returns <see langword="true"/>: The token value <br/>
    /// If the method returns <see langword="false"/>: <see cref="string.Empty"/>
    /// </param>
    /// <returns></returns>
    public bool TryGetTokenValue(TokenTypeEnum tokenType, IEnumerable<string> scopes, string customIdentifier, out string tokenValue)
    {
        _semaphore.Wait();
        
        var token = new Token(string.Empty, tokenType, scopes, customIdentifier, null);

        if (!_tokens.TryGetValue(token, out var existingToken))
        {
            tokenValue = string.Empty;
            return false;
        }

        var expiryTime = existingToken.GetExpiryTime();

        if (expiryTime.HasValue && expiryTime.Value <= DateTime.UtcNow)
        {
            tokenValue = string.Empty;
            return false;
        }

        tokenValue = existingToken.GetToken();
        _semaphore.Release();
        return true;
    }
}