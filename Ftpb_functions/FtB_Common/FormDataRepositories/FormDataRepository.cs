using FtB_Common.Enums;
using FtB_Common.Extensions;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace FtB_Common.FormDataRepositories
{
    public class FormDataRepository : IFormDataRepo
    {
        private readonly IBlobOperations _blobOperations;
        public FormDataRepository(IBlobOperations blobOperations)
        {
            _blobOperations = blobOperations;
        }

        public async Task<string> GetFormData(string archiveReference)
        {
            return await _blobOperations.GetFormdataAsync(archiveReference);
        }

        public List<AttachmentInfo> GetAttachments(string archiveReference)
        {
            return GetAttachments(archiveReference, BlobStorageMetadataTypes.SubmittalAttachment);
        }

        private List<AttachmentInfo> GetAttachments(string archiveReference, string attachmentType)
        {
            var attinfoList = new List<AttachmentInfo>();
            var metadataList = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, attachmentType)
            };

            var attachmentsFromBlobs = _blobOperations.GetAttachmentsByMetadata(BlobStorageEnum.Private, archiveReference, metadataList);

            foreach (var att in attachmentsFromBlobs)
            {
                var attachment = new AttachmentInfo()
                {
                    AttachmentTypeName = att.AttachmentTypeName,
                    Filename = att.Filename,
                    FileSize = att.Size,
                    FileSizeMb = ConvertBytesToMegabytes(att.Size)                    
                };
                attinfoList.Add(attachment);
            }

            return attinfoList;
        }

        public async Task<List<SubformInfo>> GetSubforms(string archiveReference)
        {
            var subforms = new List<SubformInfo>();
            var metadataList = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.Subform)
            };

            var subformBlobs = await _blobOperations.GetAllBlobsByFilterAsync(BlobStorageEnum.Private, archiveReference, metadataList);

            foreach (var subformBlob in subformBlobs)
            {
                var attachment = new SubformInfo()
                {
                    AttachmentTypeName = subformBlob.Metadata.AttachmentTypeName(),
                    Filename = subformBlob.FileName,
                    SubformName = subformBlob.Metadata.SubformName(),
                    DataFormatId = subformBlob.Metadata.DataFormatId(),
                    DataFormatVersion = subformBlob.Metadata.DataFormatVersion(),
                    FileSize = subformBlob.FileSize,
                    FileSizeMb = ConvertBytesToMegabytes(subformBlob.FileSize)
                };
                subforms.Add(attachment);
            }

            return subforms;
        }

        public async Task<string> GetSubformContent(string archiveReference, string subformFileName)
        {
            var blob = await _blobOperations.GetBlobStreamAsync(BlobStorageEnum.Private, archiveReference, subformFileName);
            if (blob == null)
                return null;

            string subformContent;
            using (var streamReader = new StreamReader(blob.Content))
                subformContent = await streamReader.ReadToEndAsync();

            return subformContent;
        }

        private static int ConvertBytesToMegabytes(long bytes)
        {
            var mb = (bytes / 1024f) / 1024f;
            var roundedMb = (int)Math.Round(mb);
            return roundedMb;
        }

        public async Task AddBytesAsBlob(string containerName, string fileName, byte[] fileBytes, IEnumerable<KeyValuePair<string, string>> metadata = null)
        {
            await _blobOperations.AddBytesToBlobStorageAsync(Enums.BlobStorageEnum.Private, containerName, fileName, fileBytes, null, metadata);
        }
    }

    public class AttachmentInfo
    {
        public string Filename { get; set; }
        public string AttachmentTypeName { get; set; }
        public long FileSize { get; set; }
        public int FileSizeMb { get; set; }
    }

    public class SubformInfo : AttachmentInfo
    {
        public string SubformName { get; set; }
        public string DataFormatId { get; set; }
        public string DataFormatVersion { get; set; }
    }
}
