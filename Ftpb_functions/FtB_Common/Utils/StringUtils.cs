﻿using System.Linq;

namespace FtB_Common.Utils
{
    public static class StringUtils
    {
        public static string RemoveInvalidJsonCharacters(string input)
        {
            return input.Replace("\r\n", "").Replace("\t", " ").Replace("\"", "'");
        }

        public static string RemoveInvalidChars(this string input)
        {
            char[] invalidFileNameChars = System.IO.Path.GetInvalidFileNameChars();
            return new string(input.Where(ch => !invalidFileNameChars.Contains(ch)).ToArray());
        }
    }
}
