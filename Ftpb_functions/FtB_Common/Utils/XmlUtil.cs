using System;
using System.Linq;
using System.Xml.Linq;

namespace FtB_Common.Utils
{
    public class XmlUtil
    {
        private static string RemoveAllNamespaces(string xmlDocument)
        {
            XElement xmlDocumentWithoutNs = RemoveAllNamespaces(XElement.Parse(xmlDocument));

            return xmlDocumentWithoutNs.ToString();
        }

        //Core recursion function
        private static XElement RemoveAllNamespaces(XElement xmlDocument)
        {
            if (!xmlDocument.HasElements)
            {
                XElement xElement = new XElement(xmlDocument.Name.LocalName);
                xElement.Value = xmlDocument.Value;

                foreach (XAttribute attribute in xmlDocument.Attributes())
                    xElement.Add(attribute);

                return xElement;
            }
            return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
        }

        public static string GetElementValue(string xmlString, string elementName)
        {
            try
            {
                XDocument document = XDocument.Parse(RemoveAllNamespaces(xmlString));
                var node = document.Descendants().Where(e => e.Name.LocalName.ToLower().Equals(elementName.ToLower())).FirstOrDefault();

                return node?.Value?.Trim();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string GetAttributeValue(string xmlString, string elementName, string attributeName)
        {
            try
            {
                XDocument document = XDocument.Parse(xmlString);
                var node = document.Descendants().Where(e => e.Name.LocalName.ToLower().Equals(elementName.ToLower())).FirstOrDefault();

                return node?.Attribute(attributeName)?.Value?.Trim();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string GetAttributeValueFromXml(string xmlString, string attributeName)
        {
            try
            {
                // Parse XML-strengen til et XDocument
                XDocument document = XDocument.Parse(xmlString);

                // Finn rot-elementet
                var rootElement = document.Root;

                // Returner verdien av attributtet hvis det finnes i rot-elementet
                return rootElement?.Attribute(attributeName)?.Value?.Trim();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}