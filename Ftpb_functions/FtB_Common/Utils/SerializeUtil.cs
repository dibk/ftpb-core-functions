﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace FtB_Common.Utils
{
    public class SerializeUtil
    {
        public static T DeserializeXML<T>(Stream xmlStream) where T : class
        {
            if (xmlStream == null)
                return null;

            using var reader = XmlReader.Create(xmlStream);
            reader.MoveToContent();

            var serializer = new XmlSerializer(typeof(T));

            return serializer.Deserialize(reader) as T;
        }

        public static T DeserializeXml<T>(string xmlString)
        {
            return (T) DeserializeFromString(xmlString, typeof(T));
        }

        private static object DeserializeFromString(string objectData, Type type)
        {
            var serializer = new XmlSerializer(type);
            object result;

            TextReader reader = null;

            try
            {
                reader = new StringReader(objectData);
                result = serializer.Deserialize(reader);
            }
            finally
            {
                reader?.Close();
            }

            return result;
        }


        public static string Serialize(object form)
        {
            var serializer = new XmlSerializer(form.GetType());
            var stringWriter = new Utf8StringWriter();
            serializer.Serialize(stringWriter, form);

            return stringWriter.ToString();
        }

        public static string SerializeWithNoWhitespaces(object form, XmlWriterSettings settings = null)
        {
            settings ??= new XmlWriterSettings();
            settings.Indent = false;
            settings.NewLineHandling = NewLineHandling.None;

            var serializer = new XmlSerializer(form.GetType());
            var stringWriter = new Utf8StringWriter();

            using (XmlWriter writer = XmlWriter.Create(stringWriter, settings))
            {
                serializer.Serialize(writer, form);
            }

            return stringWriter.ToString();
        }
    }
}
