﻿namespace FtB_Common.Storage.Metadata;

public static class BlobStorageMetadataTypes
{
    public const string ArchivedItemInformation = "ArchivedItemInformation";
    public const string MainForm = "MainForm";
    public const string FormData = "FormData";
    public const string Subform = "Subform";
    public const string SubmittalAttachment = "SubmittalAttachment";
    public const string ValidationResult = "ValidationResult";
    public const string GeneratedAttachment = "GeneratedAttachment";
    public const string PdfPrintImages = "PdfPrintImages";
    public const string FormDataWithNoReceivers = "FormDataWithNoReceivers";
    public const string UniqueReceiver = "UniqueReceiver";
    public const string AttachmentCollection = "AttachmentCollection";
}