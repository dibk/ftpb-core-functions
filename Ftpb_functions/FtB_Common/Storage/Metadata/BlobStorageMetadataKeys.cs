﻿namespace FtB_Common.Storage.Metadata;

public static class BlobStorageMetadataKeys
{
    public const string Type = "Type";
    public const string AttachmentTypeName = "AttachmentTypeName";
    public const string PublicBlobContainerName = "PublicBlobContainerName";
    public const string InstanceSource = "InstanceSource";
    public const string SendersArchiveReference = "SendersArchiveReference";
    public const string BlobNamePrefix = "BlobNamePrefix";
    public const string ExcludeInReceipt = "ExcludeInReceipt";
}