﻿using System.Collections.Generic;

namespace FtB_Common.Storage
{
    public class BlobContainerItem
    {
        public string Name { get; set; }
        public Dictionary<string,string> Metadata { get; set; }

        public BlobContainerItem(string name, IDictionary<string,string> metadata)
        {
            Name = name;
            if(metadata != null)
            {
                Metadata = metadata.ToDecodedDictionary();
            }
        }
    }
}
