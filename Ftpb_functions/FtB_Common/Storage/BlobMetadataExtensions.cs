﻿using System.Collections.Generic;

namespace FtB_Common.Storage
{
    public static class BlobMetadataExtensions
    {
        public static Dictionary<string, string> ToEncodedDictionary(this IEnumerable<KeyValuePair<string, string>> keyValuePairs)
        {
            if (keyValuePairs == null)
                return null;

            var dict = new Dictionary<string, string>();
            foreach (var keyValuePair in keyValuePairs)
            {
                dict.Add(keyValuePair.Key, System.Web.HttpUtility.UrlEncode(keyValuePair.Value));
            }
            return dict;
        }

        public static Dictionary<string, string> ToDecodedDictionary(this IDictionary<string,string> dictionary)
        {
            var decodedDictionary = new Dictionary<string, string>();
            foreach (var item in dictionary)
            {
                decodedDictionary.Add(item.Key, System.Web.HttpUtility.UrlDecode(item.Value));
            }

            return decodedDictionary;
        }
    }
}