﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.Extensions.Azure;
using System.Threading.Tasks;

namespace FtB_Common.Storage
{
    public class PrivateBlobStorage : BlobStorage
    {
        public PrivateBlobStorage(IAzureClientFactory<BlobServiceClient> clientFactory)
        {
            _blobServiceClient = clientFactory.CreateClient("PrivateStorage");
        }

        public override async Task CreateContainerIfNotExistsAsync(string containerName)
        {
            var containerClient = GetBlobContainerClient(containerName);
            await containerClient.CreateIfNotExistsAsync(PublicAccessType.None);
        }
    }
}