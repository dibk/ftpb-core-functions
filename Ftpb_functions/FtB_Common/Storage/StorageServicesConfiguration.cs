﻿using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Entities;
using FtB_Common.BusinessModels.TableStorage.Mappers;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Interfaces;
using FtB_Common.Storage.TableStorage;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.DependencyInjection;

namespace FtB_Common.Storage
{
    public static class StorageServicesConfiguration
    {
        public static IServiceCollection AddStorageClients(this IServiceCollection services, string privateStorageConnectionString, string publicStorageConnectionString)
        {
            services.AddScoped<PrivateBlobStorage>();
            services.AddScoped<PublicBlobStorage>();
            services.AddScoped<ITableStorage, TableStorageService>();
            services.AddScoped<ITableClientFactory, TableClientFactory>();

            services.AddBasicServices(privateStorageConnectionString, publicStorageConnectionString);

            return services;
        }

        public static IServiceCollection AddSingeltonStorageClients(this IServiceCollection services, string privateStorageConnectionString, string publicStorageConnectionString)
        {
            services.AddSingleton<PrivateBlobStorage>();
            services.AddSingleton<PublicBlobStorage>();
            services.AddSingleton<ITableStorage, TableStorageService>();
            services.AddSingleton<ITableClientFactory, TableClientSingletonFactory>();
            services.AddBasicServices(privateStorageConnectionString, publicStorageConnectionString);

            return services;
        }

        public static IServiceCollection AddDtoRepos(this IServiceCollection services)
        {
            services.AddScoped<AltinnEventRepo>();
            services.AddScoped<IDtoEntityMapper<AltinnEventDto, AltinnEventEntity>, AltinnEventMapper>();

            services.AddScoped<DistributionReceiverLogRepo>();
            services.AddScoped<IDtoEntityMapper<DistributionReceiverLogDto, DistributionReceiverLogEntity>, DistributionReceiverLogMapper>();

            services.AddScoped<DistributionReceiverRepo>();
            services.AddScoped<IDtoEntityMapper<DistributionReceiverDto, DistributionReceiverEntity>, DistributionReceiverMapper>();

            services.AddScoped<DistributionSubmittalRepo>();
            services.AddScoped<IDtoEntityMapper<DistributionSubmittalDto, DistributionSubmittalEntity>, DistributionSubmittalMapper>();

            services.AddScoped<NotificationSenderLogRepo>();
            services.AddScoped<IDtoEntityMapper<NotificationSenderLogDto, NotificationSenderLogEntity>, NotificationSenderLogMapper>();

            services.AddScoped<NotificationSenderRepo>();
            services.AddScoped<IDtoEntityMapper<NotificationSenderDto, NotificationSenderEntity>, NotificationSenderMapper>();

            return services;
        }

        private static IServiceCollection AddBasicServices(this IServiceCollection services, string privateStorageConnectionString, string publicStorageConnectionString)
        {
            var IsClientLoggingEnabled = false;

            services.AddScoped<IBlobOperations, BlobOperations>();
            services.AddAzureClients(builder =>
            {
                //Private blob storage
                builder.AddBlobServiceClient(privateStorageConnectionString)
                       .ConfigureOptions(o => { o.Diagnostics.IsLoggingEnabled = IsClientLoggingEnabled; })
                       .WithName("PrivateStorage");

                //Public blob storage
                builder.AddBlobServiceClient(publicStorageConnectionString)
                       .ConfigureOptions(o => { o.Diagnostics.IsLoggingEnabled = IsClientLoggingEnabled; })
                       .WithName("PublicStorage");

                //Private table storage
                builder.AddTableServiceClient(privateStorageConnectionString)
                       .ConfigureOptions(o => { o.Diagnostics.IsLoggingEnabled = IsClientLoggingEnabled; })
                       .WithName("PrivateTableStorage");
            });

            return services;
        }
    }
}