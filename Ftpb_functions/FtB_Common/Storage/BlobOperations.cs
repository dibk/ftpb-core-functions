using Altinn.Common.Models;
using Azure;
using Azure.Storage.Blobs.Models;
using Azure.Storage.Blobs.Specialized;
using FtB_Common.BusinessModels;
using FtB_Common.Enums;
using FtB_Common.Extensions;
using FtB_Common.Storage.Metadata;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FtB_Common.Storage
{
    public class BlobItemBase
    {
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public string BlobUrl { get; set; }
        public long FileSize { get; set; }
        public IEnumerable<KeyValuePair<string, string>> Metadata { get; set; }
    }

    public class BlobItem : BlobItemBase
    { }

    public class BlobStreamItem : BlobItemBase
    {
        public Stream Content { get; set; }
    }

    public class BlobContent : BlobItemBase
    {
        public byte[] Content { get; set; }
    }

    public class BlobOperations : IBlobOperations
    {
        private Dictionary<string, ArchivedItemInformation> _archivedItems = new Dictionary<string, ArchivedItemInformation>();
        private readonly ILogger<BlobOperations> _log;
        private PrivateBlobStorage _privateBlobStorage;
        private PublicBlobStorage _publicBlobStorage;
        private string _containerLeaseId;

        public BlobOperations(ILogger<BlobOperations> logger, PrivateBlobStorage privateBlobStorage, PublicBlobStorage publicBlobStorage)
        {
            _log = logger;
            _privateBlobStorage = privateBlobStorage;
            _publicBlobStorage = publicBlobStorage;
        }

        public string GetBlobUri(BlobStorageEnum blobStorageEnum)
        {
            return blobStorageEnum == BlobStorageEnum.Private ? _privateBlobStorage.GetBlobUri().ToString() : _publicBlobStorage.GetBlobUri().ToString();
        }

        public async Task<bool> AcquireContainerLeaseAsync(string containerName, int seconds)
        {
            try
            {
                var containerClient = _privateBlobStorage.GetBlobContainerClient(containerName);

                BlobLeaseClient blobLeaseClient = containerClient.GetBlobLeaseClient();
                var blobLeaseResponse = await blobLeaseClient.AcquireAsync(TimeSpan.FromSeconds(seconds));
                _containerLeaseId = blobLeaseClient.LeaseId;
                _log.LogInformation("Container {ContainerName} successfully leased with LeaseId={ContainerLeaseId}.", containerName, _containerLeaseId);
                return true;
            }
            catch (RequestFailedException rfx)
            {
                if (rfx.Status == 409)
                {
                    _log.LogInformation(rfx, "Container {ContainerName} failed for leasing. HTTP-status: {HttpStatusCode}. Returning false.", containerName, rfx.Status);
                    return false;
                }
                _log.LogError(rfx, "Exception: Container {ContainerName} failed for leasing. HTTp-status: {HttpStatusCode}. Throwing exception.", containerName, rfx.Status);
                throw;
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Exception: Container {ContainerName} failed for leasing.", containerName);
                throw;
            }
        }

        public async Task<bool> ReleaseContainerLeaseAsync(string containerName)
        {
            try
            {
                var containerClient = _privateBlobStorage.GetBlobContainerClient(containerName);
                BlobLeaseClient blobLeaseClient = containerClient.GetBlobLeaseClient(_containerLeaseId);
                Response<ReleasedObjectInfo> releaseObjectInfo = await blobLeaseClient.ReleaseAsync();
                _log.LogInformation("LeaseId {ContainerLeaseId} for container {ContainerName} successfully released.", _containerLeaseId, containerName);
                return true;
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Exception: LeaseId {ContainerLeaseId} for container {ContainerName} failed for releasing.", _containerLeaseId, containerName);
                return false;
            }
        }

        private async Task<ArchivedItemInformation> GetArchivedItemFromBlobAsync(string containerName)
        {
            ArchivedItemInformation retVal = null;

            try
            {
                var archivedItemJson = await GetBlobContentAsStringByBlobItemNameAsync(BlobStorageEnum.Private, containerName, $"ArchivedItemInformation_{containerName}.json");
                return JsonConvert.DeserializeObject<ArchivedItemInformation>(archivedItemJson);
            }
            catch
            {
                _log.LogDebug("Unable to find ArchivedItemInformation by name in container {containerName}. Proceeding to search by metadata.", containerName);
            }

            try
            {
                foreach (var blobItem in _privateBlobStorage.GetBlobContainerItems(containerName))
                {
                    var metadataItem = blobItem.Metadata.Where(m => m.Key.Equals(BlobStorageMetadataKeys.Type, StringComparison.OrdinalIgnoreCase)
                            && m.Value.Equals(BlobStorageMetadataTypes.ArchivedItemInformation, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

                    if (!metadataItem.Equals(default(KeyValuePair<string, string>)))
                    {
                        StringBuilder sb = new StringBuilder();
                        var blobContainerClient = _privateBlobStorage.GetBlobContainerClient(containerName);
                        var client = blobContainerClient.GetBlobClient(blobItem.Name);
                        if (await client.ExistsAsync())
                        {
                            var response = await client.DownloadAsync();
                            using (var streamReader = new StreamReader(response.Value.Content))
                            {
                                while (!streamReader.EndOfStream)
                                {
                                    sb.Append(await streamReader.ReadLineAsync());
                                }
                            }
                        }
                        retVal = JsonConvert.DeserializeObject<ArchivedItemInformation>(sb.ToString());
                        break;
                    }
                }
            }
            catch (RequestFailedException rfEx)
            {
                _log.LogError(rfEx, "Failed getting archived item from blob storage {ContainerName}. HttpStatusCode: {HttpStatusCode}", containerName, rfEx.Status);
                throw;
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Failed getting archived item from blob storage {ContainerName}", containerName);
                throw;
            }

            return retVal;
        }

        private async Task<ArchivedItemInformation> CacheArchivedItemFromBlobAsync(string containerName)
        {
            var item = await GetArchivedItemFromBlobAsync(containerName);

            if (item != null)
            {
                _archivedItems.Add(containerName, item);
                return item;
            }

            return null;
        }

        private async Task<ArchivedItemInformation> GetArchivedItemInformationAsync(string containerName)
        {
            var archiveReference = containerName.ToLower();

            if (!_archivedItems.TryGetValue(archiveReference, out var item))
                item = await CacheArchivedItemFromBlobAsync(archiveReference);

            return item;
        }

        public async Task<string> GetServiceCodeFromStoredBlobAsync(string containerName)
        {
            var result = await GetArchivedItemInformationAsync(containerName);

            return result!.ServiceCode;
        }

        public async Task<string> GetFormatIdFromStoredBlobAsync(string containerName)
        {
            var result = await GetArchivedItemInformationAsync(containerName);

            return result!.DataFormatID;
        }

        public async Task<int> GetFormatVersionIdFromStoredBlobAsync(string containerName)
        {
            var result = await GetArchivedItemInformationAsync(containerName);

            return result!.DataFormatVersionID;
        }

        public async Task<string> GetReporteeIdFromStoredBlobAsync(string containerName)
        {
            var result = await GetArchivedItemInformationAsync(containerName);

            return result!.EncryptedReporteeId;
        }

        public async Task<string> GetFormdataAsync(string containerName)
        {
            try
            {
                foreach (var blobItem in _privateBlobStorage.GetBlobContainerItems(containerName))
                {
                    var metadataItem = blobItem.Metadata?.Where(m => (m.Key.Equals(BlobStorageMetadataKeys.Type, StringComparison.OrdinalIgnoreCase)
                        && m.Value.Equals(BlobStorageMetadataTypes.FormData, StringComparison.OrdinalIgnoreCase))).FirstOrDefault();

                    if (metadataItem != null && !metadataItem.Equals(default(KeyValuePair<string, string>)))
                    {
                        var blobContainerClient = _privateBlobStorage.GetBlobContainerClient(containerName);
                        var client = blobContainerClient.GetBlobClient(blobItem.Name);

                        StringBuilder sb = new StringBuilder();
                        if (await client.ExistsAsync())
                        {
                            var response = await client.DownloadAsync();
                            using (var streamReader = new StreamReader(response.Value.Content))
                            {
                                while (!streamReader.EndOfStream)
                                {
                                    sb.Append(await streamReader.ReadLineAsync());
                                }
                            }
                        }
                        return sb.ToString();
                    }
                }
                throw new ArgumentException($"Formdata i container {containerName} finnes ikke i BlobStorage");
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<string> AddBytesToBlobStorageAsync(
            BlobStorageEnum storageEnum, string containerName, string blobName, byte[] fileBytes, string mimeType, IEnumerable<KeyValuePair<string, string>> metadata = null)
        {
            await using var stream = new MemoryStream(fileBytes, false);
            return await AddStreamToBlobStorageAsync(storageEnum, containerName, blobName, stream, mimeType, metadata);
        }

        public async Task<string> AddStreamToBlobStorageAsync(
            BlobStorageEnum storageEnum, string containerName, string blobName, Stream stream, string mimeType, IEnumerable<KeyValuePair<string, string>> metadata = null)
        {
            var storage = GetBlobStorage(storageEnum);
            var client = storage.GetBlobClient(containerName, blobName);
            await storage.CreateContainerIfNotExistsAsync(containerName);

            if (!string.IsNullOrEmpty(mimeType))
            {
                var blobHttpHeader = new BlobHttpHeaders { ContentType = mimeType };
                await client.UploadAsync(stream, blobHttpHeader);
            }
            else
            {
                await client.UploadAsync(stream);
            }

            if (metadata != null)
            {
                await client.SetMetadataAsync(metadata.ToEncodedDictionary());
            }

            return client.Uri.ToString();
        }

        public async Task<string> ParallelUploadStringContentToBlobStorageAsync(
            BlobStorageEnum storageEnum,
            string containerName,
            IEnumerable<(string blobItemName, string blobAsJson)> blobItems,
            IEnumerable<KeyValuePair<string, string>> metadata,
            string mimeType)
        {
            var storage = GetBlobStorage(storageEnum);
            await storage.CreateContainerIfNotExistsAsync(containerName);

            await Parallel.ForEachAsync(blobItems, async (blobItem, ct) =>
            {
                var blobHttpHeader = new BlobHttpHeaders { ContentType = mimeType };
                var client = storage.GetBlobClient(containerName, blobItem.blobItemName);
                using var stream = new MemoryStream(Encoding.UTF8.GetBytes(blobItem.blobAsJson), false);
                await client.UploadAsync(stream, blobHttpHeader, cancellationToken: ct, metadata: metadata.ToEncodedDictionary());
            });

            return storage.GetBlobContainerClient(containerName).Uri.ToString();
        }

        public async Task<string> GetBlobAsStringByMetadataAsync(string containerName, IEnumerable<KeyValuePair<string, string>> metaDataFilter)
        {
            var blobItems = _privateBlobStorage.GetBlobContainerItems(containerName);
            var data = string.Empty;

            var blobItem = blobItems.FirstOrDefault(b => b.Metadata.ContainsAll(metaDataFilter, new MetadataComparer()));

            if (blobItem == null)
            {
                _log.LogDebug("Unable to find any blobs with metadata matching {metadata} in {containerName}", metaDataFilter.ToString(", "), containerName);
                return data;
            }

            var blob = _privateBlobStorage.GetBlobClient(containerName, blobItem.Name);
            var response = await blob.DownloadAsync();

            using var reader = new StreamReader(response.Value.Content);
            data = await reader.ReadToEndAsync();

            return data;
        }

        public async Task<IEnumerable<byte[]>> GetBlobsAsBytesByMetadataAsync(BlobStorageEnum storageEnum, string containerName, IEnumerable<KeyValuePair<string, string>> metaDataFilter)
        {
            BlobStorage storage = GetBlobStorage(storageEnum);
            var blobItems = storage.GetBlobContainerItems(containerName);
            List<byte[]> blobs = new List<byte[]>();
            foreach (var blobItem in blobItems)
            {
                var blobBlock = storage.GetBlobClient(containerName, blobItem.Name);
                //BlobProperties properties = await blobBlock.GetPropertiesAsync();
                //var matchingMetadataElements = properties.Metadata?.Where(m => metaDataFilter.All(f => m.Key.Equals(f.Key, StringComparison.OrdinalIgnoreCase) && m.Value.Equals(f.Value, StringComparison.OrdinalIgnoreCase))).ToList();

                var matchingMetadataElements = blobItem.Metadata?.Where(m => metaDataFilter.All(f => m.Key.Equals(f.Key, StringComparison.OrdinalIgnoreCase) && m.Value.Equals(f.Value, StringComparison.OrdinalIgnoreCase))).ToList();

                foreach (var metadataElement in matchingMetadataElements) // ?? Skal det lastes ned pr metadataelement??
                {
                    var response = await blobBlock.DownloadAsync();
                    var contentLenght = response.Value.ContentLength;
                    using (var binReader = new BinaryReader(response.Value.Content))
                    {
                        var filecontent = binReader.ReadBytes(Convert.ToInt32(contentLenght));
                        blobs.Add(filecontent);
                    }
                }
            }

            return blobs;
        }

        public async Task<byte[]> GetBlobAsBytesByMetadataAsync(BlobStorageEnum storageEnum, string containerName, KeyValuePair<string, string> metaDataFilter)
        {
            BlobStorage storage = GetBlobStorage(storageEnum);
            var blobItems = storage.GetBlobContainerItems(containerName);
            byte[] filecontent = null;

            foreach (var blobItem in blobItems)
            {
                //var blobBlock = storage.GetBlockBlobClient(containerName, blobItem.Name);
                //BlobProperties properties = await blobBlock.GetPropertiesAsync();

                //var matchingMetadataElements = properties.Metadata?.Any(m => m.Key.Equals(metaDataFilter.Key, StringComparison.OrdinalIgnoreCase) && m.Value.Contains(metaDataFilter.Value, StringComparison.OrdinalIgnoreCase));

                var matchingMetadataElements = blobItem.Metadata?.Any(m => m.Key.Equals(metaDataFilter.Key, StringComparison.OrdinalIgnoreCase) && m.Value.Contains(metaDataFilter.Value, StringComparison.OrdinalIgnoreCase));

                if ((bool)matchingMetadataElements)
                {
                    var blobBlock = storage.GetBlobClient(containerName, blobItem.Name);
                    var response = await blobBlock.DownloadAsync();
                    var contentLenght = response.Value.ContentLength;

                    using (var binReader = new BinaryReader(response.Value.Content))
                    {
                        filecontent = binReader.ReadBytes(Convert.ToInt32(contentLenght));
                    }
                }
            }

            return filecontent;
        }

        public async Task<IEnumerable<BlobContent>> GetBlobContentsAsBytesByMetadataAsync(BlobStorageEnum storageEnum, string containerName, IEnumerable<KeyValuePair<string, string>> metaDataFilter)
        {
            var blobContents = new List<BlobContent>();
            BlobStorage storage = GetBlobStorage(storageEnum);
            var blobItems = storage.GetBlobContainerItems(containerName);

            foreach (var blobItem in blobItems.Where(item => item.Metadata.Intersect(metaDataFilter, new MetadataComparer()).Any()))
            {
                var blobBlock = storage.GetBlobClient(containerName, blobItem.Name);
                var blobContent = new BlobContent();
                var response = await blobBlock.DownloadAsync();
                var contentLenght = response.Value.ContentLength;

                blobContent.MimeType = response.Value.ContentType;
                blobContent.FileName = blobBlock.Name;
                blobContent.BlobUrl = blobBlock.Uri.AbsoluteUri;
                blobContent.Metadata = blobItem.Metadata.Select(metadata => new KeyValuePair<string, string>(metadata.Key, metadata.Value)).ToList();

                using (var binReader = new BinaryReader(response.Value.Content))
                {
                    blobContent.Content = binReader.ReadBytes(Convert.ToInt32(contentLenght));
                }

                blobContents.Add(blobContent);
            }

            return blobContents;
        }

        public async Task<BlobContent> GetBlobContentAsBytesByMetadataAsync(BlobStorageEnum storageEnum, string containerName, IEnumerable<KeyValuePair<string, string>> metaDataFilter)
        {
            var blobContent = new BlobContent();
            BlobStorage storage = GetBlobStorage(storageEnum);
            var blobItems = storage.GetBlobContainerItems(containerName);

            foreach (var blobItem in blobItems)
            {
                //var blobBlock = storage.GetBlockBlobClient(containerName, blobItem.Name);
                //BlobProperties properties = await blobBlock.GetPropertiesAsync();

                //var matchingMetadataElements = properties.Metadata?
                //    .Where(m => metaDataFilter
                //        .Any(f => m.Key.Equals(f.Key, StringComparison.OrdinalIgnoreCase) && m.Value.Equals(f.Value, StringComparison.OrdinalIgnoreCase)))
                //    .ToList();

                var matchingMetadataElements = blobItem.Metadata?
                    .Where(metadata => metaDataFilter
                        .Any(filterEntry => metadata.Key.Equals(filterEntry.Key, StringComparison.OrdinalIgnoreCase) && metadata.Value.Equals(filterEntry.Value, StringComparison.OrdinalIgnoreCase)))
                    .ToList();

                if (!matchingMetadataElements?.Any() ?? false)
                    continue;

                var blobBlock = storage.GetBlobClient(containerName, blobItem.Name);
                var response = await blobBlock.DownloadAsync();
                var contentLenght = response.Value.ContentLength;

                using (var binReader = new BinaryReader(response.Value.Content))
                {
                    blobContent.Content = binReader.ReadBytes(Convert.ToInt32(contentLenght));
                }

                blobContent.MimeType = response.Value.ContentType;
                blobContent.FileName = blobBlock.Name;
                blobContent.Metadata = blobItem.Metadata
                    .Select(metadata => new KeyValuePair<string, string>(metadata.Key, metadata.Value))
                    .ToList();
            }

            return blobContent;
        }

        public async Task<IEnumerable<(string attachmentFileName, string attachmentFileUrl, string metadataValue)>> GetBlobUrlsFromPublicStorageByMetadataAsync(
            string containerName, IEnumerable<KeyValuePair<string, string>> metaDataFilter)
        {
            var urls = await GetBlobUrlsFromStorageByMetadataAsync(_publicBlobStorage, containerName, metaDataFilter);

            return urls;
        }

        public async Task<IEnumerable<(string attachmentFileName, string attachmentFileUrl, string attachmentType)>> GetBlobUrlsFromPrivateStorageByMetadataAsync(
            string containerName, IEnumerable<KeyValuePair<string, string>> metaDataFilter)
        {
            var urls = await GetBlobUrlsFromStorageByMetadataAsync(_privateBlobStorage, containerName, metaDataFilter);

            return urls;
        }

        private static Task<IEnumerable<(string attachmentFileName, string attachmentFileUrl, string attachmentType)>> GetBlobUrlsFromStorageByMetadataAsync(
            BlobStorage blobStorage, string containerName, IEnumerable<KeyValuePair<string, string>> metaDataFilter)
        {
            var blobItems = blobStorage.GetBlobContainerItems(containerName);
            var blobUrls = new List<(string attachmentFileName, string attachmentFileUrl, string metadataValue)>();

            foreach (var blobItem in blobItems.Where(b => b.Metadata.Intersect(metaDataFilter, new MetadataComparer()).Any()))
            {
                var blobBlock = blobStorage.GetBlobClient(containerName, blobItem.Name);
                var metadataElement = blobItem.Metadata.Intersect(metaDataFilter, new MetadataComparer()).FirstOrDefault();
                blobUrls.Add((blobItem.Name, blobBlock.Uri.AbsoluteUri, metadataElement.Value));
            }
            var blobUrlsSorted = blobUrls.OrderBy(x => x.metadataValue).Select(x => x);

            return Task.FromResult(blobUrlsSorted);
        }

        public string GetPublicBlobContainerName(string containerName)
        {
            var blobItems = _privateBlobStorage.GetBlobContainerItems(containerName.ToLower());
            var blobUrls = new List<KeyValuePair<string, string>>();
            string publicBlobContainer = "";

            foreach (var blobItem in blobItems)
            {
                List<string> publicBlobContainerList = blobItem.Metadata.Where(x => x.Key.Equals(BlobStorageMetadataKeys.PublicBlobContainerName, StringComparison.OrdinalIgnoreCase)).Select(x => x.Value).ToList();
                if (publicBlobContainerList.Count == 1)
                {
                    publicBlobContainer = publicBlobContainerList[0];
                    break;
                }
            }

            return publicBlobContainer;
        }

        public Task<IEnumerable<(string attachmentType, string fileName, List<KeyValuePair<string, string>> vedleggsopplysninger)>> GetListOfBlobsMetadataValuesByBlobItemTypesAsync(
            BlobStorageEnum storageEnum,
            string containerName,
            IEnumerable<string> blobItemTypes,
            bool onlyReceiptAttachments = false
            )
        {
            BlobStorage storage = GetBlobStorage(storageEnum);
            var listOfAttachments = new List<(string attachmentType, string fileName, List<KeyValuePair<string, string>>)>();
            var blobItems = storage.GetBlobContainerItems(containerName);
            foreach (var blobItem in blobItems)
            {
                var blobIsOfRequestedType = blobItem.Metadata.Any(x => x.Key.Equals(BlobStorageMetadataKeys.Type, StringComparison.OrdinalIgnoreCase) && blobItemTypes.Contains(x.Value));

                if (blobIsOfRequestedType)
                {
                    if (onlyReceiptAttachments)
                    {
                        var excludeFromReceipt = blobItem.Metadata.FirstOrDefault(metadata => metadata.Key.Equals(BlobStorageMetadataKeys.ExcludeInReceipt)).Value;
                        if (excludeFromReceipt.Equals(true.ToString()))
                        {
                            continue;
                        }
                    }
                    var attachmentType = blobItem.Metadata.GetValueOrDefault(BlobStorageMetadataKeys.AttachmentTypeName);
                    var fileName = blobItem.Name;

                    var vedleggsopplysninger = blobItem.Metadata.Vedleggsopplysninger();

                    var attachment = (attachmentType, fileName, vedleggsopplysninger);

                    listOfAttachments.Add(attachment);
                }
            }

            return Task.FromResult<IEnumerable<(string attachmentType, string fileName, List<KeyValuePair<string, string>>)>>(listOfAttachments);
        }

        public async Task<IEnumerable<BlobStreamItem>> GetAllBlobsByMetadataTypeAsync(
            BlobStorageEnum storageEnum, string containerName, IEnumerable<string> blobStorageTypes, IEnumerable<string> mimeTypes = null)
        {
            var storage = GetBlobStorage(storageEnum);
            var blobStreamItems = new List<BlobStreamItem>();
            var blobItems = storage.GetBlobContainerItems(containerName);

            foreach (var blobItem in blobItems)
            {
                var blobClient = storage.GetBlobClient(containerName, blobItem.Name);
                BlobProperties properties = await blobClient.GetPropertiesAsync();

                if (mimeTypes != null && !mimeTypes.Contains(properties.ContentType))
                    continue;

                var blobIsOfRequestedType = blobItem.Metadata != null
                    && blobItem.Metadata.Any(metadata => metadata.Key.Equals(BlobStorageMetadataKeys.Type, StringComparison.OrdinalIgnoreCase) && blobStorageTypes.Contains(metadata.Value));

                if (!blobIsOfRequestedType)
                    continue;

                var content = await blobClient.DownloadAsync();

                var blobStreamItem = new BlobStreamItem()
                {
                    FileName = blobItem.Name,
                    Content = content.Value.Content,
                    MimeType = content.Value.ContentType,
                    BlobUrl = blobClient.Uri.AbsoluteUri,
                    Metadata = blobItem.Metadata.Select(metadata => new KeyValuePair<string, string>(metadata.Key, metadata.Value)).ToList()
                };

                blobStreamItems.Add(blobStreamItem);
            }

            return blobStreamItems;
        }

        public async Task<IEnumerable<BlobStreamItem>> GetAllBlobsByFilterAsync(BlobStorageEnum storageEnum, string containerName, IEnumerable<KeyValuePair<string, string>> metaDataFilter)
        {
            var blobStreamItems = new List<BlobStreamItem>();
            BlobStorage storage = GetBlobStorage(storageEnum);
            var blobs = storage.GetBlobContainerItems(containerName);

            foreach (var blobItem in blobs.Where(b => b.Metadata.Intersect(metaDataFilter, new MetadataComparer()).Any()))
            {
                var blobClient = storage.GetBlobClient(containerName, blobItem.Name);
                var content = await blobClient.DownloadAsync();
                BlobProperties properties = await blobClient.GetPropertiesAsync();

                var blobStreamItem = new BlobStreamItem()
                {
                    FileName = blobItem.Name,
                    Content = content.Value.Content,
                    MimeType = content.Value.ContentType,
                    BlobUrl = blobClient.Uri.AbsoluteUri,
                    FileSize = properties.ContentLength,
                    Metadata = blobItem.Metadata.Select(x => new KeyValuePair<string, string>(x.Key, x.Value)).ToList()
                };

                blobStreamItems.Add(blobStreamItem);
            }

            return blobStreamItems;
        }

        public async Task<IEnumerable<BlobStreamItem>> GetAllBlobsContainingFilterAsync(BlobStorageEnum storageEnum, string containerName, IEnumerable<KeyValuePair<string, string>> metaDataFilter)
        {
            var blobStreamItems = new List<BlobStreamItem>();
            BlobStorage storage = GetBlobStorage(storageEnum);
            var blobs = storage.GetBlobContainerItems(containerName);

            foreach (var blobItem in blobs.Where(b => b.Metadata.ContainsAll(metaDataFilter, new MetadataComparer())))
            {
                var blobClient = storage.GetBlobClient(containerName, blobItem.Name);
                var content = await blobClient.DownloadAsync();
                BlobProperties properties = await blobClient.GetPropertiesAsync();

                var blobStreamItem = new BlobStreamItem()
                {
                    FileName = blobItem.Name,
                    Content = content.Value.Content,
                    MimeType = content.Value.ContentType,
                    BlobUrl = blobClient.Uri.AbsoluteUri,
                    FileSize = properties.ContentLength,
                    Metadata = blobItem.Metadata.Select(x => new KeyValuePair<string, string>(x.Key, x.Value)).ToList()
                };

                blobStreamItems.Add(blobStreamItem);
            }

            return blobStreamItems;
        }

        public IEnumerable<Attachment> GetAttachmentsByMetadata(BlobStorageEnum storageEnum, string containerName, IEnumerable<KeyValuePair<string, string>> metaDataFilter)
        {
            BlobStorage storage = GetBlobStorage(storageEnum);
            var blobItems = storage.GetBlobContainerItems(containerName);
            List<Attachment> attachments = new List<Attachment>();
            foreach (var blobItem in blobItems)
            {
                var blobBlock = storage.GetBlobClient(containerName, blobItem.Name);
                BlobProperties properties = blobBlock.GetPropertiesAsync().GetAwaiter().GetResult();
                var blobIsXML = (properties.ContentType != null && properties.ContentType.ToLower().Equals("application/xml", StringComparison.OrdinalIgnoreCase))
                                    || blobItem.Name.ToLower().Contains(".xml");
                var blobIsJson = (properties.ContentType != null && properties.ContentType.ToLower().Equals("application/json", StringComparison.OrdinalIgnoreCase))
                                    || blobItem.Name.ToLower().Contains(".json");

                var matchingMetadataElements = blobItem.Metadata?.Where(meta => metaDataFilter.ToList()
                                    .Any(filter => filter.Key == meta.Key && filter.Value.Equals(meta.Value, StringComparison.OrdinalIgnoreCase)));

                if (matchingMetadataElements?.Count() > 0)
                {
                    var response = blobBlock.Download();

                    if (blobIsXML)
                    {
                        var attachment = new AttachmentXml();
                        attachment = (AttachmentXml)EnrichTheAttachment(attachment, containerName, blobItem, properties);
                        var data = string.Empty;
                        using (var reader = new StreamReader(response.Value.Content))
                        {
                            data = reader.ReadToEnd();
                            attachment.XmlStringContent = data;
                            attachments.Add(attachment);
                        }
                    }
                    else if (blobIsJson)
                    {
                        var attachment = new AttachmentJson();
                        attachment = (AttachmentJson)EnrichTheAttachment(attachment, containerName, blobItem, properties);
                        var data = string.Empty;
                        using (var reader = new StreamReader(response.Value.Content))
                        {
                            data = reader.ReadToEnd();
                            attachment.JsonStringContent = data;
                            attachments.Add(attachment);
                        }
                    }
                    else
                    {
                        var attachment = new AttachmentBinary();
                        attachment = (AttachmentBinary)EnrichTheAttachment(attachment, containerName, blobItem, properties);

                        var contentLenght = response.Value.ContentLength;
                        using (var binReader = new BinaryReader(response.Value.Content))
                        {
                            var filecontent = binReader.ReadBytes(Convert.ToInt32(contentLenght));
                            attachment.BinaryContent = filecontent;
                            attachments.Add(attachment);
                        }
                    }
                }
            }

            return attachments;
        }

        private BlobStorage GetBlobStorage(BlobStorageEnum storageEnum)
        {
            return storageEnum == BlobStorageEnum.Private ? (BlobStorage)_privateBlobStorage : (BlobStorage)_publicBlobStorage;
        }

        private Attachment EnrichTheAttachment(Attachment attachment, string containerName, BlobContainerItem blobItem, BlobProperties properties)
        {
            attachment.ArchiveReference = containerName.ToUpper();
            attachment.AttachmentTypeName = blobItem.Metadata?.FirstOrDefault(x => x.Key.Equals(BlobStorageMetadataKeys.AttachmentTypeName, StringComparison.OrdinalIgnoreCase)).Value;
            attachment.Filename = blobItem.Name;
            attachment.Name = blobItem.Metadata?.FirstOrDefault(x => x.Key.Equals(BlobStorageMetadataKeys.AttachmentTypeName, StringComparison.OrdinalIgnoreCase)).Value;
            attachment.Type = properties.ContentType;
            attachment.Size = properties.ContentLength;

            return attachment;
        }

        public async Task<T> GetTypeFromBlobStorageAsync<T>(string containerName, string typeValue)
        {
            var retVal = default(T);
            try
            {
                var metadataFilter = new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, typeValue);

                var blob = _privateBlobStorage.GetBlobContainerItems(containerName).FirstOrDefault(b => b.Metadata.Contains(metadataFilter, new MetadataComparer()));

                if (blob != null)
                {
                    StringBuilder sb = new StringBuilder();
                    var blobContainerClient = _privateBlobStorage.GetBlobContainerClient(containerName);
                    var client = blobContainerClient.GetBlobClient(blob.Name);
                    if (await client.ExistsAsync())
                    {
                        var response = await client.DownloadAsync();
                        using (var streamReader = new StreamReader(response.Value.Content))
                        {
                            while (!streamReader.EndOfStream)
                            {
                                sb.Append(await streamReader.ReadLineAsync());
                            }
                        }
                    }
                    retVal = JsonConvert.DeserializeObject<T>(sb.ToString());
                }
            }
            catch (RequestFailedException rfEx)
            {
                _log.LogError(rfEx, "Failed getting {TypeStoredAsBlob} from blob storage {ContainerName}. HttpStatusCode: {HttpStatusCode}", typeof(T), containerName, rfEx.Status);
                throw;
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Failed getting {TypeStoredAsBlob} from blob storage {ContainerName}", typeof(T), containerName);
                throw;
            }

            return retVal;
        }

        public async Task<BlobStreamItem> GetBlobStreamAsync(BlobStorageEnum storageEnum, string containerName, string fileName)
        {
            BlobStorage storage = GetBlobStorage(storageEnum);
            var blobClient = storage.GetBlobClient(containerName, fileName);

            if (!await blobClient.ExistsAsync())
                return null;
            var content = await blobClient.DownloadAsync();

            //For å hente ut dekodet metadata
            var blobContainerItems = storage.GetBlobContainerItems(containerName);
            var blobItem = blobContainerItems.SingleOrDefault(p => p.Name.Equals(fileName, StringComparison.OrdinalIgnoreCase));

            var blob = new BlobStreamItem()
            {
                FileName = blobClient.Name,
                MimeType = content.Value.ContentType,
                BlobUrl = blobClient.Uri.AbsoluteUri,
                Content = content.Value.Content,
                Metadata = blobItem.Metadata.Select(x => new KeyValuePair<string, string>(x.Key, x.Value)).ToList()
            };

            return blob;
        }

        public async Task<BlobContent> GetBlobContentAsync(BlobStorageEnum storageEnum, string containerName, string fileName)
        {
            BlobStorage storage = GetBlobStorage(storageEnum);
            var blobClient = storage.GetBlobClient(containerName, fileName);

            if (!await blobClient.ExistsAsync())
                return null;
            var content = await blobClient.DownloadAsync();

            //For å hente ut dekodet metadata
            var blobContainerItems = storage.GetBlobContainerItems(containerName);
            var blobItem = blobContainerItems.SingleOrDefault(p => p.Name.Equals(fileName, StringComparison.OrdinalIgnoreCase));

            var contentLenght = content.Value.ContentLength;

            byte[] filecontent = null;
            using (var binReader = new BinaryReader(content.Value.Content))
                filecontent = binReader.ReadBytes(Convert.ToInt32(contentLenght));

            var blob = new BlobContent()
            {
                FileName = blobClient.Name,
                MimeType = content.Value.ContentType,
                BlobUrl = blobClient.Uri.AbsoluteUri,
                Content = filecontent,
                Metadata = blobItem.Metadata.Select(x => new KeyValuePair<string, string>(x.Key, x.Value)).ToList()
            };

            return blob;
        }

        public async Task<List<BlobItem>> GetBlobItemsAsync(BlobStorageEnum storageEnum, string containerName)
        {
            BlobStorage storage = GetBlobStorage(storageEnum);

            List<BlobContainerItem> blobItems = null;
            try
            {
                blobItems = storage.GetBlobContainerItems(containerName);
            }
            catch (Azure.RequestFailedException ex)
            {
                if (ex.Status == 404)
                {
                    _log.LogWarning("Blob container {ContainerName} not found", containerName);
                    return new List<BlobItem>();
                }
            }

            var retVal = new List<BlobItem>();
            foreach (var item in blobItems)
            {
                var client = storage.GetBlobClient(containerName, item.Name);
                var props = await client.GetPropertiesAsync();

                retVal.Add(new BlobItem()
                {
                    FileName = item.Name,
                    BlobUrl = client.Uri.AbsoluteUri,
                    FileSize = props.Value.ContentLength,
                    MimeType = props.Value.ContentType,
                    Metadata = item.Metadata.Select(x => new KeyValuePair<string, string>(x.Key, x.Value)).ToList()
                });
            }

            return retVal;
        }

        public async Task<string> GetBlobContentAsStringByBlobItemNameAsync(BlobStorageEnum storageEnum, string containerName, string blobItemName)
        {
            BlobStorage storage = GetBlobStorage(storageEnum);
            var blobClient = storage.GetBlobClient(containerName, blobItemName);
            var response = await blobClient.DownloadAsync();

            using var reader = new StreamReader(response.Value.Content);

            return await reader.ReadToEndAsync();
        }

        public async Task DeleteBlobContainerAsync(BlobStorageEnum storageEnum, string containerName)
        {
            var storage = GetBlobStorage(storageEnum);
            var containerClient = storage.GetBlobContainerClient(containerName);

            await containerClient.DeleteIfExistsAsync();
        }
    }

    public static class BlobMetadata
    {
        public static KeyValuePair<string, string> AttachmentTypeName(string attachmentTypeName)
        { return CreatKeyValuePair(BlobStorageMetadataKeys.AttachmentTypeName, attachmentTypeName); }

        public static KeyValuePair<string, string> BlobType(string blobType)
        { return CreatKeyValuePair(BlobStorageMetadataKeys.Type, blobType); }

        private static KeyValuePair<string, string> CreatKeyValuePair(string key, string value)
        {
            return new KeyValuePair<string, string>(key, value);
        }
    }

    public class MetadataComparer : IEqualityComparer<KeyValuePair<string, string>>
    {
        public bool Equals(KeyValuePair<string, string> x, KeyValuePair<string, string> y)
        {
            if (x.Key.Equals(y.Key, StringComparison.OrdinalIgnoreCase) && (x.Value.Equals(y.Value, StringComparison.OrdinalIgnoreCase)))
                return true;

            return false;
        }

        public int GetHashCode(KeyValuePair<string, string> obj)
        {
            var kv = $"{obj.Key.ToUpper()}-{obj.Value.ToUpper()}";
            return kv.GetHashCode();
        }
    }
}