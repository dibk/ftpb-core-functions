﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtB_Common.Storage
{
    public abstract class BlobStorage
    {
        protected BlobServiceClient _blobServiceClient;
        protected string _privateAzureStorageConnectionString;
        protected string _publicAzureStorageConnectionString;

        public BlobContainerClient GetBlobContainerClient(string containerName)
        {
            return _blobServiceClient.GetBlobContainerClient(containerName);
        }

        public System.Uri GetBlobUri()
        {
            return _blobServiceClient.Uri;
        }

        public abstract Task CreateContainerIfNotExistsAsync(string containerName);

        public BlobClient GetBlobClient(string containerName, string blobName)
        {
            var c = _blobServiceClient.GetBlobContainerClient(containerName);
            return c.GetBlobClient(blobName);
        }

        public List<BlobContainerItem> GetBlobContainerItems(string containerName)
        {
            var containerClient = GetBlobContainerClient(containerName);
            var blobs = containerClient.GetBlobs(traits: BlobTraits.Metadata);

            return blobs.Select(b => new BlobContainerItem(b.Name, b.Metadata)).ToList();
        }
    }
}