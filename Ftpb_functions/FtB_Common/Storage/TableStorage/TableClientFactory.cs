﻿using Azure.Data.Tables;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace FtB_Common.Storage.TableStorage
{
    public class TableClientFactory : TableClientFactoryBase
    {
        public TableClientFactory(ILogger<TableClientFactory> log, IAzureClientFactory<TableServiceClient> clientFactory) : base(log, clientFactory)
        { }
    }
}