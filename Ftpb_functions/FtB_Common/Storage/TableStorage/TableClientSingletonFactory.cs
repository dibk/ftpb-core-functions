﻿using Azure.Data.Tables;
using FtB_Common.BusinessModels.TableStorage.Entities;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace FtB_Common.Storage.TableStorage
{

    public class TableClientSingletonFactory : TableClientFactoryBase
    {
        private Dictionary<string, TableClient> _tableClients;

        public TableClientSingletonFactory(ILogger<TableClientSingletonFactory> log, IAzureClientFactory<TableServiceClient> clientFactory) : base(log, clientFactory)
        {
            _tableClients = new Dictionary<string, TableClient>
            {
                { typeof(DistributionReceiverLogEntity).Name, CreateTableClient<DistributionReceiverLogEntity>() },
                { typeof(DistributionReceiverEntity).Name, CreateTableClient<DistributionReceiverEntity>() },
                { typeof(DistributionSubmittalEntity).Name, CreateTableClient<DistributionSubmittalEntity>() },
                { typeof(NotificationSenderEntity).Name, CreateTableClient<NotificationSenderEntity>() },
                { typeof(NotificationSenderLogEntity).Name, CreateTableClient<NotificationSenderLogEntity>() }
            };
        }

        public override TableClient GetTableClient<T>()
        {
            try
            {
                return _tableClients.GetValueOrDefault(typeof(T).Name);
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Failed to create table client");
                throw;
            }
        }
    }
}