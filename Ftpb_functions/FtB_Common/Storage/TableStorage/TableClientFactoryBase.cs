﻿using Azure.Data.Tables;
using FtB_Common.BusinessModels.TableStorage.Entities;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Logging;
using System;

namespace FtB_Common.Storage.TableStorage
{
    public class TableClientFactoryBase : ITableClientFactory
    {
        private TableServiceClient _tableServiceClient;
        protected readonly ILogger _log;

        public TableClientFactoryBase(ILogger log, IAzureClientFactory<TableServiceClient> clientFactory)
        {
            _log = log;
            _tableServiceClient = clientFactory.CreateClient("PrivateTableStorage");
        }

        protected TableClient CreateTableClient<T>() where T : class, ITableEntity, new()
        {
            try
            {
                string tableNameFromType = GetTableName<T>();
                return _tableServiceClient.GetTableClient(tableNameFromType);
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Failed to create table client");
                throw;
            }
        }

        public virtual TableClient GetTableClient<T>() where T : class, ITableEntity, new()
        {
            try
            {
                return CreateTableClient<T>();
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Failed to create table client");
                throw;
            }
        }

        private string GetTableName<T>()
        {
            if (typeof(T) == typeof(DistributionReceiverLogEntity))
            {
                return "ftbDistributionReceiverLog";
            }
            else if (typeof(T) == typeof(DistributionReceiverEntity))
            {
                return "ftbDistributionReceiver";
            }
            else if (typeof(T) == typeof(DistributionSubmittalEntity))
            {
                return "ftbDistributionSubmittal";
            }
            else if (typeof(T) == typeof(NotificationSenderEntity))
            {
                return "ftbNotificationSender";
            }
            else if (typeof(T) == typeof(NotificationSenderLogEntity))
            {
                return "ftbNotificationSenderLog";
            }
            else if (typeof(T) == typeof(AltinnEventEntity))
            {
                return "ftbAltinnEventLog";
            }

            throw new Exception("Illegal table storage name");
        }
    }
}