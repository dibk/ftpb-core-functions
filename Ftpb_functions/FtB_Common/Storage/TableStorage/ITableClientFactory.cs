﻿using Azure.Data.Tables;

namespace FtB_Common.Storage.TableStorage
{
    public interface ITableClientFactory
    {
        TableClient GetTableClient<T>() where T : class, ITableEntity, new();
    }
}