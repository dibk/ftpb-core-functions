using Azure;
using Azure.Data.Tables;
using FtB_Common.Exceptions;
using FtB_Common.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FtB_Common.Storage.TableStorage
{
    public class TableStorageService : ITableStorage
    {
        private readonly ILogger<TableStorageService> _log;
        private readonly ITableClientFactory _tableClientFactory;

        public TableStorageService(ILogger<TableStorageService> log, ITableClientFactory tableStorageClientFactory)
        {
            _log = log;
            _tableClientFactory = tableStorageClientFactory;
        }

        private TableClient GetTableClient<T>() where T : class, ITableEntity, new()
        {
            try
            {
                return _tableClientFactory.GetTableClient<T>();
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Failed to create table client");
                throw;
            }
        }

        public async Task<T> GetTableEntityAsync<T>(string partitionKey, string rowKey) where T : class, ITableEntity, new()
        {
            try
            {
                TableClient cloudTable = GetTableClient<T>();

                var response = await cloudTable.GetEntityAsync<T>(partitionKey, rowKey);

                return response.Value;
            }
            catch (RequestFailedException ex)
            {
                if (ex.Status == 404)
                {
                    _log.LogDebug(ex, "Failed to get entity record");
                    return null;
                }
                
                _log.LogError(ex, "Failed to get entity record");
                throw;
            }
        }

        public async Task<IEnumerable<T>> GetTableEntitiesAsync<T>(string partitionKey) where T : class, ITableEntity, new()
        {
            try
            {
                TableClient cloudTable = GetTableClient<T>();
                AsyncPageable<T> response = cloudTable.QueryAsync<T>(e => e.PartitionKey == partitionKey);

                return await response.ToListAsync();
            }
            catch (RequestFailedException ex)
            {
                if (ex.Status == 404)
                {
                    _log.LogDebug(ex, "Failed to get entity records");
                    return null;
                }

                _log.LogError(ex, "Failed to get entity records");
                throw;
            }
        }

        public IEnumerable<T> GetTableEntities<T>(IEnumerable<KeyValuePair<string, string>> filter) where T : class, ITableEntity, new()
        {
            try
            {
                TableClient cloudTable = GetTableClient<T>();

                if (filter.Count() > 0)
                {
                    var filterBuilder = new StringBuilder();
                    filterBuilder.Append($"{filter.ToList()[0].Key} eq '{filter.ToList()[0].Value}'");
                    for (int i = 1; i < filter.Count(); i++)
                    {
                        filterBuilder.Append($" and {filter.ToList()[i].Key} eq '{filter.ToList()[i].Value}'");
                    }

                    var lst = cloudTable.Query<T>(filterBuilder.ToString()).ToList();

                    return lst;
                }
                throw new ArgumentOutOfRangeException("No valid arguments for filter.");
            }
            catch (RequestFailedException ex)
            {
                var filterstrings = filter.Select(s => $"{s.Key}={s.Value}").ToArray();
                
                if (ex.Status == 404)
                {
                    _log.LogDebug(ex, $"Failed to get entity records by filter: {string.Join(", ", filterstrings)}");
                    return null;
                }

                _log.LogError(ex, $"Failed to get entity records by filter: {string.Join(", ", filterstrings)}");
                throw;
            }
        }

        public void InsertEntityRecord<T>(ITableEntity entity) where T : class, ITableEntity, new()
        {
            try
            {
                TableClient cloudTable = GetTableClient<T>();
                cloudTable.CreateIfNotExists();

                var response = cloudTable.UpsertEntity(entity);
            }
            catch (RequestFailedException rex)
            {
                _log.LogError(rex, "Failed to insert entity record");
                throw;
            }
        }

        public async Task InsertEntityRecordAsync<T>(ITableEntity entity) where T : class, ITableEntity, new()
        {
            try
            {
                TableClient cloudTable = GetTableClient<T>();
                await cloudTable.CreateIfNotExistsAsync();

                var response = await cloudTable.UpsertEntityAsync(entity);
            }
            catch (RequestFailedException rex)
            {
                _log.LogError(rex, "Failed to insert entity record");
                throw;
            }
        }

        public async Task InsertEntityRecordsAsync<T>(IEnumerable<ITableEntity> entities) where T : class, ITableEntity, new()
        {
            try
            {
                var batchResults = new List<Response<IReadOnlyList<Response>>>();

                TableClient cloudTable = GetTableClient<T>();
                await cloudTable.CreateIfNotExistsAsync();

                var batches = entities.Batch(100);

                foreach (var batch in batches)
                {
                    var addEntitiesBatch = new List<TableTransactionAction>();
                    addEntitiesBatch.AddRange(batch.Select(e => new TableTransactionAction(TableTransactionActionType.UpsertMerge, e)));

                    Response<IReadOnlyList<Response>> response = await cloudTable.SubmitTransactionAsync(addEntitiesBatch).ConfigureAwait(false);

                    batchResults.Add(response);
                }
            }
            catch (RequestFailedException rex)
            {
                _log.LogError(rex, "Failed to insert entity record");
                throw;
            }
        }

        public async Task<T> UpdateEntityRecordAsync<T>(ITableEntity entity) where T : class, ITableEntity, new()
        {
            try
            {
                TableClient cloudTable = GetTableClient<T>();
                var response = await cloudTable.UpsertEntityAsync(entity);

                return await GetTableEntityAsync<T>(entity.PartitionKey, entity.RowKey);
            }
            catch (RequestFailedException rex)
            {
                _log.LogError(rex, "Failed to update entity record");
                throw;
            }
        }

        public async Task<bool> UpdateEntitiesAsync<T>(IEnumerable<T> entities) where T : class, ITableEntity, new()
        {
            try
            {
                TableClient cloudTable = GetTableClient<T>();

                var result = new List<Response>();
                var batches = entities.Batch(100);
                foreach (var batch in batches)
                {
                    var addEntitiesBatch = new List<TableTransactionAction>();
                    addEntitiesBatch.AddRange(batch.Select(e => new TableTransactionAction(TableTransactionActionType.UpsertReplace, e)));

                    Response<IReadOnlyList<Response>> response = await cloudTable.SubmitTransactionAsync(addEntitiesBatch).ConfigureAwait(false);

                    result.AddRange(response.Value);
                }

                return result.All(r => r.Status == 204);
            }
            catch (RequestFailedException ex)
            {
                _log.LogError(ex, "Failed to update entity records");

                if (ex.Status == 412)
                {
                    throw new TableStorageConcurrentException("Optimistic concurrency violation – entity has changed since it was retrieved.", 412);
                }
                else
                    throw;
            }
        }
    }
}