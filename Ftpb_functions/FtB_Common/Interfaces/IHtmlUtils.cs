﻿namespace FtB_Common.Interfaces
{
    public interface IHtmlUtils
    {
        string GetHtmlFromTemplate(string htmlTemplatePath);
    }
}
