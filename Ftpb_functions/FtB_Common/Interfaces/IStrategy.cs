﻿namespace FtB_Common.Interfaces
{
    public interface IStrategy<T,U>
    {
        T Exceute(U queueItem);
    }
}
