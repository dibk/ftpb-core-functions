﻿using Azure.Data.Tables;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FtB_Common.Interfaces
{
    public interface ITableStorage
    {
        Task<T> GetTableEntityAsync<T>(string partitionKey, string rowKey) where T : class, ITableEntity, new();

        Task<IEnumerable<T>> GetTableEntitiesAsync<T>(string partitionKey) where T : class, ITableEntity, new();

        IEnumerable<T> GetTableEntities<T>(IEnumerable<KeyValuePair<string, string>> filter) where T : class, ITableEntity, new();

        void InsertEntityRecord<T>(ITableEntity tableEntity) where T : class, ITableEntity, new();

        Task InsertEntityRecordAsync<T>(ITableEntity tableEntity) where T : class, ITableEntity, new();

        Task InsertEntityRecordsAsync<T>(IEnumerable<ITableEntity> entities) where T : class, ITableEntity, new();

        Task<T> UpdateEntityRecordAsync<T>(ITableEntity entity) where T : class, ITableEntity, new();

        Task<bool> UpdateEntitiesAsync<T>(IEnumerable<T> entities) where T : class, ITableEntity, new();
    }
}