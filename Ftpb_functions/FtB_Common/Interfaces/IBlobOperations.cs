﻿using Altinn.Common.Models;
using FtB_Common.Enums;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace FtB_Common.Storage
{
    public interface IBlobOperations
    {
        Task<string> GetFormatIdFromStoredBlobAsync(string containerName);
        Task<int> GetFormatVersionIdFromStoredBlobAsync(string containerName);
        Task<string> GetServiceCodeFromStoredBlobAsync(string containerName);
        Task<string> GetReporteeIdFromStoredBlobAsync(string containerName);
        Task<string> GetFormdataAsync(string archiveReference);
        Task<string> AddStreamToBlobStorageAsync(BlobStorageEnum storageEnum, string containerName, string blobName, Stream stream, string mimeType, IEnumerable<KeyValuePair<string, string>> metadata = null);
        Task<string> ParallelUploadStringContentToBlobStorageAsync(BlobStorageEnum storageEnum, string containerName, IEnumerable<(string blobItemName, string blobAsJson)> blobItems, IEnumerable<KeyValuePair<string, string>> metadata, string mimeType);
        Task<string> AddBytesToBlobStorageAsync(BlobStorageEnum storageEnum, string containerName, string blobName, byte[] fileBytes, string mimeType, IEnumerable<KeyValuePair<string, string>> metadata = null);
        Task<string> GetBlobAsStringByMetadataAsync(string containerName, IEnumerable<KeyValuePair<string, string>> metaData);
        Task<byte[]> GetBlobAsBytesByMetadataAsync(BlobStorageEnum storageEnum, string containerName, KeyValuePair<string, string> metaDataFilter);
        Task<IEnumerable<byte[]>> GetBlobsAsBytesByMetadataAsync(BlobStorageEnum storageEnum, string containerName, IEnumerable<KeyValuePair<string, string>> metaDataFilter);
        IEnumerable<Attachment> GetAttachmentsByMetadata(BlobStorageEnum storageEnum, string containerName, IEnumerable<KeyValuePair<string, string>> metaDataFilter);
        Task<IEnumerable<(string attachmentType, string fileName, List<KeyValuePair<string, string>> vedleggsopplysninger)>> GetListOfBlobsMetadataValuesByBlobItemTypesAsync(BlobStorageEnum storageEnum, string containerName, IEnumerable<string> blobStorageTypes, bool onlyReceiptAttachments = false);
        Task<IEnumerable<(string attachmentFileName, string attachmentFileUrl, string metadataValue)>> GetBlobUrlsFromPublicStorageByMetadataAsync(string containerName, IEnumerable<KeyValuePair<string, string>> metaDataFilter);
        Task<IEnumerable<(string attachmentFileName, string attachmentFileUrl, string attachmentType)>> GetBlobUrlsFromPrivateStorageByMetadataAsync(string containerName, IEnumerable<KeyValuePair<string, string>> metaDataFilter);
        string GetPublicBlobContainerName(string containerName);
        Task<bool> AcquireContainerLeaseAsync(string containerName, int seconds);
        Task<bool> ReleaseContainerLeaseAsync(string containerName);
        string GetBlobUri(BlobStorageEnum blobStorageEnum);
        Task<BlobContent> GetBlobContentAsBytesByMetadataAsync(BlobStorageEnum storageEnum, string containerName, IEnumerable<KeyValuePair<string, string>> metaDataFilter);
        Task<IEnumerable<BlobContent>> GetBlobContentsAsBytesByMetadataAsync(BlobStorageEnum storageEnum, string containerName, IEnumerable<KeyValuePair<string, string>> metaDataFilter);
        Task<BlobStreamItem> GetBlobStreamAsync(BlobStorageEnum storageEnum, string containerName, string fileName);
        Task<BlobContent> GetBlobContentAsync(BlobStorageEnum storageEnum, string containerName, string fileName);
        Task<List<BlobItem>> GetBlobItemsAsync(BlobStorageEnum storageEnum, string containerName);
        Task<string> GetBlobContentAsStringByBlobItemNameAsync(BlobStorageEnum storageEnum, string containerName, string blobItemName);
        Task<IEnumerable<BlobStreamItem>> GetAllBlobsByMetadataTypeAsync(BlobStorageEnum storageEnum, string containerName, IEnumerable<string> blobItemTypes, IEnumerable<string> mimeTypes = null);
        Task<IEnumerable<BlobStreamItem>> GetAllBlobsByFilterAsync(BlobStorageEnum storageEnum, string containerName, IEnumerable<KeyValuePair<string, string>> metaDataFilter);
        Task<IEnumerable<BlobStreamItem>> GetAllBlobsContainingFilterAsync(BlobStorageEnum storageEnum, string containerName, IEnumerable<KeyValuePair<string, string>> metaDataFilter);
        Task<T> GetTypeFromBlobStorageAsync<T>(string containerName, string typeValue);
        Task DeleteBlobContainerAsync(BlobStorageEnum storageEnum, string containerName);
    }
}