﻿using FtB_Common.FormDataRepositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FtB_Common.Interfaces
{
    public interface IFormDataRepo
    {
        Task<string> GetFormData(string archiveReference);
        List<AttachmentInfo> GetAttachments(string archiveReference);
        Task<List<SubformInfo>> GetSubforms(string archiveReference);
        Task<string> GetSubformContent(string archiveReference, string subformFileName);
        Task AddBytesAsBlob(string containerName, string fileName, byte[] fileBytes, IEnumerable<KeyValuePair<string, string>> metadata = null);
    }
}