namespace Dibk.Ftpb.Integration.SvarUt.Builders
{
    public static class TemplateTags
    {
        public static string Gårdsnummer = "{gnr}";
        public static string Bruksnummer = "{bnr}";
        public static string Festenummer = "{fnr}";
        public static string Seksjonsnummer = "{snr}";
        public static string AltinnArkivReferanse = "{altinnreferanse}";
        public static string Adresselinje1 = "{adr1}";
        public static string SøknadSkjemaNavn = "{skjema}";
        public static string Tiltak = "{tiltak}";
    }
}