﻿using System.Collections.Generic;
using System.Text;

namespace Dibk.Ftpb.Integration.SvarUt.Builders
{
    public class ForsendelseDataBygg
    {
        public ForsendelseDataBygg(string archiveReference)
        {
            ArchiveReference = archiveReference;
        }

        public string ArchiveReference { get; }
        public string Adresselinje1 { get; set; }
        public string Adresselinje2 { get; set; }
        public string Adresselinje3 { get; set; }
        public string AvgivendeSystem { get; set; }
        public string Bolignummer { get; set; }
        public string Bruksnummer { get; set; }
        public string Bygningsnummer { get; set; }
        public string Festenummer { get; set; }
        public string Gårdsnummer { get; set; }
        public string KommunensSaksnummerÅr { get; set; }
        public string KommunensSaksnummerSekvensnummer { get; set; }
        public string Kommunenummer { get; set; }
        public string Landkode { get; set; }
        public string Postnr { get; set; }
        public string Poststed { get; set; }
        public string Seksjonsnummer { get; set; }
        public string SøknadSkjemaNavn { get; set; }
        public string TiltakType { get; set; } = string.Empty;
        public string ForsendelseType { get; set; } = "Byggesøknad";
        public string Konteringskode { get; set; }

        private string _forsendelseTittel;

        public string ForsendelseTittel
        {
            get
            {
                if (string.IsNullOrEmpty(_forsendelseTittel))
                {
                    return MapFromTagPattern(fallbackTitlePattern);
                }
                else return _forsendelseTittel;
            }
            set { _forsendelseTittel = value; }
        }

        private static string fallbackTitlePattern = $"{TemplateTags.Adresselinje1} {TemplateTags.SøknadSkjemaNavn} {TemplateTags.Gårdsnummer}/{TemplateTags.Bruksnummer}";

        public string MapFromTagPattern(string tagPattern)
        {
            StringBuilder sb = new StringBuilder(tagPattern);

            // Replace the tags with actual data
            sb.Replace(TemplateTags.Gårdsnummer, Gårdsnummer);
            sb.Replace(TemplateTags.Bruksnummer, Bruksnummer);
            sb.Replace(TemplateTags.Festenummer, Festenummer);
            sb.Replace(TemplateTags.Seksjonsnummer, Seksjonsnummer);
            sb.Replace(TemplateTags.Adresselinje1, Adresselinje1);
            sb.Replace(TemplateTags.SøknadSkjemaNavn, SøknadSkjemaNavn);
            sb.Replace(TemplateTags.AltinnArkivReferanse, ArchiveReference);

            return sb.ToString();
        }

        public static List<(int sorting, string name, string value)> ClassificationConfig = new List<(int sorting, string name, string value)>()
        {
            (1, "GBNR", "{gnr}/{bnr}/{fnr}/{snr}"),
            (2, "SID", "{altinnreferanse}"),
        };
    }
}