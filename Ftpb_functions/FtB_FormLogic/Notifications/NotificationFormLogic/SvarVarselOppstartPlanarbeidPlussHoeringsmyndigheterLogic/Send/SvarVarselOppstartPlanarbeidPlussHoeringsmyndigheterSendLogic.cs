using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Constants;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidLogic;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using no.kxml.skjema.dibk.planuttalelse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Dibk.Ftpb.Common.Constants.DataTypes;

namespace FtB_FormLogic
{
    //This class is not in use, as we collect the answers and send them as a collection through FuncSvarVarselOmOppstartAvPlanarbeidReporter.cs.
    //We might have a use for this class in VORPA+H as we wish to send some answers directly and ignore the collectioning. 
    [FormDataFormat(DataFormatId = DataFormatIDs.UttalelseVarselPlanoppstart, DataFormatVersion = VorpahSvarReceiverHelper.DataFormatVersion, ProcessingContext = FormLogicProcessingContext.Send)]
    public class SvarVarselOppstartPlanarbeidPlussHoeringsmyndigheterSendLogic : NotificationSendLogic<PlanuttalelseType>
    {
        private readonly IHtmlUtils _htmlUtils;
        private readonly IDecryption _decryptor;

        public SvarVarselOppstartPlanarbeidPlussHoeringsmyndigheterSendLogic(IFormDataRepo repo,
                                                                             ILogger<SvarVarselOppstartPlanarbeidPlussHoeringsmyndigheterSendLogic> log,
                                                                             IBlobOperations blobOperations,
                                                                             INotificationAdapter notificationAdapter,
                                                                             IDbUnitOfWork dbUnitOfWork,
                                                                             IHtmlUtils htmlUtils,
                                                                             IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                                                             IDecryptionFactory decryptionFactory,
                                                                             DistributionReceiverRepo distributionReceiverRepo,
                                                                             DistributionReceiverLogRepo distributionReceiverLogRepo)
            : base(repo,
                  blobOperations,
                  log,
                  notificationAdapter,
                  dbUnitOfWork,
                  htmlUtils,
                  decryptionFactory,
                  fileDownloadHttpClient,
                  distributionReceiverRepo,
                  distributionReceiverLogRepo)
        {
            _htmlUtils = htmlUtils;
            _decryptor = decryptionFactory.GetDecryptor();
        }

        public override Task<AltinnReceiver> GetReceiver()
        {
            //var reportee = await _blobOperations.GetReporteeIdFromStoredBlobAsync(this.ArchiveReference);
            var receiver = VorpahSvarReceiverHelper.GetDistributionReceiverCandidates(_decryptor, FormData);

            return Task.FromResult(receiver);
        }

        protected override MessageDataType GetSubmitterReceiptMessage(string archiveReference)
        {
            try
            {
                string planNavn = base.FormData.plannavn == null ? "" : base.FormData.plannavn;
                string planIdent = base.FormData.planid == null ? "" : base.FormData.planid;
                string htmlBody = _htmlUtils.GetHtmlFromTemplate("FtB_FormLogic.Notifications.NotificationFormLogic.SvarVarselOppstartPlanarbeidPlussHoeringsmyndigheterLogic.Send.SvarVarselOppstartPlanarbeidPlussHoeringsmyndigheterReportMessageBody.html");

                htmlBody = htmlBody.Replace("<arkivReferanse/>", archiveReference.ToUpper());
                htmlBody = htmlBody.Replace("<planNavn />", planNavn);
                htmlBody = htmlBody.Replace("<planIdent />", planIdent);

                var message = new MessageDataType()
                {
                    MessageTitle = $"Uttalelse - Oppstart av planarbeid, ({planIdent})",
                    MessageBody = htmlBody
                };

                return message;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Error occurred while getting submitter receipt message");

                throw;
            }
        }

        protected override (string Filename, string Name) GetFileNameForMainForm()
        {
            var Filename = "Uttalelse.pdf";
            var Name = "Uttalelse";
            return (Filename, Name);
        }

        protected override (string Filename, string Name) GetFileNameForAttachment()
        {
            var Filename = "Uttalelsevedlegg.pdf";
            var Name = "Uttalelsevedlegg";
            return (Filename, Name);
        }

        protected override async Task<AttachmentBinary> GetUttalelsebrev(string containerName)
        {
            var metadataList = new List<KeyValuePair<string, string>>
            {
                new(BlobStorageMetadataKeys.AttachmentTypeName, Planuttalelse)
            };

            var uttalelsebrevAsByteArray = await BlobOperations.GetBlobsAsBytesByMetadataAsync(BlobStorageEnum.Private, containerName, metadataList);
            
            return new AttachmentBinary
            {
                BinaryContent = uttalelsebrevAsByteArray.First(),
                Filename = GetFileNameForMainForm().Filename,
                Name = GetFileNameForMainForm().Name,
                ArchiveReference = ArchiveReference
            };
        }

        protected override async Task<AttachmentBinary> GetUttalelsevedlegg(string containerName)
        {
            var metadataList = new List<KeyValuePair<string, string>>();
            metadataList.Add(new KeyValuePair<string, string>(BlobStorageMetadataKeys.AttachmentTypeName, "UttalelseVedlegg"));

            var uttalelsebrevAsByteArray = await BlobOperations.GetBlobsAsBytesByMetadataAsync(BlobStorageEnum.Private, containerName, metadataList);
            
            if (uttalelsebrevAsByteArray == null || !uttalelsebrevAsByteArray.Any()) 
                return null;

            return new AttachmentBinary
            {
                BinaryContent = uttalelsebrevAsByteArray?.First(),
                Filename = GetFileNameForAttachment().Filename,
                Name = GetFileNameForAttachment().Name,
                ArchiveReference = ArchiveReference
            };
        }
    }
}
