using Altinn3.Adapters;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Constants;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidLogic;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using no.kxml.skjema.dibk.planuttalelse;

namespace FtB_FormLogic
{
    [FormDataFormat(DataFormatId = DataFormatIDs.UttalelseVarselPlanoppstart, DataFormatVersion = VorpahSvarReceiverHelper.DataFormatVersion, ProcessingContext = FormLogicProcessingContext.Report)]
    public class SvarVarselOppstartPlanarbeidPlussHoeringsmyndigheterReportLogic : NotificationReportLogic<PlanuttalelseType>
    {
        protected override bool CompleteAltinn3Instance => true;

        public SvarVarselOppstartPlanarbeidPlussHoeringsmyndigheterReportLogic(
            IBlobOperations blobOperations,
            IAltinnInstanceCompleter altinnInstanceCompleter,
            DistributionSubmittalRepo distributionSubmittalRepo,
            IFormDataRepo repo,
            ILogger<SvarVarselOppstartPlanarbeidPlussHoeringsmyndigheterReportLogic> log,
            IDbUnitOfWork dbUnitOfWork,
            IFileDownloadStatusHttpClient fileDownloadHttpClient,
            DistributionReceiverRepo distributionReceiverRepo,
            DistributionReceiverLogRepo distributionReceiverLogRepo
        ) : base(
            blobOperations,
            altinnInstanceCompleter,
            distributionSubmittalRepo,
            repo,
            log,
            dbUnitOfWork,
            fileDownloadHttpClient,
            distributionReceiverRepo,
            distributionReceiverLogRepo
        )
        {
        }
    }
}
