using Dibk.Ftpb.Api.Models;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Constants;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_Common.Utils;
using FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidLogic;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Ftb_Repositories.LogContstants;
using Microsoft.Extensions.Logging;
using no.kxml.skjema.dibk.planuttalelse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    [FormDataFormat(DataFormatId = DataFormatIDs.UttalelseVarselPlanoppstart, DataFormatVersion = VorpahSvarReceiverHelper.DataFormatVersion, ProcessingContext = FormLogicProcessingContext.Prepare)]
    public class SvarVarselOppstartPlanarbeidPlussHoeringsmyndigheterPrepareLogic : NotificationPrepareLogic<PlanuttalelseType>
    {
        private readonly string _displayFilename = "SvarVarselOppstartplanarbeid";
        private readonly string _formName = "Svar på varsel om oppstart av planarbeid";
        private readonly NotificationSenderRepo _notificationSenderRepo;
        private readonly NotificationSenderLogRepo _notificationSenderLogRepo;

        public SvarVarselOppstartPlanarbeidPlussHoeringsmyndigheterPrepareLogic(IFormDataRepo repo,
                                                                                IBlobOperations blobOperations,
                                                                                ILogger<VarselOppstartPlanarbeidPlussHoeringsmyndigheterPrepareLogic> log,
                                                                                IDbUnitOfWork dbUnitOfWork,
                                                                                IDecryptionFactory decryptionFactory,
                                                                                IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                                                                DistributionReceiverRepo distributionReceiverRepo,
                                                                                DistributionReceiverLogRepo distributionReceiverLogRepo,
                                                                                NotificationSenderRepo notificationSenderRepo,
                                                                                NotificationSenderLogRepo notificationSenderLogRepo) :
            base(repo,
                blobOperations,
                log,
                dbUnitOfWork,
                decryptionFactory,
                fileDownloadHttpClient,
                distributionReceiverRepo,
                distributionReceiverLogRepo)
        {
            this._notificationSenderRepo = notificationSenderRepo;
            this._notificationSenderLogRepo = notificationSenderLogRepo;
        }

        public override async Task<IEnumerable<SendQueueItem>> ExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {
            DbUnitOfWork.LogEntries.AddInfo($"Starter prosessering av {_formName}");
            var formmetadata = new FormMetadataApiModel()
            {
                ArchiveReference = submittalQueueItem.ArchiveReference, //TODO: Inn med meir data. FormType, MunicipalityCode, Application, ServiceCode, ApplicantName, Adress osv ++ ....
                Status = FormMetadataStatus.IKø,
                FormType = DataFormatIDs.UttalelseVarselPlanoppstart,
                Application = FormData.fraSluttbrukersystem,
                ArchiveTimestamp = DateTime.Now,
                SenderSystem = "Uttalelse til varsel om oppstart av reguleringsplanarbeid",
                ServiceCode = "UTTALELSE-VORPAH",
                ServiceEditionCode = 1
            };
            DbUnitOfWork.FormMetadata.Create(formmetadata);

            await base.ExecuteAsync(submittalQueueItem);

            var initialArchiveReference = await GetInitialArchiveReferenceAsync(FormData.hovedinnsendingsnummer);

            DbUnitOfWork.LogEntries.AddInfo($"Gjør PDF og skjema tilgjengelig");
            await MakePDFReplyAndAttachmentsPublicAccessible(submittalQueueItem.ArchiveReference, initialArchiveReference, FormData.beroertPart.navn);
            await CreateNotificationSenderDatabaseStatus(submittalQueueItem.ArchiveReference, initialArchiveReference);

            DbUnitOfWork.LogEntries.AddInfo($"Ferdig med prosessering");
            var fmd = await DbUnitOfWork.FormMetadata.Get(submittalQueueItem.ArchiveReference);
            fmd.Status = FormMetadataStatus.Ok;
            DbUnitOfWork.FormMetadata.Update(fmd);
            await DbUnitOfWork.SaveFormMetadata();
            await DbUnitOfWork.SaveLogEntries();
            //At the end of processing this method, processing is completed for this schema (FormLogic),
            //thus returning null (meaning put no element on the queue)
            return null;
        }

        private async Task MakePDFReplyAndAttachmentsPublicAccessible(string beroertPartArchiveReference, string initialArchiveReference, string partyName)
        {
            var publicBlobContainer = BlobOperations.GetPublicBlobContainerName(initialArchiveReference);
            var logmessage = new StringBuilder();
            //PDF
            var uttalelseVarselPlanoppstartResult = await GetFilesFromPrivateBlobStorageAsync(beroertPartArchiveReference, new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.MainForm));
            var PDFdoc = uttalelseVarselPlanoppstartResult.ToList()[0];
            await CopyPDFToPublicBlobStorage(PDFdoc.Content, partyName, publicBlobContainer, beroertPartArchiveReference);
            logmessage.Append($"PDF og vedlegg lagret til storage: {PDFdoc.FileName}");

            //Attachments
            var attachments = await GetFilesFromPrivateBlobStorageAsync(beroertPartArchiveReference, new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.SubmittalAttachment));
            foreach (var attachment in attachments)
            {
                attachment.FileName = $"Vedlegg_{FormData.beroertPart.navn.RemoveInvalidChars()}_{attachment.FileName}";
                await AddFileToPublicBlobStorage(attachment, publicBlobContainer, beroertPartArchiveReference);
                logmessage.Append($", {attachment.FileName}");
            }

            DbUnitOfWork.LogEntries.AddInfo(logmessage.ToString());
            Logger.LogInformation("Successfully copied PDF for archiveReference {ArchiveReference} and party {BerortPart}", beroertPartArchiveReference, partyName);
        }

        public override Task SetSenderAsync()
        {
            Enum.TryParse(EnumExtentions.GetValueFromDescription<ActorTypeEnum>(FormData.beroertPart.partstype.kodeverdi).ToString(), out ActorTypeEnum senderType);
            string id = (senderType.Equals(ActorTypeEnum.Privatperson) ? FormData.beroertPart.foedselsnummer : FormData.beroertPart.organisasjonsnummer);
            Sender = new Actor() { Id = id, Type = senderType };

            return Task.CompletedTask;
        }

        /// <summary>
        /// Setter mottaker av uttalelsen ved å bruke innsender av planforslaget. Dette vil enten være plankonsulent eller forslagsstiller.
        /// Om noe skulle være feil her, vil det forsøkes å bruke plankonsulent som mottaker, deretter forslagsstiller.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public override async Task SetReceiversAsync()
        {
            var initialArchiveReference = await GetInitialArchiveReferenceAsync(FormData.hovedinnsendingsnummer);
            var encryptedReporteeId = await BlobOperations.GetReporteeIdFromStoredBlobAsync(initialArchiveReference);
            Receiver receiver = null;

            var candidates = GetReceivers(FormData.forslagsstiller, FormData.plankonsulent);
            Logger.LogDebug($"Receiver candidates found {candidates?.Count}");

            receiver = candidates.FirstOrDefault(r => !string.IsNullOrEmpty(r.Id) && r.Id.Equals(DecryptionFactory.GetDecryptor().DecryptText(encryptedReporteeId)));

            if (receiver == null)
            {
                Logger.LogWarning("No receivers found in candidates by comparing reportee with partTypes. Using default receiver");

                if (IsValidAsReceiver(FormData.plankonsulent))
                {
                    receiver = GetReceiver(FormData.plankonsulent);
                    Logger.LogWarning("Defaults to using plankonsulent as receiver");
                }
                else
                {
                    Logger.LogInformation("No valid plankonsulent found in form data. Trying to use forslagsstiller as receiver");
                    if (IsValidAsReceiver(FormData.forslagsstiller))
                    {
                        receiver = GetReceiver(FormData.forslagsstiller);
                        Logger.LogWarning("Defaults to using forslagsstiller as receiver");
                    }
                    else
                    {
                        Logger.LogError("No valid receiver found in form data");
                        throw new Exception("No valid receiver found in form data. Unable to continue processing");
                    }
                }
            }
            else
                Logger.LogInformation("Receiver found in candidates by comparing reportee with partTypes");

            var receivers = new List<Receiver> { receiver };
            base.SetReceivers(receivers);
        }

        private bool IsValidAsReceiver(PartType partType)
        {
            if (partType == null)
                return false;

            if (partType.partstype == null)
                return false;

            if (string.IsNullOrEmpty(partType.navn))
                return false;

            Enum.TryParse(EnumExtentions.GetValueFromDescription<ActorTypeEnum>(partType.partstype.kodeverdi).ToString(), out ActorTypeEnum receiverType);

            if (receiverType == ActorTypeEnum.Privatperson)
            {
                return !string.IsNullOrEmpty(partType.foedselsnummer);
            }
            else
            {
                return !string.IsNullOrEmpty(partType.organisasjonsnummer);
            }
        }

        private List<Receiver> GetReceivers(params PartType[] partTyper)
        {
            var retval = new List<Receiver>();

            foreach (var part in partTyper)
            {
                if (part != null)
                    retval.Add(GetReceiver(part));
            }

            return retval;
        }

        private Receiver GetReceiver(PartType partType)
        {
            Enum.TryParse(EnumExtentions.GetValueFromDescription<ActorTypeEnum>(partType.partstype.kodeverdi).ToString(), out ActorTypeEnum receiverType);
            var id = (receiverType.Equals(ActorTypeEnum.Privatperson) ? DecryptionFactory.GetDecryptor().DecryptText(partType.foedselsnummer) : partType.organisasjonsnummer);

            Logger.LogDebug("Mapping part {Name} of type {ActorType} to receiver", partType.navn, receiverType);

            return new Receiver { Id = id, Type = receiverType, Name = partType.navn };
        }

        private async Task CreateNotificationSenderDatabaseStatus(string archiveReference, string initialArchiveReference)
        {
            try
            {
                initialArchiveReference = initialArchiveReference ?? archiveReference;
                var senderEntity = new NotificationSenderDto(initialArchiveReference.ToLower(), ArchiveReference.ToLower(), Sender.Id, NotificationSenderProcessStageEnum.Created, DateTime.Now);
                senderEntity.InitialExternalSystemReference = FormData.hovedinnsendingsnummer;
                senderEntity.PlanId = FormData.planid;
                senderEntity.PlanNavn = FormData.plannavn;
                senderEntity.Reply = FormData.beroertPart.kommentar;
                senderEntity.SenderName = FormData.beroertPart.navn;
                senderEntity.SenderPhone = FormData.beroertPart.telefon;
                senderEntity.SenderEmail = FormData.beroertPart.epost;
                senderEntity.ReceiverId = Receivers[0].Id;

                await _notificationSenderRepo.AddAsync(senderEntity);

                string rowKey = $"{DateTime.Now.ToString("yyyyMMddHHmmssffff")}";
                var senderLogEntity = new NotificationSenderLogDto(ArchiveReference, rowKey, Sender.Id, NotificationSenderStatusLogEnum.Created);
                await _notificationSenderLogRepo.AddAsync(senderLogEntity);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Error creating notification records in tablestorage");
                throw;
            }
        }

        protected override Guid GetDistributionFormIdFromForm()
        {
            if (Guid.TryParse(FormData.hovedinnsendingsnummer, out var newGuid))
                return newGuid;
            throw new ArgumentOutOfRangeException($"Illegal distribution id. Could not parse {FormData.hovedinnsendingsnummer}");
        }

        public override async Task AddRepliedFilesToFileDownloadStatus(string archiveReference)
        {
            await base.AddRepliedFilesToFileDownloadStatus(archiveReference,
                                                           _displayFilename,
                                                           _formName,
                                                           FileTypesForDownloadEnum.Planuttalelse,
                                                           FileTypesForDownloadEnum.Planuttalelse,
                                                           FileTypesForDownloadEnum.UttalelseVedlegg);
        }

        protected override string GetPublicPDFAttachmentTypeName()
        {
            return "SvarVarselOppstartPlanarbeid";
        }
    }
}