﻿namespace FtB_FormLogic
{
    public class SvarPaaVarselOmOppstartAvPlanarbeidSenderModel
    {
        public string SenderName { get; set; }
        public string SenderPhone { get; set; }
        public string SenderEmail { get; set; }
        public string Reply { get; set; }
        public string Objection { get; set; }
        public string SendersArchiveReference { get; set; }
        public string InitialExternalSystemReference { get; set; }
    }
}
