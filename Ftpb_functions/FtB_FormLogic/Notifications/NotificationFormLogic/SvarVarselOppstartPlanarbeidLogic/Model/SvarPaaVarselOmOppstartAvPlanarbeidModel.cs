﻿using Altinn.Common.Models;
using System;
using System.Collections.Generic;

namespace FtB_FormLogic
{
    public class SvarPaaVarselOmOppstartAvPlanarbeidModel
    {
        public AltinnReceiverType AltinnReceiverType { get; set; }
        public string ReceiverId { get; set; }
        public string PlanId { get; set; }
        public string PlanNavn { get; set; }
        public string InitialArchiveReference { get; set; }
        public DateTime FristForInnspill { get; set; }
        public List<SvarPaaVarselOmOppstartAvPlanarbeidSenderModel> Senders { get; set; }
        public List<AttachmentBinary> Attachments { get; set; }
    }
}
