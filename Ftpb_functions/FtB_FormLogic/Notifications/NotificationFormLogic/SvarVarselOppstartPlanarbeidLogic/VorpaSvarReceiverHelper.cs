﻿using FtB_Common.BusinessModels;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using System;
using System.Collections.Generic;

namespace FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidLogic
{
    public class VorpaSvarReceiverHelper
    {
        public static List<Receiver> GetReceiverCandidates(IDecryption decryptor, no.kxml.skjema.dibk.nabovarselsvarPlan.SvarPaaNabovarselPlanType svarNabovarselPlan)
        {
            var retval = new List<Receiver>();
            if (!string.IsNullOrEmpty(svarNabovarselPlan.forslagsstiller.organisasjonsnummer))
                retval.Add(new Receiver { Id = svarNabovarselPlan.forslagsstiller.organisasjonsnummer, Type = GetActorType(svarNabovarselPlan.forslagsstiller.partstype.kodeverdi), Name = svarNabovarselPlan.forslagsstiller.navn });

            if (!string.IsNullOrEmpty(svarNabovarselPlan.forslagsstiller.foedselsnummer))
                retval.Add(new Receiver { Id = decryptor.DecryptText(svarNabovarselPlan.forslagsstiller.foedselsnummer), Type = ActorTypeEnum.Privatperson, Name = svarNabovarselPlan.forslagsstiller.navn });

            if (!string.IsNullOrEmpty(svarNabovarselPlan.plankonsulent?.organisasjonsnummer))
                retval.Add(new Receiver { Id = svarNabovarselPlan.plankonsulent.organisasjonsnummer, Type = GetActorType(svarNabovarselPlan.plankonsulent.partstype.kodeverdi), Name = svarNabovarselPlan.plankonsulent.navn });

            if (!string.IsNullOrEmpty(svarNabovarselPlan.plankonsulent?.foedselsnummer))
                retval.Add(new Receiver { Id = decryptor.DecryptText(svarNabovarselPlan.plankonsulent.foedselsnummer), Type = ActorTypeEnum.Privatperson, Name = svarNabovarselPlan.plankonsulent.navn });

            return retval;
        }

        private static ActorTypeEnum GetActorType(string partsTypeKodeVerdi)
        {
            Enum.TryParse(EnumExtentions.GetValueFromDescription<ActorTypeEnum>(partsTypeKodeVerdi).ToString(), out ActorTypeEnum receiverType);
            return receiverType;
        }
    }
}
