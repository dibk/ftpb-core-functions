using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_Common.Utils;
using FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidLogic;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    [FormDataFormat(DataFormatId = "6326", DataFormatVersion = "44843", ProcessingContext = FormLogicProcessingContext.Prepare)]
    public class SvarVarselOppstartPlanarbeidPrepareLogic : NotificationPrepareLogic<no.kxml.skjema.dibk.nabovarselsvarPlan.SvarPaaNabovarselPlanType>
    {
        private readonly string _displayFilename = "SvarNabovarselPlan";

        // Bør endres til 'Svar på varsel om oppstart planarbeid', men kontrakten med integrerende system blir dermed brutt.
        // Må dermed være enighet og støtte i integrerende system for denne verdien
        private readonly string _formName = "VarselOppstartPlanarbeid";
        private readonly NotificationSenderRepo _notificationSenderRepo;
        private readonly NotificationSenderLogRepo _notificationSenderLogRepo;

        public SvarVarselOppstartPlanarbeidPrepareLogic(IFormDataRepo repo,
                                                        IBlobOperations blobOperations,
                                                        ILogger<VarselOppstartPlanarbeidPrepareLogic> log,
                                                        IDbUnitOfWork dbUnitOfWork,
                                                        IDecryptionFactory decryptionFactory,
                                                        IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                                        DistributionReceiverRepo distributionReceiverRepo,
                                                        DistributionReceiverLogRepo distributionReceiverLogRepo,
                                                        NotificationSenderRepo notificationSenderRepo,
                                                        NotificationSenderLogRepo notificationSenderLogRepo) :
            base(repo,
                blobOperations,
                log,
                dbUnitOfWork,
                decryptionFactory,
                fileDownloadHttpClient,
                distributionReceiverRepo,
                distributionReceiverLogRepo)
        {
            _notificationSenderRepo = notificationSenderRepo;
            _notificationSenderLogRepo = notificationSenderLogRepo;
        }

        public override async Task<IEnumerable<SendQueueItem>> ExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {
            await base.ExecuteAsync(submittalQueueItem);

            var initialArchiveReference = await GetInitialArchiveReferenceAsync(FormData.hovedinnsendingsnummer);

            await MakePDFReplyAndAttachmentsPublicAccessible(submittalQueueItem.ArchiveReference, initialArchiveReference, FormData.beroertPart.navn);
            await CreateNotificationSenderDatabaseStatus(submittalQueueItem.ArchiveReference);

            //At the end of processing this method, processing is completed for this schema (FormLogic),
            //thus returning null (meaning put no element on the queue)
            return null;
        }

        private async Task MakePDFReplyAndAttachmentsPublicAccessible(string neighboursArchiveReference, string initialArchiveReference, string partyName)
        {
            var publicBlobContainer = BlobOperations.GetPublicBlobContainerName(initialArchiveReference);

            //PDF
            var svarNabovaraselPlanResult = await GetFilesFromPrivateBlobStorageAsync(neighboursArchiveReference, new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.MainForm));
            var PDFdoc = svarNabovaraselPlanResult.ToList()[0];

            await CopyPDFToPublicBlobStorage(PDFdoc.Content, partyName, publicBlobContainer, neighboursArchiveReference);

            //Attachments
            var attachments = await GetFilesFromPrivateBlobStorageAsync(neighboursArchiveReference, new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.SubmittalAttachment));
            foreach (var attachment in attachments)
            {
                attachment.FileName = $"Vedlegg_{FormData.beroertPart.navn.RemoveInvalidChars()}_{attachment.FileName}";
                await AddFileToPublicBlobStorage(attachment, publicBlobContainer, neighboursArchiveReference);
            }

            Logger.LogInformation("Successfully copied PDF for archiveReference {ArchiveReference} and party {BerortPart}", neighboursArchiveReference, partyName);
        }

        public override Task SetSenderAsync()
        {
            Enum.TryParse(EnumExtentions.GetValueFromDescription<ActorTypeEnum>(FormData.beroertPart.partstype.kodeverdi).ToString(), out ActorTypeEnum senderType);
            string id = (senderType.Equals(ActorTypeEnum.Privatperson) ? FormData.beroertPart.foedselsnummer : FormData.beroertPart.organisasjonsnummer);
            Sender = new Actor() { Id = id, Type = senderType };

            return Task.CompletedTask;
        }

        public override async Task SetReceiversAsync()
        {
            var initialArchiveReference = await GetInitialArchiveReferenceAsync(FormData.hovedinnsendingsnummer);
            var encryptedReporteeId = await BlobOperations.GetReporteeIdFromStoredBlobAsync(initialArchiveReference);
            Receiver receiver = null;

            if (!string.IsNullOrEmpty(encryptedReporteeId))
            {
                receiver = VorpaSvarReceiverHelper.GetReceiverCandidates(DecryptionFactory.GetDecryptor(), FormData).FirstOrDefault(r => r.Id.Equals(DecryptionFactory.GetDecryptor().DecryptText(encryptedReporteeId)));
            }

            if (receiver == null)
            {
                Enum.TryParse(EnumExtentions.GetValueFromDescription<ActorTypeEnum>(FormData.forslagsstiller.partstype.kodeverdi).ToString(), out ActorTypeEnum receiverType);
                var id = (receiverType.Equals(ActorTypeEnum.Privatperson) ? FormData.forslagsstiller.foedselsnummer : FormData.forslagsstiller.organisasjonsnummer);
                receiver = new Receiver { Id = id, Type = receiverType, Name = FormData.forslagsstiller.navn };
            }

            var receivers = new List<Receiver> { receiver };
            base.SetReceivers(receivers);
        }

        private async Task CreateNotificationSenderDatabaseStatus(string archiveReference)
        {
            try
            {
                var initialArchiveReference = await GetInitialArchiveReferenceAsync(FormData.hovedinnsendingsnummer);

                var senderEntity = new NotificationSenderDto(initialArchiveReference.ToLower(), ArchiveReference.ToLower(), Sender.Id, NotificationSenderProcessStageEnum.Created, DateTime.Now)
                {
                    InitialExternalSystemReference = FormData.hovedinnsendingsnummer,
                    PlanId = FormData.planid,
                    PlanNavn = FormData.planNavn,
                    SenderName = FormData.beroertPart.navn,
                    SenderPhone = FormData.beroertPart.telefon,
                    SenderEmail = FormData.beroertPart.epost,
                    ReceiverId = Receivers[0].Id
                };

                if (string.IsNullOrEmpty(FormData.beroertPart.kommentar) || FormData.beroertPart.kommentar.Length < 5000)
                    senderEntity.Reply = FormData.beroertPart.kommentar;
                else
                    senderEntity.Reply = $"Uttalelsen er for lang for forhåndsvisning her. Se vedlagt uttalelse fra {FormData.beroertPart.navn}";

                await _notificationSenderRepo.AddAsync(senderEntity);

                string rowKey = $"{DateTime.Now.ToString("yyyyMMddHHmmssffff")}";
                var senderLogEntity = new NotificationSenderLogDto(ArchiveReference, rowKey, Sender.Id, NotificationSenderStatusLogEnum.Created);
                await _notificationSenderLogRepo.AddAsync(senderLogEntity);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Error creating submittal record");
                throw;
            }
        }

        protected override Guid GetDistributionFormIdFromForm()
        {
            if (Guid.TryParse(FormData.hovedinnsendingsnummer, out var newGuid))
                return newGuid;
            throw new ArgumentOutOfRangeException($"Illegal distribution id. Could not parse {FormData.hovedinnsendingsnummer}");
        }

        public override async Task AddRepliedFilesToFileDownloadStatus(string archiveReference)
        {
            await base.AddRepliedFilesToFileDownloadStatus(
                archiveReference,
                _displayFilename,
                _formName,
                FileTypesForDownloadEnum.MaskinlesbarXml,
                FileTypesForDownloadEnum.Nabovarsel,
                FileTypesForDownloadEnum.Nabomerknader
                );
        }

        protected override string GetPublicPDFAttachmentTypeName()
        {
            return "SvarNabovarselPlan";
        }
    }
}