﻿using FtB_Common.BusinessModels;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using System;
using System.Collections.Generic;
using Altinn.Common.Models;
using no.kxml.skjema.dibk.uttalelseOffentligEttersyn;

namespace FtB_FormLogic.Distributions.DistributionFormLogic.HoeringOgOffentligEttersynLogic
{
    public class HoffeSvarReceiverHelper
    {
        public const string DataFormatVersion = "1";

        public static List<Actor> GetReceiverCandidates(IDecryption decryptor, UttalelseOffentligEttersynType planuttalelse)
        {
            var retval = new List<Actor>();
            if (!string.IsNullOrEmpty(planuttalelse.forslagsstiller.organisasjonsnummer))
                retval.Add(new Actor() { Id = planuttalelse.forslagsstiller.organisasjonsnummer, Type = GetActorType(planuttalelse.forslagsstiller.partstype.kodeverdi), Name = planuttalelse.forslagsstiller.navn });

            if (!string.IsNullOrEmpty(planuttalelse.forslagsstiller.foedselsnummer))
                retval.Add(new Actor() { Id = decryptor.DecryptText(planuttalelse.forslagsstiller.foedselsnummer), Type = ActorTypeEnum.Privatperson, Name = planuttalelse.forslagsstiller.navn });

            if (!string.IsNullOrEmpty(planuttalelse.plankonsulent?.organisasjonsnummer))
                retval.Add(new Actor() { Id = planuttalelse.plankonsulent.organisasjonsnummer, Type = GetActorType(planuttalelse.plankonsulent.partstype.kodeverdi), Name = planuttalelse.plankonsulent.navn });

            if (!string.IsNullOrEmpty(planuttalelse.plankonsulent?.foedselsnummer))
                retval.Add(new Actor() { Id = decryptor.DecryptText(planuttalelse.plankonsulent.foedselsnummer), Type = ActorTypeEnum.Privatperson, Name = planuttalelse.plankonsulent.navn });

            return retval;
        }

        private static ActorTypeEnum GetActorType(string partsTypeKodeVerdi)
        {
            Enum.TryParse(EnumExtentions.GetValueFromDescription<ActorTypeEnum>(partsTypeKodeVerdi).ToString(), out ActorTypeEnum receiverType);
            return receiverType;
        }

        public static AltinnReceiver GetDistributionReceiverCandidates(IDecryption decryptor, UttalelseOffentligEttersynType planvarsel)
        {
            if (planvarsel.forslagsstiller.partstype.kodeverdi.Equals(ActorTypeEnum.Privatperson.ToString()))
            {
                return new AltinnReceiver() { Id = decryptor.DecryptText(planvarsel.forslagsstiller.foedselsnummer), Type = AltinnReceiverType.Privatperson};
            }
            if (planvarsel.forslagsstiller.partstype.kodeverdi.Equals(ActorTypeEnum.Foretak.ToString()))
            {
                return new AltinnReceiver() { Id = planvarsel.forslagsstiller.organisasjonsnummer, Type = AltinnReceiverType.Foretak };
            }
            if (planvarsel.forslagsstiller.partstype.kodeverdi.Equals(ActorTypeEnum.Organisasjon.ToString()))
            {
                return new AltinnReceiver() { Id = planvarsel.forslagsstiller.organisasjonsnummer, Type = AltinnReceiverType.Foretak };
            }
            if (planvarsel.forslagsstiller.partstype.kodeverdi.Equals(ActorTypeEnum.OffentligMyndighet.ToString()))
            {
                return new AltinnReceiver() { Id = planvarsel.forslagsstiller.organisasjonsnummer, Type = AltinnReceiverType.Foretak };
            }
            if (planvarsel.forslagsstiller.partstype.kodeverdi.Equals(ActorTypeEnum.Plankonsulent.ToString()))
            {
                return new AltinnReceiver() { Id = decryptor.DecryptText(planvarsel.forslagsstiller.foedselsnummer), Type = AltinnReceiverType.Privatperson };
            }
            return null;
        }
    }
}
