using Altinn3.Adapters;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Constants;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_FormLogic.Distributions.DistributionFormLogic.HoeringOgOffentligEttersynLogic;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using no.kxml.skjema.dibk.planuttalelse;

namespace FtB_FormLogic
{
    [FormDataFormat(DataFormatId = DataFormatIDs.UttalelseHoeringOgOffentligEttersyn, DataFormatVersion = HoffeSvarReceiverHelper.DataFormatVersion, ProcessingContext = FormLogicProcessingContext.Report)]
    public class SvarHoeringOgOffentligEttersynReportLogic : NotificationReportLogic<PlanuttalelseType>
    {
        protected override bool CompleteAltinn3Instance => true;

        public SvarHoeringOgOffentligEttersynReportLogic(
            IBlobOperations blobOperations,
            IAltinnInstanceCompleter altinnInstanceCompleter,
            DistributionSubmittalRepo distributionSubmittalRepo,
            IFormDataRepo repo,
            ILogger<SvarHoeringOgOffentligEttersynReportLogic> log,
            IDbUnitOfWork dbUnitOfWork,
            IFileDownloadStatusHttpClient fileDownloadHttpClient,
            DistributionReceiverRepo distributionReceiverRepo,
            DistributionReceiverLogRepo distributionReceiverLogRepo
        ) : base(
            blobOperations,
            altinnInstanceCompleter,
            distributionSubmittalRepo,
            repo,
            log,
            dbUnitOfWork,
            fileDownloadHttpClient,
            distributionReceiverRepo,
            distributionReceiverLogRepo
        )
        {
        }
    }
}
