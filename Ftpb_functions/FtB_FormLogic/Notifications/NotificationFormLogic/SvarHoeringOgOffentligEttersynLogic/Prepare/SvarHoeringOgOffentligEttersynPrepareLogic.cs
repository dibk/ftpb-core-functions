using Dibk.Ftpb.Api.Models;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Constants;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_Common.Utils;
using FtB_FormLogic.Distributions.DistributionFormLogic.HoeringOgOffentligEttersynLogic;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Ftb_Repositories.LogContstants;
using Microsoft.Extensions.Logging;
using no.kxml.skjema.dibk.uttalelseOffentligEttersyn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    [FormDataFormat(DataFormatId = DataFormatIDs.UttalelseHoeringOgOffentligEttersyn, DataFormatVersion = HoffeSvarReceiverHelper.DataFormatVersion, ProcessingContext = FormLogicProcessingContext.Prepare)]
    public class SvarHoeringOgOffentligEttersynPrepareLogic : NotificationPrepareLogic<UttalelseOffentligEttersynType>
    {
        private readonly NotificationSenderRepo _notificationSenderRepo;
        private readonly NotificationSenderLogRepo _notificationSenderLogRepo;

        public SvarHoeringOgOffentligEttersynPrepareLogic(IFormDataRepo repo,
                                                          IBlobOperations blobOperations,
                                                          ILogger<SvarHoeringOgOffentligEttersynPrepareLogic> log,
                                                          IDbUnitOfWork dbUnitOfWork,
                                                          IDecryptionFactory decryptionFactory,
                                                          IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                                          DistributionReceiverRepo distributionReceiverRepo,
                                                          DistributionReceiverLogRepo distributionReceiverLogRepo,
                                                          NotificationSenderRepo notificationSenderRepo,
                                                          NotificationSenderLogRepo notificationSenderLogRepo) :
            base(repo,
                blobOperations,
                log,
                dbUnitOfWork,
                decryptionFactory,
                fileDownloadHttpClient,
                distributionReceiverRepo,
                distributionReceiverLogRepo)
        {
            _notificationSenderRepo = notificationSenderRepo;
            _notificationSenderLogRepo = notificationSenderLogRepo;
        }

        public override async Task<IEnumerable<SendQueueItem>> ExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {
            var formmetadata = new FormMetadataApiModel()
            {
                ArchiveReference = submittalQueueItem.ArchiveReference, //TODO: Inn med meir data. MunicipalityCode, ServiceCode, ApplicantName, Adress osv ++ ....
                Status = FormMetadataStatus.IKø,
                FormType = DataFormatIDs.UttalelseHoeringOgOffentligEttersyn,
                Application = FormData.fraSluttbrukersystem,
                ArchiveTimestamp = DateTime.Now,
                SenderSystem = "Høring og offentlig ettersyn Altinn Applikasjon",
                ServiceCode = "UTTALELSE-HOFFE",
                ServiceEditionCode = 1
            };
            DbUnitOfWork.FormMetadata.Create(formmetadata);

            await base.ExecuteAsync(submittalQueueItem);

            var initialArchiveReference = await GetInitialArchiveReferenceAsync(FormData.hovedinnsendingsnummer);

            await MakePDFReplyAndAttachmentsPublicAccessible(submittalQueueItem.ArchiveReference, initialArchiveReference, FormData.beroertPart.navn);
            await CreateNotificationSenderDatabaseStatus(submittalQueueItem.ArchiveReference);

            var fmd = await DbUnitOfWork.FormMetadata.Get(submittalQueueItem.ArchiveReference);
            fmd.Status = FormMetadataStatus.Ok;
            DbUnitOfWork.FormMetadata.Update(fmd);
            await DbUnitOfWork.SaveFormMetadata();
            //At the end of processing this method, processing is completed for this schema (FormLogic),
            //thus returning null (meaning put no element on the queue)
            return null;
        }

        private async Task MakePDFReplyAndAttachmentsPublicAccessible(string neighboursArchiveReference, string initialArchiveReference, string partyName)
        {
            var publicBlobContainer = BlobOperations.GetPublicBlobContainerName(initialArchiveReference);

            //PDF
            var svarNabovaraselPlanResult = await GetFilesFromPrivateBlobStorageAsync(neighboursArchiveReference, new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.MainForm));
            var PDFdoc = svarNabovaraselPlanResult.ToList()[0];
            await CopyPDFToPublicBlobStorage(PDFdoc.Content, partyName, publicBlobContainer, neighboursArchiveReference);

            //Attachments
            var attachments = await GetFilesFromPrivateBlobStorageAsync(neighboursArchiveReference, new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.SubmittalAttachment));
            foreach (var attachment in attachments)
            {
                attachment.FileName = $"Vedlegg_{FormData.beroertPart.navn.RemoveInvalidChars()}_{attachment.FileName}";
                await AddFileToPublicBlobStorage(attachment, publicBlobContainer, neighboursArchiveReference);
            }

            Logger.LogInformation("Successfully copied PDF for archiveReference {ArchiveReference} and party {BerortPart}", neighboursArchiveReference, partyName);
        }

        public override Task SetSenderAsync()
        {
            Enum.TryParse(EnumExtentions.GetValueFromDescription<ActorTypeEnum>(FormData.beroertPart.partstype.kodeverdi).ToString(), out ActorTypeEnum senderType);
            string id = (senderType.Equals(ActorTypeEnum.Privatperson) ? FormData.beroertPart.foedselsnummer : FormData.beroertPart.organisasjonsnummer);
            Sender = new Actor() { Id = id, Type = senderType };

            return Task.CompletedTask;
        }

        public override Task SetReceiversAsync()
        {
            Receiver receiver;

            Enum.TryParse(EnumExtentions.GetValueFromDescription<ActorTypeEnum>(FormData.forslagsstiller.partstype.kodeverdi).ToString(), out ActorTypeEnum receiverType);
            var id = (receiverType.Equals(ActorTypeEnum.Privatperson) ? FormData.forslagsstiller.foedselsnummer : FormData.forslagsstiller.organisasjonsnummer);
            receiver = new Receiver { Id = id, Type = receiverType, Name = FormData.forslagsstiller.navn };

            var receivers = new List<Receiver> { receiver };
            base.SetReceivers(receivers);

            return Task.CompletedTask;
        }

        private async Task CreateNotificationSenderDatabaseStatus(string archiveReference)
        {
            try
            {
                // ofh for høringsbrev...
                var initialArchiveReference = await GetInitialArchiveReferenceAsync(FormData.hovedinnsendingsnummer) ?? archiveReference;

                var senderEntity = new NotificationSenderDto(initialArchiveReference.ToLower(), ArchiveReference.ToLower(), Sender.Id, NotificationSenderProcessStageEnum.Created, DateTime.Now);
                senderEntity.InitialExternalSystemReference = FormData.hovedinnsendingsnummer;
                senderEntity.PlanId = FormData.planid;
                senderEntity.PlanNavn = FormData.planNavn;
                senderEntity.Reply = FormData.beroertPart.uttalelse;
                senderEntity.SenderName = FormData.beroertPart.navn;
                senderEntity.SenderPhone = FormData.beroertPart.telefon;
                senderEntity.SenderEmail = FormData.beroertPart.epost;
                senderEntity.ReceiverId = Receivers[0].Id;
                senderEntity.Objection = FormData.beroertPart.kommentarInnsigelse;

                await _notificationSenderRepo.AddAsync(senderEntity);

                string rowKey = $"{DateTime.Now.ToString("yyyyMMddHHmmssffff")}";
                var senderLogEntity = new NotificationSenderLogDto(ArchiveReference, rowKey, Sender.Id, NotificationSenderStatusLogEnum.Created);
                await _notificationSenderLogRepo.AddAsync(senderLogEntity);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Error creating submittal record");
                throw;
            }
        }

        protected override Guid GetDistributionFormIdFromForm()
        {
            if (Guid.TryParse(FormData.hovedinnsendingsnummer, out var newGuid))
                return newGuid;
            throw new ArgumentOutOfRangeException($"Illegal distribution id. Could not parse {FormData.hovedinnsendingsnummer}");
        }

        public override async Task AddRepliedFilesToFileDownloadStatus(string archiveReference)
        {
            await base.AddRepliedFilesToFileDownloadStatus(archiveReference,
                                                           "SvarHoeringOgOffentligEttersyn",
                                                           "VarselHoeringOgOffentligEttersyn",
                                                           FileTypesForDownloadEnum.MaskinlesbarXmlHoeringOgOffentligEttersyn,
                                                           FileTypesForDownloadEnum.SkjemaPdfHoeringOgOffentligEttersyn,
                                                           FileTypesForDownloadEnum.SvarPaaHoeringOgOffentligEttersynVedlegg);
        }

        protected override string GetPublicPDFAttachmentTypeName()
        {
            return "SvarHoeringOgOffentligEttersyn";
        }
    }
}