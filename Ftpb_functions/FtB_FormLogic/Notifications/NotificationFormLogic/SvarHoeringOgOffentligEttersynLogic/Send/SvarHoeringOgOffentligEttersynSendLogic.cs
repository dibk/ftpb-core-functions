﻿using FtB_Common.Encryption;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;
using Altinn.Common.Models;
using Altinn.Common.Interfaces;
using System.Collections.Generic;
using FtB_Common.Enums;
using Ftb_Repositories;
using FtB_FormLogic.Distributions.DistributionFormLogic.HoeringOgOffentligEttersynLogic;
using no.kxml.skjema.dibk.uttalelseOffentligEttersyn;
using FtB_Common.Constants;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.Storage.Metadata;
using FtB_Common.BusinessModels.TableStorage.Repos;

namespace FtB_FormLogic
{
    [FormDataFormat(DataFormatId = DataFormatIDs.UttalelseHoeringOgOffentligEttersyn, DataFormatVersion = HoffeSvarReceiverHelper.DataFormatVersion, ProcessingContext = FormLogicProcessingContext.Send)]

    public class SvarHoeringOgOffentligEttersynSendLogic : NotificationSendLogic<UttalelseOffentligEttersynType>
    {
        private readonly IHtmlUtils _htmlUtils;
        private readonly IDecryption _decryptor;

        public SvarHoeringOgOffentligEttersynSendLogic(IFormDataRepo repo,
                                                       ILogger<SvarHoeringOgOffentligEttersynSendLogic> log,
                                                       IBlobOperations blobOperations,
                                                       INotificationAdapter notificationAdapter,
                                                       IDbUnitOfWork dbUnitOfWork,
                                                       IHtmlUtils htmlUtils,
                                                       HoeringOgOffentligEttersynFileDownloadStatusClient fileDownloadHttpClient,
                                                       IDecryptionFactory decryptionFactory,
                                                       DistributionReceiverRepo distributionReceiverRepo,
                                                       DistributionReceiverLogRepo distributionReceiverLogRepo)
            : base(repo,
                  blobOperations,
                  log,
                  notificationAdapter,
                  dbUnitOfWork,
                  htmlUtils,
                  decryptionFactory,
                  fileDownloadHttpClient,
                  distributionReceiverRepo,
                  distributionReceiverLogRepo)
        {
            _htmlUtils = htmlUtils;
            _decryptor = decryptionFactory.GetDecryptor();
        }

        public override Task<AltinnReceiver> GetReceiver()
        {
            //var reportee = await _blobOperations.GetReporteeIdFromStoredBlobAsync(this.ArchiveReference);
            var receiver = HoffeSvarReceiverHelper.GetDistributionReceiverCandidates(_decryptor, FormData);

            return Task.FromResult(receiver);
        }

        protected override MessageDataType GetSubmitterReceiptMessage(string archiveReference)
        {
            try
            {
                string planNavn = base.FormData.planNavn == null ? "" : base.FormData.planNavn;
                string planIdent = base.FormData.planid == null ? "" : base.FormData.planid;
                string htmlBody = _htmlUtils.GetHtmlFromTemplate("FtB_FormLogic.Notifications.NotificationFormLogic.SvarHoeringOgOffentligEttersynLogic.Send.SvarHoeringOgOffentligEttersynLogicReportMessageBody.html");

                htmlBody = htmlBody.Replace("<arkivReferanse/>", archiveReference.ToUpper());
                htmlBody = htmlBody.Replace("<planNavn />", planNavn);
                htmlBody = htmlBody.Replace("<planIdent />", planIdent);

                var message = new MessageDataType()
                {
                    MessageTitle = $"Uttalelse - Oppstart av planarbeid, ({planIdent})",
                    MessageBody = htmlBody
                };

                return message;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Error occurred while getting submitter receipt message");

                throw;
            }
        }

        protected override (string Filename, string Name) GetFileNameForMainForm()
        {
            var Filename = "Uttalelse.pdf";
            var Name = "Uttalelse";
            return (Filename, Name);
        }

        protected override (string Filename, string Name) GetFileNameForAttachment()
        {
            var Filename = "uttalelsevedlegg.pdf";
            var Name = "Uttalelsevedlegg";
            return (Filename, Name);
        }

        protected override async Task<AttachmentBinary> GetUttalelsebrev(string containerName)
        {
            var metadataList = new List<KeyValuePair<string, string>>();
            metadataList.Add(new KeyValuePair<string, string>(BlobStorageMetadataKeys.AttachmentTypeName, "Uttalelsebrev"));

            var uttalelsebrevAsByteArray = await BlobOperations.GetBlobsAsBytesByMetadataAsync(BlobStorageEnum.Private, containerName, metadataList);
            
            return new AttachmentBinary
            {
                BinaryContent = uttalelsebrevAsByteArray.First(),
                Filename = GetFileNameForMainForm().Filename,
                Name = GetFileNameForMainForm().Name,
                ArchiveReference = ArchiveReference
            };
        }

        protected override async Task<AttachmentBinary> GetUttalelsevedlegg(string containerName)
        {
            var metadataList = new List<KeyValuePair<string, string>>();
            metadataList.Add(new KeyValuePair<string, string>(BlobStorageMetadataKeys.AttachmentTypeName, "UttalelseVedlegg"));

            var uttalelsebrevAsByteArray = await BlobOperations.GetBlobsAsBytesByMetadataAsync(BlobStorageEnum.Private, containerName, metadataList);
            
            if (uttalelsebrevAsByteArray == null || !uttalelsebrevAsByteArray.Any()) 
                return null;

            return new AttachmentBinary
            {
                BinaryContent = uttalelsebrevAsByteArray?.First(),
                Filename = GetFileNameForAttachment().Filename,
                Name = GetFileNameForAttachment().Name,
                ArchiveReference = ArchiveReference
            };
        }
    }
}
