using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_Common.Utils;
using Ftb_DbModels;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    public abstract class NotificationPrepareLogic<T> : PrepareLogic<T>
    {
        public NotificationPrepareLogic(IFormDataRepo repo,
                                        IBlobOperations blobOperations,
                                        ILogger log,
                                        IDbUnitOfWork dbUnitOfWork,
                                        IDecryptionFactory decryptionFactory,
                                        IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                        DistributionReceiverRepo distributionReceiverRepo,
                                        DistributionReceiverLogRepo distributionReceiverLogRepo)
            : base(repo,
                  log,
                  dbUnitOfWork,
                  decryptionFactory,
                  blobOperations,
                  fileDownloadHttpClient,
                  distributionReceiverRepo,
                  distributionReceiverLogRepo)
        {
        }
        public override async Task<IEnumerable<SendQueueItem>> ExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {
            var returnValue = await base.ExecuteAsync(submittalQueueItem);
            var sendQueueItems = base.CreateSendQueueItem(submittalQueueItem);
            var queueList = new List<SendQueueItem>();
            queueList.Add(sendQueueItems);
            await UpdateDistributionFormsAsync();
            await AddRepliedFilesToFileDownloadStatus(submittalQueueItem.ArchiveReference);
            return queueList;
        }

        protected async Task<string> GetInitialArchiveReferenceAsync(string distributionId)
        {
            if (DbUnitOfWork.DistributionForms != null)
            {
                var distributionForm = await DbUnitOfWork.DistributionForms.Get(distributionId.ToUpper());

                return distributionForm.InitialArchiveReference;
            }

            return null;
        }


        public async Task UpdateDistributionFormsAsync()
        {
            var distributionId = GetDistributionFormIdFromForm();
            if (DbUnitOfWork.DistributionForms != null)
            {
                var distributionForm = await DbUnitOfWork.DistributionForms.Get(distributionId.ToString());
                distributionForm.Signed = DateTime.Now;
                distributionForm.DistributionStatus = DistributionStatus.signed;
                distributionForm.SignedArchiveReference = ArchiveReference.ToUpper();

                var success = await DbUnitOfWork.DistributionForms.Update(distributionForm.InitialArchiveReference, distributionId, distributionForm);
            }
        }
        public virtual Task AddRepliedFilesToFileDownloadStatus(string archiveReference)
        {
            return Task.CompletedTask;
        }


        public async Task AddRepliedFilesToFileDownloadStatus(
            string archiveReference, 
            string displayFileName, 
            string formName, 
            FileTypesForDownloadEnum formDataFileType, 
            FileTypesForDownloadEnum mainFormFileType, 
            FileTypesForDownloadEnum attachmentFileType
            )
        {
            var targetContainerName = GetDistributionFormIdFromForm();
            var sourceContainerName = archiveReference;
            /*
            Main form XML
                metadata: type = FormData
            */
            var mainFormXMLDataMetadataList = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.FormData)
            };
            var mainFormData = await BlobOperations.GetBlobContentAsBytesByMetadataAsync(BlobStorageEnum.Private, sourceContainerName, mainFormXMLDataMetadataList);
            var uri = await BlobOperations.AddBytesToBlobStorageAsync(BlobStorageEnum.Private, targetContainerName.ToString(), $"{displayFileName}.xml", mainFormData.Content, mainFormData.MimeType);

            var fds = new FileDownloadStatus()
            {
                ArchiveReference = archiveReference.ToUpper(),
                BlobLink = uri,
                FileAccessCount = 0,
                Filename = $"{displayFileName}.xml",
                FileType = formDataFileType,
                FormName = formName,
                Guid = targetContainerName,
                IsDeleted = false,
                MimeType = mainFormData.MimeType
            };

            await FileDownloadHttpClient.PostAsync(archiveReference, fds);

            /*          
            Main form PDF
                metadata: attachmenttypename = SvarNabovarselPlan
                          type = MainForm
            */
            var mainFormPdfDataMetadataList = new List<KeyValuePair<string, string>>();
            mainFormPdfDataMetadataList.Add(new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.MainForm));

            var mainFormPdfData = await BlobOperations.GetBlobContentAsBytesByMetadataAsync(BlobStorageEnum.Private, sourceContainerName, mainFormPdfDataMetadataList);
            var mainFormPdfUri = await BlobOperations.AddBytesToBlobStorageAsync(BlobStorageEnum.Private, targetContainerName.ToString(), $"{displayFileName}.pdf", mainFormPdfData.Content, mainFormPdfData.MimeType);

            var mainFormPdfFds = new FileDownloadStatus()
            {
                ArchiveReference = archiveReference.ToUpper(),
                BlobLink = mainFormPdfUri,
                FileAccessCount = 0,
                Filename = $"{displayFileName}.pdf",
                FileType = mainFormFileType,
                FormName = formName,
                Guid = targetContainerName,
                IsDeleted = false,
                MimeType = mainFormPdfData.MimeType
            };

            await FileDownloadHttpClient.PostAsync(archiveReference, mainFormPdfFds);

            /*
            Attachments
                metadata: attachmenttypename = Annet
                          type = SubmittalAttachment            
            */
            var metadataList = new List<KeyValuePair<string, string>>();
            metadataList.Add(new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.SubmittalAttachment));

            var attachments = await BlobOperations.GetBlobContentsAsBytesByMetadataAsync(BlobStorageEnum.Private, sourceContainerName, metadataList);

            foreach (var attachment in attachments)
            {                
                //This is probably not the correct order...
                var fileName = $"Vedlegg.{attachment.FileName}";

                var attachmentUri = await BlobOperations.AddBytesToBlobStorageAsync(BlobStorageEnum.Private, targetContainerName.ToString(), fileName, attachment.Content, attachment.MimeType);

                var attachmentFds = new FileDownloadStatus()
                {
                    ArchiveReference = archiveReference.ToUpper(),
                    BlobLink = attachmentUri,
                    FileAccessCount = 0,
                    Filename = fileName,
                    FileType = attachmentFileType,
                    FormName = formName,
                    Guid = targetContainerName,
                    IsDeleted = false,
                    MimeType = attachment.MimeType
                };

                await FileDownloadHttpClient.PostAsync(archiveReference, attachmentFds);
            }
        }

        protected abstract string GetPublicPDFAttachmentTypeName();

        protected async Task CopyPDFToPublicBlobStorage(byte[] pdfDoc, string senderName, string publicContainer, string sendersArchiveReference)
        {
            var validSenderFilename = senderName.RemoveInvalidChars();

            var metadataList = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>(BlobStorageMetadataKeys.AttachmentTypeName, GetPublicPDFAttachmentTypeName()),
                new KeyValuePair<string, string>(BlobStorageMetadataKeys.SendersArchiveReference, sendersArchiveReference)
            };

            await BlobOperations.AddBytesToBlobStorageAsync(
                BlobStorageEnum.Public,
                publicContainer,
                $"Uttalelse_{validSenderFilename}_{DateTime.Now.ToString("yyyyMMdd_HHmmss")}.pdf",
                pdfDoc,
                "application/pdf",
                metadataList
            );
        }

        protected async Task AddFileToPublicBlobStorage(BlobContent blobContent, string publicContainer, string sendersArchiveReference)
        {
            var metadataList = new List<KeyValuePair<string, string>>();
            metadataList.AddRange(blobContent.Metadata);
            metadataList.Add(new KeyValuePair<string, string>(BlobStorageMetadataKeys.SendersArchiveReference, sendersArchiveReference));
            await BlobOperations.AddBytesToBlobStorageAsync(BlobStorageEnum.Public,
                                                                publicContainer,
                                                                blobContent.FileName,
                                                                blobContent.Content,
                                                                blobContent.MimeType,
                                                                metadataList);
        }
            

        protected async Task<IEnumerable< BlobContent>> GetFilesFromPrivateBlobStorageAsync(string archiveReference, KeyValuePair<string, string> metadataFilter)
        {            
            return await BlobOperations.GetBlobContentsAsBytesByMetadataAsync(BlobStorageEnum.Private, archiveReference, new List<KeyValuePair<string, string>> { metadataFilter });
        }

        protected abstract Guid GetDistributionFormIdFromForm();
    }
}