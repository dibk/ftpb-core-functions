using Altinn3.Adapters;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    public abstract class NotificationReportLogic<T> : ReportLogic<T>
    {
        protected override StatusApiFileInfo MainFormDownloadFileInfo => null;
        protected override StatusApiFileInfo ReceiptDownloadFileInfo => null;

        protected NotificationReportLogic(
            IBlobOperations blobOperations,
            IAltinnInstanceCompleter altinnInstanceCompleter,
            DistributionSubmittalRepo distributionSubmittalRepo,
            IFormDataRepo repo,
            ILogger log,
            IDbUnitOfWork dbUnitOfWork,
            IFileDownloadStatusHttpClient fileDownloadHttpClient,
            DistributionReceiverRepo distributionReceiverRepo,
            DistributionReceiverLogRepo distributionReceiverLogRepo
        ) : base(
            blobOperations,
            altinnInstanceCompleter,
            distributionSubmittalRepo,
            repo,
            log,
            dbUnitOfWork,
            fileDownloadHttpClient,
            distributionReceiverRepo,
            distributionReceiverLogRepo
        )
        { }

        public override async Task<string> ExecuteAsync(ReportQueueItem reportQueueItem)
        {
            var returnItem = await base.ExecuteAsync(reportQueueItem);

            return returnItem;
        }
    }
}
