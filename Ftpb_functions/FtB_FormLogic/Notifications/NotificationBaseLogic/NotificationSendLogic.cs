﻿using Altinn.Common;
using Altinn.Common.Exceptions;
using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    public abstract class NotificationSendLogic<T> : SendLogic<T>
    {
        protected readonly IBlobOperations BlobOperations;
        private readonly int BLOB_CONTAINER_LEASE_DURATION_MAX = 60;
        private readonly INotificationAdapter _notificationAdapter;

        public NotificationSendLogic(IFormDataRepo repo,
                                     IBlobOperations blobOperations,
                                     ILogger log,
                                     INotificationAdapter notificationAdapter,
                                     IDbUnitOfWork dbUnitOfWork,
                                     IHtmlUtils htmlUtils,
                                     IDecryptionFactory decryptionFactory,
                                     IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                     DistributionReceiverRepo distributionReceiverRepo,
                                     DistributionReceiverLogRepo distributionReceiverLogRepo)
            : base(repo,
                  log,
                  dbUnitOfWork,
                  decryptionFactory,
                  fileDownloadHttpClient,
                  distributionReceiverRepo,
                  distributionReceiverLogRepo)
        {
            BlobOperations = blobOperations;
            _notificationAdapter = notificationAdapter;
        }

        public override async Task<ReportQueueItem> ExecuteAsync(SendQueueItem sendQueueItem)
        {
            await base.ExecuteAsync(sendQueueItem);

            var returnReportQueueItem = new ReportQueueItem()
            {
                ArchiveReference = sendQueueItem.ArchiveReference,
                ReceiverLogPartitionKey = sendQueueItem.ReceiverLogPartitionKey,
                ReceiverSequenceNumber = sendQueueItem.ReceiverSequenceNumber,
                Sender = sendQueueItem.Sender,
                Receiver = sendQueueItem.Receiver
            };
            await SetReportingFlagForSubmittal(returnReportQueueItem.ArchiveReference.ToLower());
            await SendReceiptToSubmitterWhenReceiverIsProcessedAsync(returnReportQueueItem);

            return returnReportQueueItem;
        }

        private async Task<bool> SetReportingFlagForSubmittal(string containerName)
        {
            return await BlobOperations.AcquireContainerLeaseAsync(containerName, BLOB_CONTAINER_LEASE_DURATION_MAX);
        }

        private async Task SendReceiptToSubmitterWhenReceiverIsProcessedAsync(ReportQueueItem reportQueueItem)
        {
            try
            {
                Logger.LogInformation("Receiver has been processed. ReceiverSequenceNumber {ReceiverLogPartitionKey} triggers reporting", reportQueueItem.ReceiverLogPartitionKey);

                await SendReceiptAsync(reportQueueItem);
            }
            catch (SendNotificationException ex)
            {
                base.Logger.LogError("Error: {ExceptionMessage}: {DistriutionStep}", ex.Text, ex.DistriutionStep);
                throw ex;
            }
            catch (Exception ex)
            {
                base.Logger.LogError(ex, "Error occurred when creating and sending receipt");
                throw;
            }
            finally
            {
                await BlobOperations.ReleaseContainerLeaseAsync(reportQueueItem.ArchiveReference.ToLower());
            }
        }

        protected virtual async Task SendReceiptAsync(ReportQueueItem reportQueueItem)
        {
            AltinnNotificationMessage notificationMessage = await CreateNotificationMessage(reportQueueItem.ArchiveReference);

            await SendReceiptAsNotification(notificationMessage);
        }

        private async Task<AltinnNotificationMessage> CreateNotificationMessage(string archiveReference)
        {
            var notificationMessage = new AltinnNotificationMessage();
            notificationMessage.ArchiveReference = ArchiveReference;
            notificationMessage.Receiver = await GetReceiver();
            notificationMessage.ArchiveReference = archiveReference;
            notificationMessage.RespectReservable = false;

            var messageData = GetSubmitterReceiptMessage(archiveReference);
            notificationMessage.MessageData = messageData;

            var uttalelsebrev = await GetUttalelsebrev(archiveReference);
            var uttalelsevedlegg = await GetUttalelsevedlegg(archiveReference);

            notificationMessage.Attachments = new List<Attachment>() { uttalelsebrev };
            if (uttalelsevedlegg != null)
            {
                notificationMessage.Attachments = notificationMessage.Attachments.Append(uttalelsevedlegg);
            }

            return notificationMessage;
        }

        private async Task SendReceiptAsNotification(AltinnNotificationMessage notificationMessage)
        {
            Logger.LogInformation("Sends notification to receiverId {ReceiverId}", notificationMessage.Receiver.Id);
            AltinnNotificationCorrespondenceResult result = await _notificationAdapter.SendNotificationAsync(notificationMessage);

            var sendingSucceeded = result.IsSuccessfull();

            if (!sendingSucceeded)
            {
                var failedStep = result.FinalState;

                throw new SendNotificationException("Error: Failed during sending of submittal receipt", failedStep);
            }
        }

        public abstract Task<AltinnReceiver> GetReceiver();
        protected abstract MessageDataType GetSubmitterReceiptMessage(string archiveReference);
        protected abstract (string Filename, string Name) GetFileNameForMainForm();
        protected abstract (string Filename, string Name) GetFileNameForAttachment();
        protected abstract Task<AttachmentBinary> GetUttalelsebrev(string containerName);
        protected abstract Task<AttachmentBinary> GetUttalelsevedlegg(string containerName);
    }
}
