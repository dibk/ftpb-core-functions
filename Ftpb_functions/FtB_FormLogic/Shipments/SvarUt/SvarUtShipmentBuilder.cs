using Dibk.Ftpb.Common;
using Dibk.Ftpb.Common.Constants;
using Dibk.Ftpb.Common.Models;
using Dibk.Ftpb.Common.Registry;
using Dibk.Ftpb.Integration.SvarUt.Builders;
using Dibk.Ftpb.Integration.SvarUt.Builders.Models;
using Dibk.Ftpb.Integration.SvarUt.Models;
using FtB_Common.BusinessModels;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.Extensions;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_DataModels;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtB_FormLogic.Shipments.SvarUt;

public class SvarUtShipmentBuilder(
    ILogger<SvarUtShipmentBuilder> logger,
    IBlobOperations blobOperations,
    IDecryptionFactory decryptionFactory
)
{
    private IEnumerable<BlobStreamItem> _publicBlobs = [];
    private IEnumerable<Entry> _ekstraMetadataFraAvleverendeSystem = [];

    private IEnumerable<KeyValuePair<string, string>> _metadataFilter = 
    [
        BlobMetadata.BlobType(BlobStorageMetadataTypes.MainForm),
        BlobMetadata.BlobType(BlobStorageMetadataTypes.SubmittalAttachment),
        BlobMetadata.BlobType(BlobStorageMetadataTypes.Subform),
        BlobMetadata.BlobType(BlobStorageMetadataTypes.ValidationResult),
        BlobMetadata.BlobType(BlobStorageMetadataTypes.FormData),
        BlobMetadata.BlobType(BlobStorageMetadataTypes.GeneratedAttachment)
    ];

    private bool _useFormDataWithoutBeroerteParter = false;

    private readonly IEnumerable<string> AttachmentsToIgnore = 
    [
        BlobStorageMetadataTypes.AttachmentCollection
    ];

    public async Task<Forsendelse> Build(
        string archiveReference,
        Receiver receiver,
        IActor sender,
        ForsendelseDataBygg shipmentData)
    {
        var builder = new ForsendelseBuilder(shipmentData, GetDataTypeConfig);

        builder.AddBasicClassificationValues();
        builder.AddDigitalReceiver(receiver.Name, receiver.Id);
        builder.AddSender(CreateSvarSendesTil(sender));

        var blobs = await blobOperations.GetAllBlobsByFilterAsync(BlobStorageEnum.Private, archiveReference, _metadataFilter);

        if (_publicBlobs.Any())
            blobs = blobs.Concat(_publicBlobs);

        foreach (var blob in blobs)
        {
            var blobNamePrefix = blob.Metadata.BlobNamePrefix();

            if (blobNamePrefix != null)
                blob.FileName = blob.FileName.Remove(0, blobNamePrefix.Length);
        }

        //MainForm PDF
        var mainFormPdf = blobs.FirstOrDefault(p => p.Metadata.Contains(BlobMetadata.BlobType(BlobStorageMetadataTypes.MainForm)));
        if (mainFormPdf != null)
            builder.AddMainFormPdf(mainFormPdf.Metadata.AttachmentTypeName(),
                "Skjema.pdf",
                mainFormPdf.Content, mainFormPdf.Metadata.Vedleggsopplysninger());

        //MainForm XML
        var mainFormXml = _useFormDataWithoutBeroerteParter 
            ? blobs.FirstOrDefault(p => p.Metadata.Contains(BlobMetadata.BlobType(BlobStorageMetadataTypes.FormDataWithNoReceivers)))
            : blobs.FirstOrDefault(p => p.Metadata.Contains(BlobMetadata.BlobType(BlobStorageMetadataTypes.FormData)));
        builder.AddMainFormXml(mainFormXml.Metadata.AttachmentTypeName(),
            "Skjema.xml",
            mainFormXml.Content,
            $"{mainFormXml.Metadata.DataFormatId()}.{mainFormXml.Metadata.DataFormatVersion()}", mainFormXml.Metadata.Vedleggsopplysninger());


        //Subforms
        var subForms = blobs.Where(p => p.Metadata.Contains(BlobMetadata.BlobType(BlobStorageMetadataTypes.Subform)));
        foreach (var subForm in subForms)
        {
            var subformMetadata = SubformMetadataProvider.GetSubformMetadata(subForm.Metadata.DataFormatId(), subForm.Metadata.DataFormatVersion(), subForm.Metadata.SubformName());
            if (subForm.MimeType == "application/pdf")
                builder.AddSubformPdf(subForm.Metadata.AttachmentTypeName(),
                    $"{subformMetadata.SvarUtPdfFilename}.pdf",
                    subForm.Content, subForm.Metadata.Vedleggsopplysninger());
            else
                builder.AddSubformXml(subForm.Metadata.AttachmentTypeName(),
                    $"{subformMetadata.SvarUtXmlFilename}.xml",
                    subForm.Content,
                    $"{subForm.Metadata.DataFormatId()}.{subForm.Metadata.DataFormatVersion()}", subForm.Metadata.Vedleggsopplysninger());
        }

        //Validation result
        var validationResults = blobs.Where(p => p.Metadata.Contains(BlobMetadata.BlobType(BlobStorageMetadataTypes.ValidationResult)));
        foreach (var validationResult in validationResults)
        {
            //bruk attachmentTypeName fra metadata hvis det finnes, ellers bruk DataTypes.Valideringsrapport
            var attachmentTypeName = validationResult.Metadata.AttachmentTypeName() ?? DataTypes.Valideringsrapport;

            builder.AddAttachment(attachmentTypeName,
                validationResult.FileName,
                validationResult.MimeType,
                validationResult.Content, 
                validationResult.Metadata.Vedleggsopplysninger());
        }

        //Submittal attachments
        var submittalAttachments = blobs.Where(p => p.Metadata.Contains(BlobMetadata.BlobType(BlobStorageMetadataTypes.SubmittalAttachment)));
        foreach (var submittalAttachment in submittalAttachments)
        {
            var filename = submittalAttachment.FileName;

            bool inneholderPersonsensitivinformasjon = DataTypeRegistry.PersonalSensitiveInformationDataTypes.Contains(submittalAttachment.Metadata.AttachmentTypeName());

            builder.AddAttachment(submittalAttachment.Metadata.AttachmentTypeName(),
                filename,
                submittalAttachment.MimeType,
                submittalAttachment.Content,
                submittalAttachment.Metadata.Vedleggsopplysninger(),
                inneholderPersonsensitivinformasjon);
        }

        //Generated attachments
        var generatedAttachments = blobs.Where(p => p.Metadata.Contains(BlobMetadata.BlobType(BlobStorageMetadataTypes.GeneratedAttachment)));
        foreach (var generatedAttachment in generatedAttachments)
        {
            if (AttachmentsToIgnore.Contains(generatedAttachment.Metadata.AttachmentTypeName()))
                continue;

            var svarutFilename = generatedAttachment.Metadata.SvarUtFilename();
            //Hacky løsning for nå. Det eneste GeneratedAttachment enn så lenge skal ses på som en SubformPdf
            builder.AddSubformPdf(generatedAttachment.Metadata.AttachmentTypeName(),
                string.IsNullOrEmpty(svarutFilename) ? generatedAttachment.FileName : svarutFilename,
                generatedAttachment.Content, generatedAttachment.Metadata.Vedleggsopplysninger());
        }

        var shipment = builder.Build();

        shipment.MetadataFraAvleverendeSystem.EkstraMetadata.AddRange(_ekstraMetadataFraAvleverendeSystem);

        return shipment;
    }

    /// <summary>
    /// Include attachments from public storage
    /// </summary>
    /// <param name="containerName"></param>
    /// <returns></returns>
    public SvarUtShipmentBuilder AddAttachmentsFromPublicStorage(string containerName)
    {
        _publicBlobs = blobOperations.GetAllBlobsByFilterAsync(BlobStorageEnum.Public, containerName, _metadataFilter).Result;
        return this;
    }

    public SvarUtShipmentBuilder AddEkstraMetadataFraAvleverendeSystem(IEnumerable<Entry> ekstraMetadata)
    {
        _ekstraMetadataFraAvleverendeSystem = ekstraMetadata;
        return this;
    }

    /// <summary>
    /// This method is only to be used if the service has uploaded a form that does not contain any receivers. <br/>
    /// Examine the Prepare-logic for the service when in doubt.
    /// </summary>
    /// <returns>The same instance of <see cref="SvarUtShipmentBuilder"/>.</returns>
    public SvarUtShipmentBuilder UseFormDataWithoutBeroerteParter()
    {
        _metadataFilter = _metadataFilter
            .Where(m => !m.Equals(BlobMetadata.BlobType(BlobStorageMetadataTypes.FormData)))
            .Concat([BlobMetadata.BlobType(BlobStorageMetadataTypes.FormDataWithNoReceivers)]);

        _useFormDataWithoutBeroerteParter = true;

        return this;
    }

    private (string, string, string, string, string) GetDataTypeConfig(string dataType)
    {
        DataTypeConfig retVal;
        try
        {
            retVal = DataTypeMapper.Get(dataType);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Unable to find dataTypeConfig for {DataType}", dataType);
            retVal = DataTypeMapper.Get(DataTypes.Annet);
        }

        return (retVal.DataType, retVal.DisplayName, retVal.Arkivlett1DokumentType, retVal.Arkivlett2DokumentType, retVal.Category);
    }

    private Adresse CreateSvarSendesTil(IActor sender)
    {
        var shipmentSender = sender as ShipmentActor;

        Adresse svarSendesTil = new Adresse();
        if (sender.Type == ActorTypeEnum.Privatperson)
        {
            svarSendesTil.DigitalAdresse = new Digitaladresse
            {
                FodselsNummer = decryptionFactory.GetDecryptor().DecryptText(shipmentSender.Id)
            };
        }
        else
        {
            svarSendesTil.DigitalAdresse = new Digitaladresse
            {
                OrganisasjonsNummer = shipmentSender.Id
            };
        }

        svarSendesTil.PostAdresse = new PostAdresse
        {
            Navn = shipmentSender.Name,
            Adresse1 = shipmentSender.Address,
            PostNummer = shipmentSender.PostalCode,
            PostSted = shipmentSender.Poststed
        };

        if (string.IsNullOrEmpty(svarSendesTil.PostAdresse.PostNummer)) svarSendesTil.PostAdresse.PostNummer = "9999";
        if (string.IsNullOrEmpty(svarSendesTil.PostAdresse.PostSted)) svarSendesTil.PostAdresse.PostSted = "Digital levering";

        return svarSendesTil;
    }
}