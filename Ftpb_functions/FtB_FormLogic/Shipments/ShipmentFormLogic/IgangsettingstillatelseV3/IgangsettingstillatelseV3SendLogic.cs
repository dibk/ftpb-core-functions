﻿using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Integration.SvarUt.Builders.Models;
using FtB_Common;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Constants;
using FtB_Common.Encryption;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_FormLogic.Shipments.SvarUt;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace FtB_FormLogic.Shipments.ShipmentFormLogic.IgangsettingstillatelseV3
{
    [FormDataFormat(
        DataFormatId = DataFormatIDs.IgangsettingstillatelseV3,
        DataFormatVersion = IgangsettingstillatelseV3Helper.DataFormatVersion,
        ProcessingContext = FormLogicProcessingContext.Send
    )]
    public class IgangsettingstillatelseV3SendLogic : SvarUtSendLogic<Igangsettingstillatelse>
    {
        private readonly EnvironmentProvider _env;

        public IgangsettingstillatelseV3SendLogic(IFormDataRepo repo,
                                        ILogger<IgangsettingstillatelseV3SendLogic> log,
                                        IBlobOperations blobOperations,
                                        ISvarUtAdapter svarUtAdapter,
                                        IDbUnitOfWork dbUnitOfWork,
                                        IDecryptionFactory decryptionFactory,
                                        IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                        SvarUtShipmentBuilder shipmentBuilder,
                                        EnvironmentProvider env,
                                        DistributionReceiverRepo distributionReceiverRepo,
                                        DistributionReceiverLogRepo distributionReceiverLogRepo)
            : base(repo,
                  log,
                  blobOperations,
                  svarUtAdapter,
                  dbUnitOfWork,
                  decryptionFactory,
                  fileDownloadHttpClient,
                  shipmentBuilder,
                  distributionReceiverRepo,
                  distributionReceiverLogRepo)
        { _env = env; }

        public override async Task<ReportQueueItem> ExecuteAsync(SendQueueItem sendQueueItem)
        {
            var reportQueueItem = await base.ExecuteAsync(sendQueueItem);

            return reportQueueItem;
        }

        public override Task SetSenderAsync()
        {
            var address = string.Empty;
            var postalCode = string.Empty;
            var poststed = string.Empty;

            if (FormData.AnsvarligSoeker != null)
            {
                address = FormData.AnsvarligSoeker.Adresse?.Adresselinje1;
                postalCode = FormData.AnsvarligSoeker.Adresse?.Postnr;
                poststed = FormData.AnsvarligSoeker.Adresse?.Poststed;
            }

            Sender = new ShipmentActor(SendQueueItem.Sender)
            {
                Address = address,
                PostalCode = postalCode,
                Poststed = poststed
            };

            return Task.CompletedTask;
        }

        protected override ForsendelseDataBygg MapForsendelseData()
        {
            return new ForsendelseDataBygg(SendQueueItem.ArchiveReference)
            {
                Adresselinje1 = FormData.EiendomByggested[0].Adresse.Adresselinje1,
                Adresselinje2 = FormData.EiendomByggested[0].Adresse.Adresselinje2,
                Adresselinje3 = FormData.EiendomByggested[0].Adresse.Adresselinje3,
                Kommunenummer = FormData.EiendomByggested[0].Eiendomsidentifikasjon.Kommunenummer,
                Gårdsnummer = FormData.EiendomByggested[0].Eiendomsidentifikasjon.Gaardsnummer,
                Bruksnummer = FormData.EiendomByggested[0].Eiendomsidentifikasjon.Bruksnummer,
                Seksjonsnummer = FormData.EiendomByggested[0].Eiendomsidentifikasjon.Seksjonsnummer,
                Festenummer = FormData.EiendomByggested[0].Eiendomsidentifikasjon.Festenummer,
                Postnr = FormData.EiendomByggested[0].Adresse.Postnr,
                Poststed = FormData.EiendomByggested[0].Adresse.Poststed,
                AvgivendeSystem = FormData.Metadata?.FraSluttbrukersystem,
                Bolignummer = FormData.EiendomByggested[0].Bolignummer,
                KommunensSaksnummerSekvensnummer = FormData.KommunensSaksnummer.Sakssekvensnummer.ToString(),
                KommunensSaksnummerÅr = FormData.KommunensSaksnummer.Saksaar.ToString(),
                ForsendelseType = "Byggesøknad",
                SøknadSkjemaNavn = "Søknad om igangsettingstillatelse",
                Konteringskode = "IG-V3"
            };
        }
    }
}