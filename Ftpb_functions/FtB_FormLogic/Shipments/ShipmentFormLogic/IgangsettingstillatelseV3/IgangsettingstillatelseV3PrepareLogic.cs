﻿using Dibk.Ftpb.Api.Models;
using Dibk.Ftpb.Common.Constants;
using Dibk.Ftpb.Common.Datamodels;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Constants;
using FtB_Common.Encryption;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Ftb_Repositories.LogContstants;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtB_FormLogic.Shipments.ShipmentFormLogic.IgangsettingstillatelseV3
{
    [FormDataFormat(
        DataFormatId = DataFormatIDs.IgangsettingstillatelseV3,
        DataFormatVersion = IgangsettingstillatelseV3Helper.DataFormatVersion,
        ProcessingContext = FormLogicProcessingContext.Prepare
    )]
    public class IgangsettingstillatelseV3PrepareLogic : ShipmentPrepareLogic<Igangsettingstillatelse>
    {
        private readonly MunicipalityService _municipalityService;

        private const string FormType = "Søknad om igangsettingstillatelse";

        public IgangsettingstillatelseV3PrepareLogic(IFormDataRepo repo,
                                           ILogger<IgangsettingstillatelseV3PrepareLogic> log,
                                           IBlobOperations blobOperations,
                                           IDbUnitOfWork dbUnitOfWork,
                                           IDecryptionFactory decryptionFactory,
                                           IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                           MunicipalityService municipalityService,
                                           DistributionReceiverRepo distributionReceiverRepo,
                                           DistributionReceiverLogRepo distributionReceiverLogRepo) 
            : base(repo,
                  log,
                  blobOperations,
                  dbUnitOfWork,
                  decryptionFactory,
                  fileDownloadHttpClient,
                  distributionReceiverRepo,
                  distributionReceiverLogRepo)
        {
            _municipalityService = municipalityService;
        }

        public override Task<IEnumerable<SendQueueItem>> ExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {
            Logger.LogInformation("{formType} mottatt av CoreFunctions", FormType);
            Logger.LogInformation("Oppretter formMetadata");
            var formmetadata = new FormMetadataApiModel
            {
                ArchiveReference = submittalQueueItem.ArchiveReference,
                Status = FormMetadataStatus.IKø,
                ArchiveTimestamp = DateTime.Now,
                FormType = FormType,
                Application = FormData.Metadata?.FraSluttbrukersystem,
                SenderSystem = "SvarUt",
                ServiceCode = "IGV3",
                ServiceEditionCode = 1
            };
            DbUnitOfWork.FormMetadata.Create(formmetadata);

            return base.ExecuteAsync(submittalQueueItem);
        }

        public override async Task SetReceiversAsync()
        {
            var kommunenummer = FormData.EiendomByggested[0].Eiendomsidentifikasjon.Kommunenummer;
            var (name, organizationNumber) = await _municipalityService.GetMunicipalityOrgnr(kommunenummer);

            try
            {
                var receivers = new List<Receiver>
                {
                    new()
                    {
                        Type = ActorTypeEnum.OffentligMyndighet,
                        Id = organizationNumber,
                        Name = name
                    }
                };
                SetReceivers(receivers);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public override async Task SetSenderAsync()
        {
            var encryptedReporteeId = await BlobOperations.GetReporteeIdFromStoredBlobAsync(ArchiveReference);
            var senderCandidates = GetSenderCandidates(DecryptionFactory.GetDecryptor(), FormData);

            Sender = senderCandidates.FirstOrDefault(r =>
                r.Id.Equals(DecryptionFactory.GetDecryptor().DecryptText(encryptedReporteeId)));
        }

        public static List<Actor> GetSenderCandidates(IDecryption decryptor, Igangsettingstillatelse igangsettingstillatelse)
        {
            var retval = new List<Actor>();
            if (!string.IsNullOrEmpty(igangsettingstillatelse.AnsvarligSoeker?.Organisasjonsnummer))
                retval.Add(new Actor
                {
                    Id = igangsettingstillatelse.AnsvarligSoeker.Organisasjonsnummer,
                    Name = igangsettingstillatelse.AnsvarligSoeker.Navn,
                    Type = ActorTypeEnum.Foretak
                });

            if (!string.IsNullOrEmpty(igangsettingstillatelse.AnsvarligSoeker?.Foedselsnummer))
                retval.Add(new Actor
                {
                    Id = decryptor.DecryptText(igangsettingstillatelse.AnsvarligSoeker.Foedselsnummer),
                    Name = igangsettingstillatelse.AnsvarligSoeker.Navn,
                    Type = ActorTypeEnum.Privatperson
                });

            return retval;
        }
    }
}