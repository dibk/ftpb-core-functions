using Altinn3.Adapters;
using Dibk.Ftpb.Common.Constants;
using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.ModelToPdf.Common.Client;
using Dibk.Ftpb.ModelToPdf.Common.Models;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Constants;
using FtB_Common.Enums;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_FormLogic.Shipments.ShipmentBaseLogic;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace FtB_FormLogic.Shipments.ShipmentFormLogic.IgangsettingstillatelseV3;

[FormDataFormat(
    DataFormatId = DataFormatIDs.IgangsettingstillatelseV3,
    DataFormatVersion = IgangsettingstillatelseV3Helper.DataFormatVersion,
    ProcessingContext = FormLogicProcessingContext.Report
)]
public class IgangsettingstillatelseV3ReportLogic : SvarUtReportLogic<Igangsettingstillatelse>
{
    private readonly IDatamodelToPdfHttpClient _datamodelToPdfHttpClient;

    protected override bool CompleteAltinn3Instance => true;

    protected override StatusApiFileInfo MainFormDownloadFileInfo => new()
    {
        Name = DataTypes.SøknadOmIgangsettingstillatelse,
        FileName = $"{DataTypes.SøknadOmIgangsettingstillatelse}.pdf",
        FileType = FileTypesForDownloadEnum.SoeknadOmIgangsettingstillatelse,
        MimeType = "application/pdf"
    };

    private readonly StatusApiFileInfo _receiptFileDownloadInfo = new()
    {
        FileName = $"{DataTypes.SøknadOmIgangsettingstillatelse}-receipt.pdf",
        FileType = FileTypesForDownloadEnum.SoeknadOmIgangsettingstillatelseKvittering,
        MimeType = "application/pdf"
    };

    protected override StatusApiFileInfo ReceiptDownloadFileInfo => _receiptFileDownloadInfo;

    public IgangsettingstillatelseV3ReportLogic(
        IDatamodelToPdfHttpClient datamodelToPdfHttpClient,
        IBlobOperations blobOperations,
        IAltinnInstanceCompleter altinnInstanceCompleter,
        DistributionSubmittalRepo distributionSubmittalRepo,
        IFormDataRepo formDataRepo,
        ILogger<IgangsettingstillatelseV3ReportLogic> logger,
        IDbUnitOfWork dbUnitOfWork,
        IFileDownloadStatusHttpClient fileDownloadHttpClient,
        DistributionReceiverRepo distributionReceiverRepo,
        DistributionReceiverLogRepo distributionReceiverLogRepo
    ) : base(
        blobOperations,
        altinnInstanceCompleter,
        distributionSubmittalRepo,
        formDataRepo,
        logger,
        dbUnitOfWork,
        fileDownloadHttpClient,
        distributionReceiverRepo,
        distributionReceiverLogRepo
    )
    {
        _datamodelToPdfHttpClient = datamodelToPdfHttpClient;
    }

    public override async Task PreExecuteAsync(ReportQueueItem reportQueueItem)
    {
        await base.PreExecuteAsync(reportQueueItem);
        var formMetadata = await DbUnitOfWork.FormMetadata.Get();
        ReceiptDownloadFileInfo.Name = formMetadata.SvarUtDocumentTitle;
    }

    protected override async Task<byte[]> GetSubmitterReceiptPdfAsBytesAsync(string archiveReference)
    {
        var modelToPdfRequest = await CreateModelToPdfRequestAsync(archiveReference);

        await using var pdfStream = await _datamodelToPdfHttpClient.CreatePdfAsync(modelToPdfRequest, CancellationToken.None);

        var pdfBytes = new byte[pdfStream.Length];

        _ = await pdfStream.ReadAsync(pdfBytes);

        return pdfBytes;
    }

    private async Task<ModelToPdfRequest> CreateModelToPdfRequestAsync(string archiveReference)
    {
        var receiptData = await CreateReceiptDataAsync();

        var receiptInformationAsync = await GetAttachmentInfosForReceiptAsync(archiveReference);

        return new ModelToPdfRequest
        {
            ArchiveReference = archiveReference,
            Type = DataModelTypes.ByggReceiptToDistributor,
            ContentString = JsonSerializer.Serialize(receiptData),
            Vedlegg = receiptInformationAsync
        };
    }

    private async Task<ByggModelToPdfKvittering> CreateReceiptDataAsync()
    {
        return new ByggModelToPdfKvittering
        {
            TittelForSoeknaden = ReceiptDownloadFileInfo.Name,
            KommunensSaksnummer = $"{FormData.KommunensSaksnummer.Saksaar}/{FormData.KommunensSaksnummer.Sakssekvensnummer}",
            SoeknadMottattAvKommune = (await DbUnitOfWork.FormMetadata.Get(ArchiveReference)).SvarUtShippingTimestamp
        };
    }

    private async Task<List<VedleggsInfo>> GetAttachmentInfosForReceiptAsync(string archiveReference)
    {
        var blobStorageTypes = new List<string> { BlobStorageMetadataTypes.MainForm, BlobStorageMetadataTypes.SubmittalAttachment };
        var listOfAttachmentsInSubmittal = await BlobOperations.GetListOfBlobsMetadataValuesByBlobItemTypesAsync(BlobStorageEnum.Private, archiveReference.ToLower(), blobStorageTypes, true);

        var vedlegg = new List<VedleggsInfo>();

        foreach (var (attachmentTypeName, fileName, vedleggsopplysninger) in listOfAttachmentsInSubmittal)
        {
            vedlegg.Add(new VedleggsInfo
            {
                Filename = fileName,
                DataType = attachmentTypeName,
                DisplayName = attachmentTypeName,
                AttachmentDescription = vedleggsopplysninger.FirstOrDefault(x => x.Key == Tags.VedleggsopplysningerFraSøker.VedleggKommentar.ToString()).Value,
                AttachmentVersionNumber = vedleggsopplysninger.FirstOrDefault(x => x.Key == Tags.VedleggsopplysningerFraSøker.VedleggVersjonsnr.ToString()).Value,
                AttachmentVersionDate = vedleggsopplysninger.FirstOrDefault(x => x.Key == Tags.VedleggsopplysningerFraSøker.VedleggVersjonsdato.ToString()).Value

            });
        }

        return vedlegg;
    }
}