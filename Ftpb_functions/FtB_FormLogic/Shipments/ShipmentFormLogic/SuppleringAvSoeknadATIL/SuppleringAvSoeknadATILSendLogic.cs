﻿//using Dibk.Ftpb.Integration.SvarUt.Models;
//using FtB_Common.BusinessModels;
//using FtB_Common.Encryption;
//using FtB_Common.Enums;
//using FtB_Common.FormLogic;
//using FtB_Common.Interfaces;
//using FtB_Common.Storage;
//using FtB_Common.Utils;
//using FtB_FormLogic.Shipments.Models;
//using Ftb_Repositories;
//using Ftb_Repositories.HttpClients;
//using FtB_Validation;
//using FtB_Validation.Models;
//using Microsoft.Extensions.Logging;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Text.RegularExpressions;
//using System.Threading;
//using System.Threading.Tasks;

//namespace FtB_FormLogic.Shipments.ShipmentFormLogic.SuppleringAvSoeknadATIL
//{
//    [FormDataFormat(DataFormatId = "7086", DataFormatVersion = "47365", ProcessingContext = FormLogicProcessingContext.Send)]
//    public class SuppleringAvSoeknadATILSendLogic : ShipmentSendLogic<no.kxml.skjema.dibk.suppleringArbeidstilsynet.SuppleringArbeidstilsynetType>
//    {
//        private readonly IValidationApiService _validationServiceHttpClient;

//        public SuppleringAvSoeknadATILSendLogic(IFormDataRepo repo,
//                                        ITableStorage tableStorage,
//                                        ILogger<SuppleringAvSoeknadATILSendLogic> log,
//                                        IBlobOperations blobOperations,
//                                        ISvarUtAdapter svarUtAdapter,
//                                        IDbUnitOfWork dbUnitOfWork,
//                                        IDecryptionFactory decryptionFactory,
//                                        IFileDownloadStatusHttpClient fileDownloadHttpClient,
//                                        IValidationApiService validationServiceHttpClient)
//            : base(repo, tableStorage, log, blobOperations, svarUtAdapter, dbUnitOfWork, decryptionFactory, fileDownloadHttpClient)
//        {
//            _validationServiceHttpClient = validationServiceHttpClient;
//        }

//        protected override void SetShipmentContext()
//        {
//            ShipmentContext = new ShipmentContext();
//            ShipmentContext.AvgivendeSystem = FormData.metadata.fraSluttbrukersystem;
//        }

//        public override async Task<ReportQueueItem> ExecuteAsync(SendQueueItem sendQueueItem)
//        {
//            var validationInputData = new ValidationInputData()
//            {
//                AuthenticatedSubmitter = this.AuthenticatedSubmitter,
//                FormData = this.XmlData,
//                Attachments = this.Attachments.Select(s => new AttachmentValidationInfo()
//                {
//                    AttachmentTypeName = s.AttachmentTypeName,
//                    Filename = s.Filename,
//                    FileSize = s.FileSizeMb
//                }).ToList()
//            };
//            var validationResult = await _validationServiceHttpClient.ValidateReportAsync(validationInputData, CancellationToken.None);
//            var validationIsOK = validationResult.Errors == 0;

//            if (validationIsOK)
//            {
//                await UpdateFormDataBlobWithReplacingKommuneNrWithArbeidstilsynetsSaksnummerAsync(sendQueueItem.ArchiveReference.ToLower());
//                await AddValidationReportToBlobStorage(sendQueueItem.ArchiveReference.ToLower(), validationResult);

//                await base.ExecuteAsync(sendQueueItem);
//            }
//            else
//            {
//                DbUnitOfWork.LogEntries.AddInfo($"Feil i datavalidering. Innsending avbrutt. Feilmeldinger: {validationResult.GetValidationMessages()}");
//                await UpdateFormProcessStatus("Feil - datavalidering");

//                //Set status for the submittal to error
//            }

//            return null;
//        }

//        private async Task AddValidationReportToBlobStorage(string container, ValidationResponse validationResult)
//        {
//            var validationResultAsXml = SerializeUtil.SerializeWithNoWhitespaces(validationResult);

//            var metadataList = new List<KeyValuePair<string, string>>();
//            metadataList.Add(new KeyValuePair<string, string>("Type", BlobStorageMetadataTypeEnum.ValidationResult.ToString()));
//            var validationResultAsBytes = Encoding.UTF8.GetBytes(validationResultAsXml);
//            await BlobOperations.AddBytesToBlobStorageAsync(BlobStorageEnum.Private, container,
//                                                            "valideringsrapportFraFellestjenesterBygg.xml", validationResultAsBytes, "application/xml", metadataList);
//        }

//        private async Task UpdateFormDataBlobWithReplacingKommuneNrWithArbeidstilsynetsSaksnummerAsync(string containerName)
//        {
//            //Replacing "kommunensOrganisasjonnummer" with ArbeidstilsynetsSaksnummer
//            //1. Get the name of the FormData blob (type = FormData)
//            //2. Get ArbeidstilsynetsSaksnummer
//            //3. Update kommunensSaksnummer with value from ArbeidstilsynetsSaksnummer
//            //4. Serialize and persist the FormDataXML to BlobStorage
//            var metadataFilterMainBlobs = new List<KeyValuePair<string, string>>();
//            metadataFilterMainBlobs.Add(new KeyValuePair<string, string>("Type", Types.FormData));
//            var formBlob = await BlobOperations.GetAllBlobsByFilterAsync(BlobStorageEnum.Private, containerName, metadataFilterMainBlobs);
//            var formBlobFound = formBlob.First();
//            if (formBlobFound != null)
//            {
//                var formBlobName = formBlobFound.FileName;

//                var arbeidstilsynetsSaksnummer = this.FormData.arbeidstilsynetsSaksnummer;
//                FormData.kommunensSaksnummer = arbeidstilsynetsSaksnummer;
//                var xmlWithArbeidstilsynetsSaksnummer = SerializeUtil.SerializeWithNoWhitespaces(FormData);

//                var metadataListFormData = new List<KeyValuePair<string, string>>();
//                metadataListFormData.Add(new KeyValuePair<string, string>("Type", Types.FormData));
//                var xmlWithArbeidstilsynetsSaksnummerAsBytes = Encoding.UTF8.GetBytes(xmlWithArbeidstilsynetsSaksnummer);
//                await BlobOperations.AddBytesToBlobStorageAsync(BlobStorageEnum.Private, containerName,
//                                                                formBlobName, xmlWithArbeidstilsynetsSaksnummerAsBytes, "application/xml", metadataListFormData);
//            }
//            else
//            {
//                throw new Exception($"FormData blob not found in container {containerName}");//   FileNotFoundException("");
//            }
//        }

//        protected override Task<ForsendelseSupplementation> LoadSupplementationData(string archiveReference)
//        {
//            var supplementationData = new ForsendelseSupplementation()
//            {
//                ReceivingAuthority = new Models.ReceivingAuthority()
//                {
//                    AuthorityType = Authority.Sektormyndighet,
//                    Name = this.Receiver.Name,
//                    OrganizationNumber = this.Receiver.Id
//                },
//                SvarUtClassificationValues = new List<KeyValuePair<string, string>>(), //Skal kanskje inneholde klasseringer som brukes ved import i saksbehandlingssystem
//                SubmittalAttachments = new List<SubmittalAttachment>(), // Inneholder metadata fra Alfa med vedleggsinfo som brukes pr dokument i svarut forsendelsen
//                Metadata = new List<MetadataItem>(), //Skal kanskje inneholde metadata som mappes over i "ekstrametadata" i svarut forsendelsen
//                SvarUtDocumentTitle = GetSvarUtDocumentTitle()
//            };

//            return Task.FromResult(supplementationData);
//        }

//        private string GetSvarUtDocumentTitle()
//        {
//            StringBuilder sb = new StringBuilder();
//            if (!string.IsNullOrEmpty(FormData.eiendomByggested[0].adresse.adresselinje1))
//            {
//                sb.Append(FormData.eiendomByggested[0].adresse.adresselinje1 + " ");
//            }

//            sb.Append("Supplering av søknad til Arbeidstilsynets samtykke");

//            if (!string.IsNullOrEmpty(FormData.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer) && !string.IsNullOrEmpty(FormData.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer))
//            {
//                sb.Append(" " + FormData.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer + "/" + FormData.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer);
//            }

//            return sb.ToString();
//        }

//        protected override async Task<List<IDokument>> GetDocumentsForShipment(string archiveReference)
//        {
//            //Get from blob storage
//            var mainFormDocuments = await GetBlobItemsForSvarUt(archiveReference.ToLower());
//            var allDocuments = new List<SvarUtDokument>();
//            allDocuments.AddRange(mainFormDocuments);

//            return allDocuments.Select(p => (IDokument)p).ToList();
//        }

//        private async Task<IEnumerable<SvarUtDokument>> GetBlobItemsForSvarUt(string archiveReference)
//        {
//            const int AttachmentPrefixIncrement = 2;
//            var metadataFilterMainBlobs = new List<KeyValuePair<string, string>>();
//            metadataFilterMainBlobs.Add(new KeyValuePair<string, string>("Type", Types.MainForm));
//            metadataFilterMainBlobs.Add(new KeyValuePair<string, string>("Type", Types.SubmittalAttachment));
//            metadataFilterMainBlobs.Add(new KeyValuePair<string, string>("Type", BlobStorageMetadataTypeEnum.Subform.ToString()));
//            metadataFilterMainBlobs.Add(new KeyValuePair<string, string>("Type", BlobStorageMetadataTypeEnum.ValidationResult.ToString()));
//            metadataFilterMainBlobs.Add(new KeyValuePair<string, string>("Type", Types.FormData));
//            var mainBlobs = await BlobOperations.GetAllBlobsByFilterAsync(BlobStorageEnum.Private, archiveReference, metadataFilterMainBlobs);

//            //Get the largest prefixed number from files
//            var allPrefixNumbers = mainBlobs.Where(x => Regex.IsMatch(x.FileName, @"^\d+"))
//                                            .Select(y => Regex.Match(y.FileName, @"^\d+").Value)
//                                            .Select(int.Parse).ToList();

//            //Let form-PDF get prefix 1, and then let attachemtns already containg prefix increment with 2 (due to added form-pdf and 1-based array)
//            //, and finally add prefix to non-prefixed attachements with following numbers
//            int nextAvailablePrefixForUnprefixedAttachments = allPrefixNumbers.Count() + 1;

//            //Document of type BlobStorageMetadataTypeEnum.MainForm ("Skjema.pdf") has to be the first in this methods returned list (hence OrderBy in return statement)
//            mainBlobs = mainBlobs.Select(b =>
//            {
//                if (b.Metadata.Contains(new KeyValuePair<string, string>("Type", Types.MainForm), new MetadataComparer()))
//                {
//                    //Handle "MainForm" ("Skjema.pdf")
//                    b.FileName = $"1_{b.FileName}";
//                }
//                else if (b.Metadata.Contains(new KeyValuePair<string, string>("Type", Types.FormData), new MetadataComparer()))
//                {
//                    //Handle "Formdata"
//                    var t = mainBlobs.FirstOrDefault(m => m.Metadata.Contains(new KeyValuePair<string, string>("Type", Types.MainForm), new MetadataComparer()));
//                    var filename = System.IO.Path.GetFileNameWithoutExtension(t.FileName);
//                    nextAvailablePrefixForUnprefixedAttachments++;
//                    //Hack for å skille mellom søknadens pdf-navn og signerte skjema sitt
//                    b.FileName = $"{nextAvailablePrefixForUnprefixedAttachments}_{filename}_KAN-INNEHOLDE-PERSONOPPLYSNINGER.xml";
//                }
//                else if (int.TryParse(Regex.Match(b.FileName, @"^\d+").Value, out var existingPrefix))
//                {
//                    //Increment already prefixed attachments with 2 (due to added form-pdf and 1-based array)
//                    //File name without prefix => remove prefix and "_"
//                    var fileNameWOPrefix = b.FileName.Substring(existingPrefix.ToString().Length + 1);
//                    existingPrefix = existingPrefix + AttachmentPrefixIncrement;
//                    b.FileName = $"{existingPrefix}_{fileNameWOPrefix}";
//                }
//                else
//                {
//                    //Handle non-prefixed attachments
//                    if (!Regex.IsMatch(b.FileName, @"^\d+"))
//                    {
//                        nextAvailablePrefixForUnprefixedAttachments++;
//                        b.FileName = $"{nextAvailablePrefixForUnprefixedAttachments}_{b.FileName}";
//                    }
//                }

//                return b;
//            }).ToList();

//            return mainBlobs.Select(s => new SvarUtDokument() { DocumentContent = s.Content, Filnavn = s.FileName }).OrderBy(x => x.Filnavn).ToList();
//        }

//        public override async Task SetSenderAsync()
//        {
//            Sender = new ShipmentActor(SendQueueItem.Sender)
//            {
//                Address = FormData.ansvarligSoeker.adresse.adresselinje1,
//                PostalCode = FormData.ansvarligSoeker.adresse.postnr,
//                Poststed = FormData.ansvarligSoeker.adresse.poststed
//            };
//        }
//    }
//}