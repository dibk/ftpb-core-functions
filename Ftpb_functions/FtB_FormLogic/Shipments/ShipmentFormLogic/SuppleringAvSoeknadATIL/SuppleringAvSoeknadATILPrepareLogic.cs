﻿using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
//using FtB_FormLogic.Shipments.Models;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FtB_FormLogic.Shipments.ShipmentFormLogic.SuppleringAvSoeknadATIL
{
    [FormDataFormat(DataFormatId = "7086", DataFormatVersion = "47365", ProcessingContext = FormLogicProcessingContext.Prepare)]
    public class SuppleringAvSoeknadATILPrepareLogic : ShipmentPrepareLogic<no.kxml.skjema.dibk.suppleringArbeidstilsynet.SuppleringArbeidstilsynetType>
    {
        private readonly string _atilName;
        private readonly string _atilOrgnr;

        public SuppleringAvSoeknadATILPrepareLogic(IFormDataRepo repo,
                                                   ILogger<SuppleringAvSoeknadATILPrepareLogic> log,
                                                   IDbUnitOfWork dbUnitOfWork,
                                                   IDecryptionFactory decryptionFactory,
                                                   IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                                   IBlobOperations blobOperations,
                                                   IConfiguration configuration,
                                                   DistributionReceiverRepo distributionReceiverRepo,
                                                   DistributionReceiverLogRepo distributionReceiverLogRepo) :
            base(repo,
                log,
                blobOperations,
                dbUnitOfWork,
                decryptionFactory,
                fileDownloadHttpClient,
                distributionReceiverRepo,
                distributionReceiverLogRepo)
        {
            _atilName = "Arbeidstilsynet";
            _atilOrgnr = configuration["Arbeidstilsynet:OrgNr"];
        }

        public override Task SetReceiversAsync()
        {
            var receivers = new List<Receiver>
            {
                new() { Type = ActorTypeEnum.OffentligMyndighet, Id = _atilOrgnr, Name = _atilName }
            };

            base.SetReceivers(receivers);
            return Task.CompletedTask;
        }

        public override Task SetSenderAsync()
        {
            var id = string.Empty;
            Enum.TryParse(EnumExtentions.GetValueFromDescription<ActorTypeEnum>(FormData.ansvarligSoeker.partstype.kodeverdi).ToString(), out ActorTypeEnum receiverType);
            if (receiverType.Equals(ActorTypeEnum.Privatperson))
            {
                id = DecryptionFactory.GetDecryptor().DecryptText(FormData.ansvarligSoeker.foedselsnummer);
            }
            else
            {
                id = FormData.ansvarligSoeker.organisasjonsnummer;
            }

            base.Sender = new Actor() { Id = id, Name = FormData.ansvarligSoeker.navn, Type = receiverType };

            return Task.CompletedTask;
        }
    }
}