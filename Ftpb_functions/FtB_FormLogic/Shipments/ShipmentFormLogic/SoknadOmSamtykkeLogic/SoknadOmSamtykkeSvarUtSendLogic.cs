using Dibk.Ftpb.Integration.SvarUt.Builders.Models;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_Common.Utils;
using FtB_FormLogic.Shipments.SvarUt;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Ftb_Repositories.LogContstants;
using FtB_Validation;
using FtB_Validation.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FtB_FormLogic.Shipments.ShipmentFormLogic.SoknadOmSamtykkeLogic
{
    [FormDataFormat(DataFormatId = "6821", DataFormatVersion = "45957", ProcessingContext = FormLogicProcessingContext.Send)]
    public class SoknadOmSamtykkeSvarUtSendLogic : SvarUtSendLogic<no.kxml.skjema.dibk.arbeidstilsynetsSamtykke2.ArbeidstilsynetsSamtykkeType>
    {
        private readonly IValidationApiService _validationServiceHttpClient;
        private readonly MunicipalityService _municipalityService;

        public SoknadOmSamtykkeSvarUtSendLogic(IFormDataRepo repo,
                                               ILogger<SoknadOmSamtykkeSvarUtSendLogic> log,
                                               IBlobOperations blobOperations,
                                               ISvarUtAdapter svarUtAdapter,
                                               IDbUnitOfWork dbUnitOfWork,
                                               IDecryptionFactory decryptionFactory,
                                               IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                               IValidationApiService validationServiceHttpClient,
                                               MunicipalityService municipalityService,
                                               SvarUtShipmentBuilder shipmentBuilder,
                                               DistributionReceiverRepo distributionReceiverRepo,
                                               DistributionReceiverLogRepo distributionReceiverLogRepo)
            : base(repo,
                  log,
                  blobOperations,
                  svarUtAdapter,
                  dbUnitOfWork,
                  decryptionFactory,
                  fileDownloadHttpClient,
                  shipmentBuilder,
                  distributionReceiverRepo,
                  distributionReceiverLogRepo)
        {
            _validationServiceHttpClient = validationServiceHttpClient;
            _municipalityService = municipalityService;
        }

        public override async Task<ReportQueueItem> ExecuteAsync(SendQueueItem sendQueueItem)
        {
            var validationInputData = new ValidationInputData()
            {
                AuthenticatedSubmitter = this.AuthenticatedSubmitter,
                FormData = SerializeUtil.Serialize(FormData),
                Attachments = this.Attachments.Select(s => new AttachmentValidationInfo()
                {
                    AttachmentTypeName = s.AttachmentTypeName,
                    Filename = s.Filename,
                    FileSize = s.FileSizeMb
                }).ToList()
            };
            var validationResult = await _validationServiceHttpClient.ValidateReportAsync(validationInputData, CancellationToken.None);
            var validationIsOK = validationResult.Errors == 0;

            if (validationIsOK)
            {
                await UpdateFormDataBlobWithKommunensOrganisasjonnummerAsync(sendQueueItem.ArchiveReference.ToLower());
                await AddValidationReportToBlobStorage(sendQueueItem.ArchiveReference.ToLower(), validationResult);

                await base.ExecuteAsync(sendQueueItem);
            }
            else
            {
                DbUnitOfWork.LogEntries.AddInfo($"Feil i datavalidering. Innsending avbrutt. Feilmeldinger: {validationResult.GetValidationMessages()}");
                await UpdateFormProcessStatus(FormMetadataStatus.FeilDatavaliering);

                //Set status for the submittal to error
            }

            return null;
        }

        public override Task SetSenderAsync()
        {
            Sender = new ShipmentActor(SendQueueItem.Sender)
            {
                Address = FormData.ansvarligSoeker.adresse.adresselinje1,
                PostalCode = FormData.ansvarligSoeker.adresse.postnr,
                Poststed = FormData.ansvarligSoeker.adresse.poststed
            };

            return Task.CompletedTask;
        }

        protected override ForsendelseDataBygg MapForsendelseData()
        {
            return new ForsendelseDataBygg(this.ArchiveReference.ToUpper())
            {
                Adresselinje1 = FormData.eiendomByggested[0].adresse.adresselinje1,
                Adresselinje2 = FormData.eiendomByggested[0].adresse.adresselinje2,
                Adresselinje3 = FormData.eiendomByggested[0].adresse.adresselinje3,
                Landkode = FormData.eiendomByggested[0].adresse.landkode,
                Postnr = FormData.eiendomByggested[0].adresse.postnr,
                Poststed = FormData.eiendomByggested[0].adresse.poststed,

                Bolignummer = FormData.eiendomByggested[0].bolignummer,
                Bygningsnummer = FormData.eiendomByggested[0].bygningsnummer,
                Gårdsnummer = FormData.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer,
                Seksjonsnummer = FormData.eiendomByggested[0].eiendomsidentifikasjon.seksjonsnummer,
                Bruksnummer = FormData.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer,
                Festenummer = FormData.eiendomByggested[0].eiendomsidentifikasjon.festenummer,
                Kommunenummer = FormData.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer,

                SøknadSkjemaNavn = "Søknad om arbeidstilsynets samtykke",
                AvgivendeSystem = FormData.metadata.fraSluttbrukersystem,
            };
        }

        private async Task AddValidationReportToBlobStorage(string container, ValidationResponse validationResult)
        {
            var validationResultAsXml = SerializeUtil.SerializeWithNoWhitespaces(validationResult);

            var metadataList = new List<KeyValuePair<string, string>>();
            metadataList.Add(new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.ValidationResult));
            var validationResultAsBytes = Encoding.UTF8.GetBytes(validationResultAsXml);
            await BlobOperations.AddBytesToBlobStorageAsync(BlobStorageEnum.Private, container,
                                                            "valideringsrapportFraFellestjenesterBygg.xml", validationResultAsBytes, "application/xml", metadataList);
        }

        private async Task UpdateFormDataBlobWithKommunensOrganisasjonnummerAsync(string containerName)
        {
            //Add "kommunensOrganisasjonnummer" to the xml metadata
            //1. Get the name of the FormData blob (type = FormData)
            //2. Get the municipality org.no
            //3. Update metadata/kommunensOrganisasjonnummer with value from FtBMunicipalityAPI
            //4. Serialize and persist the FormDataXML to BlobStorage
            var metadataFilterMainBlobs = new List<KeyValuePair<string, string>>();
            metadataFilterMainBlobs.Add(new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.FormData));
            var formBlob = await BlobOperations.GetAllBlobsByFilterAsync(BlobStorageEnum.Private, containerName, metadataFilterMainBlobs);
            var formBlobFound = formBlob.First();
            if (formBlobFound != null)
            {
                var formBlobName = formBlobFound.FileName;

                var municipality = await _municipalityService.GetMunicipalityOrgnr(this.FormData.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer);
                FormData.metadata.kommunensOrganisasjonsnummer = municipality.OrganizationNumber;
                var xmlWithKommunensOrganisasjonsnummer = SerializeUtil.SerializeWithNoWhitespaces(FormData);

                var metadataListFormData = new List<KeyValuePair<string, string>>();
                metadataListFormData.Add(new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.FormData));
                var xmlWithKommunensOrganisasjonsnummerAsBytes = Encoding.UTF8.GetBytes(xmlWithKommunensOrganisasjonsnummer);
                await BlobOperations.AddBytesToBlobStorageAsync(BlobStorageEnum.Private, containerName,
                                                                formBlobName, xmlWithKommunensOrganisasjonsnummerAsBytes, "application/xml", metadataListFormData);
            }
            else
            {
                throw new Exception($"FormData blob not found in container {containerName}");//   FileNotFoundException("");
            }
        }
    }
}