﻿using Dibk.Ftpb.Integration.SvarUt.Builders.Models;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_FormLogic.Shipments.SvarUt;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace FtB_FormLogic.Shipments.ShipmentFormLogic.SuppleringAvSoeknad
{
    [FormDataFormat(DataFormatId = "6962", DataFormatVersion = "42350", ProcessingContext = FormLogicProcessingContext.Send)]
    public class SuppleringAvSoeknadSvarUtSendLogic : SvarUtSendLogic<no.kxml.skjema.dibk.suppleringAvSoknad.SuppleringAvSoeknadType>
    {
        public SuppleringAvSoeknadSvarUtSendLogic(IFormDataRepo repo,
                                                  ILogger<SuppleringAvSoeknadSvarUtSendLogic> log,
                                                  IBlobOperations blobOperations,
                                                  ISvarUtAdapter svarUtAdapter,
                                                  IDbUnitOfWork dbUnitOfWork,
                                                  IDecryptionFactory decryptionFactory,
                                                  IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                                  SvarUtShipmentBuilder shipmentBuilder,
                                                  DistributionReceiverRepo distributionReceiverRepo,
                                                  DistributionReceiverLogRepo distributionReceiverLogRepo)
            : base(repo,
                  log,
                  blobOperations,
                  svarUtAdapter,
                  dbUnitOfWork,
                  decryptionFactory,
                  fileDownloadHttpClient,
                  shipmentBuilder,
                  distributionReceiverRepo,
                  distributionReceiverLogRepo)
        { }

        public override async Task<ReportQueueItem> ExecuteAsync(SendQueueItem sendQueueItem)
        {
            Logger.LogInformation("Supplering er klar for forsendelse til SvarUt");
            DbUnitOfWork.LogEntries.AddInfo("Supplering behandles for utsendelse til SvarUt");

            await base.ExecuteAsync(sendQueueItem);
            return null;
        }

        protected override ForsendelseDataBygg MapForsendelseData()
        {
            return new ForsendelseDataBygg(this.ArchiveReference.ToUpper())
            {
                Adresselinje1 = FormData.eiendomByggested[0].adresse.adresselinje1,
                Adresselinje2 = FormData.eiendomByggested[0].adresse.adresselinje2,
                Adresselinje3 = FormData.eiendomByggested[0].adresse.adresselinje3,
                Landkode = FormData.eiendomByggested[0].adresse.landkode,
                Postnr = FormData.eiendomByggested[0].adresse.postnr,
                Poststed = FormData.eiendomByggested[0].adresse.poststed,

                Bolignummer = FormData.eiendomByggested[0].bolignummer,
                Bygningsnummer = FormData.eiendomByggested[0].bygningsnummer,
                Gårdsnummer = FormData.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer,
                Seksjonsnummer = FormData.eiendomByggested[0].eiendomsidentifikasjon.seksjonsnummer,
                Bruksnummer = FormData.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer,
                Festenummer = FormData.eiendomByggested[0].eiendomsidentifikasjon.festenummer,
                Kommunenummer = FormData.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer,

                KommunensSaksnummerSekvensnummer = FormData.kommunensSaksnummer.sakssekvensnummer,
                KommunensSaksnummerÅr = FormData.kommunensSaksnummer.saksaar,

                SøknadSkjemaNavn = "Supplering av søknad",
                AvgivendeSystem = FormData.metadata.fraSluttbrukersystem,
            };
        }

        public override Task SetSenderAsync()
        {
            Sender = new ShipmentActor(SendQueueItem.Sender)
            {
                Address = FormData.ansvarligSoeker.adresse.adresselinje1,
                PostalCode = FormData.ansvarligSoeker.adresse.postnr,
                Poststed = FormData.ansvarligSoeker.adresse.poststed
            };

            return Task.CompletedTask;
        }
    }
}