﻿using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtB_FormLogic.Shipments.ShipmentFormLogic.SuppleringAvSoeknad
{
    [FormDataFormat(DataFormatId = "6962", DataFormatVersion = "46471", ProcessingContext = FormLogicProcessingContext.Prepare)]
    public class SuppleringAvSoeknadPrepareLogic : ShipmentPrepareLogic<no.kxml.skjema.dibk.suppleringAvSoknad.SuppleringAvSoeknadType>
    {
        private readonly MunicipalityService _municipalityService;

        public SuppleringAvSoeknadPrepareLogic(IFormDataRepo repo,
                                               ILogger<SuppleringAvSoeknadPrepareLogic> log,
                                               IBlobOperations blobOperations,
                                               IDbUnitOfWork dbUnitOfWork,
                                               IDecryptionFactory decryptionFactory,
                                               IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                               MunicipalityService municipalityService,
                                               DistributionReceiverRepo distributionReceiverRepo,
                                               DistributionReceiverLogRepo distributionReceiverLogRepo)
            : base(repo,
                  log,
                  blobOperations,
                  dbUnitOfWork,
                  decryptionFactory,
                  fileDownloadHttpClient,
                  distributionReceiverRepo,
                  distributionReceiverLogRepo)
        {
            _municipalityService = municipalityService;
        }

        public override Task<IEnumerable<SendQueueItem>> ExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {
            Logger.LogInformation("Supplering av søknad mottatt av CoreFunctions");

            return base.ExecuteAsync(submittalQueueItem);
        }

        public override async Task SetReceiversAsync()
        {
            var receivers = new List<Receiver>();

            var eiendomKommunenummer = this.FormData.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer;
            var municipality = await _municipalityService.GetMunicipalityOrgnr(eiendomKommunenummer, FtPBContext.Bygg);

            var kommunensOrgnr = municipality.OrganizationNumber;
            var kommuneNavn = municipality.Name;

            receivers.Add(new Receiver { Type = ActorTypeEnum.OffentligMyndighet, Id = kommunensOrgnr, Name = kommuneNavn });

            base.SetReceivers(receivers);
        }

        public override async Task SetSenderAsync()
        {
            var id = string.Empty;

            var encryptedReporteeId = await BlobOperations.GetReporteeIdFromStoredBlobAsync(ArchiveReference);
            base.Sender = GetSenderCandidates(DecryptionFactory.GetDecryptor(), base.FormData).FirstOrDefault(r => r.Id.Equals(DecryptionFactory.GetDecryptor().DecryptText(encryptedReporteeId)));

            if (Sender == null)
                DbUnitOfWork.LogEntries.AddError("Søker angitt i skjema er ikke samme som avsender (pålogget bruker) i Altinn.");
        }

        private static List<Actor> GetSenderCandidates(IDecryption decryptor, no.kxml.skjema.dibk.suppleringAvSoknad.SuppleringAvSoeknadType supplering)
        {
            var retval = new List<Actor>();
            if (!string.IsNullOrEmpty(supplering.tiltakshaver?.organisasjonsnummer))
                retval.Add(new Actor() { Id = supplering.tiltakshaver.organisasjonsnummer, Type = GetActorType(supplering.tiltakshaver.partstype.kodeverdi), Name = supplering.tiltakshaver.navn });

            if (!string.IsNullOrEmpty(supplering.tiltakshaver?.foedselsnummer))
                retval.Add(new Actor() { Id = decryptor.DecryptText(supplering.tiltakshaver.foedselsnummer), Type = ActorTypeEnum.Privatperson, Name = supplering.tiltakshaver.navn });

            if (!string.IsNullOrEmpty(supplering.ansvarligSoeker?.organisasjonsnummer))
                retval.Add(new Actor() { Id = supplering.ansvarligSoeker.organisasjonsnummer, Type = GetActorType(supplering.ansvarligSoeker.partstype.kodeverdi), Name = supplering.ansvarligSoeker.navn });

            if (!string.IsNullOrEmpty(supplering.ansvarligSoeker?.foedselsnummer))
                retval.Add(new Actor() { Id = decryptor.DecryptText(supplering.ansvarligSoeker.foedselsnummer), Type = ActorTypeEnum.Privatperson, Name = supplering.ansvarligSoeker.navn });

            return retval;
        }

        private static ActorTypeEnum GetActorType(string partsTypeKodeVerdi)
        {
            Enum.TryParse(EnumExtentions.GetValueFromDescription<ActorTypeEnum>(partsTypeKodeVerdi).ToString(), out ActorTypeEnum receiverType);
            return receiverType;
        }
    }
}