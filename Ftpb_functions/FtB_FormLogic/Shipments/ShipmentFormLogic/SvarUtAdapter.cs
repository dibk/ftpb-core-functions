﻿using Dibk.Ftpb.Integration.SvarUt;
using Dibk.Ftpb.Integration.SvarUt.Models;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    public class SvarUtAdapter : ISvarUtAdapter
    {
        private readonly ILogger<SvarUtAdapter> _logger;
        private readonly ISvarUtClient _svarUtClient;

        public SvarUtAdapter(ILogger<SvarUtAdapter> logger, ISvarUtClient svarUtClient)
        {
            _logger = logger;
            _svarUtClient = svarUtClient;
        }

        public async Task<string> SendAsync(Forsendelse forsendelse)
        {
            _logger.LogInformation("Sender forsendelse til SvarUt");

            var f = await _svarUtClient.SendForsendelseAsync(forsendelse);

            return f.Id;
        }
    }
}
