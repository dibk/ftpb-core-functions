using Dibk.Ftpb.Common.Constants;
using Dibk.Ftpb.ModelToPdf.Common.Client;
using Dibk.Ftpb.ModelToPdf.Common.Models;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.FormDataRepositories;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_Common.Utils;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Ftb_Repositories.LogContstants;
using FtB_Validation;
using FtB_Validation.Models;
using FtB_Validation.Models.Legacy;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FtB_FormLogic.Shipments.ShipmentFormLogic.Ferdigattest
{
    [FormDataFormat(DataFormatId = "5855", DataFormatVersion = "43192", ProcessingContext = FormLogicProcessingContext.Prepare)]
    public class FerdigattestPrepareLogic : ShipmentPrepareLogic<no.kxml.skjema.dibk.ferdigattestV2.FerdigattestType>
    {
        private readonly IValidationApiService _validationApiService;
        private readonly IDatamodelToPdfHttpClient _datamodelToPdfHttpClient;
        private readonly MunicipalityService _municipalityService;

        public FerdigattestPrepareLogic(IFormDataRepo repo,
                                        ILogger<FerdigattestPrepareLogic> log,
                                        IBlobOperations blobOperations,
                                        IDbUnitOfWork dbUnitOfWork,
                                        IDecryptionFactory decryptionFactory,
                                        IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                        IValidationApiService validationApiService,
                                        IDatamodelToPdfHttpClient datamodelToPdfHttpClient,
                                        MunicipalityService municipalityService,
                                        DistributionReceiverRepo distributionReceiverRepo,
                                        DistributionReceiverLogRepo distributionReceiverLogRepo)
            : base(repo,
                  log,
                  blobOperations,
                  dbUnitOfWork,
                  decryptionFactory,
                  fileDownloadHttpClient,
                  distributionReceiverRepo,
                  distributionReceiverLogRepo)
        {
            _validationApiService = validationApiService;
            _datamodelToPdfHttpClient = datamodelToPdfHttpClient;
            _municipalityService = municipalityService;
        }

        public override async Task<IEnumerable<SendQueueItem>> ExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {
            var result = await base.ExecuteAsync(submittalQueueItem);

            var mainformValidationResult = await ValidateMainform(Attachments, Subforms);

            if (result.Any() && result.First().Sender == null)
                mainformValidationResult.Messages.Add(new FtB_Validation.Models.Legacy.ValidationMessage()
                {
                    Message = "Søker angitt i skjema er ikke samme som avsender(pålogget bruker) i Altinn.",
                    Messagetype = "Error",
                    Reference = "Submitter"
                });
            else if (result.Any() && result.First().Receiver == null)
                mainformValidationResult.Messages.Add(new FtB_Validation.Models.Legacy.ValidationMessage()
                {
                    Message = "Klarer ikke finne korrekt avsender i skjema.",
                    Messagetype = "Error",
                    Reference = "Submitter"
                });

            if (mainformValidationResult.Messages.Count > 0) DbUnitOfWork.LogEntries.AddInfo($"Valideringsresultat: {mainformValidationResult.GetValidationMessages()}");

            if (mainformValidationResult.Messages.Where(p => p.Messagetype.Equals("Error", StringComparison.OrdinalIgnoreCase)).Count() > 0)
            {
                DbUnitOfWork.LogEntries.AddError($"Feil i datavalidering. Innsending avbrutt.");
                var formMetadata = await DbUnitOfWork.FormMetadata.Get();
                formMetadata.ValidationErrors = mainformValidationResult.Errors;
                formMetadata.ValidationWarnings = mainformValidationResult.Warnings;
                await UpdateFormProcessStatus(FormMetadataStatus.FeilDatavaliering);
                return null;
            }
            else
                DbUnitOfWork.LogEntries.AddInfo("Hovedskjema validert OK");

            try
            {
                var sluttrapportForBygningsavfallXml = await FormDataRepo.GetSubformContent(ArchiveReference, "Sluttrapport for bygningsavfall.xml");
                if (!string.IsNullOrEmpty(sluttrapportForBygningsavfallXml))
                {
                    var sluttrapportValidationResult = await ValidateSluttrapportForAvfallsplan(sluttrapportForBygningsavfallXml, Attachments);

                    if (sluttrapportValidationResult.Messages.Count > 0) DbUnitOfWork.LogEntries.AddInfo($"Valideringsresultat for sluttrapport for bygningsavfall: {sluttrapportValidationResult.GetValidationMessages()}");

                    if (sluttrapportValidationResult.Errors > 0)
                    {
                        DbUnitOfWork.LogEntries.AddError($"Feil i datavalidering for sluttrapport for bygningsavfall. Innsending avbrutt.");
                        await UpdateFormProcessStatus(FormMetadataStatus.FeilDatavaliering);
                        return null;
                    }
                    else
                    {
                        DbUnitOfWork.LogEntries.AddInfo("Sluttrapport for bygningsavfall validert OK");
                        await CreateSluttrapportForAvfallsplanPdf(sluttrapportForBygningsavfallXml);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "An error occurred when handling Sluttrapport for bygningsavfall validation/pdf-generation");
                throw;
            }

            return result;
        }

        public override async Task PostExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {
            await DbUnitOfWork.SaveLogEntries();
            await base.PostExecuteAsync(submittalQueueItem);
        }

        private async Task UpdateFormProcessStatus(string status)
        {
            var formMetadata = await DbUnitOfWork.FormMetadata.Get();
            formMetadata.Status = status;
            DbUnitOfWork.FormMetadata.Update(formMetadata);
            await DbUnitOfWork.FormMetadata.Save();
        }

        private async Task<LegacyValidationResponse> ValidateMainform(List<AttachmentInfo> attachments, List<SubformInfo> subforms)
        {
            var formData = await FormDataRepo.GetFormData(ArchiveReference);
            var dataFormatId = await BlobOperations.GetFormatIdFromStoredBlobAsync(ArchiveReference);
            var dataFormatVersion = await BlobOperations.GetFormatVersionIdFromStoredBlobAsync(ArchiveReference);

            var legacyValidationInputData = new LegacyValidationInputData()
            {
                FormData = formData,
                DataFormatId = dataFormatId,
                DataFormatVersion = dataFormatVersion.ToString(),
                AttachmentTypesAndForms = new List<LegacyAttachmentInfo>()
            };
            legacyValidationInputData.AttachmentTypesAndForms.AddRange(attachments.Select(s => new LegacyAttachmentInfo() { Filename = s.Filename, FileSize = (int)s.FileSizeMb, Name = s.AttachmentTypeName }).ToList());
            legacyValidationInputData.AttachmentTypesAndForms.AddRange(subforms.Select(s => new LegacyAttachmentInfo() { Filename = s.Filename, FileSize = (int)s.FileSizeMb, Name = s.SubformName }).ToList());

            var mainFormValidationResult = await _validationApiService.ValidateAsync(legacyValidationInputData, CancellationToken.None);
            var serializedMainFormValidationResult = SerializeUtil.Serialize(mainFormValidationResult);

            //Lagre til blob
            await BlobOperations.AddBytesToBlobStorageAsync(BlobStorageEnum.Private,
                                                  ArchiveReference,
                                                  "valideringsrapport.xml",
                                                  System.Text.Encoding.UTF8.GetBytes(serializedMainFormValidationResult),
                                                  "application/xml",
                                                  new List<KeyValuePair<string, string>>()
                                                  {
                                                      BlobMetadata.BlobType(BlobStorageMetadataTypes.ValidationResult),
                                                      BlobMetadata.AttachmentTypeName(DataTypes.Valideringsrapport)
                                                  });
            return mainFormValidationResult;
        }

        private async Task<ValidationResponse> ValidateSluttrapportForAvfallsplan(string avfallsplanXmlString, List<AttachmentInfo> attachments)
        {
            var validationInputData = new ValidationInputData()
            {
                FormData = avfallsplanXmlString,
                Attachments = attachments.Select(s => new AttachmentValidationInfo()
                {
                    AttachmentTypeName = s.AttachmentTypeName,
                    Filename = s.Filename,
                    FileSize = s.FileSizeMb
                }).ToList()
            };

            var avfallsplanValidationResult = await _validationApiService.ValidateReportAsync(validationInputData, CancellationToken.None);
            var serializedAvfallsplanValidationResult = SerializeUtil.Serialize(avfallsplanValidationResult);

            await BlobOperations.AddBytesToBlobStorageAsync(BlobStorageEnum.Private,
                                                  ArchiveReference,
                                                  "valideringsrapport-sluttrapport for bygningsavfall.xml",
                                                  System.Text.Encoding.UTF8.GetBytes(serializedAvfallsplanValidationResult),
                                                  "application/xml",
                                                  new List<KeyValuePair<string, string>>()
                                                  {
                                                      BlobMetadata.BlobType(BlobStorageMetadataTypes.ValidationResult),
                                                      BlobMetadata.AttachmentTypeName(DataTypes.ValideringsrapportSluttrapportBygningsavfall)
                                                  });
            return avfallsplanValidationResult;
        }

        private async Task CreateSluttrapportForAvfallsplanPdf(string avfallsplanXml)
        {
            var modelToPdfRequest = CreateModelToPdfRequest(avfallsplanXml);

            await using var pdfStream = await _datamodelToPdfHttpClient.CreatePdfAsync(modelToPdfRequest, CancellationToken.None);

            //Sluttrapport for bygningsavfall
            await BlobOperations.AddStreamToBlobStorageAsync(
                BlobStorageEnum.Private,
                ArchiveReference,
                "Ftpb-Generert_Sluttrapport for bygningsavfall.pdf",
                pdfStream,
                "application/pdf",
                new List<KeyValuePair<string, string>>() {
                    BlobMetadata.BlobType(BlobStorageMetadataTypes.GeneratedAttachment) ,
                    BlobMetadata.AttachmentTypeName("SluttrapportForBygningsavfall"),
                    new("SvarUtFilename","Sluttrapport for bygningsavfall.pdf"),
                });
        }

        private ModelToPdfRequest CreateModelToPdfRequest(string avfallsplanXml)
        {
            var modelToPdfRequest = new ModelToPdfRequest
            {
                Type = DataModelTypes.SluttrapportForBygningsavfall,
                ContentString = avfallsplanXml,
                ArchiveReference = ArchiveReference,
                PdfType = "Signed",
            };
            return modelToPdfRequest;
        }

        public override async Task SetReceiversAsync()
        {
            var receivers = new List<Receiver>();

            var municipality = await _municipalityService.GetMunicipalityOrgnr(this.FormData.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer);

            var kommunensOrgnr = municipality.OrganizationNumber;
            var kommuneNavn = municipality.Name;

            receivers.Add(new Receiver { Type = ActorTypeEnum.OffentligMyndighet, Id = kommunensOrgnr, Name = kommuneNavn });

            base.SetReceivers(receivers);
        }

        public override async Task SetSenderAsync()
        {
            var id = string.Empty;

            var encryptedReporteeId = await BlobOperations.GetReporteeIdFromStoredBlobAsync(ArchiveReference);
            base.Sender = GetSenderCandidates(DecryptionFactory.GetDecryptor(), base.FormData).FirstOrDefault(r => r.Id.Equals(DecryptionFactory.GetDecryptor().DecryptText(encryptedReporteeId)));

            if (Sender == null)
                DbUnitOfWork.LogEntries.AddError("Søker angitt i skjema er ikke samme som avsender (pålogget bruker) i Altinn.");
        }

        private static List<Actor> GetSenderCandidates(IDecryption decryptor, no.kxml.skjema.dibk.ferdigattestV2.FerdigattestType ferdigattest)
        {
            var retval = new List<Actor>();
            if (!string.IsNullOrEmpty(ferdigattest.tiltakshaver?.organisasjonsnummer))
                retval.Add(new Actor() { Id = ferdigattest.tiltakshaver.organisasjonsnummer, Type = GetActorType(ferdigattest.tiltakshaver.partstype.kodeverdi), Name = ferdigattest.tiltakshaver.navn });

            if (!string.IsNullOrEmpty(ferdigattest.tiltakshaver?.foedselsnummer))
                retval.Add(new Actor() { Id = decryptor.DecryptText(ferdigattest.tiltakshaver.foedselsnummer), Type = ActorTypeEnum.Privatperson, Name = ferdigattest.tiltakshaver.navn });

            if (!string.IsNullOrEmpty(ferdigattest.ansvarligSoeker?.organisasjonsnummer))
                retval.Add(new Actor() { Id = ferdigattest.ansvarligSoeker.organisasjonsnummer, Type = GetActorType(ferdigattest.ansvarligSoeker.partstype.kodeverdi), Name = ferdigattest.ansvarligSoeker.navn });

            if (!string.IsNullOrEmpty(ferdigattest.ansvarligSoeker?.foedselsnummer))
                retval.Add(new Actor() { Id = decryptor.DecryptText(ferdigattest.ansvarligSoeker.foedselsnummer), Type = ActorTypeEnum.Privatperson, Name = ferdigattest.ansvarligSoeker.navn });

            return retval;
        }

        private static ActorTypeEnum GetActorType(string partsTypeKodeVerdi)
        {
            Enum.TryParse(EnumExtentions.GetValueFromDescription<ActorTypeEnum>(partsTypeKodeVerdi).ToString(), out ActorTypeEnum receiverType);
            return receiverType;
        }
    }
}