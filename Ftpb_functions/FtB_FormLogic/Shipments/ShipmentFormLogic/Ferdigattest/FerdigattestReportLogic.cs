﻿using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Utils;
using Ftb_Repositories;
using Microsoft.Extensions.Logging;
using no.kxml.skjema.dibk.ferdigattestV2;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FtB_FormLogic.Shipments.ShipmentFormLogic.Ferdigattest
{
    [FormDataFormat(DataFormatId = "5855", DataFormatVersion = "43192", ProcessingContext = FormLogicProcessingContext.Report)]
    public class FerdigattestReportLogic : IFormLogic<string, ReportQueueItem>
    {
        private const string ssbContainerName = "ssb-statistics";
        protected string XmlData;
        public string ArchiveReference { get; set; }
        protected readonly IFormDataRepo FormDataRepo;
        private readonly ILogger<FerdigattestReportLogic> _logger;
        private readonly IDbUnitOfWork _dbUnitOfWork;
        private readonly IBlobOperations _blobOperations;

        public FerdigattestType FormData { get; set; }
        public ReportQueueItem ReportQueueItem { get; set; }

        public FerdigattestReportLogic(ILogger<FerdigattestReportLogic> logger,
                                       IDbUnitOfWork dbUnitOfWork,
                                       IBlobOperations blobOperations,
                                       IFormDataRepo formDataRepo)
        {
            _logger = logger;
            _dbUnitOfWork = dbUnitOfWork;
            _blobOperations = blobOperations;
            FormDataRepo = formDataRepo;
        }

        public async Task LoadDataAsync(string archiveReference)
        {
            ArchiveReference = archiveReference;
            var data = await FormDataRepo.GetFormData(ArchiveReference);
            XmlData = data;
            FormData = SerializeUtil.DeserializeXml<FerdigattestType>(data);
        }

        public async Task<string> ExecuteAsync(ReportQueueItem input)
        {
            await SaveSubformSluttrapportForBygningsavfall();
            await _dbUnitOfWork.SaveLogEntries();

            return null;
        }

        private async Task SaveSubformSluttrapportForBygningsavfall()
        {
            //Sjekk for sluttrapport for bygningsavfall som underskjema
            var subforms = await _blobOperations.GetAllBlobsContainingFilterAsync(FtB_Common.Enums.BlobStorageEnum.Private, ArchiveReference, new List<KeyValuePair<string, string>>
            {
                BlobMetadata.AttachmentTypeName("SluttrapportForBygningsavfall"),
                BlobMetadata.BlobType(FtB_Common.Storage.Metadata.BlobStorageMetadataTypes.Subform)
            });
            var sluttrapportSubForm = subforms.SingleOrDefault(p => p.MimeType == "application/xml");

            if (sluttrapportSubForm != null)
            {
                _logger.LogDebug("Subform 'SluttrapportForBygningsavfall' found, anonymizes and saves it.");
                var subformName = sluttrapportSubForm.Metadata.FirstOrDefault(p => p.Key.Equals("subformname", StringComparison.OrdinalIgnoreCase));
                _dbUnitOfWork.LogEntries.AddInfoInternal($"Anonymiserer og tilgjengeliggjør underskjema '{subformName.Value}' for statistikk", "STATISTIC_REPORTING");
                var sluttrapportForBygningsavfallXml = await _blobOperations.GetBlobStreamAsync(FtB_Common.Enums.BlobStorageEnum.Private, ArchiveReference, sluttrapportSubForm.FileName);

                string subformContent;
                using (var streamReader = new StreamReader(sluttrapportForBygningsavfallXml.Content))
                    subformContent = await streamReader.ReadToEndAsync();

                //Anonymisér data og lagre anonymisert struktur
                var anonymisertSluttrapport = AnonymizeSluttrapport(subformContent);

                var ids = GetFtbIdAndVersjonsId(subformContent);

                //filnavn "ftbid - v{versjonsnummerAvfallsplan} - {archivereference} {sluttrapportSubForm.FileName}"
                var filenameBuilder = new StringBuilder();
                if (!string.IsNullOrEmpty(ids.ftbid) && ids.ftbid.Length > 0)
                    filenameBuilder.Append($"{ids.ftbid} - ");

                if (!string.IsNullOrEmpty(ids.versjonsid) && ids.versjonsid.Length > 0)
                    filenameBuilder.Append($"v{ids.versjonsid} - ");

                filenameBuilder.Append($"{ArchiveReference} {sluttrapportSubForm.FileName}");

                var xmlFilename = $@"{DateTime.Today.Year}/xml/{filenameBuilder.ToString()}";
                _logger.LogDebug($"Saves subform {xmlFilename} for statistics");

                await _blobOperations.AddBytesToBlobStorageAsync(FtB_Common.Enums.BlobStorageEnum.Private,
                                                            ssbContainerName,
                                                            xmlFilename,
                                                            Encoding.UTF8.GetBytes(anonymisertSluttrapport),
                                                            "application/xml",
                                                            null);

                _logger.LogDebug("Anonymized subform 'SluttrapportForBygningsavfall' saved");
            }
            else
                _logger.LogDebug("Subform of type 'SluttrapportForBygningsavfall' not found");
        }

        private (string ftbid, string versjonsid) GetFtbIdAndVersjonsId(string subFormContent)
        {
            if (subFormContent == null)
                throw new ArgumentNullException(subFormContent);

            var sluttrapport = SerializeUtil.DeserializeXml<no.kxml.skjema.dibk.avfallsplanV2.AvfallsplanType>(subFormContent);

            if (sluttrapport == null)
                throw new ArgumentException("Unable to deserialize to type SluttrapportType");

            return (sluttrapport.metadata?.ftbId, sluttrapport.metadata?.versjonsnummerAvfallsplan);
        }

        /*
            Sluttrapport for bygningsavfall som vedlegg skal ikke tilgjengeliggjøres for SSB.
            Derfor er dette kommentert ut.
         */
        //private async Task SaveAttachmentTypeSluttrapportForBygningsavfall()
        //{
        //    //Sjekk for sluttrapport for bygningsavfall som vedlegg
        //    var attachments = await _blobOperations.GetAllBlobsContainingFilterAsync(FtB_Common.Enums.BlobStorageEnum.Private, ArchiveReference, new List<KeyValuePair<string, string>>
        //    {
        //        BlobMetadata.AttachmentTypeName("SluttrapportForBygningsavfall"),
        //        BlobMetadata.BlobType(FtB_Common.Enums.BlobStorageMetadataTypeEnum.SubmittalAttachment)
        //    });

        //    if (attachments != null || attachments.Count() > 0)
        //    {
        //        foreach (var sluttrapportPdf in attachments)
        //        {
        //            _logger.LogDebug($"Attachment '{sluttrapportPdf.FileName}' found");
        //            _dbUnitOfWork.LogEntries.AddInfoInternal($"Tilgjengeliggjør vedlegg '{sluttrapportPdf.FileName}' for statistikk", "STATISTIC_REPORTING");
        //            var sluttrapportBlob = await _blobOperations.GetBlobContentAsync(FtB_Common.Enums.BlobStorageEnum.Private, ArchiveReference, sluttrapportPdf.FileName);

        //            var memoryStream = new MemoryStream();
        //            await sluttrapportBlob.Content.CopyToAsync(memoryStream);
        //            memoryStream.Position = 0;

        //            //Lagre til blob
        //            var pdfFilename = $@"{DateTime.Today.Year}/pdf/{ArchiveReference} {sluttrapportPdf.FileName}";
        //            _logger.LogDebug($"Saves attachment {pdfFilename} for statistics");
        //            await _blobOperations.AddStreamToBlobStorageAsync(FtB_Common.Enums.BlobStorageEnum.Private,
        //                                                              ssbContainerName,
        //                                                              pdfFilename,
        //                                                              memoryStream,
        //                                                              sluttrapportBlob.MimeType,
        //                                                              sluttrapportBlob.Metadata);

        //            _logger.LogDebug($"Attachment '{sluttrapportPdf.FileName}' saved");
        //        }
        //    }
        //    else
        //        _logger.LogDebug("Attachment of type 'SluttrapportForBygningsavfall' not found");
        //}

        private string AnonymizeSluttrapport(string subFormContent)
        {
            if (subFormContent == null)
                throw new ArgumentNullException(subFormContent);

            var sluttrapport = SerializeUtil.DeserializeXml<no.kxml.skjema.dibk.avfallsplanV2.AvfallsplanType>(subFormContent);

            if (sluttrapport == null)
                throw new ArgumentException("Unable to deserialize to type SluttrapportType");

            sluttrapport.tiltakshaver = null;
            sluttrapport.ansvarligSoeker = null;
            sluttrapport.metadata.prosjektnavn = null;
            sluttrapport.metadata.prosjektnr = null;
            sluttrapport.metadata.fraSluttbrukersystem = null;
            sluttrapport.metadata.sluttbrukersystemUrl = null;
            sluttrapport.signatur = null;            

            return SerializeUtil.SerializeWithNoWhitespaces(sluttrapport);
        }

        public virtual async Task PreExecuteAsync(ReportQueueItem reportQueueItem)
        {
            ReportQueueItem = reportQueueItem;
            ArchiveReference = reportQueueItem.ArchiveReference;

            _logger.LogDebug("Setting unitOfWork archiveReference");
            _dbUnitOfWork.SetArchiveReference(reportQueueItem.ArchiveReference);

            _logger.LogDebug("Loading formdata");
            await LoadDataAsync(reportQueueItem.ArchiveReference);
        }

        public virtual Task PostExecuteAsync(ReportQueueItem reportQueueItem)
        {
            _logger.LogDebug("Reportlogic executed");
            return Task.CompletedTask;
        }
    }
}