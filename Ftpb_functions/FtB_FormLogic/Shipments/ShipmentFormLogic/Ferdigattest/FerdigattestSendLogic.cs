﻿using FtB_Common.BusinessModels;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.Extensions;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_FormLogic.Shipments.SvarUt;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dibk.Ftpb.Integration.SvarUt.Builders.Models;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;

namespace FtB_FormLogic.Shipments.ShipmentFormLogic.Ferdigattest
{
    [FormDataFormat(DataFormatId = "5855", DataFormatVersion = "43192", ProcessingContext = FormLogicProcessingContext.Send)]
    public class FerdigattestSendLogic : SvarUtSendLogic<no.kxml.skjema.dibk.ferdigattestV2.FerdigattestType>
    {
        public FerdigattestSendLogic(IFormDataRepo repo,
                                     ILogger<FerdigattestSendLogic> log,
                                     IBlobOperations blobOperations,
                                     ISvarUtAdapter svarUtAdapter,
                                     IDbUnitOfWork dbUnitOfWork,
                                     IDecryptionFactory decryptionFactory,
                                     IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                     SvarUtShipmentBuilder shipmentBuilder,
                                     DistributionReceiverRepo distributionReceiverRepo,
                                     DistributionReceiverLogRepo distributionReceiverLogRepo)
            : base(repo,
                  log,
                  blobOperations,
                  svarUtAdapter,
                  dbUnitOfWork,
                  decryptionFactory,
                  fileDownloadHttpClient,
                  shipmentBuilder,
                  distributionReceiverRepo,
                  distributionReceiverLogRepo)
        { }

        public override async Task<ReportQueueItem> ExecuteAsync(SendQueueItem sendQueueItem)
        {
            var reportQueueItem = await base.ExecuteAsync(sendQueueItem);

            var blobs = await BlobOperations.GetAllBlobsContainingFilterAsync(
                FtB_Common.Enums.BlobStorageEnum.Private,
                this.ArchiveReference,
                new List<KeyValuePair<string, string>>() {
                    BlobMetadata.AttachmentTypeName("SluttrapportForBygningsavfall"),
                    BlobMetadata.BlobType(BlobStorageMetadataTypes.GeneratedAttachment),
                });

            if (blobs != null && blobs.Count() > 0)
            {
                Logger.LogInformation("'Sluttrapport for bygningsavfall' found. Persists it to Filedownloadstatus.");
                var generertSluttrapport = blobs.First();

                var containerId = Guid.NewGuid();
                Ftb_DbModels.FileDownloadStatus existingFileDownloadStatusRecord = null;

                var fds = await FileDownloadHttpClient.GetAllAsync(this.ArchiveReference);
                if (fds != null && fds.Count() > 0)
                {
                    existingFileDownloadStatusRecord = fds.FirstOrDefault(p => p.FileType == FileTypesForDownloadEnum.SluttrapportForBygningsavfall
                                                            && p.MimeType.Equals(generertSluttrapport.MimeType, StringComparison.OrdinalIgnoreCase));

                    if (existingFileDownloadStatusRecord != null)
                        containerId = existingFileDownloadStatusRecord.Guid;
                }

                var blobUrl = await BlobOperations.AddStreamToBlobStorageAsync(
                    BlobStorageEnum.Private,
                    containerId.ToString(),
                    generertSluttrapport.Metadata.SvarUtFilename(),
                    generertSluttrapport.Content,
                    null);

                await FileDownloadHttpClient.PostAsync(this.ArchiveReference, new Ftb_DbModels.FileDownloadStatus()
                {
                    ArchiveReference = this.ArchiveReference,
                    Guid = containerId,
                    BlobLink = blobUrl,
                    Filename = generertSluttrapport.FileName,
                    FileType = FileTypesForDownloadEnum.SluttrapportForBygningsavfall,
                    MimeType = generertSluttrapport.MimeType,
                    TimeReceived = DateTime.Now,
                    FormName = "Ferdigattest",
                    IsDeleted = false,
                    FileAccessCount = 0,
                });
            }

            return reportQueueItem;
        }

        public override Task SetSenderAsync()
        {
            Sender = new ShipmentActor(SendQueueItem.Sender)
            {
                Address = FormData.ansvarligSoeker.adresse.adresselinje1,
                PostalCode = FormData.ansvarligSoeker.adresse.postnr,
                Poststed = FormData.ansvarligSoeker.adresse.poststed
            };

            return Task.CompletedTask;
        }

        protected override ForsendelseDataBygg MapForsendelseData()
        {
            return new ForsendelseDataBygg(SendQueueItem.ArchiveReference.ToUpper())
            {
                Kommunenummer = FormData.eiendomByggested[0].eiendomsidentifikasjon.kommunenummer,
                Gårdsnummer = FormData.eiendomByggested[0].eiendomsidentifikasjon.gaardsnummer,
                Bruksnummer = FormData.eiendomByggested[0].eiendomsidentifikasjon.bruksnummer,
                Festenummer = FormData.eiendomByggested[0].eiendomsidentifikasjon.festenummer,
                Seksjonsnummer = FormData.eiendomByggested[0].eiendomsidentifikasjon.seksjonsnummer,
                Adresselinje1 = FormData.eiendomByggested[0].adresse.adresselinje1,
                Adresselinje2 = FormData.eiendomByggested[0].adresse.adresselinje2,
                Adresselinje3 = FormData.eiendomByggested[0].adresse.adresselinje3,
                Postnr = FormData.eiendomByggested[0].adresse.postnr,
                Poststed = FormData.eiendomByggested[0].adresse.poststed,
                Landkode = FormData.eiendomByggested[0].adresse.landkode,
                Bygningsnummer = FormData.eiendomByggested[0].bygningsnummer,
                Bolignummer = FormData.eiendomByggested[0].bolignummer,
                SøknadSkjemaNavn = "Søknad om ferdigattest",
                AvgivendeSystem = FormData.fraSluttbrukersystem,
                KommunensSaksnummerÅr = FormData.kommunensSaksnummer.saksaar,
                KommunensSaksnummerSekvensnummer = FormData.kommunensSaksnummer.sakssekvensnummer
            };
        }
    }
}