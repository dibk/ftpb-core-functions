﻿using Altinn3.Adapters;
using Dibk.Ftpb.ModelToPdf.Common.Client;
using Dibk.Ftpb.ModelToPdf.Common.Models;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Constants;
using FtB_Common.Enums;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_FormLogic.Shipments.ShipmentBaseLogic;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Dibk.Ftpb.Common.Datamodels;

namespace FtB_FormLogic.Shipments.ShipmentFormLogic.InnsendingAvReguleringsplanforslag
{
    [FormDataFormat(
        DataFormatId = DataFormatIDs.InnsendingReguleringsplanforslag,
        DataFormatVersion = RegfoHelper.DataFormatVersion,
        ProcessingContext = FormLogicProcessingContext.Report
    )]
    public class InnsendingAvReguleringsplanforslagReportLogic : SvarUtReportLogic<OversendelseReguleringsplanforslag>
    {
        private readonly IDatamodelToPdfHttpClient _datamodelToPdfHttpClient;
        private readonly MunicipalityService _municipalityService;

        protected override bool CompleteAltinn3Instance => true;

        protected override StatusApiFileInfo MainFormDownloadFileInfo => new()
        {
            Name = Dibk.Ftpb.Common.Constants.DataTypes.OversendelseReguleringsplanforslag,
            FileName = $"{Dibk.Ftpb.Common.Constants.DataTypes.OversendelseReguleringsplanforslag}.pdf",
            FileType = FileTypesForDownloadEnum.InnsendingAvReguleringsplanforslag,
            MimeType = "application/pdf"
        };

        private readonly StatusApiFileInfo _receiptFileDownloadInfo = new()
        {
            FileName = $"{Dibk.Ftpb.Common.Constants.DataTypes.OversendelseReguleringsplanforslag}-receipt.pdf",
            FileType = FileTypesForDownloadEnum.InnsendingAvReguleringsplanforslagKvittering,
            MimeType = "application/pdf"
        };

        protected override StatusApiFileInfo ReceiptDownloadFileInfo => _receiptFileDownloadInfo;

        public InnsendingAvReguleringsplanforslagReportLogic(
            IDatamodelToPdfHttpClient datamodelToPdfHttpClient,
            MunicipalityService municipalityService,
            IBlobOperations blobOperations,
            IAltinnInstanceCompleter altinnInstanceCompleter,
            DistributionSubmittalRepo distributionSubmittalRepo,
            IFormDataRepo repo,
            ILogger<InnsendingAvReguleringsplanforslagReportLogic> log,
            IDbUnitOfWork dbUnitOfWork,
            IFileDownloadStatusHttpClient fileDownloadHttpClient,
            DistributionReceiverRepo distributionReceiverRepo,
            DistributionReceiverLogRepo distributionReceiverLogRepo
        ) : base(
            blobOperations,
            altinnInstanceCompleter,
            distributionSubmittalRepo,
            repo,
            log,
            dbUnitOfWork,
            fileDownloadHttpClient,
            distributionReceiverRepo,
            distributionReceiverLogRepo
        )
        {
            _datamodelToPdfHttpClient = datamodelToPdfHttpClient;
            _municipalityService = municipalityService;
        }

        public override async Task PreExecuteAsync(ReportQueueItem reportQueueItem)
        {
            await base.PreExecuteAsync(reportQueueItem);
            var formMetadata = await DbUnitOfWork.FormMetadata.Get();
            ReceiptDownloadFileInfo.Name = formMetadata.SvarUtDocumentTitle;
        }
        protected override async Task<byte[]> GetSubmitterReceiptPdfAsBytesAsync(string archiveReference)
        {
            var modelToPdfRequest = await CreateModelToPdfRequestAsync(archiveReference);

            await using var pdfStream = await _datamodelToPdfHttpClient.CreatePdfAsync(modelToPdfRequest, CancellationToken.None);

            var pdfBytes = new byte[pdfStream.Length];

            _ = await pdfStream.ReadAsync(pdfBytes);

            return pdfBytes;
        }

        private async Task<ModelToPdfRequest> CreateModelToPdfRequestAsync(string archiveReference)
        {
            var receiptData = await CreateReceiptDataAsync();

            var receiptInformationAsync = await GetAttachmentInfosForReceiptAsync(archiveReference);

            return new ModelToPdfRequest
            {
                ArchiveReference = archiveReference,
                Type = DataModelTypes.ReceiptToDistributor,
                ContentString = JsonSerializer.Serialize(receiptData),
                Vedlegg = receiptInformationAsync
            };
        }

        private async Task<PlanModelToPdfKvittering> CreateReceiptDataAsync()
        {
            var kommuneInfo = await _municipalityService.GetMunicipalityOrgnr(FormData.SendesTilKommune.Nummer);
            return new PlanModelToPdfKvittering
            {
                Tittel = ReceiptDownloadFileInfo.Name,
                Plannavn = FormData.Planforslag.Plannavn,
                Forslagstillernavn = FormData.Forslagsstiller.Navn,
                Plankonsulentnavn = FormData.Plankonsulent?.Navn,
                VarsledeInteressenter = new List<Interessent> { new Interessent() { Navn = $"{kommuneInfo.Name} (org. nr. {kommuneInfo.OrganizationNumber})" } },
                SoeknadMottattAvKommune = (await DbUnitOfWork.FormMetadata.Get(ArchiveReference)).SvarUtShippingTimestamp
            };
        }

        private async Task<List<VedleggsInfo>> GetAttachmentInfosForReceiptAsync(string archiveReference)
        {
            var blobStorageTypes = new List<string> { BlobStorageMetadataTypes.MainForm, BlobStorageMetadataTypes.SubmittalAttachment };
            var attachmentsInSubmittal = await BlobOperations.GetListOfBlobsMetadataValuesByBlobItemTypesAsync(BlobStorageEnum.Private, archiveReference.ToLower(), blobStorageTypes, true);
            var orderedListOfAttachments = attachmentsInSubmittal.OrderBy(attachment => ReceiptData.AttachmentOrder.IndexOf(attachment.attachmentType)).ToList();
            var orderedListOfAttachmentWithDisplayName = ReceiptData.GetAttachemntsWithDisplayName(orderedListOfAttachments);

            var vedlegg = new List<VedleggsInfo>();

            foreach (var (attachmentTypeName, displayName, fileName, vedleggsopplysninger) in orderedListOfAttachmentWithDisplayName)
            {
                vedlegg.Add(new VedleggsInfo
                {
                    Filename = fileName,
                    DataType = attachmentTypeName,
                    DisplayName = displayName,
                    AttachmentDescription = vedleggsopplysninger.FirstOrDefault(x => x.Key == Tags.VedleggsopplysningerFraSøker.VedleggKommentar.ToString()).Value,
                    AttachmentVersionNumber = vedleggsopplysninger.FirstOrDefault(x => x.Key == Tags.VedleggsopplysningerFraSøker.VedleggVersjonsnr.ToString()).Value,
                    AttachmentVersionDate = vedleggsopplysninger.FirstOrDefault(x => x.Key == Tags.VedleggsopplysningerFraSøker.VedleggVersjonsdato.ToString()).Value
                });
            }

            return vedlegg;
        }
    }
}