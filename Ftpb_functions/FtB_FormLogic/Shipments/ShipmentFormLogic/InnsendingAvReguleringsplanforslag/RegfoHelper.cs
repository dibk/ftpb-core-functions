﻿using FtB_Common.BusinessModels;
using FtB_Common.Encryption;
using System.Collections.Generic;
using System.Linq;
using Dibk.Ftpb.Common.Constants;
using Dibk.Ftpb.Common.Datamodels;
using static Dibk.Ftpb.Common.Constants.DataTypes;

namespace FtB_FormLogic.Shipments.ShipmentFormLogic.InnsendingAvReguleringsplanforslag
{
    public class RegfoHelper
    {
        public const string DataFormatVersion = "1";

        public static List<Actor> GetDistributionSenderCandidates(IDecryption decryptor, OversendelseReguleringsplanforslag oversendelseReguleringsplanforslag)
        {
            var retval = new List<Actor>();
            if (!string.IsNullOrEmpty(oversendelseReguleringsplanforslag.Forslagsstiller.Organisasjonsnummer))
                retval.Add(new Actor() { Id = oversendelseReguleringsplanforslag.Forslagsstiller.Organisasjonsnummer, Name = oversendelseReguleringsplanforslag.Forslagsstiller.Navn, Type = ActorTypeEnum.Foretak });

            if (!string.IsNullOrEmpty(oversendelseReguleringsplanforslag.Forslagsstiller.Foedselsnummer))
                retval.Add(new Actor() { Id = decryptor.DecryptText(oversendelseReguleringsplanforslag.Forslagsstiller.Foedselsnummer), Name = oversendelseReguleringsplanforslag.Forslagsstiller.Navn, Type = ActorTypeEnum.Privatperson });

            if (!string.IsNullOrEmpty(oversendelseReguleringsplanforslag.Plankonsulent?.Organisasjonsnummer))
                retval.Add(new Actor() { Id = oversendelseReguleringsplanforslag.Plankonsulent.Organisasjonsnummer, Name = oversendelseReguleringsplanforslag.Plankonsulent.Navn, Type = ActorTypeEnum.Foretak });

            if (!string.IsNullOrEmpty(oversendelseReguleringsplanforslag.Plankonsulent?.Foedselsnummer))
                retval.Add(new Actor() { Id = decryptor.DecryptText(oversendelseReguleringsplanforslag.Plankonsulent.Foedselsnummer), Name = oversendelseReguleringsplanforslag.Plankonsulent.Navn, Type = ActorTypeEnum.Privatperson });

            return retval;
        }
    }

    public class ReceiptData
    {
        public static readonly IEnumerable<(string attachmentTypeName, string displayName)> DisplayName = new List<(string attachmentTypeName, string displayName)>
        {
            (Planbeskrivelse, AttachmentDisplayName.Planbeskrivelse),
            (Plankart, AttachmentDisplayName.Plankart),
            (Planbestemmelser, AttachmentDisplayName.Planbestemmelser),
            (Illustrasjon, AttachmentDisplayName.Illustrasjon),
            (Konsekvensutredning, AttachmentDisplayName.Konsekvensutredning),
            (Kunngjoring, AttachmentDisplayName.Kunngjoring),
            (Rapport, AttachmentDisplayName.Rapport),
            (ROSAnalyse, AttachmentDisplayName.ROSAnalyse),
            (Utredning, AttachmentDisplayName.Utredning),
            (Uttalelse, AttachmentDisplayName.Uttalelse),
            (Valideringsrapport, AttachmentDisplayName.Valideringsrapport),
            (Annet, AttachmentDisplayName.Annet),
            (Saksframlegg, AttachmentDisplayName.Saksframlegg),
            (Tegnforklaringer, AttachmentDisplayName.Tegnforklaringer),
            (Planvarsel, AttachmentDisplayName.Planvarsel),
            (Varslingsliste, AttachmentDisplayName.Varslingsliste),
            (SamlefilMottatteUttalelser, AttachmentDisplayName.SamlefilMottatteUttalelser),
            (KommentarTilUttalelser, AttachmentDisplayName.KommentarTilUttalelser),
            (GrunnforholdGeoteknikk, AttachmentDisplayName.GrunnforholdGeoteknikk),
            (GrunnforholdForurensetGrunn, AttachmentDisplayName.GrunnforholdForurensetGrunn),
            (Trafikkutredning, AttachmentDisplayName.Trafikkutredning),
            (Stoyutredning, AttachmentDisplayName.Stoyutredning),
            (ArkeologiskeUndersokelser, AttachmentDisplayName.ArkeologiskeUndersokelser),
            (RammeplanVannAvlopOvervann, AttachmentDisplayName.RammeplanVannAvlopOvervann),
            (BiologiskMangfold, AttachmentDisplayName.BiologiskMangfold),
            (DataTypes.OversendelseReguleringsplanforslag, AttachmentDisplayName.OversendelseReguleringsplanforslag),
        };

        public static List<string> AttachmentOrder
        {
            get
            {
                var attachmentOrder = new List<string>
                {
                    DataTypes.OversendelseReguleringsplanforslag,
                    Plankart,
                    Planbestemmelser,
                    Planbeskrivelse,
                };

                return attachmentOrder;
            }
        }

        public static List<(string attachmentType, string displayname, string fileName, List<KeyValuePair<string, string>> vedleggsopplysninger)> GetAttachemntsWithDisplayName(IEnumerable<(string attachmentType, string fileName, List<KeyValuePair<string, string>> vedleggsopplysninger)> attachments)
        {
            return (from attachment in attachments
                    let displayname = DisplayName
                        .Where(d => d.attachmentTypeName == attachment.attachmentType)
                        .Select(a => a.displayName)
                        .FirstOrDefault()
                    select (attachment.attachmentType, string.IsNullOrEmpty(displayname) ? Annet : displayname, attachment.fileName, attachment.vedleggsopplysninger)).ToList();
        }
    }

    public class Regfo
    {
        public static readonly string FormDataFormType = "Innsending av reguleringsplanforslag";
        public static readonly string FormDataApplication = "Innsending av reguleringsplanforslag";
    }
}
