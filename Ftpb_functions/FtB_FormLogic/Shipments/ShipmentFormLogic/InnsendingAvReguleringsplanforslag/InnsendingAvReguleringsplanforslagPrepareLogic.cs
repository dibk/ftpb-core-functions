﻿using Dibk.Ftpb.Api.Models;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Constants;
using FtB_Common.Encryption;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Ftb_Repositories.LogContstants;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dibk.Ftpb.Common.Datamodels;

namespace FtB_FormLogic.Shipments.ShipmentFormLogic.InnsendingAvReguleringsplanforslag
{
    [FormDataFormat(DataFormatId = DataFormatIDs.InnsendingReguleringsplanforslag, DataFormatVersion = RegfoHelper.DataFormatVersion, ProcessingContext = FormLogicProcessingContext.Prepare)]
    public class InnsendingAvReguleringsplanforslagPrepareLogic : ShipmentPrepareLogic<OversendelseReguleringsplanforslag>
    {
        private readonly MunicipalityService _municipalityService;

        public InnsendingAvReguleringsplanforslagPrepareLogic(IFormDataRepo repo,
                                                              ILogger<InnsendingAvReguleringsplanforslagPrepareLogic> log,
                                                              IBlobOperations blobOperations,
                                                              IDbUnitOfWork dbUnitOfWork,
                                                              IDecryptionFactory decryptionFactory,
                                                              IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                                              MunicipalityService municipalityService,
                                                              DistributionReceiverRepo distributionReceiverRepo,
                                                              DistributionReceiverLogRepo distributionReceiverLogRepo)
            : base(repo,
                  log,
                  blobOperations,
                  dbUnitOfWork,
                  decryptionFactory,
                  fileDownloadHttpClient,
                  distributionReceiverRepo,
                  distributionReceiverLogRepo)
        {
            _municipalityService = municipalityService;
        }

        public override Task<IEnumerable<SendQueueItem>> ExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {
            Logger.LogInformation("Innsending av reguleringsplanforslag mottatt av CoreFunctions");
            Logger.LogInformation("Opprett formMetadata");
            var formmetadata = new FormMetadataApiModel()
            {
                ArchiveReference = submittalQueueItem.ArchiveReference,
                Status = FormMetadataStatus.IKø,
                ArchiveTimestamp = DateTime.Now,
                FormType = Regfo.FormDataFormType,
                Application = FormData.Metadata.FraSluttbrukersystem,
                SenderSystem = "Innsending av reguleringsplanforslag",
                ServiceCode = "INNSENDING-REGFO",
                ServiceEditionCode = 1,
                MunicipalityCode = FormData.SendesTilKommune.Nummer,
            };
            DbUnitOfWork.FormMetadata.Create(formmetadata);

            return base.ExecuteAsync(submittalQueueItem);
        }

        public override async Task SetReceiversAsync()
        {
            var municipality = await _municipalityService.GetMunicipalityOrgnr(this.FormData.SendesTilKommune.Nummer, FtPBContext.Plan);

            try
            {
                var receivers = new List<Receiver>
                {
                    new() { Type = ActorTypeEnum.OffentligMyndighet, Id = municipality.OrganizationNumber, Name = FormData.SendesTilKommune.Navn}
                };
                base.SetReceivers(receivers);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public override async Task SetSenderAsync()
        {
            var encryptedReporteeId = await BlobOperations.GetReporteeIdFromStoredBlobAsync(ArchiveReference);
            var senderCandidates = GetSenderCandidates(DecryptionFactory.GetDecryptor(), FormData);

            base.Sender = senderCandidates.FirstOrDefault(r => r.Id.Equals(DecryptionFactory.GetDecryptor().DecryptText(encryptedReporteeId)));
        }

        public static List<Actor> GetSenderCandidates(IDecryption decryptor, OversendelseReguleringsplanforslag oversendelseReguleringsplanforslag)
        {
            var retval = new List<Actor>();
            if (!string.IsNullOrEmpty(oversendelseReguleringsplanforslag.Plankonsulent?.Organisasjonsnummer))
                retval.Add(new Actor() { Id = oversendelseReguleringsplanforslag.Plankonsulent.Organisasjonsnummer, Name = oversendelseReguleringsplanforslag.Plankonsulent.Navn, Type = ActorTypeEnum.Foretak });

            if (!string.IsNullOrEmpty(oversendelseReguleringsplanforslag.Plankonsulent?.Foedselsnummer))
                retval.Add(new Actor() { Id = decryptor.DecryptText(oversendelseReguleringsplanforslag.Plankonsulent.Foedselsnummer), Name = oversendelseReguleringsplanforslag.Plankonsulent.Navn, Type = ActorTypeEnum.Privatperson });

            if (!string.IsNullOrEmpty(oversendelseReguleringsplanforslag.Forslagsstiller.Organisasjonsnummer))
                retval.Add(new Actor() { Id = oversendelseReguleringsplanforslag.Forslagsstiller.Organisasjonsnummer, Name = oversendelseReguleringsplanforslag.Forslagsstiller.Navn, Type = ActorTypeEnum.Foretak });

            if (!string.IsNullOrEmpty(oversendelseReguleringsplanforslag.Forslagsstiller.Foedselsnummer))
                retval.Add(new Actor() { Id = decryptor.DecryptText(oversendelseReguleringsplanforslag.Forslagsstiller.Foedselsnummer), Name = oversendelseReguleringsplanforslag.Forslagsstiller.Navn, Type = ActorTypeEnum.Privatperson });

            return retval;
        }
    }
}