﻿using System.Threading.Tasks;
using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Integration.SvarUt.Builders.Models;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Constants;
using FtB_Common.Encryption;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_FormLogic.Shipments.SvarUt;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;

namespace FtB_FormLogic.Shipments.ShipmentFormLogic.InnsendingAvReguleringsplanforslag
{
    [FormDataFormat(DataFormatId = DataFormatIDs.InnsendingReguleringsplanforslag, DataFormatVersion = RegfoHelper.DataFormatVersion, ProcessingContext = FormLogicProcessingContext.Send)]
    public class InnsendingAvReguleringsplanforslagSendLogic : SvarUtSendLogic<OversendelseReguleringsplanforslag>
    {
        public InnsendingAvReguleringsplanforslagSendLogic(IFormDataRepo repo,
                                                           ILogger<InnsendingAvReguleringsplanforslagSendLogic> log,
                                                           IBlobOperations blobOperations,
                                                           ISvarUtAdapter svarUtAdapter,
                                                           IDbUnitOfWork dbUnitOfWork,
                                                           IDecryptionFactory decryptionFactory,
                                                           IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                                           SvarUtShipmentBuilder shipmentBuilder,
                                                           DistributionReceiverRepo distributionReceiverRepo,
                                                           DistributionReceiverLogRepo distributionReceiverLogRepo)
            : base(repo,
                  log,
                  blobOperations,
                  svarUtAdapter,
                  dbUnitOfWork,
                  decryptionFactory,
                  fileDownloadHttpClient,
                  shipmentBuilder,
                  distributionReceiverRepo,
                  distributionReceiverLogRepo)
        { }

        public override async Task<ReportQueueItem> ExecuteAsync(SendQueueItem sendQueueItem)
        {
            var reportQueueItem = await base.ExecuteAsync(sendQueueItem);

            return reportQueueItem;
        }

        public override Task SetSenderAsync()
        {
            var address = string.Empty;
            var postalCode = string.Empty;
            var poststed = string.Empty;

            if (FormData.Plankonsulent != null)
            {
                address = FormData.Plankonsulent.Adresse?.Adresselinje1;
                postalCode = FormData.Plankonsulent.Adresse?.Postnr;
                poststed = FormData.Plankonsulent.Adresse?.Poststed;
            }
            else if (FormData.Forslagsstiller != null)
            {
                address = FormData.Forslagsstiller.Adresse?.Adresselinje1;
                postalCode = FormData.Forslagsstiller.Adresse?.Postnr;
                poststed = FormData.Forslagsstiller.Adresse?.Poststed;
            }

            Sender = new ShipmentActor(SendQueueItem.Sender)
            {
                Address = address,
                PostalCode = postalCode,
                Poststed = poststed
            };

            return Task.CompletedTask;
        }

        protected override ForsendelseDataBygg MapForsendelseData()
        {
            return new ForsendelseDataBygg(SendQueueItem.ArchiveReference)
            {
                AvgivendeSystem = FormData.Metadata.FraSluttbrukersystem,
                Kommunenummer = FormData.SendesTilKommune.Nummer,
                ForsendelseType = "Innsending av reguleringsplanforslag",
                ForsendelseTittel = "Oversendelse av reguleringsplanforslag",
                SøknadSkjemaNavn = "Innsending av reguleringsplanforslag",
                Konteringskode = "REGFO"
            };
        }
    }
}