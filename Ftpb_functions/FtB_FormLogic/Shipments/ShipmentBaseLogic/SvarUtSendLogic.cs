using Dibk.Ftpb.Integration.SvarUt;
using Dibk.Ftpb.Integration.SvarUt.Builders.Models;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.FormDataRepositories;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_FormLogic.Shipments.SvarUt;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Ftb_Repositories.LogContstants;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    public abstract class SvarUtSendLogic<T> : SendLogic<T>
    {
        protected readonly ISvarUtAdapter SvarUtAdapter;
        protected readonly IBlobOperations BlobOperations;
        protected string AuthenticatedSubmitter;
        protected List<AttachmentInfo> Attachments;
        protected List<SubformInfo> Subforms;
        protected ForsendelseDataBygg ForsendelseData;

        private readonly SvarUtShipmentBuilder _shipmentBuilder;

        public SvarUtSendLogic(IFormDataRepo repo,
                               ILogger log,
                               IBlobOperations blobOperations,
                               ISvarUtAdapter svarUtAdapter,
                               IDbUnitOfWork dbUnitOfWork,
                               IDecryptionFactory decryptionFactory,
                               IFileDownloadStatusHttpClient fileDownloadHttpClient,
                               SvarUtShipmentBuilder shipmentBuilder,
                               DistributionReceiverRepo distributionReceiverRepo,
                               DistributionReceiverLogRepo distributionReceiverLogRepo) :
            base(repo,
                log,
                dbUnitOfWork,
                decryptionFactory,
                fileDownloadHttpClient,
                distributionReceiverRepo,
                distributionReceiverLogRepo)
        {
            SvarUtAdapter = svarUtAdapter;
            BlobOperations = blobOperations;
            _shipmentBuilder = shipmentBuilder;
        }

        public override async Task PreExecuteAsync(SendQueueItem sendQueueItem)
        {
            AuthenticatedSubmitter = await BlobOperations.GetReporteeIdFromStoredBlobAsync(sendQueueItem.ArchiveReference);
            await base.PreExecuteAsync(sendQueueItem);

            Attachments = FormDataRepo.GetAttachments(ArchiveReference);
            Subforms = await FormDataRepo.GetSubforms(ArchiveReference);
            ForsendelseData = MapForsendelseData();
        }

        public override async Task<ReportQueueItem> ExecuteAsync(SendQueueItem sendQueueItem)
        {
            var reportQueueItem = await base.ExecuteAsync(sendQueueItem);
            var fm = await DbUnitOfWork.FormMetadata.Get(ArchiveReference);

            if (!string.IsNullOrEmpty(fm.SvarUtForsendelsesId))
            {
                Logger.LogWarning("Forsendelse sent previously with id: {ForsendelseId}", fm.SvarUtForsendelsesId);
                DbUnitOfWork.LogEntries.AddInfo($"Forsendelse er sent tidligere med forsendelseId {fm.SvarUtForsendelsesId}");

                if (fm.Status == FormMetadataStatus.Ok)
                    return null;
            }

            try
            {
                var forsendelse = await _shipmentBuilder.Build(ArchiveReference, Receiver, Sender, ForsendelseData);
                DbUnitOfWork.LogEntries.AddInfo($"Sender skjema til: {sendQueueItem.Receiver.Name} organisasjonsnummer: {sendQueueItem.Receiver.Id}");

                var forsendelseId = string.Empty;
                try
                {
                    forsendelseId = await SvarUtAdapter.SendAsync(forsendelse);
                }
                catch (ArgumentOutOfRangeException ae)
                {
                    Logger.LogError(ae, "SvarUt forsendelse is invalid");
                    DbUnitOfWork.LogEntries.AddError($"Data i SvarUt-forsendelsen er ugyldige: {ae}");
                    await UpdateFormProcessStatus(FormMetadataStatus.Feil);
                    return null;
                }
                catch (SvarUtRequestException sure)
                {
                    DbUnitOfWork.LogEntries.AddError($"Forsendelse til SvarUt feilet: {sure.ToString()}");
                    Logger.LogError(sure, "SvarUt did not accept the shipment");

                    if (sure.HttpStatusCode == System.Net.HttpStatusCode.BadRequest
                        || sure.HttpStatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        await UpdateFormProcessStatus(FormMetadataStatus.Feil);
                        return null;
                    }
                    else
                        throw;
                }

                fm.MunicipalityCode = ForsendelseData.Kommunenummer;
                if (sendQueueItem.Receiver.Type != ActorTypeEnum.Privatperson)
                    fm.SendTo = $"{sendQueueItem.Receiver.Name} ({sendQueueItem.Receiver.Id})";

                if (int.TryParse(ForsendelseData.KommunensSaksnummerÅr, out var parsedSaksår))
                    fm.MunicipalityArchiveCaseYear = parsedSaksår;

                if (long.TryParse(ForsendelseData.KommunensSaksnummerSekvensnummer, out var parsedSakssekvensnr))
                    fm.MunicipalityArchiveCaseSequence = parsedSakssekvensnr;

                fm.Status = FormMetadataStatus.Ok;
                fm.SvarUtForsendelsesId = forsendelseId;
                fm.SvarUtDocumentTitle = forsendelse.Tittel;
                fm.SvarUtShippingTimestamp = forsendelse.MetadataForImport.DokumentetsDato;

                DbUnitOfWork.LogEntries.AddInfo($"Forsendelse akseptert av SvarUt med {forsendelseId}");
            }
            catch (Exception ex)
            {
                DbUnitOfWork.LogEntries.AddError($"En feil oppstod ifm sending av forsendelse til SvarUt: {ex.Message}");
                Logger.LogError(ex, "Error occurred while sending forsendelse to SvarUt");
                throw;
            }

            await DbUnitOfWork.FormMetadata.Save();

            return reportQueueItem;
        }

        protected async Task UpdateFormProcessStatus(string status)
        {
            var formMetadata = await DbUnitOfWork.FormMetadata.Get();
            formMetadata.Status = status;
            DbUnitOfWork.FormMetadata.Update(formMetadata);
            await DbUnitOfWork.FormMetadata.Save();
        }

        protected abstract ForsendelseDataBygg MapForsendelseData();
    }
}