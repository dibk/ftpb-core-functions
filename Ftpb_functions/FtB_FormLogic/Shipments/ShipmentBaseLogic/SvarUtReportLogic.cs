using Altinn.Common.Models;
using Altinn3.Adapters;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Enums;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Ftb_Repositories.LogContstants;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FtB_FormLogic.Shipments.ShipmentBaseLogic;

public abstract class SvarUtReportLogic<T> : ReportLogic<T>
{
    protected SvarUtReportLogic(
        IBlobOperations blobOperations,
        IAltinnInstanceCompleter altinnInstanceCompleter,
        DistributionSubmittalRepo distributionSubmittalRepo,
        IFormDataRepo repo,
        ILogger log,
        IDbUnitOfWork dbUnitOfWork,
        IFileDownloadStatusHttpClient fileDownloadHttpClient,
        DistributionReceiverRepo distributionReceiverRepo,
        DistributionReceiverLogRepo distributionReceiverLogRepo
    ) : base(
        blobOperations,
        altinnInstanceCompleter,
        distributionSubmittalRepo,
        repo,
        log,
        dbUnitOfWork,
        fileDownloadHttpClient,
        distributionReceiverRepo,
        distributionReceiverLogRepo
    )
    {
    }

    public override async Task<string> ExecuteAsync(ReportQueueItem reportQueueItem)
    {
        var returnItem = await base.ExecuteAsync(reportQueueItem);

        await SendReceiptToSubmitterAsync(reportQueueItem);

        return returnItem;
    }

    private async Task SendReceiptToSubmitterAsync(ReportQueueItem reportQueueItem)
    {
        try
        {
            await UploadFilesToStatusApiAsync(reportQueueItem);
            await ReportFormProcessStatusAsync(FormMetadataStatus.Ok);
        }
        catch (Exception ex)
        {
            Logger.LogError(ex, "Error occurred when creating and sending receipt");
            throw;
        }
        finally
        {
            await BlobOperations.ReleaseContainerLeaseAsync(reportQueueItem.ArchiveReference.ToLower());
        }
    }

    private async Task UploadFilesToStatusApiAsync(ReportQueueItem reportQueueItem)
    {
        var containerNameGuid = Guid.NewGuid();

        var downloadFileInfosAndBinaries = await GetDownloadFileInfosAndBinariesAsync(reportQueueItem.ArchiveReference);

        await AddFileDownloadStatusesAsync(reportQueueItem.ArchiveReference, containerNameGuid, downloadFileInfosAndBinaries);
    }

    private async Task<IEnumerable<(StatusApiFileInfo downloadFileInfo, AttachmentBinary binaryFile)>>
        GetDownloadFileInfosAndBinariesAsync(string archiveReference)
    {
        var mainFormAttachment = await CreateMainFormAttachmentAsync(archiveReference);

        var receiptAttachment = await CreateReceiptAttachmentAsync(archiveReference);

        return mainFormAttachment != null
            ? [(MainFormDownloadFileInfo, mainFormAttachment), (ReceiptDownloadFileInfo, receiptAttachment)]
            : [(ReceiptDownloadFileInfo, receiptAttachment)];
    }

    private async Task<AttachmentBinary> CreateMainFormAttachmentAsync(string archiveReference)
    {
        KeyValuePair<string, string> metadata = new(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.MainForm);

        var mainFormBytes = await BlobOperations.GetBlobAsBytesByMetadataAsync(BlobStorageEnum.Private, archiveReference, metadata);

        return mainFormBytes != null
            ? new AttachmentBinary
            {
                BinaryContent = mainFormBytes,
                Filename = MainFormDownloadFileInfo.FileName,
                Name = MainFormDownloadFileInfo.Name,
                ArchiveReference = ArchiveReference
            }
            : null;
    }

    private async Task<AttachmentBinary> CreateReceiptAttachmentAsync(string archiveReference)
    {
        var pdfAsBytes = await GetSubmitterReceiptPdfAsBytesAsync(archiveReference);

        return new AttachmentBinary
        {
            BinaryContent = pdfAsBytes,
            Filename = "Kvittering.pdf",
            Name = "Kvittering",
            ArchiveReference = ArchiveReference
        };
    }

    protected abstract Task<byte[]> GetSubmitterReceiptPdfAsBytesAsync(string archiveReference);
}