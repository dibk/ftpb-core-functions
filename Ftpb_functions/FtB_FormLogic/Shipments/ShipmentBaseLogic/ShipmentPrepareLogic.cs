﻿using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.FormDataRepositories;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    public abstract class ShipmentPrepareLogic<T> : PrepareLogic<T>
    {
        protected List<AttachmentInfo> Attachments;
        protected List<SubformInfo> Subforms;

        public ShipmentPrepareLogic(IFormDataRepo repo,
                                    ILogger log,
                                    IBlobOperations blobOperations,
                                    IDbUnitOfWork dbUnitOfWork,
                                    IDecryptionFactory decryptionFactory,
                                    IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                    DistributionReceiverRepo distributionReceiverRepo,
                                    DistributionReceiverLogRepo distributionReceiverLogRepo) : 
            base(repo,
                log,
                dbUnitOfWork,
                decryptionFactory,
                blobOperations,
                fileDownloadHttpClient,
                distributionReceiverRepo,
                distributionReceiverLogRepo)
        {
        }
        public override async Task PreExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {
            await base.PreExecuteAsync(submittalQueueItem);

            Attachments = base.FormDataRepo.GetAttachments(ArchiveReference);
            Subforms = await base.FormDataRepo.GetSubforms(ArchiveReference);
        }
        public override async Task<IEnumerable<SendQueueItem>> ExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {          
            var returnValue = await base.ExecuteAsync(submittalQueueItem);
            var sendQueueItems = base.CreateSendQueueItem(submittalQueueItem);
            var queueList = new List<SendQueueItem>();
            queueList.Add(sendQueueItems);
            
            var attribute = GetFormDataFormatAttribute();
            if (attribute != default(FormDataFormatAttribute))
            {
                var logMessage = $"Starter '{attribute.ProcessingContext}' behandling av: {attribute.DataFormatId} {attribute.DataFormatVersion}";
                DbUnitOfWork.LogEntries.AddInfoInternal(logMessage, "prosessering");
            }

            return queueList;
        }
    }
}
