﻿using Altinn3.Adapters;
using Dibk.Ftpb.Common.Constants;
using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.ModelToPdf.Common.Client;
using Dibk.Ftpb.ModelToPdf.Common.Models;
using FtB_Common;
using FtB_Common.BusinessModels;
using FtB_Common.Constants;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.Storage;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using static Dibk.Ftpb.Common.Constants.DataTypes;

namespace FtB_FormLogic.AltinnEventsLogic
{
    [Altinn(Resource = AltinnEventResources.IgangsettingstillatelseV3, Type = AltinnEventTypes.InstanceCompleted)]
    public class IgangsettingstillatelseProcessCompletedEventHandler : BaseProcessCompletedEventHandler<Igangsettingstillatelse>
    {
        protected override string DataFormatId => DataFormatIDs.IgangsettingstillatelseV3;
        protected override string FormDataDataType => SøknadOmIgangsettingstillatelse;
        protected override string MainFormFileName => $"{AttachmentDisplayName.SøknadOmIgangsettingstillatelse}.pdf";
        protected override string MainFormAttachmentTypeName => SøknadOmIgangsettingstillatelse;
        protected override string DataModelType => DataModelTypes.SoeknadOmIgangsettingstillatelse;
        protected override string MainFormDatatype => RefDataAsPdf;

        protected override bool GeneratePdfForDataModelType => false;
        protected override bool GenerateZipCollectionOfAttachments => false;

        protected override BlobStorageEnum BlobStorageForAttachments => BlobStorageEnum.Private;
        protected override bool SupportUserDefinedMetadata => true;

        protected override List<(string datatype, string extension)> DatatypeWithExtensionToExludeInReceipt => new()
        {
            (Gjennomføringsplan, "xml"),
            (SøknadOmIgangsettingstillatelse, "pdf")
        };

        public IgangsettingstillatelseProcessCompletedEventHandler(
            ILogger<IgangsettingstillatelseProcessCompletedEventHandler> logger,
            IBlobOperations blobOperations,
            Altinn3Service altinn3Service,
            IDecryptionFactory decryptionFactory,
            IDatamodelToPdfHttpClient datamodelToPdfHttpClient,
            EnvironmentProvider env
            ) : base(logger, blobOperations, altinn3Service, decryptionFactory, datamodelToPdfHttpClient, env)
        {
        }

        protected override string GetPlankonsulentOrganisationNumber(Igangsettingstillatelse formData)
        {
            return null;
        }
    }
}
