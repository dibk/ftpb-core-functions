using Altinn3.Adapters;
using Dibk.Ftpb.Common.Constants;
using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.ModelToPdf.Common.Client;
using Dibk.Ftpb.ModelToPdf.Common.Models;
using FtB_Common;
using FtB_Common.BusinessModels;
using FtB_Common.Constants;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.Storage;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using static Dibk.Ftpb.Common.Constants.DataTypes;

namespace FtB_FormLogic.AltinnEventsLogic
{
    [Altinn(Resource = AltinnEventResources.InnsendingPlanforslag, Type = AltinnEventTypes.InstanceCompleted)]
    public class InnsendingPlanforslagProcessCompletedEventHandler : BaseProcessCompletedEventHandler<OversendelseReguleringsplanforslag>
    {
        protected override string DataFormatId => DataFormatIDs.InnsendingReguleringsplanforslag;
        protected override string FormDataDataType => DataTypes.OversendelseReguleringsplanforslag;
        protected override string MainFormFileName => "Oversendelsebrev.pdf";
        protected override string MainFormAttachmentTypeName => DataTypes.OversendelseReguleringsplanforslag;
        protected override string DataModelType => DataModelTypes.InnsendingReguleringsplanforslag;

        protected override bool GeneratePdfForDataModelType => false;
        protected override bool GenerateZipCollectionOfAttachments => false;
        protected override bool SupportUserDefinedMetadata => false;

        protected override List<string> AltinnDataTypesToIgnoreWhenProcessingAltinnInstanceData =>
        [
            .. base.AltinnDataTypesToIgnoreWhenProcessingAltinnInstanceData,
            RefDataAsPdf,
            Valideringsrapport
        ];


        protected override BlobStorageEnum BlobStorageForAttachments => BlobStorageEnum.Private;

        protected override List<(string datatype, string extension)> DatatypeWithExtensionToExludeInReceipt => new();

        public InnsendingPlanforslagProcessCompletedEventHandler(
            ILogger<InnsendingPlanforslagProcessCompletedEventHandler> logger,
            IBlobOperations blobOperations,
            Altinn3Service altinn3Service,
            IDecryptionFactory decryptionFactory,
            IDatamodelToPdfHttpClient datamodelToPdfHttpClient,
            EnvironmentProvider env
            ) : base(logger, blobOperations, altinn3Service, decryptionFactory, datamodelToPdfHttpClient, env)
        {
        }

        protected override string GetPlankonsulentOrganisationNumber(OversendelseReguleringsplanforslag formData)
        {
            return formData.Plankonsulent?.Organisasjonsnummer;
        }
    }
}
