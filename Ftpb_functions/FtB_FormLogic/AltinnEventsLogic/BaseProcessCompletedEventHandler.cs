using Altinn.Platform.Storage.Interface.Models;
using Altinn3.Adapters;
using Dibk.Ftpb.ModelToPdf.Common.Client;
using Dibk.Ftpb.ModelToPdf.Common.Models;
using FtB_Common;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.Constants;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.Exceptions;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_Common.Utils;
using FtB_FormLogic.BaseLogic.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using static Dibk.Ftpb.Common.Constants.DataTypes;

namespace FtB_FormLogic.AltinnEventsLogic
{
    // Dersom alle tjenester skal følge IG3 (lager PDF av datamodell i signeringssteget), kan egenskapene
    // MainFormFileName og GeneratePdfForDataModelType samt implementasjonen av logikken fjernes
    public abstract class BaseProcessCompletedEventHandler<TFormData> : IAltinnLogic<SubmittalQueueItem, AltinnEvent>
    {
        protected abstract string DataFormatId { get; }
        protected abstract string FormDataDataType { get; }
        protected abstract string MainFormFileName { get; }
        protected abstract string MainFormAttachmentTypeName { get; }
        protected abstract string DataModelType { get; }
        protected abstract bool GeneratePdfForDataModelType { get; }
        protected abstract bool GenerateZipCollectionOfAttachments { get; }
        protected abstract BlobStorageEnum BlobStorageForAttachments { get; }
        protected abstract bool SupportUserDefinedMetadata { get; }

        protected virtual List<string> SubFormDataTypes => [
            Gjennomføringsplan
        ];

        protected abstract List<(string datatype, string extension)> DatatypeWithExtensionToExludeInReceipt { get; }

        protected virtual List<string> AltinnDataTypesToIgnoreWhenProcessingAltinnInstanceData =>
        [
            Signatur,
            FormDataDataType,
            MainFormDatatype
            // Handled by ProcessFormData
        ];

        protected virtual string MainFormDatatype => FormDataDataType + "-pdf";

        private List<string> ContentTypesForZip => new() { "application/pdf", "image/jpeg", "image/tiff", "image/png" };

        private readonly ILogger<BaseProcessCompletedEventHandler<TFormData>> _logger;
        private readonly IBlobOperations _blobOperations;
        private readonly Altinn3Service _altinn3Service;
        private readonly IDecryption _decryptor;
        private readonly IDatamodelToPdfHttpClient _datamodelToPdfHttpClient;
        private readonly EnvironmentProvider _env;

        protected BaseProcessCompletedEventHandler(
            ILogger<BaseProcessCompletedEventHandler<TFormData>> logger,
            IBlobOperations blobOperations,
            Altinn3Service altinn3Service,
            IDecryptionFactory decryptionFactory,
            IDatamodelToPdfHttpClient datamodelToPdfHttpClient,
            EnvironmentProvider env)
        {
            _logger = logger;
            _blobOperations = blobOperations;
            _decryptor = decryptionFactory.GetDecryptor();
            _datamodelToPdfHttpClient = datamodelToPdfHttpClient;
            _env = env;
            _altinn3Service = altinn3Service;
        }

        public async Task<SubmittalQueueItem> ExecuteAsync(AltinnEvent altinnEvent)
        {
            using (var scope = _logger.BeginScope(new Dictionary<string, string>
            {
                { "ResourceInstance", altinnEvent.ResourceInstance },
                { "FormDataType", FormDataDataType }
            }))
            {
                var altinnInstance = await _altinn3Service.GetAltinnInstanceAsync(altinnEvent);

                if (!ContinueProcessInThisEnvironment(altinnInstance))
                {
                    _logger.LogInformation(
                        "Environment {environment} is not correct. Aborting process of resourceInstance: {resourceInstance}.",
                        _env.CurrentEnvironment.ToString(), altinnEvent.ResourceInstance);
                    throw new WrongEnvironmentException("Aborting process as environment is not correct.");
                }

                var archiveReference = altinnEvent.ArchiveReference;

                try
                {
                    await SaveAltinnInstanceDataToBlob(altinnInstance, archiveReference, altinnEvent.Source);

                    await CreateArchivedItemInformationInBlob(altinnInstance, archiveReference);
                }
                catch (Exception e)
                {
                    _logger.LogError(
                        "Failed to save Altinn files to Blob Storage. ResourceInstance: {resourceInstance}",
                        altinnEvent.ResourceInstance);
                    throw new CancelProcessException(
                        $"Failed to save Altinn files to Blob Storage. ResourceInstance: {altinnEvent.ResourceInstance}",
                        e);
                }

                return new SubmittalQueueItem()
                {
                    ArchiveReference = archiveReference,
                };
            }
        }

        private bool ContinueProcessInThisEnvironment(Instance altinnInstance)
        {
            var formDataData = altinnInstance?.Data?.Where(data => data.DataType.Equals(FormDataDataType)).FirstOrDefault();
            var isDevelopmentTagPresent = !string.IsNullOrEmpty(formDataData?.Tags?.Where(tag => tag.Equals(Tags.DevelopmentTagValue)).FirstOrDefault());

            if (_env.IsDevelopment)
            {
                if (isDevelopmentTagPresent)
                {
                    _logger.LogInformation("DevelopmentTag {developmentTag} is present on formdata with data type {formdataDataType}. Environment is {environment}.", Tags.DevelopmentTagValue, FormDataDataType, _env.CurrentEnvironment.ToString());
                    return true;
                }
                _logger.LogInformation("DevelopmentTag is missing on formdata with data type {formdataDataType}. Environment is {environment}.", FormDataDataType, _env.CurrentEnvironment.ToString());
                return false;
            }
            else
            {
                if (isDevelopmentTagPresent)
                {
                    _logger.LogInformation("DevelopmentTag is exists on data type {formdataDataType}. Environment is {environment}.", FormDataDataType, _env.CurrentEnvironment.ToString());
                    return false;
                }
            }
            return true;
        }

        private async Task<Stream> GetMainFormPdf(Instance instance, string archiveReference)
        {
            var modelToPdfRequest = await CreateModelToPdfRequestAsync(instance, archiveReference);
            var result = await _datamodelToPdfHttpClient.CreatePdfAsync(modelToPdfRequest, CancellationToken.None);
            return result;
        }

        private async Task<ModelToPdfRequest> CreateModelToPdfRequestAsync(Instance oversendelseInstance, string archiveReference)
        {
            string responseXml = null;

            try
            {
                var oversendelseDataItem = oversendelseInstance.Data.FirstOrDefault(d => d.DataType.Equals(FormDataDataType));
                using var responseStream = await _altinn3Service.GetStreamFromAltinnInstanceAsync(oversendelseDataItem);

                using (var reader = new StreamReader(responseStream))
                    responseXml = reader.ReadToEnd();
            }
            catch (Exception exception)
            {
                _logger.LogError(exception,
                    "Failed to get oversendelse av {dataModelType} as XML. Source: {oversendelseInstance}",
                    DataModelType, oversendelseInstance);
                throw;
            }

            if (string.IsNullOrEmpty(responseXml))
                return null;

            List<VedleggsInfo> vedlegg = new();
            foreach (var dataItem in oversendelseInstance.Data)
            {
                if (dataItem.DataType.Equals(FormDataDataType)
                    && dataItem.ContentType.Contains("xml"))
                    continue;

                vedlegg.Add(new VedleggsInfo
                {
                    DataType = dataItem.DataType,
                    Filename = dataItem.Filename,
                    Size = dataItem.Size.ToString(),
                });
            }

            return new ModelToPdfRequest
            {
                Type = DataModelType,
                Altinn3InstanceOwnerIsPlankonsulent = IsSenderPlankonsulent(oversendelseInstance.InstanceOwner, responseXml),
                ArchiveReference = archiveReference,
                Vedlegg = vedlegg,
                ContentString = responseXml
            };
        }

        private bool IsSenderPlankonsulent(InstanceOwner instanceOwner, string mainFormAsXml)
        {
            var orgNo = instanceOwner.OrganisationNumber;

            // Plankonsulent kan ikke være privatperson
            if (string.IsNullOrEmpty(orgNo))
                return false;

            using var reader = new StringReader(mainFormAsXml);

            if (new XmlSerializer(typeof(TFormData)).Deserialize(reader) is not TFormData formData)
                return false;

            var plankonsulentOrgNo = GetPlankonsulentOrganisationNumber(formData);

            return orgNo.Equals(plankonsulentOrgNo);
        }

        private async Task CreateArchivedItemInformationInBlob(Instance altinnInstance, string archivedReference)
        {
            var archivedItemInformation = new ArchivedItemInformation
            {
                ArchiveReference = archivedReference,
                EncryptedReporteeId = GetReporteeId(altinnInstance),
                DataFormatVersionID = 1,
                DataFormatID = DataFormatId
            };

            const string mimeType = "application/json";
            var fileName = "ArchivedItemInformation_" + archivedReference + ".json";

            var archivedItemInformationAsJsonString = JsonConvert.SerializeObject(archivedItemInformation);
            var stream = new MemoryStream(Encoding.Default.GetBytes(archivedItemInformationAsJsonString));

            var metadata = CreateMetaDataForArchivedItemInformation();

            await _blobOperations.AddStreamToBlobStorageAsync(BlobStorageEnum.Private,
                archivedItemInformation.ArchiveReference, fileName, stream, mimeType, metadata);
        }

        private static Dictionary<string, string> CreateMetaDataForArchivedItemInformation()
        {
            Dictionary<string, string> metadata = new() { { BlobStorageMetadataKeys.Type, "ArchivedItemInformation" } };

            return metadata;
        }

        private string GetReporteeId(Instance altinnInstance)
        {
            return !string.IsNullOrEmpty(altinnInstance.InstanceOwner.OrganisationNumber)
                ? altinnInstance.InstanceOwner.OrganisationNumber
                : _decryptor.EncryptText(altinnInstance.InstanceOwner.PersonNumber);
        }

        private async Task SaveAltinnInstanceDataToBlob(Instance instance, string archiveReference, string instanceSource)
        {
            _logger.LogInformation("Start saving Altinn instance data to Blob Storage");

            var zipStream = new MemoryStream();
            var addedFileNames = new Dictionary<string, int>();
            var attachmentContainerReference = BlobStorageForAttachments == BlobStorageEnum.Private ? archiveReference : Guid.NewGuid().ToString();

            try
            {
                using (var zipArchive = new ZipArchive(zipStream, ZipArchiveMode.Create, true))
                {
                    await ProcessFormData(instance, archiveReference, instanceSource, attachmentContainerReference);
                    await ProcessMainForm(instance, attachmentContainerReference, instanceSource, addedFileNames);

                    if (GeneratePdfForDataModelType)
                    {
                        await ProcessPdfData(instance, archiveReference, zipArchive, addedFileNames, attachmentContainerReference);
                    }

                    await ProcessInstanceData(instance, addedFileNames, zipArchive, attachmentContainerReference);

                    if (GenerateZipCollectionOfAttachments)
                    {
                        await SaveZipArchive(zipStream, addedFileNames, attachmentContainerReference);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while saving Altinn instance data to Blob Storage");
                throw;
            }
            finally
            {
                zipStream.Dispose();
            }
        }

        private async Task ProcessFormData(Instance instance, string archiveReference, string instanceSource, string attachmentContainerReference)
        {
            _logger.LogInformation("Processing form data {FormDataType}", FormDataDataType);

            var formDataInstanceItem = instance.Data.FirstOrDefault(dataItem => dataItem.DataType.Equals(FormDataDataType));

            if (formDataInstanceItem != null)
            {
                await using var formdataAsStream = await _altinn3Service.GetStreamFromAltinnInstanceAsync(formDataInstanceItem);
                if (formdataAsStream.CanSeek)
                    formdataAsStream.Position = 0;

                var blobName = !string.IsNullOrEmpty(formDataInstanceItem.Filename)
                    ? formDataInstanceItem.Filename
                    : $"{formDataInstanceItem.DataType}.xml";

                formDataInstanceItem.Filename = blobName;

                var metadata = await CreateMetadataForFormData(attachmentContainerReference, instanceSource,formDataInstanceItem, formdataAsStream);

                await SaveStreamToBlobAsync(BlobStorageEnum.Private,
                                            archiveReference,
                                            blobName,
                                            formdataAsStream,
                                            formDataInstanceItem.ContentType,
                                            metadata);
            }
            else
            {
                _logger.LogWarning("No form data found for {formDataType}", FormDataDataType);
            }
        }

        private async Task ProcessMainForm(Instance instance, string archiveReference, string instanceSource, Dictionary<string, int> addedFileNames)
        {
            var formDataDataElementId = instance.Data.FirstOrDefault(dataItem => dataItem.DataType.Equals(FormDataDataType))?.Id;

            using (var scope = _logger.BeginScope(new Dictionary<string, string>
            {
                { "MainformDataType", MainFormDatatype }
            }))
            {
                _logger.LogInformation("Processing mainForm data {MainformDataType}", MainFormDatatype);
                var mainFormDataItem = instance.Data.FirstOrDefault(dataItem => dataItem.DataType.Equals(MainFormDatatype));

                if (mainFormDataItem != null)
                {
                    var blobName = GenerateBlobName(mainFormDataItem, addedFileNames, out var blobNamePrefixMetadata);

                    await using var dataAsStream = await _altinn3Service.GetStreamFromAltinnInstanceAsync(mainFormDataItem);

                    if (dataAsStream.CanSeek)
                        dataAsStream.Position = 0;

                    var metadata = CreateMetadataForMainForm(blobNamePrefixMetadata, mainFormDataItem, formDataDataElementId);

                    await SaveStreamToBlobAsync(BlobStorageForAttachments, archiveReference, blobName, dataAsStream, mainFormDataItem.ContentType, metadata);
                }
                else
                {
                    _logger.LogWarning("No mainform data found for {MainformDataType}", MainFormDatatype);
                }
            }
        }

        private async Task ProcessPdfData(Instance instance, string archiveReference, ZipArchive zipArchive, Dictionary<string, int> addedFileNames, string attachmentContainerReference)
        {
            var mainFormPdf = await GetMainFormPdf(instance, archiveReference);

            if (mainFormPdf.CanSeek)
                mainFormPdf.Position = 0;

            if (GenerateZipCollectionOfAttachments)
            {
                AddAttachmentToZip(zipArchive, mainFormPdf, MainFormFileName);
            }

            var metadata = CreateMetadataForMainForm();
            await _blobOperations.AddStreamToBlobStorageAsync(BlobStorageForAttachments, attachmentContainerReference, MainFormFileName, mainFormPdf, "application/pdf", metadata);

            addedFileNames.Add(MainFormFileName, 1);
        }

        private async Task ProcessInstanceData(Instance instance, Dictionary<string, int> addedFileNames, ZipArchive zipArchive, string attachmentContainerReference)
        {
            foreach (var dataItem in instance.Data)
            {
                using (var scope = _logger.BeginScope(new Dictionary<string, string>
                {
                    { "DataType", dataItem.DataType },
                    { "DataId", dataItem.Id },
                }))
                {
                    _logger.LogInformation("Processing Instance data {DataType}, Id: {DataId}", dataItem.DataType, dataItem.Id);

                    if (AltinnDataTypesToIgnoreWhenProcessingAltinnInstanceData.Contains(dataItem.DataType))
                        continue;

                    var blobName = GenerateBlobName(dataItem, addedFileNames, out var blobNamePrefixMetadata);
                    await using var attachmentAsStream = await _altinn3Service.GetStreamFromAltinnInstanceAsync(dataItem);

                    if (attachmentAsStream.CanSeek)
                        attachmentAsStream.Position = 0;

                    switch (dataItem.DataType)
                    {
                        case var dt when dt.Equals(Valideringsrapport):
                            await HandleValideringsrapport(dataItem, blobName, blobNamePrefixMetadata, attachmentContainerReference);
                            break;

                        default:
                            await HandleAttachment(dataItem, zipArchive, attachmentAsStream, blobName, blobNamePrefixMetadata, attachmentContainerReference);
                            break;
                    }
                }
            }
        }

        private async Task SaveZipArchive(MemoryStream zipStream, Dictionary<string, int> addedFileNames, string attachmentContainerReference)
        {
            try
            {
                var zipCollectionBlobName = GenerateZipCollectionBlobName(addedFileNames);
                var metadataForZipCollection = CreateMetadataForZipCollection();

                if (zipStream.CanSeek)
                {
                    zipStream.Position = 0;
                }
                else
                {
                    throw new InvalidOperationException("Cannot reset the position of a closed or non-seekable stream.");
                }

                _logger.LogInformation("Starting to save zip archive to Blob Storage. Container Reference: {AttachmentContainerReference}", attachmentContainerReference);

                await SaveStreamToBlobAsync(BlobStorageEnum.Public, attachmentContainerReference, zipCollectionBlobName, zipStream, "application/x-zip", metadataForZipCollection);

                _logger.LogInformation("Successfully saved zip archive to Blob Storage.");
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to save zip archive to Blob Storage. Container Reference: {AttachmentContainerReference}", attachmentContainerReference);
                throw;
            }
        }

        private string GenerateBlobName(DataElement dataItem, Dictionary<string, int> addedFileNames, out KeyValuePair<string, string>? blobNamePrefixMetadata)
        {
            var blobName = !string.IsNullOrEmpty(dataItem.Filename) ? dataItem.Filename : $"{dataItem.DataType}.xml";

            if (!addedFileNames.TryAdd(blobName, 1))
            {
                var blobNamePrefix = $"{++addedFileNames[blobName]:00}_";
                blobName = $"{blobNamePrefix}{blobName}";
                blobNamePrefixMetadata = new KeyValuePair<string, string>(BlobStorageMetadataKeys.BlobNamePrefix, blobNamePrefix);
            }
            else
            {
                blobNamePrefixMetadata = null;
            }

            return blobName;
        }

        private async Task HandleValideringsrapport(DataElement dataItem, string blobName, KeyValuePair<string, string>? blobNamePrefixMetadata, string attachmentContainerReference)
        {
            _logger.LogInformation("Handle valideringsrapport");
            var metadata = CreateMetadataForValideringsrapport(dataItem, blobNamePrefixMetadata);
            await SaveStreamToBlobAsync(BlobStorageForAttachments, attachmentContainerReference, blobName, await _altinn3Service.GetStreamFromAltinnInstanceAsync(dataItem), dataItem.ContentType, metadata);
        }

        private async Task HandleAttachment(DataElement dataItem, ZipArchive zipArchive, Stream attachmentAsStream, string blobName, KeyValuePair<string, string>? blobNamePrefixMetadata, string attachmentContainerReference)
        {
            _logger.LogInformation("Start handling attachment {Filename}, Id: {DataId}", dataItem.Filename, dataItem.Id);

            await EnsureStreamIsSeekableAsync(attachmentAsStream);

            if (GenerateZipCollectionOfAttachments && ContentTypesForZip.Contains(dataItem.ContentType))
            {
                _logger.LogInformation("Adding attachment {Filename} to zip archive.", dataItem.Filename);
                AddAttachmentToZip(zipArchive, attachmentAsStream, dataItem.Filename);
                attachmentAsStream.Position = 0;
            }

            Dictionary<string, string> metadata;

            if (AttachmentIsSubform(dataItem))
            {
                _logger.LogInformation("Processing XML content for subform {DataId}.", dataItem.Id);
                var (dataFormatId, dataFormatVersion) = await ProcessXmlContentAsync(attachmentAsStream);
                metadata = CreateMetadataForSubform(dataItem, blobNamePrefixMetadata, dataFormatId, dataFormatVersion);
            }
            else
            {
                metadata = CreateMetadataForAttachments(dataItem, blobNamePrefixMetadata);
            }

            await SaveStreamToBlobAsync(BlobStorageForAttachments, attachmentContainerReference, blobName, attachmentAsStream, dataItem.ContentType, metadata);
        }

        private static async Task EnsureStreamIsSeekableAsync(Stream attachmentAsStream)
        {
            if (attachmentAsStream.CanSeek)
            {
                attachmentAsStream.Position = 0;
            }
            else
            {
                var memoryStream = new MemoryStream();
                await attachmentAsStream.CopyToAsync(memoryStream);
                attachmentAsStream = memoryStream;
                attachmentAsStream.Position = 0;
            }
        }

        /// <summary>
        /// Dataelementer som er subForm xml eller pdf
        /// Eksempel Gjennomføringsplan kan sendes inn som pdf eller xml. Dersom det er en xml blir det produsert en signert pdf.
        /// Alle disse har samme datatype, men kun xml og signert pdf skal håndteres som subform
        /// </summary>
        /// <param name="dataItem"></param>
        /// <returns></returns>
        private bool AttachmentIsSubform(DataElement dataItem)
        {
            return SubFormDataTypes.Contains(dataItem.DataType) &&
                   (dataItem.ContentType is "text/xml" or "application/xml" ||
                   (dataItem.Metadata != null && dataItem.Metadata.Select(m => m.Key).Contains(Tags.Vedleggsopplysninger.ReferanseXML.ToString()))); // Eneste måte vi har å skille generert og opplastet pdf.
        }

        // Hjelpefunksjon for å legge til metadata
        private void AddCommonMetadata(Dictionary<string, string> metadata, string type, string attachmentTypeName, DataElement dataItem = null)
        {
            _logger.LogInformation("Add common metadata");

            var dataType = GetDataType(attachmentTypeName);

            metadata[BlobStorageMetadataKeys.Type] = type;
            metadata[BlobStorageMetadataKeys.AttachmentTypeName] = dataType;
            if (dataItem == null)
            {
                _logger.LogDebug("DataItem is null, skips dataItem related metadata");
                return;
            }


            metadata[BlobStorageMetadataKeys.ExcludeInReceipt] = DatatypeWithExtensionToExludeInReceipt.Contains((dataType, dataItem.Filename.Split('.').Last())).ToString();

            metadata[Tags.Vedleggsopplysninger.VedleggFilnavn.ToString()] = dataItem.Filename;
            AddMetadataFromDataelement(metadata, dataItem.Metadata, typeof(Tags.Vedleggsopplysninger));
            if (SupportUserDefinedMetadata)
            {
                AddMetadataFromDataelement(metadata, dataItem.UserDefinedMetadata, typeof(Tags.VedleggsopplysningerFraSøker));
            }
        }

        private void AddMetadataFromDataelement(Dictionary<string, string> metadata, List<KeyValueEntry> metadataInput, Type validTagType)
        {
            _logger.LogInformation("Start processing metadata.");
            var filteredMetadata = GetFilteredMetadata(metadataInput, validTagType);

            foreach (var m in filteredMetadata)
            {
                if (IsValid(m.Key, m.Value))
                {
                    metadata.Add(m.Key, m.Value);
                }
            }
            _logger.LogInformation("Finished processing metadata.");
        }

        private Dictionary<string, string> GetFilteredMetadata(List<KeyValueEntry>? metadataInput, Type validTagType)
        {
            var metadata = new Dictionary<string, string>();

            if (metadataInput == null) return metadata;

            foreach (var entry in metadataInput.Where(e => Enum.IsDefined(validTagType, e.Key)))
            {
                if (!metadata.TryAdd(entry.Key, entry.Value))
                {
                    _logger.LogWarning(
                        "Key {key} already exists in metadata. Value: {existingValue}, Attempted value: {value}",
                        entry.Key,
                        metadata[entry.Key],
                        entry.Value
                    );
                }
            }

            return metadata;
        }

        // Hjelpefunksjon for å prosessere XML-innhold
        private async Task<(string dataFormatId, string dataFormatVersion)> ProcessXmlContentAsync(Stream stream)
        {
            string xmlString;

            using (var reader = new StreamReader(stream, leaveOpen: true))
            {
                xmlString = await reader.ReadToEndAsync();
            }

            stream.Position = 0;

            var dataFormatId = XmlUtil.GetAttributeValueFromXml(xmlString, "dataFormatId");

            var dataFormatVersion = XmlUtil.GetAttributeValueFromXml(xmlString, "dataFormatVersion");

            return (dataFormatId, dataFormatVersion);
        }

        private string GenerateZipCollectionBlobName(Dictionary<string, int> addedFileNames)
        {
            var zipCollectionBlobName = $"{DataFormatId}.zip";
            if (addedFileNames.ContainsKey(zipCollectionBlobName))
            {
                var blobNamePrefix = $"{++addedFileNames[zipCollectionBlobName]:00}_";
                zipCollectionBlobName = $"{blobNamePrefix}{zipCollectionBlobName}";
            }
            return zipCollectionBlobName;
        }

        private Dictionary<string, string> CreateMetadataForZipCollection()
        {
            return new Dictionary<string, string>
            {
                { BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.GeneratedAttachment },
                { BlobStorageMetadataKeys.AttachmentTypeName, BlobStorageMetadataTypes.AttachmentCollection }
            };
        }

        private static void AddAttachmentToZip(ZipArchive zipOutputStream, Stream attachment, string attachmentFileName)
        {
            attachment.Position = 0;
            var zipEntry = zipOutputStream.CreateEntry(attachmentFileName);
            using var entryStream = zipEntry.Open();
            attachment.CopyTo(entryStream);
            attachment.Flush();
            entryStream.Flush();
            entryStream.Close();
            attachment.Position = 0;
        }

        private bool IsValid(string key, string value)
        {
            switch (key)
            {
                case var k when k == Tags.VedleggsopplysningerFraSøker.VedleggVersjonsdato.ToString():
                    return ValidateDate(key, value);

                case var k when k == Tags.VedleggsopplysningerFraSøker.VedleggFulgtNabovarsel.ToString():
                    return ValidateVedleggFulgtNabovarselValue(key, value);

                // evt andre valideriger...

                default:
                    return true; // Returner true for alle andre keys
            }
        }

        private bool ValidateVedleggFulgtNabovarselValue(string key, string value)
        {
            if (bool.TryParse(value, out _))
            {
                _logger.LogInformation("Value '{value}' for key '{key}' is valid boolean", value, key);
                return true;
            }

            _logger.LogWarning("Value '{value}' for key '{key}' is not a valid boolean", value, key);
            return false;
        }

        private bool ValidateDate(string key, string value)
        {
            var culture = new CultureInfo("nb-NO"); // Norsk kulturinnstilling
            if (DateTime.TryParse(value, culture, DateTimeStyles.None, out var parsedDate))
            {
                _logger.LogInformation("Value '{value}' for key '{key}' was parsed as '{parsedDate}'", value, key, parsedDate.ToString("dd.MM.yyyy", culture));
                return true;
            }

            // Vi skal ikke ta bort innhold selv om det er ugyldig. Så returnerer derfor true, men logger at det er en ugyldig dato.
            _logger.LogWarning("Value '{value}' for key '{key}' is not a valid date", value, key);
            //return false;
            return true;
        }

        private async Task<Dictionary<string, string>> CreateMetadataForFormData(string attachmentContainerReference,
                                                                                 string instanceSource,
                                                                                 DataElement dataItem,
                                                                                 Stream formDataStream)
        {
            _logger.LogInformation("Create metadata for form data");
            Dictionary<string, string> metadata = new();
            AddCommonMetadata(metadata, BlobStorageMetadataTypes.FormData, MainFormAttachmentTypeName, dataItem);
            metadata[BlobStorageMetadataKeys.InstanceSource] = instanceSource;
            metadata[Tags.Vedleggsopplysninger.ReferanseXML.ToString()] = dataItem.Id;

            var (dataFormatId, dataFormatVersion) = await ProcessXmlContentAsync(formDataStream);

            if (dataFormatId != null) metadata[Tags.DataFormatId] = dataFormatId;
            if (dataFormatVersion != null) metadata[Tags.DataFormatVersion] = dataFormatVersion;

            if (attachmentContainerReference != null)
                metadata[BlobStorageMetadataKeys.PublicBlobContainerName] = attachmentContainerReference;

            return metadata;
        }

        private Dictionary<string, string> CreateMetadataForMainForm(KeyValuePair<string, string>? blobNamePrefixMetadata = null, DataElement dataItem = null, string formDataDataElementId = null)
        {
            Dictionary<string, string> metadata = new();
            AddCommonMetadata(metadata, BlobStorageMetadataTypes.MainForm, MainFormAttachmentTypeName, dataItem);

            if (blobNamePrefixMetadata.HasValue)
                metadata[blobNamePrefixMetadata.Value.Key] = blobNamePrefixMetadata.Value.Value;

            if (formDataDataElementId != null) metadata[Tags.Vedleggsopplysninger.ReferanseXML.ToString()] = formDataDataElementId;

            return metadata;
        }

        private Dictionary<string, string> CreateMetadataForValideringsrapport(DataElement dataItem, KeyValuePair<string, string>? blobNamePrefixMetadata = null)
        {
            Dictionary<string, string> metadata = new();
            AddCommonMetadata(metadata, BlobStorageMetadataTypes.ValidationResult, Valideringsrapport, dataItem);

            if (blobNamePrefixMetadata.HasValue)
                metadata[blobNamePrefixMetadata.Value.Key] = blobNamePrefixMetadata.Value.Value;

            return metadata;
        }

        private Dictionary<string, string> CreateMetadataForAttachments(DataElement dataItem, KeyValuePair<string, string>? blobNamePrefixMetadata)
        {
            _logger.LogInformation("Create metadata for attachments");
            Dictionary<string, string> metadata = new();

            AddCommonMetadata(metadata, BlobStorageMetadataTypes.SubmittalAttachment, dataItem.DataType, dataItem);

            if (blobNamePrefixMetadata.HasValue)
                metadata[blobNamePrefixMetadata.Value.Key] = blobNamePrefixMetadata.Value.Value;

            return metadata;
        }

        private Dictionary<string, string> CreateMetadataForSubform(DataElement dataItem, KeyValuePair<string, string>? blobNamePrefixMetadata, string dataFormatId, string dataFormatVersion)
        {
            _logger.LogInformation("Create metadata for subforms");
            Dictionary<string, string> metadata = new();

            AddCommonMetadata(metadata, BlobStorageMetadataTypes.Subform, dataItem.DataType, dataItem);
            metadata[Tags.SubformName] = Path.GetFileNameWithoutExtension(dataItem.Filename);

            if (blobNamePrefixMetadata.HasValue)
                metadata[blobNamePrefixMetadata.Value.Key] = blobNamePrefixMetadata.Value.Value;

            if (dataItem.ContentType is "text/xml" or "application/xml")
            {
                if (dataFormatId != null) metadata[Tags.DataFormatId] = dataFormatId;
                if (dataFormatVersion != null) metadata[Tags.DataFormatVersion] = dataFormatVersion;

                metadata[Tags.Vedleggsopplysninger.ReferanseXML.ToString()] = dataItem.Id;
            }

            return metadata;
        }

        /// <summary>
        /// Mapper enkelte dataItemDataType til en annen datatype.
        /// </summary>
        /// <param name="dataItemDataType"></param>
        /// <returns></returns>
        private string GetDataType(string dataItemDataType)
        {
            var dataType = dataItemDataType switch
            {
                var dt when dt.Equals(GjennomføringsplanPdf) => Gjennomføringsplan,
                _ => dataItemDataType
            };

            return dataType;
        }

        private async Task SaveStreamToBlobAsync(
            BlobStorageEnum blobStorageEnum,
            string containerReference,
            string blobName,
            Stream attachment,
            string blobContentType,
            Dictionary<string, string> metadata)
        {
            try
            {
                _logger.LogInformation("Start saving stream to Blob Storage");
                attachment.Position = 0;
                await _blobOperations.AddStreamToBlobStorageAsync(blobStorageEnum, containerReference, blobName, attachment, blobContentType, metadata);
                _logger.LogInformation("Stream saved to Blob Storage");
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "ArchiveReference: {containerReference}", containerReference);
                throw;
            }
        }

        protected abstract string GetPlankonsulentOrganisationNumber(TFormData formData);
    }
}