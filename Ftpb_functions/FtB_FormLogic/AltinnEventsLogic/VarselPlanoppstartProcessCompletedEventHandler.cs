using Altinn3.Adapters;
using Dibk.Ftpb.Common.Constants;
using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.ModelToPdf.Common.Client;
using Dibk.Ftpb.ModelToPdf.Common.Models;
using FtB_Common;
using FtB_Common.BusinessModels;
using FtB_Common.Constants;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.Storage;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using static Dibk.Ftpb.Common.Constants.DataTypes;

namespace FtB_FormLogic.AltinnEventsLogic
{
    [Altinn(Resource = AltinnEventResources.VarselPlanoppstart, Type = AltinnEventTypes.InstanceCompleted)]
    public class VarselPlanoppstartProcessCompletedEventHandler : BaseProcessCompletedEventHandler<PlanvarselV2>
    {
        protected override string DataFormatId => DataFormatIDs.VarselPlanoppstart;
        protected override string FormDataDataType => Planvarsel;
        protected override string MainFormFileName => $"{AttachmentDisplayName.Planvarsel}.pdf";
        protected override string MainFormAttachmentTypeName => Planvarsel;
        protected override string DataModelType => DataModelTypes.VarselPlanoppstart;

        protected override bool GeneratePdfForDataModelType => false;
        protected override bool GenerateZipCollectionOfAttachments => true;
        protected override bool SupportUserDefinedMetadata => false;

        protected override BlobStorageEnum BlobStorageForAttachments => BlobStorageEnum.Public;

        protected override List<string> AltinnDataTypesToIgnoreWhenProcessingAltinnInstanceData =>
        [
            .. base.AltinnDataTypesToIgnoreWhenProcessingAltinnInstanceData,
            RefDataAsPdf,
            Valideringsrapport
        ];

        protected override List<(string datatype, string extension)> DatatypeWithExtensionToExludeInReceipt => new();

        public VarselPlanoppstartProcessCompletedEventHandler(
            ILogger<VarselPlanoppstartProcessCompletedEventHandler> logger,
            IBlobOperations blobOperations,
            Altinn3Service altinn3Service,
            IDecryptionFactory decryptionFactory,
            IDatamodelToPdfHttpClient datamodelToPdfHttpClient,
            EnvironmentProvider env
            ) : base(logger, blobOperations, altinn3Service, decryptionFactory, datamodelToPdfHttpClient, env)
        {
        }

        protected override string GetPlankonsulentOrganisationNumber(PlanvarselV2 formData)
        {
            return formData.Plankonsulent?.Organisasjonsnummer;
        }
    }
}
