using Altinn3.Adapters;
using Dibk.Ftpb.Common.Constants;
using Dibk.Ftpb.ModelToPdf.Common.Client;
using Dibk.Ftpb.ModelToPdf.Common.Models;
using FtB_Common;
using FtB_Common.BusinessModels;
using FtB_Common.Constants;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.Storage;
using Microsoft.Extensions.Logging;
using no.kxml.skjema.dibk.uttalelseOffentligEttersyn;
using System.Collections.Generic;
using static Dibk.Ftpb.Common.Constants.DataTypes;

namespace FtB_FormLogic.AltinnEventsLogic
{
    [Altinn(Resource = AltinnEventResources.UttalelseHoffe, Type = AltinnEventTypes.InstanceCompleted)]
    public class UttalelseHoffeProcessCompletedEventHandler : BaseProcessCompletedEventHandler<UttalelseOffentligEttersynType>
    {
        protected override string DataFormatId => DataFormatIDs.UttalelseHoeringOgOffentligEttersyn;
        protected override string FormDataDataType => UttalelseOffentligEttersyn;
        protected override string MainFormFileName => $"{AttachmentDisplayName.UttalelseOffentligEttersyn}.pdf";
        protected override string MainFormAttachmentTypeName => Uttalelsebrev;
        protected override string DataModelType => DataModelTypes.UttalelseHoffe;

        protected override bool GeneratePdfForDataModelType => true;
        protected override bool GenerateZipCollectionOfAttachments => false;

        protected override BlobStorageEnum BlobStorageForAttachments => BlobStorageEnum.Private;
        protected override bool SupportUserDefinedMetadata => false;

        protected override List<string> AltinnDataTypesToIgnoreWhenProcessingAltinnInstanceData =>
        [
            .. base.AltinnDataTypesToIgnoreWhenProcessingAltinnInstanceData,
            RefDataAsPdf,
            Valideringsrapport
        ];
        protected override List<(string datatype, string extension)> DatatypeWithExtensionToExludeInReceipt => new();

        public UttalelseHoffeProcessCompletedEventHandler(
            ILogger<UttalelseHoffeProcessCompletedEventHandler> logger,
            IBlobOperations blobOperations,
            Altinn3Service altinn3Service,
            IDecryptionFactory decryptionFactory,
            IDatamodelToPdfHttpClient datamodelToPdfHttpClient,
            EnvironmentProvider env
            ) : base(logger, blobOperations, altinn3Service, decryptionFactory, datamodelToPdfHttpClient, env)
        {
        }

        protected override string GetPlankonsulentOrganisationNumber(UttalelseOffentligEttersynType formData)
        {
            return formData.plankonsulent?.organisasjonsnummer;
        }
    }
}
