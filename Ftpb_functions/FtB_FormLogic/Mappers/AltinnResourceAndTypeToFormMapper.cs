﻿using FtB_Common.BusinessModels;
using FtB_FormLogic.BaseLogic.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace FtB_FormLogic.Mappers
{
    public class AltinnResourceAndTypeToFormMapper
    {
        private readonly IServiceProvider _services;
        private readonly ILogger<AltinnResourceAndTypeToFormMapper> _log;

        public AltinnResourceAndTypeToFormMapper(IServiceProvider services, ILogger<AltinnResourceAndTypeToFormMapper> log)
        {
            Debug.WriteLine("Constructor AltinnResourceAndTypeToFormMapper");
            _services = services;
            _log = log;
        }

        public IAltinnLogic<T, U> GetAltinnFormLogic<T,U>(string resource, string altinnType)
        {
            //Retrieves classes implementing IAltinnLogic, filtering altinnType and resource
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(t => t.IsDefined(typeof(AltinnAttribute), true))
                .Where(t => t.GetCustomAttribute<AltinnAttribute>().Resource == resource)
                .Where(t => t.GetCustomAttribute<AltinnAttribute>().Type == altinnType);

            object altinnLogic = null;
            if (types.Count() > 0)
            {
                //Resolves an instance of the class
                var formType = types.FirstOrDefault();
                altinnLogic = _services.GetService(formType);
            }

            return altinnLogic as IAltinnLogic<T, U>;
        }
    }
}
