﻿using Altinn.Common.Models;
using FtB_Common.Encryption;
using FtB_Common.Interfaces;
using FtB_DataModels.Mappers;

namespace FtB_FormLogic.Distributions
{
    public class SendDataProviderBase
    {        
        protected readonly IHtmlUtils _htmlUtils;
        private readonly IDecryption _decryptor;
        public SendDataProviderBase(IHtmlUtils htmlUtils, IDecryptionFactory decryptionFactory)
        {
            _decryptor = decryptionFactory.GetDecryptor();
            _htmlUtils = htmlUtils;
        }

        public virtual AltinnReceiver GetReceiver(DistributionReceiver receivingPart)
        {
            var receiver = new AltinnReceiver();
            if (string.IsNullOrEmpty(receivingPart.Ssn) && string.IsNullOrEmpty(receivingPart.Orgnr))
                throw new System.Exception("Receiver information doesn't exist in prefill data");


            if (!string.IsNullOrEmpty(receivingPart.Ssn))
            {
                receiver.Id = _decryptor.DecryptText(receivingPart.Ssn);
                receiver.Type = AltinnReceiverType.Privatperson;
            }
            else
            {
                receiver.Id = receivingPart.Orgnr;
                receiver.Type = AltinnReceiverType.Foretak;
            }

            return receiver;
        }
    }
}
