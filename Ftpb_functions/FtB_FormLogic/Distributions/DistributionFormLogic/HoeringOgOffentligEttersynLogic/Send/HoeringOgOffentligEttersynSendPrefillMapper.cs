﻿using FtB_Common.Encryption;
using FtB_Common.Interfaces;
using FtB_DataModels.Mappers;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using no.kxml.skjema.dibk.offentligEttersyn;
using no.kxml.skjema.dibk.uttalelseOffentligEttersyn;

namespace FtB_FormLogic.Distributions.DistributionFormLogic.HoeringOgOffentligEttersynLogic.Send
{
    public class HoeringOgOffentligEttersynSendPrefillMapper : IFormMapper<OffentligEttersynType>
    {
        private readonly IDecryptionFactory _decryptionFactory;
        private readonly ILogger<HoeringOgOffentligEttersynSendPrefillMapper> _logger;

        public HoeringOgOffentligEttersynSendPrefillMapper(IDecryptionFactory decryptionFactory, ILogger<HoeringOgOffentligEttersynSendPrefillMapper> logger)
        {
            _decryptionFactory = decryptionFactory;
            _logger = logger;
        }

        public IEnumerable<IPrefillData> Map(OffentligEttersynType form, string receiverId, string receiverName, Guid distributionFormReferenceId)
        {
            //Find all "berort part" for same receiver
            var decryptor = _decryptionFactory.GetDecryptor();
            var decryptedReceiverId = decryptor.DecryptText(receiverId);

            var berørteParter = form.beroerteParter?
                .Where(b => ((b.foedselsnummer != null && decryptor.DecryptText(b.foedselsnummer) != null && decryptor.DecryptText(b.foedselsnummer).Equals(decryptedReceiverId)) ||
                    (b.organisasjonsnummer != null && b.organisasjonsnummer.Equals(decryptedReceiverId))) &&
                    b.navn.Equals(receiverName))
                .ToList();

            var svarPaaHoeringOgOffentligEttersyns = new List<HoeringOgOffentligEttersynData>();
            foreach (var berørtPart in berørteParter)
            {
                var svarPaaHoeringOgOffentligEttersyn = new UttalelseOffentligEttersynType();

                var parttypemapper = HoeringOgOffentligEttersynMappers.GetHoeringOgOffentligEttersynPartTypeMapper();

                svarPaaHoeringOgOffentligEttersyn.forslagsstiller = parttypemapper.Map<no.kxml.skjema.dibk.offentligEttersyn.PartType, no.kxml.skjema.dibk.uttalelseOffentligEttersyn.PartType>(form.forslagsstiller);
                if (svarPaaHoeringOgOffentligEttersyn.forslagsstiller != null)
                {
                    svarPaaHoeringOgOffentligEttersyn.forslagsstiller.epost = form.forslagsstiller.epost;
                    svarPaaHoeringOgOffentligEttersyn.forslagsstiller.telefon = form.forslagsstiller.telefon;
                }

                svarPaaHoeringOgOffentligEttersyn.plankonsulent = parttypemapper.Map<no.kxml.skjema.dibk.offentligEttersyn.PartType, no.kxml.skjema.dibk.uttalelseOffentligEttersyn.PartType>(form.planKonsulent);
                if (svarPaaHoeringOgOffentligEttersyn.plankonsulent != null)
                {
                    svarPaaHoeringOgOffentligEttersyn.plankonsulent.epost = form.planKonsulent?.epost;
                    svarPaaHoeringOgOffentligEttersyn.plankonsulent.telefon = form.planKonsulent?.telefon;
                }
                else
                {
                    svarPaaHoeringOgOffentligEttersyn.plankonsulent = new() { partstype = new(), adresse = new()};
                }

                svarPaaHoeringOgOffentligEttersyn.beroertPart = HoeringOgOffentligEttersynMappers.GetHoeringOgOffentligEttersynBerortPartMapper()
                    .Map<no.kxml.skjema.dibk.offentligEttersyn.BeroertPartType, no.kxml.skjema.dibk.uttalelseOffentligEttersyn.BeroertPartType>(berørtPart);

                //distribution form reference id is used to connect the distribution form row with
                // the form when FTPB receives it from the notified party. 
                svarPaaHoeringOgOffentligEttersyn.hovedinnsendingsnummer = distributionFormReferenceId.ToString();
                svarPaaHoeringOgOffentligEttersyn.fraSluttbrukersystem = form.metadata.fraSluttbrukersystem;
                svarPaaHoeringOgOffentligEttersyn.planNavn = form.planforslag?.plannavn;
                svarPaaHoeringOgOffentligEttersyn.planid = form.planforslag?.arealplanId;
                svarPaaHoeringOgOffentligEttersyn.kommune = parttypemapper.Map<no.kxml.skjema.dibk.offentligEttersyn.PartType, no.kxml.skjema.dibk.uttalelseOffentligEttersyn.PartType>(form.kommune);
                svarPaaHoeringOgOffentligEttersyn.kommune = svarPaaHoeringOgOffentligEttersyn.kommune == null ? new() { adresse = new(), partstype = new() } : svarPaaHoeringOgOffentligEttersyn.kommune;
                svarPaaHoeringOgOffentligEttersyn.kommunenavn = form.kommunenavn;
                svarPaaHoeringOgOffentligEttersyn.fristForUttalelse = form.planforslag?.fristForUttalelse;
                svarPaaHoeringOgOffentligEttersyn.fristForUttalelseSpecified = form.planforslag.fristForUttalelseSpecified;
                svarPaaHoeringOgOffentligEttersyn.saksnummer = form.planforslag.kommunensSaksnummer?.saksaar +
                                               form.planforslag.kommunensSaksnummer?.sakssekvensnummer;
                svarPaaHoeringOgOffentligEttersyn.signatur = new no.kxml.skjema.dibk.uttalelseOffentligEttersyn.SignaturType();

                svarPaaHoeringOgOffentligEttersyns.Add(new HoeringOgOffentligEttersynData(svarPaaHoeringOgOffentligEttersyn, form.metadata.hovedinnsendingsnummer, form));
            }

            return svarPaaHoeringOgOffentligEttersyns;
        }
    }
}