﻿using Altinn.Common.Models;
using FtB_Common.BusinessModels;
using FtB_Common.Encryption;
using FtB_Common.Interfaces;
using FtB_DataModels.Mappers;
using FtB_FormLogic.Distributions;
using FtB_FormLogic.Distributions.DistributionFormLogic.HoeringOgOffentligEttersynLogic.Send;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FtB_Common.Enums;
using no.kxml.skjema.dibk.offentligEttersyn;
using no.kxml.skjema.dibk.uttalelseOffentligEttersyn;
using static Dibk.Ftpb.Common.Constants.DataTypes;
using BeroertPartType = no.kxml.skjema.dibk.uttalelseOffentligEttersyn.BeroertPartType;
using GjelderEiendomType = no.kxml.skjema.dibk.uttalelseOffentligEttersyn.GjelderEiendomType;
using PartType = no.kxml.skjema.dibk.uttalelseOffentligEttersyn.PartType;

namespace FtB_FormLogic
{
    public class HoeringOgOffentligEttersynSendDataProvider : SendDataProviderBase, IDistributionDataMapper<OffentligEttersynType>
    {
        public HoeringOgOffentligEttersynSendDataProvider(IHtmlUtils htmlUtils, IDecryptionFactory decryptionFactory) : base(htmlUtils, decryptionFactory)
        {
        }

        public IDistributionMessage GetDistributionMessage(IEnumerable<IPrefillData> prefills, OffentligEttersynType mainFormData, Guid distributionFormId, string archiveReference, DistributionSender distributionSender, bool digitallyReserved)
        {
            var prefill = prefills.First() as HoeringOgOffentligEttersynData;

            var distributionMessage = new HoeringOgOffentligEttersynDistributionMessage()
            {
                AppId = "dibk/uttalelse-hoffe",
                PrefillDataFormatId = prefill.DataFormatId,
                PrefillDataFormatVersion = prefill.DataFormatVersion,
                DistributionFormReferenceId = distributionFormId,
                PrefillServiceCode = prefill.PrefillServiceCode,
                PrefillServiceEditionCode = prefill.PrefillServiceEditionCode,
                PrefilledXmlDataString = prefill.ToString(),
                DaysValid = 14,
                DueDate = null,
                IsHoeringsmyndighet = prefill.FormInstance.beroertPart.erHoeringsmyndighet.HasValue ? prefill.FormInstance.beroertPart.erHoeringsmyndighet.Value : false,
                DigitallyReserved = digitallyReserved,
                ExpectedFormDataType = UttalelseOffentligEttersyn
            };

            distributionMessage.PrefillData = new List<AltinnPrefillData>
            {
                new AltinnPrefillData(UttalelseOffentligEttersyn, prefill.ToString(), "application/xml", null, null),
            };

            distributionMessage.NotificationMessage.Receiver = base.GetReceiver(HoeringOgOffentligEttersynMappers.GetHoeringOgOffentligEttersynReceiverMapper().Map<DistributionReceiver>(prefill.FormInstance.beroertPart));
            distributionMessage.NotificationMessage.MessageData = CreateMessageData(prefill.FormInstance, distributionSender);
            distributionMessage.NotificationMessage.ArchiveReference = archiveReference;
            distributionMessage.NotificationMessage.ReplyLink = CreateReplyLink();
            distributionMessage.NotificationMessage.NotificationTemplate = "DIBK-nabo-1";
            distributionMessage.NotificationMessage.SenderEmail = "noreply@noreply.no";

            //Add notifications
            var fristForInnspillFormatted = prefill.FormInstance.fristForUttalelse?.ToString("dd.MM.yyyy");
            var smsContent = GetSMSNotificationMessage(
                prefill.FormInstance.beroertPart.organisasjonsnummer,
                prefill.FormInstance.beroertPart.navn,
                prefill.FormInstance.kommunenavn,
                distributionSender
            );

            string avsenderOrgNummer = "";
            if (!string.IsNullOrEmpty(prefill.FormInstance.plankonsulent?.partstype?.kodeverdi))
            {
                if (prefill.FormInstance.plankonsulent?.partstype?.kodeverdi != ActorTypeEnum.Privatperson.ToString())
                {
                    avsenderOrgNummer = prefill.FormInstance.plankonsulent.organisasjonsnummer;
                }
            }
            else
            {
                if (prefill.FormInstance.forslagsstiller?.partstype?.kodeverdi != ActorTypeEnum.Privatperson.ToString())
                {
                    avsenderOrgNummer = prefill.FormInstance.forslagsstiller.organisasjonsnummer;
                }
            }

            var emailContent = GetEmailNotificationBody(
                prefill.FormInstance.beroertPart.organisasjonsnummer,
                prefill.FormInstance.beroertPart.navn,
                prefill.FormInstance.kommunenavn,
                prefill.FormInstance.planNavn,
                prefill.FormInstance.forslagsstiller.navn,
                fristForInnspillFormatted,
                distributionSender,
                archiveReference,
                avsenderOrgNummer
            );

            var notifications = new List<Notification>
            {
                new Notification()
                {
                    EmailContent = emailContent,
                    EmailSubject = $"Høring og offentlig ettersyn av reguleringsplan {mainFormData.planforslag.plannavn}",
                    SmsContent = smsContent,
                    Receiver = prefill.FormInstance.beroertPart.epost
                }
            };

            distributionMessage.NotificationMessage.Notifications = notifications;

            return distributionMessage;
        }

        private MessageDataType CreateMessageData(UttalelseOffentligEttersynType prefillData, DistributionSender distributionSender)
        {
            var body = GetPrefillNotificationBody(distributionSender, prefillData.forslagsstiller, prefillData.beroertPart, prefillData.fristForUttalelse, prefillData.kommunenavn);
            var summary = string.Empty;
            var title = GetPrefillNotificationTitle(prefillData.planNavn, prefillData.planid);
            return new MessageDataType() { MessageBody = body, MessageSummary = summary, MessageTitle = title };
        }

        private ReplyLink CreateReplyLink()
        {
            var replyLink = new ReplyLink()
            {
                //URL settes av adapteret..
                UrlTitle = "Trykk her for å fylle ut svarskjemaet"
            };
            return replyLink;

        }

        public string GetPrefillNotificationBody(DistributionSender avsenderDistribusjon, PartType forslagsstiller, BeroertPartType beroertPart, DateTime? fristForInnspill, string kommune)
        {
            string datoFristInnspill = fristForInnspill?.ToString("dd.MM.yyyy");
            string htmlBody = _htmlUtils.GetHtmlFromTemplate("FtB_FormLogic.Distributions.DistributionFormLogic.HoeringOgOffentligEttersynLogic.Send.VarselHoeringOgOffentligEttersynPrefillNotificationBody.html");
            htmlBody = htmlBody.Replace("<beroertPart.navn />", beroertPart.navn);
            htmlBody = htmlBody.Replace("<kommune />", kommune);
            htmlBody = htmlBody.Replace("<forslagsstiller.navn />", forslagsstiller.navn);
            htmlBody = htmlBody.Replace("<avsenderDistribusjon.navn />", avsenderDistribusjon.Name);
            htmlBody = htmlBody.Replace("<avsenderDistribusjon.epost />", avsenderDistribusjon.Email);
            htmlBody = htmlBody.Replace("<avsenderDistribusjon.telefonnummer />", avsenderDistribusjon.Phone);
            htmlBody = htmlBody.Replace("<datoFristInnspill />", datoFristInnspill);
            htmlBody = htmlBody.Replace("<gjeldendeEiendom />", BeroerteEiendommerHasAddressInformation(beroertPart.gjelderEiendommer)
                    ? $":<br/>{string.Join("<br/>", GetBeroertEiendomInformation(beroertPart.gjelderEiendommer))}"
                    : " området vi vil regulere.");
            if (avsenderDistribusjon.Role is DistributionSenderRoleEnum.Plankonsulent)
                htmlBody = htmlBody.Replace("<paaVegneAvForslagsstiller />", $", på vegne av {forslagsstiller.navn}");

            return htmlBody;
        }

        public static string GetPrefillNotificationTitle(string planNavn, string planid)
        {
            return $"Høring og offentlig ettersyn av reguleringsplan - {planNavn}";
        }

        private static string GetSMSNotificationMessage(string orgnr, string berortPartNavn, string kommunenavn, DistributionSender avsenderDistribusjon)
        {
            string evtOrgnr = string.Empty;
            if (!string.IsNullOrEmpty(orgnr)) evtOrgnr = $" (org.nr. {orgnr})";

            var notificationBuilder = new StringBuilder();
            notificationBuilder.Append($"{berortPartNavn}{evtOrgnr} har fått brev om høring og offentlig ettersyn av reguleringsplan i {kommunenavn} kommune. ");
            notificationBuilder.Append($"Logg inn på Altinn for å se brevet. ");
            notificationBuilder.Append($"Hilsen {avsenderDistribusjon.Role} {avsenderDistribusjon.Name}");

            return notificationBuilder.ToString();
        }

        private static string GetEmailNotificationBody(
            string orgnr,
            string nabo,
            string kommunenavn,
            string plannavn,
            string forslagsstillerNavn,
            string fristForInnspill,
            DistributionSender avsenderDistribusjon,
            string archiveReference,
            string avsenderOrgNummer)
        {
            string evtOrgnr = "";
            if (!string.IsNullOrEmpty(orgnr)) evtOrgnr = $" (org.nr. {orgnr})";

            // Dersom berørt part er en organisasjon


            // Melding
            var notificationBuilder = new StringBuilder();

            notificationBuilder.AppendLine($"Til {nabo}{evtOrgnr}");

            notificationBuilder.AppendLine($"<p>");
            notificationBuilder.Append($"Vi har sendt informasjon om høring og offentlig ettersyn av reguleringsplan i {kommunenavn} kommune. ");
            notificationBuilder.AppendLine($"Navnet på reguleringsplanen er: {plannavn}.");
            notificationBuilder.AppendLine($"</p>");

            notificationBuilder.AppendLine($"<p>");
            notificationBuilder.AppendLine($"Vi sender deg dette brevet fordi du kan være berørt eller ha interesser i nærheten av området vi vil regulere.");
            notificationBuilder.AppendLine($"</p>");

            notificationBuilder.AppendLine($"<p>");
            notificationBuilder.AppendLine($"<strong>Du kan uttale deg om reguleringsplanen</strong><br/>");
            notificationBuilder.AppendLine($"Logg inn på Altinn for å se brevet og svar innen {fristForInnspill}.");
            notificationBuilder.AppendLine($"</p>");

            if (!string.IsNullOrEmpty(orgnr))
            {
                notificationBuilder.AppendLine($"<p>");
                notificationBuilder.AppendLine($"<strong>Om tilgang i Altinn</strong><br/>");
                notificationBuilder.AppendLine($"For å se brevet, må du representere {nabo} {evtOrgnr} i Altinn. <br/>");
                notificationBuilder.Append($"Den som skal se og gi en uttalelse, må ha rollen «Plan- og byggesak» i organisasjonen. Les mer på dibk.no/rettigheter-i-altinn.<br>");
                notificationBuilder.AppendLine($"</p>");
            }


            notificationBuilder.AppendLine($"<p>");
            notificationBuilder.AppendLine($"Med vennlig hilsen,<br/>");
            notificationBuilder.Append($"{avsenderDistribusjon.Name}");
            if (avsenderDistribusjon.Role is DistributionSenderRoleEnum.Plankonsulent)
                notificationBuilder.Append($", på vegne av forslagsstiller {forslagsstillerNavn}");
            notificationBuilder.AppendLine();
            notificationBuilder.AppendLine($"<br/>");

            if (!string.IsNullOrWhiteSpace(avsenderDistribusjon.Phone))
                notificationBuilder.AppendLine($"Telefon: {avsenderDistribusjon.Phone} <br/>");

            if (!string.IsNullOrWhiteSpace(avsenderDistribusjon.Email))
                notificationBuilder.AppendLine($"E-post: {avsenderDistribusjon.Email}");

            notificationBuilder.AppendLine($"</p>");

            string avsenderId = string.IsNullOrEmpty(avsenderOrgNummer) ? "" : $" (org. nr. {avsenderOrgNummer})";
            notificationBuilder.AppendLine($"<p><em>Det er ikke mulig å svare på denne e-posten. Har du spørsmål om høring og offentlig ettersyn i Altinn, kan du kontakte Direktoratet for byggkvalitet. Oppgi referanse: {archiveReference}<br/>Spørsmål om inneholdet i brevet, må du rette til {avsenderDistribusjon.Name}{avsenderId}.</em></p>");
            return notificationBuilder.ToString();
        }

        private bool BeroerteEiendommerHasAddressInformation(GjelderEiendomType[] eiendommer)
        {
            if (eiendommer?.Length == 0)
                return false;

            return eiendommer.Any(e =>
                !string.IsNullOrWhiteSpace(e?.adresse?.adresselinje1) ||
                (!string.IsNullOrWhiteSpace(e?.eiendomsidentifikasjon?.gaardsnummer) &&
                 !string.IsNullOrWhiteSpace(e.eiendomsidentifikasjon.bruksnummer)
                ));
        }

        private static IEnumerable<string> GetBeroertEiendomInformation(GjelderEiendomType[] eiendommer)
        {
            foreach (var eiendom in eiendommer)
            {
                var sb = new StringBuilder();
                var hasAdresselinje1 = !string.IsNullOrWhiteSpace(eiendom.adresse.adresselinje1);
                var hasGaardsnummer = !string.IsNullOrWhiteSpace(eiendom.eiendomsidentifikasjon.gaardsnummer);
                var hasFestenummer = !string.IsNullOrWhiteSpace(eiendom.eiendomsidentifikasjon.festenummer);
                var hasSeksjonsnummer = !string.IsNullOrWhiteSpace(eiendom.eiendomsidentifikasjon.seksjonsnummer);

                if (hasAdresselinje1)
                {
                    sb.Append(eiendom.adresse.adresselinje1);

                    if (hasGaardsnummer || hasFestenummer || hasSeksjonsnummer)
                        sb.Append(" (");
                }

                if (hasGaardsnummer)
                {
                    if (!string.IsNullOrWhiteSpace(eiendom.eiendomsidentifikasjon.bruksnummer))
                        sb.Append($"gnr./bnr. {eiendom.eiendomsidentifikasjon.gaardsnummer}/{eiendom.eiendomsidentifikasjon.bruksnummer}");
                    else
                        sb.Append($"gnr. {eiendom.eiendomsidentifikasjon.gaardsnummer}");

                    if (hasFestenummer || hasSeksjonsnummer)
                        sb.Append(", ");
                }

                if (hasFestenummer)
                {
                    sb.Append($"festenr. {eiendom.eiendomsidentifikasjon.festenummer}");

                    if (hasSeksjonsnummer)
                        sb.Append(", ");
                }

                if (hasSeksjonsnummer)
                {
                    sb.Append($"seksjonsnr. {eiendom.eiendomsidentifikasjon.seksjonsnummer}");
                }

                if (hasAdresselinje1)
                    sb.Append(')');

                yield return sb.ToString();
            }
        }
    }
}
