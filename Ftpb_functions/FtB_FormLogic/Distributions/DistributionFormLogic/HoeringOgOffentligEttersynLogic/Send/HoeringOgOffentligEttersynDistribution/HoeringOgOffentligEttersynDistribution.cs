﻿using Altinn.Common;
using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using Altinn.Distribution;
using Altinn3.Adapters;
using Dibk.Ftpb.Integration.SvarUt;
using Dibk.Ftpb.Integration.SvarUt.Models;
using FtB_Common.Exceptions;
using KontaktOgReservasjonsRegisteret;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FtB_FormLogic.Distributions.DistributionFormLogic.HoeringOgOffentligEttersynLogic.Send
{
    public class HoeringOgOffentligEttersynDistributionMessage : AltinnDistributionMessage
    {
        public IEnumerable<Attachment> Attachments { get; set; }
        public Forsendelse SvarUtForsendelse { get; set; }
        public bool IsHoeringsmyndighet { get; set; }
    }

    public class HoeringOgOffentligEttersynDistribution : IDistributionAdapter
    {
        private readonly IPrefillAdapter _prefillAdapter;
        private readonly ISvarUtAdapter _svarUtAdapter;
        private readonly CorrespondenceAdapter _correspondenceAdapter;
        ILogger<HoeringOgOffentligEttersynDistribution> _logger;

        public HoeringOgOffentligEttersynDistribution(
            IPrefillAdapter prefillAdapter, ISvarUtAdapter svarUtAdapter, CorrespondenceAdapter correspondenceAdapter, ILogger<HoeringOgOffentligEttersynDistribution> logger)
        {
            _prefillAdapter = prefillAdapter;
            _svarUtAdapter = svarUtAdapter;
            _correspondenceAdapter = correspondenceAdapter;
            _logger = logger;
        }

        public Task<IAltinnPrefilledDistributionResult> SendCorrespondenceAsync(IDistributionMessage altinnMessage, string prefillReferenceId)
        {
            throw new NotImplementedException();

        }

        public async Task<IAltinnPrefilledDistributionResult> SendPrefillAndCorrespondenceAsync(IDistributionMessage altinnMessage)
        {
            var hoffeDistributionMessage = altinnMessage as HoeringOgOffentligEttersynDistributionMessage;
            var distResult = new Altinn3PrefillDistributionResult();

            if (hoffeDistributionMessage is { IsHoeringsmyndighet: true })
            {
                return await SendCorrespondenceForHoeringsmyndighetAsync(hoffeDistributionMessage, distResult);
            }

            try
            {
                AltinnPrefillResult prefillResult = new AltinnPrefillResult();
                if (altinnMessage.NotificationMessage.Receiver.Type == AltinnReceiverType.Privatperson)
                {
                    if (!hoffeDistributionMessage.DigitallyReserved)
                    {
                        prefillResult = await _prefillAdapter.SendPrefill(altinnMessage);
                        distResult.PrefillResult = prefillResult;
                    }
                    else
                    {
                        distResult.PrefillResult = new AltinnPrefillResult();
                        distResult.PrefillResult.Message = "User is reserved against digital communication or has expiered contact information";
                        distResult.PrefillResult.AltinnCommunicationResult = AltinnCommunicationResultType.ReservedReportee;
                        distResult.PrefillResult.PrefillReferenceId = "";
                        distResult.PrefillResult.PrefillAltinnReceivedTime = DateTime.Now;
                    }
                }
                else
                {
                    prefillResult = await _prefillAdapter.SendPrefill(altinnMessage);
                    distResult.PrefillResult = prefillResult;
                }
            }
            catch (Exception)
            {
                _logger.LogError("Failed to reach receiver digitally.");
                distResult.PrefillResult = new AltinnPrefillResult();
                distResult.PrefillResult.Message = "Failed to reach the receiver digitally";
                distResult.PrefillResult.AltinnCommunicationResult = AltinnCommunicationResultType.UnableToReachReceiver;
                distResult.PrefillResult.PrefillReferenceId = "";
                distResult.PrefillResult.PrefillAltinnReceivedTime = DateTime.Now;
            }

            if (distResult.PrefillResult.IsSuccessfull())
            {
                distResult.CorrespondenceResult = await _correspondenceAdapter.SendCorrespondence(altinnMessage, distResult.PrefillResult.PrefillAltinnReceiptId);
            }

            return distResult;
        }

        private async Task<IAltinnPrefilledDistributionResult> SendCorrespondenceForHoeringsmyndighetAsync(HoeringOgOffentligEttersynDistributionMessage hoffeDistributionMessage, Altinn3PrefillDistributionResult distResult)
        {
            try
            {
                var forsendelseID = await _svarUtAdapter.SendAsync(hoffeDistributionMessage.SvarUtForsendelse);
                if (forsendelseID != null)
                {
                    distResult.PrefillResult = new AltinnPrefillResult();
                    distResult.PrefillResult.Message = "AltinnPrefill skipped. Reason: høringsmyndighet";
                    distResult.PrefillResult.AltinnCommunicationResult = AltinnCommunicationResultType.NotUsed;
                    distResult.PrefillResult.PrefillAltinnReceiptId = forsendelseID;
                    distResult.PrefillResult.PrefillAltinnReceivedTime = DateTime.Now;

                    distResult.CorrespondenceResult = new AltinnCorrespondenceResult();
                    distResult.CorrespondenceResult.AltinnCommunicationResult = AltinnCommunicationResultType.Sent;
                    distResult.CorrespondenceResult.CorrespondenceAltinnReceiptId = forsendelseID;
                    return distResult;
                }
                else
                {
                    distResult.PrefillResult = new AltinnPrefillResult();
                    distResult.PrefillResult.Message = "AltinnPrefill skipped. Reason: høringsmyndighet";
                    distResult.PrefillResult.AltinnCommunicationResult = AltinnCommunicationResultType.NotUsed;
                    distResult.PrefillResult.PrefillAltinnReceivedTime = DateTime.Now;

                    distResult.CorrespondenceResult = new AltinnCorrespondenceResult();
                    distResult.CorrespondenceResult.AltinnCommunicationResult = AltinnCommunicationResultType.Failed;
                    distResult.CorrespondenceResult.Message = "UnknownError";
                    return distResult;
                }
            }
            catch (SvarUtRequestException svarUtException)
            {
                if (svarUtException.HttpStatusCode == System.Net.HttpStatusCode.BadRequest || svarUtException.HttpStatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    throw new CancelProcessException($"SvarUt-forsendelse failed with statuscode {svarUtException.HttpStatusCode} with message: {svarUtException.Message}");
                }
                else
                {
                    distResult.PrefillResult = new AltinnPrefillResult();
                    distResult.PrefillResult.Message = "AltinnPrefill skipped. Reason: høringsmyndighet";
                    distResult.PrefillResult.AltinnCommunicationResult = AltinnCommunicationResultType.NotUsed;
                    distResult.PrefillResult.PrefillAltinnReceivedTime = DateTime.Now;

                    distResult.CorrespondenceResult = new AltinnCorrespondenceResult();
                    distResult.CorrespondenceResult.AltinnCommunicationResult = AltinnCommunicationResultType.Failed;
                    distResult.CorrespondenceResult.Message = svarUtException.Message;
                    return distResult;
                }
            }
        }
    }
}
