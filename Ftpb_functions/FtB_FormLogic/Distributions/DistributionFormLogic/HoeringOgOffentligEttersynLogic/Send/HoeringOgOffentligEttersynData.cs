﻿using FtB_Common.Utils;
using no.kxml.skjema.dibk.offentligEttersyn;
using no.kxml.skjema.dibk.uttalelseOffentligEttersyn;

namespace FtB_FormLogic.Distributions.DistributionFormLogic.HoeringOgOffentligEttersynLogic.Send
{
    public class HoeringOgOffentligEttersynData : PrefillSendData<UttalelseOffentligEttersynType>
    {
        public HoeringOgOffentligEttersynData(UttalelseOffentligEttersynType formInstance, string hovedinnsendingsNummer, OffentligEttersynType hoeringOffentligEttersynVarsel) : base(formInstance)
        {
            InitialExternalSystemReference = hovedinnsendingsNummer;

            HoeringOgOffentligEttersynVarsel = SerializeUtil.Serialize(hoeringOffentligEttersynVarsel);
        }

        public override string PrefillFormName => "Uttalelse til høring og offentlig ettersyn";

        //public override string InitialExternalSystemReference { get => FormInstance.hovedinnsendingsnummer; set { FormInstance.hovedinnsendingsnummer = value; } }

        public override string ExternalSystemReference => FormInstance.beroertPart.systemReferanse;

        public override string PrefillServiceCode => "SvarUttalelsehoeringoOgOffentligEttersyn";

        public override string PrefillServiceEditionCode => "1";

        public string HoeringOgOffentligEttersynVarsel { get; set; }

    }
}
