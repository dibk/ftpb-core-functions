using FtB_Common.BusinessModels;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FtB_FormLogic.Distributions.DistributionFormLogic;
using FtB_FormLogic.Distributions.DistributionFormLogic.HoeringOgOffentligEttersynLogic;
using Ftb_Repositories.LogContstants;
using no.kxml.skjema.dibk.offentligEttersyn;
using FtB_Common.Constants;
using FtB_Common.BusinessModels.QueueMessageModels;
using KontaktOgReservasjonsregisteret;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.BusinessModels.TableStorage.Dtos;
using Dibk.Ftpb.Api.Models;

namespace FtB_FormLogic
{
    [FormDataFormat(DataFormatId = DataFormatIDs.VarselHoeringOgOffentligEttersyn, DataFormatVersion = HoeringOgOffentligEttersynSenderHelper.DataFormatVersion, ProcessingContext = FormLogicProcessingContext.Prepare)]
    public class HoeringOgOffentligEttersynPrepareLogic : DistributionPrepareLogic<OffentligEttersynType, BeroertPartType>
    {
        protected override string DataFormatId => DataFormatIDs.VarselHoeringOgOffentligEttersyn;

        public HoeringOgOffentligEttersynPrepareLogic(IFormDataRepo repo,
                                                      ILogger<VarselOppstartPlanarbeidPlussHoeringsmyndigheterPrepareLogic> log,
                                                      IDbUnitOfWork dbUnitOfWork,
                                                      IDecryptionFactory decryptionFactory,
                                                      IBlobOperations blobOperations,
                                                      IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                                      KRRClient krrClient,
                                                      DistributionReceiverRepo distributionReceiverRepo,
                                                      DistributionReceiverLogRepo distributionReceiverLogRepo,
                                                      DistributionSubmittalRepo distributionSubmittalRepo) :
            base(repo,
                log,
                dbUnitOfWork,
                decryptionFactory,
                blobOperations,
                fileDownloadHttpClient,
                krrClient,
                distributionReceiverRepo,
                distributionReceiverLogRepo,
                distributionSubmittalRepo)
        {
        }

        public override async Task<IEnumerable<SendQueueItem>> ExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {
            var formmetadata = new FormMetadataApiModel()
            {
                ArchiveReference = submittalQueueItem.ArchiveReference,
                Status = FormMetadataStatus.IKø,
                ArchiveTimestamp = DateTime.Now,
                FormType = Hoffe.FormDataFormType,
                Application = FormData.metadata?.fraSluttbrukersystem,                
                SenderSystem = "Høring og offentlig ettersyn",
                ServiceCode = "VARSEL-HOFFE",
                ServiceEditionCode = 1
            };
            DbUnitOfWork.FormMetadata.Create(formmetadata);

            return await base.ExecuteAsync(submittalQueueItem);
        }

        public override async Task SetSenderAsync()
        {
            var reportee = await BlobOperations.GetReporteeIdFromStoredBlobAsync(base.ArchiveReference);

            var decryptedReportee = DecryptionFactory.GetDecryptor().DecryptText(reportee);

            var distributionSenderCandidates = HoeringOgOffentligEttersynSenderHelper.GetDistributionSenderCandidates(DecryptionFactory.GetDecryptor(), FormData);

            var distributionSender = distributionSenderCandidates.FirstOrDefault(r => r.Id.Equals(decryptedReportee));

            Sender = distributionSender;
        }

        public override async Task SetReceiversAsync()
        {
            var receivers = new Dictionary<ReceiverDistinctKeyItem, Receiver>();

            ReceiversDistinct = new Dictionary<ReceiverDistinctKeyItem, BeroertPartBlobData>();

            var counter = 0;

            foreach (var beroertPart in FormData.beroerteParter)
            {
                var receiverType = EnumExtentions.GetValueFromDescription<ActorTypeEnum>(beroertPart.partstype.kodeverdi);

                var id = receiverType.Equals(ActorTypeEnum.Privatperson) ? beroertPart.foedselsnummer : beroertPart.organisasjonsnummer;

                var decryptedId = id.Length > 11 ? DecryptionFactory.GetDecryptor().DecryptText(id) : id;

                var blobItemName = $"{counter}.json";

                var receiversDistinctKey = new ReceiverDistinctKeyItem(decryptedId, beroertPart.erHoeringsmyndighet.GetValueOrDefault());

                if (ReceiversDistinct.TryAdd(receiversDistinctKey, new BeroertPartBlobData { BeroertPart = beroertPart, BlobItemName = blobItemName }))
                {
                    receivers.Add(receiversDistinctKey, new Receiver
                    {
                        Id = id,
                        Type = receiverType,
                        Name = beroertPart.navn,
                        DigitallyReserved = false,
                        IsHoeringsmyndighet = beroertPart.erHoeringsmyndighet.GetValueOrDefault(),
                        BlobItemName = blobItemName
                    });
                    counter++;
                }
                else
                {
                    var beroerteEiendommer = ReceiversDistinct[receiversDistinctKey].BeroertPart.gjelderEiendommer;
                    ReceiversDistinct[receiversDistinctKey].BeroertPart.gjelderEiendommer = beroerteEiendommer.Concat(beroertPart.gjelderEiendommer).ToArray();
                }
            }

            await SetDigitallyReservedInformation(receivers);

            Receivers = receivers.Values.ToList();

            Logger.LogInformation(
                "Receiver count from form data: {ReceiverCount}. Receiver count after removed duplicates: {ReceiverCountDistinct}",
                FormData.beroerteParter.Length, Receivers.Count);
        }

        protected override DistributionSubmittalDto CreateDistributionSubmittalDto(string archiveReference, string senderId,
            int receiverCount)
        {
            return new DistributionSubmittalDto(
                archiveReference,
                senderId,
                receiverCount,
                DistributionSubmittalTypes.HOFFE,
                DateTime.Now
            )
            {
                ReplyDeadline = (DateTime)FormData.planforslag.fristForUttalelse
            };
        }

        protected override void SetBeroerteParterToZero(OffentligEttersynType formData)
        {
            formData.beroerteParter = Array.Empty<BeroertPartType>();
        }
    }
}