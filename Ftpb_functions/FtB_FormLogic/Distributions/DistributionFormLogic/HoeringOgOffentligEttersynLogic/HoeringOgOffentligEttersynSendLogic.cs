using Altinn.Common.Models;
using Dibk.Ftpb.Integration.SvarUt.Builders.Models;
using Dibk.Ftpb.Integration.SvarUt.Models;
using FtB_Common;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Constants;
using FtB_Common.Encryption;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_FormLogic.Distributions.DistributionFormLogic;
using FtB_FormLogic.Distributions.DistributionFormLogic.HoeringOgOffentligEttersynLogic;
using FtB_FormLogic.Distributions.DistributionFormLogic.HoeringOgOffentligEttersynLogic.Send;
using FtB_FormLogic.Shipments.SvarUt;
using FtB_Print.Services.Print;
using Ftb_Repositories;
using Microsoft.Extensions.Logging;
using no.kxml.skjema.dibk.offentligEttersyn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Dibk.Ftpb.Common.Constants.DataTypes;

namespace FtB_FormLogic
{
    [FormDataFormat(DataFormatId = DataFormatIDs.VarselHoeringOgOffentligEttersyn, DataFormatVersion = HoeringOgOffentligEttersynSenderHelper.DataFormatVersion, ProcessingContext = FormLogicProcessingContext.Send)]
    public class HoeringOgOffentligEttersynSendLogic : DistributionSendLogic<OffentligEttersynType, BeroertPartType>
    {
        private readonly EnvironmentProvider _env;
        private readonly SvarUtShipmentBuilder _svarUtShipmentBuilder;

        protected override List<string> AttachmentsNotToIncludeForOrganizationsAndPrivatePersons => new()
        {
            PlankartGml, PlankartSosi, PlanbestemmelseXml
        };
        protected override List<string> AttachmentOrder => HoffeReceiptData.AttachmentOrder;

        public HoeringOgOffentligEttersynSendLogic(IFormDataRepo repo,
                                                   IBlobOperations blobOperations,
                                                   ILogger<HoeringOgOffentligEttersynSendLogic> logger,
                                                   HoeringOgOffentligEttersynDistribution distributionAdapter,
                                                   IDistributionDataMapper<OffentligEttersynType> distributionDataMapper,
                                                   HoeringOgOffentligEttersynSendPrefillMapper prefillMapper,
                                                   IDbUnitOfWork dbUnitOfWork,
                                                   IDecryptionFactory decryptionFactory,
                                                   IPrintService printService,
                                                   HoeringOgOffentligEttersynFileDownloadStatusClient fileDownloadHttpClient,
                                                   EnvironmentProvider environmentProvider,
                                                   SvarUtShipmentBuilder svarUtShipmentBuilder,
                                                   DistributionReceiverRepo distributionReceiverRepo,
                                                   DistributionReceiverLogRepo distributionReceiverLogRepo) 
            : base(repo,
                  blobOperations,
                  logger,
                  distributionAdapter,
                  distributionDataMapper,
                  prefillMapper,
                  dbUnitOfWork,
                  decryptionFactory,
                  printService,
                  fileDownloadHttpClient,
                  distributionReceiverRepo,
                  distributionReceiverLogRepo)
        {
            _env = environmentProvider;
            _svarUtShipmentBuilder = svarUtShipmentBuilder;
        }

        protected override void SetBeroertPart(OffentligEttersynType formData, BeroertPartType beroertPart)
        {
            formData.beroerteParter = new[] { beroertPart };
        }

        protected override DistributionSender GetDistributionSender(string decryptedReporteeId)
        {
            return HoeringOgOffentligEttersynSenderHelper.GetDistributionSender(DecryptionFactory.GetDecryptor(), decryptedReporteeId, FormData);
        }
        
        protected override async Task AddShipmentForHoeringsmyndighet(IDistributionMessage distributionMessage, Guid distributionFormReferenceId)
        {
            if (distributionMessage is not HoeringOgOffentligEttersynDistributionMessage dMessage)
                return;
            
            var ekstraMetadataFraAvleverendeSystem = new List<Entry>
            {
                new() { Key = "Hovedinnsendingsnummer", Value = distributionFormReferenceId.ToString() }
            };

            dMessage.SvarUtForsendelse = await _svarUtShipmentBuilder
                .AddAttachmentsFromPublicStorage(PublicBlobContainerName)
                .AddEkstraMetadataFraAvleverendeSystem(ekstraMetadataFraAvleverendeSystem)
                .UseFormDataWithoutBeroerteParter()
                .Build(ArchiveReference, Receiver, Sender, GetShipmentData());
        }

        private ForsendelseDataBygg GetShipmentData()
        {
            var titlePrefix = _env.IsDevelopment ? "DEV: " : _env.IsTest ? "TEST: " : string.Empty;
            var titlePostfix = _env.IsProduction ? string.Empty : Guid.NewGuid().ToString()[..8];
            
            return new ForsendelseDataBygg(SendQueueItem.ArchiveReference)
            {
                ForsendelseType ="Varsel om planoppstart",
                ForsendelseTittel = $"{titlePrefix}Høring og offentlig ettersyn av reguleringsplan {FormData.planforslag?.plannavn}{titlePostfix}",
                SøknadSkjemaNavn = "Informasjon om høring og offentlig ettersyn",
                Konteringskode = "HOFFE",
                Adresselinje1 = FormData.eiendomByggested?[0].adresse?.adresselinje1,
                Adresselinje2 = FormData.eiendomByggested?[0].adresse?.adresselinje2,
                Adresselinje3 = FormData.eiendomByggested?[0].adresse?.adresselinje3,
                Kommunenummer = FormData.eiendomByggested?[0].eiendomsidentifikasjon?.kommunenummer,
                Gårdsnummer = FormData.eiendomByggested?[0].eiendomsidentifikasjon?.gaardsnummer,
                Bruksnummer = FormData.eiendomByggested?[0].eiendomsidentifikasjon?.bruksnummer,
                Seksjonsnummer = FormData.eiendomByggested?[0].eiendomsidentifikasjon?.seksjonsnummer,
                Festenummer = FormData.eiendomByggested?[0].eiendomsidentifikasjon?.festenummer,
                Postnr = FormData.eiendomByggested?[0].adresse?.postnr,
                Poststed = FormData.eiendomByggested?[0].adresse?.poststed,
                AvgivendeSystem = FormData.metadata?.fraSluttbrukersystem,
                Bolignummer = FormData.eiendomByggested?[0].bolignummer,
                KommunensSaksnummerSekvensnummer = FormData.planforslag?.kommunensSaksnummer?.sakssekvensnummer,
                KommunensSaksnummerÅr = FormData.planforslag?.kommunensSaksnummer?.saksaar,
            };
        }

        protected override Dictionary<string, string> GetAttachmentsWithDisplayName(List<AltinnPrefillData> attachments)
        {
            return HoffeReceiptData.GetAttachmentsWithDisplayName(attachments.Select(a => (a.Id, a.Filename)))
                .ToDictionary(keySelector => keySelector.fileName, valueSelector => valueSelector.displayname);
        }

        public override Task SetSenderAsync()
        {
            if (SendQueueItem.Sender.Name == FormData.planKonsulent?.navn)
            {
                Sender = new ShipmentActor(SendQueueItem.Sender)
                {
                    Address = FormData.planKonsulent.adresse.adresselinje1,
                    PostalCode = FormData.planKonsulent.adresse.postnr,
                    Poststed = FormData.planKonsulent.adresse.poststed
                };
            }
            else
            {
                Sender = new ShipmentActor(SendQueueItem.Sender)
                {
                    Address = FormData.forslagsstiller.adresse.adresselinje1,
                    PostalCode = FormData.forslagsstiller.adresse.postnr,
                    Poststed = FormData.forslagsstiller.adresse.poststed
                };
            }

            return Task.CompletedTask;
        }
    }
}
