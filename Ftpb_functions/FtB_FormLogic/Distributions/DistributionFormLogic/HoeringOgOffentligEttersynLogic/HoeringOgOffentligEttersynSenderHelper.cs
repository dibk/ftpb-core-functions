﻿using System.Collections.Generic;
using FtB_Common.BusinessModels;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.Exceptions;
using no.kxml.skjema.dibk.offentligEttersyn;

namespace FtB_FormLogic.Distributions.DistributionFormLogic.HoeringOgOffentligEttersynLogic
{
    public class HoeringOgOffentligEttersynSenderHelper
    {
        public const string DataFormatVersion = "1";

        public static List<Actor> GetDistributionSenderCandidates(IDecryption decryptor, OffentligEttersynType varselHoeringOffentligEttersyn)
        {
            var retval = new List<Actor>();

            if (!string.IsNullOrEmpty(varselHoeringOffentligEttersyn.planKonsulent?.organisasjonsnummer))
                retval.Add(new Actor() { Id = varselHoeringOffentligEttersyn.planKonsulent.organisasjonsnummer, Name = varselHoeringOffentligEttersyn.planKonsulent.navn, Type = ActorTypeEnum.Foretak });

            if (!string.IsNullOrEmpty(varselHoeringOffentligEttersyn.planKonsulent?.foedselsnummer))
                retval.Add(new Actor()
                {
                    Id = decryptor.DecryptText(varselHoeringOffentligEttersyn.planKonsulent.foedselsnummer),
                    Name = varselHoeringOffentligEttersyn.planKonsulent.navn, Type = ActorTypeEnum.Privatperson
                });

            if (!string.IsNullOrEmpty(varselHoeringOffentligEttersyn.forslagsstiller.organisasjonsnummer))
                retval.Add(new Actor() { Id = varselHoeringOffentligEttersyn.forslagsstiller.organisasjonsnummer, Name= varselHoeringOffentligEttersyn.forslagsstiller.navn, Type = ActorTypeEnum.Foretak });

            if (!string.IsNullOrEmpty(varselHoeringOffentligEttersyn.forslagsstiller.foedselsnummer))
                retval.Add(new Actor() { Id = decryptor.DecryptText(varselHoeringOffentligEttersyn.forslagsstiller.foedselsnummer), Name = varselHoeringOffentligEttersyn.forslagsstiller.navn, Type = ActorTypeEnum.Privatperson });

            return retval;
        }

        public static DistributionSender GetDistributionSender(IDecryption decryptor, string decryptedReporteeId, OffentligEttersynType varselHoeringOffentligEttersyn)
        {
            if (!string.IsNullOrEmpty(varselHoeringOffentligEttersyn.planKonsulent?.organisasjonsnummer) && (varselHoeringOffentligEttersyn.planKonsulent?.organisasjonsnummer == decryptedReporteeId)
                    ||
                    !string.IsNullOrEmpty(varselHoeringOffentligEttersyn.planKonsulent?.foedselsnummer) && (decryptor.DecryptText(varselHoeringOffentligEttersyn.planKonsulent.foedselsnummer) == decryptedReporteeId)
                    )
            {
                return new DistributionSender()
                {
                    Role = DistributionSenderRoleEnum.Plankonsulent,
                    Name = varselHoeringOffentligEttersyn.planKonsulent.navn,
                    Phone = !string.IsNullOrEmpty(varselHoeringOffentligEttersyn.planKonsulent.telefon) ?
                        varselHoeringOffentligEttersyn.planKonsulent.telefon : varselHoeringOffentligEttersyn.planKonsulent.telefon,
                    Email = !string.IsNullOrEmpty(varselHoeringOffentligEttersyn.planKonsulent.epost) ?
                        varselHoeringOffentligEttersyn.planKonsulent.epost : varselHoeringOffentligEttersyn.planKonsulent.epost
                };
            }

            if (!string.IsNullOrEmpty(varselHoeringOffentligEttersyn.forslagsstiller.organisasjonsnummer) && (varselHoeringOffentligEttersyn.forslagsstiller.organisasjonsnummer == decryptedReporteeId)
                ||
                !string.IsNullOrEmpty(varselHoeringOffentligEttersyn.forslagsstiller.foedselsnummer) && (decryptor.DecryptText(varselHoeringOffentligEttersyn.forslagsstiller.foedselsnummer) == decryptedReporteeId)
                )
            {

                return new DistributionSender()
                {
                    Role = DistributionSenderRoleEnum.Forslagsstiller,
                    Name = varselHoeringOffentligEttersyn.forslagsstiller.navn,
                    Phone = string.IsNullOrEmpty(varselHoeringOffentligEttersyn.forslagsstiller.telefon) ?
                        "" : varselHoeringOffentligEttersyn.forslagsstiller.telefon,
                    Email = string.IsNullOrEmpty(varselHoeringOffentligEttersyn.forslagsstiller.epost) ?
                        "": varselHoeringOffentligEttersyn.forslagsstiller.epost
                };
            }

            throw new InvalidDistributionSenderException("The form has no legal distribution sender.");
        }
    }
}
