﻿using Ftb_DbModels;
using Ftb_Repositories.HttpClients;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace FtB_FormLogic.Distributions.DistributionFormLogic.HoeringOgOffentligEttersynLogic
{
    public class HoeringOgOffentligEttersynFileDownloadStatusClient : IFileDownloadStatusHttpClient
    {
        public HttpClient Client { get; }

        public HoeringOgOffentligEttersynFileDownloadStatusClient(HttpClient httpClient)
        {
            Client = httpClient;
        }

        public Task<IEnumerable<FileDownloadStatus>> GetAllAsync(string archiveReference)
        {
            return Task.FromResult<IEnumerable<FileDownloadStatus>>(new List<FileDownloadStatus>());
        }

        public Task<bool> PostAsync(string archiveReference, FileDownloadStatus fileDownload)
        {
            return Task.FromResult(true);
        }

        public Task<bool> PutAsync(string archiveReference, FileDownloadStatus fileDownload)
        {
            return null;
        }
    }
}
