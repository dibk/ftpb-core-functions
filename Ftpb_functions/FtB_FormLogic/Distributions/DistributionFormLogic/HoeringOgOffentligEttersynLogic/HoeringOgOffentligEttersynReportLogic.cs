using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using Altinn3.Adapters;
using Dibk.Ftpb.Common.Constants;
using Dibk.Ftpb.ModelToPdf.Common.Client;
using Dibk.Ftpb.ModelToPdf.Common.Models;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Constants;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_DataModels.Mappers;
using FtB_FormLogic.Distributions.DistributionFormLogic.HoeringOgOffentligEttersynLogic;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using no.kxml.skjema.dibk.offentligEttersyn;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FtB_FormLogic
{
    [FormDataFormat(DataFormatId = DataFormatIDs.VarselHoeringOgOffentligEttersyn, DataFormatVersion = HoeringOgOffentligEttersynSenderHelper.DataFormatVersion, ProcessingContext = FormLogicProcessingContext.Report)]
    public class HoeringOgOffentligEttersynReportLogic : DistributionReportLogic<OffentligEttersynType, BeroertPartType>
    {
        private readonly IHtmlUtils _htmlUtils;
        private readonly IDecryption _decryptor;

        protected override bool CompleteAltinn3Instance => true;

        protected override StatusApiFileInfo MainFormDownloadFileInfo => new()
        {
            Name = AttachmentDisplayName.Hoeringsbrev,
            FileName = $"{DataTypes.Hoeringsbrev}.pdf",
            FileType = FileTypesForDownloadEnum.SkjemaPdfHoeringOgOffentligEttersyn,
            MimeType = "application/pdf"
        };

        protected override StatusApiFileInfo ReceiptDownloadFileInfo => new()
        {
            Name = "HoeringOgOffentligEttersynKvittering",
            FileName = "hoering_og_offentlig_ettersyn_kvittering.pdf",
            FileType = FileTypesForDownloadEnum.DistribusjonHoeringOgOffentligEttersynKvittering,
            MimeType = "application/pdf"
        };

        public HoeringOgOffentligEttersynReportLogic(
            IHtmlUtils htmlUtils,
            IDecryptionFactory decryptionFactory,
            INotificationAdapter notificationAdapter,
            IDatamodelToPdfHttpClient datamodelToPdfHttpClient,
            IBlobOperations blobOperations,
            IAltinnInstanceCompleter altinnInstanceCompleter,
            DistributionSubmittalRepo distributionSubmittalRepo,
            IFormDataRepo repo,
            ILogger<HoeringOgOffentligEttersynReportLogic> log,
            IDbUnitOfWork dbUnitOfWork,
            IFileDownloadStatusHttpClient fileDownloadHttpClient,
            DistributionReceiverRepo distributionReceiverRepo,
            DistributionReceiverLogRepo distributionReceiverLogRepo
        ) : base(
            notificationAdapter,
            datamodelToPdfHttpClient,
            blobOperations,
            altinnInstanceCompleter,
            distributionSubmittalRepo,
            repo,
            log,
            dbUnitOfWork,
            fileDownloadHttpClient,
            distributionReceiverRepo,
            distributionReceiverLogRepo
        )
        {
            _htmlUtils = htmlUtils;
            _decryptor = decryptionFactory.GetDecryptor();
        }

        protected override void SetBeroertPart(OffentligEttersynType formData, BeroertPartType beroertPart)
        {
            formData.beroerteParter = new[] { beroertPart };
        }

        protected override async Task<AltinnReceiver> GetReceiver()
        {
            var reportee = await BlobOperations.GetReporteeIdFromStoredBlobAsync(this.ArchiveReference);
            var decryptedReportee = _decryptor.DecryptText(reportee);
            var receiverCandidates = HoeringOgOffentligEttersynSenderHelper.GetDistributionSenderCandidates(_decryptor, FormData);
            Actor actor = receiverCandidates.FirstOrDefault(r => r.Id.Equals(decryptedReportee));

            return new AltinnReceiver() { Id = actor.Id, Type = GetAltinnReceiverTypeFromActorTypeEnum(actor.Type) };
        }

        private AltinnReceiverType GetAltinnReceiverTypeFromActorTypeEnum(ActorTypeEnum type)
        {
            switch (type)
            {
                case ActorTypeEnum.Privatperson:
                    return AltinnReceiverType.Privatperson;

                case ActorTypeEnum.Foretak:
                case ActorTypeEnum.OffentligMyndighet:
                case ActorTypeEnum.Organisasjon:
                case ActorTypeEnum.Plankonsulent:
                    return AltinnReceiverType.Foretak;

                default:
                    return AltinnReceiverType.Foretak;
            }
        }

        protected override MessageDataType GetSubmitterReceiptMessage(string archiveReference)
        {
            try
            {
                string adresse = GetAddressWithPostalLocationInfo(FormData.eiendomByggested.First().adresse);
                string planNavn = FormData.planforslag.plannavn ?? "";
                string byggested = !string.IsNullOrWhiteSpace(adresse) ? $"{adresse}, {planNavn}" : $"{planNavn}";
                string htmlBody = _htmlUtils.GetHtmlFromTemplate("FtB_FormLogic.Distributions.DistributionFormLogic.HoeringOgOffentligEttersynLogic.Report.VarselHoeringOgOffentligEttesynReceiptMessageBody.html");

                htmlBody = htmlBody.Replace("<plannavn />", planNavn);
                htmlBody = htmlBody.Replace("<arkivReferanse />", archiveReference.ToUpper());

                var mess = new MessageDataType()
                {
                    MessageTitle = $"Kvittering - Høring og offentlig ettersyn av reguleringsplan, {byggested}",
                    MessageSummary = "Trykk på vedleggene under for å laste ned brevet og kvittering med oversikt over berørte parter som har mottatt brevet.",
                    MessageBody = htmlBody
                };

                return mess;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Error occurred while getting submitter receipt message");

                throw;
            }
        }

        public static List<IGrouping<string, BeroertPartType>> GetGroupsOfBeroerteParterWithDecryptedIDsParallel(BeroertPartType[] beroerteParter, IEnumerable<DistributionReceiverDto> successfullyNotified, Func<string, string> decryptorFunc)
        {
            var beroertPartListeWithDecryptedIDsFodselsnummer = new ConcurrentBag<BeroertPartType>();
            var beroertPartListeWithDecryptedIDsOrganisasjonsnummer = new ConcurrentBag<BeroertPartType>();

            Parallel.ForEach(beroerteParter, (beroertPart) =>
            {
                if (!string.IsNullOrEmpty(beroertPart.organisasjonsnummer))
                {
                    beroertPartListeWithDecryptedIDsOrganisasjonsnummer.Add(beroertPart);
                }

                if (!string.IsNullOrEmpty(beroertPart.foedselsnummer))
                {
                    var newBeroertPartWithDecryptedId = CreateDeepCopy(beroertPart);
                    newBeroertPartWithDecryptedId.foedselsnummer = decryptorFunc(beroertPart.foedselsnummer);
                    beroertPartListeWithDecryptedIDsFodselsnummer.Add(newBeroertPartWithDecryptedId);
                }
            });

            var groupedByFoedselsnummer = beroertPartListeWithDecryptedIDsFodselsnummer.GroupBy(b => b.foedselsnummer).ToList();
            var completedGroupsWithDecryptedIds = beroertPartListeWithDecryptedIDsOrganisasjonsnummer.GroupBy(b => b.organisasjonsnummer).ToList();
            completedGroupsWithDecryptedIds.AddRange(groupedByFoedselsnummer);
            var listOfDecryptedSucsessfullyNotifiedIds = new ConcurrentBag<string>();

            Parallel.ForEach(successfullyNotified, (notified) =>
            {
                listOfDecryptedSucsessfullyNotifiedIds.Add(decryptorFunc(notified.ReceiverId));
            });
            var noe = completedGroupsWithDecryptedIds.Select(g => g.Key).Intersect(listOfDecryptedSucsessfullyNotifiedIds);
            completedGroupsWithDecryptedIds.Where(p => listOfDecryptedSucsessfullyNotifiedIds.Contains(p.Key));

            return completedGroupsWithDecryptedIds;
        }

        public static T CreateDeepCopy<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                XmlSerializer serializer = new XmlSerializer(obj.GetType());
                serializer.Serialize(ms, obj);
                ms.Seek(0, SeekOrigin.Begin);
                return (T)serializer.Deserialize(ms);
            }
        }

        private static string GetAddressWithPostalLocationInfo(EiendommensAdresseType eiendom)
        {
            if (string.IsNullOrWhiteSpace(eiendom.adresselinje1))
                return "";

            if (string.IsNullOrWhiteSpace(eiendom.postnr) || string.IsNullOrWhiteSpace(eiendom.poststed))
                return $"{eiendom.adresselinje1}";

            return $"{eiendom.adresselinje1}, {eiendom.postnr} {eiendom.poststed}";
        }

        protected override bool? SubmitterIsPlankonsulent(string encryptedReporteeId)
        {
            var reporteeId = _decryptor.DecryptText(encryptedReporteeId);

            return reporteeId.Equals(FormData.planKonsulent?.organisasjonsnummer);
        }

        protected override PlanModelToPdfKvittering GetModelToPdfReceiptInfo()
        {
            return new()
            {
                Tittel = "Kvittering for høring og offentlig ettersyn av reguleringsplan",
                Plannavn = FormData.planforslag.plannavn ?? "",
                Forslagstillernavn = FormData.forslagsstiller.navn,
                Plankonsulentnavn = FormData.planKonsulent?.navn ?? "",
            };
        }

        protected override async Task<(List<Interessent> notifiedParties, List<Interessent> notNotifiedParties)> GetInterestedPartiesAsync(string archiveReference)
        {
            var successfullyNotified = await GetReceiverSuccessfullyNotifiedAsync(archiveReference);

            var groupsWithBeroerteParterWithDecryptedIDs = GetGroupsOfBeroerteParterWithDecryptedIDsParallel(FormData.beroerteParter, successfullyNotified, (ssn) => _decryptor.DecryptText(ssn));
            var listOfBeroertPartWithEiendom = HoeringOgOffentligEttersynMappers.GetListOfHoeringOgOffentligEttersynVarselTypeBeroertPartWithEiendomFromGroups(groupsWithBeroerteParterWithDecryptedIDs);

            var listOfNotReachableReceiverNames = await GetNotReachableReceiversAsync();

            var notReachableBeroerteParter = listOfBeroertPartWithEiendom.Where(b =>
                listOfNotReachableReceiverNames.Contains(b.navn, StringComparer.InvariantCultureIgnoreCase)).ToList();

            var notNotifiedParties = new List<Interessent>();
            foreach (var receiverName in listOfNotReachableReceiverNames)
            {
                notNotifiedParties.Add(new Interessent()
                {
                    Navn = receiverName
                });
            }

            var notifiedParties = new List<Interessent>();
            foreach (var receiver in listOfBeroertPartWithEiendom.Except(notReachableBeroerteParter))
            {
                var interestedParty = new Interessent()
                {
                    Navn = receiver.navn,
                    PartstypeKodebeskrivelse = receiver.partstype.kodebeskrivelse,
                };

                interestedParty.BerørteEiendommer = new List<Eiendom>();

                foreach (var eiendom in receiver.gjelderEiendommer)
                {
                    interestedParty.BerørteEiendommer.Add(new Eiendom()
                    {
                        Adresse = new Adresse()
                        {
                            Adresselinje1 = eiendom.adresse.adresselinje1,
                            Adresselinje2 = eiendom.adresse.adresselinje2,
                            Adresselinje3 = eiendom.adresse.adresselinje3,
                            Postnummer = eiendom.adresse.postnr,
                            Poststed = eiendom.adresse.poststed,
                        },
                        Eiendomsidentifikasjon = new Eiendomsidentifikasjon()
                        {
                            Gaardsnummer = eiendom.eiendomsidentifikasjon.gaardsnummer,
                            Bruksnummer = eiendom.eiendomsidentifikasjon.bruksnummer,
                            Seksjonsnummer = eiendom.eiendomsidentifikasjon.seksjonsnummer,
                            Festenummer = eiendom.eiendomsidentifikasjon.festenummer
                        }
                    });
                }

                notifiedParties.Add(interestedParty);
            }

            return (notifiedParties, notNotifiedParties);
        }

        protected override IEnumerable<(string Id, string navn)> GetSocialSecurityNumbers()
        {
            return FormData.beroerteParter
                    .Where(x => x.foedselsnummer != null)
                    .Select(x => (x.foedselsnummer, x.navn));
        }

        protected override IEnumerable<(string Id, string navn)> GetOrganisationNumbers()
        {
            return FormData.beroerteParter
                    .Where(x => x.organisasjonsnummer != null)
                    .Select(x => (x.organisasjonsnummer, x.navn));
        }

        private bool IsSubmitterPlankonsulent(string encryptedReporteeId, string plankonsulentOrganisasjonsnummer)
        {
            var reporteeId = _decryptor.DecryptText(encryptedReporteeId);

            return reporteeId.Equals(plankonsulentOrganisasjonsnummer);
        }
    }
}