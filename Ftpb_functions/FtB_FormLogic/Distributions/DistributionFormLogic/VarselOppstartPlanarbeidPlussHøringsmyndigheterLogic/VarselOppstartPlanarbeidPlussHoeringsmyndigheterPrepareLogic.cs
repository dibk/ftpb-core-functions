using Dibk.Ftpb.Api.Models;
using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Constants;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_FormLogic.Distributions.DistributionFormLogic;
using FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidTilHøringsmyndigheterLogic;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Ftb_Repositories.LogContstants;
using KontaktOgReservasjonsregisteret;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    [FormDataFormat(DataFormatId = DataFormatIDs.VarselPlanoppstart, DataFormatVersion = VorpahSenderHelper.DataFormatVersion, ProcessingContext = FormLogicProcessingContext.Prepare)]
    public class VarselOppstartPlanarbeidPlussHoeringsmyndigheterPrepareLogic : DistributionPrepareLogic<PlanvarselV2, Beroertpart>
    {
        protected override string DataFormatId => DataFormatIDs.VarselPlanoppstart;

        public VarselOppstartPlanarbeidPlussHoeringsmyndigheterPrepareLogic(IFormDataRepo repo,
                                                                            ILogger<VarselOppstartPlanarbeidPlussHoeringsmyndigheterPrepareLogic> log,
                                                                            IDbUnitOfWork dbUnitOfWork,
                                                                            IDecryptionFactory decryptionFactory,
                                                                            IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                                                            IBlobOperations blobOperations,
                                                                            KRRClient krrClient,
                                                                            DistributionReceiverRepo distributionReceiverRepo,
                                                                            DistributionReceiverLogRepo distributionReceiverLogRepo,
                                                                            DistributionSubmittalRepo distributionSubmittalRepo) :
            base(repo,
                log,
                dbUnitOfWork,
                decryptionFactory,
                blobOperations,
                fileDownloadHttpClient,
                krrClient,
                distributionReceiverRepo,
                distributionReceiverLogRepo,
                distributionSubmittalRepo)
        {
        }

        public override async Task<IEnumerable<SendQueueItem>> ExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {
            var formmetadata = new FormMetadataApiModel()
            {
                ArchiveReference = submittalQueueItem.ArchiveReference,
                Status = FormMetadataStatus.IKø,
                ArchiveTimestamp = DateTime.Now,
                FormType = Vorpah.FormDataFormType,
                Application = FormData?.Metadata?.FraSluttbrukersystem,
                SenderSystem = "Varsel oppstart av reguleringsplanarbeid",
                ServiceCode = "VORPAH",
                ServiceEditionCode = 1
            };
            DbUnitOfWork.FormMetadata.Create(formmetadata);

            var sendQueueItems = await base.ExecuteAsync(submittalQueueItem);

            return sendQueueItems;
        }

        public override async Task SetSenderAsync()
        {
            var reportee = await BlobOperations.GetReporteeIdFromStoredBlobAsync(ArchiveReference);

            var decryptedReportee = DecryptionFactory.GetDecryptor().DecryptText(reportee);

            var distributionSenderCandidates = VorpahSenderHelper.GetDistributionSenderCandidates(DecryptionFactory.GetDecryptor(), FormData);

            var distributionSender = distributionSenderCandidates.FirstOrDefault(r => r.Id.Equals(decryptedReportee));

            Sender = distributionSender;
        }

        public override async Task SetReceiversAsync()
        {
            var receivers = new Dictionary<ReceiverDistinctKeyItem, Receiver>();

            ReceiversDistinct = new Dictionary<ReceiverDistinctKeyItem, BeroertPartBlobData>();

            var counter = 0;

            foreach (var beroertPart in FormData.BeroerteParter)
            {
                var receiverType = EnumExtentions.GetValueFromDescription<ActorTypeEnum>(beroertPart.Partstype.Kodeverdi);

                var id = receiverType.Equals(ActorTypeEnum.Privatperson) ? beroertPart.Foedselsnummer : beroertPart.Organisasjonsnummer;

                var decryptedId = id.Length > 11 ? DecryptionFactory.GetDecryptor().DecryptText(id) : id;

                var blobItemName = $"{counter}.json";

                var receiversDistinctKey = new ReceiverDistinctKeyItem(decryptedId, beroertPart.ErHoeringsmyndighet.GetValueOrDefault());

                if (ReceiversDistinct.TryAdd(receiversDistinctKey, new BeroertPartBlobData { BeroertPart = beroertPart, BlobItemName = blobItemName }))
                {
                    receivers.Add(receiversDistinctKey, new Receiver
                    {
                        Id = id,
                        Type = receiverType,
                        Name = beroertPart.Navn,
                        DigitallyReserved = false,
                        IsHoeringsmyndighet = beroertPart.ErHoeringsmyndighet.GetValueOrDefault(),
                        BlobItemName = blobItemName
                    });
                    counter++;
                }
                else
                {
                    var beroerteEiendommer = ReceiversDistinct[receiversDistinctKey].BeroertPart.GjelderEiendom;
                    if (beroertPart.GjelderEiendom != null)
                        ReceiversDistinct[receiversDistinctKey].BeroertPart.GjelderEiendom = beroerteEiendommer.Concat(beroertPart.GjelderEiendom).ToArray();
                }
            }

            await SetDigitallyReservedInformation(receivers);

            Receivers = receivers.Values.ToList();

            Logger.LogInformation(
                "Receiver count from form data: {ReceiverCount}. Receiver count after removed duplicates: {ReceiverCountDistinct}",
                FormData.BeroerteParter.Length, Receivers.Count);
        }

        protected override DistributionSubmittalDto CreateDistributionSubmittalDto(string archiveReference, string senderId,
            int receiverCount)
        {
            return new DistributionSubmittalDto(
                archiveReference,
                senderId,
                receiverCount,
                DistributionSubmittalTypes.VORPAH,
                DateTime.Now
            )
            {
                ReplyDeadline = FormData.Planforslag.FristForInnspill!.Value
            };
        }

        protected override void SetBeroerteParterToZero(PlanvarselV2 formData)
        {
            formData.BeroerteParter = Array.Empty<Beroertpart>();
        }
    }
}