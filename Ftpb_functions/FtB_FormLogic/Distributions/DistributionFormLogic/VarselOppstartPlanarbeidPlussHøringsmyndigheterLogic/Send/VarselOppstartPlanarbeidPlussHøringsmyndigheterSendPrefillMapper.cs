using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using FtB_Common.Interfaces;
using FtB_DataModels.Mappers;
using Microsoft.Extensions.Logging;
using no.kxml.skjema.dibk.planuttalelse;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidTilHøringsmyndigheterLogic.Send
{
    public class VarselOppstartPlanarbeidPlussHøringsmyndigheterSendPrefillMapper : IFormMapper<PlanvarselV2>
    {
        private readonly ILogger<VarselOppstartPlanarbeidPlussHøringsmyndigheterSendPrefillMapper> _logger;

        public VarselOppstartPlanarbeidPlussHøringsmyndigheterSendPrefillMapper(ILogger<VarselOppstartPlanarbeidPlussHøringsmyndigheterSendPrefillMapper> logger)
        {
            _logger = logger;
        }

        public IEnumerable<IPrefillData> Map(PlanvarselV2 form, string receiverId, string receiverName, Guid distributionFormReferenceId)
        {
            var berørtPart = form.BeroerteParter?
                .FirstOrDefault(b => ((b.Foedselsnummer != null && b.Foedselsnummer.Equals(receiverId)) ||
                    (b.Organisasjonsnummer != null && b.Organisasjonsnummer.Equals(receiverId))) &&
                    b.Navn.Equals(receiverName));

            var svarPaaNabovarsels = new List<VarselOppstartPlanarbeidPlussHoeringsmyndigheterData>();
            if (berørtPart == null) return svarPaaNabovarsels;


            var svarPaaNabovarsel = new PlanuttalelseType();

            var parttypemapper = VarselOppstartPlanarbeidPlussHoeringsmyndigheterMappers.GetAktoerMapper();

            svarPaaNabovarsel.forslagsstiller =
                parttypemapper.Map<AktoerPlan, PartType>(form.Forslagsstiller);
            if (svarPaaNabovarsel.forslagsstiller != null)
            {
                svarPaaNabovarsel.forslagsstiller.epost = form.Forslagsstiller.Epost;
                svarPaaNabovarsel.forslagsstiller.telefonnummer = form.Forslagsstiller.Telefon;
            }

            svarPaaNabovarsel.plankonsulent =
                parttypemapper.Map<AktoerPlan, PartType>(form.Plankonsulent);
            if (svarPaaNabovarsel.plankonsulent != null)
            {
                svarPaaNabovarsel.plankonsulent.epost = form.Plankonsulent?.Epost;
                svarPaaNabovarsel.plankonsulent.telefonnummer = form.Plankonsulent?.Telefon;
            }

            svarPaaNabovarsel.beroertPart = VarselOppstartPlanarbeidPlussHoeringsmyndigheterMappers.GetBerortPartMapper()
                .Map<Beroertpart, BeroertPartType>(berørtPart);

            //distribution form reference id is used to connect the distribution form row with
            // the form when FTPB receives it from the notified party. 
            svarPaaNabovarsel.hovedinnsendingsnummer = distributionFormReferenceId.ToString();
            svarPaaNabovarsel.fraSluttbrukersystem = form.Metadata?.FraSluttbrukersystem;
            svarPaaNabovarsel.plannavn = form.Planforslag?.Plannavn;
            svarPaaNabovarsel.planid = form.Planforslag?.ArealplanId;
            svarPaaNabovarsel.kommune = form.Kommunenavn;
            svarPaaNabovarsel.fristForInnspill = form.Planforslag?.FristForInnspill;
            svarPaaNabovarsel.fristForInnspillSpecified = form.Planforslag?.FristForInnspill != null;
            svarPaaNabovarsel.saksnummer = $"{form.Planforslag?.KommunensSaksnummer?.Saksaar}/{form.Planforslag?.KommunensSaksnummer?.Sakssekvensnummer}";

            svarPaaNabovarsels.Add(new VarselOppstartPlanarbeidPlussHoeringsmyndigheterData(svarPaaNabovarsel,
                form.Metadata?.Hovedinnsendingsnummer, form));


            return svarPaaNabovarsels;
        }
    }
}