﻿using Dibk.Ftpb.Common.Datamodels;
using FtB_Common.Utils;
using no.kxml.skjema.dibk.planuttalelse;

namespace FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidTilHøringsmyndigheterLogic.Send
{
    public class VarselOppstartPlanarbeidPlussHoeringsmyndigheterData : PrefillSendData<PlanuttalelseType>
    {
        public VarselOppstartPlanarbeidPlussHoeringsmyndigheterData(PlanuttalelseType formInstance, string hovedinnsendingsNummer, PlanvarselV2 planvarsel) : base(formInstance)
        {
            InitialExternalSystemReference = hovedinnsendingsNummer;

            Planvarsel = SerializeUtil.Serialize(planvarsel);
        }

        public override string PrefillFormName => "Uttalelse til oppstart av reguleringsplanarbeid";

        //public override string InitialExternalSystemReference { get => FormInstance.hovedinnsendingsnummer; set { FormInstance.hovedinnsendingsnummer = value; } }

        public override string ExternalSystemReference => FormInstance.beroertPart.systemReferanse;

        public override string PrefillServiceCode => "SvarUttalelsehoeringsmyndigheter";

        public override string PrefillServiceEditionCode => "1";

        public string Planvarsel { get; set; }
    }
}
