﻿using Altinn.Common;
using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using Altinn.Distribution;
using Altinn3.Adapters;
using Dibk.Ftpb.Integration.SvarUt;
using Dibk.Ftpb.Integration.SvarUt.Models;
using FtB_Common.Exceptions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidTilHøringsmyndigheterLogic.Send
{
    public class VorpaPlussHDistributionMessage : AltinnDistributionMessage
    {
        public IEnumerable<Attachment> Attachments { get; set; }
        public Forsendelse SvarUtForsendelse { get; set; }
        public bool IsHoeringsmyndighet { get; set; }
    }

    public class VorpahDistribution : IDistributionAdapter
    {
        private readonly IPrefillAdapter _prefillAdapter;
        private readonly ISvarUtAdapter _svarUtAdapter;
        private readonly CorrespondenceAdapter _correspondenceAdapter;
        ILogger<VorpaPlussHDistributionMessage> _logger;


        public VorpahDistribution(
            IPrefillAdapter prefillAdapter, ISvarUtAdapter svarUtAdapter, CorrespondenceAdapter correspondenceAdapter, ILogger<VorpaPlussHDistributionMessage> logger)
        {
            _prefillAdapter = prefillAdapter;
            _svarUtAdapter = svarUtAdapter;
            _correspondenceAdapter = correspondenceAdapter;
            _logger = logger;
        }

        public Task<IAltinnPrefilledDistributionResult> SendCorrespondenceAsync(IDistributionMessage altinnMessage, string prefillReferenceId)
        {
            throw new System.NotImplementedException();
        }

        public async Task<IAltinnPrefilledDistributionResult> SendPrefillAndCorrespondenceAsync(IDistributionMessage altinnMessage)
        {
            var vorpahDistributionMessage = altinnMessage as VorpaPlussHDistributionMessage;
            var distResult = new Altinn3PrefillDistributionResult();

            if (vorpahDistributionMessage is { IsHoeringsmyndighet: true })
            {
                return await SendCorrespondenceForHoeringsmyndighetAsync(vorpahDistributionMessage, distResult);
            }

            try
            {
                AltinnPrefillResult prefillResult = new AltinnPrefillResult();
                if (altinnMessage.NotificationMessage.Receiver.Type == AltinnReceiverType.Privatperson && vorpahDistributionMessage.DigitallyReserved)
                {
                    distResult.PrefillResult = new AltinnPrefillResult();
                    distResult.PrefillResult.Message = "User is reserved against digital communication or has expiered contact information";
                    distResult.PrefillResult.AltinnCommunicationResult = AltinnCommunicationResultType.ReservedReportee;
                    distResult.PrefillResult.PrefillReferenceId = "";
                    distResult.PrefillResult.PrefillAltinnReceivedTime = DateTime.Now;
                }
                else
                {
                    prefillResult = await _prefillAdapter.SendPrefill(altinnMessage);
                    distResult.PrefillResult = prefillResult;
                }
            }
            catch (Exception)
            {
                _logger.LogError("Failed to reach receiver digitally.");
                distResult.PrefillResult = new AltinnPrefillResult();
                distResult.PrefillResult.Message = "Failed to reach the receiver digitally";
                distResult.PrefillResult.AltinnCommunicationResult = AltinnCommunicationResultType.UnableToReachReceiver;
                distResult.PrefillResult.PrefillReferenceId = "";
                distResult.PrefillResult.PrefillAltinnReceivedTime = DateTime.Now;
            }

            if (distResult.PrefillResult.IsSuccessfull())
            {
                distResult.CorrespondenceResult = await _correspondenceAdapter.SendCorrespondence(altinnMessage, distResult.PrefillResult.PrefillAltinnReceiptId);
            }

            return distResult;
        }

        private async Task<IAltinnPrefilledDistributionResult> SendCorrespondenceForHoeringsmyndighetAsync(VorpaPlussHDistributionMessage vorpahDistributionMessage, Altinn3PrefillDistributionResult distResult)
        {
            try
            {
                distResult.PrefillResult = new AltinnPrefillResult();
                distResult.PrefillResult.Message = "AltinnPrefill skipped. Reason: høringsmyndighet";
                distResult.PrefillResult.AltinnCommunicationResult = AltinnCommunicationResultType.NotUsed;                
                distResult.PrefillResult.PrefillAltinnReceivedTime = DateTime.Now;

                var forsendelseID = await _svarUtAdapter.SendAsync(vorpahDistributionMessage.SvarUtForsendelse);
                if (forsendelseID != null)
                {                    
                    distResult.PrefillResult.PrefillAltinnReceiptId = forsendelseID;

                    distResult.CorrespondenceResult = new AltinnCorrespondenceResult();
                    distResult.CorrespondenceResult.AltinnCommunicationResult = AltinnCommunicationResultType.Sent;
                    distResult.CorrespondenceResult.CorrespondenceAltinnReceiptId = forsendelseID;
                    return distResult;
                }
                else
                {
                    distResult.CorrespondenceResult = new AltinnCorrespondenceResult();
                    distResult.CorrespondenceResult.AltinnCommunicationResult = AltinnCommunicationResultType.Failed;
                    distResult.CorrespondenceResult.Message = "UnknownError";
                    return distResult;
                }
            }
            catch (SvarUtRequestException svarUtException)
            {
                var logmessage = $"SvarUt-forsendelse failed with statuscode {svarUtException.HttpStatusCode} with message: {svarUtException.Message}";
                if (svarUtException.HttpStatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    throw new CancelProcessException(logmessage);
                }
                else if(svarUtException.HttpStatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    // Midlertidig løsning for å håndtere at SvarUt returnerer 400 Bad Request ved forsøk på å sende til høringsmyndighet
                    distResult.CorrespondenceResult = new AltinnCorrespondenceResult();
                    distResult.CorrespondenceResult.AltinnCommunicationResult = AltinnCommunicationResultType.SvarUtFailed;
                    distResult.CorrespondenceResult.Message = logmessage;
                    return distResult;
                }
                else
                {
                    distResult.CorrespondenceResult = new AltinnCorrespondenceResult();
                    distResult.CorrespondenceResult.AltinnCommunicationResult = AltinnCommunicationResultType.Failed;
                    distResult.CorrespondenceResult.Message = logmessage;
                    return distResult;
                }
            }
        }
    }
}
