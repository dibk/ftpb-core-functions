using Altinn.Common.Models;
using Dibk.Ftpb.Common.Datamodels;
using FtB_Common.BusinessModels;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.Interfaces;
using FtB_DataModels.Mappers;
using FtB_FormLogic.Distributions;
using FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidTilHøringsmyndigheterLogic.Send;
using no.kxml.skjema.dibk.planuttalelse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Dibk.Ftpb.Common.Constants.DataTypes;
using BeroertPartType = no.kxml.skjema.dibk.planuttalelse.BeroertPartType;
using GjelderEiendomType = no.kxml.skjema.dibk.planuttalelse.GjelderEiendomType;
using PartType = no.kxml.skjema.dibk.planuttalelse.PartType;

namespace FtB_FormLogic
{
    public class VarselOppstartPlanarbeidPlussHoeringsmyndigheterSendDataProvider : SendDataProviderBase, IDistributionDataMapper<PlanvarselV2>
    {
        public VarselOppstartPlanarbeidPlussHoeringsmyndigheterSendDataProvider(IHtmlUtils htmlUtils, IDecryptionFactory decryptionFactory) : base(htmlUtils, decryptionFactory)
        {
        }

        public IDistributionMessage GetDistributionMessage(IEnumerable<IPrefillData> prefills, PlanvarselV2 mainFormData, Guid distributionFormId, string archiveReference, DistributionSender distributionSender, bool digitallyreserved)
        {
            var prefill = prefills.First() as VarselOppstartPlanarbeidPlussHoeringsmyndigheterData;

            var distributionMessage = new VorpaPlussHDistributionMessage()
            {
                AppId = "dibk/uttalelse-varselplanoppstart",
                PrefillDataFormatId = prefill.DataFormatId,
                PrefillDataFormatVersion = prefill.DataFormatVersion,
                DistributionFormReferenceId = distributionFormId,
                PrefillServiceCode = prefill.PrefillServiceCode,
                PrefillServiceEditionCode = prefill.PrefillServiceEditionCode,
                PrefilledXmlDataString = prefill.ToString(),
                DaysValid = 14,
                DueDate = null,
                IsHoeringsmyndighet = prefill.FormInstance.beroertPart.erHoeringsmyndighet.HasValue ? prefill.FormInstance.beroertPart.erHoeringsmyndighet.Value : false,
                DigitallyReserved = digitallyreserved,
                ExpectedFormDataType = Planuttalelse
            };

            distributionMessage.PrefillData = new List<AltinnPrefillData>
            {
                new AltinnPrefillData(Planuttalelse, prefill.ToString(), "application/xml", null, null),
            };

            distributionMessage.NotificationMessage.Receiver = base.GetReceiver(VarselOppstartPlanarbeidPlussHoeringsmyndigheterMappers.GetVorpaPlussHReceiverMapper().Map<DistributionReceiver>(prefill.FormInstance.beroertPart));
            distributionMessage.NotificationMessage.MessageData = CreateMessageData(prefill.FormInstance, distributionSender);
            distributionMessage.NotificationMessage.ArchiveReference = archiveReference;
            distributionMessage.NotificationMessage.ReplyLink = CreateReplyLink();
            distributionMessage.NotificationMessage.NotificationTemplate = "DIBK-nabo-1";
            distributionMessage.NotificationMessage.SenderEmail = "noreply@noreply.no";

            //Add notifications
            var fristForInnspillFormatted = prefill.FormInstance.fristForInnspill?.ToString("dd.MM.yyyy");
            var smsContent = GetSMSNotificationMessage(
                prefill.FormInstance.beroertPart.organisasjonsnummer,
                prefill.FormInstance.beroertPart.navn,
                prefill.FormInstance.kommune,
                distributionSender
            );

            string avsenderOrgNummer = "";
            if (!string.IsNullOrEmpty(prefill.FormInstance.plankonsulent?.partstype?.kodeverdi))
            {
                if (prefill.FormInstance.plankonsulent?.partstype?.kodeverdi != ActorTypeEnum.Privatperson.ToString())
                {
                    avsenderOrgNummer = prefill.FormInstance.plankonsulent.organisasjonsnummer;
                }
            }
            else
            {
                if (prefill.FormInstance.forslagsstiller?.partstype?.kodeverdi != ActorTypeEnum.Privatperson.ToString())
                {
                    avsenderOrgNummer = prefill.FormInstance.forslagsstiller.organisasjonsnummer;
                }
            }

            var emailContent = GetEmailNotificationBody(
                prefill.FormInstance.beroertPart.organisasjonsnummer,
                prefill.FormInstance.beroertPart.navn,
                prefill.FormInstance.kommune,
                prefill.FormInstance.plannavn,
                prefill.FormInstance.forslagsstiller.navn,
                fristForInnspillFormatted,
                distributionSender,
                archiveReference,
                avsenderOrgNummer
            );

            var notifications = new List<Notification>
            {
                new Notification()
                {
                    EmailContent = emailContent,
                    EmailSubject = $"Varsel om oppstart av reguleringsplanarbeid for {mainFormData.Planforslag.Plannavn}",
                    SmsContent = smsContent,
                    Receiver = prefill.FormInstance.beroertPart.epost
                }
            };

            distributionMessage.NotificationMessage.Notifications = notifications;

            return distributionMessage;
        }
        private MessageDataType CreateMessageData(PlanuttalelseType prefillData, DistributionSender distributionSender)
        {
            var body = GetPrefillNotificationBody(distributionSender, prefillData.forslagsstiller, prefillData.beroertPart, prefillData.fristForInnspill, prefillData.kommune);
            var summary = string.Empty;
            var title = GetPrefillNotificationTitle(prefillData.plannavn, prefillData.planid);
            return new MessageDataType() { MessageBody = body, MessageSummary = summary, MessageTitle = title };
        }

        private ReplyLink CreateReplyLink()
        {
            var replyLink = new ReplyLink()
            {
                //URL settes av adapteret..
                UrlTitle = "Trykk her for å fylle ut svarskjemaet"
            };
            return replyLink;

        }

        public string GetPrefillNotificationBody(DistributionSender avsenderDistribusjon, PartType forslagsstiller, BeroertPartType beroertPart, DateTime? fristForInnspill, string kommune)
        {
            string datoFristInnspill = fristForInnspill?.ToString("dd.MM.yyyy");
            string htmlBody = _htmlUtils.GetHtmlFromTemplate("FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidPlussHøringsmyndigheterLogic.Send.VarselOppstartPlanarbeidPlussHøringsmyndigheterPrefillNotificationBody.html");
            
            htmlBody = htmlBody.Replace("<beroertPart.navn />", beroertPart.navn);
            htmlBody = htmlBody.Replace("<kommune />", kommune);
            htmlBody = htmlBody.Replace("<forslagsstiller.navn />", forslagsstiller.navn);
            htmlBody = htmlBody.Replace("<avsenderDistribusjon.navn />", avsenderDistribusjon.Name);
            htmlBody = htmlBody.Replace("<avsenderDistribusjon.epost />", avsenderDistribusjon.Email);
            htmlBody = htmlBody.Replace("<avsenderDistribusjon.telefonnummer />", avsenderDistribusjon.Phone);
            htmlBody = htmlBody.Replace("<datoFristInnspill />", datoFristInnspill);
            htmlBody = htmlBody.Replace("<gjeldendeEiendom />", BeroerteEiendommerHasAddressInformation(beroertPart.gjelderEiendom)
                    ? $":<br/>{string.Join("<br/>", GetBeroertEiendomInformation(beroertPart.gjelderEiendom))}"
                    : " området vi vil regulere.");
            if (avsenderDistribusjon.Role is DistributionSenderRoleEnum.Plankonsulent)
                htmlBody = htmlBody.Replace("<paaVegneAvForslagsstiller />", $", på vegne av {forslagsstiller.navn}");

            return htmlBody;
        }

        public static string GetPrefillNotificationTitle(string planNavn, string planid)
        {
            return $"Varsel om oppstart av reguleringsplanarbeid - {planNavn}";
        }

        private static string GetSMSNotificationMessage(string orgnr, string berortPartNavn, string kommunenavn, DistributionSender avsenderDistribusjon)
        {
            string evtOrgnr = string.Empty;
            if (!string.IsNullOrEmpty(orgnr)) evtOrgnr = $" (org.nr. {orgnr})";

            var notificationBuilder = new StringBuilder();
            notificationBuilder.Append($"{berortPartNavn}{evtOrgnr} har fått varsel om oppstart av reguleringsplanarbeid i {kommunenavn} kommune. ");
            notificationBuilder.Append($"Logg inn på Altinn for å se brevet. ");
            notificationBuilder.Append($"Hilsen {avsenderDistribusjon.Role} {avsenderDistribusjon.Name}");

            return notificationBuilder.ToString();
        }

        private static string GetEmailNotificationBody(
            string orgnr,
            string nabo,
            string kommunenavn,
            string plannavn,
            string forslagsstillerNavn,
            string fristForInnspill,
            DistributionSender avsenderDistribusjon,
            string archiveReference,
            string avsenderOrgNummer)
        {
            string evtOrgnr = "";
            if (!string.IsNullOrEmpty(orgnr)) evtOrgnr = $" (org.nr. {orgnr})";

            // Dersom berørt part er en organisasjon


            // Melding
            var notificationBuilder = new StringBuilder();

            notificationBuilder.AppendLine($"Til {nabo}{evtOrgnr}");

            notificationBuilder.AppendLine($"<p>");
            notificationBuilder.Append($"Vi har sendt varsel om oppstart av reguleringsplanarbeid i {kommunenavn} kommune. ");
            notificationBuilder.AppendLine($"Navnet på reguleringsplanen er: {plannavn}.");
            notificationBuilder.AppendLine($"</p>");

            notificationBuilder.AppendLine($"<p>");
            notificationBuilder.AppendLine($"Vi sender deg dette brevet fordi du kan være berørt eller ha interesser i nærheten av området vi vil regulere.");
            notificationBuilder.AppendLine($"</p>");

            notificationBuilder.AppendLine($"<p>");
            notificationBuilder.AppendLine($"<strong>Du kan uttale deg om reguleringsplanforslaget</strong><br/>");
            notificationBuilder.AppendLine($"Logg inn på Altinn for å se brevet og svar innen {fristForInnspill}.");
            notificationBuilder.AppendLine($"</p>");

            if (!string.IsNullOrEmpty(orgnr))
            {
                notificationBuilder.AppendLine($"<p>");
                notificationBuilder.AppendLine($"<strong>Om tilgang i Altinn</strong><br/>");
                notificationBuilder.AppendLine($"For å se varselet om oppstart, må du representere {nabo} {evtOrgnr} i Altinn. <br/>");
                notificationBuilder.Append($"Den som skal se og svare på varselet, må ha rollen «Plan- og byggesak» i organisasjonen. Les mer på dibk.no/rettigheter-i-altinn ");
                notificationBuilder.AppendLine($"</p>");
            }


            notificationBuilder.AppendLine($"<p>");
            notificationBuilder.AppendLine($"Med vennlig hilsen,<br/>");
            notificationBuilder.Append($"{avsenderDistribusjon.Name}");
            if (avsenderDistribusjon.Role is DistributionSenderRoleEnum.Plankonsulent)
                notificationBuilder.Append($", på vegne av forslagsstiller {forslagsstillerNavn}");
            notificationBuilder.AppendLine();
            notificationBuilder.AppendLine($"<br/>");

            if (!string.IsNullOrWhiteSpace(avsenderDistribusjon.Phone))
                notificationBuilder.AppendLine($"Telefon: {avsenderDistribusjon.Phone} <br/>");

            if (!string.IsNullOrWhiteSpace(avsenderDistribusjon.Email))
                notificationBuilder.AppendLine($"E-post: {avsenderDistribusjon.Email}");

            notificationBuilder.AppendLine($"</p>");

            string avsenderId = string.IsNullOrEmpty(avsenderOrgNummer) ? "" : $" (org. nr. {avsenderOrgNummer})";
            notificationBuilder.AppendLine($"<p><em>Det er ikke mulig å svare på denne e-posten. Har du spørsmål om varselet i Altinn, kan du kontakte Direktoratet for byggkvalitet. Oppgi referanse: {archiveReference}<br/>Spørsmål om inneholdet i brevet, må du rette til {avsenderDistribusjon.Name}{avsenderId}.</em></p>");
            return notificationBuilder.ToString();
        }

        private bool BeroerteEiendommerHasAddressInformation(GjelderEiendomType[] eiendommer)
        {
            if (eiendommer?.Length == 0)
                return false;

            return eiendommer != null && eiendommer.Any(e =>
                !string.IsNullOrWhiteSpace(e?.adresse?.adresselinje1) ||
                (!string.IsNullOrWhiteSpace(e?.eiendomsidentifikasjon?.gaardsnummer) &&
                 !string.IsNullOrWhiteSpace(e.eiendomsidentifikasjon?.bruksnummer)
                ));
        }

        private static IEnumerable<string> GetBeroertEiendomInformation(GjelderEiendomType[] eiendommer)
        {
            if (eiendommer == null) yield break;
            foreach (var eiendom in eiendommer)
            {
                var sb = new StringBuilder();
                var hasAdresselinje1 = !string.IsNullOrWhiteSpace(eiendom.adresse?.adresselinje1);
                var hasGaardsnummer = !string.IsNullOrWhiteSpace(eiendom.eiendomsidentifikasjon.gaardsnummer);
                var hasFestenummer = !string.IsNullOrWhiteSpace(eiendom.eiendomsidentifikasjon.festenummer);
                var hasSeksjonsnummer = !string.IsNullOrWhiteSpace(eiendom.eiendomsidentifikasjon.seksjonsnummer);

                if (hasAdresselinje1)
                {
                    sb.Append(eiendom.adresse.adresselinje1);

                    if (hasGaardsnummer || hasFestenummer || hasSeksjonsnummer)
                        sb.Append(" (");
                }

                if (hasGaardsnummer)
                {
                    if (!string.IsNullOrWhiteSpace(eiendom.eiendomsidentifikasjon.bruksnummer))
                        sb.Append(
                            $"gnr./bnr. {eiendom.eiendomsidentifikasjon.gaardsnummer}/{eiendom.eiendomsidentifikasjon.bruksnummer}");
                    else
                        sb.Append($"gnr. {eiendom.eiendomsidentifikasjon.gaardsnummer}");

                    if (hasFestenummer || hasSeksjonsnummer)
                        sb.Append(", ");
                }

                if (hasFestenummer)
                {
                    sb.Append($"festenr. {eiendom.eiendomsidentifikasjon.festenummer}");

                    if (hasSeksjonsnummer)
                        sb.Append(", ");
                }

                if (hasSeksjonsnummer)
                {
                    sb.Append($"seksjonsnr. {eiendom.eiendomsidentifikasjon.seksjonsnummer}");
                }

                if (hasAdresselinje1)
                    sb.Append(')');

                yield return sb.ToString();
            }
        }
    }
}
