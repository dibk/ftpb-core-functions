using Altinn.Common.Models;
using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using Dibk.Ftpb.Integration.SvarUt.Builders.Models;
using Dibk.Ftpb.Integration.SvarUt.Models;
using FtB_Common;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Constants;
using FtB_Common.Encryption;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_FormLogic.Distributions.DistributionFormLogic;
using FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidTilHøringsmyndigheterLogic;
using FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidTilHøringsmyndigheterLogic.Send;
using FtB_FormLogic.Shipments.SvarUt;
using FtB_Print.Services.Print;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Dibk.Ftpb.Common.Constants.DataTypes;

namespace FtB_FormLogic
{
    [FormDataFormat(DataFormatId = DataFormatIDs.VarselPlanoppstart, DataFormatVersion = VorpahSenderHelper.DataFormatVersion, ProcessingContext = FormLogicProcessingContext.Send)]
    public class VarselOppstartPlanarbeidPlussHoeringsmyndigheterSendLogic : DistributionSendLogic<PlanvarselV2, Beroertpart>
    {
        private readonly EnvironmentProvider _env;
        private readonly SvarUtShipmentBuilder _svarUtShipmentBuilder;

        protected override List<string> AttachmentsNotToIncludeForOrganizationsAndPrivatePersons => new()
        {
            Planomraade, PlanomraadeSosi
        };
        protected override List<string> AttachmentOrder => ReceiptData.AttachmentOrder;

        public VarselOppstartPlanarbeidPlussHoeringsmyndigheterSendLogic(IFormDataRepo repo,
                                                                         IBlobOperations blobOperations,
                                                                         ILogger<VarselOppstartPlanarbeidPlussHoeringsmyndigheterSendLogic> logger,
                                                                         VorpahDistribution distributionAdapter,
                                                                         IDistributionDataMapper<PlanvarselV2> distributionDataMapper,
                                                                         VarselOppstartPlanarbeidPlussHøringsmyndigheterSendPrefillMapper prefillMapper,
                                                                         IDbUnitOfWork dbUnitOfWork,
                                                                         IDecryptionFactory decryptionFactory,
                                                                         IPrintService printService,
                                                                         IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                                                         EnvironmentProvider env,
                                                                         SvarUtShipmentBuilder svarUtShipmentBuilder,
                                                                         DistributionReceiverRepo distributionReceiverRepo,
                                                                         DistributionReceiverLogRepo distributionReceiverLogRepo)
            : base(repo,
                  blobOperations,
                  logger,
                  distributionAdapter,
                  distributionDataMapper,
                  prefillMapper,
                  dbUnitOfWork,
                  decryptionFactory,
                  printService,
                  fileDownloadHttpClient,
                  distributionReceiverRepo,
                  distributionReceiverLogRepo)
        {
            _env = env;
            _svarUtShipmentBuilder = svarUtShipmentBuilder;
        }

        protected override void SetBeroertPart(PlanvarselV2 formData, Beroertpart beroertPart)
        {
            formData.BeroerteParter = new[] { beroertPart };
        }

        protected override DistributionSender GetDistributionSender(string decryptedReporteeId)
        {
            return VorpahSenderHelper.GetDistributionSender(DecryptionFactory.GetDecryptor(), decryptedReporteeId, FormData);
        }

        protected override async Task AddShipmentForHoeringsmyndighet(IDistributionMessage distributionMessage, Guid distributionFormReferenceId)
        {
            if (distributionMessage is not VorpaPlussHDistributionMessage dMessage)
                return;

            var ekstraMetadataFraAvleverendeSystem = new List<Entry>
            {
                new() { Key = "Hovedinnsendingsnummer", Value = distributionFormReferenceId.ToString() }
            };

            dMessage.SvarUtForsendelse = await _svarUtShipmentBuilder
                .AddAttachmentsFromPublicStorage(PublicBlobContainerName)
                .AddEkstraMetadataFraAvleverendeSystem(ekstraMetadataFraAvleverendeSystem)
                .UseFormDataWithoutBeroerteParter()
                .Build(ArchiveReference, Receiver, Sender, GetShipmentData());
        }

        private ForsendelseDataBygg GetShipmentData()
        {
            return new ForsendelseDataBygg(SendQueueItem.ArchiveReference)
            {
                ForsendelseType ="Varsel om planoppstart",
                ForsendelseTittel = $"Varsel om planoppstart: {FormData.Planforslag?.Plannavn}",
                SøknadSkjemaNavn = "Varsel om oppstart av reguleringsplanarbeid",
                Konteringskode = "VORPA",
                Adresselinje1 = FormData.EiendomByggested?[0].Adresse?.Adresselinje1,
                Adresselinje2 = FormData.EiendomByggested?[0].Adresse?.Adresselinje2,
                Adresselinje3 = FormData.EiendomByggested?[0].Adresse?.Adresselinje3,
                Kommunenummer = FormData.EiendomByggested?[0].Eiendomsidentifikasjon?.Kommunenummer,
                Gårdsnummer = FormData.EiendomByggested?[0].Eiendomsidentifikasjon?.Gaardsnummer,
                Bruksnummer = FormData.EiendomByggested?[0].Eiendomsidentifikasjon?.Bruksnummer,
                Seksjonsnummer = FormData.EiendomByggested?[0].Eiendomsidentifikasjon?.Seksjonsnummer,
                Festenummer = FormData.EiendomByggested?[0].Eiendomsidentifikasjon?.Festenummer,
                Postnr = FormData.EiendomByggested?[0].Adresse?.Postnr,
                Poststed = FormData.EiendomByggested?[0].Adresse?.Poststed,
                AvgivendeSystem = FormData.Metadata?.FraSluttbrukersystem,
                Bolignummer = FormData.EiendomByggested?[0].Bolignummer,
                KommunensSaksnummerSekvensnummer = null, // Høringsmyndigheter har egne saksnummer og skal ikke bruke disse
                KommunensSaksnummerÅr = null, // Høringsmyndigheter har egne saksnummer og skal ikke bruke disse
            };
        }

        protected override Dictionary<string, string> GetAttachmentsWithDisplayName(List<AltinnPrefillData> attachments)
        {
            return ReceiptData.GetAttachemntsWithDisplayName(attachments.Select(a => (a.Id, a.Filename)))
                .ToDictionary(keySelector => keySelector.fileName, valueSelector => valueSelector.displayname);
        }

        public override Task SetSenderAsync()
        {
            if (SendQueueItem.Sender.Name == FormData.Plankonsulent?.Navn)
            {
                Sender = new ShipmentActor(SendQueueItem.Sender)
                {
                    Address = FormData.Plankonsulent.Adresse.Adresselinje1,
                    PostalCode = FormData.Plankonsulent.Adresse.Postnr,
                    Poststed = FormData.Plankonsulent.Adresse.Poststed
                };
            }
            else
            {
                Sender = new ShipmentActor(SendQueueItem.Sender)
                {
                    Address = FormData.Forslagsstiller.Adresse.Adresselinje1,
                    PostalCode = FormData.Forslagsstiller.Adresse.Postnr,
                    Poststed = FormData.Forslagsstiller.Adresse.Poststed
                };
            }

            return Task.CompletedTask;
        }
    }
}