﻿using Dibk.Ftpb.Common.Datamodels;
using FtB_Common.BusinessModels;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.Exceptions;
using System.Collections.Generic;

namespace FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidTilHøringsmyndigheterLogic
{
    public class VorpahSenderHelper
    {
        public const string DataFormatVersion = "2.0";

        public static List<Actor> GetDistributionSenderCandidates(IDecryption decryptor, PlanvarselV2 planvarsel)
        {
            var retval = new List<Actor>();
            if (!string.IsNullOrEmpty(planvarsel.Forslagsstiller.Organisasjonsnummer))
                retval.Add(new Actor() { Id = planvarsel.Forslagsstiller.Organisasjonsnummer, Name= planvarsel.Forslagsstiller.Navn, Type = ActorTypeEnum.Foretak });

            if (!string.IsNullOrEmpty(planvarsel.Forslagsstiller.Foedselsnummer))
                retval.Add(new Actor() { Id = decryptor.DecryptText(planvarsel.Forslagsstiller.Foedselsnummer), Name = planvarsel.Forslagsstiller.Navn, Type = ActorTypeEnum.Privatperson });

            if (!string.IsNullOrEmpty(planvarsel.Plankonsulent?.Organisasjonsnummer))
                retval.Add(new Actor() { Id = planvarsel.Plankonsulent.Organisasjonsnummer, Name = planvarsel.Plankonsulent.Navn, Type = ActorTypeEnum.Foretak });

            if (!string.IsNullOrEmpty(planvarsel.Plankonsulent?.Foedselsnummer))
                retval.Add(new Actor() { Id = decryptor.DecryptText(planvarsel.Plankonsulent.Foedselsnummer), Name = planvarsel.Plankonsulent.Navn, Type = ActorTypeEnum.Privatperson });

            return retval;
        }

        public static DistributionSender GetDistributionSender(IDecryption decryptor, string decryptedReporteeId, PlanvarselV2 planvarsel)
        {
            if (!string.IsNullOrEmpty(planvarsel.Plankonsulent?.Organisasjonsnummer) && (planvarsel.Plankonsulent?.Organisasjonsnummer == decryptedReporteeId)
                    ||
                    !string.IsNullOrEmpty(planvarsel.Plankonsulent?.Foedselsnummer) && (decryptor.DecryptText(planvarsel.Plankonsulent.Foedselsnummer) == decryptedReporteeId)
                    )
            {
                return new DistributionSender()
                {
                    Role = DistributionSenderRoleEnum.Plankonsulent,
                    Name = planvarsel.Plankonsulent.Navn,
                    Phone = planvarsel.Plankonsulent.Telefon,
                    Email = planvarsel.Plankonsulent.Epost
                };
            }


            if (!string.IsNullOrEmpty(planvarsel.Forslagsstiller.Organisasjonsnummer) && (planvarsel.Forslagsstiller.Organisasjonsnummer == decryptedReporteeId)
                ||
                !string.IsNullOrEmpty(planvarsel.Forslagsstiller.Foedselsnummer) && (decryptor.DecryptText(planvarsel.Forslagsstiller.Foedselsnummer) == decryptedReporteeId)
                )
            {

                return new DistributionSender()
                {
                    Role = DistributionSenderRoleEnum.Forslagsstiller,
                    Name = planvarsel.Forslagsstiller.Navn,
                    Phone = planvarsel.Forslagsstiller.Telefon,
                    Email = planvarsel.Forslagsstiller.Epost
                };
            }


            throw new InvalidDistributionSenderException("The form has no legal distribution sender.");
        }
    }
}
