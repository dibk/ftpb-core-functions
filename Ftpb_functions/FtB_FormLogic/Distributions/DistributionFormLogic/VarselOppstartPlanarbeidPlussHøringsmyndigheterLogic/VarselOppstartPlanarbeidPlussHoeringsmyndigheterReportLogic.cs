using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using Altinn3.Adapters;
using Dibk.Ftpb.Common.Constants;
using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using Dibk.Ftpb.ModelToPdf.Common.Client;
using Dibk.Ftpb.ModelToPdf.Common.Models;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Constants;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_DataModels.Mappers;
using FtB_FormLogic.Distributions.DistributionFormLogic;
using FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidTilHøringsmyndigheterLogic;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Adresse = Dibk.Ftpb.Common.Datamodels.Parts.Adresse;
using Eiendom = Dibk.Ftpb.ModelToPdf.Common.Models.Eiendom;
using Eiendomsidentifikasjon = Dibk.Ftpb.ModelToPdf.Common.Models.Eiendomsidentifikasjon;

namespace FtB_FormLogic
{
    [FormDataFormat(DataFormatId = DataFormatIDs.VarselPlanoppstart, DataFormatVersion = VorpahSenderHelper.DataFormatVersion, ProcessingContext = FormLogicProcessingContext.Report)]
    public class VarselOppstartPlanarbeidPlussHoeringsmyndigheterReportLogic : DistributionReportLogic<PlanvarselV2, Beroertpart>
    {
        private readonly IHtmlUtils _htmlUtils;
        private readonly IDecryption _decryptor;

        protected override bool CompleteAltinn3Instance => true;

        protected override StatusApiFileInfo MainFormDownloadFileInfo => new()
        {
            Name = DataTypes.Planvarsel,
            FileName = $"{DataTypes.Planvarsel}.pdf",
            FileType = FileTypesForDownloadEnum.Planvarsel,
            MimeType = "application/pdf"
        };

        protected override StatusApiFileInfo ReceiptDownloadFileInfo => new()
        {
            Name = "Kvittering for varsel om oppstart av planarbeid",
            FileName = "varsel_oppstart_planarbeid_kvittering.pdf",
            FileType = FileTypesForDownloadEnum.PlanvarselKvittering,
            MimeType = "application/pdf"
        };

        public VarselOppstartPlanarbeidPlussHoeringsmyndigheterReportLogic(
            IHtmlUtils htmlUtils,
            IDecryptionFactory decryptionFactory,
            INotificationAdapter notificationAdapter,
            IDatamodelToPdfHttpClient datamodelToPdfHttpClient,
            IBlobOperations blobOperations,
            IAltinnInstanceCompleter altinnInstanceCompleter,
            DistributionSubmittalRepo distributionSubmittalRepo,
            IFormDataRepo repo,
            ILogger<VarselOppstartPlanarbeidPlussHoeringsmyndigheterReportLogic> log,
            IDbUnitOfWork dbUnitOfWork,
            IFileDownloadStatusHttpClient fileDownloadHttpClient,
            DistributionReceiverRepo distributionReceiverRepo,
            DistributionReceiverLogRepo distributionReceiverLogRepo
        ) : base(
            notificationAdapter,
            datamodelToPdfHttpClient,
            blobOperations,
            altinnInstanceCompleter,
            distributionSubmittalRepo,
            repo,
            log,
            dbUnitOfWork,
            fileDownloadHttpClient,
            distributionReceiverRepo,
            distributionReceiverLogRepo
        )
        {
            _htmlUtils = htmlUtils;
            _decryptor = decryptionFactory.GetDecryptor();
        }

        protected override void SetBeroertPart(PlanvarselV2 formData, Beroertpart beroertPart)
        {
            formData.BeroerteParter = new[] { beroertPart };
        }

        protected override async Task<AltinnReceiver> GetReceiver()
        {
            var reportee = await BlobOperations.GetReporteeIdFromStoredBlobAsync(this.ArchiveReference);
            var decryptedReportee = _decryptor.DecryptText(reportee);
            var receiverCandidates = VorpahSenderHelper.GetDistributionSenderCandidates(_decryptor, FormData);
            Actor actor = receiverCandidates.FirstOrDefault(r => r.Id.Equals(decryptedReportee));

            return new AltinnReceiver() { Id = actor.Id, Type = GetAltinnReceiverTypeFromActorTypeEnum(actor.Type) };
        }

        private AltinnReceiverType GetAltinnReceiverTypeFromActorTypeEnum(ActorTypeEnum type)
        {
            switch (type)
            {
                case ActorTypeEnum.Privatperson:
                    return AltinnReceiverType.Privatperson;

                case ActorTypeEnum.Foretak:
                case ActorTypeEnum.OffentligMyndighet:
                case ActorTypeEnum.Organisasjon:
                case ActorTypeEnum.Plankonsulent:
                    return AltinnReceiverType.Foretak;

                default:
                    return AltinnReceiverType.Foretak;
            }
        }

        protected override MessageDataType GetSubmitterReceiptMessage(string archiveReference)
        {
            try
            {
                string adresse = FormData.EiendomByggested != null && FormData.EiendomByggested.Any() ? GetAddressWithPostalLocationInfo(FormData.EiendomByggested?.First().Adresse) : null;
                string planNavn = FormData.Planforslag.Plannavn ?? "";
                string byggested = !string.IsNullOrWhiteSpace(adresse) ? $"{adresse}, {planNavn}" : $"{planNavn}";
                string htmlBody = _htmlUtils.GetHtmlFromTemplate("FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidPlussHøringsmyndigheterLogic.Report.VarselOppstartPlanarbeidPlussHoeringsmyndigheterReceiptMessageBody.html");

                htmlBody = htmlBody.Replace("<plannavn/>", planNavn);
                htmlBody = htmlBody.Replace("<arkivReferanse/>", archiveReference.ToUpper());

                var mess = new MessageDataType()
                {
                    MessageTitle = $"Kvittering - varsel om oppstart av reguleringsplanarbeid, {byggested}",
                    MessageSummary = "Trykk på vedleggene under for å laste ned varselet og kvittering med liste over hvilke berørte parter som har blitt varslet",
                    MessageBody = htmlBody
                };

                return mess;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Error occurred while getting submitter receipt message");

                throw;
            }
        }

        protected override bool? SubmitterIsPlankonsulent(string encryptedReporteeId)
        {
            var reporteeId = _decryptor.DecryptText(encryptedReporteeId);

            return reporteeId.Equals(FormData.Plankonsulent?.Organisasjonsnummer);
        }

        protected override async Task<AltinnNotificationMessage> CreateNotificationMessageAsync(string archiveReference)
        {
            var notificationMessage = await base.CreateNotificationMessageAsync(archiveReference);

            var publicContainerName = BlobOperations.GetPublicBlobContainerName(archiveReference.ToLower());
            var blobStorageTypes = new List<string>
            {
                BlobStorageMetadataTypes.SubmittalAttachment
            };

            var listOfAttachmentsInSubmittal = await BlobOperations.GetListOfBlobsMetadataValuesByBlobItemTypesAsync(BlobStorageEnum.Public, publicContainerName, blobStorageTypes);
            var attachmentOrdered = listOfAttachmentsInSubmittal.OrderBy(a => ReceiptData.AttachmentOrder.IndexOf(a.attachmentType)).ToList();
            var attachmentsWithDisplayName = ReceiptData.GetAttachemntsWithDisplayName(attachmentOrdered.Select(a => (a.attachmentType, a.fileName)))
                .ToDictionary(keySelector => keySelector.fileName, valueSelector => valueSelector.displayname);

            var attachments = new List<Attachment>(notificationMessage.Attachments);

            foreach (var ( attachmentType,  fileName,  _) in attachmentOrdered)
            {
                var blobContent = await BlobOperations.GetBlobContentAsync(BlobStorageEnum.Public, publicContainerName, fileName);

                attachments.Add(new AttachmentBinary
                {
                    BinaryContent = blobContent.Content,
                    Filename = fileName,
                    Name = attachmentsWithDisplayName[fileName],
                    ArchiveReference = archiveReference,
                    Size = blobContent.FileSize,
                });
            }

            notificationMessage.Attachments = attachments;

            return notificationMessage;
        }

        public static List<IGrouping<string, Beroertpart>> GetGroupsOfBeroerteParterWithDecryptedIDsParallel(Beroertpart[] beroerteParter, IEnumerable<DistributionReceiverDto> successfullyNotified, Func<string, string> decryptorFunc)
        {
            var beroertPartListeWithDecryptedIDsFodselsnummer = new ConcurrentBag<Beroertpart>();
            var beroertPartListeWithDecryptedIDsOrganisasjonsnummer = new ConcurrentBag<Beroertpart>();

            Parallel.ForEach(beroerteParter, (beroertPart) =>
            {
                if (!string.IsNullOrEmpty(beroertPart.Organisasjonsnummer))
                {
                    beroertPartListeWithDecryptedIDsOrganisasjonsnummer.Add(beroertPart);
                }

                if (!string.IsNullOrEmpty(beroertPart.Foedselsnummer))
                {
                    var newBeroertPartWithDecryptedId = new Beroertpart
                    {
                        Navn = beroertPart.Navn,
                        Adresse = beroertPart.Adresse,
                        Foedselsnummer = decryptorFunc(beroertPart.Foedselsnummer),
                        GjelderEiendom = beroertPart.GjelderEiendom,
                        Partstype = beroertPart.Partstype,
                        Telefon = beroertPart.Telefon,
                        Epost = beroertPart.Epost
                    };
                    beroertPartListeWithDecryptedIDsFodselsnummer.Add(newBeroertPartWithDecryptedId);
                }
            });

            var groupedByFoedselsnummer = beroertPartListeWithDecryptedIDsFodselsnummer.GroupBy(b => b.Foedselsnummer).ToList();
            var completedGroupsWithDecryptedIds = beroertPartListeWithDecryptedIDsOrganisasjonsnummer.GroupBy(b => b.Organisasjonsnummer).ToList();
            completedGroupsWithDecryptedIds.AddRange(groupedByFoedselsnummer);
            var listOfDecryptedSucsessfullyNotifiedIds = new ConcurrentBag<string>();

            Parallel.ForEach(successfullyNotified, (notified) =>
            {
                listOfDecryptedSucsessfullyNotifiedIds.Add(decryptorFunc(notified.ReceiverId));
            });
            var noe = completedGroupsWithDecryptedIds.Select(g => g.Key).Intersect(listOfDecryptedSucsessfullyNotifiedIds);
            completedGroupsWithDecryptedIds.Where(p => listOfDecryptedSucsessfullyNotifiedIds.Contains(p.Key));

            return completedGroupsWithDecryptedIds;
        }

        protected override IEnumerable<(string Id, string navn)> GetSocialSecurityNumbers()
        {
            return FormData.BeroerteParter
                    .Where(x => x.Foedselsnummer != null)
                    .Select(x => (x.Foedselsnummer, x.Navn));
        }

        protected override IEnumerable<(string Id, string navn)> GetOrganisationNumbers()
        {
            return FormData.BeroerteParter
                    .Where(x => x.Organisasjonsnummer != null)
                    .Select(x => (x.Organisasjonsnummer, x.Navn));
        }

        private static string GetAddressWithPostalLocationInfo(Adresse eiendom)
        {
            if (eiendom == null) { return null; }
            if (string.IsNullOrWhiteSpace(eiendom.Adresselinje1))
                return "";

            if (string.IsNullOrWhiteSpace(eiendom.Postnr) || string.IsNullOrWhiteSpace(eiendom.Poststed))
                return $"{eiendom.Adresselinje1}";

            return $"{eiendom.Adresselinje1}, {eiendom.Postnr} {eiendom.Poststed}";
        }

        protected override PlanModelToPdfKvittering GetModelToPdfReceiptInfo()
        {
            return new()
            {
                Tittel = "Kvittering for varsel om oppstart av reguleringsplanarbeid",
                Plannavn = FormData.Planforslag.Plannavn ?? "",
                Forslagstillernavn = FormData.Forslagsstiller.Navn,
                Plankonsulentnavn = FormData.Plankonsulent?.Navn ?? ""
            };
        }

        protected override async Task<(List<Interessent> notifiedParties, List<Interessent> notNotifiedParties)> GetInterestedPartiesAsync(string archiveReference)
        {
            var successfullyNotified = await GetReceiverSuccessfullyNotifiedAsync(archiveReference);

            var groupsWithBeroerteParterWithDecryptedIDs = GetGroupsOfBeroerteParterWithDecryptedIDsParallel(FormData.BeroerteParter, successfullyNotified, (ssn) => _decryptor.DecryptText(ssn));
            var listOfBeroertPartWithEiendom = VarselOppstartPlanarbeidPlussHoeringsmyndigheterMappers.GetListOfPlanvarselBeroertPartWithEiendomFromGroups(groupsWithBeroerteParterWithDecryptedIDs);

            var listOfNotReachableReceiverNames = await GetNotReachableReceiversAsync();

            var notReachableBeroerteParter = listOfBeroertPartWithEiendom.Where(b =>
                listOfNotReachableReceiverNames.Contains(b.Navn, StringComparer.InvariantCultureIgnoreCase)).ToList();

            var notNotifiedParties = new List<Interessent>();
            foreach (var receiverName in listOfNotReachableReceiverNames)
            {
                notNotifiedParties.Add(new Interessent()
                {
                    Navn = receiverName
                });
            }

            var notifiedParties = new List<Interessent>();
            foreach (var receiver in listOfBeroertPartWithEiendom.Except(notReachableBeroerteParter))
            {
                var interestedParty = new Interessent()
                {
                    Navn = receiver.Navn,
                    PartstypeKodebeskrivelse = receiver.Partstype.Kodebeskrivelse,
                };

                interestedParty.BerørteEiendommer = new List<Eiendom>();

                if (receiver.GjelderEiendom != null)
                    foreach (var eiendom in receiver.GjelderEiendom)
                    {
                        interestedParty.BerørteEiendommer.Add(new Eiendom()
                        {
                            Adresse = new Dibk.Ftpb.ModelToPdf.Common.Models.Adresse()
                            {
                                Adresselinje1 = eiendom.Adresse?.Adresselinje1,
                                Adresselinje2 = eiendom.Adresse?.Adresselinje2,
                                Adresselinje3 = eiendom.Adresse?.Adresselinje3,
                                Postnummer = eiendom.Adresse?.Postnr,
                                Poststed = eiendom.Adresse?.Poststed,
                            },
                            Eiendomsidentifikasjon = new Eiendomsidentifikasjon()
                            {
                                Gaardsnummer = eiendom.Eiendomsidentifikasjon?.Gaardsnummer,
                                Bruksnummer = eiendom.Eiendomsidentifikasjon?.Bruksnummer,
                                Seksjonsnummer = eiendom.Eiendomsidentifikasjon?.Seksjonsnummer,
                                Festenummer = eiendom.Eiendomsidentifikasjon?.Festenummer
                            }
                        });
                    }

                notifiedParties.Add(interestedParty);
            }

            return (notifiedParties, notNotifiedParties);
        }
    }
}