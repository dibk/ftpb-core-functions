﻿using Dibk.Ftpb.Common.Constants;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FtB_FormLogic.Distributions.DistributionFormLogic
{
    public class Vorpah
    {
        public static readonly string FormDataFormType = "Distribusjon for varsel om oppstart av reguleringsplanarbeid";
    }

    public class Hoffe
    {
        public static readonly string FormDataFormType = "Distribusjon for høring og offentlig ettersyn";
        public static readonly string FormDataApplication = "Fellestjenester Plan";
    }

    public class Plan
    {
        public static string BuildAdresselinje(string adresselinje1, string adresselinje2, string adresselinje3, string postnr, string poststed)
        {
            var adrParts = new List<string>();

            if (!string.IsNullOrEmpty(adresselinje1))
                adrParts.Add(adresselinje1);

            if (!string.IsNullOrEmpty(adresselinje2))
                adrParts.Add(adresselinje2);

            if (!string.IsNullOrEmpty(adresselinje3))
                adrParts.Add(adresselinje3);

            var postnrBuilder = new StringBuilder();
            postnrBuilder.Append(!string.IsNullOrEmpty(postnr) ? postnr : string.Empty);
            postnrBuilder.Append(!string.IsNullOrEmpty(postnr) ? " " : string.Empty);
            postnrBuilder.Append(!string.IsNullOrEmpty(poststed) ? poststed : string.Empty);

            if (postnrBuilder.Length > 0)
                adrParts.Add(postnrBuilder.ToString());

            return string.Join(", ", adrParts);
        }
    }

    public class ReceiptData
    {
        public static readonly IEnumerable<(string attachmentTypeName, string displayName)> DisplayName = new List<(string attachmentTypeName, string displayName)>
        {
            (DataTypes.Varselbrev, AttachmentDisplayName.Varselbrev),
            (DataTypes.Planvarsel, AttachmentDisplayName.Planvarsel),
            (DataTypes.Planinitiativ, AttachmentDisplayName.Planinitiativ),
            (DataTypes.Planprogram, AttachmentDisplayName.Planprogram),
            (DataTypes.KartDetaljert, AttachmentDisplayName.KartDetaljert),
            (DataTypes.KartPlanavgrensing, AttachmentDisplayName.KartPlanavgrensing),
            (DataTypes.KartPlanavgrensning, AttachmentDisplayName.KartPlanavgrensning),
            (DataTypes.Planomraade, AttachmentDisplayName.Planomraade),
            (DataTypes.PlanomraadePdf, AttachmentDisplayName.PlanomraadePdf),
            (DataTypes.PlanomraadeSosi, AttachmentDisplayName.PlanomraadeSosi),
            (DataTypes.ReferatOppstartsmoete, AttachmentDisplayName.ReferatOppstartsmoete),
            (DataTypes.Annet, AttachmentDisplayName.Annet)
        };

        public static List<string> AttachmentOrder
        {
            get
            {
                var attachmentOrder = new List<string>
                {
                    DataTypes.Varselbrev,
                    DataTypes.Planvarsel,
                    DataTypes.Planinitiativ,
                    DataTypes.Planprogram,
                    DataTypes.KartDetaljert,
                    DataTypes.KartPlanavgrensing,
                    DataTypes.KartPlanavgrensning,
                    DataTypes.Planomraade,
                    DataTypes.PlanomraadePdf,
                    DataTypes.PlanomraadeSosi,
                    DataTypes.ReferatOppstartsmoete,
                    DataTypes.Annet,
                };

                return attachmentOrder;
            }
        }

        public static List<(string displayname, string fileName)> GetAttachemntsWithDisplayName(IEnumerable<(string attachmentType, string fileName)> attachments)
        {
            return (from attachment in attachments
                    let displayname = DisplayName
                        .Where(d => d.attachmentTypeName == attachment.attachmentType)
                        .Select(a => a.displayName)
                        .FirstOrDefault()
                    select (string.IsNullOrEmpty(displayname) ? DataTypes.Annet : displayname, attachment.fileName)).ToList();
        }
    }

    public class HoffeReceiptData
    {
        public static readonly IEnumerable<(string attachmentTypeName, string displayName)> DisplayName = new List<(string attachmentTypeName, string displayName)>
        {
            (DataTypes.Hoeringsbrev, AttachmentDisplayName.Hoeringsbrev ),
            (DataTypes.PlankartPdf, AttachmentDisplayName.PlankartPdf),
            (DataTypes.PlankartGml, AttachmentDisplayName.PlankartGml),
            (DataTypes.PlankartSosi, AttachmentDisplayName.PlankartSosi),
            (DataTypes.PlanbestemmelsePdf, AttachmentDisplayName.Planbeskrivelse),
            (DataTypes.PlanbestemmelseXml, AttachmentDisplayName.Planbestemmelser),
            (DataTypes.Planbeskrivelse, AttachmentDisplayName.Planbeskrivelse),
            (DataTypes.Konsekvensutredning, AttachmentDisplayName.Konsekvensutredning),
            (DataTypes.Illustrasjoner, AttachmentDisplayName.Illustrasjoner),
            (DataTypes.ROSAnalyse, AttachmentDisplayName.ROSAnalyse),
            (DataTypes.Annet, AttachmentDisplayName.Annet),
        };

        public static List<string> AttachmentOrder
        {
            get
            {
                var attachmentOrder = new List<string>
                {
                    DataTypes.Hoeringsbrev,
                    DataTypes.PlankartPdf,
                    DataTypes.PlankartGml,
                    DataTypes.PlankartSosi,
                    DataTypes.PlanbestemmelsePdf,
                    DataTypes.PlanbestemmelseXml,
                    DataTypes.Planbeskrivelse,
                    DataTypes.Konsekvensutredning,
                    DataTypes.Illustrasjoner,
                    DataTypes.ROSAnalyse,
                    DataTypes.Annet,
                };

                return attachmentOrder;
            }
        }

        public static List<(string displayname, string fileName)> GetAttachmentsWithDisplayName(IEnumerable<(string attachmentType, string fileName)> attachments)
        {
            return (from attachment in attachments
                    let displayname = DisplayName
                        .Where(d => d.attachmentTypeName == attachment.attachmentType)
                        .Select(a => a.displayName)
                        .FirstOrDefault()
                    select (string.IsNullOrEmpty(displayname) ? DataTypes.Annet : displayname, attachment.fileName)).ToList();
        }
    }
}