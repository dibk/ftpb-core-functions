using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_FormLogic.Distributions.DistributionFormLogic;
using FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidLogic;
using FtB_Print.Services.Print;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using no.kxml.skjema.dibk.nabovarselPlan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    [FormDataFormat(DataFormatId = "6325", DataFormatVersion = "44824", ProcessingContext = FormLogicProcessingContext.Send)]
    public class VarselOppstartPlanarbeidSendLogic : DistributionSendLogic<NabovarselPlanType, BeroertPartType>
    {
        protected override List<string> AttachmentsNotToIncludeForOrganizationsAndPrivatePersons => Array.Empty<string>().ToList();
        protected override List<string> AttachmentOrder => ReceiptData.AttachmentOrder;

        public VarselOppstartPlanarbeidSendLogic(IFormDataRepo repo,
                                                 IBlobOperations blobOperations,
                                                 ILogger<VarselOppstartPlanarbeidSendLogic> log,
                                                 IDistributionAdapter distributionAdapter,
                                                 IDistributionDataMapper<NabovarselPlanType> distributionDataMapper,
                                                 VarselOppstartPlanarbeidPrefillMapper prefillMapper,
                                                 IDbUnitOfWork dbUnitOfWork,
                                                 IDecryptionFactory decryptionFactory,
                                                 IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                                 IPrintService printService,
                                                 DistributionReceiverRepo distributionReceiverRepo,
                                                 DistributionReceiverLogRepo distributionReceiverLogRepo)
            : base(repo,
                  blobOperations,
                  log,
                  distributionAdapter,
                  distributionDataMapper,
                  prefillMapper,
                  dbUnitOfWork,
                  decryptionFactory,
                  printService,
                  fileDownloadHttpClient,
                  distributionReceiverRepo,
                  distributionReceiverLogRepo)
        {
        }

        protected override void SetBeroertPart(NabovarselPlanType formData, BeroertPartType beroertPart)
        {
            formData.beroerteParter = new[] { beroertPart };
        }

        protected override DistributionSender GetDistributionSender(string decryptedReporteeId)
        {
            return VorpaSenderHelper.GetDistributionSender(DecryptionFactory.GetDecryptor(), decryptedReporteeId, FormData);
        }

        protected override Task AddShipmentForHoeringsmyndighet(IDistributionMessage distributionMessage, Guid distributionFormReferenceId)
        {
            // Denne tjenesten sender ikkje til Høringsmyndigheter
            return Task.CompletedTask;
        }

        protected override Dictionary<string, string> GetAttachmentsWithDisplayName(List<AltinnPrefillData> attachments)
        {
            return ReceiptData.GetAttachemntsWithDisplayName(attachments.Select(a => (a.Id, a.Filename)))
                .ToDictionary(keySelector => keySelector.fileName, valueSelector => valueSelector.displayname);
        }
    }
}
