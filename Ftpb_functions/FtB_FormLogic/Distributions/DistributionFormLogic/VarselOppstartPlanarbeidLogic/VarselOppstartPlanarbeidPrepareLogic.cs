using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidLogic;
using FtB_Print.Services.Print;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using KontaktOgReservasjonsregisteret;
using Microsoft.Extensions.Logging;
using no.kxml.skjema.dibk.nabovarselPlan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    [FormDataFormat(DataFormatId = "6325", DataFormatVersion = "44824", ProcessingContext = FormLogicProcessingContext.Prepare)]
    public class VarselOppstartPlanarbeidPrepareLogic : DistributionPrepareLogic<NabovarselPlanType, BeroertPartType>
    {
        private readonly IPrintService _printService;

        protected override string DataFormatId => "NabovarselPlan";

        public VarselOppstartPlanarbeidPrepareLogic(IFormDataRepo repo,
                                                    ILogger<VarselOppstartPlanarbeidPrepareLogic> log,
                                                    IDbUnitOfWork dbUnitOfWork,
                                                    IDecryptionFactory decryptionFactory,
                                                    IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                                    IBlobOperations blobOperations,
                                                    IPrintService printService,
                                                    KRRClient krrClient,
                                                    DistributionReceiverRepo distributionReceiverRepo,
                                                    DistributionReceiverLogRepo distributionReceiverLogRepo,
                                                    DistributionSubmittalRepo distributionSubmittalRepo)
            : base(repo,
                   log,
                   dbUnitOfWork,
                   decryptionFactory,
                   blobOperations,
                   fileDownloadHttpClient,
                   krrClient,
                   distributionReceiverRepo,
                   distributionReceiverLogRepo,
                   distributionSubmittalRepo)
        {
            _printService = printService;
        }

        public override async Task<IEnumerable<SendQueueItem>> ExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {
            await _printService.ConvertImagesToPdfAsync(submittalQueueItem.ArchiveReference);

            return await base.ExecuteAsync(submittalQueueItem);
        }

        public override async Task SetSenderAsync()
        {
            var reportee = await BlobOperations.GetReporteeIdFromStoredBlobAsync(ArchiveReference);
            string id;
            ActorTypeEnum senderType;

            try
            {
                var decryptedReportee = DecryptionFactory.GetDecryptor().DecryptText(reportee);
                var distributionSenderCandidates = VorpaSenderHelper.GetDistributionSenderCandidates(DecryptionFactory.GetDecryptor(), FormData);

                var distributionSender = distributionSenderCandidates.FirstOrDefault(r => r.Id.Equals(decryptedReportee));
                id = distributionSender.Id;

                senderType = distributionSender.Type == Altinn.Common.Models.AltinnReceiverType.Privatperson ?
                    ActorTypeEnum.Privatperson : ActorTypeEnum.Foretak;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"Error set sender for archiveReference={ArchiveReference}.");
                throw;
            }

            Sender = new Actor() { Id = id, Type = senderType };
        }

        public override async Task SetReceiversAsync()
        {
            var receivers = new Dictionary<ReceiverDistinctKeyItem, Receiver>();

            ReceiversDistinct = new Dictionary<ReceiverDistinctKeyItem, BeroertPartBlobData>();

            var counter = 0;

            foreach (var beroertPart in FormData.beroerteParter)
            {
                var receiverType = EnumExtentions.GetValueFromDescription<ActorTypeEnum>(beroertPart.partstype.kodeverdi);

                var id = receiverType.Equals(ActorTypeEnum.Privatperson) ? beroertPart.foedselsnummer : beroertPart.organisasjonsnummer;

                var decryptedId = id.Length > 11 ? DecryptionFactory.GetDecryptor().DecryptText(id) : id;

                var blobItemName = $"{counter}.json";
                var receiversDistinctKey = new ReceiverDistinctKeyItem(decryptedId, false);

                if (ReceiversDistinct.TryAdd(receiversDistinctKey, new BeroertPartBlobData { BeroertPart = beroertPart, BlobItemName = blobItemName }))
                {
                    receivers.Add(receiversDistinctKey, new Receiver { Type = receiverType, Id = id, Name = beroertPart.navn, DigitallyReserved = false, BlobItemName = blobItemName });
                    counter++;
                }
                else
                {
                    var beroerteEiendommer = ReceiversDistinct[receiversDistinctKey].BeroertPart.gjelderEiendom;
                    ReceiversDistinct[receiversDistinctKey].BeroertPart.gjelderEiendom = beroerteEiendommer.Concat(beroertPart.gjelderEiendom).ToArray();
                }
            }

            await SetDigitallyReservedInformation(receivers);

            Receivers = receivers.Values.ToList();

            Logger.LogInformation(
                "Receiver count from form data: {ReceiverCount}. Receiver count after removed duplicates: {ReceiverCountDistinct}",
                FormData.beroerteParter.Length, Receivers.Count);
        }

        protected override DistributionSubmittalDto CreateDistributionSubmittalDto(string archiveReference, string senderId,
            int receiverCount)
        {
            return new DistributionSubmittalDto(
                archiveReference,
                senderId,
                receiverCount,
                DistributionSubmittalTypes.VORPA,
                DateTime.Now
            )
            {
                ReplyDeadline = (DateTime)FormData.planforslag.fristForInnspill
            };
        }

        protected override void SetBeroerteParterToZero(NabovarselPlanType formData)
        {
            formData.beroerteParter = Array.Empty<BeroertPartType>();
        }
    }
}