﻿using Altinn.Common.Models;
using FtB_Common.BusinessModels;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using System.Collections.Generic;
using FtB_Common.Exceptions;

namespace FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidLogic
{
    public class VorpaSenderHelper
    {
        public static List<AltinnReceiver> GetDistributionSenderCandidates(IDecryption decryptor, no.kxml.skjema.dibk.nabovarselPlan.NabovarselPlanType nabovarselPlan)
        {
            var retval = new List<AltinnReceiver>();
            if (!string.IsNullOrEmpty(nabovarselPlan.forslagsstiller.organisasjonsnummer))
                retval.Add(new AltinnReceiver() { Id = nabovarselPlan.forslagsstiller.organisasjonsnummer, Type = AltinnReceiverType.Foretak });

            if (!string.IsNullOrEmpty(nabovarselPlan.forslagsstiller.foedselsnummer))
                retval.Add(new AltinnReceiver() { Id = decryptor.DecryptText(nabovarselPlan.forslagsstiller.foedselsnummer), Type = AltinnReceiverType.Privatperson });

            if (!string.IsNullOrEmpty(nabovarselPlan.planKonsulent?.organisasjonsnummer))
                retval.Add(new AltinnReceiver() { Id = nabovarselPlan.planKonsulent.organisasjonsnummer, Type = AltinnReceiverType.Foretak });

            if (!string.IsNullOrEmpty(nabovarselPlan.planKonsulent?.foedselsnummer))
                retval.Add(new AltinnReceiver() { Id = decryptor.DecryptText(nabovarselPlan.planKonsulent.foedselsnummer), Type = AltinnReceiverType.Privatperson });

            return retval;
        }

        public static DistributionSender GetDistributionSender(IDecryption decryptor, string decryptedReporteeId, no.kxml.skjema.dibk.nabovarselPlan.NabovarselPlanType planvarsel)
        {
            if (!string.IsNullOrEmpty(planvarsel.planKonsulent?.organisasjonsnummer) 
                && planvarsel.planKonsulent?.organisasjonsnummer == decryptedReporteeId 
                || !string.IsNullOrEmpty(planvarsel.planKonsulent?.foedselsnummer) 
                && decryptor.DecryptText(planvarsel.planKonsulent.foedselsnummer) 
                == decryptedReporteeId)
            {
                return new DistributionSender()
                {
                    Role = DistributionSenderRoleEnum.Plankonsulent,
                    Name = planvarsel.planKonsulent.navn,
                    Phone = planvarsel.planKonsulent.telefon,
                    Email = planvarsel.planKonsulent.epost
                };
            }

            if (!string.IsNullOrEmpty(planvarsel.forslagsstiller.organisasjonsnummer) 
                && planvarsel.forslagsstiller.organisasjonsnummer == decryptedReporteeId
                || !string.IsNullOrEmpty(planvarsel.forslagsstiller.foedselsnummer) 
                && decryptor.DecryptText(planvarsel.forslagsstiller.foedselsnummer) 
                == decryptedReporteeId)
            {

                return new DistributionSender()
                {
                    Role = DistributionSenderRoleEnum.Forslagsstiller,
                    Name = planvarsel.forslagsstiller.navn,
                    Phone = planvarsel.forslagsstiller.telefon,
                    Email = planvarsel.forslagsstiller.epost
                };
            }


            throw new InvalidDistributionSenderException("The form has no legal distribution sender.");
        }
    }
}
