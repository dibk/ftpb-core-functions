﻿using FtB_Common.Encryption;
using FtB_Common.Interfaces;
using FtB_DataModels.Mappers;
using FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidLogic.Send;
using Microsoft.Extensions.Logging;
using no.kxml.skjema.dibk.nabovarselPlan;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FtB_FormLogic
{
    public class VarselOppstartPlanarbeidPrefillMapper : IFormMapper<NabovarselPlanType>
    {
        private readonly IDecryptionFactory _decryptionFactory;
        private readonly ILogger<VarselOppstartPlanarbeidPrefillMapper> _logger;

        public VarselOppstartPlanarbeidPrefillMapper(IDecryptionFactory decryptionFactory, ILogger<VarselOppstartPlanarbeidPrefillMapper> logger)
        {
            _decryptionFactory = decryptionFactory;
            _logger = logger;
        }

        public IEnumerable<IPrefillData> Map(NabovarselPlanType form, string receiverId, string receiverName, Guid distributionFormReferenceId)
        {
            //Find all "berort part" for same receiver
            var decryptor = _decryptionFactory.GetDecryptor();
            var decryptedReceiverId = decryptor.DecryptText(receiverId);

            var berortParter = form.beroerteParter?.Where(b => (
                                                                  (b.foedselsnummer != null && decryptor.DecryptText(b.foedselsnummer).Equals(decryptedReceiverId))
                                                                  || (b.organisasjonsnummer != null && b.organisasjonsnummer.Equals(decryptedReceiverId))
                                                               )
                                                               && b.navn.Equals(receiverName)
                                                          ).ToList();

            no.kxml.skjema.dibk.nabovarselsvarPlan.GjelderEiendomType[] beroertPartGjelderEiendom = BuildBeroertPartGjelderEiendom(berortParter);

            var svarPaaNabovarsels = new List<VarselOppstartPlanarbeidData>();
            foreach (var beroertPart in berortParter)
            {
                var svarPaaNabovarsel = new no.kxml.skjema.dibk.nabovarselsvarPlan.SvarPaaNabovarselPlanType();

                var parttypemapper = NabovarselPlanMappers.GetNabovarselPartTypeMapper();

                svarPaaNabovarsel.forslagsstiller = parttypemapper.Map<PartType, no.kxml.skjema.dibk.nabovarselsvarPlan.PartType>(form.forslagsstiller);
                svarPaaNabovarsel.forslagsstiller.epost = form.forslagsstiller.epost;
                svarPaaNabovarsel.forslagsstiller.telefon = form.forslagsstiller.telefon;

                svarPaaNabovarsel.plankonsulent = parttypemapper.Map<PartType, no.kxml.skjema.dibk.nabovarselsvarPlan.PartType>(form.planKonsulent);
                svarPaaNabovarsel.plankonsulent.epost = form.planKonsulent.epost;
                svarPaaNabovarsel.plankonsulent.telefon = form.planKonsulent.telefon;

                svarPaaNabovarsel.beroertPart = NabovarselPlanMappers.GetNabovarselBerortPartMapper()
                    .Map<BeroertPartType, no.kxml.skjema.dibk.nabovarselsvarPlan.BeroertPartType>(beroertPart);

                svarPaaNabovarsel.beroertPart.gjelderEiendom = beroertPartGjelderEiendom;

                //distribution form reference id is used to connect the distribution form row with
                // the form when FTPB receives it from the notified party. 
                svarPaaNabovarsel.hovedinnsendingsnummer = distributionFormReferenceId.ToString();
                svarPaaNabovarsel.fraSluttbrukersystem = form.metadata.fraSluttbrukersystem;
                svarPaaNabovarsel.planNavn = form.planforslag.plannavn;
                svarPaaNabovarsel.planid = form.planforslag.arealplanId;
                svarPaaNabovarsel.kommune = form.kommunenavn;
                svarPaaNabovarsel.fristForInnspill = form.planforslag.fristForInnspill;
                svarPaaNabovarsel.fristForInnspillSpecified = form.planforslag.fristForInnspillSpecified;

                svarPaaNabovarsels.Add(new VarselOppstartPlanarbeidData(svarPaaNabovarsel, form.metadata.hovedinnsendingsnummer));
            }

            return svarPaaNabovarsels;
        }

        private no.kxml.skjema.dibk.nabovarselsvarPlan.GjelderEiendomType[] BuildBeroertPartGjelderEiendom(List<BeroertPartType> berortParter)
        {
            List<no.kxml.skjema.dibk.nabovarselsvarPlan.GjelderEiendomType> gjelderEiendom = new List<no.kxml.skjema.dibk.nabovarselsvarPlan.GjelderEiendomType>();

            foreach (var beroertPart in berortParter)
            {
                foreach (var eiendom in beroertPart.gjelderEiendom)
                {
                    var eiendomType = NabovarselPlanMappers.GetNabovarselGjelderEiendomTypeMapper().Map<no.kxml.skjema.dibk.nabovarselsvarPlan.GjelderEiendomType>(eiendom);

                    if (!gjelderEiendom.Contains(eiendomType))
                    {
                        gjelderEiendom.Add(eiendomType);
                    }
                }
            }

            return gjelderEiendom.ToArray();
        }
    }
}