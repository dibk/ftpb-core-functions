﻿using Altinn.Common.Models;
using FtB_Common.BusinessModels;
using FtB_Common.Encryption;
using FtB_Common.Interfaces;
using FtB_DataModels.Mappers;
using FtB_FormLogic.Distributions;
using FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidLogic.Send;
using no.kxml.skjema.dibk.nabovarselPlan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FtB_FormLogic
{
    public class VarselOppstartPlanarbeidSendDataProvider : SendDataProviderBase, IDistributionDataMapper<NabovarselPlanType>
    {
        public VarselOppstartPlanarbeidSendDataProvider(IHtmlUtils htmlUtils, IDecryptionFactory decryptionFactory) : base(htmlUtils, decryptionFactory)
        {
        }

        public IDistributionMessage GetDistributionMessage(IEnumerable<IPrefillData> prefills, NabovarselPlanType mainFormData, Guid distributionFormId, string archiveReference, DistributionSender distributionSender, bool digitallyReserved)
        {
            var prefill = prefills.First() as VarselOppstartPlanarbeidData;
            var forenkletEndring = mainFormData.planforslag?.plantype?.kodeverdi == "36";

            var distributionMessage = new AltinnDistributionMessage()
            {
                PrefillDataFormatId = prefill.DataFormatId,
                PrefillDataFormatVersion = prefill.DataFormatVersion,
                DistributionFormReferenceId = distributionFormId,
                PrefillServiceCode = prefill.PrefillServiceCode,
                PrefillServiceEditionCode = prefill.PrefillServiceEditionCode,
                PrefilledXmlDataString = prefill.ToString(),
                DaysValid = 14,
                DueDate = null
            };

            distributionMessage.NotificationMessage.Receiver = base.GetReceiver(NabovarselPlanMappers.GetNabovarselReceiverMapper().Map<DistributionReceiver>(prefill.FormInstance.beroertPart));
            distributionMessage.NotificationMessage.MessageData = CreateMessageData(mainFormData, prefill.FormInstance, distributionSender, forenkletEndring);
            distributionMessage.NotificationMessage.ArchiveReference = archiveReference;
            distributionMessage.NotificationMessage.ReplyLink = CreateReplyLink();
            distributionMessage.NotificationMessage.NotificationTemplate = "DIBK-nabo-1";
            distributionMessage.NotificationMessage.SenderEmail = "noreply@noreply.no";

            //Add notifications
            var fristForInnspillFormatted = prefill.FormInstance.fristForInnspill?.ToString("dd.MM.yyyy");

            var smsContent = GetSMSNotificationMessage(prefill.FormInstance.beroertPart.organisasjonsnummer,
                                                        prefill.FormInstance.beroertPart.navn,
                                                        prefill.FormInstance.kommune,
                                                        fristForInnspillFormatted,
                                                        archiveReference,
                                                        distributionSender,
                                                        forenkletEndring);

            var emailContent = GetEmailNotificationBody(prefill.FormInstance.beroertPart.organisasjonsnummer,
                                                         prefill.FormInstance.beroertPart.navn,
                                                         prefill.FormInstance.kommune,
                                                         prefill.FormInstance.planNavn,
                                                         fristForInnspillFormatted,
                                                         distributionSender,
                                                         forenkletEndring);

            var notifications = new List<Notification>();
            notifications.Add(new Notification()
            {
                EmailContent = emailContent,
                EmailSubject = forenkletEndring ?
                    $"Varsel - Oppstart av forenklet endring av reguleringsplan" :
                    $"Varsel - Oppstart av reguleringsplanarbeid {mainFormData.planforslag?.plannavn}",
                SmsContent = smsContent,
                Receiver = prefill.FormInstance.beroertPart.epost
            });

            distributionMessage.NotificationMessage.Notifications = notifications;

            return distributionMessage;
        }

        private MessageDataType CreateMessageData(NabovarselPlanType mainFormData, no.kxml.skjema.dibk.nabovarselsvarPlan.SvarPaaNabovarselPlanType prefillData, DistributionSender distributionSender, bool forenkletEndring)
        {
            var body = GetPrefillNotificationBody(distributionSender, prefillData.forslagsstiller, prefillData.beroertPart, prefillData.fristForInnspill, prefillData.kommune, forenkletEndring);
            var summary = string.Empty;
            var title = GetPrefillNotificationTitle(prefillData.planNavn, prefillData.planid, forenkletEndring);
            return new MessageDataType() { MessageBody = body, MessageSummary = summary, MessageTitle = title };
        }

        private ReplyLink CreateReplyLink()
        {
            var replyLink = new ReplyLink()
            {
                //URL settes av adapteret..
                UrlTitle = "Trykk her for å fylle ut svarskjemaet"
            };
            return replyLink;
        }

        public string GetPrefillNotificationBody(DistributionSender avsenderDistribusjon, no.kxml.skjema.dibk.nabovarselsvarPlan.PartType forslagsstiller, no.kxml.skjema.dibk.nabovarselsvarPlan.BeroertPartType beroertPart, DateTime? fristForInnspill, string kommune, bool forenkletEndring)
        {
            string datoFristInnspill = fristForInnspill?.ToString("dd.MM.yyyy");

            string htmlBody = forenkletEndring ?
                _htmlUtils.GetHtmlFromTemplate("FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidLogic.Send.VarselForenkletEndringPrefillNotificationBody.html") :
                _htmlUtils.GetHtmlFromTemplate("FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidLogic.Send.VarselOppstartPlanarbeidPrefillNotificationBody.html");

            htmlBody = htmlBody.Replace("<beroertPart.navn />", beroertPart.navn);
            htmlBody = htmlBody.Replace("<kommune />", kommune);
            htmlBody = htmlBody.Replace("<forslagsstiller.navn />", forslagsstiller.navn);
            htmlBody = htmlBody.Replace("<avsenderDistribusjon.rolle />", avsenderDistribusjon.Role.ToString());
            htmlBody = htmlBody.Replace("<avsenderDistribusjon.navn />", avsenderDistribusjon.Name);
            htmlBody = htmlBody.Replace("<avsenderDistribusjon.epost />", avsenderDistribusjon.Email);
            htmlBody = htmlBody.Replace("<avsenderDistribusjon.telefonnummer />", avsenderDistribusjon.Phone);
            htmlBody = htmlBody.Replace("<datoFristInnspill />", datoFristInnspill);

            return htmlBody;
        }

        public static string GetPrefillNotificationTitle(string planNavn, string planid, bool forenkletEndring)
        {
            return forenkletEndring ?
                    $"Oppstart av forenklet endring av reguleringsplan - {planNavn} ({planid})" :
                    $"Oppstart av planarbeid - {planNavn} ({planid})";
        }

        public static string GetSMSNotificationMessage(string orgnr, string berortPartNavn, string kommunenavn, string fristForInnspill,
            string archiveReference, DistributionSender avsenderDistribusjon, bool forenkletEndring)
        {
            string evtOrgnr = string.Empty;
            if (!string.IsNullOrEmpty(orgnr)) evtOrgnr = $" (org.nr. {orgnr})";

            var notificationBuilder = new StringBuilder();
            notificationBuilder.Append(forenkletEndring ?
                                        $"{berortPartNavn}{evtOrgnr} har fått varsel om forenklet endring av reguleringsplan i {kommunenavn} kommune. " :
                                        $"{berortPartNavn}{evtOrgnr} har fått varsel om oppstart av reguleringsplanarbeid i {kommunenavn} kommune. ");
            notificationBuilder.Append($"Logg inn på Altinn for å se brevet. ");
            notificationBuilder.Append(forenkletEndring ?
                                        $"Du må svare innen {(fristForInnspill)} hvis du vil uttale deg. Altinn-referanse: {archiveReference?.ToUpper()}. " :
                                        $"Dere må svare innen {(fristForInnspill)} hvis dere vil uttale dere om planforslaget. Altinn-referanse: {archiveReference?.ToUpper()}. ");
            notificationBuilder.Append($"Hilsen {avsenderDistribusjon.Role.ToString().ToLower()} {avsenderDistribusjon.Name} ");

            return notificationBuilder.ToString();
        }

        public static string GetEmailNotificationBody(string orgnr, string nabo, string kommunenavn, string plannavn, string fristForInnspill, DistributionSender avsenderDistribusjon, bool forenkletEndring)
        {
            string evtOrgnr = "";
            if (!string.IsNullOrEmpty(orgnr)) evtOrgnr = $"(org.nr. {orgnr})";

            // Melding
            var notificationBuilder = new StringBuilder();

            notificationBuilder.Append($"Til {nabo} {evtOrgnr}");

            notificationBuilder.Append($"<p>");

            notificationBuilder.Append(forenkletEndring ?
                                        $"Vi varsler om oppstart av arbeid med forenklet endring av reguleringsplan i {kommunenavn} kommune. <br>" :
                                        $"Vi varsler om oppstart av arbeid med reguleringsplan i {kommunenavn} kommune. <br>");

            notificationBuilder.Append($"Navnet på reguleringsplanen er: {plannavn}.");
            notificationBuilder.Append($"</p>");

            notificationBuilder.Append(forenkletEndring ?
                                        "Vi sender deg dette brevet fordi du kan være berørt eller ha interesser i nærheten av området vi vil endre reguleringsplanen for." :
                                        "Vi sender deg dette brevet fordi du kan være berørt eller ha interesser i nærheten av området vi vil regulere.");

            notificationBuilder.Append($"<p>");
            notificationBuilder.Append($"<strong>Vil du ikke uttale deg?</strong><br> ");
            notificationBuilder.Append($"Da trenger du ikke å gjøre noe som helst.");
            notificationBuilder.Append($"</p>");

            notificationBuilder.Append($"<p>");
            notificationBuilder.Append(forenkletEndring ?
                                        $"<strong>Du kan uttale deg om endringsforslaget</strong><br>" :
                                        $"<strong>Du kan uttale deg om oppstarten av reguleringsplanforslaget</strong><br>");
            notificationBuilder.Append($"Logg inn på Altinn for å se brevet og svar innen {fristForInnspill}.");
            notificationBuilder.Append($"</p>");

            if (!string.IsNullOrEmpty(orgnr))
            {
                notificationBuilder.Append($"<p>");
                notificationBuilder.Append($"<strong>Om tilgang i Altinn</strong><br>");
                notificationBuilder.Append(forenkletEndring ?
                                            $"For å se varselet om endring, må du representere {nabo} {evtOrgnr} i Altinn. <br>" :
                                            $"For å se varselet om oppstart, må du representere {nabo} {evtOrgnr} i Altinn. <br>");
                notificationBuilder.Append($"Den som skal se og svare på varselet, må ha rollen «Plan- og byggesak» i organisasjonen. Les mer på dibk.no/rettigheter-i-altinn <br>");
                notificationBuilder.Append($"</p>");
            }

            notificationBuilder.Append($"<p>");
            notificationBuilder.Append($"Med vennlig hilsen,<br>");
            notificationBuilder.Append($"{avsenderDistribusjon.Role} {avsenderDistribusjon.Name}<br>");

            notificationBuilder.Append($"Telefon: {avsenderDistribusjon.Phone} <br>");

            notificationBuilder.Append($"E-post: {avsenderDistribusjon.Email}");

            notificationBuilder.Append($"</p>");

            notificationBuilder.Append($"<p><em>Det er ikke mulig å svare på denne e-posten. Spørsmål om innholdet i varselet, må du rette til {avsenderDistribusjon.Name}.</em></p>");

            return notificationBuilder.ToString();
        }
    }
}