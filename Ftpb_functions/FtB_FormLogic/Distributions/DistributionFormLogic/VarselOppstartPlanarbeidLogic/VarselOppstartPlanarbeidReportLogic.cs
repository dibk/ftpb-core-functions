using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using Altinn3.Adapters;
using Dibk.Ftpb.ModelToPdf.Common.Client;
using Dibk.Ftpb.ModelToPdf.Common.Models;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_DataModels.Mappers;
using FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidLogic;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using no.kxml.skjema.dibk.nabovarselPlan;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FtB_FormLogic
{
    [FormDataFormat(DataFormatId = "6325", DataFormatVersion = "44824", ProcessingContext = FormLogicProcessingContext.Report)]
    public class VarselOppstartPlanarbeidReportLogic : DistributionReportLogic<NabovarselPlanType, BeroertPartType>
    {
        private readonly IHtmlUtils _htmlUtils;
        private readonly IDecryption _decryptor;

        protected override bool CompleteAltinn3Instance => false;

        protected override StatusApiFileInfo MainFormDownloadFileInfo => new()
        {
            FileName = "varselbrev.pdf",
            Name = "Varsel om oppstart av planarbeid",
            FileType = FileTypesForDownloadEnum.VarselTilBeroerteParter,
            MimeType = "application/pdf"
        };

        protected override StatusApiFileInfo ReceiptDownloadFileInfo => new()
        {
            FileName = "varsel_oppstart_planarbeid_kvittering.pdf",
            Name = "Kvittering for varsel om oppstart av planarbeid",
            FileType = FileTypesForDownloadEnum.VarselTilBeroerteParterKvittering,
            MimeType = "application/pdf"
        };

        public VarselOppstartPlanarbeidReportLogic(
            IHtmlUtils htmlUtils,
            IDecryptionFactory decryptionFactory,
            INotificationAdapter notificationAdapter,
            IDatamodelToPdfHttpClient datamodelToPdfHttpClient,
            IBlobOperations blobOperations,
            IAltinnInstanceCompleter altinnInstanceCompleter,
            DistributionSubmittalRepo distributionSubmittalRepo,
            IFormDataRepo repo,
            ILogger<VarselOppstartPlanarbeidReportLogic> log,
            IDbUnitOfWork dbUnitOfWork,
            IFileDownloadStatusHttpClient fileDownloadHttpClient,
            DistributionReceiverRepo distributionReceiverRepo,
            DistributionReceiverLogRepo distributionReceiverLogRepo
        ) : base(
            notificationAdapter,
            datamodelToPdfHttpClient,
            blobOperations,
            altinnInstanceCompleter,
            distributionSubmittalRepo,
            repo,
            log,
            dbUnitOfWork,
            fileDownloadHttpClient,
            distributionReceiverRepo,
            distributionReceiverLogRepo
        )
        {
            _htmlUtils = htmlUtils;
            _decryptor = decryptionFactory.GetDecryptor();
        }

        protected override void SetBeroertPart(NabovarselPlanType formData, BeroertPartType beroertPart)
        {
            formData.beroerteParter = new[] { beroertPart };
        }

        protected override async Task<AltinnReceiver> GetReceiver()
        {
            var reportee = await BlobOperations.GetReporteeIdFromStoredBlobAsync(this.ArchiveReference);

            AltinnReceiver receiver;
            if (!string.IsNullOrEmpty(reportee))
            {
                var decryptedReportee = _decryptor.DecryptText(reportee);
                var receiverCandidates = VorpaSenderHelper.GetDistributionSenderCandidates(_decryptor, FormData);
                receiver = receiverCandidates.FirstOrDefault(r => r.Id.Equals(decryptedReportee));
            }
            else // Making sure it is backwards compatible with VORPAs not having the reportee stored in metadata
            {
                receiver = new AltinnReceiver()
                {
                    Id = FormData.forslagsstiller.organisasjonsnummer,
                    Type = AltinnReceiverType.Foretak
                };
            }
            return receiver;
        }

        protected override MessageDataType GetSubmitterReceiptMessage(string archiveReference)
        {
            try
            {
                var forenkletEndring = FormData.planforslag?.plantype?.kodeverdi == "36";
                string adresse = base.FormData.eiendomByggested.First().adresse.adresselinje1;
                string planNavn = base.FormData.planforslag.plannavn == null ? "" : base.FormData.planforslag.plannavn;
                string byggested = adresse != null && adresse.Trim().Length > 0 ? $"{adresse}, {planNavn}" : $"{planNavn}";
                string htmlBody = _htmlUtils.GetHtmlFromTemplate("FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidLogic.Report.VarselOppstartPlanarbeidReceiptMessageBody.html");

                htmlBody = htmlBody.Replace("<byggested/>", byggested);
                htmlBody = htmlBody.Replace("<arkivReferanse/>", archiveReference.ToUpper());

                var mess = new MessageDataType()
                {
                    MessageTitle = forenkletEndring ?
                        $"Kvittering - varsel om forenklet endring av reguleringsplan, {byggested}" :
                        $"Kvittering - varsel om oppstart av reguleringsplanarbeid, {byggested}",

                    MessageSummary = "Trykk på vedleggene under for å laste ned varselet og kvittering med liste over hvilke berørte parter som har blitt varslet",
                    MessageBody = htmlBody
                };

                return mess;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Error occurred while getting submitter receipt message");

                throw;
            }
        }

        protected override bool? SubmitterIsPlankonsulent(string encryptedReporteeId)
        {
            return null;
        }

        protected override PlanModelToPdfKvittering GetModelToPdfReceiptInfo()
        {
            var forenkletEndring = FormData.planforslag?.plantype?.kodeverdi == "36";

            return new()
            {
                Tittel = forenkletEndring ?
                    "Kvittering for varsel om forenklet endring av reguleringsplan" :
                    "Kvittering for varsel om oppstart av reguleringsplanarbeid",
                Plannavn = FormData.planforslag.plannavn ?? "",
                Forslagstillernavn = FormData.forslagsstiller.navn,
                Plankonsulentnavn = FormData.planKonsulent?.navn ?? ""
            };
        }

        protected override async Task<(List<Interessent> notifiedParties, List<Interessent> notNotifiedParties)> GetInterestedPartiesAsync(string archiveReference)
        {
            var successfullyNotified = await GetReceiverSuccessfullyNotifiedAsync(archiveReference);

            var groupsWithBeroerteParterWithDecryptedIDs = GetGroupsOfBeroerteParterWithDecryptedIDsParallel(FormData.beroerteParter, successfullyNotified, (ssn) => _decryptor.DecryptText(ssn));
            var listOfBeroertPartWithEiendom = NabovarselPlanMappers.GetListOfBeroertPartWithEiendomFromGroups(groupsWithBeroerteParterWithDecryptedIDs);

            var listOfNotReachableReceiverNames = await GetNotReachableReceiversAsync();

            var notReachableBeroerteParter = listOfBeroertPartWithEiendom.Where(b =>
                listOfNotReachableReceiverNames.Contains(b.navn, StringComparer.InvariantCultureIgnoreCase)).ToList();

            var notNotifiedParties = new List<Interessent>();
            foreach (var receiverName in listOfNotReachableReceiverNames)
            {
                notNotifiedParties.Add(new Interessent()
                {
                    Navn = receiverName
                });
            }

            var notifiedParties = new List<Interessent>();
            foreach (var receiver in listOfBeroertPartWithEiendom.Except(notReachableBeroerteParter))
            {
                var interestedParty = new Interessent()
                {
                    Navn = receiver.navn,
                    PartstypeKodebeskrivelse = receiver.partstype.kodebeskrivelse,
                };

                interestedParty.BerørteEiendommer = new List<Eiendom>();

                if (receiver.gjelderEiendom != null)
                    foreach (var eiendom in receiver.gjelderEiendom)
                    {
                        interestedParty.BerørteEiendommer.Add(new Eiendom()
                        {
                            Adresse = new Adresse()
                            {
                                Adresselinje1 = eiendom.adresse?.adresselinje1,
                                Adresselinje2 = eiendom.adresse?.adresselinje2,
                                Adresselinje3 = eiendom.adresse?.adresselinje3,
                                Postnummer = eiendom.adresse?.postnr,
                                Poststed = eiendom.adresse?.poststed,
                            },
                            Eiendomsidentifikasjon = new Eiendomsidentifikasjon()
                            {
                                Gaardsnummer = eiendom.eiendomsidentifikasjon.gaardsnummer,
                                Bruksnummer = eiendom.eiendomsidentifikasjon.bruksnummer,
                                Seksjonsnummer = eiendom.eiendomsidentifikasjon.seksjonsnummer,
                                Festenummer = eiendom.eiendomsidentifikasjon.festenummer
                            }
                        });
                    }

                notifiedParties.Add(interestedParty);
            }

            return (notifiedParties, notNotifiedParties);
        }

        public static List<IGrouping<string, no.kxml.skjema.dibk.nabovarselPlan.BeroertPartType>> GetGroupsOfBeroerteParterWithDecryptedIDsParallel(no.kxml.skjema.dibk.nabovarselPlan.BeroertPartType[] beroerteParter, IEnumerable<DistributionReceiverDto> successfullyNotified, Func<string, string> decryptorFunc)
        {
            var beroertPartListeWithDecryptedIDsFodselsnummer = new ConcurrentBag<no.kxml.skjema.dibk.nabovarselPlan.BeroertPartType>();
            var beroertPartListeWithDecryptedIDsOrganisasjonsnummer = new ConcurrentBag<no.kxml.skjema.dibk.nabovarselPlan.BeroertPartType>();

            Parallel.ForEach(beroerteParter, (beroertPart) =>
            {
                if (!string.IsNullOrEmpty(beroertPart.organisasjonsnummer))
                {
                    beroertPartListeWithDecryptedIDsOrganisasjonsnummer.Add(beroertPart);
                }

                if (!string.IsNullOrEmpty(beroertPart.foedselsnummer))
                {
                    var newBeroertPartWithDecryptedId = CreateDeepCopy(beroertPart);
                    newBeroertPartWithDecryptedId.foedselsnummer = decryptorFunc(beroertPart.foedselsnummer);
                    beroertPartListeWithDecryptedIDsFodselsnummer.Add(newBeroertPartWithDecryptedId);
                }
            });

            var groupedByFoedselsnummer = beroertPartListeWithDecryptedIDsFodselsnummer.GroupBy(b => b.foedselsnummer).ToList();
            var completedGroupsWithDecryptedIds = beroertPartListeWithDecryptedIDsOrganisasjonsnummer.GroupBy(b => b.organisasjonsnummer).ToList();

            completedGroupsWithDecryptedIds.AddRange(groupedByFoedselsnummer);

            var listOfDecryptedSucsessfullyNotifiedIds = new ConcurrentBag<string>();
            Parallel.ForEach(successfullyNotified, (notified) =>
            {
                listOfDecryptedSucsessfullyNotifiedIds.Add(decryptorFunc(notified.ReceiverId));
            });

            return completedGroupsWithDecryptedIds.Where(p => listOfDecryptedSucsessfullyNotifiedIds.Contains(p.Key)).ToList();
        }

        public static List<IGrouping<string, no.kxml.skjema.dibk.nabovarselPlan.BeroertPartType>> GetGroupsOfBeroerteParterWithDecryptedIDs(no.kxml.skjema.dibk.nabovarselPlan.BeroertPartType[] beroerteParter, IEnumerable<DistributionReceiverDto> successfullyNotified, Func<string, string> decryptorFunc)
        {
            var beroertPartListeWithDecryptedIDsFodselsnummer = new List<no.kxml.skjema.dibk.nabovarselPlan.BeroertPartType>();
            var beroertPartListeWithDecryptedIDsOrganisasjonsnummer = new List<no.kxml.skjema.dibk.nabovarselPlan.BeroertPartType>();

            foreach (var beroertPart in beroerteParter)
            {
                if (!string.IsNullOrEmpty(beroertPart.organisasjonsnummer))
                {
                    beroertPartListeWithDecryptedIDsOrganisasjonsnummer.Add(beroertPart);
                }

                if (!string.IsNullOrEmpty(beroertPart.foedselsnummer))
                {
                    var newBeroertPartWithDecryptedId = new no.kxml.skjema.dibk.nabovarselPlan.BeroertPartType();
                    newBeroertPartWithDecryptedId.foedselsnummer = decryptorFunc(beroertPart.foedselsnummer);
                    beroertPartListeWithDecryptedIDsFodselsnummer.Add(newBeroertPartWithDecryptedId);
                }
            }

            var groupedByFoedselsnummer = beroertPartListeWithDecryptedIDsFodselsnummer.GroupBy(b => b.foedselsnummer).ToList();
            var completedGroupsWithDecryptedIds = beroertPartListeWithDecryptedIDsOrganisasjonsnummer.GroupBy(b => b.organisasjonsnummer).ToList();

            completedGroupsWithDecryptedIds.AddRange(groupedByFoedselsnummer);

            var listOfDecryptedSucsessfullyNotifiedIds = new List<string>();
            foreach (var notified in successfullyNotified)
            {
                listOfDecryptedSucsessfullyNotifiedIds.Add(decryptorFunc(notified.ReceiverId));
            }

            var noe = completedGroupsWithDecryptedIds.Select(g => g.Key).Intersect(listOfDecryptedSucsessfullyNotifiedIds);
            completedGroupsWithDecryptedIds.Where(p => listOfDecryptedSucsessfullyNotifiedIds.Contains(p.Key));

            return completedGroupsWithDecryptedIds;
        }

        protected override IEnumerable<(string Id, string navn)> GetSocialSecurityNumbers()
        {
            return FormData.beroerteParter
                    .Where(x => x.foedselsnummer != null)
                    .Select(x => (x.foedselsnummer, x.navn));
        }

        protected override IEnumerable<(string Id, string navn)> GetOrganisationNumbers()
        {
            return FormData.beroerteParter
                    .Where(x => x.organisasjonsnummer != null)
                    .Select(x => (x.organisasjonsnummer, x.navn));
        }

        public static T CreateDeepCopy<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                XmlSerializer serializer = new XmlSerializer(obj.GetType());
                serializer.Serialize(ms, obj);
                ms.Seek(0, SeekOrigin.Begin);
                return (T)serializer.Deserialize(ms);
            }
        }
    }
}