using Altinn.Common;
using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Constants;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.Exceptions;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using Ftb_DbModels;
using FtB_Print.Services.Print;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    public abstract class DistributionSendLogic<T, TBeroertPart> : SendLogic<T>
    {
        private readonly IBlobOperations _blobOperations;
        private readonly IDistributionAdapter _distributionAdapter;
        private readonly IDistributionDataMapper<T> _distributionDataMapper;
        private readonly IFormMapper<T> _prefillMapper;
        private readonly IPrintService _printService;

        private IEnumerable<IPrefillData> _prefillSendData;

        protected abstract List<string> AttachmentsNotToIncludeForOrganizationsAndPrivatePersons { get; }
        protected abstract List<string> AttachmentOrder { get; }

        public IDistributionMessage DistributionMessage { get; set; }

        private string _publicBlobContainerName = null;
        protected string PublicBlobContainerName => GetPublicBlobContainerName();

        public DistributionSendLogic(IFormDataRepo repo,
                                     IBlobOperations blobOperations,
                                     ILogger log,
                                     IDistributionAdapter distributionAdapter,
                                     IDistributionDataMapper<T> distributionDataMapper,
                                     IFormMapper<T> prefillMapper,
                                     IDbUnitOfWork dbUnitOfWork,
                                     IDecryptionFactory decryptionFactory,
                                     IPrintService printService,
                                     IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                     DistributionReceiverRepo distributionReceiverRepo,
                                     DistributionReceiverLogRepo distributionReceiverLogRepo)
            : base(repo,
                  log,
                  dbUnitOfWork,
                  decryptionFactory,
                  fileDownloadHttpClient,
                  distributionReceiverRepo,
                  distributionReceiverLogRepo)
        {
            _blobOperations = blobOperations;
            _distributionAdapter = distributionAdapter;
            _distributionDataMapper = distributionDataMapper;
            _prefillMapper = prefillMapper;
            _printService = printService;
        }

        public override async Task LoadDataAsync(string archiveReference)
        {
            ArchiveReference = archiveReference;
            var distributionContainerName = $"{archiveReference}-distribution";
            var formDataWithNoReceiversJson = await _blobOperations.GetBlobContentAsStringByBlobItemNameAsync(BlobStorageEnum.Private, distributionContainerName, $"{BlobStorageMetadataTypes.FormDataWithNoReceivers}.json");
            var receiverDataJson = await _blobOperations.GetBlobContentAsStringByBlobItemNameAsync(BlobStorageEnum.Private, distributionContainerName, Receiver.BlobItemName);

            var formData = JsonConvert.DeserializeObject<T>(formDataWithNoReceiversJson);
            var beroertPart = JsonConvert.DeserializeObject<TBeroertPart>(receiverDataJson);

            SetBeroertPart(formData, beroertPart);

            FormData = formData;
        }

        protected abstract void SetBeroertPart(T formData, TBeroertPart beroertPart);

        public override async Task<ReportQueueItem> ExecuteAsync(SendQueueItem sendQueueItem)
        {
            var sw = new Stopwatch();

            if (Receiver == null || string.IsNullOrEmpty(Receiver.Id))
            {
                DbUnitOfWork.LogEntries.AddError("Fant ikke personnummer/organisasjonsnummer");
                DbUnitOfWork.LogEntries.AddErrorInternal($"ArchiveReference {sendQueueItem.ArchiveReference} - Fant ikke personnummer/organisasjonsnummer i Receiver", "AltinnPrefill");
                throw new Exception($"ArchiveReference {sendQueueItem.ArchiveReference} - Fant ikke personnummer/organisasjonsnummer i Receiver");
            }

            sw.Start();
            await MapFormDataToLogicData(sendQueueItem);
            sw.Stop();
            Logger.LogDebug("|ExecuteAsync|: Elapsed time for mapping form data to logic data: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            var prefillData = _prefillSendData.FirstOrDefault();

            sw.Restart();
            var distributionReceiver = await DistributionReceiverRepo.GetAsync(sendQueueItem.ArchiveReference, sendQueueItem.ReceiverSequenceNumber);
            sw.Stop();
            Logger.LogDebug("|ExecuteAsync|: Elapsed time for getting distribution receiver: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            if (distributionReceiver.ProcessStage == null || distributionReceiver.ProcessStage != DistributionReceiverProcessStageEnum.PrefillSent)
            {
                sw.Restart();
                await CreateDistributionForm(prefillData);
                sw.Stop();
                Logger.LogDebug("|ExecuteAsync|: Elapsed time for creating distribution form: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);
            }

            sw.Restart();
            if (!DistributionReceiverEntityHasStatusSentAsync(distributionReceiver))
            {
                sw.Stop();
                Logger.LogDebug("|ExecuteAsync|: Elapsed time for determining distribution receiver entity status: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

                sw.Restart();
                await SendDistribution(sendQueueItem, prefillData, distributionReceiver);
                sw.Stop();
                Logger.LogDebug("|ExecuteAsync|: Elapsed time for sending distribution: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);
            }
            else
            {
                sw.Stop();
                Logger.LogDebug("|ExecuteAsync|: Elapsed time for determining distribution receiver entity status: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

                sw.Restart();
                //Update mother distributionForm with the same receEntity.SubmitPrefillTaskReceiptId. TaskReceiptId will be synched to children
                var mainDistributionForm = await DbUnitOfWork.DistributionForms.Get(DistributionMessage.DistributionFormReferenceId);
                mainDistributionForm.SubmitAndInstantiatePrefilledFormTaskReceiptId = distributionReceiver.SubmitPrefillTaskReceiptId;
                sw.Stop();
                Logger.LogDebug("|ExecuteAsync|: Elapsed time handling status not sent: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

                if (!await DbUnitOfWork.SaveDistributionForms())
                {
                    throw new Exception($"Failed during update of DistributionForms for archiveReference {sendQueueItem.ArchiveReference}");
                }
            }

            return await base.ExecuteAsync(sendQueueItem);
        }

        private static bool DistributionReceiverEntityHasStatusSentAsync(DistributionReceiverDto receiverEntity)
        {
            return receiverEntity.ProcessOutcome != null && receiverEntity.ProcessOutcome == ReceiverProcessOutcomeEnum.Sent;
        }

        private async Task SendDistribution(SendQueueItem sendQueueItem, IPrefillData prefillData, DistributionReceiverDto distributionReceiver)
        {
            var sw = new Stopwatch();

            IAltinnPrefilledDistributionResult result;

            // Cheking if distributionReceiver.PrefillReferenceId exists (means that prefill previously has been sent to Altinn)
            string prefillReferenceId = distributionReceiver.PrefillReferenceId;
            string svarUtForsendelseId = string.Empty;

            if (string.IsNullOrEmpty(prefillReferenceId))
            {
                //First time distribution is attempted to be sent to receiver
                Logger.LogInformation("Sending distribution form with reference {0} for {1} - {2}", DistributionMessage.DistributionFormReferenceId, ArchiveReference, prefillData.ExternalSystemReference);
                sw.Start();
                result = await _distributionAdapter.SendPrefillAndCorrespondenceAsync(DistributionMessage);
                sw.Stop();
                Logger.LogDebug("|SendDistribution|: Elapsed time for |SendPrefillAndCorrespondenceAsync|: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

                if (result.IsSuccessfull())
                {
                    distributionReceiver.SubmitPrefillTaskReceiptId = result.PrefillResult.PrefillAltinnReceiptId;
                    distributionReceiver.PrefillReferenceId = result.PrefillResult.PrefillReferenceId;
                    distributionReceiver.ProcessStage = DistributionReceiverProcessStageEnum.PrefillSent;

                    await DistributionReceiverRepo.UpdateAsync(distributionReceiver);

                    //TODO: Remember "postdistribution metadata SendPrefillServiceV2 line 152 - 162

                    //await LogAndPersistPrefillProcessing(result, prefillData);

                    //if (!await _dbUnitOfWork.SaveDistributionForms())
                    //{
                    //    throw new Exception($"Failed during update of DistributionForms for archiveReference {sendQueueItem.ArchiveReference}");
                    //}
                }
                else if (result.FinalState == AltinnCommunicationResultType.ReservedReportee || result.FinalState == AltinnCommunicationResultType.UnableToReachReceiver)
                {
                    try
                    {
                        sw.Restart();
                        svarUtForsendelseId = await HandleUnavailableReceiver(distributionReceiver);
                        sw.Stop();
                        Logger.LogDebug("|SendDistribution|: Elapsed time for |HandleUnavailableReceiver|: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

                        if (svarUtForsendelseId != null)
                        {
                            result.CorrespondenceResult = new AltinnCorrespondenceResult() { AltinnCommunicationResult = AltinnCommunicationResultType.Sent, CorrespondenceAltinnReceiptId = svarUtForsendelseId };
                            distributionReceiver.ProcessOutcome = ReceiverProcessOutcomeEnum.Sent;
                        }
                        else
                        {
                            result.CorrespondenceResult = new AltinnCorrespondenceResult() { AltinnCommunicationResult = AltinnCommunicationResultType.UnableToReachReceiver, Message = SvarUtMessage.Error };
                        }
                    }
                    catch
                    {
                        result.CorrespondenceResult = new AltinnCorrespondenceResult() { AltinnCommunicationResult = AltinnCommunicationResultType.UnableToReachReceiver, Message = SvarUtMessage.Error };
                    }
                }
                else if (result.FinalState == AltinnCommunicationResultType.SvarUtFailed) //Super-midlertidig løsning for å håndtere når mottaker ikke eksisterer i enhetsregisteret...
                {
                    result.CorrespondenceResult.AltinnCommunicationResult = AltinnCommunicationResultType.UnableToReachReceiver;
                }

                distributionReceiver.DistributionFormReferenceId = DistributionMessage.DistributionFormReferenceId.ToString();
                await DistributionReceiverRepo.UpdateAsync(distributionReceiver);
            }
            else
            {
                Logger.LogWarning("Prefill previously sent to receiver. Sending distribution correspondence only for prefillReferenceId {0} and reference {1} for {2} - {3}", prefillReferenceId, DistributionMessage.DistributionFormReferenceId, ArchiveReference, prefillData.ExternalSystemReference);
                sw.Restart();
                result = await _distributionAdapter.SendCorrespondenceAsync(DistributionMessage, prefillReferenceId);
                sw.Stop();
                Logger.LogDebug("|SendDistribution|: Elapsed time for |(Re)SendCorrespondenceAsync|: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);
            }

            sw.Restart();
            await SetDistributionStatus(result, prefillData, sendQueueItem, svarUtForsendelseId, distributionReceiver);
            sw.Stop();
            Logger.LogDebug("|SendDistribution|: Elapsed time for |SetDistributionStatus|: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            if (result.FinalState == AltinnCommunicationResultType.Failed || result.FinalState == AltinnCommunicationResultType.UnkownErrorOccurred)
                throw new DistributionSendExeception(prefillData.ExternalSystemReference, DistributionMessage.DistributionFormReferenceId, $"CorrespondenceSendingFailed - {result.ResultMessage}");

            await UpdateReceiverProcessStageAsync(distributionReceiver, sendQueueItem.ReceiverSequenceNumber, DistributionReceiverProcessStageEnum.Distributed);
        }

        private async Task<string> HandleUnavailableReceiver(DistributionReceiverDto distributionReceiver)
        {
            return await _printService.SendToPrintAsync(distributionReceiver.PartitionKey, distributionReceiver.ReceiverId, DistributionMessage.DistributionFormReferenceId);
        }

        private async Task UpdateReceiverProcessOutcomeAsync(DistributionReceiverDto receiverEntity, string receiverSequenceNumber, ReceiverProcessOutcomeEnum processOutcomeEnum)
        {
            try
            {
                receiverEntity.ProcessOutcome = processOutcomeEnum;
                await DistributionReceiverRepo.UpdateAsync(receiverEntity);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "UpdateReceiverProcessOutcome: Error adding receiver record for ID={ReceiverSequenceNumber} and receiverID {ReceiverID}.", receiverSequenceNumber, receiverEntity.ReceiverId);
                throw;
            }
        }

        private async Task SetDistributionStatus(IAltinnPrefilledDistributionResult distributionResult,
                                                 IPrefillData prefillData,
                                                 SendQueueItem sendQueueItem,
                                                 string svarUtForsendelseId,
                                                 DistributionReceiverDto receiverEntity)
        {
            var sw = new Stopwatch();

            var finalState = distributionResult.FinalState;
            var processStatuses = GetProcessingStatuses(finalState);

            sw.Start();
            await UpdateReceiverProcessOutcomeAsync(receiverEntity, sendQueueItem.ReceiverSequenceNumber, processStatuses.Item1);
            Logger.LogDebug("|SetDistributionStatus|: Elapsed time for |UpdateReceiverProcessOutcomeAsync|: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            sw.Restart();
            await AddToReceiverProcessLogAsync(sendQueueItem.ReceiverLogPartitionKey, sendQueueItem.Receiver.Id, sendQueueItem.Receiver.Name, processStatuses.Item2);
            Logger.LogDebug("|SetDistributionStatus|: Elapsed time for |AddToReceiverProcessLogAsync|: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            sw.Restart();
            var distributionForm = await DbUnitOfWork.DistributionForms.Get(DistributionMessage.DistributionFormReferenceId);
            Logger.LogDebug("|SetDistributionStatus|: Elapsed time for getting distribution form: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            sw.Restart();
            if (distributionResult.IsSuccessfull() && distributionResult.PrefillResult != null && string.IsNullOrEmpty(svarUtForsendelseId))
            {
                DbUnitOfWork.LogEntries.AddInfo($"Dist id {DistributionMessage.DistributionFormReferenceId} - Distribusjon behandling ferdig");
                distributionForm.DistributionStatus = DistributionStatus.submittedPrefilled;
                distributionForm.ErrorMessage = null;
                distributionForm.SubmitAndInstantiatePrefilledFormTaskReceiptId = distributionResult.PrefillResult.PrefillAltinnReceiptId;
                distributionForm.SubmitAndInstantiatePrefilled = distributionResult.PrefillResult.PrefillAltinnReceivedTime;
            }
            else if (!string.IsNullOrEmpty(svarUtForsendelseId))
            {
                DbUnitOfWork.LogEntries.AddInfo($"Dist id {DistributionMessage.DistributionFormReferenceId} - Distribusjon behandling ferdig");
                distributionForm.DistributionStatus = DistributionStatus.submittedPrefilled;
                distributionForm.ErrorMessage = null;
                distributionForm.Printed = true;
                distributionForm.SubmitAndInstantiatePrefilledFormTaskReceiptId = svarUtForsendelseId;
                distributionForm.SubmitAndInstantiatePrefilled = DateTime.Now;
            }
            else
            {
                if (distributionResult.CorrespondenceResult?.Message != null && distributionResult.CorrespondenceResult.Message.Equals(SvarUtMessage.Error))
                {
                    Logger.LogInformation("Sending distribution form to print with reference {0} for {1} - {2} failed", DistributionMessage.DistributionFormReferenceId, ArchiveReference, prefillData.ExternalSystemReference);
                    DbUnitOfWork.LogEntries.AddInfoInternal($"Dist id {DistributionMessage.DistributionFormReferenceId} - Send manuelt pga. feil ved sending", "Prefill");
                    distributionForm.DistributionStatus = DistributionStatus.error;
                    distributionForm.ErrorMessage = "Send manuelt";
                }
                else if (finalState == AltinnCommunicationResultType.UnableToReachReceiver)
                {
                    Logger.LogWarning("Sending distribution form with reference {0} for {1} - {2} failed due to unable to reach receiver", DistributionMessage.DistributionFormReferenceId, ArchiveReference, prefillData.ExternalSystemReference);
                    DbUnitOfWork.LogEntries.AddError($"Mottaker kunne ikke nås - {Receiver.PresentationId} : {distributionResult.ResultMessage}");

                    distributionForm.DistributionStatus = DistributionStatus.error;
                    distributionForm.ErrorMessage = "Send manuelt";
                }
                else if (finalState == AltinnCommunicationResultType.Failed || finalState == AltinnCommunicationResultType.UnkownErrorOccurred)
                {
                    Logger.LogError("Sending distribution form with reference {0} for {1} - {2} failed", DistributionMessage.DistributionFormReferenceId, ArchiveReference, prefillData.ExternalSystemReference);

                    DbUnitOfWork.LogEntries.AddErrorInternal($"Dist id {DistributionMessage.DistributionFormReferenceId} - Distribusjon/correspondence Altinn feilet for {Receiver.PresentationId}: {distributionResult.ResultMessage}", "Correspondence");
                    DbUnitOfWork.LogEntries.AddError($"Dist id {DistributionMessage.DistributionFormReferenceId} - Feil ved utsendelse av melding og skjema til {Receiver.PresentationId}");

                    distributionForm.DistributionStatus = DistributionStatus.error;
                    distributionForm.ErrorMessage = "Feil oppstod";
                }
            }
            sw.Stop();
            Logger.LogDebug("|SetDistributionStatus|: Elapsed time for Adding LogEntries to DbUnitOfWork: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            if (finalState == AltinnCommunicationResultType.Sent || finalState == AltinnCommunicationResultType.ReservedReportee || finalState == AltinnCommunicationResultType.UnableToReachReceiver)
            {
                sw.Restart();
                if (!await DbUnitOfWork.SaveDistributionForms())
                {
                    sw.Stop();
                    Logger.LogDebug("|SetDistributionStatus|: Elapsed time for saving distribution forms: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);
                    throw new Exception(
                        $"Failed during update of DistributionForms for archiveReference {sendQueueItem.ArchiveReference}");
                }
                sw.Stop();
                Logger.LogDebug("|SetDistributionStatus|: Elapsed time for saving distribution forms: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);
            }
        }

        private Tuple<ReceiverProcessOutcomeEnum, DistributionReceiverStatusLogEnum> GetProcessingStatuses(AltinnCommunicationResultType distributionStep)
        {
            switch (distributionStep)
            {
                case AltinnCommunicationResultType.Sent:
                    return new Tuple<ReceiverProcessOutcomeEnum, DistributionReceiverStatusLogEnum>(ReceiverProcessOutcomeEnum.Sent, DistributionReceiverStatusLogEnum.CorrespondenceSent);

                case AltinnCommunicationResultType.Failed:
                case AltinnCommunicationResultType.UnableToReachReceiver:
                case AltinnCommunicationResultType.UnkownErrorOccurred:
                    return new Tuple<ReceiverProcessOutcomeEnum, DistributionReceiverStatusLogEnum>(ReceiverProcessOutcomeEnum.Failed, DistributionReceiverStatusLogEnum.CorrespondenceSendingFailed);

                case AltinnCommunicationResultType.ReservedReportee:
                    return new Tuple<ReceiverProcessOutcomeEnum, DistributionReceiverStatusLogEnum>(ReceiverProcessOutcomeEnum.ReservedReportee, DistributionReceiverStatusLogEnum.ReservedReportee);

                default:
                    return new Tuple<ReceiverProcessOutcomeEnum, DistributionReceiverStatusLogEnum>(ReceiverProcessOutcomeEnum.Sent, DistributionReceiverStatusLogEnum.CorrespondenceSent);
            }
        }

        private async Task CreateDistributionForm(IPrefillData prefillData)
        {
            DbUnitOfWork.DistributionForms.Add(new DistributionForm()
            {
                Id = DistributionMessage.DistributionFormReferenceId,
                InitialExternalSystemReference = prefillData.InitialExternalSystemReference,
                ExternalSystemReference = prefillData.ExternalSystemReference,
                DistributionType = prefillData.PrefillFormName,
            });

            var distributionForm = await DbUnitOfWork.DistributionForms.Get(DistributionMessage.DistributionFormReferenceId);

            //Creates combined distribution data structure
            foreach (var combinedCandidate in _prefillSendData.Where(p => p != prefillData))
            {
                var childDistribution = new DistributionForm()
                {
                    Id = Guid.NewGuid(),
                    InitialExternalSystemReference = prefillData.InitialExternalSystemReference,
                    ExternalSystemReference = combinedCandidate.ExternalSystemReference,
                    DistributionReference = DistributionMessage.DistributionFormReferenceId,
                };
                DbUnitOfWork.DistributionForms.Add(childDistribution);
                DbUnitOfWork.LogEntries.AddInfo($"Distribusjon med søknadsystemsreferanse {prefillData.ExternalSystemReference} kombinert med {childDistribution.ExternalSystemReference} og distribusjonsid {childDistribution.Id}");
            }

            DbUnitOfWork.LogEntries.AddInfo($"Dist id {DistributionMessage.DistributionFormReferenceId} - Distribusjon av {prefillData.PrefillFormName} til tjeneste {prefillData.PrefillServiceCode}/{prefillData.PrefillServiceEditionCode}");
            distributionForm.SubmitAndInstantiatePrefilled = DateTime.Now;
        }

        private async Task MapFormDataToLogicData(SendQueueItem sendQueueItem)
        {
            var sw = new Stopwatch();
            sw.Start();
            var receiverEntity = await DistributionReceiverRepo.GetAsync(sendQueueItem.ArchiveReference, sendQueueItem.ReceiverSequenceNumber);
            sw.Stop();
            Logger.LogDebug("|MapFormDataToLogicData|: Elapsed time for getting distribution receiver: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            //Using existing distributionFormsID if it exists. If not, create a new id and persist to DistributionReceiverEntity
            Guid distributionFormReferenceId;
            if (string.IsNullOrEmpty(receiverEntity.DistributionFormReferenceId))
                distributionFormReferenceId = Guid.NewGuid();
            else
                distributionFormReferenceId = Guid.Parse(receiverEntity.DistributionFormReferenceId);

            sw.Restart();
            MapPrefillData(sendQueueItem.Receiver.Id, sendQueueItem.Receiver.Name, distributionFormReferenceId);
            sw.Stop();
            Logger.LogDebug("|MapFormDataToLogicData|: Elapsed time for mapping prefill data: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            sw.Restart();
            await AddToReceiverProcessLogAsync(sendQueueItem.ReceiverLogPartitionKey, sendQueueItem.Receiver.Id, sendQueueItem.Receiver.Name, DistributionReceiverStatusLogEnum.PrefillDataMapped);
            sw.Stop();
            Logger.LogDebug("|MapFormDataToLogicData|: Elapsed time for adding to receiver process log: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            sw.Restart();
            await MapDistributionMessageAsync(distributionFormReferenceId);
            sw.Stop();
            Logger.LogDebug("|MapFormDataToLogicData|: Elapsed time for mapping distribution message: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            receiverEntity.ProcessStage = DistributionReceiverProcessStageEnum.Distributing;

            sw.Restart();
            await DistributionReceiverRepo.UpdateAsync(receiverEntity);
            sw.Stop();
            Logger.LogDebug("|MapFormDataToLogicData|: Elapsed time for updating distribution receiver entity in table storage: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);
        }

        private void MapPrefillData(string receiverId, string receiverName, Guid distributionFormReferenceId)
        {
            _prefillSendData = _prefillMapper.Map(FormData, receiverId, receiverName, distributionFormReferenceId);
        }

        private async Task MapDistributionMessageAsync(Guid distributionFormReferenceId)
        {
            var sw = new Stopwatch();

            sw.Start();
            DistributionMessage = await CreateDistributionMessageAsync(distributionFormReferenceId);
            sw.Stop();
            Logger.LogDebug("|MapDistributionMessageAsync|: Elapsed time for creating distribution message: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            if (Receiver.IsHoeringsmyndighet.HasValue && Receiver.IsHoeringsmyndighet.Value)
            {
                sw.Restart();
                await AddShipmentForHoeringsmyndighet(DistributionMessage, distributionFormReferenceId);
                sw.Stop();
                Logger.LogDebug("|MapDistributionMessageAsync|: Elapsed time for creating shipment for høringsmyndighet: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

                return;
            }

            sw.Restart();
            var attachments = await GetAttachmentsForPrivatePersonsAndOrganizationsAsync();
            sw.Stop();
            Logger.LogDebug("|MapDistributionMessageAsync|: Elapsed time for getting attachments for private persons and organizations: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            sw.Restart();
            attachments = GetAttachmentsInOrder(attachments);
            sw.Stop();
            Logger.LogDebug("|MapDistributionMessageAsync|: Elapsed time for sorting attachments: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            sw.Restart();
            var attachmentsWithDisplayName = GetAttachmentsWithDisplayName(attachments);
            sw.Stop();
            Logger.LogDebug("|MapDistributionMessageAsync|: Elapsed time for getting attachments with display name: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            var metadataList = new List<KeyValuePair<string, string>>
            {
                new(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.MainForm),
                new(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.SubmittalAttachment),
                new(BlobStorageMetadataKeys.AttachmentTypeName, BlobStorageMetadataTypes.AttachmentCollection)
            };

            sw.Restart();
            var urlsToPublicAttachments = (await _blobOperations.GetBlobUrlsFromPublicStorageByMetadataAsync(PublicBlobContainerName, metadataList)).ToList();
            sw.Stop();
            Logger.LogDebug("|MapDistributionMessageAsync|: Elapsed time for getting blob urls from public storage by metadata: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            var urlListAsHtml = new StringBuilder();

            urlListAsHtml.Append("<br></br>");

            sw.Restart();
            foreach (var attachment in attachments)
            {
                var (fileName, fileUrl, _) = urlsToPublicAttachments.FirstOrDefault(a => a.attachmentFileName.Equals(attachment.Filename));

                if (string.IsNullOrEmpty(fileName) || string.IsNullOrEmpty(fileUrl))
                    continue;

                var attachmentTypeDisplayName = attachmentsWithDisplayName[attachment.Filename];

                AddAttachmentToUrlList(urlListAsHtml, fileUrl, attachmentTypeDisplayName, attachment.Filename);
            }

            var allPublicFilesZipped = urlsToPublicAttachments.Find(a => a.metadataValue == BlobStorageMetadataTypes.AttachmentCollection);

            if (allPublicFilesZipped != default)
                AddDownloadAllAttachmentsButton(urlListAsHtml, allPublicFilesZipped.attachmentFileUrl);
            sw.Stop();
            Logger.LogDebug("|MapDistributionMessageAsync|: Elapsed time for adding attachments to url list: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            sw.Restart();
            DistributionMessage.NotificationMessage.MessageData.MessageBody = DistributionMessage.NotificationMessage.MessageData.MessageBody.Replace("<vedleggsliste />", urlListAsHtml.ToString());
            sw.Stop();
            Logger.LogDebug("|MapDistributionMessageAsync|: Elapsed time for replacing attachment list in message body: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);
        }

        private async Task<IDistributionMessage> CreateDistributionMessageAsync(Guid distributionFormReferenceId)
        {
            var sw = new Stopwatch();
            sw.Start();
            var decryptedReporteeId = await GetDecryptedReporteeIdAsync();
            sw.Stop();
            Logger.LogDebug("|CreateDistributionMessageAsync|: Elapsed time for getting decrypted reportee id: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            sw.Restart();
            var distributionSender = GetDistributionSender(decryptedReporteeId);
            sw.Stop();
            Logger.LogDebug("|CreateDistributionMessageAsync|: Elapsed time for getting distribution sender: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            sw.Restart();
            var distributionMessage = _distributionDataMapper.GetDistributionMessage(_prefillSendData, FormData,
                distributionFormReferenceId, ArchiveReference, distributionSender, Receiver.DigitallyReserved);
            sw.Stop();
            Logger.LogDebug("|CreateDistributionMessageAsync|: Elapsed time for getting distribution message: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            return distributionMessage;
        }

        private static void AddAttachmentToUrlList(
            StringBuilder urlListAsHtml,
            string attachmentFileUri,
            string attachmentTypeDisplayName,
            string attachmentFileName)
        {
            var attachmentIconClass = GetAttachmentIconClass(Path.GetExtension(attachmentFileName));

            urlListAsHtml.Append("<li>");
            urlListAsHtml.Append($"<a class='' href='{attachmentFileUri}' target='_blank' rel='noreferrer noopener'>");
            urlListAsHtml.Append($"<i class='{attachmentIconClass} ai-md ai-nw pr-1 ai-fontSizeS' aria-hidden='true'></i>");
            urlListAsHtml.Append($"<b>{attachmentTypeDisplayName}&nbsp; ({attachmentFileName})</b>");
            urlListAsHtml.Append("</a>");
            urlListAsHtml.Append("</li>");
        }

        private static string GetAttachmentIconClass(string fileExtension)
        {
            return "reg reg-attachment" + fileExtension switch
            {
                ".pdf" => "-pdf",
                ".xml" or ".gml" => "-xml",
                _ => string.Empty
            };
        }

        private static void AddDownloadAllAttachmentsButton(StringBuilder urlListAsHtml, string url)
        {
            urlListAsHtml.Append("<br />");

            urlListAsHtml.Append($"<a id='' class='a-btn-link a-btn-lg-block mr-2' href='{url}' download>");
            urlListAsHtml.Append("<i class='reg reg-download pr-1' aria-hidden='true'></i>");
            urlListAsHtml.Append("<span class='a-btn-icon-text'>");
            urlListAsHtml.Append("Last ned alle vedlegg");
            urlListAsHtml.Append("</span>");
            urlListAsHtml.Append("</a>");
        }

        private async Task<string> GetDecryptedReporteeIdAsync()
        {
            var sw = new Stopwatch();
            sw.Start();
            var encryptedReporteeId = await _blobOperations.GetReporteeIdFromStoredBlobAsync(ArchiveReference);
            sw.Stop();
            Logger.LogDebug("|GetDecryptedReporteeIdAsync|: Elapsed time for getting reportee id from stored blob: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            sw.Restart();
            string decryptedReportee = null;
            if (!string.IsNullOrEmpty(encryptedReporteeId))
            {
                decryptedReportee = DecryptionFactory.GetDecryptor().DecryptText(encryptedReporteeId);
            }
            sw.Stop();
            Logger.LogDebug("|GetDecryptedReporteeIdAsync|: Elapsed time for decrypting reportee id: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

            return decryptedReportee;
        }

        protected abstract DistributionSender GetDistributionSender(string decryptedReporteeId);

        protected abstract Task AddShipmentForHoeringsmyndighet(IDistributionMessage distributionMessage, Guid distributionFormReferenceId);

        private string GetPublicBlobContainerName()
        {
            return _publicBlobContainerName ??= _blobOperations.GetPublicBlobContainerName(ArchiveReference);
        }

        private async Task<List<AltinnPrefillData>> GetAttachmentsForPrivatePersonsAndOrganizationsAsync()
        {
            var prefillData = new List<AltinnPrefillData>();

            foreach (var attachmentTypeName in GetMetadataForAttachments().Where(a =>
                         !AttachmentsNotToIncludeForOrganizationsAndPrivatePersons.Contains(a.Value)))
            {
                var filter = new List<KeyValuePair<string, string>> { attachmentTypeName };
                var blobContents = await _blobOperations.GetBlobContentsAsBytesByMetadataAsync(BlobStorageEnum.Public, PublicBlobContainerName.ToLower(), filter);

                foreach (var blobContent in blobContents)
                {
                    if (blobContent.Content != null)
                    {
                        prefillData.Add(new AltinnPrefillData(attachmentTypeName.Value, null, blobContent.MimeType, blobContent.Content, blobContent.FileName));
                    }
                }
            }

            return prefillData;
        }

        protected abstract Dictionary<string, string> GetAttachmentsWithDisplayName(List<AltinnPrefillData> attachments);

        private List<AltinnPrefillData> GetAttachmentsInOrder(List<AltinnPrefillData> attachments)
        {
            return attachments.OrderBy(a => AttachmentOrder.IndexOf(a.Id)).ToList();
        }

        private List<KeyValuePair<string, string>> GetMetadataForAttachments()
        {
            return AttachmentOrder
                .Select(typeName => new KeyValuePair<string, string>(BlobStorageMetadataKeys.AttachmentTypeName, typeName))
                .ToList();
        }
    }
}