using Altinn.Common;
using Altinn.Common.Exceptions;
using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using Altinn3.Adapters;
using Dibk.Ftpb.ModelToPdf.Common.Client;
using Dibk.Ftpb.ModelToPdf.Common.Models;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Enums;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_Common.Utils;
using FtB_FormLogic.Distributions.DistributionFormLogic;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Ftb_Repositories.LogContstants;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    public abstract class DistributionReportLogic<T, TBeroertPart> : ReportLogic<T>
    {
        private readonly INotificationAdapter _notificationAdapter;
        private readonly IDatamodelToPdfHttpClient _datamodelToPdfHttpClient;

        protected DistributionReportLogic(
            INotificationAdapter notificationAdapter,
            IDatamodelToPdfHttpClient datamodelToPdfHttpClient,
            IBlobOperations blobOperations,
            IAltinnInstanceCompleter altinnInstanceCompleter,
            DistributionSubmittalRepo distributionSubmittalRepo,
            IFormDataRepo repo,
            ILogger log,
            IDbUnitOfWork dbUnitOfWork,
            IFileDownloadStatusHttpClient fileDownloadHttpClient,
            DistributionReceiverRepo distributionReceiverRepo,
            DistributionReceiverLogRepo distributionReceiverLogRepo
        ) : base(
            blobOperations,
            altinnInstanceCompleter,
            distributionSubmittalRepo,
            repo,
            log,
            dbUnitOfWork,
            fileDownloadHttpClient,
            distributionReceiverRepo,
            distributionReceiverLogRepo
        )
        {
            _notificationAdapter = notificationAdapter;
            _datamodelToPdfHttpClient = datamodelToPdfHttpClient;
        }

        public override async Task LoadDataAsync(string archiveReference)
        {
            ArchiveReference = archiveReference;
            var distributionContainerName = $"{archiveReference}-distribution";
            var receiver = ReportQueueItem.Receiver;
            var formDataWithNoReceiversJson = await BlobOperations.GetBlobContentAsStringByBlobItemNameAsync(BlobStorageEnum.Private, distributionContainerName, $"{BlobStorageMetadataTypes.FormDataWithNoReceivers}.json");
            var receiverDataJson = await BlobOperations.GetBlobContentAsStringByBlobItemNameAsync(BlobStorageEnum.Private, distributionContainerName, receiver.BlobItemName);

            var formData = JsonConvert.DeserializeObject<T>(formDataWithNoReceiversJson);
            var beroertPart = JsonConvert.DeserializeObject<TBeroertPart>(receiverDataJson);

            SetBeroertPart(formData, beroertPart);

            FormData = formData;
        }

        protected abstract void SetBeroertPart(T formData, TBeroertPart beroertPart);

        protected abstract Task<AltinnReceiver> GetReceiver();

        protected abstract MessageDataType GetSubmitterReceiptMessage(string archiveReference);

        public override async Task<string> ExecuteAsync(ReportQueueItem reportQueueItem)
        {
            DbUnitOfWork.SetArchiveReference(reportQueueItem.ArchiveReference);

            var returnItem = await base.ExecuteAsync(reportQueueItem);

            if (await SubmitterHasBeenNotifiedAsync())
                return returnItem;

            var receiverEntity = await DistributionReceiverRepo.GetAsync(reportQueueItem.ArchiveReference, reportQueueItem.ReceiverSequenceNumber);

            await UpdateReceiverProcessStageAsync(receiverEntity, reportQueueItem.ReceiverSequenceNumber, DistributionReceiverProcessStageEnum.ReadyForReporting);
            await AddToReceiverProcessLogAsync(reportQueueItem.ReceiverLogPartitionKey, reportQueueItem.Receiver.Id, reportQueueItem.Receiver.Name, DistributionReceiverStatusLogEnum.ReadyForReporting);

            if (await ReadyForSubmittalReportingAsync(reportQueueItem))
            {
                FormData = SerializeUtil.DeserializeXml<T>(await BlobOperations.GetFormdataAsync(reportQueueItem.ArchiveReference));
                await SendReceiptToSubmitterWhenAllReceiversAreProcessedAsync(reportQueueItem);
            }

            return returnItem;
        }

        private async Task<bool> SubmitterHasBeenNotifiedAsync()
        {
            var submittalEntity = await DistributionSubmittalRepo.GetAsync(ArchiveReference, ArchiveReference);

            bool alreadySent = submittalEntity.Status == DistributionSubmittalStatusEnum.Distributed
                || submittalEntity.Status == DistributionSubmittalStatusEnum.ReceiptSentToSubmitter;

            if (alreadySent)
                Logger.LogInformation("Receipt already sent to submitter for archive reference {archiveReference}. DistributionSubmittalEntity.Status is: {SubmittalStatus}", ArchiveReference, submittalEntity.Status);

            return alreadySent;
        }

        public override async Task PostExecuteAsync(ReportQueueItem reportQueueItem)
        {
            var archiveReference = reportQueueItem.ArchiveReference;

            if (await SubmitterHasBeenNotifiedAsync())
            {
                if (CompleteAltinn3Instance)
                {
                    Logger.LogInformation("Completing Altinn3 instance for archive reference {archiveReference}", archiveReference);
                    try
                    {
                        await AltinnInstanceCompleter.PerformCompleteInstanceAsync(archiveReference);
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex, "Error occurred when completing Altinn3 instance for archive reference {archiveReference}", archiveReference);
                        throw;
                    }
                }

                Logger.LogInformation("Deleting distribution container for archive reference {archiveReference}", archiveReference);
                await BlobOperations.DeleteBlobContainerAsync(BlobStorageEnum.Private, $"{archiveReference}-distribution");
            }
        }

        private async Task SendReceiptAsync(ReportQueueItem reportQueueItem)
        {
            DistributionSubmittalDto submittalEntity = await DistributionSubmittalRepo.GetAsync(reportQueueItem.ArchiveReference, reportQueueItem.ArchiveReference);
            var receiptAlreadySentToSubmitter = false;
            if (submittalEntity.Status != null)
                receiptAlreadySentToSubmitter = submittalEntity.Status == DistributionSubmittalStatusEnum.Distributed
                                              || submittalEntity.Status == DistributionSubmittalStatusEnum.ReceiptSentToSubmitter;

            AltinnNotificationMessage notificationMessage = await CreateNotificationMessageAsync(reportQueueItem.ArchiveReference);

            if (!receiptAlreadySentToSubmitter)
            {
                await SendReceiptAsNotificationAsync(notificationMessage, submittalEntity);
            }

            await UploadFilesToStatusApiAsync(reportQueueItem.ArchiveReference, submittalEntity, notificationMessage);

            var allReceivers = await DistributionReceiverRepo.GetAsync(reportQueueItem.ArchiveReference.ToLower());
            var updatedEntities = allReceivers.Select(receiver => { receiver.ProcessStage = DistributionReceiverProcessStageEnum.Reported; return receiver; }).ToList();
            var success = await DistributionReceiverRepo.UpdateAsync(updatedEntities);
            if (!success)
            {
                throw new Exception($"Update av DistributionReceiverEntity til ProcessStage=Reported feilet for {reportQueueItem.ArchiveReference}");
            }

            //Setter submittal status for � indikere at kvittering er sendt til innsender
            submittalEntity.Status = DistributionSubmittalStatusEnum.ReceiptSentToSubmitter;
            await DistributionSubmittalRepo.UpdateAsync(submittalEntity);
        }

        private async Task UploadFilesToStatusApiAsync(
            string archiveReference,
            DistributionSubmittalDto submittalDto,
            AltinnNotificationMessage notificationMessage
        )
        {
            if (submittalDto.ReceiptsContainerName == Guid.Empty)
            {
                submittalDto.ReceiptsContainerName = Guid.NewGuid();
                submittalDto = await DistributionSubmittalRepo.UpdateAsync(submittalDto);
            }

            var containerName = submittalDto.ReceiptsContainerName;

            var downloadFileInfosAndBinaries = GetDownloadFileInfosAndBinaries(notificationMessage);

            await AddFileDownloadStatusesAsync(archiveReference, containerName, downloadFileInfosAndBinaries);
        }

        private IEnumerable<(StatusApiFileInfo downloadFileInfo, AttachmentBinary binaryFile)>
            GetDownloadFileInfosAndBinaries(AltinnNotificationMessage notificationMessage)
        {
            var mainFormBinary = (AttachmentBinary)notificationMessage.Attachments.FirstOrDefault(x => x.Name == MainFormDownloadFileInfo.Name);
            var receiptBinary = (AttachmentBinary)notificationMessage.Attachments.FirstOrDefault(x => x.Name == ReceiptDownloadFileInfo.Name);

            ArgumentNullException.ThrowIfNull(mainFormBinary, $"Main form binary ({MainFormDownloadFileInfo.Name}) is null for archive reference {ArchiveReference}");
            ArgumentNullException.ThrowIfNull(receiptBinary, $"Receipt binary ({ReceiptDownloadFileInfo.Name})  is null for archive reference {ArchiveReference}");


            return
            [
                (MainFormDownloadFileInfo, mainFormBinary),
                (ReceiptDownloadFileInfo, receiptBinary)
            ];
        }

        private async Task SendReceiptToSubmitterWhenAllReceiversAreProcessedAsync(ReportQueueItem reportQueueItem)
        {
            try
            {
                Logger.LogInformation("All receivers have been processed. ReceiverSequenceNumber {ReceiverLogPartitionKey} triggers reporting", reportQueueItem.ReceiverLogPartitionKey);

                await SendReceiptAsync(reportQueueItem);

                DistributionSubmittalDto submittalEntity = await DistributionSubmittalRepo.GetAsync(reportQueueItem.ArchiveReference, reportQueueItem.ArchiveReference);
                await BulkAddLogEntryToReceiversAsync(reportQueueItem.ArchiveReference, DistributionReceiverStatusLogEnum.Completed);

                submittalEntity.Status = DistributionSubmittalStatusEnum.Distributed;
                await DistributionSubmittalRepo.UpdateAsync(submittalEntity);
                await ReportFormProcessStatusAsync(FormMetadataStatus.Ok);
                Logger.LogInformation("Reporting performed successfully SubmittalStatus= {SubmittalStatus}", submittalEntity.Status);
            }
            catch (SendNotificationException ex)
            {
                base.Logger.LogError("Error: {ExceptionMessage}: {DistriutionStep}", ex.Text, ex.DistriutionStep);
                throw ex;
            }
            catch (Exception ex)
            {
                base.Logger.LogError(ex, "Error occurred when creating and sending receipt");
                throw;
            }
            finally
            {
                await BlobOperations.ReleaseContainerLeaseAsync(reportQueueItem.ArchiveReference.ToLower());
            }
        }

        private async Task BulkAddLogEntryToReceiversAsync(string archiveReference, DistributionReceiverStatusLogEnum statusEnum)
        {
            var partititonKey = archiveReference.ToLower();

            var receivers = await DistributionReceiverRepo.GetAsync(partititonKey);

            var tasks = receivers.Select(s => AddToReceiverProcessLogAsync(s.ReceiverLogPartitionKey, s.ReceiverId, s.ReceiverName, statusEnum));

            await Task.WhenAll(tasks);
        }

        protected virtual async Task<AltinnNotificationMessage> CreateNotificationMessageAsync(string archiveReference)
        {
            var notificationMessage = new AltinnNotificationMessage();
            notificationMessage.ArchiveReference = ArchiveReference;
            notificationMessage.Receiver = await GetReceiver();
            notificationMessage.ArchiveReference = archiveReference;
            notificationMessage.RespectReservable = false;

            var messageData = GetSubmitterReceiptMessage(archiveReference);
            notificationMessage.MessageData = messageData;

            Logger.LogDebug("Creates submitter receipt");

            var request = await GetModelToPdfRequestAsync(archiveReference);

            var htmlResult = await _datamodelToPdfHttpClient.CreateHtmlAsync(request, CancellationToken.None);
            var receiptHtmlUrl = await BlobOperations.AddBytesToBlobStorageAsync(BlobStorageEnum.Private, archiveReference, $"plainReceiptHtml-{Guid.NewGuid()}.html", Encoding.UTF8.GetBytes(htmlResult), "text/html");

            Logger.LogDebug($"Added receipt html to blobstorage: {receiptHtmlUrl}");

            var receiptAttachment = new AttachmentBinary()
            {
                Filename = ReceiptDownloadFileInfo.FileName,
                Name = ReceiptDownloadFileInfo.Name,
                ArchiveReference = archiveReference
            };

            await using (var pdfResult = await _datamodelToPdfHttpClient.CreatePdfAsync(request, CancellationToken.None))
            {
                var byteArray = new byte[pdfResult.Length];
                _ = await pdfResult.ReadAsync(byteArray);

                receiptAttachment.BinaryContent = byteArray;
            }

            string publicContainerName = BlobOperations.GetPublicBlobContainerName(archiveReference.ToLower());
            var metadata = new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.MainForm);
            var mainFormFromBlobStorage = await BlobOperations.GetBlobAsBytesByMetadataAsync(BlobStorageEnum.Public, publicContainerName, metadata);

            var mainFormAttachment = new AttachmentBinary()
            {
                BinaryContent = mainFormFromBlobStorage,
                Filename = MainFormDownloadFileInfo.FileName,
                Name = MainFormDownloadFileInfo.Name,
                ArchiveReference = archiveReference
            };

            notificationMessage.Attachments = new List<Attachment> { receiptAttachment, mainFormAttachment };

            return notificationMessage;
        }

        private async Task<ModelToPdfRequest> GetModelToPdfRequestAsync(string archiveReference)
        {
            var encryptedReporteeId = await BlobOperations.GetReporteeIdFromStoredBlobAsync(archiveReference);

            return new ModelToPdfRequest
            {
                ArchiveReference = archiveReference,
                Type = DataModelTypes.ReceiptToDistributor,
                Vedlegg = await GetReceiptInformationAsync(archiveReference),
                Altinn3InstanceOwnerIsPlankonsulent = SubmitterIsPlankonsulent(encryptedReporteeId),
                ContentString = await GetModelToPdfReceiptInfoAsync(archiveReference)
            };
        }

        protected abstract bool? SubmitterIsPlankonsulent(string encryptedReporteeId);

        private async Task SendReceiptAsNotificationAsync(AltinnNotificationMessage notificationMessage, DistributionSubmittalDto submittalEntity)
        {
            Logger.LogInformation("Sends notification to receiverId {ReceiverId}", notificationMessage.Receiver.Id);
            AltinnNotificationCorrespondenceResult result = await _notificationAdapter.SendNotificationAsync(notificationMessage);

            var sendingSucceeded = result.IsSuccessfull();

            if (!sendingSucceeded)
            {
                var failedStep = result.FinalState;

                throw new SendNotificationException("Error: Failed during sending of submittal receipt", failedStep);
            }

            submittalEntity.Status = DistributionSubmittalStatusEnum.ReceiptSentToSubmitter;
            await DistributionSubmittalRepo.UpdateAsync(submittalEntity);
        }

        private async Task<string> GetModelToPdfReceiptInfoAsync(string archiveReference)
        {
            PlanModelToPdfKvittering modelToPdfReceiptInfo = GetModelToPdfReceiptInfo();

            (List<Interessent> notifiedParties, List<Interessent> notNotifiedParties) partyLists = await GetInterestedPartiesAsync(archiveReference);
            modelToPdfReceiptInfo.VarsledeInteressenter = partyLists.notifiedParties;
            modelToPdfReceiptInfo.IkkeVarsledeInteressenter = partyLists.notNotifiedParties;

            return JsonConvert.SerializeObject(modelToPdfReceiptInfo);
        }

        private async Task<List<VedleggsInfo>> GetReceiptInformationAsync(string archiveReference)
        {
            var publicContainerName = BlobOperations.GetPublicBlobContainerName(archiveReference.ToLower());
            var blobStorageTypes = new List<string> { BlobStorageMetadataTypes.MainForm, BlobStorageMetadataTypes.SubmittalAttachment };
            var listOfAttachmentsInSubmittal = await BlobOperations.GetListOfBlobsMetadataValuesByBlobItemTypesAsync(BlobStorageEnum.Public, publicContainerName, blobStorageTypes, true);

            var attachmentOrdered = listOfAttachmentsInSubmittal.OrderBy(a => ReceiptData.AttachmentOrder.IndexOf(a.attachmentType)).ToList();

            var vedlegg = new List<VedleggsInfo>();

            foreach (var (attachmentTypeName, fileName, _) in attachmentOrdered)
            {
                vedlegg.Add(new VedleggsInfo
                {
                    Filename = fileName,
                    DataType = attachmentTypeName,
                    DisplayName = attachmentTypeName,
                });
            }

            return vedlegg;
        }

        protected abstract PlanModelToPdfKvittering GetModelToPdfReceiptInfo();

        protected abstract Task<(List<Interessent> notifiedParties, List<Interessent> notNotifiedParties)> GetInterestedPartiesAsync(string archiveReference);



        protected async Task<IEnumerable<string>> GetNotReachableReceiversAsync()
        {
            var allReceiversInSubmittal = await DistributionReceiverRepo.GetAsync(ArchiveReference);
            var socialSecurityNumbers = GetSocialSecurityNumbers();
            var orgNumbers = GetOrganisationNumbers();
            return DistributionReceiptReportHelper.GetNamesOfNotReachableReceivers(allReceiversInSubmittal, socialSecurityNumbers, orgNumbers);
        }

        protected abstract IEnumerable<(string Id, string navn)> GetSocialSecurityNumbers();

        protected abstract IEnumerable<(string Id, string navn)> GetOrganisationNumbers();
    }
}