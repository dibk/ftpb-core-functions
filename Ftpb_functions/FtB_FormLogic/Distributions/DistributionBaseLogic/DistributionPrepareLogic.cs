using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.Extensions;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_Common.Utils;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using KontaktOgReservasjonsregisteret;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    public abstract class DistributionPrepareLogic<T, TBeroertPart> : PrepareLogic<T>
    {
        private T _formDataWithoutBeroerteParter;

        private readonly KRRClient _krrClient;

        protected Dictionary<ReceiverDistinctKeyItem, BeroertPartBlobData> ReceiversDistinct { get; set; }

        protected abstract string DataFormatId { get; }
        protected readonly DistributionSubmittalRepo DistributionSubmittalRepo;

        protected DistributionPrepareLogic(IFormDataRepo repo,
                                           ILogger log,
                                           IDbUnitOfWork dbUnitOfWork,
                                           IDecryptionFactory decryptionFactory,
                                           IBlobOperations blobOperations,
                                           IFileDownloadStatusHttpClient fileDownloadHttpClient,
                                           KRRClient krrClient,
                                           DistributionReceiverRepo distributionReceiverRepo,
                                           DistributionReceiverLogRepo distributionReceiverLogRepo,
                                           DistributionSubmittalRepo distributionSubmittalRepo)
            : base(repo,
                   log,
                   dbUnitOfWork,
                   decryptionFactory,
                   blobOperations,
                   fileDownloadHttpClient,
                   distributionReceiverRepo,
                   distributionReceiverLogRepo)
        {
            _krrClient = krrClient;
            DistributionSubmittalRepo = distributionSubmittalRepo;
        }

        public override async Task PreExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {
            await base.PreExecuteAsync(submittalQueueItem);

            var distributionContainerName = $"{submittalQueueItem.ArchiveReference}-distribution";

            var stopwatch = new Stopwatch();

            await UploadFormDataWithZeroBeroerteParterAsync(submittalQueueItem.ArchiveReference, FileFormatEnum.Xml);

            stopwatch.Start();
            Logger.LogDebug("Starting process of uploading distinct receivers");
            await UploadBeroertPartFilesWithDistinctReceiversAsync(distributionContainerName);
            stopwatch.Stop();
            Logger.LogDebug("Elapsed time for uploading distinct receivers: {elapsedMilliseconds} ms", stopwatch.ElapsedMilliseconds);

            stopwatch.Restart();
            Logger.LogDebug("Starting process of uploading form data without receivers");
            await UploadFormDataWithZeroBeroerteParterAsync(distributionContainerName, FileFormatEnum.Json);
            stopwatch.Stop();
            Logger.LogDebug("Elapsed time for uploading form data without receivers: {elapsedMilliseconds} ms", stopwatch.ElapsedMilliseconds);
        }

        public override async Task<IEnumerable<SendQueueItem>> ExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {
            await base.ExecuteAsync(submittalQueueItem);

            await CreateDistributionSubmittalDatabaseStatus(submittalQueueItem.ArchiveReference, Sender.Id,
                Receivers.Count);
            var sendQueueItems = await CreateDistributionSendQueueItems();

            return sendQueueItems;
        }

        private async Task UploadBeroertPartFilesWithDistinctReceiversAsync(string containerName)
        {
            Logger.LogDebug("Creating files with distinct receivers for {DataFormatId}", DataFormatId);

            var metadata = new Dictionary<string, string>
            {
                { BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.UniqueReceiver }
            };

            var blobItems = ReceiversDistinct.Values.Select(x => (x.BlobItemName, x.BeroertPart.ToJson(false)));

            await BlobOperations.ParallelUploadStringContentToBlobStorageAsync(
                BlobStorageEnum.Private,
                containerName,
                blobItems,
                metadata,
                "application/json");
        }

        private async Task UploadFormDataWithZeroBeroerteParterAsync(string containerName, FileFormatEnum fileFormat)
        {
            Logger.LogDebug("Creating form data with 0 receivers for {DataFormatId}", DataFormatId);

            var formData = GetFormDataWithoutBeroerteParter();

            var formDataAsString = SerializeFormData(formData, fileFormat);

            await using var stream = new MemoryStream();
            await stream.WriteAsync(Encoding.UTF8.GetBytes(formDataAsString));
            stream.Position = 0;

            var fileName = $"{BlobStorageMetadataTypes.FormDataWithNoReceivers}.{fileFormat.GetDescription()}";

            var metadata = new Dictionary<string, string>
            {
                { BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.FormDataWithNoReceivers },
            };

            await BlobOperations.AddStreamToBlobStorageAsync(BlobStorageEnum.Private, containerName, fileName, stream, "application/json", metadata);
        }

        private static string SerializeFormData(T formData, FileFormatEnum fileFormat)
        {
            return fileFormat switch
            {
                FileFormatEnum.Json => formData.ToJson(false),
                FileFormatEnum.Xml => SerializeUtil.Serialize(formData),
                _ => throw new ArgumentOutOfRangeException(nameof(fileFormat), fileFormat, null)
            };
        }

        private T GetFormDataWithoutBeroerteParter()
        {
            if (_formDataWithoutBeroerteParter != null)
                return _formDataWithoutBeroerteParter;

            var formData = FormData.CloneJson();
            SetBeroerteParterToZero(formData);
            _formDataWithoutBeroerteParter = formData;

            return _formDataWithoutBeroerteParter;
        }

        protected abstract void SetBeroerteParterToZero(T formData);

        private async Task<IEnumerable<SendQueueItem>> CreateDistributionSendQueueItems()
        {
            var sendQueueItems = new List<SendQueueItem>();

            //Bulk add receivers to database
            //FUN FACT: Since the partition key differs for all receivers a true bulk operation cannot be performed..
            var receiverDtos = new List<DistributionReceiverDto>();
            var receiverLogDtos = new List<DistributionReceiverLogDto>();
            for (int i = 0; i < Receivers.Count; i++)
            {
                string receiverLogPartitionKey = $"{ArchiveReference}-{i}";
                string rowKey = $"{DateTime.Now.ToString("yyyyMMddHHmmssffff")}";

                receiverDtos.Add(new DistributionReceiverDto(ArchiveReference,
                                                                 i.ToString(),
                                                                 Receivers[i].Id,
                                                                 Receivers[i].Name,
                                                                 DistributionReceiverProcessStageEnum.Created,
                                                                 DateTime.Now,
                                                                 receiverLogPartitionKey));
                receiverLogDtos.Add(new DistributionReceiverLogDto(receiverLogPartitionKey,
                                                                       rowKey,
                                                                       Receivers[i].Id,
                                                                       Receivers[i].Name,
                                                                       DistributionReceiverStatusLogEnum.Created));

                sendQueueItems.Add(new SendQueueItem()
                {
                    ArchiveReference = ArchiveReference,
                    ReceiverSequenceNumber = i.ToString(),
                    ReceiverLogPartitionKey = receiverLogPartitionKey,
                    Receiver = Receivers[i],
                    Sender = Sender,
                });
            }

            await DistributionReceiverRepo.AddAsync(receiverDtos);
            ParallelInsertEntities(receiverLogDtos);

            Logger.LogInformation("Created {ReceiverEntityCount} receiver entities", receiverDtos.Count);

            return sendQueueItems;
        }

        private void ParallelInsertEntities(IEnumerable<DistributionReceiverLogDto> dtos)
        {
            var list = dtos.ToList();

            //Partitioner creates batches of elements. This makes a more predictable
            //way of performing parallel executions of logic
            var partitioner = Partitioner.Create(0, dtos.Count(), 100);
            var options = new ParallelOptions() { MaxDegreeOfParallelism = 8 };
            Parallel.ForEach(partitioner, options, range =>
            {
                for (int i = range.Item1; i < range.Item2; i++)
                {
                    DistributionReceiverLogRepo.Add(list[i]);
                }
            });
        }

        private async Task CreateDistributionSubmittalDatabaseStatus(string archiveReference, string senderId,
            int receiverCount)
        {
            try
            {
                var entity = CreateDistributionSubmittalDto(archiveReference, senderId, receiverCount);
                await DistributionSubmittalRepo.AddAsync(entity);
                Logger.LogInformation("Created submittal database status with receiver count: {ReceiverCount}.",
                    receiverCount);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"Error creating submittal record for archiveReference={archiveReference}.");
                throw;
            }
        }

        protected async Task SetDigitallyReservedInformation(Dictionary<ReceiverDistinctKeyItem, Receiver> receivers)
        {
            List<string> digitallyReservedIds = new();
            foreach (var item in receivers)
            {
                var decryptedId = item.Key?.decryptedId;
                if (decryptedId.Length > 9)
                {
                    digitallyReservedIds.Add(decryptedId);
                }
            }

            var digitalPersonInformation = await _krrClient.GetDigitalReservationInformationAsync(digitallyReservedIds);

            foreach (var person in digitalPersonInformation)
            {
                var receiverKey = new ReceiverDistinctKeyItem(person.Personidentifikator, false);
                if (receivers.TryGetValue(receiverKey, out _))
                {
                    receivers[receiverKey].DigitallyReserved = !person.CanBeDigitallyContacted();
                }
            }
        }

        protected abstract DistributionSubmittalDto CreateDistributionSubmittalDto(
            string archiveReference,
            string senderId,
            int receiverCount);

        protected class BeroertPartBlobData
        {
            public string BlobItemName { get; set; }
            public TBeroertPart BeroertPart { get; set; }
        }
    }
}