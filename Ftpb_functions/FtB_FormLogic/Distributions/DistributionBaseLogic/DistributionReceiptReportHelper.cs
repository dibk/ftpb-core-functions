﻿using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.Enums;
using System.Collections.Generic;
using System.Linq;

namespace FtB_FormLogic
{
    public static class DistributionReceiptReportHelper
    {
        public static IEnumerable<string> GetNamesOfNotReachableReceivers(IEnumerable<DistributionReceiverDto> receiversInSubmittal, IEnumerable<(string Id, string Navn)> ssns, IEnumerable<(string Id, string Navn)> orgnrs)
        {
            var reservedOrFailedReporteeReceiverIds = receiversInSubmittal
                    .Where(x => x.ProcessOutcome != null && (x.ProcessOutcome == ReceiverProcessOutcomeEnum.ReservedReportee
                                                          || x.ProcessOutcome == ReceiverProcessOutcomeEnum.Failed))
                    .Select(x => x.ReceiverId).ToList();


            var reservedReporteeNames = ssns.Union(orgnrs)
                    .Where(x => reservedOrFailedReporteeReceiverIds.Any(y => y == x.Id))
                    .Select(x => x.Navn).ToList();

            return reservedReporteeNames.OrderBy(x => x);
        }
    }
}
