using FtB_Common.BusinessLogic;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.Extensions;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_Common.Utils;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    public abstract class PrepareLogic<T> : LogicBase<T>, IFormLogic<IEnumerable<SendQueueItem>, SubmittalQueueItem>
    {
        protected readonly IDecryptionFactory DecryptionFactory;
        protected readonly IBlobOperations BlobOperations;

        protected SubmittalQueueItem SubmittalQueueItem { get; private set; }
        protected Actor Sender { get; set; }
        protected virtual List<Receiver> Receivers { get; set; }

        public PrepareLogic(
            IFormDataRepo repo,
            ILogger log,
            IDbUnitOfWork dbUnitOfWork,
            IDecryptionFactory decryptionFactory,
            IBlobOperations blobOperations,
            IFileDownloadStatusHttpClient fileDownloadHttpClient,
            DistributionReceiverRepo distributionReceiverRepo,
            DistributionReceiverLogRepo distributionReceiverLogRepo)
            : base(repo, log, dbUnitOfWork, fileDownloadHttpClient, distributionReceiverRepo, distributionReceiverLogRepo)
        {
            DecryptionFactory = decryptionFactory;
            BlobOperations = blobOperations;
        }

        public abstract Task SetReceiversAsync();

        public abstract Task SetSenderAsync();

        public void SetReceivers(IEnumerable<Receiver> receivers)
        {
            var comparrisonSource = new List<ActorInternal>();
            foreach (var receiver in receivers)
            {
                var receiverInternal = new ActorInternal(receiver);
                receiverInternal.DecryptedId = receiverInternal.Id.Length > 11 ? DecryptionFactory.GetDecryptor().DecryptText(receiverInternal.Id) : receiver.Id;
                receiverInternal.Name = receiver.Name;
                comparrisonSource.Add(receiverInternal);
            }

            var distinctList = comparrisonSource.Distinct(new ReceiverEqualtiyComparer()).ToList();

            Receivers = distinctList.Select(s => new Receiver { Id = s.Id, Type = s.Type, Name = s.Name }).ToList();
            Logger.LogInformation("Receiver count from form data: {ReceiverCount}. Receiver count after removed duplicates: {ReceiverCountDistinct}", receivers.Count(), Receivers.Count);
        }

        /// <summary>
        /// Sets: Properties SubmittalQueueItem and ArchiveReference
        /// Initiates the dbUnitOfWorks archivereference and loads formdata
        /// Executes setting for Sender and Receiver
        /// </summary>
        /// <param name="submittalQueueItem"></param>
        /// <returns></returns>
        public virtual async Task PreExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {
            SubmittalQueueItem = submittalQueueItem;
            base.ArchiveReference = submittalQueueItem.ArchiveReference;

            Logger.LogDebug("Setting unitOfWork archiveReference");
            base.DbUnitOfWork.SetArchiveReference(submittalQueueItem.ArchiveReference);

            Logger.LogDebug("Loading formdata");
            await base.LoadDataAsync(submittalQueueItem.ArchiveReference);

            Logger.LogDebug("Sets sender");
            await SetSenderAsync();

            Logger.LogDebug("Sets receivers");
            await SetReceiversAsync();
        }

        private async Task LogFtId()
        {
            var data = await FormDataRepo.GetFormData(ArchiveReference);

            var ftid = XmlUtil.GetElementValue(data, "ftbId");

            if (!string.IsNullOrEmpty(ftid))
            {
                Logger.LogInformation("Logging ftId: {ftid}", ftid);

                try
                {
                    await DbUnitOfWork.FtId.Add(ArchiveReference, ftid);
                    DbUnitOfWork.LogEntries.AddInfo($"FtId {ftid} er koblet til {ArchiveReference}");
                }
                catch (System.Net.Http.HttpRequestException hre)
                {
                    if (hre.StatusCode == System.Net.HttpStatusCode.BadRequest)
                        DbUnitOfWork.LogEntries.AddError($"FtId {ftid} er ikke gyldig og kan ikke kobles mot {ArchiveReference}. {hre.Message}");
                    else
                        throw;
                }
            }
            else
                Logger.LogInformation("No ftId found in form data");
        }

        public virtual async Task PostExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {
            //Skal logging gjerast i eit anna steg? Alle filer bør være tilgjegelige for logging når prepare er ferdig.
            //Etter prepare er det utsending som skal skje, og da skal alle filer være klare for utsending.
            //Forslag: Me ser på muligheten for å bruke topics og har ein egen funksjon for logging av feks filer.
            //         Endrer da følgende logging til å skje i egen funksjon som kjøres etter prepare og før utsending.

            Logger.LogInformation("Logs information about attachements to Alfa");
            var allBlobs = new List<BlobItem>();

            //Henter private blobs
            var privateBlobs = await BlobOperations.GetBlobItemsAsync(FtB_Common.Enums.BlobStorageEnum.Private, ArchiveReference);
            allBlobs.AddRange(privateBlobs);

            Logger.LogDebug($"Found {privateBlobs.Count} blobs in private storage");

            //Henter også public blobs hvis det finnes
            var publicContainerName = BlobOperations.GetPublicBlobContainerName(ArchiveReference);
            if (!string.IsNullOrEmpty(publicContainerName))
            {
                var publicBlobs = await BlobOperations.GetBlobItemsAsync(FtB_Common.Enums.BlobStorageEnum.Public, publicContainerName);
                Logger.LogDebug($"Found {publicBlobs.Count} blobs in public storage");
                allBlobs.AddRange(publicBlobs);
            }

            //Filtrere blob til å kun være vedlegg/underskjema lasta opp av bruker, samt systemgenerete filer slik som valideringsrapporter

            var litOfTypes = new List<string>
            {
                BlobStorageMetadataTypes.SubmittalAttachment,
                BlobStorageMetadataTypes.Subform,
                BlobStorageMetadataTypes.ValidationResult,
                BlobStorageMetadataTypes.GeneratedAttachment
            };

            var filesInSubmittal = allBlobs.Where(b => litOfTypes.Contains(b.Metadata.Type()))
                .Select(b => new Dibk.Ftpb.Api.Models.FormAttachmentMetadataApiModel()
                {
                    AttachmentType = b.Metadata.AttachmentTypeName(),
                    FileName = b.FileName,
                    Size = (int)b.FileSize,
                    MimeType = b.MimeType,
                    Source = GetSource(b.Metadata.Type())
                })
                .ToList();

            if (filesInSubmittal?.Count > 0)
            {
                var userProvidedFiles = filesInSubmittal.Where(p => p.Source == "UserProvided").ToList();
                var summaryLogMessage = $"Skjema har {userProvidedFiles.Count()} vedlagte filer";
                DbUnitOfWork.LogEntries.AddInfo(summaryLogMessage);

                if (userProvidedFiles.Count() > 0)
                {
                    var logEntryMessage = string.Join(",", userProvidedFiles.Select(s => $"{s.AttachmentType} {s.FileName} [{s.Size}]").ToList());

                    DbUnitOfWork.LogEntries.AddInfo(logEntryMessage);

                    await DbUnitOfWork.FormAttachmentMetadata.AddAttachmentsAsync(userProvidedFiles);
                }
            }

            //Logger FtId
            await LogFtId();

            await DbUnitOfWork.SaveLogEntries();

            //!!------ NB : Må legge til logging av valideringsresultat til logEntries -------!!

            Logger.LogInformation("PrepareLogic executed");
        }

        private string GetSource(string type)
        {
            switch (type)
            {
                case BlobStorageMetadataTypes.SubmittalAttachment:
                    return "UserProvided";

                case BlobStorageMetadataTypes.Subform:
                    return "UserProvided";

                case BlobStorageMetadataTypes.ValidationResult:
                    return "SystemGenerated";

                case BlobStorageMetadataTypes.GeneratedAttachment:
                    return "SystemGenerated";

                default:
                    return "Unknown";
            }
        }

        public virtual Task<IEnumerable<SendQueueItem>> ExecuteAsync(SubmittalQueueItem submittalQueueItem)
        {
            return Task.FromResult<IEnumerable<SendQueueItem>>(null);
        }

        protected SendQueueItem CreateSendQueueItem(SubmittalQueueItem submittalQueueItem)
        {
            return new SendQueueItem()
            {
                ArchiveReference = ArchiveReference,
                ReceiverSequenceNumber = "0",
                Receiver = Receivers[0],
                ReceiverLogPartitionKey = $"{submittalQueueItem.ArchiveReference}-0",
                Sender = Sender
            };
        }
    }
}