﻿using Altinn.Common.Models;
using Altinn3.Adapters;
using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Enums;
using FtB_Common.Exceptions;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using Ftb_DbModels;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    public abstract class ReportLogic<T> : LogicBase<T>, IFormLogic<string, ReportQueueItem>
    {
        private readonly int BLOB_CONTAINER_LEASE_DURATION_MAX = 60;

        protected readonly IBlobOperations BlobOperations;
        protected readonly IAltinnInstanceCompleter AltinnInstanceCompleter;
        protected readonly DistributionSubmittalRepo DistributionSubmittalRepo;

        protected abstract bool CompleteAltinn3Instance { get; }
        protected abstract StatusApiFileInfo MainFormDownloadFileInfo { get; }
        protected abstract StatusApiFileInfo ReceiptDownloadFileInfo { get; }

        protected ReportQueueItem ReportQueueItem { get; private set; }

        protected ReportLogic(
            IBlobOperations blobOperations,
            IAltinnInstanceCompleter altinnInstanceCompleter,
            DistributionSubmittalRepo distributionSubmittalRepo,
            IFormDataRepo repo,
            ILogger log,
            IDbUnitOfWork dbUnitOfWork,
            IFileDownloadStatusHttpClient fileDownloadHttpClient,
            DistributionReceiverRepo distributionReceiverRepo,
            DistributionReceiverLogRepo distributionReceiverLogRepo
        ) : base(
            repo,
            log,
            dbUnitOfWork,
            fileDownloadHttpClient,
            distributionReceiverRepo,
            distributionReceiverLogRepo
        )
        {
            BlobOperations = blobOperations;
            AltinnInstanceCompleter = altinnInstanceCompleter;
            DistributionSubmittalRepo = distributionSubmittalRepo;
        }

        public virtual async Task PreExecuteAsync(ReportQueueItem reportQueueItem)
        {
            ReportQueueItem = reportQueueItem;
            base.ArchiveReference = reportQueueItem.ArchiveReference;

            Logger.LogDebug("Setting unitOfWork archiveReference");
            base.DbUnitOfWork.SetArchiveReference(reportQueueItem.ArchiveReference);

            Logger.LogDebug("Loading formdata");
            await LoadDataAsync(reportQueueItem.ArchiveReference);
        }

        public virtual async Task PostExecuteAsync(ReportQueueItem reportQueueItem)
        {
            if (CompleteAltinn3Instance)
            {
                bool instanceCompleted = await AltinnInstanceCompleter.PerformCompleteInstanceAsync(ArchiveReference);
            }
            Logger.LogDebug("Reportlogic executed");
        }

        public virtual Task<string> ExecuteAsync(ReportQueueItem reportQueueItem)
        {
            return Task.FromResult(reportQueueItem.ArchiveReference);
        }

        protected async Task<bool> ReadyForSubmittalReportingAsync(ReportQueueItem reportQueueItem)
        {
            if (await AllReceiversReadyForReporting(reportQueueItem))
            {
                return await SetReportingFlagForSubmittal(reportQueueItem.ArchiveReference.ToLower());
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// If method returns false, then this process was not able to acquire a lase on the container for this submittal.
        /// This is due to a preceding process already ha acquired the lease, and is therefore sending the submittal receipt
        /// </summary>
        /// <returns></returns>
        private async Task<bool> SetReportingFlagForSubmittal(string containerName)
        {
            return await BlobOperations.AcquireContainerLeaseAsync(containerName, BLOB_CONTAINER_LEASE_DURATION_MAX);
        }

        private async Task<bool> AllReceiversReadyForReporting(ReportQueueItem reportQueueItem)
        {
            DistributionSubmittalDto submittalEntity = await DistributionSubmittalRepo.GetAsync(reportQueueItem.ArchiveReference, reportQueueItem.ArchiveReference);
            var totalNumberOfReceivers = submittalEntity.ReceiverCount;
            var allReceiversInSubmittal = await DistributionReceiverRepo.GetAsync(reportQueueItem.ArchiveReference);
            //Get number of receivers with process-stage = Done, and compare this number to the totalNumberOfReceivers

            var validEnumValues = new DistributionReceiverProcessStageEnum[] { DistributionReceiverProcessStageEnum.ReadyForReporting, DistributionReceiverProcessStageEnum.Reported };
            var receiversReadyForReporting = allReceiversInSubmittal.Where(x => x.ProcessStage.HasValue && validEnumValues.Contains(x.ProcessStage.Value)).Count();

            return receiversReadyForReporting == totalNumberOfReceivers;
        }

        protected async Task<int> GetReceiverSuccessfullyNotifiedCountAsync(string archiveReference)
        {
            var allReceiversInSubmittal = await DistributionReceiverRepo.GetAsync(archiveReference);

            return allReceiversInSubmittal.Where(x => x.ProcessOutcome != null && x.ProcessOutcome == ReceiverProcessOutcomeEnum.Sent).Count();
        }

        protected async Task<IEnumerable<DistributionReceiverDto>> GetReceiverSuccessfullyNotifiedAsync(string archiveReference)
        {
            var allReceiversInSubmittal = await DistributionReceiverRepo.GetAsync(archiveReference);

            return allReceiversInSubmittal.Where(x => x.ProcessOutcome != null && x.ProcessOutcome == ReceiverProcessOutcomeEnum.Sent);
        }

        protected async Task ReportFormProcessStatusAsync(string status)
        {
            var formMetadata = await DbUnitOfWork.FormMetadata.Get();
            formMetadata.Status = status;
            DbUnitOfWork.FormMetadata.Update(formMetadata);
            await DbUnitOfWork.FormMetadata.Save();
        }

        protected async Task AddFileDownloadStatusesAsync(
            string archiveReference,
            Guid containerNameGuid,
            IEnumerable<(StatusApiFileInfo downloadFileInfo, AttachmentBinary binaryFile)> downloadFileInfosAndBinaries)
        {
            var allExistingFileDownloadStatuses = (await FileDownloadHttpClient.GetAllAsync(archiveReference)).ToList();

            foreach (var (downloadFileInfo, binaryFile) in downloadFileInfosAndBinaries)
            {
                await AddFileDownloadStatusAsync(
                    archiveReference,
                    downloadFileInfo,
                    binaryFile,
                    allExistingFileDownloadStatuses,
                    containerNameGuid);
            }

            await UpdateMetadataWithReceiptLinkAsync(archiveReference, containerNameGuid.ToString());
        }

        private async Task AddFileDownloadStatusAsync(
            string archiveReference,
            StatusApiFileInfo downloadFileInfo,
            AttachmentBinary binaryFile,
            List<FileDownloadStatus> allExistingFileDownloadStatuses,
            Guid containerGuid)
        {
            var blobLink = await PersistBlobAsync(containerGuid, downloadFileInfo, binaryFile.BinaryContent);

            FileDownloadStatus fileDownloadStatus = null;
            if (blobLink != null)
            {
                fileDownloadStatus = CreateFileDownloadStatus(
                    archiveReference,
                    containerGuid,
                    downloadFileInfo,
                    blobLink);
            }

            if (fileDownloadStatus == null)
            {
                return;
            }

            var existingFileDownloadStatus = allExistingFileDownloadStatuses.FirstOrDefault(p =>
                p.Filename.Equals(downloadFileInfo.FileName) && p.FileType.Equals(downloadFileInfo.FileType));

            if (existingFileDownloadStatus != null)
            {
                Logger.LogInformation("FileDownloadStatus for {formName} already exists; updating it..", downloadFileInfo.Name);
                fileDownloadStatus.Id = existingFileDownloadStatus.Id;
                var updateSuccessful = await FileDownloadHttpClient.PutAsync(archiveReference, fileDownloadStatus);
                if (!updateSuccessful)
                    throw new FileDownloadStatusException($"Updating FileDownloadStatus failed for {fileDownloadStatus.Filename}");
            }
            else
            {
                Logger.LogInformation("Creating FileDownloadStatus for {formName}", downloadFileInfo.Name);
                var postSuccessful = await AddToFileDownloads(archiveReference, fileDownloadStatus);
                if (!postSuccessful)
                    throw new FileDownloadStatusException($"Adding FileDownloadStatus failed for {fileDownloadStatus.Filename}");
            }
        }

        private async Task<string> PersistBlobAsync(
            Guid containerNameGuid,
            StatusApiFileInfo fileInfo,
            byte[] pdfByteStream)
        {
            if (pdfByteStream == null)
            {
                Logger.LogInformation("{pdfByteStream} is empty for '{fileName}'", nameof(pdfByteStream), fileInfo.FileName);
                return null;
            }

            var containerName = containerNameGuid.ToString().ToLower();

            Logger.LogInformation("Adding file '{fileName}' to blob storage {containerName}", fileInfo.FileName, containerName);

            var blobUri = await BlobOperations.AddBytesToBlobStorageAsync(
                BlobStorageEnum.Private,
                containerName,
                fileInfo.FileName,
                pdfByteStream,
                fileInfo.MimeType);

            return $"{blobUri}{containerName}/{fileInfo.FileName}";
        }

        private static FileDownloadStatus CreateFileDownloadStatus(
            string archiveReference,
            Guid guid,
            StatusApiFileInfo fileInfo,
            string blobLink)
        {
            return new FileDownloadStatus(
                archiveReference.ToUpper(),
                guid,
                fileInfo.FileType,
                fileInfo.FileName,
                blobLink,
                fileInfo.MimeType,
                fileInfo.Name);
        }

        private async Task UpdateMetadataWithReceiptLinkAsync(string archiveReference, string guid)
        {
            //Update FormMetadata with "DistributionReceiptLink"
            var formMetadata = await DbUnitOfWork.FormMetadata.Get(archiveReference.ToUpper());
            formMetadata.DistributionRecieptLink = guid;
            DbUnitOfWork.FormMetadata.Update(formMetadata);
            await DbUnitOfWork.FormMetadata.Save();
        }
    }
}