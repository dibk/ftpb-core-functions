using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Enums;
using FtB_Common.FormLogic;
using FtB_Common.Interfaces;
using FtB_Common.Utils;
using Ftb_DbModels;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    public abstract class LogicBase<T>
    {        
        protected readonly ILogger Logger;
        protected readonly IDbUnitOfWork DbUnitOfWork;
        protected readonly IFileDownloadStatusHttpClient FileDownloadHttpClient;
        protected readonly DistributionReceiverRepo DistributionReceiverRepo;
        protected readonly DistributionReceiverLogRepo DistributionReceiverLogRepo;

        public LogicBase(IFormDataRepo repo,
                         ILogger log,
                         IDbUnitOfWork dbUnitOfWork,
                         IFileDownloadStatusHttpClient fileDownloadHttpClient,
                         DistributionReceiverRepo distributionReceiverRepo,
                         DistributionReceiverLogRepo distributionReceiverLogRepo)
        {
            FormDataRepo = repo;
            Logger = log;
            DbUnitOfWork = dbUnitOfWork;
            FileDownloadHttpClient = fileDownloadHttpClient;
            DistributionReceiverRepo = distributionReceiverRepo;
            DistributionReceiverLogRepo = distributionReceiverLogRepo;
        }

        public string ArchiveReference { get; set; }
        protected readonly IFormDataRepo FormDataRepo;
        public T FormData { get; set; }

        public virtual async Task LoadDataAsync(string archiveReference)
        {
            ArchiveReference = archiveReference;
            var data = await FormDataRepo.GetFormData(ArchiveReference);
            FormData = SerializeUtil.DeserializeXml<T>(data);
        }

        protected async Task UpdateReceiverProcessStageAsync(DistributionReceiverDto receiverDto, string receiverSequenceNumber, DistributionReceiverProcessStageEnum processStageEnum)
        {
            try
            {
                receiverDto.ProcessStage = processStageEnum;
                await DistributionReceiverRepo.UpdateAsync(receiverDto);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "UpdateReceiverProcessStage: Error adding receiver record for ID={ReceiverSequenceNumber}", receiverSequenceNumber);
                throw;
            }
        }

        protected virtual async Task AddToReceiverProcessLogAsync(string receiverPartitionKey, string receiverID, string receiverName, DistributionReceiverStatusLogEnum statusEnum)
        {
            try
            {
                var receiverEntity = new DistributionReceiverLogDto(receiverPartitionKey, $"{DateTime.Now.ToString("yyyyMMddHHmmssffff")}", receiverID, receiverName, statusEnum);
                await DistributionReceiverLogRepo.AddAsync(receiverEntity);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Error adding receiver record for ID={ReceiverPartitionKey} and receiverID {ReceiverID}", receiverPartitionKey, receiverID);
                throw;
            }
        }

        protected async Task<bool> AddToFileDownloads(string archiveReference, FileDownloadStatus fileDownload)
        {
            return await FileDownloadHttpClient.PostAsync(archiveReference, fileDownload);
        }

        protected async Task<string> FileDownloadStatusExists(string archiveReference)
        {
            var listOfFileDownloadRecords = await FileDownloadHttpClient.GetAllAsync(archiveReference);

            if (listOfFileDownloadRecords != null && listOfFileDownloadRecords.ToList().Count > 0)
            {
                return listOfFileDownloadRecords.ToList()[0].Guid.ToString();
            }
            return null;
        }

        protected FormDataFormatAttribute GetFormDataFormatAttribute()
        {
            var type = this.GetType();
            return (FormDataFormatAttribute)type.GetCustomAttributes(typeof(FormDataFormatAttribute), true).FirstOrDefault();
        }
    }
}