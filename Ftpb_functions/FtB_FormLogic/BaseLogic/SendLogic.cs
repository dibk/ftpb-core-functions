﻿using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.Interfaces;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    public abstract class SendLogic<T> : LogicBase<T>, IFormLogic<ReportQueueItem, SendQueueItem>
    {
        protected readonly IDecryptionFactory DecryptionFactory;
        protected SendQueueItem SendQueueItem;
        protected virtual Receiver Receiver { get; set; }
        protected virtual IActor Sender { get; set; }

        protected DistributionReceiverStatusLogEnum State { get; set; }

        public SendLogic(IFormDataRepo repo,
                         ILogger log,
                         IDbUnitOfWork dbUnitOfWork,
                         IDecryptionFactory decryptionFactory,
                         IFileDownloadStatusHttpClient fileDownloadHttpClient,
                         DistributionReceiverRepo distributionReceiverRepo,
                         DistributionReceiverLogRepo distributionReceiverLogRepo)
            : base(repo,
                   log,
                   dbUnitOfWork,
                   fileDownloadHttpClient,
                   distributionReceiverRepo,
                   distributionReceiverLogRepo)
        {
            DecryptionFactory = decryptionFactory;
        }
        /// <summary>
        /// Sets: Properties SendQueueItem and ArchiveReference
        /// Initiates the dbUnitOfWorks archivereference and loads formdata
        /// Executes setting for Sender and Receiver
        /// </summary>
        /// <param name="sendQueueItem"></param>
        /// <returns></returns>
        public virtual async Task PreExecuteAsync(SendQueueItem sendQueueItem)
        {
            SendQueueItem = sendQueueItem;
            base.ArchiveReference = sendQueueItem.ArchiveReference;

            Logger.LogDebug("Setting unitOfWork archiveReference");
            base.DbUnitOfWork.SetArchiveReference(sendQueueItem.ArchiveReference);

            Logger.LogDebug("Sets receivers");
            await SetReceiversAsync();

            Logger.LogDebug("Loading formdata");
            await LoadDataAsync(sendQueueItem.ArchiveReference);

            Logger.LogDebug("Sets sender");
            await SetSenderAsync();
        }

        public virtual Task PostExecuteAsync(SendQueueItem sendQueueItem)
        {
            Logger.LogInformation("SendLogic executed");

            return Task.CompletedTask;
        }

        public virtual Task SetReceiversAsync()
        {
            Receiver = SendQueueItem.Receiver;

            return Task.CompletedTask;
        }

        public virtual Task SetSenderAsync()
        {
            Sender = SendQueueItem.Sender;

            return Task.CompletedTask;
        }

        public virtual Task<ReportQueueItem> ExecuteAsync(SendQueueItem sendQueueItem)
        {
            return Task.FromResult(new ReportQueueItem
            {
                ArchiveReference = sendQueueItem.ArchiveReference,
                Receiver = sendQueueItem.Receiver,
                ReceiverLogPartitionKey = sendQueueItem.ReceiverLogPartitionKey,
                ReceiverSequenceNumber = sendQueueItem.ReceiverSequenceNumber,
                Sender = sendQueueItem.Sender,
            });
        }

        protected override async Task AddToReceiverProcessLogAsync(string receiverPartitionKey, string receiverID, string receiverName, DistributionReceiverStatusLogEnum statusEnum)
        {
            this.State = statusEnum;
            await base.AddToReceiverProcessLogAsync(receiverPartitionKey, receiverID, receiverName, statusEnum);
        }
    }
}
