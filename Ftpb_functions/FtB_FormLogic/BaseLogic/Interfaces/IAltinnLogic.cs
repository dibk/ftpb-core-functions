﻿using System.Threading.Tasks;

namespace FtB_FormLogic.BaseLogic.Interfaces
{
    public interface IAltinnLogic<T, U>
    {
        Task<T> ExecuteAsync(U input);
    }
}