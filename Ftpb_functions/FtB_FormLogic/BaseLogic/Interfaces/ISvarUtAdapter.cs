﻿using Dibk.Ftpb.Integration.SvarUt.Models;
using System.Threading.Tasks;

namespace FtB_FormLogic
{
    public interface ISvarUtAdapter
    {
        /// <summary>
        /// Sends forsendelse to SvarUt
        /// </summary>
        /// <param name="forsendelse"></param>
        /// <returns>Forsendelse id</returns>
        Task<string> SendAsync(Forsendelse forsendelse);
    }
}
