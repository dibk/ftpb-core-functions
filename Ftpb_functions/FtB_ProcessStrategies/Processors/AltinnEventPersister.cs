﻿using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Interfaces;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;

namespace FtB_ProcessStrategies
{
    public class AltinnEventPersister
    {
        private readonly ILogger _logger;
        private readonly AltinnEventRepo _altinnEventRepo;

        public AltinnEventPersister(ILogger<AltinnEventPersister> logger, AltinnEventRepo altinnEventRepo)
        {
            _logger = logger;
            _altinnEventRepo = altinnEventRepo;
        }

        public async Task<AltinnEventQueueItem> HandleEvent(AltinnEvent altinnEvent)
        {
            using (var scope = _logger.BeginScope(new Dictionary<string, string> { { "ArchiveReference", altinnEvent.ArchiveReference }, { "AltinnEventType", altinnEvent.Type } }))
            {
                var storedEvent = await _altinnEventRepo.GetAsync(altinnEvent.ArchiveReference, altinnEvent.Type);

                if (storedEvent == null)
                {
                    _logger.LogInformation("New event received, storing it to table storage");
                    var newEvent = new AltinnEventDto(altinnEvent.ArchiveReference, altinnEvent.Type, JsonSerializer.Serialize(altinnEvent));
                    await _altinnEventRepo.AddAsync(newEvent);

                    return new AltinnEventQueueItem
                    {
                        ArchiveReference = altinnEvent.ArchiveReference,
                        AltinnEventType = altinnEvent.Type,
                    };
                }
                else
                {
                    _logger.LogWarning("Event already received. Exists in table storage. Skips further processing.");
                    return null;
                }
            }
        }
    }
}