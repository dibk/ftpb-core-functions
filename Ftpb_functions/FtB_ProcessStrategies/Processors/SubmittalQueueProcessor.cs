﻿using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.FormLogic;
using FtB_Common.Storage;
using FtB_FormLogic;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FtB_ProcessStrategies
{
    public class SubmittalQueueProcessor
    {
        private readonly FormatIdToFormMapper _formatIdToFormMapper;
        private readonly IBlobOperations _blobOperations;
        private readonly ILogger _log;

        public SubmittalQueueProcessor(FormatIdToFormMapper formatIdToFormMapper,
                                       IBlobOperations blobOperations,
                                       ILogger<SubmittalQueueProcessor> log)
        {
            _formatIdToFormMapper = formatIdToFormMapper;
            _blobOperations = blobOperations;
            _log = log;
        }
        public async Task<IEnumerable<SendQueueItem>> ExecuteProcessingStrategy(SubmittalQueueItem submittalQueueItem)
        {
            try
            {
                _log.LogInformation("Starts processing submittalQueueItem");
                string formatId = await _blobOperations.GetFormatIdFromStoredBlobAsync(submittalQueueItem.ArchiveReference);
                var formLogicBeingProcessed = _formatIdToFormMapper.GetFormLogic<IEnumerable<SendQueueItem>, SubmittalQueueItem>(formatId, FormLogicProcessingContext.Prepare);

                if (formLogicBeingProcessed == null)
                    throw new ArgumentException($"Unable to identify formlogic for dataformatid: {formatId} and processing context {FormLogicProcessingContext.Prepare}");

                await formLogicBeingProcessed.PreExecuteAsync(submittalQueueItem);
                var executeResult = await formLogicBeingProcessed.ExecuteAsync(submittalQueueItem);
                await formLogicBeingProcessed.PostExecuteAsync(submittalQueueItem);

                return executeResult;
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "An error occurred while processing");
                throw;
            }
        }
    }
}
