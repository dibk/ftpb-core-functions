﻿using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.Exceptions;
using FtB_Common.FormLogic;
using FtB_Common.Storage;
using FtB_FormLogic;
using Ftb_Repositories;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace FtB_ProcessStrategies
{
    public class SendQueueProcessor
    {
        private readonly FormatIdToFormMapper _formatIdToFormMapper;
        private readonly IBlobOperations _blobOperations;
        private readonly ILogger _log;
        private readonly IDbUnitOfWork _dbUnitOfWork;

        public SendQueueProcessor(FormatIdToFormMapper formatIdToFormMapper, IBlobOperations blobOperations, ILogger<SendQueueProcessor> log, IDbUnitOfWork dbUnitOfWork)
        {
            _formatIdToFormMapper = formatIdToFormMapper;
            _blobOperations = blobOperations;
            _log = log;
            _dbUnitOfWork = dbUnitOfWork;
        }

        public async Task<ReportQueueItem> ExecuteProcessingStrategyAsync(SendQueueItem sendQueueItem)
        {
            try
            {
                var sw = new Stopwatch();

                sw.Start();
                string serviceCode = await _blobOperations.GetServiceCodeFromStoredBlobAsync(sendQueueItem.ArchiveReference);
                sw.Stop();
                _log.LogInformation("Elapsed time for getServiceCodeFromStoredBlobAsync: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

                sw.Restart();
                string formatId = await _blobOperations.GetFormatIdFromStoredBlobAsync(sendQueueItem.ArchiveReference);
                sw.Stop();
                _log.LogInformation("Elapsed time for getFormatIdFromStoredBlobAsync: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

                sw.Restart();
                var formLogicBeingProcessed = _formatIdToFormMapper.GetFormLogic<ReportQueueItem, SendQueueItem>(formatId, FormLogicProcessingContext.Send);
                sw.Stop();
                _log.LogInformation("Elapsed time for getFormLogic: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

                if (formLogicBeingProcessed == null)
                    throw new ArgumentException($"Unable to identify formlogic for dataformatid: {formatId} and processing context {FormLogicProcessingContext.Send}");

                sw.Restart();
                await formLogicBeingProcessed.PreExecuteAsync(sendQueueItem);
                sw.Stop();
                _log.LogInformation("Elapsed time for preExecuteAsync: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);
                sw.Restart();
                var result = await formLogicBeingProcessed.ExecuteAsync(sendQueueItem);
                sw.Stop();
                _log.LogInformation("Elapsed time for executeAsync: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);
                sw.Restart();
                await formLogicBeingProcessed.PostExecuteAsync(sendQueueItem);
                sw.Stop();
                _log.LogInformation("Elapsed time for postExecuteAsync: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);
                return result;
            }
            catch (DistributionSendExeception dsex)
            {
                _log.LogError(dsex, "An error occurred while distribution for archiveReference {ArchiveReference}. Exception: {ExceptionContent}", sendQueueItem?.ArchiveReference, dsex.ToString());
                throw;
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "An error occurred while processing: {ArchiveReference}", sendQueueItem?.ArchiveReference);
                throw;
            }
            finally
            {
                await _dbUnitOfWork.SaveLogEntries();
            }
        }
    }
}
