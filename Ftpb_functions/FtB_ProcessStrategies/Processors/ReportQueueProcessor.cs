﻿using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.FormLogic;
using FtB_Common.Storage;
using FtB_FormLogic;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace FtB_ProcessStrategies
{
    public class ReportQueueProcessor
    {
        private readonly FormatIdToFormMapper _formatIdToFormMapper;
        private readonly IBlobOperations _blobOperations;
        
        private readonly ILogger _log;

        public ReportQueueProcessor(FormatIdToFormMapper formatIdToFormMapper, IBlobOperations blobOperations, ILogger<ReportQueueProcessor> log)
        {
            _blobOperations = blobOperations;
            _log = log;
            _formatIdToFormMapper = formatIdToFormMapper;
        }

        public async Task<string> ExecuteProcessingStrategyAsync(ReportQueueItem reportQueueItem)
        {
            string formatId = await _blobOperations.GetFormatIdFromStoredBlobAsync(reportQueueItem.ArchiveReference);
            var formLogicBeingProcessed = _formatIdToFormMapper.GetFormLogic<string, ReportQueueItem>(formatId, FormLogicProcessingContext.Report);
            if (formLogicBeingProcessed == null)
                throw new ArgumentException($"Unable to identify formlogic for dataformatid: {formatId} and processing context {FormLogicProcessingContext.Report}");

            await formLogicBeingProcessed.PreExecuteAsync(reportQueueItem);
            var executeResult = await formLogicBeingProcessed.ExecuteAsync(reportQueueItem);
            await formLogicBeingProcessed.PostExecuteAsync(reportQueueItem);
            return executeResult;
        }
    }
}
