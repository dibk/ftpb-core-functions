﻿using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Exceptions;
using FtB_FormLogic.Mappers;
using Microsoft.Extensions.Logging;
using System;
using System.Text.Json;
using System.Threading.Tasks;

namespace FtB_ProcessStrategies
{
    public class AltinnEventProcessor
    {
        private readonly ILogger _log;
        private readonly AltinnResourceAndTypeToFormMapper _altinnResourceToFormMapper;
        private readonly AltinnEventRepo _altinnEventRepo;

        public AltinnEventProcessor(ILogger<AltinnEventProcessor> log, AltinnResourceAndTypeToFormMapper altinnResourceToFormMapper, AltinnEventRepo altinnEventRepo)
        {
            _log = log;
            _altinnResourceToFormMapper = altinnResourceToFormMapper;
            _altinnEventRepo = altinnEventRepo;
        }

        public async Task<SubmittalQueueItem> ExecuteProcessingStrategy(AltinnEventQueueItem altinnEventQueueItem)
        {
            //Hente altinnEvent fra table storage
            _log.LogInformation("Loading AltinnEvent from table storage");
            var altinnEventDto = await _altinnEventRepo.GetAsync(altinnEventQueueItem.ArchiveReference, altinnEventQueueItem.AltinnEventType);
            ArgumentNullException.ThrowIfNull(altinnEventDto);

            _log.LogDebug("AltinnEvent loaded from table storage, deserializes it.");
            var altinnEvent = JsonSerializer.Deserialize<AltinnEvent>(altinnEventDto.AltinnEventJson);
            ArgumentNullException.ThrowIfNull(altinnEvent);

            altinnEventDto.EventStatus = AltinnEventStatusEnum.Processing;
            altinnEventDto = await _altinnEventRepo.UpdateAsync(altinnEventDto);

            try
            {
                _log.LogInformation("Starts processing AltinnEvent for instance {AltinnInstance}", altinnEvent.ResourceInstance);
                string formatId = altinnEvent.Resource;
                var altinnLogicToProcess = _altinnResourceToFormMapper.GetAltinnFormLogic<SubmittalQueueItem, AltinnEvent>(formatId, altinnEvent.Type);

                if (altinnLogicToProcess == null)
                    throw new ArgumentException($"Unable to identify formlogic for dataformatid: {formatId} and processing context {altinnEvent.Type}");

                var executeResult = await altinnLogicToProcess.ExecuteAsync(altinnEvent);

                altinnEventDto.EventStatus = AltinnEventStatusEnum.Completed;
                altinnEventDto = await _altinnEventRepo.UpdateAsync(altinnEventDto);

                _log.LogInformation("Finished processing AltinnEvent for instance {AltinnInstance}", altinnEvent.ResourceInstance);
                return executeResult;
            }
            catch (Exception ex)
            {
                if (ex is WrongEnvironmentException)
                {
                    _log.LogInformation(ex, "Attempting to set EventStatus to Completed and aborting AltinnEventprocessing as WrongEnvironmentException is thrown.");
                    // Skal me sette status til Completed her?
                    altinnEventDto.EventStatus = AltinnEventStatusEnum.Completed;
                    await _altinnEventRepo.UpdateAsync(altinnEventDto);
                    return null;
                }
                else
                {
                    // Skal me sette status til failed her?
                    _log.LogError(ex, "An error occurred while processing");
                    altinnEventDto.EventStatus = AltinnEventStatusEnum.Failed;
                    await _altinnEventRepo.UpdateAsync(altinnEventDto);
                }

                throw;
            }
        }
    }
}