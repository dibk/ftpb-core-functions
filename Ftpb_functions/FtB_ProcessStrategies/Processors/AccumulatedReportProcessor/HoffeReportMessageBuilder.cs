﻿using FtB_Common.Enums;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using Microsoft.Extensions.Logging;

namespace FtB_ProcessStrategies.Processors.AccumulatedReportProcessor
{
    public class HoffeReportMessageBuilder : ReportMessageBuilder
    {
        public HoffeReportMessageBuilder(ILogger<ReportMessageBuilder> logger, IBlobOperations blobOperations, IHtmlUtils htmlUtils) : base(logger, blobOperations, htmlUtils)
        { }

        public override string ReportMessageHtml => "FtB_FormLogic.Notifications.NotificationFormLogic.SvarHoeringOgOffentligEttersynLogic.SvarHoeringOgOffentligEttersynLogicReportMessageBody.html";
        public override string TypeOfProcess => "høring og offentlig ettersyn";
        public override string SupportingType => "HOFFE";
        public override string AttachmentTypeName => "VarselHoeringOgOffentligEttersyn";
        public override string ReportRepliesHTML => "FtB_FormLogic.Notifications.NotificationFormLogic.SvarHoeringOgOffentligEttersynLogic.SvarHoeringOgOffentligEttersynReport.html";
        public override string FormName => "VarselHoeringOgOffentligEttersyn";

        public override FileTypesForDownloadEnum FileType => FileTypesForDownloadEnum.KommentarHoeringOgOffentligEttersynKvittering;

        public override int ReplyDeadLineAcceptedDays => 14;
    }
}
