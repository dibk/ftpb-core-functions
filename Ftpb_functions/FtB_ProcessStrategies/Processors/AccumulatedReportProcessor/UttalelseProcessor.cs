﻿using Altinn.Common;
using Altinn.Common.Exceptions;
using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Enums;
using FtB_Common.Storage;
using FtB_Common.Utils;
using Ftb_DbModels;
using FtB_FormLogic;
using FtB_Print.HttpClients.Pdf;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace FtB_ProcessStrategies.Processors.AccumulatedReportProcessor
{
    public class UttalelseProcessor
    {
        private readonly IBlobOperations _blobOperations;
        private readonly INotificationAdapter _notificationAdapter;
        private readonly IPdfHttpClient _pdfHttpClient;
        private readonly ILogger _log;
        private readonly IDbUnitOfWork _dbUnitOfWork;
        private readonly IFileDownloadStatusHttpClient _fileDownloadHttpClient;
        private readonly ReportMessageBuilderFactory _reportMessageBuilderFactory;
        private readonly DistributionSubmittalRepo _distributionSubmittalRepo;
        private readonly NotificationSenderRepo _notificationSenderRepo;
        private readonly NotificationSenderLogRepo _notificationSenderLogRepo;

        public UttalelseProcessor(
            IBlobOperations blobOperations,
            INotificationAdapter notificationAdapter,
            IPdfHttpClient pdfHttpClient,
            ILogger<UttalelseProcessor> log,
            IDbUnitOfWork dbUnitOfWork,
            IFileDownloadStatusHttpClient fileDownloadHttpClient,
            ReportMessageBuilderFactory reportMessageBuilderFactory,
            DistributionSubmittalRepo distributionSubmittalRepo,
            NotificationSenderRepo notificationSenderRepo,
            NotificationSenderLogRepo notificationSenderLogRepo)
        {
            _blobOperations = blobOperations;
            _notificationAdapter = notificationAdapter;
            _pdfHttpClient = pdfHttpClient;
            _log = log;
            _dbUnitOfWork = dbUnitOfWork;
            _fileDownloadHttpClient = fileDownloadHttpClient;
            _reportMessageBuilderFactory = reportMessageBuilderFactory;
            _distributionSubmittalRepo = distributionSubmittalRepo;
            _notificationSenderRepo = notificationSenderRepo;
            _notificationSenderLogRepo = notificationSenderLogRepo;
        }

        //TODO: See ToDo's below
        /// <summary>
        /// What is happening here:
        /// </summary>
        /// - Get all DistributionSubmittal To Report For
        /// - Get all answers for each DistributionSubmittal
        ///   - If reply due date is passed, set status for DistributionSubmittal to "Reported"
        ///   - Populate SvarPaaVarselOmOppstartAvPlanarbeidModel with main data for the DistributionSubmitter report
        ///   - For each answer, populate SvarPaaVarselOmOppstartAvPlanarbeidSenderModel with data from the sender (neighbour)
        /// - For each DistributionSubmittal
        ///   - If status is Distributed, set it to "ReportingInProgress"
        ///   - If more than 0 answers, Send Replies Report To DistributionSubmitter
        ///     - Build Altinn Notification Message
        ///       - Get Accumulated Replies Report from HTML template and merge DistributionSubmitter data
        ///       - Convert the Accumulated Replies Report to PDF, to be used as attachment to the Altinn report message (below)
        ///       - GetReportMessage
        ///         - Get SvarVarselOppstartPlanarbeidReportMessageBody HTML template
        ///         - Merge report main data into template
        ///         - Get a list of blob URL's of reply PDF's from public bblob storage
        ///         - Add a list of all replies from neighbours to the *Altinn notofication message* (TODO: What are the size limit for the message? Can this be to big?)
        ///       - Add message and report attachment to the Altinn Notification Message
        ///     - Send Altinn Notification Message
        ///     - Check if sending was successfull, and for each sender
        ///       - Get the DB distributionForm record for *this* answer and update distributionStatus and receiptfields
        ///       - Send the PDF report to the archive reference private blob storage (TODO: should me moved above the loop)
        ///       - Add a DB FileDownloadStatus record with link to the DistributionSubmittal report
        ///       - Update the senders notification answer entity status (NotificationSenderProcessStageEnum) to "Reported" and add to list
        ///     - Persist the notification answer entity list (above) to table storage
        ///     - Add logging "NotificationSenderStatusLogEnum.Completed"
        ///   - Return list of report receiverID and archiveReference
        /// <returns></returns>

        public async Task<IEnumerable<string>> ExecuteProcessingStrategyAsync(string type)
        {
            var distributionSubmittalEntities = GetDistributionSubmittalEntitiesToReportFor(type);

            _log.LogInformation("Number of distributionSubmittalEntities found: {DistributionSubmittalEntitiesCount}", distributionSubmittalEntities.Count());
            if (distributionSubmittalEntities.Count() > 0)
            {
                var processedArchiveReferences = new List<string>();

                foreach (var distributionSubmittalEntity in distributionSubmittalEntities)
                {
                    var archiveReference = distributionSubmittalEntity.PartitionKey;

                    using (var scope = _log.BeginScope(new Dictionary<string, string> { { "ArchiveReference", archiveReference } }))
                    {
                        _dbUnitOfWork.SetArchiveReference(archiveReference);

                        var reportBuilder = _reportMessageBuilderFactory.GetBuilder(type);

                        bool replyDeadlinePassed = await IsReplyDeadlinePassed(distributionSubmittalEntity, reportBuilder.ReplyDeadLineAcceptedDays);
                        if (replyDeadlinePassed)
                        {
                            continue;
                        }

                        var answerToDistributionSubmitter = GetNotificationSenderReplies(distributionSubmittalEntity);
                        _log.LogInformation($"Number of answers for {distributionSubmittalEntity.PartitionKey} found: {(answerToDistributionSubmitter == null || answerToDistributionSubmitter.Senders.Count() == 0 ? "0" : answerToDistributionSubmitter.Senders.Count().ToString())}");

                        if (distributionSubmittalEntity.Status != null && distributionSubmittalEntity.Status == DistributionSubmittalStatusEnum.Distributed)
                        {
                            distributionSubmittalEntity.Status = DistributionSubmittalStatusEnum.ReportingInProgress;
                            _log.LogInformation("Setting distributionSubmittal status to {DistributionSubmittalEntityStatus}", distributionSubmittalEntity.Status);
                            await _distributionSubmittalRepo.UpdateAsync(distributionSubmittalEntity);
                        }

                        if (answerToDistributionSubmitter != null)
                        {
                            _dbUnitOfWork.LogEntries.AddInfo($"Sender rapport om svar fra berørte parter på {reportBuilder.TypeOfProcess} til forslagstiller med id {distributionSubmittalEntity.SenderId}");
                            var publicBlobContainer = _blobOperations.GetPublicBlobContainerName(answerToDistributionSubmitter.InitialArchiveReference.ToLower());
                            _log.LogInformation($"Reporting PDF replies to submitter with senderId {distributionSubmittalEntity.SenderId} for archiveReference {answerToDistributionSubmitter.InitialArchiveReference}");
                            await SendRepliesReportToDistributionSubmitterAsync(
                                answerToDistributionSubmitter,
                                publicBlobContainer,
                                distributionSubmittalEntity.ReceiptsContainerName,
                                type);

                            processedArchiveReferences.Add(distributionSubmittalEntity.PartitionKey);
                        }

                        await _dbUnitOfWork.SaveLogEntries();
                    }
                }

                return processedArchiveReferences;
            }

            return null;
        }

        private async Task<bool> IsReplyDeadlinePassed(DistributionSubmittalDto distributionSubmittalDto, int replyDeadLineAcceptedDays)
        {
            //Increases the reply deadline with {replyDeadLineAcceptedDays} to make sure a form can be reported on if it
            // was received within the accepted time period after the execution of the report.
            var adjustedReplyDeadlineDate = distributionSubmittalDto.ReplyDeadline.Date;
            adjustedReplyDeadlineDate = adjustedReplyDeadlineDate.AddDays(replyDeadLineAcceptedDays);

            bool fristUtgaatt = DateTime.Now.Date > adjustedReplyDeadlineDate;

            if (fristUtgaatt)
            {
                distributionSubmittalDto.Status = DistributionSubmittalStatusEnum.Reported;
                distributionSubmittalDto = await _distributionSubmittalRepo.UpdateAsync(distributionSubmittalDto);
            }
            return fristUtgaatt;
        }

        private IEnumerable<DistributionSubmittalDto> GetDistributionSubmittalEntitiesToReportFor(string type)
        {
            var distributionSubmittalEntitiesDistributed = GetDistributionsByStatusAndType(DistributionSubmittalStatusEnum.Distributed.ToString(), type);
            var distributionSubmittalEntitiesReportingInProgress = GetDistributionsByStatusAndType(DistributionSubmittalStatusEnum.ReportingInProgress.ToString(), type);

            IEnumerable<DistributionSubmittalDto> distributionSubmittalEntities = distributionSubmittalEntitiesDistributed.Select(x => x).Concat(distributionSubmittalEntitiesReportingInProgress.Select(y => y));
            return distributionSubmittalEntities;
        }

        private IEnumerable<DistributionSubmittalDto> GetDistributionsByStatusAndType(string status, string type)
        {
            var filters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("Status", status),
                new KeyValuePair<string, string>("Type", type)
            };

            return _distributionSubmittalRepo.Get(filters);
        }

        private SvarPaaVarselOmOppstartAvPlanarbeidModel GetNotificationSenderReplies(DistributionSubmittalDto distributionSubmittalEntity)
        {
            var filters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("PartitionKey", distributionSubmittalEntity.PartitionKey),
                new KeyValuePair<string, string>("ProcessStage", NotificationSenderProcessStageEnum.Created.ToString())
            };

            var notificationSendersReadyToReport = _notificationSenderRepo.Get(filters)?.ToList();

            if (notificationSendersReadyToReport != null && notificationSendersReadyToReport.Count > 0)
            {
                SvarPaaVarselOmOppstartAvPlanarbeidModel reportData = new SvarPaaVarselOmOppstartAvPlanarbeidModel();
                reportData.InitialArchiveReference = distributionSubmittalEntity.PartitionKey;
                reportData.FristForInnspill = distributionSubmittalEntity.ReplyDeadline;
                reportData.ReceiverId = distributionSubmittalEntity.SenderId;
                reportData.AltinnReceiverType = AltinnReceiverType.Foretak;
                reportData.Senders = new List<SvarPaaVarselOmOppstartAvPlanarbeidSenderModel>();

                foreach (var notificationSender in notificationSendersReadyToReport)
                {
                    reportData.PlanId = notificationSender.PlanId;
                    reportData.PlanNavn = notificationSender.PlanNavn;

                    var sender = new SvarPaaVarselOmOppstartAvPlanarbeidSenderModel();
                    sender.InitialExternalSystemReference = notificationSender.InitialExternalSystemReference;
                    sender.SenderName = notificationSender.SenderName;
                    sender.SenderPhone = notificationSender.SenderPhone;
                    sender.SenderEmail = notificationSender.SenderEmail;
                    sender.Reply = notificationSender.Reply;
                    sender.Objection = notificationSender.Objection;
                    sender.SendersArchiveReference = notificationSender.RowKey;
                    reportData.Senders.Add(sender);
                }

                return reportData;
            }

            return null;
        }

        private async Task SendRepliesReportToDistributionSubmitterAsync(SvarPaaVarselOmOppstartAvPlanarbeidModel repliesToPlanNotice, string publicContainer, Guid containerName, string reportingForType)
        {
            try
            {
                _log.LogInformation("Sends replies for {PlanId} ", repliesToPlanNotice.PlanId);
                var distributionResults = await BuildNotificationMessageAsync(repliesToPlanNotice, publicContainer, reportingForType, containerName);

                string receiptId = null;
                if (distributionResults.IsSuccessfull())
                    receiptId = distributionResults.CorrespondenceResult.CorrespondenceAltinnReceiptId;
                if (distributionResults.IsSuccessfull() && receiptId != null)
                {
                    var entitiesToUpdate = new List<NotificationSenderDto>();

                    foreach (var sender in repliesToPlanNotice.Senders)
                    {
                        var distrForm = await _dbUnitOfWork.DistributionForms.Get(sender.InitialExternalSystemReference);
                        distrForm.DistributionStatus = DistributionStatus.receiptSent;
                        distrForm.RecieptSent = DateTime.Now;
                        distrForm.RecieptSentArchiveReference = receiptId;

                        var success = await _dbUnitOfWork.DistributionForms.Update(distrForm.InitialArchiveReference, distrForm.Id, distrForm);

                        var filters = new List<KeyValuePair<string, string>>();
                        filters.Add(new KeyValuePair<string, string>("PartitionKey", repliesToPlanNotice.InitialArchiveReference));
                        filters.Add(new KeyValuePair<string, string>("RowKey", sender.SendersArchiveReference));

                        var notificationSenderEntity = await _notificationSenderRepo.GetAsync(repliesToPlanNotice.InitialArchiveReference, sender.SendersArchiveReference);

                        notificationSenderEntity.ProcessStage = NotificationSenderProcessStageEnum.Reported;
                        entitiesToUpdate.Add(notificationSenderEntity);
                    }

                    await _notificationSenderRepo.UpdateAsync(entitiesToUpdate);

                    var tasks = entitiesToUpdate.Select(s => AddToSenderProcessLogAsync(s.RowKey, s.SenderId, NotificationSenderStatusLogEnum.Completed));

                    await Task.WhenAll(tasks);
                }
                else
                {
                    var failedStep = distributionResults.FinalState;
                    throw new SendNotificationException("Error: Failed during sending report of neighbours replies to submitter", failedStep);
                }
            }
            catch (SendNotificationException ex)
            {
                _log.LogError($"Sending of report of replies to submitter for archiveReference {repliesToPlanNotice.InitialArchiveReference} failed. DistributionStep: {ex.DistriutionStep}");
                throw;
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Error occurred when creating and sending receipt");
                throw;
            }
        }

        private async Task<AltinnNotificationCorrespondenceResult> BuildNotificationMessageAsync(SvarPaaVarselOmOppstartAvPlanarbeidModel repliesToPlanNotice, string publicContainer, string reportingForType, Guid containerName)
        {
            var reportBuilder = _reportMessageBuilderFactory.GetBuilder(reportingForType);
            var notificationMessage = new AltinnNotificationMessage();
            notificationMessage.ArchiveReference = repliesToPlanNotice.InitialArchiveReference;
            notificationMessage.Receiver = new AltinnReceiver() { Id = repliesToPlanNotice.ReceiverId, Type = repliesToPlanNotice.AltinnReceiverType };
            notificationMessage.RespectReservable = false;

            var reportHtml = await reportBuilder.GetAccumulatedRepliesReport(repliesToPlanNotice);
            using var pdfStream = await _pdfHttpClient.GeneratePdfDocumentAsync(reportHtml);

            var messageData = await reportBuilder.GetReportMessageAsync(repliesToPlanNotice, publicContainer);
            var validFilename = repliesToPlanNotice.PlanNavn.RemoveInvalidChars();
            notificationMessage.MessageData = messageData;

            var reportAttachment = new AttachmentBinary()
            {
                BinaryContent = pdfStream.ToArray(),
                Filename = $"Uttalelser_{validFilename}_{DateTime.Now.ToString("dd.MM.yyyy, kl.HH.mm.ss")}.pdf",
                Name = $"Uttalelser pr. {DateTime.Now.ToString("dd.MM.yyyy")}",
                ArchiveReference = repliesToPlanNotice.InitialArchiveReference
            };

            notificationMessage.Attachments = new List<Attachment>() { reportAttachment };

            //return notificationMessage;

            AltinnNotificationCorrespondenceResult distributionResults = await _notificationAdapter.SendNotificationAsync(notificationMessage);

            string receiptId = null;

            if (distributionResults.IsSuccessfull())
                receiptId = distributionResults.CorrespondenceResult.CorrespondenceAltinnReceiptId;

            if (distributionResults.IsSuccessfull() && receiptId != null)
            {
                _log.LogInformation($"Successfully sent report of replies to submitter for archiveReference {repliesToPlanNotice.InitialArchiveReference} with {notificationMessage.Attachments.Count()} attachments.");
                AttachmentBinary submittersReceipt = (AttachmentBinary)notificationMessage.Attachments.ToList()[0];

                var uri = await _blobOperations.AddBytesToBlobStorageAsync(BlobStorageEnum.Private,
                                                       containerName.ToString().ToLower(),
                                                       submittersReceipt.Filename,
                                                       submittersReceipt.BinaryContent,
                                                       submittersReceipt.Type);

                var fds = new FileDownloadStatus()
                {
                    ArchiveReference = submittersReceipt.ArchiveReference.ToUpper(),
                    BlobLink = uri,
                    FileAccessCount = 0,
                    Filename = submittersReceipt.Filename,
                    FileType = reportBuilder.FileType,
                    FormName = reportBuilder.FormName,
                    Guid = containerName != Guid.Empty ? containerName : Guid.NewGuid(),
                    IsDeleted = false,
                    MimeType = "application/pdf"
                };

                await _fileDownloadHttpClient.PostAsync(fds.ArchiveReference, fds);
            }

            return distributionResults;
        }

        protected virtual async Task AddToSenderProcessLogAsync(string initialArchiveReference, string senderId, NotificationSenderStatusLogEnum statusEnum)
        {
            try
            {
                var senderEntity = new NotificationSenderLogDto(initialArchiveReference, $"{DateTime.Now.ToString("yyyyMMddHHmmssffff")}", senderId, statusEnum);
                await _notificationSenderLogRepo.AddAsync(senderEntity);
            }
            catch (Exception ex)
            {
                _log.LogError(ex, $"Error adding sender record for ID={initialArchiveReference} and senderId {senderId}");
                throw;
            }
        }
    }

    public class AttachmentTypeNameComparer : IComparer<string>
    {
        public int Compare([AllowNull] string x, [AllowNull] string y)
        {
            var order = new List<(int, string)>()
            {
                (1, "SvarNabovarselPlan"),
                (2, "SvarHoeringOgOffentligEttersyn"),
                (3, "Annet")
            };

            var xOrderElement = order.FirstOrDefault(o => o.Item2.Equals(x, StringComparison.OrdinalIgnoreCase));
            var yOrderElement = order.FirstOrDefault(o => o.Item2.Equals(y, StringComparison.OrdinalIgnoreCase));

            var xOrder = xOrderElement != (default(int), default(string)) ? xOrderElement.Item1 : 99;
            var yOrder = yOrderElement != (default(int), default(string)) ? yOrderElement.Item1 : 99;

            return xOrder.CompareTo(yOrder);
        }
    }
}