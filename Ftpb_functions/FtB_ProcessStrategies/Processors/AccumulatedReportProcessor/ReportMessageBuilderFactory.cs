﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FtB_ProcessStrategies.Processors.AccumulatedReportProcessor
{
    public class ReportMessageBuilderFactory
    {
        private readonly IEnumerable<IReportMessageBuilder> _reportMessageBuilders;

        public ReportMessageBuilderFactory(IEnumerable<IReportMessageBuilder> reportMessageBuilders)
        {
            _reportMessageBuilders = reportMessageBuilders;
        }

        public IReportMessageBuilder GetBuilder(string type)
        {
            return _reportMessageBuilders.Single(p => p.SupportingType.Equals(type, StringComparison.OrdinalIgnoreCase));
        }
    }
}
