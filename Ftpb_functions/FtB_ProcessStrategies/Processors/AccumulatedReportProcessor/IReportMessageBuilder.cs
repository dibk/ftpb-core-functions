﻿using Altinn.Common.Models;
using FtB_Common.Enums;
using FtB_FormLogic;
using System.Threading.Tasks;

namespace FtB_ProcessStrategies.Processors.AccumulatedReportProcessor
{
    public interface IReportMessageBuilder
    {
        string SupportingType { get; }
        string FormName { get; }
        string TypeOfProcess { get; }
        int ReplyDeadLineAcceptedDays { get; }
        FileTypesForDownloadEnum FileType { get; }
        Task<MessageDataType> GetReportMessageAsync(SvarPaaVarselOmOppstartAvPlanarbeidModel answer, string publicContainer);
        Task<string> GetAccumulatedRepliesReport(SvarPaaVarselOmOppstartAvPlanarbeidModel answer);
    }
}
