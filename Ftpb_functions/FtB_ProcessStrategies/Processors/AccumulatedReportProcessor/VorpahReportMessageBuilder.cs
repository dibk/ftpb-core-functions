﻿using FtB_Common.Enums;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using Microsoft.Extensions.Logging;

namespace FtB_ProcessStrategies.Processors.AccumulatedReportProcessor
{
    public class VorpahReportMessageBuilder : ReportMessageBuilder
    {
        public VorpahReportMessageBuilder(ILogger<ReportMessageBuilder> logger, IBlobOperations blobOperations, IHtmlUtils htmlUtils) : base(logger, blobOperations, htmlUtils)
        { }

        public override string ReportMessageHtml => "FtB_FormLogic.Notifications.NotificationFormLogic.SvarVarselOppstartPlanarbeidPlussHoeringsmyndigheterLogic.Send.SvarVarselOppstartPlanarbeidPlussHoeringsmyndigheterReportMessageBody.html";
        public override string TypeOfProcess => "Varsel oppstart av reguleringsplanarbeid";
        public override string SupportingType => "VORPAH";
        public override string AttachmentTypeName => "VarselOppstartReguleringsplanarbeid";
        public override string ReportRepliesHTML => "FtB_FormLogic.Notifications.NotificationFormLogic.SvarVarselOppstartPlanarbeidPlussHoeringsmyndigheterLogic.Send.SvarVarselOppstartPlanarbeidPlussHoeringsmyndigheterReport.html";
        public override string FormName => "VarselHoeringOgOffentligEttersyn";

        public override FileTypesForDownloadEnum FileType => FileTypesForDownloadEnum.RapportVarselOppstartPlanarbeid;

        public override int ReplyDeadLineAcceptedDays => 14;
    }
}
