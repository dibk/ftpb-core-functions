﻿using FtB_Common.Enums;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using Microsoft.Extensions.Logging;

namespace FtB_ProcessStrategies.Processors.AccumulatedReportProcessor
{
    public class VorpaReportMessageBuilder : ReportMessageBuilder
    {
        public VorpaReportMessageBuilder(ILogger<ReportMessageBuilder> logger, IBlobOperations blobOperations, IHtmlUtils htmlUtils) : base(logger, blobOperations, htmlUtils)
        { }

        public override string ReportMessageHtml => "FtB_FormLogic.Notifications.NotificationFormLogic.SvarVarselOppstartPlanarbeidLogic.SvarVarselOppstartPlanarbeidReportMessageBody.html";
        public override string TypeOfProcess => "varsel om oppstart av planarbeid";
        public override string SupportingType => "VORPA";
        public override string AttachmentTypeName => "VarselOppstartPlanarbeid";
        public override string ReportRepliesHTML => "FtB_FormLogic.Notifications.NotificationFormLogic.SvarVarselOppstartPlanarbeidLogic.SvarVarselOppstartPlanarbeidReport.html";
        public override string FormName => "VarselOppstartPlanarbeid";

        public override FileTypesForDownloadEnum FileType => FileTypesForDownloadEnum.KommentarNabomerknader;

        public override int ReplyDeadLineAcceptedDays => 365; //Ønsker at det skal sendes oppsamling i inntil eit år etter fristen..
    }
}