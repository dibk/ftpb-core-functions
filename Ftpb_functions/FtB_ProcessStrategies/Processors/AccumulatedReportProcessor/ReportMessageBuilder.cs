﻿using Altinn.Common.Models;
using FtB_Common.Enums;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_FormLogic;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FtB_ProcessStrategies.Processors.AccumulatedReportProcessor
{
    public abstract class ReportMessageBuilder : IReportMessageBuilder
    {
        public abstract string ReportMessageHtml { get; }
        public abstract string TypeOfProcess { get; }
        public abstract string AttachmentTypeName { get; }
        public abstract string ReportRepliesHTML { get; }
        public abstract string SupportingType { get; }
        public abstract string FormName { get; }
        public abstract int ReplyDeadLineAcceptedDays { get; }
        public abstract FileTypesForDownloadEnum FileType { get; }

        private readonly ILogger<ReportMessageBuilder> _log;
        private readonly IBlobOperations _blobOperations;
        private readonly IHtmlUtils _htmlUtils;
        public ReportMessageBuilder(ILogger<ReportMessageBuilder> logger, IBlobOperations blobOperations, IHtmlUtils htmlUtils)
        {
            _log = logger;
            _blobOperations = blobOperations;
            _htmlUtils = htmlUtils;
        }
        public async Task<MessageDataType> GetReportMessageAsync(SvarPaaVarselOmOppstartAvPlanarbeidModel answer, string publicContainer)
        {
            try
            {
                string archiveReference = answer.InitialArchiveReference;
                string planNavn = answer.PlanNavn;
                string planId = answer.PlanId;

                string htmlBody = _htmlUtils.GetHtmlFromTemplate(ReportMessageHtml);

                htmlBody = htmlBody.Replace("<fristForInnspill />", answer.FristForInnspill.ToLocalTime().ToString("dd.MM.yyyy"));
                htmlBody = htmlBody.Replace("<arkivReferanse />", archiveReference.ToUpper());
                htmlBody = htmlBody.Replace("<planId />", planId);
                htmlBody = htmlBody.Replace("<planNavn />", planNavn);

                var metadataList = new List<KeyValuePair<string, string>>();

                foreach (var sender in answer.Senders)
                    metadataList.Add(new KeyValuePair<string, string>(BlobStorageMetadataKeys.SendersArchiveReference, sender.SendersArchiveReference));

                var urlToPublicAttachments = await _blobOperations.GetBlobContentsAsBytesByMetadataAsync(BlobStorageEnum.Public, publicContainer, metadataList);
                var groups = urlToPublicAttachments.GroupBy(g => g.Metadata.FirstOrDefault(m => m.Key.Equals(BlobStorageMetadataKeys.SendersArchiveReference, StringComparison.OrdinalIgnoreCase)).Value);

                var urlListAsHtml = new StringBuilder();

                foreach (var group in groups)
                {
                    var sender = answer.Senders.FirstOrDefault(s => s.SendersArchiveReference.Equals(group.Key, StringComparison.OrdinalIgnoreCase));

                    //Sorts attachments to make the SvarNabovarselPlan appear first then trailed by other attachments
                    var sortedAttachments = group.ToList().OrderBy(a => a.Metadata.FirstOrDefault(s => s.Key.Equals(BlobStorageMetadataKeys.AttachmentTypeName, StringComparison.OrdinalIgnoreCase)).Value, new AttachmentTypeNameComparer());

                    foreach (var attachmentInfo in sortedAttachments.ToList())
                    {
                        urlListAsHtml.Append($"<li><a href='{attachmentInfo.BlobUrl}' target='_blank'>{attachmentInfo.FileName}</a></li>");
                    }
                }

                htmlBody = htmlBody.Replace("<svarbrevsliste />", urlListAsHtml.ToString());

                var mess = new MessageDataType()
                {
                    MessageTitle = $"Rapport - svar fra berørte parter ang. {TypeOfProcess}, {planNavn}",
                    MessageSummary = "Trykk på vedleggene under for å åpne rapporten eller svarene fra de berørte partene",
                    MessageBody = htmlBody
                };

                return mess;
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Error occurred while getting submitter receipt message");

                throw;
            }
        }

        public async Task<string> GetAccumulatedRepliesReport(SvarPaaVarselOmOppstartAvPlanarbeidModel answer)
        {
            var publicContainerName = _blobOperations.GetPublicBlobContainerName(answer.InitialArchiveReference);
            var sendersFiles = await _blobOperations.GetBlobContentsAsBytesByMetadataAsync(BlobStorageEnum.Public, publicContainerName, answer.Senders.Select(s => new KeyValuePair<string, string>(BlobStorageMetadataKeys.SendersArchiveReference, s.SendersArchiveReference)));

            var attachments = sendersFiles.Where(s => !s.Metadata.Where(m => m.Key.Equals(BlobStorageMetadataKeys.AttachmentTypeName, StringComparison.OrdinalIgnoreCase) && m.Value.Equals(AttachmentTypeName, StringComparison.OrdinalIgnoreCase)).Any());

            try
            {
                string arkivReferanse = answer.InitialArchiveReference;
                var sb = new StringBuilder();
                foreach (var sender in answer.Senders)
                {
                    sb.Append($"<thead><tr><td><label>Navn:</label></td><td><label>Telefon:</label></td><td><label>E-post:</label></td></tr></thead>");
                    sb.Append($"<tbody><tr><td>{sender.SenderName}</td><td>{sender.SenderPhone}</td><td>{sender.SenderEmail}</td></tr>"
                              + "<tr><td colspan='3'><label>Uttalelse</label></td></tr>"
                              + "<tr><td colspan='3'>" + sender.Reply + "</td></tr>");
                    if (!string.IsNullOrEmpty(sender.Objection))
                    {
                        sb.Append($"<tr><td colspan='3'><label>Innsigelse</label></td></tr>"
                              + "<tr><td colspan='3'>" + sender.Objection + "</td></tr>");
                    }
                    
                    var at = attachments.Where(a => a.Metadata.Where(m => m.Key.Equals(BlobStorageMetadataKeys.SendersArchiveReference, StringComparison.OrdinalIgnoreCase) && m.Value.Equals(sender.SendersArchiveReference, StringComparison.OrdinalIgnoreCase)).Any());

                    if (at.Any())
                    {
                        sb.Append("<tr><td colspan='3'><label>Vedlegg til uttalelsen</label></td></tr>");
                        var attachmentString = string.Join("", at.Select(p => "<tr><td colspan='3'>" + p.FileName + "</td></tr>"));
                        sb.Append(attachmentString);
                    }

                    sb.Append("<tr><td colspan='3'><hr></td></tr></tbody>");
                }

                string htmlTemplate = _htmlUtils.GetHtmlFromTemplate(ReportRepliesHTML);
                htmlTemplate = htmlTemplate.Replace("<planId />", answer.PlanId);
                htmlTemplate = htmlTemplate.Replace("<planNavn />", answer.PlanNavn);
                htmlTemplate = htmlTemplate.Replace("<arkivReferanse />", arkivReferanse.ToUpper());
                htmlTemplate = htmlTemplate.Replace("<fristForInnspill />", answer.FristForInnspill.ToLocalTime().ToString("dd.MM.yyyy"));
                htmlTemplate = htmlTemplate.Replace("<svarFraBeroertPart />", sb.ToString());

                return htmlTemplate;
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Error occurred when getting report with replies from neighbours");

                throw;
            }
        }

    }
}
