﻿using System;

namespace FtB_Validation
{
    public class ValidationException : Exception
    {
        public string StatusCode { get; set; }

        public ValidationException(string statusCode, string message, Exception innerException) : base(message, innerException)
        {
            StatusCode = statusCode;
        }
    }
}