﻿using FtB_Validation.Models;
using FtB_Validation.Models.Legacy;
using System.Threading;
using System.Threading.Tasks;

namespace FtB_Validation
{
    public interface IValidationApiService
    {
        Task<LegacyValidationResponse> ValidateAsync(LegacyValidationInputData inputData, CancellationToken cancellationToken);
        Task<ValidationResponse> ValidateAsync(ValidationInputData inputData, CancellationToken cancellationToken);
        Task<ValidationResponse> ValidateReportAsync(ValidationInputData inputData, CancellationToken cancellationToken);
    }
}