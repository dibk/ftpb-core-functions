﻿using FtB_Validation.HttpClients;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace FtB_Validation
{
    public static class ValidationConfig
    {
        public static void AddValidation(this IServiceCollection services, Uri legacyValidationUri, Uri validationUri)
        {
            services.AddHttpClient<ValidateFormHttpClient>((c) => { c.BaseAddress = validationUri; } );
            services.AddHttpClient<ValidateFormReportHttpClient>((c) => { c.BaseAddress = validationUri; });
            services.AddHttpClient<LegacyValidationServiceHttpClient>((c) => { c.BaseAddress = legacyValidationUri; });
            services.AddScoped<IValidationApiService, ValidationApiService>();
        }
    }
}
