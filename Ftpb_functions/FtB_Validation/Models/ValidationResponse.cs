﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace FtB_Validation.Models
{
    [XmlRoot("ValidationResult")]
    public class ValidationResponse
    {
        public int Errors { get; set; }
        public int Warnings { get; set; }

        public List<string> tiltakstyperISoeknad { get; set; }

        public string Soknadtype { get; set; }

        [XmlArray("messages", IsNullable = false)]
        [XmlArrayItem("ValidationMessage")]
        public List<ValidationMessage> Messages { get; set; }
        public string GetValidationMessages()
        {
            if (Messages != null)
                return string.Join(" ", Messages.Select(x => x.Messagetype.ToString() + ": " + x.Message + ": " + x.Reference).ToList());
            else
                return string.Empty;
        }

        [XmlArray("rulesChecked", IsNullable = false)]
        [XmlArrayItem("ValidationRule")]
        public List<ValidationRule> RulesChecked { get; set; }

        [XmlArray("prefillChecklist", IsNullable = false)]
        [XmlArrayItem("ChecklistAnswer")]
        public List<ChecklistAnswer> PrefillChecklist { get; set; }
    }

    public class ValidationMessage
    {
        [XmlElement("rule")]
        public string Rule { get; set; }

        [XmlElement("reference")]
        public string Reference { get; set; }

        [XmlElement("message")]
        public string Message { get; set; }

        [XmlElement("xpathField")]
        public string XpathField { get; set; }

        [XmlElement("preCondition")]
        public string PreCondition { get; set; }

        [XmlElement("checklistReference")]
        public string ChecklistReference { get; set; }

        [XmlElement("messagetype")]
        public ValidationResultSeverityEnum Messagetype { get; set; }
    }

    public class ValidationRule
    {
        [XmlElement("id")]
        public string Id { get; set; }

        [XmlElement("rule")]
        public string Rule { get; set; }

        [XmlElement("message")]
        public string Message { get; set; }

        [XmlElement("xpathField")]
        public string XpathField { get; set; }

        [XmlElement("preCondition")]
        public string PreCondition { get; set; }

        [XmlElement("messagetype")]
        public ValidationResultSeverityEnum? Messagetype { get; set; }
    }

    public enum ValidationResultSeverityEnum
    {
        ERROR,
        WARNING,
        CRITICAL
    }

    public class ChecklistAnswer
    {
        [XmlElement("checklistReference")]
        public string ChecklistReference { get; set; }

        [XmlElement("checklistQuestion")]
        public string ChecklistQuestion { get; set; }

        [XmlElement("yesNo")]
        public bool YesNo { get; set; }

        [XmlElement("documentation")]
        public string Documentation { get; set; }

        [XmlArrayItem("validationRuleId")]
        [XmlArray("supportingDataValidationRuleId", IsNullable = false)]
        public List<string> SupportingDataValidationRuleId { get; set; }

        [XmlArrayItem("xpathField")]
        [XmlArray("supportingDataXpathField", IsNullable = false)]
        public List<string> SupportingDataXpathField { get; set; }
    }
}