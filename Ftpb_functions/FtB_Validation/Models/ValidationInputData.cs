﻿using System.Collections.Generic;

namespace FtB_Validation.Models
{
    public class ValidationInputData
    {
        public string AuthenticatedSubmitter { get; set; }

        /// <summary>
        /// The Form data. https://www.altinn.no/api/help
        /// </summary>
        public string FormData { get; set; }

        /// <summary>
        /// List of all subforms name used to validate required documentation. V2 enables more detailed validation of altinn rules for each attachement. Size, file extension and count of attachmenttypes.
        /// </summary>
        public List<SubformValidationInfo> SubForms { get; set; }

        /// <summary>
        /// List of all attachment types and forms/subforms name used to validate required documentation. V2 enables more detailed validation of altinn rules for each attachement. Size, file extension and count of attachmenttypes.
        /// </summary>
        public List<AttachmentValidationInfo> Attachments { get; set; }
    }

    public class SubformValidationInfo
    {
        /// <summary>
        /// Name is attachmentType for attachment and form name for form and subforms
        /// </summary>
        public string FormName { get; set; }

        /// <summary>
        /// filename with extension
        /// </summary>
        public string SubFormData { get; set; }
    }

    public class AttachmentValidationInfo
    {
        /// <summary>
        /// Name is attachmentType for attachment and form name for form and subforms
        /// </summary>
        public string AttachmentTypeName { get; set; }

        /// <summary>
        /// filename with extension
        /// </summary>
        public string Filename { get; set; }

        /// <summary>
        /// Filstørrelse i byte
        /// </summary>
        public int FileSize { get; set; }
    }
}