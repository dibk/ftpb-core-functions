﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace FtB_Validation.Models.Legacy
{
    [XmlRoot("ValidationResult")]
    public class LegacyValidationResponse
    {
        /// <summary>
        /// Number of errors
        /// </summary>
        public int Errors { get; set; }

        /// <summary>
        /// Number of warnings
        /// </summary>
        public int Warnings { get; set; }

        /// <summary>
        /// Liste over tiltakstyper i Søknad for å filtrere regler som ikke gjelder for tiltak
        /// </summary>
        public List<string> tiltakstyperISoeknad = new List<string>();

        public List<string> _checklistErrors = new List<string>();

        /// <summary>
        /// Type søknad for å bruke jf. sjekklistene for å hente Tiltakstype
        /// </summary>
        public string Soknadtype { get; set; }

        [XmlArray("messages", IsNullable = false)]
        [XmlArrayItem("ValidationMessage")]
        public List<ValidationMessage> Messages { get; set; }
        public string GetValidationMessages()
        {
            if (Messages != null)
                return string.Join(" ", Messages.Select(x => x.Messagetype + ": " + x.Message + ": " + x.Reference ).ToList());
            else
                return string.Empty;
        }

        [XmlArray("rulesChecked", IsNullable = false)]
        [XmlArrayItem("ValidationRule")]
        public List<ValidationRule> RulesChecked { get; set; }

        [XmlArray("prefillChecklist", IsNullable = true)]
        [XmlArrayItem("ChecklistAnswer")]
        public List<ChecklistAnswer> PrefillChecklist { get; set; }
    }

    public class ValidationMessage
    {
        [XmlElement("message")]
        public string Message;
        [XmlElement("messagetype")]
        public string Messagetype;
        [XmlElement("reference")]
        public string Reference;
        [XmlElement("xpathField")]
        public string XpathField;
    }

    public class ValidationRule
    {
        [XmlElement("id")]
        public string Id;
        [XmlElement("message")]
        public string Message;
        [XmlElement("messagetype")]
        public string Messagetype;
        [XmlElement("preCondition")]
        public string PreCondition;
        [XmlElement("checklistReference")]
        public string ChecklistReference;
        [XmlElement("xpathField")]
        public string XpathField;
    }
    public class ChecklistAnswer
    {
        [XmlElement("checklistReference")]
        public string ChecklistReference;
        [XmlElement("checklistQuestion")]
        public string ChecklistQuestion;
        [XmlElement("yesNo")]
        public bool YesNo;
        [XmlArrayItem("validationRuleId")]
        [XmlArray("supportingDataValidationRuleId")]
        public List<string> SupportingDataValidationRuleId;
        [XmlElement("supportingDataXpathField")]
        public string SupportingDataXpathField;
    }
}