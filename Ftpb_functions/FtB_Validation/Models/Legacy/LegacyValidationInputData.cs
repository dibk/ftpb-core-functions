﻿using System.Collections.Generic;

namespace FtB_Validation.Models.Legacy
{
    public class LegacyValidationInputData
    {
        /// <summary>
        /// DataFormatId of the Form. https://www.altinn.no/api/help
        /// </summary>
        public string DataFormatId { get; set; }

        /// <summary>
        /// DataFormatVersion of the Form. https://www.altinn.no/api/help
        /// </summary>
        public string DataFormatVersion { get; set; }

        /// <summary>
        /// The Form data. https://www.altinn.no/api/help
        /// </summary>
        public string FormData { get; set; }

        /// <summary>
        /// List of all attachment types and forms/subforms name used to validate required documentation. V2 enables more detailed validation of altinn rules for each attachement. Size, file extension and count of attachmenttypes.
        /// </summary>
        public List<LegacyAttachmentInfo> AttachmentTypesAndForms { get; set; }
    }

    public class LegacyAttachmentInfo
    {
        /// <summary>
        /// Name is attachmentType for attachment and form name for form and subforms
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// filename with extension
        /// </summary>
        public string Filename { get; set; }
        /// <summary>
        /// Filstørrelse i byte
        /// </summary>
        public int FileSize { get; set; }

    }
}