﻿using FtB_Validation.HttpClients;
using System.Threading;
using System.Threading.Tasks;

namespace FtB_Validation
{
    public class ValidationApiService : IValidationApiService
    {
        private readonly LegacyValidationServiceHttpClient _legacyValidationServiceHttpClient;
        private readonly ValidateFormHttpClient _validationHttpClient;
        private readonly ValidateFormReportHttpClient _validateFormReportHttpClient;

        public ValidationApiService(LegacyValidationServiceHttpClient legacyValidationServiceHttpClient,
                                    ValidateFormHttpClient validationHttpClient,
                                    ValidateFormReportHttpClient validateFormReportHttpClient)
        {
            _legacyValidationServiceHttpClient = legacyValidationServiceHttpClient;
            _validationHttpClient = validationHttpClient;
            _validateFormReportHttpClient = validateFormReportHttpClient;
        }

        public async Task<Models.ValidationResponse> ValidateAsync(Models.ValidationInputData inputData, CancellationToken cancellationToken)
        {
            return await _validationHttpClient.ValidateAsync(inputData, cancellationToken);
        }

        public async Task<Models.ValidationResponse> ValidateReportAsync(Models.ValidationInputData inputData, CancellationToken cancellationToken)
        {
            return await _validateFormReportHttpClient.ValidateAsync(inputData, cancellationToken);
        }

        public async Task<Models.Legacy.LegacyValidationResponse> ValidateAsync(Models.Legacy.LegacyValidationInputData inputData, CancellationToken cancellationToken)
        {
            return await _legacyValidationServiceHttpClient.ValidateAsync(inputData, cancellationToken);
        }
    }
}
