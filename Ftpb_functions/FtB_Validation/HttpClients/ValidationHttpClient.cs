﻿using FtB_Validation.Models;
using Microsoft.Extensions.Logging;
using System.Net.Http;

namespace FtB_Validation.HttpClients
{
    public class ValidateFormHttpClient : ValidationHttpClientBase<ValidationInputData, ValidationResponse>
    {
        public ValidateFormHttpClient(HttpClient httpClient,
                                    ILogger<ValidateFormHttpClient> logger) : base(
                                        httpClient,
                                        "api/validation",
                                        logger)
        {}
    }

    public class ValidateFormReportHttpClient : ValidationHttpClientBase<ValidationInputData, ValidationResponse>
    {
        public ValidateFormReportHttpClient(HttpClient httpClient,
                                    ILogger<ValidateFormHttpClient> logger) : base(
                                        httpClient,
                                        "api/validationReport",
                                        logger)
        { }
    }
}