﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FtB_Validation.HttpClients
{
    public class ValidationHttpClientBase<TInput, TOutput> 
        where TInput : class
        where TOutput : class
    {
        private readonly HttpClient _httpClient;
        private readonly string _requestUri;
        private readonly ILogger _logger;
        public ValidationHttpClientBase(HttpClient httpClient, string requestUri, ILogger logger)
        {
            _httpClient = httpClient;
            _requestUri = requestUri;
            _logger = logger;
        }

        public async Task<TOutput> ValidateAsync(TInput input, CancellationToken cancellationToken)
        {
            var content = JObject.FromObject(input).ToString();

            var request = new HttpRequestMessage(HttpMethod.Post, _requestUri)
            { Content = new StringContent(JObject.FromObject(input).ToString(), Encoding.UTF8, "application/json") };

            var response = await _httpClient.SendAsync(request, cancellationToken).ConfigureAwait(false);
            try
            {
                response.EnsureSuccessStatusCode();
                var jsonContent = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                return JsonConvert.DeserializeObject<TOutput>(jsonContent);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured when validating");
                throw new ValidationException(((int)response.StatusCode).ToString(), $"Error occured when validation '{_requestUri}'", ex);
            }
        }
    }
}
