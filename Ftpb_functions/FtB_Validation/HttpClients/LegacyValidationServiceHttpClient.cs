﻿using FtB_Validation.Models.Legacy;
using Microsoft.Extensions.Logging;
using System.Net.Http;

namespace FtB_Validation.HttpClients
{
    public class LegacyValidationServiceHttpClient : ValidationHttpClientBase<LegacyValidationInputData, LegacyValidationResponse>
    {
        public LegacyValidationServiceHttpClient(HttpClient httpClient,
                                                 ILogger<LegacyValidationServiceHttpClient> logger) : base(
                                                     httpClient,
                                                     "api/validatev2/form",
                                                     logger)
        {}
    }
}
