using FtB_Common.Enums;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace FtB_Tests
{
    [TestClass]
    public class BlobStorageTests
    {
        IEnumerable<MyBlobItem> _myBlobItems;
        public BlobStorageTests()
        {
            _myBlobItems = new List<MyBlobItem>() {
                    new MyBlobItem()
                    {
                        FileName = "TEST-file.xml",
                        Metadata = new List<KeyValuePair<string, string>>() {
                            new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.FormData),
                            new KeyValuePair<string, string>(BlobStorageMetadataKeys.PublicBlobContainerName, "12345678901234567")
                        }
                    },
                                new MyBlobItem()
                    {
                        FileName = "TEST-file.pdf",
                        Metadata = new List<KeyValuePair<string, string>>() {
                            new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.MainForm)
                        }
                    }
                };
        }
        [TestMethod]
        public void MetadataComparerTest_WhereWithIntersectAndSimpleFilter()
        {
            var filterMetadata = new List<KeyValuePair<string, string>>() {
                new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.FormData)};

            var foundBlobMetadata = _myBlobItems.Where(b => b.Metadata.Intersect(filterMetadata, new MetadataComparer()).Any());

            Assert.IsTrue(foundBlobMetadata.Count() == 1);
        }

        [TestMethod]
        public void MetadataComparerTest_WhereWithIntersect_MultipleFilterValues()
        {
            var filterMetadata = new List<KeyValuePair<string, string>>() {
                new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.FormData),
            new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.MainForm)};

            var foundBlobMetadata = _myBlobItems.Where(b => b.Metadata.Intersect(filterMetadata, new MetadataComparer()).Any());

            Assert.IsTrue(foundBlobMetadata.Count() == 2);
        }

        [TestMethod]
        public void MetadataComparerTest_WhereWithIntersect_NoResult()
        {
            var filterMetadata = new List<KeyValuePair<string, string>>() {
                new KeyValuePair<string, string>("epyT", "DataForm") };

            var foundBlobMetadata = _myBlobItems.Where(b => b.Metadata.Intersect(filterMetadata, new MetadataComparer()).Any());

            Assert.IsTrue(foundBlobMetadata.Count() == 0);
        }

        [TestMethod]
        public void MetadataComparerTest_WhereWithIntersectLoweCaseValuesInFilter_NoResult()
        {
            var filterMetadata = new List<KeyValuePair<string, string>>() {
                new KeyValuePair<string, string>("type", "formdata") };

            var foundBlobMetadata = _myBlobItems.Where(b => b.Metadata.Intersect(filterMetadata, new MetadataComparer()).Any());

            Assert.IsTrue(foundBlobMetadata.Count() == 1);
        }

        [TestMethod]
        public void MetadataComparerTest_Equals_AreEqual()
        {
            var kv1 = new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, "formdata");
            var kv2 = new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, "FormData");

            var areEqual = new MetadataComparer().Equals(kv1, kv2);

            Assert.IsTrue(areEqual);
        }
        [TestMethod]
        public void MetadataComparerTest_Equals_AreNotEqual()
        {
            var kv1 = new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, "data");
            var kv2 = new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, "FormData");

            var areEqual = new MetadataComparer().Equals(kv1, kv2);

            Assert.IsFalse(areEqual);
        }

        internal class MyBlobItem
        {
            public IEnumerable<KeyValuePair<string, string>> Metadata { get; set; }
            public string FileName { get; set; }
        }
    }
}
