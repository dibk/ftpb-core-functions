using Altinn.Common;
using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using FluentAssertions;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Storage.TableStorage;
using FtB_Common.Utils;
using FtB_FormLogic;
using FtB_Print.Services.Print;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using KontaktOgReservasjonsregisteret;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using no.kxml.skjema.dibk.nabovarselPlan;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Xunit;

namespace FtB_Tests.VorpaDistributionTest
{
    public class VorpaDistributionTest
    {
        private readonly IConfiguration _configuration;
        private readonly ILoggerFactory _loggerFactory;

        public VorpaDistributionTest()
        {
            var configurationRoot = new ConfigurationBuilder()
            .AddJsonFile("local.settings.json", optional: true, reloadOnChange: true)
            .AddEnvironmentVariables()
            .Build();
            _configuration = configurationRoot.GetSection("Values");

            _loggerFactory = (ILoggerFactory)new LoggerFactory();
        }

        [Fact]
        public async Task Distribute_VarselOmOppstartAvReguleringplanarbeid()
        {
            string arkode = "ar" + DateTime.Now.ToString("yyMMddHHmmss");
            var submittalQueueItem = new SubmittalQueueItem() { ArchiveReference = arkode };

            var logTableStorage = _loggerFactory.CreateLogger<TableStorageService>();
            var logVarselOppstartPlanarbeidPrepareLogic = _loggerFactory.CreateLogger<VarselOppstartPlanarbeidPrepareLogic>();

            var repoMock = new Mock<IFormDataRepo>();
            var blobOperationsMock = new Mock<IBlobOperations>();
            var dbUnitOfWorkMock = new Mock<IDbUnitOfWork>();
            var fileDownloadStatusClientMock = new Mock<IFileDownloadStatusHttpClient>().Object;

            repoMock.Setup(s => s.GetFormData(arkode).Result).Returns(File.ReadAllText("Formdata_AR02037098_6325_44842.xml"));

            blobOperationsMock.Setup(s => s.GetReporteeIdFromStoredBlobAsync(It.IsAny<string>()).Result).Returns("910467905");

            dbUnitOfWorkMock.Setup(d => d.DistributionForms.Get(It.IsAny<string>()).Result).Returns(new Ftb_DbModels.DistributionForm() { InitialArchiveReference = arkode });

            IOptions<EncryptionSettings> options = Options.Create<EncryptionSettings>(new EncryptionSettings());
            options.Value.CertificateThumbprint = "8901079e90f724ca967ecf3dfc6a164748e1660c";

            var decryptor = new Decryption(options);
            var decryptionFactory = new DecryptionFactory(new List<IDecryption>() { decryptor });

            var distributionReceiverRepo = new Mock<DistributionReceiverRepo>().Object;
            var distributionReceiverLogRepo = new Mock<DistributionReceiverLogRepo>().Object;
            var distributionSubmittalRepo = new Mock<DistributionSubmittalRepo>().Object;

            var logic = new VarselOppstartPlanarbeidPrepareLogic(repoMock.Object,
                                                                 logVarselOppstartPlanarbeidPrepareLogic,
                                                                 dbUnitOfWorkMock.Object,
                                                                 decryptionFactory,
                                                                 fileDownloadStatusClientMock,
                                                                 blobOperationsMock.Object,
                                                                 Mock.Of<IPrintService>(),
                                                                 Mock.Of<KRRClient>(),
                                                                 distributionReceiverRepo,
                                                                 distributionReceiverLogRepo,
                                                                 distributionSubmittalRepo);

            logic.FormData = LoadDistributionFormData();

            await logic.SetReceiversAsync();
            var sendQueueItems = await logic.ExecuteAsync(submittalQueueItem);
            sendQueueItems.Should().HaveCount(3);

            var reportQueueItems = new List<ReportQueueItem>();
            int index = 0;
            foreach (var sendQueueItem in sendQueueItems)
            {
                var reportItem = await SendFunction(sendQueueItem);
                reportQueueItems.Add(reportItem);
                var receiverEntity = await distributionReceiverRepo.GetAsync(arkode, index.ToString());
                receiverEntity.ProcessStage.Should().Be(FtB_Common.Enums.DistributionReceiverProcessStageEnum.Distributed);
            }
        }

        private async Task<ReportQueueItem> SendFunction(SendQueueItem sendQueueItem)
        {
            var logTableStorage = _loggerFactory.CreateLogger<TableStorageService>();
            var logVarselOppstartPlanarbeidSendLogic = _loggerFactory.CreateLogger<VarselOppstartPlanarbeidSendLogic>();
            var logHtmlUtils = _loggerFactory.CreateLogger<HtmlUtils>();
            var logVarselOppstartPlanarbeidPrefillMapper = _loggerFactory.CreateLogger<VarselOppstartPlanarbeidPrefillMapper>();

            string arkode = sendQueueItem.ArchiveReference;

            var repoMock = new Mock<IFormDataRepo>();
            var blobOperationsMock = new Mock<IBlobOperations>();
            var dbUnitOfWorkMock = new Mock<IDbUnitOfWork>();
            var distributionAdapterMock = new Mock<IDistributionAdapter>();

            repoMock.Setup(s => s.GetFormData(arkode).Result).Returns(File.ReadAllText("Formdata_AR02037098_6325_44842.xml"));

            blobOperationsMock.Setup(s => s.GetReporteeIdFromStoredBlobAsync(It.IsAny<string>()).Result).Returns("910467905");

            var distributionFormId = Guid.NewGuid();
            dbUnitOfWorkMock.Setup(d => d.DistributionForms.Get(It.IsAny<Guid>()).Result)
                            .Returns(new Ftb_DbModels.DistributionForm() { InitialArchiveReference = arkode, Id = distributionFormId });
            dbUnitOfWorkMock.Setup(d => d.LogEntries.AddInfo(""));
            dbUnitOfWorkMock.Setup(d => d.SaveDistributionForms().Result).Returns(true);

            AltinnPrefilledDistributionResult dr = new AltinnPrefilledDistributionResult()
            {
                PrefillResult = new AltinnPrefillResult() { PrefillAltinnReceiptId = "99", PrefillReferenceId = "999", PrefillAltinnReceivedTime = DateTime.Now },
                CorrespondenceResult = new AltinnCorrespondenceResult()
            };

            distributionAdapterMock.Setup(s => s.SendPrefillAndCorrespondenceAsync(It.IsAny<AltinnDistributionMessage>()).Result)
                                    .Returns(dr);

            IOptions<EncryptionSettings> options = Options.Create<EncryptionSettings>(new EncryptionSettings());
            options.Value.CertificateThumbprint = "8901079e90f724ca967ecf3dfc6a164748e1660c";

            var decryptor = new Decryption(options);
            var decryptionFactory = new DecryptionFactory(new List<IDecryption>() { decryptor });

            var htmlUtilsOptions = Options.Create(new HtmlUtilSettings() { HtmlTemplateAssembly = "FtB_FormLogic" });
            HtmlUtils htmlUtils = new HtmlUtils(htmlUtilsOptions, logHtmlUtils);

            IDistributionDataMapper<NabovarselPlanType> distributionDataMapper = new VarselOppstartPlanarbeidSendDataProvider(htmlUtils, decryptionFactory);

            VarselOppstartPlanarbeidPrefillMapper varselOppstartPlanarbeidPrefillMapper = new VarselOppstartPlanarbeidPrefillMapper(decryptionFactory, logVarselOppstartPlanarbeidPrefillMapper);

            var fileDownloadStatusClient = new Mock<IFileDownloadStatusHttpClient>().Object;

            var distributionReceiverRepo = new Mock<DistributionReceiverRepo>().Object;
            var distributionReceiverLogRepo = new Mock<DistributionReceiverLogRepo>().Object;

            var logic = new VarselOppstartPlanarbeidSendLogic(
                repoMock.Object,
                blobOperationsMock.Object,
                logVarselOppstartPlanarbeidSendLogic,
                distributionAdapterMock.Object,
                distributionDataMapper,
                varselOppstartPlanarbeidPrefillMapper,
                dbUnitOfWorkMock.Object,
                decryptionFactory,
                fileDownloadStatusClient,
                Mock.Of<IPrintService>(),
                distributionReceiverRepo,
                distributionReceiverLogRepo
            );

            logic.FormData = LoadDistributionFormData();

            await logic.SetReceiversAsync();
            var reportQueueItem = await logic.ExecuteAsync(sendQueueItem);

            return reportQueueItem;
        }

        private no.kxml.skjema.dibk.nabovarselPlan.NabovarselPlanType LoadDistributionFormData()
        {
            var retval = new no.kxml.skjema.dibk.nabovarselPlan.NabovarselPlanType();

            var fileContent = File.ReadAllText("nabovarselPlan_med_plankonsulent.xml");

            retval = SerializeUtil.DeserializeXml<no.kxml.skjema.dibk.nabovarselPlan.NabovarselPlanType>(fileContent);

            return retval;
        }
    }
}