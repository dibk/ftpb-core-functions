﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using FtB_Print.Extensions;
using Newtonsoft.Json;
using no.kxml.skjema.dibk.nabovarselPlan;

namespace FtB_Tests.Vorpa
{
    public class VorpaBerortPartPersonGenerator
    {
        public static (List<BeroertPartType>, List<string>) GenerateBeroerteParter(int numberOf)
        {
            var usedFnrs = new List<string>();

            var retVal = new List<BeroertPartType>();
            var personer = GetFnr();
            var navnOffset = 0;
            var personIndex = 0;

            var loopCount = 1;

            for (int i = 0; i < numberOf; i++)
            {
                if (i > personer.Count * loopCount - 1 - navnOffset)
                {
                    personIndex = 0;
                    navnOffset++;
                    loopCount++;
                }

                var personEncryptedFnr = personer[personIndex].EncryptedSsn;
                var personNavn = personer[personIndex].Name;
                var adresse = personer[personIndex].Address;
                usedFnrs.Add(personer[personIndex].EncryptedSsn);

                var neigbour = new BeroertPartType()
                {
                    partstype = new KodeType()
                    {
                        kodebeskrivelse = "Privatperson",
                        kodeverdi = "Privatperson"
                    },
                    navn = personNavn,
                    foedselsnummer = personEncryptedFnr,

                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = adresse,
                        postnr = "3502",
                        poststed = "Hønefoss",
                        landkode = "no"
                    },
                    telefon = "12312399",
                    epost = "privat@person.test",
                    systemReferanse = $"ref-{i}",
                    gjelderEiendom = new[]
                    {
                        new GjelderEiendomType()
                        {
                            eiendomsidentifikasjon = new MatrikkelnummerType()
                            {
                                kommunenummer = "5001",
                                gaardsnummer = $"11{i}",
                                bruksnummer = $"1{i}",
                                festenummer = "0",
                                seksjonsnummer = "0"

                            },
                            adresse = new EiendommensAdresseType()
                            {
                                adresselinje1 = $"Storgata {i}",
                                postnr = "3502",
                                poststed = "Hønefoss"
                            }
                        }
                    }
                };
                retVal.Add(neigbour);
                personIndex++;
            }

            return (retVal, usedFnrs.Distinct().ToList());
        }

        private static List<SyntetiskPerson> GetFnr()
        {
            var r = Assembly.GetExecutingAssembly().GetResourceStream("encryptedfnr.json");

            using var sr = new StreamReader(r);
            var result = sr.ReadToEnd();

            return JsonConvert.DeserializeObject<List<SyntetiskPerson>>(result);
        }
    }
}