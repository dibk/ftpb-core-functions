using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Entities;
using FtB_Common.Encryption;
using FtB_DataModels.Mappers;
using FtB_FormLogic;
using FtB_Print.Extensions;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Xunit;
using Xunit.Abstractions;

namespace FtB_Tests.Vorpa
{
    public class ReceiptTests
    {
        private readonly ITestOutputHelper _output;
        private readonly int _antallPersoner = 1000;
        private readonly int _antallForetak = 50;
        private readonly string _encryptionThumbprint = "8901079e90f724ca967ecf3dfc6a164748e1660c";

        public ReceiptTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void GenerateListForReceipt_performanceTest()
        {
            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            var orgs = VorpaBerortPartOrganisasjonGenerator.GenerateBeroerteParter(_antallForetak);
            var personer = VorpaBerortPartPersonGenerator.GenerateBeroerteParter(_antallPersoner);

            var mockVorpaForm = VorpaGenerator.Generate(orgs.Item1, personer.Item1);

            var someOptions = Options.Create(new EncryptionSettings());
            someOptions.Value.CertificateThumbprint = _encryptionThumbprint;

            var decrypter = new Decryption(someOptions);

            var distribuerteOrgnr = orgs.Item2;
            var distribuertePersoner = personer.Item2;

            distribuerteOrgnr.AddRange(distribuertePersoner);

            var input = distribuerteOrgnr.Select(p => new DistributionReceiverDto() { ReceiverId = p });
            _output.WriteLine($"Generert kildedata {sw.ElapsedMilliseconds}ms");
            sw.Restart();
            var groupsStep = VarselOppstartPlanarbeidReportLogic.GetGroupsOfBeroerteParterWithDecryptedIDs(mockVorpaForm.beroerteParter, input, (ssn) => decrypter.DecryptText(ssn));

            var toUniqueBeroertPartTypeWithEiendoms = NabovarselPlanMappers.GetListOfBeroertPartWithEiendomFromGroups(groupsStep);

            _output.WriteLine($"Sekvensiell - BerÝrteparter-liste {sw.ElapsedMilliseconds}ms");
            sw.Restart();

            var groupsStepParallel = VarselOppstartPlanarbeidReportLogic.GetGroupsOfBeroerteParterWithDecryptedIDsParallel(mockVorpaForm.beroerteParter, input, (ssn) => decrypter.DecryptText(ssn));

            var toUniqueBeroertPartTypeWithEiendoms2 = NabovarselPlanMappers.GetListOfBeroertPartWithEiendomFromGroups(groupsStepParallel);
            _output.WriteLine($"Parallel - BerÝrteparter-liste {sw.ElapsedMilliseconds}ms");
            sw.Restart();

            _output.WriteLine($"Html generering {sw.ElapsedMilliseconds}ms");

            Assert.True(toUniqueBeroertPartTypeWithEiendoms.Count() <= mockVorpaForm.beroerteParter.Count());
        }

        [Fact]
        public void GenerateListForReceiptOld_performanceTest()
        {
            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            var orgs = VorpaBerortPartOrganisasjonGenerator.GenerateBeroerteParter(_antallForetak);
            var personer = VorpaBerortPartPersonGenerator.GenerateBeroerteParter(_antallPersoner);

            var mockVorpaForm = VorpaGenerator.Generate(orgs.Item1, personer.Item1);

            var someOptions = Options.Create(new EncryptionSettings());
            someOptions.Value.CertificateThumbprint = _encryptionThumbprint;

            var decrypter = new Decryption(someOptions);

            var distribuerteOrgnr = orgs.Item2;
            var distribuertePersoner = personer.Item2;

            distribuerteOrgnr.AddRange(distribuertePersoner);
            var input = distribuerteOrgnr.Select(p => new DistributionReceiverEntity() { ReceiverId = p });

            _output.WriteLine($"Generert kildedata {sw.ElapsedMilliseconds}ms");
            sw.Restart();

            var listOfBeroertPartWithEiendom = new List<no.kxml.skjema.dibk.nabovarselPlan.BeroertPartType>();
            foreach (var item in input)
            {
                listOfBeroertPartWithEiendom.Add(mockVorpaForm.ToUniqueBeroertPartTypeWithPopulatedGjeldendeEiendom2(item.ReceiverId, decryptID => decrypter.DecryptText(decryptID)));
            }
            _output.WriteLine($"BerÝrteparter-liste {sw.ElapsedMilliseconds}ms");

            sw.Restart();
            _output.WriteLine($"Html generering {sw.ElapsedMilliseconds}ms");

            Assert.True(listOfBeroertPartWithEiendom.Count() <= mockVorpaForm.beroerteParter.Count());
        }

        [Fact]
        public void GenerateListForReceipt()
        {
            var orgs = VorpaBerortPartOrganisasjonGenerator.GenerateBeroerteParter(2);
            var personer = VorpaBerortPartPersonGenerator.GenerateBeroerteParter(2);

            var mockVorpaForm = VorpaGenerator.Generate(orgs.Item1, personer.Item1);

            var someOptions = Options.Create(new EncryptionSettings());
            someOptions.Value.CertificateThumbprint = _encryptionThumbprint;

            var decrypter = new Decryption(someOptions);

            var distribusjoner = orgs.Item2;
            distribusjoner.AddRange(personer.Item2);
            distribusjoner.RemoveAt(distribusjoner.Count - 1);

            var successfullyNotified = distribusjoner.Select(p => new DistributionReceiverDto() { ReceiverId = p });

            var groupsStep = VarselOppstartPlanarbeidReportLogic.GetGroupsOfBeroerteParterWithDecryptedIDsParallel(mockVorpaForm.beroerteParter, successfullyNotified, (ssn) => decrypter.DecryptText(ssn));
            Assert.True(groupsStep.Count() == 3);

            var toUniqueBeroertPartTypeWithEiendoms = NabovarselPlanMappers.GetListOfBeroertPartWithEiendomFromGroups(groupsStep);

            Assert.True(toUniqueBeroertPartTypeWithEiendoms.Count() <= mockVorpaForm.beroerteParter.Count());
        }

        [Fact]
        public void GenerateListForReceipt_forExampleData()
        {
            var r = Assembly.GetExecutingAssembly().GetResourceStream("ReceiptTestData-1.xml");

            var mockVorpaForm = FtB_Common.Utils.SerializeUtil.DeserializeXML<no.kxml.skjema.dibk.nabovarselPlan.NabovarselPlanType>(r);

            var someOptions = Options.Create(new EncryptionSettings());
            someOptions.Value.CertificateThumbprint = _encryptionThumbprint;

            var decrypter = new Decryption(someOptions);

            var distribusjoner = mockVorpaForm.beroerteParter.Where(p => p.partstype.kodeverdi.Equals("Privatperson")).Select(p => p.foedselsnummer).ToList();
            distribusjoner.AddRange(mockVorpaForm.beroerteParter.Where(p => p.partstype.kodeverdi.Equals("Foretak")).Select(p => p.organisasjonsnummer).ToList());

            var successfullyNotified = distribusjoner.Select(p => new DistributionReceiverDto() { ReceiverId = p });

            var groupsStep = VarselOppstartPlanarbeidReportLogic.GetGroupsOfBeroerteParterWithDecryptedIDsParallel(mockVorpaForm.beroerteParter, successfullyNotified, (ssn) => decrypter.DecryptText(ssn));

            var toUniqueBeroertPartTypeWithEiendoms = NabovarselPlanMappers.GetListOfBeroertPartWithEiendomFromGroups(groupsStep);

            Assert.True(toUniqueBeroertPartTypeWithEiendoms.Count() <= mockVorpaForm.beroerteParter.Count());
        }
    }

    public static class Testegreier
    {
        public static no.kxml.skjema.dibk.nabovarselPlan.BeroertPartType ToUniqueBeroertPartTypeWithPopulatedGjeldendeEiendom2(this no.kxml.skjema.dibk.nabovarselPlan.NabovarselPlanType nabovarsel, string beroertPartId, Func<string, string> idDecryptor)
        {
            var decryptedReceiverId = idDecryptor(beroertPartId);
            var berortParter = nabovarsel.beroerteParter?.Where(b => (
                                                                              !string.IsNullOrEmpty(b.foedselsnummer) && idDecryptor(b.foedselsnummer).Equals(decryptedReceiverId))
                                                                              || (!string.IsNullOrEmpty(b.organisasjonsnummer) && b.organisasjonsnummer.Equals(decryptedReceiverId))
                                                                      ).ToList();

            var beroertPartType = berortParter.FirstOrDefault();

            List<no.kxml.skjema.dibk.nabovarselPlan.GjelderEiendomType> gjelderEiendom = new List<no.kxml.skjema.dibk.nabovarselPlan.GjelderEiendomType>();

            foreach (var beroertPart in berortParter)
            {
                foreach (var eiendom in beroertPart.gjelderEiendom)
                {
                    if (!gjelderEiendom.Contains(eiendom))
                    {
                        gjelderEiendom.Add(eiendom);
                    }
                }
            }
            var gjelderEiendomArray = gjelderEiendom.ToArray();
            beroertPartType.gjelderEiendom = gjelderEiendomArray;

            return beroertPartType;
        }
    }
}