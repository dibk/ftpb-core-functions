﻿using FtB_Common.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FtB_Tests
{
    [TestClass]
    public class StringUtilsTests
    {
        [TestMethod]
        public void MakeSimpleStringJsonCompatible()
        {
            var s = "I'm	a 		test string";

            var result = StringUtils.RemoveInvalidJsonCharacters(s);

            Assert.AreNotEqual(s, result);
            Assert.IsFalse(result.Contains("\t"));
        }

        [TestMethod]
        public void MakeStringJsonCompatible()
        {
            var s = System.IO.File.ReadAllText("HtmlForStringUtilsTestFile.txt");

            var result = StringUtils.RemoveInvalidJsonCharacters(s);

            Assert.AreNotEqual(s, result);
            Assert.IsFalse(result.Contains("\t"));
        }
    }
}
