﻿using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Utils;
using FtB_FormLogic;
using FtB_FormLogic.Shipments.ShipmentFormLogic.SuppleringAvSoeknad;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtB_Tests.SuppleringAvSoeknad
{
    [TestClass]
    public class SuppleringAvSoeknadTests
    {
        [TestMethod]
        public async Task Test()
        {
            var skjema = new no.kxml.skjema.dibk.suppleringAvSoknad.SuppleringAvSoeknadType()
            {
                dataFormatId = "5689",
                dataFormatVersion = "42350",
                ansvarligSoeker = new no.kxml.skjema.dibk.suppleringAvSoknad.PartType()
                {
                    adresse = new no.kxml.skjema.dibk.suppleringAvSoknad.EnkelAdresseType() { adresselinje1 = "Kleppesveiven 667" },
                    epost = "håkken-e@du.ar",
                    organisasjonsnummer = "90188247",
                    navn = "Hågåberja Knekk og avis ENK",
                    partstype = new no.kxml.skjema.dibk.suppleringAvSoknad.KodeType()
                    {
                        kodebeskrivelse = "Foretak",
                        kodeverdi = "Foretak"
                    },
                    kontaktperson = new no.kxml.skjema.dibk.suppleringAvSoknad.KontaktpersonType()
                    {
                        navn = "Gunnulv Ælva",
                        epost = "harkje@slikt.no"
                    }
                },
            };


            var formDataRepo = new Moq.Mock<IFormDataRepo>();
            formDataRepo.Setup(s => s.GetFormData(It.IsAny<string>())).Returns(Task.FromResult(SerializeUtil.Serialize(skjema)));

            var tableRepo = new Mock<ITableStorage>();
            var blobOperations = new Mock<IBlobOperations>();
            var svarUtAdapter = new Mock<ISvarUtAdapter>();
            var dbUnitOfWork = new Mock<IDbUnitOfWork>();
            var municipalityService = new Mock<MunicipalityService>();
            var de = new Mock<IDecryption>();            
            de.Setup(s => s.DecryptText(It.IsAny<string>())).Returns("123456789");
            List<IDecryption> decryptions = new List<IDecryption>() { de.Object };
            var decryptionFactory = new DecryptionFactory(decryptions);
            var fileDownloadStatus = new Mock<IFileDownloadStatusHttpClient>();
            var distributionReceiverRepo = new Mock<DistributionReceiverRepo>().Object;
            var distributionReceiverLogRepo = new Mock<DistributionReceiverLogRepo>().Object;

            var formLogic = new SuppleringAvSoeknadPrepareLogic(formDataRepo.Object,
                                                                NullLogger<SuppleringAvSoeknadPrepareLogic>.Instance,
                                                                blobOperations.Object,
                                                                dbUnitOfWork.Object,
                                                                decryptionFactory,
                                                                fileDownloadStatus.Object,
                                                                municipalityService.Object,
                                                                distributionReceiverRepo,
                                                                distributionReceiverLogRepo
                                                                );


            await formLogic.LoadDataAsync("ar1234567");

            var result = await formLogic.ExecuteAsync(new SubmittalQueueItem() { ArchiveReference = "123456789" });

            Assert.IsNotNull(result);
            var sendQueueItem = result.ToList()[0];
            Assert.IsTrue(sendQueueItem.ArchiveReference == "123456789");
            Assert.IsTrue(sendQueueItem.Receiver.Name.Equals("BANKE KOMMUNE"));
            Assert.IsTrue(sendQueueItem.Sender.Name.Equals("Hågåberja Knekk og avis ENK"));
        }
    }
}
