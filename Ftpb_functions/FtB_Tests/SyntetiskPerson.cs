﻿namespace FtB_Tests
{
    public class SyntetiskPerson
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Ssn { get; set; }
        public string EncryptedSsn { get; set; }
    }
}