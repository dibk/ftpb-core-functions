﻿using FtB_ProcessStrategies.Processors.AccumulatedReportProcessor;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace FtB_Tests
{
    [TestClass]
    public class AttachmentTypeNameComparerTests
    {
        private List<string> _valuesToBeOrdered = new List<string>()
            {
                "Annet",
                "SvarNabovarselPlan",
                "Planinitiativ"
            };

        [TestMethod]
        public void OrderListOfValues()
        {
            var ordererd = _valuesToBeOrdered.OrderBy(s => s, new AttachmentTypeNameComparer());

            Assert.AreNotEqual(_valuesToBeOrdered, ordererd.ToList());
        }

        [TestMethod]
        public void OrderListOfValuesReversed_Ok()
        {
          var ordererd = _valuesToBeOrdered.OrderByDescending(s => s, new AttachmentTypeNameComparer());

            Assert.AreNotEqual(_valuesToBeOrdered, ordererd.ToList());
        }
    }
}
