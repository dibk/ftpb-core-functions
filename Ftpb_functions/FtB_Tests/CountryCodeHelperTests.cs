﻿using FtB_Print.Mapping;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FtB_Tests
{
    [TestClass]
    public class CountryCodeHelperTests
    {
        [TestMethod]
        public void GetAdjustedPostalCode_NorwegianIsoCountryCode()
        {
            var result = CountryCodeHandler.GetAdjustedPostalCode(postalCode: "3802", countryCode: "NO");
            Assert.AreEqual("3802", result);
        }

        [TestMethod]
        public void GetAdjustedPostalCode_NorwegianSSBCountryCode()
        {
            var result = CountryCodeHandler.GetAdjustedPostalCode(postalCode: "3802", countryCode: "000");
            Assert.AreEqual("3802", result);
        }

        [TestMethod]
        public void GetAdjustedPostalCode_ForeignIsoCountryCode()
        {
            var result = CountryCodeHandler.GetAdjustedPostalCode(postalCode: "201512", countryCode: "SE");
            Assert.AreEqual("SE-201512", result);
        }

        [TestMethod]
        public void GetAdjustedPostalCode_ForeignSSBCountryCode()
        {
            var result = CountryCodeHandler.GetAdjustedPostalCode(postalCode: "201512", countryCode: "106");
            Assert.AreEqual("SE-201512", result);
        }

        [TestMethod]
        public void GetAdjustedPostalCode_ZeroAsCountryCode()
        {
            var result = CountryCodeHandler.GetAdjustedPostalCode(postalCode: "3802", countryCode: "0");
            Assert.AreEqual("3802", result);
        }

        [TestMethod]
        public void GetAdjustedPostalCode_NullAsCountryCode()
        {
            var result = CountryCodeHandler.GetAdjustedPostalCode(postalCode: "3802", countryCode: null);
            Assert.AreEqual("3802", result);
        }

        [TestMethod]
        public void GetAdjustedPostalCode_InvalidValueAsCountryCode()
        {
            var result = CountryCodeHandler.GetAdjustedPostalCode(postalCode: "3802", countryCode: "InvalidCountryCode");
            Assert.AreEqual("3802", result);
        }

        [TestMethod]
        public void GetCountryName_Iso_Norway_returnsNorge()
        {
            var result = CountryCodeHandler.GetCountryName("NO");
            Assert.AreEqual("Norge", result);
        }

        [TestMethod]
        public void GetCountryName_SSB_Norway_returnsNorge()
        {
            var result = CountryCodeHandler.GetCountryName("000");
            Assert.AreEqual("Norge", result);
        }

        [TestMethod]
        public void GetCountryName_Zero_returnsNorge()
        {
            var result = CountryCodeHandler.GetCountryName("0");
            Assert.AreEqual("Norge", result);
        }

        [TestMethod]
        public void GetCountryName_ISO_Sweden_returnsSverige()
        {
            var result = CountryCodeHandler.GetCountryName("SE");
            Assert.AreEqual("Sverige", result);
        }

        [TestMethod]
        public void GetCountryName_SSB_Sweden_returnsSverige()
        {
            var result = CountryCodeHandler.GetCountryName("106");
            Assert.AreEqual("Sverige", result);
        }
    }
}