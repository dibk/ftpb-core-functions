﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using FtB_Print.Extensions;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace FtB_Tests.Vorpah
{
    public class VorpahBerortPartPersonGenerator
    {
        public static (List<Beroertpart>, List<string>) GenerateBeroerteParter(int numberOf)
        {
            var usedFnrs = new List<string>();

            var retVal = new List<Beroertpart>();
            var personer = GetFnr();
            var navnOffset = 0;
            var personIndex = 0;

            var loopCount = 1;

            for (int i = 0; i < numberOf; i++)
            {
                if (i > personer.Count * loopCount - 1 - navnOffset)
                {
                    personIndex = 0;
                    navnOffset++;
                    loopCount++;
                }

                var personEncryptedFnr = personer[personIndex].EncryptedSsn;
                var personNavn = personer[personIndex].Name;
                var adresse = personer[personIndex].Address;
                usedFnrs.Add(personer[personIndex].EncryptedSsn);

                var neigbour = new Beroertpart
                {
                    Partstype = new Kodeliste
                    {
                        Kodebeskrivelse = "Privatperson",
                        Kodeverdi = "Privatperson"
                    },
                    Navn = personNavn,
                    Foedselsnummer = personEncryptedFnr,

                    Adresse = new EnkelAdresse
                    {
                        Adresselinje1 = adresse,
                        Postnr = "3502",
                        Poststed = "Hønefoss",
                        Landkode = "no"
                    },
                    Telefon = "12312399",
                    Epost = "privat@person.test",
                    Systemreferanse = $"ref-{i}",
                    GjelderEiendom = new[]
                    {
                        new Eiendom
                        {
                            Eiendomsidentifikasjon = new Eiendomsidentifikasjon()
                            {
                                Kommunenummer = "5001",
                                Gaardsnummer = $"11{i}",
                                Bruksnummer = $"1{i}",
                                Festenummer = "0",
                                Seksjonsnummer = "0"

                            },
                            Adresse = new Adresse()
                            {
                                Adresselinje1 = $"Storgata {i}",
                                Postnr = "3502",
                                Poststed = "Hønefoss"
                            }
                        }
                    }
                };
                retVal.Add(neigbour);
                personIndex++;
            }

            return (retVal, usedFnrs.Distinct().ToList());
        }

        private static List<SyntetiskPerson> GetFnr()
        {
            var r = Assembly.GetExecutingAssembly().GetResourceStream("encryptedfnr.json");

            using var sr = new StreamReader(r);
            var result = sr.ReadToEnd();

            return JsonConvert.DeserializeObject<List<SyntetiskPerson>>(result);
        }
    }
}