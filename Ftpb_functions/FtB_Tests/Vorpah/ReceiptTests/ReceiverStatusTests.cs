﻿using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_FormLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace FtB_Tests.Vorpah.ReceiptTests
{
    public class ReceiverStatusTests
    {
        [Fact]
        public void GetListOfFailedReceivers_3_failed()
        {
            var tableStorageRows = new List<DistributionReceiverDto>() {
                new DistributionReceiverDto()
                {
                    ReceiverId = "RECEIVERID1",
                    ProcessOutcome = FtB_Common.Enums.ReceiverProcessOutcomeEnum.Sent,
                    ReceiverName = "Testperson 1 sent",
                    ProcessStage = FtB_Common.Enums.DistributionReceiverProcessStageEnum.ReadyForReporting
                },
                new DistributionReceiverDto()
                {
                    ReceiverId = "RECEIVERID2",
                    ProcessOutcome = FtB_Common.Enums.ReceiverProcessOutcomeEnum.Failed,
                    ReceiverName = "Testperson 2 failed",
                    ProcessStage = FtB_Common.Enums.DistributionReceiverProcessStageEnum.ReadyForReporting
                },
                new DistributionReceiverDto()
                {
                    ReceiverId = "RECEIVERID3",
                    ProcessOutcome = FtB_Common.Enums.ReceiverProcessOutcomeEnum.ReservedReportee,
                    ReceiverName = "Testperson 3 reserved",
                    ProcessStage = FtB_Common.Enums.DistributionReceiverProcessStageEnum.ReadyForReporting
                },
                new DistributionReceiverDto()
                {
                    ReceiverId = "ORGNR1",
                    ProcessOutcome = FtB_Common.Enums.ReceiverProcessOutcomeEnum.Sent,
                    ReceiverName = "Test org 1 sent",
                    ProcessStage = FtB_Common.Enums.DistributionReceiverProcessStageEnum.ReadyForReporting
                },
                new DistributionReceiverDto()
                {
                    ReceiverId = "ORGNR2",
                    ProcessOutcome = FtB_Common.Enums.ReceiverProcessOutcomeEnum.Failed,
                    ReceiverName = "Test org 2 failed",
                    ProcessStage = FtB_Common.Enums.DistributionReceiverProcessStageEnum.ReadyForReporting
                },
            };

            var ssns = new List<(string Id, string Navn)>()
            {
                ("RECEIVERID1", "Testperson 1 sent"),
                ("RECEIVERID2", "Testperson 2 failed"),
                ("RECEIVERID3", "Testperson 3 reserved")
            };

            var orgs = new List<(string Id, string Navn)>()
            {
                ("ORGNR1", "Test org 1 sent"),
                ("ORGNR2", "Test org 2 failed")
            };

            IEnumerable<string> result = DistributionReceiptReportHelper.GetNamesOfNotReachableReceivers(tableStorageRows, ssns, orgs);

            Assert.True(result.Count() == 3);
        }

        [Fact]
        public void GetListOfFailedReceivers_all_failed()
        {
            var tableStorageRows = new List<DistributionReceiverDto>() {
                new DistributionReceiverDto()
                {
                    ReceiverId = "RECEIVERID1",
                    ProcessOutcome = FtB_Common.Enums.ReceiverProcessOutcomeEnum.Failed,
                    ReceiverName = "Testperson 1 failed",
                    ProcessStage = FtB_Common.Enums.DistributionReceiverProcessStageEnum.ReadyForReporting
                },
                new DistributionReceiverDto()
                {
                    ReceiverId = "RECEIVERID2",
                    ProcessOutcome = FtB_Common.Enums.ReceiverProcessOutcomeEnum.Failed,
                    ReceiverName = "Testperson 2 failed",
                    ProcessStage = FtB_Common.Enums.DistributionReceiverProcessStageEnum.ReadyForReporting
                },
                new DistributionReceiverDto()
                {
                    ReceiverId = "RECEIVERID3",
                    ProcessOutcome = FtB_Common.Enums.ReceiverProcessOutcomeEnum.Failed,
                    ReceiverName = "Testperson 3 failed",
                    ProcessStage = FtB_Common.Enums.DistributionReceiverProcessStageEnum.ReadyForReporting
                },
                new DistributionReceiverDto()
                {
                    ReceiverId = "ORGNR1",
                    ProcessOutcome = FtB_Common.Enums.ReceiverProcessOutcomeEnum.Failed,
                    ReceiverName = "Test org 1 failed",
                    ProcessStage = FtB_Common.Enums.DistributionReceiverProcessStageEnum.ReadyForReporting
                },
                new DistributionReceiverDto()
                {
                    ReceiverId = "ORGNR2",
                    ProcessOutcome = FtB_Common.Enums.ReceiverProcessOutcomeEnum.Failed,
                    ReceiverName = "Test org 2 failed",
                    ProcessStage = FtB_Common.Enums.DistributionReceiverProcessStageEnum.ReadyForReporting
                },
            };

            var ssns = new List<(string Id, string Navn)>()
            {
                ("RECEIVERID1", "Testperson 1 failed"),
                ("RECEIVERID2", "Testperson 2 failed"),
                ("RECEIVERID3", "Testperson 3 failed")
            };

            var orgs = new List<(string Id, string Navn)>()
            {
                ("ORGNR1", "Test org 1 failed"),
                ("ORGNR2", "Test org 2 failed")
            };

            IEnumerable<string> result = DistributionReceiptReportHelper.GetNamesOfNotReachableReceivers(tableStorageRows, ssns, orgs);

            Assert.True(result.Count() == 5);
        }

        [Fact]
        public void GetListOfFailedReceivers_no_failed()
        {
            var tableStorageRows = new List<DistributionReceiverDto>() {
                new DistributionReceiverDto()
                {
                    ReceiverId = "RECEIVERID1",
                    ProcessOutcome = FtB_Common.Enums.ReceiverProcessOutcomeEnum.Sent,
                    ReceiverName = "Testperson 1 Sent",
                    ProcessStage = FtB_Common.Enums.DistributionReceiverProcessStageEnum.ReadyForReporting
                },
                new DistributionReceiverDto()
                {
                    ReceiverId = "RECEIVERID2",
                    ProcessOutcome = FtB_Common.Enums.ReceiverProcessOutcomeEnum.Sent,
                    ReceiverName = "Testperson 2 Sent",
                    ProcessStage = FtB_Common.Enums.DistributionReceiverProcessStageEnum.ReadyForReporting
                },
                new DistributionReceiverDto()
                {
                    ReceiverId = "RECEIVERID3",
                    ProcessOutcome = FtB_Common.Enums.ReceiverProcessOutcomeEnum.Sent,
                    ReceiverName = "Testperson 3 Sent",
                    ProcessStage = FtB_Common.Enums.DistributionReceiverProcessStageEnum.ReadyForReporting
                },
                new DistributionReceiverDto()
                {
                    ReceiverId = "ORGNR1",
                    ProcessOutcome = FtB_Common.Enums.ReceiverProcessOutcomeEnum.Sent,
                    ReceiverName = "Test org 1 Sent",
                    ProcessStage = FtB_Common.Enums.DistributionReceiverProcessStageEnum.ReadyForReporting
                },
                new DistributionReceiverDto()
                {
                    ReceiverId = "ORGNR2",
                    ProcessOutcome = FtB_Common.Enums.ReceiverProcessOutcomeEnum.Sent,
                    ReceiverName = "Test org 2 Sent",
                    ProcessStage = FtB_Common.Enums.DistributionReceiverProcessStageEnum.ReadyForReporting
                },
            };

            var ssns = new List<(string Id, string Navn)>()
            {
                ("RECEIVERID1", "Testperson 1 Sent"),
                ("RECEIVERID2", "Testperson 2 Sent"),
                ("RECEIVERID3", "Testperson 3 Sent")
            };

            var orgs = new List<(string Id, string Navn)>()
            {
                ("ORGNR1", "Test org 1 Sent"),
                ("ORGNR2", "Test org 2 Sent")
            };

            IEnumerable<string> result = DistributionReceiptReportHelper.GetNamesOfNotReachableReceivers(tableStorageRows, ssns, orgs);

            Assert.True(result.Count() == 0);
        }
    }
}