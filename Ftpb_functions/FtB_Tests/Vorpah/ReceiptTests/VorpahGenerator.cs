﻿using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FtB_Tests.Vorpah
{
    public class VorpahGenerator
    {
        public static PlanvarselV2 Generate(IEnumerable<Beroertpart> berørteOrganisasjoner, IEnumerable<Beroertpart> berørtePersoner)
        {
            var nabovarselPlan = new PlanvarselV2();

            var berørteParter = new List<Beroertpart>();

            if (berørteOrganisasjoner?.Count() > 0)
                berørteParter.AddRange(berørteOrganisasjoner);

            if(berørtePersoner?.Count() > 0)
                berørteParter.AddRange(berørtePersoner);

            nabovarselPlan.BeroerteParter = berørteParter.ToArray();

            nabovarselPlan.Kommunenavn = "Ringerike";
            nabovarselPlan.Metadata = new MetadataPlan
            {
                FraSluttbrukersystem = "Fellestjenester bygg og plan testmotor"
            };

            nabovarselPlan.Forslagsstiller = new AktoerPlan
            {
                Partstype = new Kodeliste
                {
                    Kodeverdi = "Foretak",
                    Kodebeskrivelse = "Foretak"
                },
                Organisasjonsnummer = "910467905",
                Navn = "BRYGGJA OG RANHEIM AS",
                Adresse = new EnkelAdresse
                {
                    Adresselinje1 = "Sentrum",
                    Poststed = "Hønefoss",
                    Postnr = "3502"
                },
                Telefon = "98839131",
                Epost = "norah@bye.no"
            };

            nabovarselPlan.EiendomByggested = new[]
            {
                new Eiendom
                {
                    Adresse = new Adresse
                    {
                        Adresselinje1 = "Gata 19",
                        Landkode = "NO",
                        Postnr = "3502",
                        Poststed = "Hønefoss"
                    },
                    Eiendomsidentifikasjon = new Eiendomsidentifikasjon
                    {
                        Gaardsnummer = "318",
                        Bruksnummer = "97",
                        Festenummer = "0",
                        Seksjonsnummer = "0",
                        Kommunenummer = "3007"
                    },
                    Kommunenavn = "Ringerike"
                }
            };

            nabovarselPlan.GjeldendePlan = new[]
            {
                new Gjeldendeplan
                {
                    Navn = "Reguleringsplan for Hønefoss",
                    Plantype = new Kodeliste
                    {
                        Kodeverdi = "RP",
                        Kodebeskrivelse = "Reguleringsplan"
                    }
                }
            };

            nabovarselPlan.Planforslag = new Planforslag
            {
                Plannavn = $"VORPA-generator {DateTime.Now}",
                HjemmesidePlanforslag = "www.kommunensHjemmeside.no",
                ArealplanId = "464",
                KravKonsekvensutredning = false,
                BegrunnelseKU = "Begrunnelse for hvorfor det ikke er krav om konsekvensutredning.",
                HjemmesidePlanprogram = "www.planprogram.no",
                Planhensikt = 
                    "I tråd med områdereguleringsplanens målsettinger om å skape et levende og attraktivt " +
                    "bysentrum i Hønefoss, ønskes området detaljregulert for å legge til rette for et " +
                    "bærekraftig og realistisk byutviklingsprosjekt.  Aktuelle funksjoner er utvidelse av hotell/konferansesenter, serveringsteder, " +
                    "handel og boliger. Eksisterende kulturmiljø vil være et viktig tema i planarbeidet. " +
                    "Det er ikke krav om planprogram eller konsekvensutredning i tilknytning til planforslaget.",
                FristForInnspill = new DateTime(2020, 08, 01),
                SaksgangOgMedvirkning = "Saksgang og medvirkning....",

                Plantype = new Kodeliste
                {
                    Kodeverdi = "35",
                    Kodebeskrivelse = "Detaljregulering"
                },
                KommunensSaksnummer = new Saksnummer
                {
                    Saksaar = 2020,
                    Sakssekvensnummer = 11111
                }
            };

            nabovarselPlan.Signatur = new Signatur
            {
                Signaturdato = new DateTime(2019, 09, 18),
                SignertAv = "",
                SignertPaaVegneAv = "",
            };

            return nabovarselPlan;
        }
    }
}