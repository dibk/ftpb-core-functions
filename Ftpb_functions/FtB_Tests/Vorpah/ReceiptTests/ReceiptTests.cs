using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Entities;
using FtB_Common.Encryption;
using FtB_DataModels.Mappers;
using FtB_FormLogic;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Xunit.Abstractions;

namespace FtB_Tests.Vorpah.ReceiptTests
{
    public class ReceiptTests
    {
        private readonly ITestOutputHelper _output;
        private readonly int _antallPersoner = 1000;
        private readonly int _antallForetak = 50;
        private readonly string _encryptionThumbprint = "";

        public ReceiptTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void GenerateListForReceipt()
        {
            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            var orgs = VorpahBerortPartOrganisasjonGenerator.GenerateBeroerteParter(_antallForetak);
            var personer = VorpahBerortPartPersonGenerator.GenerateBeroerteParter(_antallPersoner);

            var mockVorpaForm = VorpahGenerator.Generate(orgs.Item1, personer.Item1);

            var someOptions = Options.Create(new EncryptionSettings());
            someOptions.Value.CertificateThumbprint = _encryptionThumbprint;

            var decrypter = new Decryption(someOptions);

            var distribuerteOrgnr = orgs.Item2;
            var distribuertePersoner = personer.Item2;

            distribuerteOrgnr.AddRange(distribuertePersoner);

            var input = distribuerteOrgnr.Select(p => new DistributionReceiverDto() { ReceiverId = p });
            _output.WriteLine($"Generert kildedata {sw.ElapsedMilliseconds}ms");
            sw.Restart();
            var groupsStep = VarselOppstartPlanarbeidPlussHoeringsmyndigheterReportLogic.GetGroupsOfBeroerteParterWithDecryptedIDsParallel(mockVorpaForm.BeroerteParter, input, (ssn) => decrypter.DecryptText(ssn));

            var toUniqueBeroertPartTypeWithEiendoms = VarselOppstartPlanarbeidPlussHoeringsmyndigheterMappers.GetListOfPlanvarselBeroertPartWithEiendomFromGroups(groupsStep);

            _output.WriteLine($"Parallel - BerÝrteparter-liste {sw.ElapsedMilliseconds}ms");
            sw.Restart();

            _output.WriteLine($"Html generering {sw.ElapsedMilliseconds}ms");

            Assert.True(toUniqueBeroertPartTypeWithEiendoms.Count() <= mockVorpaForm.BeroerteParter.Count());
        }
    }

    public static class Testegreier
    {
        public static no.kxml.skjema.dibk.nabovarselPlan.BeroertPartType ToUniqueBeroertPartTypeWithPopulatedGjeldendeEiendom2(this no.kxml.skjema.dibk.nabovarselPlan.NabovarselPlanType nabovarsel, string beroertPartId, Func<string, string> idDecryptor)
        {
            var decryptedReceiverId = idDecryptor(beroertPartId);
            var berortParter = nabovarsel.beroerteParter?.Where(b => (
                                                                              !string.IsNullOrEmpty(b.foedselsnummer) && idDecryptor(b.foedselsnummer).Equals(decryptedReceiverId))
                                                                              || (!string.IsNullOrEmpty(b.organisasjonsnummer) && b.organisasjonsnummer.Equals(decryptedReceiverId))
                                                                      ).ToList();

            var beroertPartType = berortParter.FirstOrDefault();

            List<no.kxml.skjema.dibk.nabovarselPlan.GjelderEiendomType> gjelderEiendom = new List<no.kxml.skjema.dibk.nabovarselPlan.GjelderEiendomType>();

            foreach (var beroertPart in berortParter)
            {
                foreach (var eiendom in beroertPart.gjelderEiendom)
                {
                    if (!gjelderEiendom.Contains(eiendom))
                    {
                        gjelderEiendom.Add(eiendom);
                    }
                }
            }
            var gjelderEiendomArray = gjelderEiendom.ToArray();
            beroertPartType.gjelderEiendom = gjelderEiendomArray;

            return beroertPartType;
        }
    }
}