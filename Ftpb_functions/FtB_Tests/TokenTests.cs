﻿using System;
using System.Collections.Generic;
using FtB_Common.BusinessModels;
using FtB_Common.Enums;
using FtB_Common.Extensions;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FtB_Tests;

[TestClass]
public class TokenTests
{
    [TestMethod]
    public void EqualsWorksAsIntended()
    {
        var now = DateTime.UtcNow;

        var token1 = new Token("tokenValue", TokenTypeEnum.Maskinporten, new List<string> { "noko1", "noko2" }, null, now.AddSeconds(60));
        var token2 = new Token(string.Empty, TokenTypeEnum.Maskinporten, new List<string> { "noko2", "noko1" }, null, null);

        Assert.IsTrue(token1.Equals(token2));
    }

    [TestMethod]
    public void GetHashCodeWorksAsIntended()
    {
        var now = DateTime.UtcNow;

        var token1 = new Token("tokenWithValue", TokenTypeEnum.Maskinporten, new List<string> { "noko1", "noko2" }, null, now.AddSeconds(60));
        var token2 = new Token(string.Empty, TokenTypeEnum.Maskinporten, new List<string> { "noko2", "noko1" }, null, null);

        var hash1 = token1.GetHashCode();
        var hash2 = token2.GetHashCode();

        Assert.IsTrue(hash1.Equals(hash2));

        var hashset = new HashSet<Token> { token1 };

        Assert.IsTrue(hashset.TryGetValue(token2, out _));

        Assert.IsTrue(hashset.Contains(token2));

        Assert.IsTrue(hashset.Remove(token2));

        Assert.IsFalse(hashset.Contains(token2));

        Assert.IsTrue(hashset.Add(token1));

        hashset.Replace(token2);

        Assert.IsTrue(hashset.Contains(token2));
    }

    [TestMethod]
    public void JwtTests()
    {
        const string token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjM4QUE3QTc5MjUzNDNCQjE0NjFCRUUwMURCNUQwOTRBM0VCOTgwMjUiLCJ4NXQiOiJPS3A2ZVNVME83RkdHLTRCMjEwSlNqNjVnQ1UiLCJ0eXAiOiJKV1QifQ.eyJzY29wZSI6ImFsdGlubjpzZXJ2aWNlb3duZXIvaW5zdGFuY2VzLndyaXRlIGFsdGlubjpzZXJ2aWNlb3duZXIvaW5zdGFuY2VzLnJlYWQiLCJ0b2tlbl90eXBlIjoiQmVhcmVyIiwiZXhwIjoxNzA4OTUzMTIwLCJpYXQiOjE3MDg5NTEzMjAsImNsaWVudF9pZCI6IjVmZDJlNzY3LWIwNTYtNGU5Mi1hYjA2LTE4ODRiZGU4ZWVmOCIsImNvbnN1bWVyIjp7ImF1dGhvcml0eSI6ImlzbzY1MjMtYWN0b3JpZC11cGlzIiwiSUQiOiIwMTkyOjk3NDc2MDIyMyJ9LCJ1cm46YWx0aW5uOm9yZyI6ImRpYmsiLCJ1cm46YWx0aW5uOm9yZ051bWJlciI6Ijk3NDc2MDIyMyIsInVybjphbHRpbm46YXV0aGVudGljYXRlbWV0aG9kIjoibWFza2lucG9ydGVuIiwidXJuOmFsdGlubjphdXRobGV2ZWwiOjMsImlzcyI6Imh0dHBzOi8vcGxhdGZvcm0udHQwMi5hbHRpbm4ubm8vYXV0aGVudGljYXRpb24vYXBpL3YxL29wZW5pZC8iLCJqdGkiOiI5M2NhZWMyOC05YjE5LTRkMGMtYjhkZS1jNWM1Y2RmODcyNDgiLCJuYmYiOjE3MDg5NTEzMjB9.DCePIuyvk6K-UzxabEDo1De4NYG3LTs0dc1Vn8FqU5kZS8uTMSYlcH9aekC77zN8f1Uc-oQE9iFPIUOjpZv5kyIoJx9H7CEOg_5EtVK8nQKGv8eIp_aWjXapqwmfeaRc4NlnoXfoqtkUO24w--Yh2CT0smelPF_R4BqpoCU941NLfaWv5n4oHs9BY4VePGbyhSUbdYANQFp6pi2DWa4WGoplPRjeMofydidrZJTgAaKI-sps0osy7OlILzVGzB46cspNpc_X7bjZbMlvNCZAfUN3PgsvmcvvB5XQjStqGGjOve1QzzH43JiEpPLm-g_bBTCOO_BslC5gV2WQ4o-4MQ";

        var tokenObj = new JsonWebToken(token);

        DateTime expiryTime = tokenObj.ValidTo;
        
        var actualExpiryTime = new DateTime(2024, 2, 26, 13, 12, 0, DateTimeKind.Utc);

        Assert.AreEqual(actualExpiryTime, expiryTime);
    }
}