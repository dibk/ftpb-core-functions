﻿using FtB_Common.BusinessModels.TableStorage.Repos;
using FtB_Common.Encryption;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Utils;
using FtB_FormLogic;
using FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidLogic;
using Ftb_Repositories;
using Ftb_Repositories.HttpClients;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtB_Tests
{
    [TestClass]
    public class VorpaSvarReceiverHelperTests
    {
        [TestMethod]
        public void GetReceiverCandidatesTest()
        {
            IOptions<EncryptionSettings> options = Options.Create<EncryptionSettings>(new EncryptionSettings());
            options.Value.CertificateThumbprint = "8901079e90f724ca967ecf3dfc6a164748e1660c";
            var decryptor = new Decryption(options);

            var formData = LoadFormData();

            var result = VorpaSvarReceiverHelper.GetReceiverCandidates(decryptor, formData);

            Assert.IsNotNull(result);

            var candidate = result.Where(c => c.Id.Equals("911455307")).FirstOrDefault();

            Assert.IsNotNull(candidate);
        }

        [TestMethod]
        public async Task LoadReceiversTestSvarPaaNabovarselPlanType()
        {
            var repo = new Mock<IFormDataRepo>().Object;
            var tableStorage = new Mock<ITableStorage>().Object;
            var logger = new Mock<ILogger<VarselOppstartPlanarbeidPrepareLogic>>().Object;

            var blobStorageMock = new Mock<IBlobOperations>();
            blobStorageMock.Setup(s => s.GetReporteeIdFromStoredBlobAsync(It.IsAny<string>()).Result).Returns("911455307");

            var dbUnitOfWorkMock = new Mock<IDbUnitOfWork>();
            dbUnitOfWorkMock.Setup(d => d.DistributionForms.Get(It.IsAny<string>()).Result).Returns(new Ftb_DbModels.DistributionForm() { InitialArchiveReference = "ARxxxxxxx" });
            
            var distributionReceiverRepo = new Mock<DistributionReceiverRepo>().Object;
            var distributionReceiverLogRepo = new Mock<DistributionReceiverLogRepo>().Object;
            var notificationSenderRepo = new Mock<NotificationSenderRepo>().Object;
            var notificationSenderLogRepo = new Mock<NotificationSenderLogRepo>().Object;

            IOptions<EncryptionSettings> options = Options.Create<EncryptionSettings>(new EncryptionSettings());
            options.Value.CertificateThumbprint = "8901079e90f724ca967ecf3dfc6a164748e1660c";
            var decryptor = new Decryption(options);

            var decryptionFactory = new DecryptionFactory(new List<IDecryption>() { decryptor });

            var fileDownloadStatusClient = new Mock<IFileDownloadStatusHttpClient>().Object;

            var logic = new SvarVarselOppstartPlanarbeidPrepareLogic(repo,
                                                                     blobStorageMock.Object,
                                                                     logger,
                                                                     dbUnitOfWorkMock.Object,
                                                                     decryptionFactory,
                                                                     fileDownloadStatusClient,
                                                                     distributionReceiverRepo,
                                                                     distributionReceiverLogRepo,
                                                                     notificationSenderRepo,
                                                                     notificationSenderLogRepo);
            logic.FormData = LoadFormData();

            await logic.SetReceiversAsync();
        }

        private no.kxml.skjema.dibk.nabovarselsvarPlan.SvarPaaNabovarselPlanType LoadFormData()
        {
            var retval = new no.kxml.skjema.dibk.nabovarselsvarPlan.SvarPaaNabovarselPlanType();

            var fileContent = System.IO.File.ReadAllText("SvarPaaNabovarselPlanTest.xml");

            retval = SerializeUtil.DeserializeXml<no.kxml.skjema.dibk.nabovarselsvarPlan.SvarPaaNabovarselPlanType>(fileContent);

            return retval;
        }
    }
}
