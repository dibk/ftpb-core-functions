﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FtB_Tests.UtilTests
{
    [TestClass]
    public class XmlUtilTests
    {
        [TestMethod]
        public void GetAttributeValue_ExistingRootElementAndAttribute_ReturnsValue()
        {
            var xmlString = @"<root myattribute='rootAttributeValue'><element>value</element></root>";
            var elementName = "root";
            var attributeName = "myattribute";

            var result = FtB_Common.Utils.XmlUtil.GetAttributeValue(xmlString, elementName, attributeName);

            Assert.AreEqual("rootAttributeValue", result);
        }

        [TestMethod]
        public void GetAttributeValue_ExistingElementAndAttribute_ReturnsValue()
        {
            var xmlString = @"<root><element myattribute='elementAttributeValue'>value</element></root>";
            var elementName = "element";
            var attributeName = "myattribute";

            var result = FtB_Common.Utils.XmlUtil.GetAttributeValue(xmlString, elementName, attributeName);

            Assert.AreEqual("elementAttributeValue", result);
        }

        [TestMethod]
        public void GetAttributeValue_ExistingElementAndNoAttribute_ReturnsNull()
        {
            var xmlString = @"<root><element>value</element></root>";
            var elementName = "element";
            var attributeName = "myattribute";

            var result = FtB_Common.Utils.XmlUtil.GetAttributeValue(xmlString, elementName, attributeName);

            Assert.IsNull(result);
        }
    }
}