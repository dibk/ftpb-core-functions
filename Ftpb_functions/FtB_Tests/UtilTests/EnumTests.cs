﻿using FtB_Common.BusinessModels;
using FtB_Common.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FtB_Tests.UtilTests
{
    [TestClass]
    public class EnumExtentionsTests
    {
        [TestMethod]
        public void GetEnumValue_sameAsEnumValue_ReturnsEnum()
        {
            var actorStringValue = "Foretak";

            var result = EnumExtentions.GetValueFromDescription<ActorTypeEnum>(actorStringValue).ToString();

            Assert.AreEqual("Foretak", result);
        }

        [TestMethod]
        public void GetEnumValue_lowerCaseValue_ReturnsEnum()
        {
            var actorStringValue = "foretak";

            var result = EnumExtentions.GetValueFromDescription<ActorTypeEnum>(actorStringValue).ToString();

            Assert.AreEqual("Foretak", result);
        }

        [TestMethod]
        public void GetEnumValue_sameAsDescription_ReturnsEnum()
        {
            var actorStringValue = "Offentlig myndighet";

            var result = EnumExtentions.GetValueFromDescription<ActorTypeEnum>(actorStringValue).ToString();

            Assert.AreEqual("OffentligMyndighet", result);
        }

        [TestMethod]
        public void GetEnumValue_sameAsDescriptionLowerCase_ReturnsEnum()
        {
            var actorStringValue = "offentlig myndighet";

            var result = EnumExtentions.GetValueFromDescription<ActorTypeEnum>(actorStringValue).ToString();

            Assert.AreEqual("OffentligMyndighet", result);
        }

        [TestMethod]
        public void GetEnumValue_nonExistingValue_ThrowsException()
        {
            var actorStringValue = "EinIkkjeEksisterandeVerdi";

            Assert.ThrowsException<ArgumentException>(() => EnumExtentions.GetValueFromDescription<ActorTypeEnum>(actorStringValue));
        }
    }
}