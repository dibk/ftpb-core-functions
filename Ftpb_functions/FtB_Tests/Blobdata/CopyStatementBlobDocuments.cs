﻿using FtB_Common.Enums;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace FtB_Tests.Blobdata
{
    [TestClass]
    public class CopyStatementBlobDocuments
    {
        // 1. Bruk HOVED-ARKODE til å hente ut containernavn for public storage (stftbpbpublic) i blob "Formdata...xml", metadata key "PublicBlobContainerName"
        // 2. Hent "Skjema.pdf" fra UTTALELSE-ARKODE
        // 3. Kopier på public storage (stftbpbpublic) "Uttalelse_<avsendernavn>_<yyyymmdd>_<ttmmss>.pdf" til "Uttalelse_<avsendernavn>_<yyyymmdd>_<ttmmss>_ERROR.pdf"
        // 4. Kopier "Skjema.pdf" til public storage (stftbpbpublic) med navn "Uttalelse_<avsendernavn>_<yyyymmdd>_<ttmmss>.pdf"

        private readonly BlobOperations _blobOperations;
        private readonly List<Uttalelser> _uttalelser;

        public CopyStatementBlobDocuments()
        {
            var uttalelserData = File.ReadAllText(@"Blobdata\uttalelser.json");
            _uttalelser = JsonConvert.DeserializeObject<List<Uttalelser>>(uttalelserData);

            IServiceCollection services = null;
            services = new ServiceCollection();
            services.AddLogging();

            services.AddScoped<PrivateBlobStorage>();
            services.AddScoped<PublicBlobStorage>();

            services.AddTransient<IConfiguration>(sp =>
            {
                IConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
                configurationBuilder.AddJsonFile("local.settings.json", optional: true, reloadOnChange: true);
                configurationBuilder.AddEnvironmentVariables();

                return configurationBuilder.Build();
            });

            var oneService = new ServiceDescriptor(typeof(IBlobOperations), typeof(BlobOperations), ServiceLifetime.Scoped);
            services.Add(oneService);
            var serviceProvider = services.BuildServiceProvider();
            _blobOperations = (BlobOperations)serviceProvider.GetService<IBlobOperations>();
        }

        [TestMethod]
        public void GetSkjemaFromPrivateContainer()
        {
            var publicBlobContainer = "";
            
            foreach (var uttalelse in _uttalelser)
            {
                // 1. Bruk HOVED-ARKODE til å hente ut containernavn for public storage (stftbpbpublic) i blob "Formdata...xml", metadata key "PublicBlobContainerName"
                var blobStorageTypeFormdata = new List<string> { BlobStorageMetadataTypes.FormData };
                IEnumerable<BlobStreamItem> formdataFraInnsending = _blobOperations.GetAllBlobsByMetadataTypeAsync(BlobStorageEnum.Private, uttalelse.hovedAr, blobStorageTypeFormdata).Result;
                if (formdataFraInnsending != null && formdataFraInnsending.ToList().Count != 1)
                {
                    Debug.WriteLine($"Feil ved henting av Formdata for {uttalelse.hovedAr}");
                    break;
                }
                else
                {
                    publicBlobContainer = formdataFraInnsending.ToList()[0].Metadata.First(x => x.Key.Equals(BlobStorageMetadataKeys.PublicBlobContainerName)).Value;
                    Debug.WriteLine($"Hentet publicBlobContainer {publicBlobContainer} for Formdata for {uttalelse.hovedAr}");
                }

                // 2. Hent "Skjema.pdf" fra UTTALELSE-ARKODE
                BlobStreamItem mainformFraUttalelse;
                var blobStorageTypeMainform = new List<string> { BlobStorageMetadataTypes.MainForm };
                var mainforms = _blobOperations.GetAllBlobsByMetadataTypeAsync(BlobStorageEnum.Private, uttalelse.svarAr, blobStorageTypeMainform).Result;
                if (mainforms != null && mainforms.ToList().Count != 1)
                {
                    Debug.WriteLine($"Feil ved henting av MainForm (Skjema.pdf) for {uttalelse.svarAr}");
                    break;
                }
                else
                {
                    mainformFraUttalelse = mainforms.ToList()[0];
                    Debug.WriteLine($"Hentet opprinnelig SvarNabovarselPlan for {uttalelse.svarAr}");
                }

                // 3. Kopier på public storage (stftbpbpublic) "Uttalelse_<avsendernavn>_<yyyymmdd>_<ttmmss>.pdf" til "Uttalelse_<avsendernavn>_<yyyymmdd>_<ttmmss>_ERROR.pdf"

                var metadataFilter = new List<KeyValuePair<string, string>>();
                //metadataFilter.Add(new KeyValuePair<string, string>("AttachmentTypeName", "SvarNabovarselPlan"));
                metadataFilter.Add(new KeyValuePair<string, string>(BlobStorageMetadataKeys.SendersArchiveReference, uttalelse.svarAr));
                var origSvarUttalelseRes = _blobOperations.GetAllBlobsByFilterAsync(BlobStorageEnum.Public, publicBlobContainer, metadataFilter).Result;

                if (origSvarUttalelseRes.ToList().Count == 0)
                {
                    Debug.WriteLine($"Feil ved henting av public SvarNabovarselPlan (som er feil) for {uttalelse.svarAr}");
                    break;
                }
                else
                {
                    BlobStreamItem origSvarUttalelse;
                    if (origSvarUttalelseRes.ToList().Count == 1)
                    {
                        origSvarUttalelse = origSvarUttalelseRes.ToList()[0];
                    }
                    else
                    {
                        origSvarUttalelse = origSvarUttalelseRes.ToList().First(x => x.Metadata.Any(y => y.Key.Equals(BlobStorageMetadataKeys.AttachmentTypeName) && y.Value.Equals("SvarNabovarselPlan")));
                    }

                    var newMetadata = new List<KeyValuePair<string, string>>();
                    newMetadata.Add(new KeyValuePair<string, string>(BlobStorageMetadataKeys.AttachmentTypeName, "SvarNabovarselPlan"));
                    newMetadata.Add(new KeyValuePair<string, string>(BlobStorageMetadataKeys.SendersArchiveReference, uttalelse.svarAr));
                    
                    var errorFile = _blobOperations.AddStreamToBlobStorageAsync(BlobStorageEnum.Public, publicBlobContainer, origSvarUttalelse.FileName + "_ERROR", origSvarUttalelse.Content, origSvarUttalelse.MimeType, newMetadata).Result;

                    // 4. Kopier "Skjema.pdf" til public storage (stftbpbpublic) med navn "Uttalelse_<avsendernavn>_<yyyymmdd>_<ttmmss>.pdf"
                    var correctFile = _blobOperations.AddStreamToBlobStorageAsync(BlobStorageEnum.Public, publicBlobContainer, origSvarUttalelse.FileName, mainformFraUttalelse.Content, mainformFraUttalelse.MimeType, newMetadata).Result;

                    Debug.WriteLine($"Kopiert uttalelses-PDF for {uttalelse.svarAr} til public blob container {publicBlobContainer}");
                }
            }

            Assert.IsTrue(1 == 1);
        }
    }

    public class Uttalelser
    {
        public string hovedAr { get; set; }
        public string svarAr { get; set; }
    }
}
