﻿namespace KontaktOgReservasjonsRegisteret
{
    public static class KRRConstants
    {
        public const string GyldigStatus = "AKTIV";
        public const string GyldigVarslingsstatus = "KAN_VARSLES";
        public const string GyldigReservertstatus = "NEI";
    }
}
