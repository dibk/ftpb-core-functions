﻿using FtB_Common;
using KontaktOgReservasjonsRegisteret;
using Maskinporten;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace KontaktOgReservasjonsregisteret
{
    public class KRRClient
    {
        private readonly MaskinportenClient _maskinportenClient;
        private readonly KrrClientConfiguration _configuration;
        private readonly HttpClient _httpClient;
        ILogger<KRRClient> _logger;


        public KRRClient(
            IOptions<KrrClientConfiguration> configuration, MaskinportenClient maskinportenClient, ILogger<KRRClient> logger, HttpClient httpClient)
        {
            _maskinportenClient = maskinportenClient;
            _configuration = configuration.Value;
            _httpClient = httpClient;
            _logger = logger;
        }

        public async Task<List<DigitalPersonInformation>> GetDigitalReservationInformationAsync(List<string> IDs)
        {
            var scopes = _configuration.GlobalKontaktinformasjonReadScope;
            string maskinportenToken = await _maskinportenClient.GetAccessToken(scopes.Split(";"));
            if (maskinportenToken == null)
            {
                _logger.LogError($"Unable to receive AccessToken from Maskinporten. Aborting request to Kontakt- og reservasjonsregisteret.");
                return null;
            }

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", maskinportenToken);

            var requestUri = _configuration.KrrEndpoint;
            HttpResponseMessage response = null;

            var digitalPersonInformation = new List<DigitalPersonInformation>();

            // https://docs.digdir.no/docs/Kontaktregisteret/oppslagstjenesten_rest#endepunkt
            var batchedIdsList = IDs.Batch(1000);

            foreach (var batchedIds in batchedIdsList)
            {
                KRRRequestBody kRRRequestBody = new KRRRequestBody();
                kRRRequestBody.personidentifikatorer = batchedIds;

                var content = new StringContent(JsonConvert.SerializeObject(kRRRequestBody), Encoding.UTF8, "application/json");

                try
                {
                    response = await _httpClient.PostAsync(requestUri, content);
                    response.EnsureSuccessStatusCode();
                    var result = await response.Content.ReadAsStringAsync();
                    digitalPersonInformation.AddRange(JsonConvert.DeserializeObject<KRRInformation>(result).Personer);
                }
                catch (Exception exception)
                {
                    _logger.LogError(exception, $"Unable to fetch answer from {requestUri}. Statuscode: {response.StatusCode}");
                    response.Dispose();
                }
            }

            return digitalPersonInformation;
        }
    }
}
