﻿namespace KontaktOgReservasjonsregisteret
{
    public class KrrClientConfiguration
    {
        public readonly static string SectionName = "KontaktOgReservasjonsregsiteret";
        
        public string KrrEndpoint { get; set; }
        public string GlobalKontaktinformasjonReadScope { get; set; }
    }
}