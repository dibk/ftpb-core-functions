﻿using System;
using System.Collections.Generic;

namespace KontaktOgReservasjonsRegisteret
{

    public class KRRInformation
    {
        public List<DigitalPersonInformation> Personer { get; set; }
    }

    public class DigitalPersonInformation
    {
        public string Personidentifikator { get; set; }
        public string Reservasjon { get; set; }
        public string Status { get; set; }
        public string Varslingsstatus { get; set; }
        public KontaktInformasjon Kontaktinformasjon { get; set; }

        public bool CanBeDigitallyContacted()
        {
            return
                Status.Equals(KRRConstants.GyldigStatus) &&
                Varslingsstatus.Equals(KRRConstants.GyldigVarslingsstatus) &&
                Reservasjon.Equals(KRRConstants.GyldigReservertstatus);
        }
    }

    public class KontaktInformasjon
    {
        public string epostadresse { get; set; }
        public DateTime epostadresse_oppdatert { get; set; }
        public DateTime epostadresse_sist_verifisert { get; set; }
        public string mobiltelefonnummer { get; set; }
        public DateTime mobiltelefonnummer_oppdatert { get; set; }
        public DateTime mobiltelefonnummer_sist_verifisert { get; set; }
    }

    public class KRRRequestBody
    {
        public List<string> personidentifikatorer { get; set; }


    }
}
