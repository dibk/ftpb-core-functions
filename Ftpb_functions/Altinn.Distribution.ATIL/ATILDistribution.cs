﻿using Altinn2.Adapters;
using Altinn2.Adapters.Bindings;
using Altinn2.Adapters.WS;
using Altinn2.Adapters.WS.Correspondence;
using Altinn2.Adapters.WS.Prefill;
using AltinnWebServices.WS.Prefill;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace Altinn.Distribution.ATIL
{
    public static class ServicesConfiguration
    {
        public static IServiceCollection AddAtilAltinn2Distribution(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IBindingFactory, BindingFactory>();
            services.AddScoped<IBinding, MtomBindingProvider>();
            services.AddScoped<IBinding, BasicBindingProvider>();
            services.AddScoped<ATIL_PrefillClient>();
            services.AddScoped<ATIL_PrefillTmpClient>();
            services.AddScoped<ATIL_PrefillAdapter>();
            services.AddScoped<IPrefillFormTaskBuilder, PrefillFormTaskBuilder>();

            services.AddScoped<ATIL_CorrespondenceClient>();
            services.AddScoped<ATIL_CorrespondenceAdapter>();
            services.AddScoped<ICorrespondenceBuilder, CorrespondenceBuilder>();

            services.AddScoped<ATIL_Altinn2Distribution>();

            services.Configure<ATIL_AltinnServiceOwnerSettings>(configuration.GetSection(ATIL_AltinnServiceOwnerSettings.ConfigSection));
            services.Configure<AltinnSettings>(configuration.GetSection(AltinnSettings.ConfigSection));

            return services;
        }
    }
    public class ATIL_PrefillTmpClient : IPrefillClient
    {
        public Task<ReceiptExternal> SendPrefill(PrefillFormTask prefillFormTask, DateTime? dueDate)
        {
            var rnd = new Random().Next(100, 10000);

            var receipt = new ReceiptExternal()
            {
                ReceiptStatusCode = ReceiptStatusEnum.OK,
                ReceiptId = rnd,
                LastChanged = DateTime.Now,
                References = new ReferenceList()
            };

            receipt.References.Add(new Reference() { ReferenceTypeName = ReferenceType.WorkFlowReference, ReferenceValue = $"{rnd}" });

            return Task.FromResult(receipt);
        }
    }

    public class ATIL_PrefillClient : PrefillClient
    {
        public ATIL_PrefillClient(IBindingFactory bindingFactory, IOptions<AltinnSettings> connectionOptions, IOptions<ATIL_AltinnServiceOwnerSettings> serviceOwnerSettings, ILogger<ATIL_PrefillClient> log)
            : base(bindingFactory, connectionOptions, serviceOwnerSettings, log) { }
    }

    public class ATIL_CorrespondenceClient : CorrespondenceClient
    {
        public ATIL_CorrespondenceClient(ILogger<ATIL_CorrespondenceClient> logger, IBindingFactory bindingFactory, IOptions<AltinnSettings> connectionOptions, IOptions<ATIL_AltinnServiceOwnerSettings> serviceOwnerSettings)
            : base(logger, bindingFactory, connectionOptions, serviceOwnerSettings) { }
    }

    public class ATIL_PrefillAdapter : PrefillAdapter
    {
        public ATIL_PrefillAdapter(ILogger<ATIL_PrefillAdapter> logger, IPrefillFormTaskBuilder prefillFormTaskBuilder, ATIL_PrefillClient altinnPrefillClient)
            : base(logger, prefillFormTaskBuilder, altinnPrefillClient) { }
    }

    public class ATIL_CorrespondenceAdapter : CorrespondenceAdapter
    {
        public ATIL_CorrespondenceAdapter(ILogger<ATIL_CorrespondenceAdapter> logger, IOptions<ATIL_AltinnServiceOwnerSettings> correspondenceSettings, ATIL_CorrespondenceClient correspondenceClient)
            : base(logger, correspondenceSettings, correspondenceClient) { }
    }

    public class ATIL_Altinn2Distribution : Altinn2Distribution
    {
        public ATIL_Altinn2Distribution(ILogger<ATIL_Altinn2Distribution> logger, ATIL_PrefillAdapter prefillAdapter, ATIL_CorrespondenceAdapter correspondenceAdapter, IOptions<AltinnSettings> connectionOptions)
            : base(logger, prefillAdapter, correspondenceAdapter, connectionOptions) { }
    }

    //public class ATIL_Altinn2Distribution : IDistributionAdapter
    //{
    //    private readonly ILogger<ATIL_Altinn2Distribution> _logger;
    //    private readonly ATIL_PrefillAdapter _prefillAdapter;

    //    public ATIL_Altinn2Distribution(ILogger<ATIL_Altinn2Distribution> logger, ATIL_PrefillAdapter prefillAdapter)
    //    {
    //        _logger = logger;
    //        _prefillAdapter = prefillAdapter;
    //    }

    //    public async Task<IAltinnPrefilledDistributionResult> SendCorrespondenceAsync(AltinnDistributionMessage altinnMessage, string prefillReferenceId)
    //    {

    //        var res = new AltinnPrefilledDistributionResult();
    //        res.CorrespondenceResult = new AltinnCorrespondenceResult() { AltinnCommunicationResult = AltinnCommunicationResultType.Sent };

    //        return res;

    //    }

    //    public async Task<IAltinnPrefilledDistributionResult> SendPrefillAndCorrespondenceAsync(AltinnDistributionMessage altinnMessage)
    //    {
    //        var res = new AltinnPrefilledDistributionResult();

    //        //Send prefill
    //        var prefillResult = await _prefillAdapter.SendPrefill(altinnMessage);
    //        res.PrefillResult = prefillResult;

    //        if (prefillResult.IsSuccessfull())
    //        {

    //            res.CorrespondenceResult = new AltinnCorrespondenceResult() { AltinnCommunicationResult = AltinnCommunicationResultType.Sent };
    //        }

    //        return res;
    //    }
    //}
}