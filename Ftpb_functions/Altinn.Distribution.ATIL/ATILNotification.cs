﻿using Altinn2.Adapters.Bindings;
using Altinn2.Adapters.WS;
using Altinn2.Adapters.WS.Correspondence;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Altinn.Distribution.ATIL
{
    public static class ServiceConfiguration { 
        public static IServiceCollection AddAtilAltinnNotification(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddScoped<IBindingFactory, BindingFactory>();
        services.AddScoped<IBinding, MtomBindingProvider>();
        services.AddScoped<IBinding, BasicBindingProvider>();

        services.AddScoped<ATIL_CorrespondenceClient>();
        services.AddScoped<ATIL_CorrespondenceAdapter>();
        services.AddScoped<ICorrespondenceBuilder, CorrespondenceBuilder>();

        services.Configure<ATIL_AltinnServiceOwnerSettings>(configuration.GetSection(ATIL_AltinnServiceOwnerSettings.ConfigSection));
        services.Configure<AltinnSettings>(configuration.GetSection(AltinnSettings.ConfigSection));

        services.AddScoped<ATIL_AltinnNotification>();

        return services;
    }
}
public class ATIL_AltinnNotification : AltinnNotification
{
    public ATIL_AltinnNotification(ILogger<ATIL_AltinnNotification> logger, ATIL_CorrespondenceAdapter correspondenceAdapter)
        : base(logger, correspondenceAdapter) { }
}
}
