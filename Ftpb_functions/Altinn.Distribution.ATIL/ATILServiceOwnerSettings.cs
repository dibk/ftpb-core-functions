﻿using Altinn2.Adapters.WS;

namespace Altinn.Distribution.ATIL
{
    public class ATIL_AltinnServiceOwnerSettings : ServiceOwnerSettings
    {
        public static string ConfigSection => "ATIL-AltinnSettings";
    }
}
