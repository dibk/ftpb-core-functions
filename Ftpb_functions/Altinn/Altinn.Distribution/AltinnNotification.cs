﻿using Altinn.Common;
using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Altinn.Distribution
{
    public class AltinnNotification : INotificationAdapter
    {
        private readonly ILogger<AltinnNotification> _logger;
        private readonly ICorrespondenceAdapter _correspondenceAdapter;

        public AltinnNotification(ILogger<AltinnNotification> logger, ICorrespondenceAdapter correspondenceAdapter)
        {
            _logger = logger;
            _correspondenceAdapter = correspondenceAdapter;
        }

        public async Task<AltinnNotificationCorrespondenceResult> SendNotificationAsync(AltinnMessageBase altinnMessage)
        {
            var correspondenceResult = await _correspondenceAdapter.SendMessageAsync(altinnMessage);
            return new AltinnNotificationCorrespondenceResult() { CorrespondenceResult = correspondenceResult };
        }
    }
}