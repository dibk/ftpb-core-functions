﻿using Altinn.Common;
using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using Altinn2.Adapters.WS;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace Altinn.Distribution
{
    public class Altinn2Distribution : IDistributionAdapter
    {
        private readonly ILogger<Altinn2Distribution> _logger;
        private readonly IPrefillAdapter _prefillAdapter;
        private readonly ICorrespondenceAdapter _correspondenceAdapter;
        private readonly IOptions<AltinnSettings> _connectionOptions;

        public Altinn2Distribution(ILogger<Altinn2Distribution> logger, 
                                   IPrefillAdapter prefillAdapter,
                                   ICorrespondenceAdapter correspondenceAdapter,
                                   IOptions<AltinnSettings> connectionOptions)
        {
            _logger = logger;
            _prefillAdapter = prefillAdapter;
            _correspondenceAdapter = correspondenceAdapter;
            _connectionOptions = connectionOptions;
        }

        public async Task<IAltinnPrefilledDistributionResult> SendPrefillAndCorrespondenceAsync(IDistributionMessage altinnMessage)
        {
            var res = new AltinnPrefilledDistributionResult();
            
            //Send prefill
            var prefillResult = await _prefillAdapter.SendPrefill(altinnMessage);
            res.PrefillResult = prefillResult;

            if (prefillResult.IsSuccessfull())
            {
                if (altinnMessage.NotificationMessage?.ReplyLink?.UrlTitle != string.Empty)
                {
                    var baseAddress = _connectionOptions.Value.BaseAddress;
                    altinnMessage.NotificationMessage.ReplyLink.Url = $"{baseAddress}/Pages/ServiceEngine/Dispatcher/Dispatcher.aspx?ReporteeElementID={prefillResult.PrefillReferenceId}";
                }

                var correspondenceResults = await _correspondenceAdapter.SendMessageAsync(altinnMessage.NotificationMessage, altinnMessage.DistributionFormReferenceId.ToString());
                res.CorrespondenceResult = correspondenceResults;
            }

            return res;
        }

        public async Task<IAltinnPrefilledDistributionResult> SendCorrespondenceAsync(IDistributionMessage altinnMessage, string prefillReferenceId)
        {
            
            var result = new AltinnPrefilledDistributionResult();

            //Send correspondence
            if (altinnMessage.NotificationMessage?.ReplyLink?.UrlTitle != string.Empty)
            {
                var baseAddress = _connectionOptions.Value.BaseAddress;
                altinnMessage.NotificationMessage.ReplyLink.Url = $"{baseAddress}/Pages/ServiceEngine/Dispatcher/Dispatcher.aspx?ReporteeElementID={prefillReferenceId}";
                result.CorrespondenceResult = await _correspondenceAdapter.SendMessageAsync(altinnMessage.NotificationMessage, altinnMessage.DistributionFormReferenceId.ToString());
            }

            return result;
        }
    }
}
