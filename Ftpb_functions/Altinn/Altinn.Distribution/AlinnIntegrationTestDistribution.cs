﻿using Altinn.Common;
using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using System;
using System.Threading.Tasks;

namespace Altinn.Distribution
{
    public class AlinnIntegrationTestDistribution : IDistributionAdapter
    {
        public async Task<IAltinnPrefilledDistributionResult> SendPrefillAndCorrespondenceAsync(IDistributionMessage altinnMessage)
        {
            var results = new AltinnPrefilledDistributionResult();

            //Prefill
            var prefillSleepMs = new Random(5).Next(100, 600);
            await Task.Delay(prefillSleepMs);

            var random = new Random();

            var randomNumber = random.Next(0, 100);

            if (randomNumber < 80)
            {
                results.PrefillResult = new AltinnPrefillResult() { Message = "Ok", AltinnCommunicationResult = AltinnCommunicationResultType.Sent };
                //Correspondence
                results.CorrespondenceResult = new AltinnCorrespondenceResult() { Message = "Ok", AltinnCommunicationResult = AltinnCommunicationResultType.Sent };
                //results.Add(new DistributionResult(DistributionComponent.Correspondence) { Message = "Ok", Step = DistributionStep.Sent });
                var correspondenceSleepMs = new Random(5).Next(100, 600);
                await Task.Delay(correspondenceSleepMs);
            }
            else
            {
                if (randomNumber >= 80 && randomNumber < 90)
                    results.PrefillResult = new AltinnPrefillResult() { Message = "Reserved", AltinnCommunicationResult = AltinnCommunicationResultType.ReservedReportee };
                //results.Add(new DistributionResult(DistributionComponent.Prefill) { Message = "Reserved", Step = DistributionStep.ReservedReportee });
                else if (randomNumber >= 90 && randomNumber < 95)
                    results.PrefillResult = new AltinnPrefillResult() { Message = "Unable to reach", AltinnCommunicationResult = AltinnCommunicationResultType.UnableToReachReceiver };
                //results.Add(new DistributionResult(DistributionComponent.Prefill) { Message = "Unable to reach", Step = DistributionStep.UnableToReachReceiver });
                else
                    results.PrefillResult = new AltinnPrefillResult() { Message = "Error", AltinnCommunicationResult = AltinnCommunicationResultType.UnkownErrorOccurred };
                //results.Add(new DistributionResult(DistributionComponent.Prefill) { Message = "Error", Step = DistributionStep.UnkownErrorOccurred });
            }

            return results;
        }

        public Task<IAltinnPrefilledDistributionResult> SendCorrespondenceAsync(IDistributionMessage altinnMessage, string prefillReferenceId)
        {
            throw new NotImplementedException();
        }
    }
}
