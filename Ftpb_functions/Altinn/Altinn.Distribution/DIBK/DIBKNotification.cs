﻿using Microsoft.Extensions.Logging;

namespace Altinn.Distribution.DIBK
{
    public class DIBK_AltinnNotification : AltinnNotification
    {
        public DIBK_AltinnNotification(ILogger<DIBK_AltinnNotification> logger, DIBK_CorrespondenceAdapter correspondenceAdapter)
            : base(logger, correspondenceAdapter) { }
    }
}
