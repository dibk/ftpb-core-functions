﻿using Altinn2.Adapters;
using Altinn2.Adapters.Bindings;
using Altinn2.Adapters.WS;
using Altinn2.Adapters.WS.Correspondence;
using Altinn2.Adapters.WS.Prefill;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Altinn.Distribution.DIBK
{
    public class DIBK_PrefillClient : PrefillClient
    {
        public DIBK_PrefillClient(IBindingFactory bindingFactory, IOptions<AltinnSettings> connectionOptions, IOptions<DIBK_AltinnServiceOwnerSettings> serviceOwnerSettings, ILogger<DIBK_PrefillClient> log)
            : base(bindingFactory, connectionOptions, serviceOwnerSettings, log) { }
    }

    public class DIBK_CorrespondenceClient : CorrespondenceClient
    {
        public DIBK_CorrespondenceClient(ILogger<DIBK_CorrespondenceClient> logger, IBindingFactory bindingFactory, IOptions<AltinnSettings> connectionOptions, IOptions<DIBK_AltinnServiceOwnerSettings> serviceOwnerSettings)
            : base(logger, bindingFactory, connectionOptions, serviceOwnerSettings) { }
    }

    public class DIBK_PrefillAdapter : PrefillAdapter
    {
        public DIBK_PrefillAdapter(ILogger<DIBK_PrefillAdapter> logger, IPrefillFormTaskBuilder prefillFormTaskBuilder, DIBK_PrefillClient altinnPrefillClient)
            : base(logger, prefillFormTaskBuilder, altinnPrefillClient) { }
    }

    public class DIBK_CorrespondenceAdapter : CorrespondenceAdapter
    {
        public DIBK_CorrespondenceAdapter(ILogger<DIBK_CorrespondenceAdapter> logger, IOptions<DIBK_AltinnServiceOwnerSettings> serviceOwnerSettings, DIBK_CorrespondenceClient correspondenceClient)
            : base(logger, serviceOwnerSettings, correspondenceClient) { }
    }

    public class DIBK_Altinn2Distribution : Altinn2Distribution
    {
        public DIBK_Altinn2Distribution(ILogger<DIBK_Altinn2Distribution> logger, DIBK_PrefillAdapter prefillAdapter, DIBK_CorrespondenceAdapter correspondenceAdapter, IOptions<AltinnSettings> connectionOptions)
            : base(logger, prefillAdapter, correspondenceAdapter, connectionOptions) { }
    }
}