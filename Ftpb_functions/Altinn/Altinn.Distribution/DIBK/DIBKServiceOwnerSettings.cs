﻿using Altinn2.Adapters.WS;

namespace Altinn.Distribution.DIBK
{
    public class DIBK_AltinnServiceOwnerSettings : ServiceOwnerSettings
    {
        public static string ConfigSection => "DIBK-AltinnSettings";
    }
}
