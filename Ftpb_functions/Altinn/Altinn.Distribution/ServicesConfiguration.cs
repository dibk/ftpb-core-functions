using Altinn.Common.Interfaces;
using Altinn.Distribution.DIBK;
using Altinn2.Adapters.Bindings;
using Altinn2.Adapters.WS;
using Altinn2.Adapters.WS.AltinnStandaloneNotification;
using Altinn2.Adapters.WS.Correspondence;
using Altinn2.Adapters.WS.Prefill;
using Altinn3.Adapters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Altinn.Distribution
{
    public static class ServicesConfiguration
    {
        public static IServiceCollection AddAltinn3Distribution(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<Altinn3Settings>(configuration.GetSection(Altinn3Settings.SectionName));
            services.AddAltinn3PrefillService(configuration);
            services.AddScoped<Altinn3Distribution>();
            services.AddScoped<IStandaloneNotificationClient, StandAloneNotificationClient>();
            services.AddScoped<CorrespondenceAdapter>();

            return services;
        }
        public static IServiceCollection AddAltinn2Distribution(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IBindingFactory, BindingFactory>();
            services.AddScoped<IBinding, MtomBindingProvider>();
            services.AddScoped<IBinding, BasicBindingProvider>();

            services.AddScoped<DIBK_PrefillClient>();
            services.AddScoped<DIBK_PrefillAdapter>();
            services.AddScoped<IPrefillFormTaskBuilder, PrefillFormTaskBuilder>();

            services.AddScoped<DIBK_CorrespondenceClient>();
            services.AddScoped<DIBK_CorrespondenceAdapter>();
            services.AddScoped<ICorrespondenceBuilder, CorrespondenceBuilder>();

            services.Configure<ServiceOwnerSettings>(configuration.GetSection(DIBK_AltinnServiceOwnerSettings.ConfigSection));
            services.Configure<AltinnSettings>(configuration.GetSection(AltinnSettings.ConfigSection));

            services.AddScoped<IDistributionAdapter, DIBK_Altinn2Distribution>();

            return services;
        }
        public static IServiceCollection AddAltinnNotification(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IBindingFactory, BindingFactory>();
            services.AddScoped<IBinding, MtomBindingProvider>();
            services.AddScoped<IBinding, BasicBindingProvider>();

            services.AddScoped<DIBK_CorrespondenceClient, DIBK_CorrespondenceClient>();
            services.AddScoped<DIBK_CorrespondenceAdapter, DIBK_CorrespondenceAdapter>();
            services.AddScoped<ICorrespondenceBuilder, CorrespondenceBuilder>();

            services.Configure<DIBK_AltinnServiceOwnerSettings>(configuration.GetSection(DIBK_AltinnServiceOwnerSettings.ConfigSection));
            services.Configure<AltinnSettings>(configuration.GetSection(AltinnSettings.ConfigSection));

            services.AddScoped<INotificationAdapter, DIBK_AltinnNotification>();

            return services;
        }
    }
}
