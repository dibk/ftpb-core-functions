﻿using Altinn.Common;
using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using Altinn2.Adapters.WS.Correspondence;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace Altinn.Distribution
{
    public class Altinn3Distribution : IDistributionAdapter
    {
        private readonly IPrefillAdapter _prefillAdapter;
        private readonly ICorrespondenceAdapter _correspondenceAdapter;
        private readonly IOptions<CorrespondenceConnectionSettings> _connectionOptions;
        private readonly ILogger<Altinn3Distribution> _logger;

        public Altinn3Distribution(
            IPrefillAdapter prefillAdapter, 
            ICorrespondenceAdapter correspondenceAdapter, 
            ILogger<Altinn3Distribution> logger)
        {
            _logger = logger;
            _prefillAdapter = prefillAdapter;
            _correspondenceAdapter = correspondenceAdapter;
        }

        public async Task<IAltinnPrefilledDistributionResult> SendPrefillAndCorrespondenceAsync(IDistributionMessage altinnMessage)
        {
            var distResult = new Altinn3PrefillDistributionResult();
            var prefillResult = await _prefillAdapter.SendPrefill(altinnMessage);
            distResult.PrefillResult = prefillResult;

            distResult.CorrespondenceResult = new AltinnCorrespondenceResult
            {
                AltinnCommunicationResult = AltinnCommunicationResultType.Sent
            };
            
            return distResult;
        }

        public Task<IAltinnPrefilledDistributionResult> SendCorrespondenceAsync(IDistributionMessage altinnMessage, string prefillReferenceId)
        {
            throw new System.NotImplementedException();
        }
    }

    public class Altinn3PrefillDistributionResult : IAltinnPrefilledDistributionResult
    {
        public AltinnPrefillResult PrefillResult { get; set; }
        public AltinnCorrespondenceResult CorrespondenceResult { get; set; }
        
        public AltinnCommunicationResultType FinalState 
        {
            get
            {
                if (CorrespondenceResult != null)
                    return CorrespondenceResult.AltinnCommunicationResult;

                return PrefillResult.AltinnCommunicationResult;
            }
        }
        
        public string ResultMessage
        {
            get
            {
                if (CorrespondenceResult != null)
                    return CorrespondenceResult.Message;

                return PrefillResult.Message;
            }
        }

        public bool IsSuccessfull()
        {
            if (PrefillResult != null && CorrespondenceResult != null)
                if (PrefillResult.IsSuccessfull() && CorrespondenceResult.IsSuccessfull())
                    return true;

            return false;
        }
    }
}
