﻿namespace Altinn.Common
{

    public class AltinnResult
    {
        public string Message { get; set; }
        public AltinnCommunicationResultType AltinnCommunicationResult { get; set; }
        public bool IsSuccessfull()
        {
            return AltinnCommunicationResult == AltinnCommunicationResultType.Sent || AltinnCommunicationResult == AltinnCommunicationResultType.NotUsed;
        }
    }
}
