﻿using Altinn.Common.Models;
using System.Threading.Tasks;

namespace Altinn.Common.Interfaces
{
    public interface IDistributionAdapter
    {
        Task<IAltinnPrefilledDistributionResult> SendPrefillAndCorrespondenceAsync(IDistributionMessage altinnMessage);
        Task<IAltinnPrefilledDistributionResult> SendCorrespondenceAsync(IDistributionMessage altinnMessage, string prefillReferenceId);
    }
}
