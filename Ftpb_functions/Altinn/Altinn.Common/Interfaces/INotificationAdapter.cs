﻿using Altinn.Common.Models;
using System.Threading.Tasks;

namespace Altinn.Common.Interfaces
{
    public interface INotificationAdapter
    {
        Task<AltinnNotificationCorrespondenceResult> SendNotificationAsync(AltinnMessageBase altinnMessage);
    }
}