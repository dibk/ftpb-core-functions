﻿using Altinn.Common.Models;
using System.Threading.Tasks;

namespace Altinn.Common.Interfaces
{
    public interface IPrefillAdapter
    {
        //PrefillResult SendPrefill(AltinnDistributionMessage altinnDistributionMessage);
        Task<AltinnPrefillResult> SendPrefill(IDistributionMessage altinnDistributionMessage);
    }
}