﻿using Altinn.Common.Models;
using System.Threading.Tasks;

namespace Altinn.Common.Interfaces
{
    public interface ICorrespondenceAdapter
    {
        Task<AltinnCorrespondenceResult> SendMessageAsync(AltinnMessageBase altinnMessage);
        Task<AltinnCorrespondenceResult> SendMessageAsync(AltinnMessageBase altinnMessage, string externalShipmentReference);
    }
}
