﻿using System.Threading.Tasks;

namespace Altinn.Common.Interfaces
{
    public interface IStandaloneNotificationClient
    {
        Task<IAltinnPrefilledDistributionResult> SendStandaloneNotification(string receiver, string emailContent, string reportee, string notificationTemplate, string emailSubject);
        Task<AltinnCorrespondenceResult> SendRegularMessageWithContentTemporarySolution(
                string toEmail,
                string reportee,
                string emailContent,
                string smsContent,
                string notificationTemplate,
                string subject,
                string externalShipmentReference,
                string archiveReference,
                string messageTitle,
                string messageSummary,
                string messageBody,
                string linkToSchema
            );
    }
}
