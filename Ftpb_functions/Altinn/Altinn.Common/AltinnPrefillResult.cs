﻿using System;

namespace Altinn.Common
{
    public class AltinnPrefillResult : AltinnResult
    {
        public string PrefillReferenceId { get; set; }
        public string PrefillAltinnReceiptId { get; set; }
        public DateTime PrefillAltinnReceivedTime { get; set; }
    }
}
