﻿namespace Altinn.Common.Models
{
    public class AltinnReceiver
    {
        public AltinnReceiverType Type { get; set; }
        public string Id { get; set; }
    }
}
