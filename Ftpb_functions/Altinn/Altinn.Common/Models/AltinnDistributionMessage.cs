﻿using System;
using System.Collections.Generic;

namespace Altinn.Common.Models
{
    public abstract class AltinnMessageBase
    {
        public AltinnMessageBase()
        {
            this.Attachments = new List<Attachment>();
            this.MessageData = new MessageDataType();
            this.Receiver = new AltinnReceiver();
            this.ReplyLink = new ReplyLink();
        }
        public MessageDataType MessageData { get; set; }
        public IEnumerable<Attachment> Attachments { get; set; }
        public AltinnReceiver Receiver { get; set; }
        public string ArchiveReference { get; set; }
        /// <summary>
        /// Ignores if a receiver has opted out of electronic communication in Altinn
        /// </summary>
        public bool RespectReservable { get; set; } = true;
        public ReplyLink ReplyLink { get; set; }
    }

    public class AltinnMessage : AltinnMessageBase
    {}

    public class AltinnNotificationMessage : AltinnMessageBase
    {
        public AltinnNotificationMessage()
        {
            this.Notifications = new List<Notification>();
        }

        public IEnumerable<Notification> Notifications { get; set; }
        public string NotificationTemplate { get; set; }
        public string SenderEmail { get; set; }
    }

    public interface IDistributionMessage
    {
        AltinnNotificationMessage NotificationMessage { get; set; }
        string PrefillDataFormatId { get; set; }
        string PrefillDataFormatVersion { get; set; }
        string PrefillServiceCode { get; set; }
        string PrefillServiceEditionCode { get; set; }
        string PrefilledXmlDataString { get; set; }
        List<AltinnPrefillData> PrefillData { get; set; }
        string AppId { get; set; }
        Guid DistributionFormReferenceId { get; set; }
        int DaysValid { get; set; }
        DateTime? DueDate { get; set; }
    }

    public class AltinnDistributionMessage : IDistributionMessage
    {
        public AltinnDistributionMessage()
        {               
            this.NotificationMessage = new AltinnNotificationMessage();
        }        

        public AltinnNotificationMessage NotificationMessage { get; set; }
        public string PrefillDataFormatId { get; set; }
        public string PrefillDataFormatVersion { get; set; }
        public string PrefillServiceCode { get; set; }
        public string PrefillServiceEditionCode { get; set; }
        public string PrefilledXmlDataString { get; set; }
        public List<AltinnPrefillData> PrefillData { get; set; }
        public string AppId { get; set; }
        public Guid DistributionFormReferenceId { get; set; }
        public int DaysValid { get; set; }
        public DateTime? DueDate { get; set; }
        public bool DigitallyReserved { get; set; }
        public string ExpectedFormDataType { get; set; }
    }

    public class AltinnPrefillData
    {
        public AltinnPrefillData(string id, string content, string contentType, byte[] contentBytes, string filename)
        {
            Id = id;
            Content = content;
            ContentType = contentType;
            ContentBytes = contentBytes;
            Filename = filename ?? id;
        }

        public string Id { get; set; }
        public string Content { get; set; }
        public string ContentType { get; set; }
        public byte[] ContentBytes { get; set; }
        public string Filename { get; set;  }
    }
}
