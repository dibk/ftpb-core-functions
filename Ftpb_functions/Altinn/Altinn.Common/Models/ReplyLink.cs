﻿namespace Altinn.Common.Models
{
    public class ReplyLink
    {
        public string Url { get; set; }
        public string UrlTitle { get; set; }
    }
}
