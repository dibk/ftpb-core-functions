﻿namespace Altinn.Common
{
    public interface IAltinnPrefilledDistributionResult
    {
        AltinnPrefillResult PrefillResult { get; set; }
        AltinnCorrespondenceResult CorrespondenceResult { get; set; }
        AltinnCommunicationResultType FinalState { get; }
        string ResultMessage { get; }
        bool IsSuccessfull();
    }

    public class AltinnPrefilledDistributionResult : IAltinnPrefilledDistributionResult
    {
        public AltinnPrefillResult PrefillResult { get; set; }
        public AltinnCorrespondenceResult CorrespondenceResult { get; set; }
        public bool IsSuccessfull()
        {
            if (PrefillResult != null && CorrespondenceResult != null)
                if (PrefillResult.IsSuccessfull() && CorrespondenceResult.IsSuccessfull())
                    return true;

            return false;
        }

        public AltinnCommunicationResultType FinalState
        {
            get
            {
                if (CorrespondenceResult != null)
                    return CorrespondenceResult.AltinnCommunicationResult;

                return PrefillResult.AltinnCommunicationResult;
            }
        }

        public string ResultMessage
        {
            get
            {
                if (CorrespondenceResult != null)
                    return CorrespondenceResult.Message;

                return PrefillResult.Message;
            }
        }
    }
}
