﻿namespace Altinn.Common
{
    public class AltinnCorrespondenceResult : AltinnResult
    {
        public string CorrespondenceAltinnReceiptId { get; set; }
    }
}
