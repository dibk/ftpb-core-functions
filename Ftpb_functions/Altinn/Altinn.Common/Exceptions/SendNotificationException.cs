﻿using System;

namespace Altinn.Common.Exceptions
{
    public class SendNotificationException : Exception
    {
        public string Text { get; set; }
        public AltinnCommunicationResultType DistriutionStep { get; private set; }
        public SendNotificationException(string text, AltinnCommunicationResultType distriutionStep)
        {
            Text = text;
            DistriutionStep = distriutionStep;
        }
    }
}
