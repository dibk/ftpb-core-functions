﻿namespace Altinn.Common
{
    public class AltinnNotificationCorrespondenceResult
    {
        public AltinnCorrespondenceResult CorrespondenceResult { get; set; }
        public bool IsSuccessfull()
        {
            return CorrespondenceResult.IsSuccessfull();
        }
        public bool IsReserved()
        {
            return false;
        }

        public AltinnCommunicationResultType FinalState
        {
            get
            {
                return CorrespondenceResult.AltinnCommunicationResult;
            }
        }

        public string ResultMessage
        {
            get
            {
                return CorrespondenceResult.Message;
            }
        }
    }
}
