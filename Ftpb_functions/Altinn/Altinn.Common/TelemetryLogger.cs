﻿using System;

namespace Altinn.Common
{ 
    public class AltinnTelemetryLogger
    {
        public static void LogTelemetry<T>(string serviceType, Uri serviceUri, string serviceCode, string serviceEdition)
        {
            Serilog.Log.ForContext<AltinnTelemetryLogger>()
                .Information("Altinn {ServiceType} {ServiceCode} - {ServiceEdition} request Uri: {ServiceUri} sourceContext: {TelemetrySourceContext}", serviceType, serviceCode, serviceEdition, serviceUri, typeof(T).FullName);
        }
    }
}