﻿namespace Altinn.Common
{
    public enum AltinnCommunicationResultType
    {
        Sent,
        Failed,
        UnkownErrorOccurred,
        ReservedReportee,
        UnableToReachReceiver,
        NotUsed,
        SvarUtFailed
    }
}