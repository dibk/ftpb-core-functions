﻿using Altinn.Common;
using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using Altinn.Platform.Storage.Interface.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Altinn3.Adapters
{
    public class PrefillAdapter : IPrefillAdapter
    {
        private readonly Altinn3Service _altinnService;
        private readonly ILogger<PrefillAdapter> _logger;

        public PrefillAdapter(
            Altinn3Service altinnService,
            ILogger<PrefillAdapter> logger
            )
        {
            _altinnService = altinnService;
            _logger = logger;
        }

        public async Task<AltinnPrefillResult> SendPrefill(IDistributionMessage distributionMessage)
        {
            var receiverId = distributionMessage.NotificationMessage.Receiver.Id;
            InstanceOwner instanceOwner = new InstanceOwner();

            if (distributionMessage.NotificationMessage.Receiver.Type == AltinnReceiverType.Privatperson)
                instanceOwner.PersonNumber = receiverId;
            else
                instanceOwner.OrganisationNumber = receiverId;

            var instanceTemplate = new Instance
            {
                AppId = distributionMessage.AppId,
                InstanceOwner = instanceOwner,
                //VisibleAfter = DateTime.Now.AddDays(14)
            };

            var contentBuilder = new MultipartContentBuilder(instanceTemplate);

            foreach (var prefillData in distributionMessage.PrefillData)
            {
                var bytes = prefillData.Content != null ? Encoding.UTF8.GetBytes(prefillData.Content) : prefillData.ContentBytes;
                var memoryStream = new MemoryStream(bytes);

                contentBuilder.AddDataElement(prefillData.Id, memoryStream, prefillData.ContentType, prefillData.Filename);
            }

            var prefillResult = new AltinnPrefillResult();

            try
            {
                var altinnDistributionMessage = distributionMessage as AltinnDistributionMessage;
                var sw = new Stopwatch();
                var content = contentBuilder.Build();
                sw.Start();
                Instance instanceResult = await _altinnService.PostPrefilledInstanceAsync(content, distributionMessage.AppId, altinnDistributionMessage.ExpectedFormDataType);
                sw.Stop();
                _logger.LogInformation("Elapsed time for sending prefill to Altinn: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);

                if (instanceResult == null)
                {
                    prefillResult.AltinnCommunicationResult = AltinnCommunicationResultType.UnableToReachReceiver;
                    prefillResult.PrefillAltinnReceiptId = null;
                    prefillResult.PrefillReferenceId = null;
                    prefillResult.PrefillAltinnReceivedTime = DateTime.Now;
                    _logger.LogError($"Failed to create Altinn instance for {distributionMessage.DistributionFormReferenceId}");
                    return prefillResult;
                }
                else
                {
                    _logger.LogInformation($"Instance {instanceResult.Id} created");
                    prefillResult.AltinnCommunicationResult = AltinnCommunicationResultType.Sent;
                    prefillResult.PrefillAltinnReceiptId = instanceResult.Id;
                    prefillResult.PrefillReferenceId = instanceResult.SelfLinks.Apps;
                    prefillResult.PrefillAltinnReceivedTime = DateTime.Now;
                }
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "Kunne ikke sende preutfylt instans til Altinn!");
                throw;
            }
            return prefillResult;
        }
    }
}
