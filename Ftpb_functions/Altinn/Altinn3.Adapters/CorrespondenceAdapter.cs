﻿using Altinn.Common;
using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Altinn3.Adapters
{
    public class CorrespondenceAdapter : ICorrespondenceAdapter
    {
        private readonly ILogger<CorrespondenceAdapter> _logger;
        private readonly IStandaloneNotificationClient _client;
        private readonly Altinn3Settings _altinn3Settings;

        public CorrespondenceAdapter(ILogger<CorrespondenceAdapter> logger, IStandaloneNotificationClient client, IOptions<Altinn3Settings> altinn3Settings)
        {
            _logger = logger;
            _client = client;
            _altinn3Settings = altinn3Settings.Value;
        }

        public Task<AltinnCorrespondenceResult> SendMessageAsync(AltinnMessageBase altinnMessage)
        {
            throw new NotImplementedException();
        }

        public Task<AltinnCorrespondenceResult> SendMessageAsync(AltinnMessageBase altinnMessage, string externalShipmentReference)
        {
            throw new NotImplementedException();
        }

        public async Task<AltinnCorrespondenceResult> SendCorrespondence(IDistributionMessage altinnDistributionMessage, string prefillAltinnReceiptId)
        {
            var altinnCorrespondenceResult = new AltinnCorrespondenceResult();
            if (altinnDistributionMessage.NotificationMessage.Notifications.Count() > 0)
            {
                var notification = altinnDistributionMessage.NotificationMessage.Notifications.First();

                string linkToSchema = $"{_altinn3Settings.BaseUrl}/{altinnDistributionMessage.AppId}/#/instance/{prefillAltinnReceiptId}";

                try
                {
                    altinnCorrespondenceResult = await _client.SendRegularMessageWithContentTemporarySolution(
                        notification.Receiver,
                        altinnDistributionMessage.NotificationMessage.Receiver.Id,
                        notification.EmailContent,
                        notification.SmsContent,
                        altinnDistributionMessage.NotificationMessage.NotificationTemplate,
                        notification.EmailSubject,
                        altinnDistributionMessage.DistributionFormReferenceId.ToString(),
                        altinnDistributionMessage.NotificationMessage.ArchiveReference,
                        altinnDistributionMessage.NotificationMessage.MessageData.MessageTitle,
                        altinnDistributionMessage.NotificationMessage.MessageData.MessageSummary,
                        altinnDistributionMessage.NotificationMessage.MessageData.MessageBody,
                        linkToSchema
                        );
                }
                catch (Exception exception)
                {
                    _logger.LogError(exception, "Kunne ikke sende preutfylt instans til Altinn!");
                    throw;
                }
                //await _client.SendStandaloneNotification(
                //    notification.Receiver,
                //    altinnDistributionMessage.NotificationMessage.Receiver.Id,
                //    notification.EmailContent,
                //    altinnDistributionMessage.NotificationMessage.NotificationTemplate,
                //    notification.EmailSubject
                //    );

            }
            return altinnCorrespondenceResult;
        }

    }
}
