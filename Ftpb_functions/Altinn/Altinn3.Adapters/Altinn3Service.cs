using Altinn.Platform.Storage.Interface.Models;
using FtB_Common;
using FtB_Common.BusinessModels;
using FtB_Common.Constants;
using FtB_Common.Exceptions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Altinn3.Adapters
{
    public class Altinn3Service
    {
        private readonly Altinn3Client _altinnClient;
        private readonly ILogger<Altinn3Service> _logger;
        private readonly EnvironmentProvider _env;
        private readonly Altinn3Settings _settings;

        public Altinn3Service(Altinn3Client altinnClient,
                              IOptions<Altinn3Settings> options,
                              ILogger<Altinn3Service> logger,
                              EnvironmentProvider env)
        {
            _altinnClient = altinnClient;
            _settings = options.Value;
            _logger = logger;
            _env = env;
        }

        public async Task<Instance> PostPrefilledInstanceAsync(MultipartContent content, string appId, string expectedFormDataType)
        {
            var requestUri = $"{appId}/instances";

            var request = new HttpRequestMessage(HttpMethod.Post, requestUri)
            {
                Content = content
            };

            HttpResponseMessage response = null;
            try
            {
                response = await _altinnClient.SendRequestAsync(request);

                var result = await response.Content.ReadAsStringAsync();
                Altinn.Common.AltinnTelemetryLogger.LogTelemetry<Altinn3Service>("Prefill-Altinn3", new Uri(_settings.BaseUrl), appId, string.Empty);
                var instance = JsonConvert.DeserializeObject<Instance>(result);

                if (_env.IsDevelopment)
                    await AddTagToFormDataDataTypeAsync(expectedFormDataType, instance);

                return instance;
            }
            catch (Exception exception)
            {
                var errorResponse = await response?.Content?.ReadAsStringAsync();
                _logger.LogError(exception, "Unable to create prefilled instance. Statuscode: {StatusCode} - Response message: {ResponseMessage}", response.StatusCode, errorResponse);
                response?.Dispose();
                return null;
            }
        }

        private async Task AddTagToFormDataDataTypeAsync(string expectedFormDataType, Instance instance)
        {
            var dataElement = instance?.Data?.FirstOrDefault(d => d.DataType.Equals(expectedFormDataType));
            if (dataElement != null)
            {
                var dataElementId = dataElement.Id;

                var requestUri = $"{instance.AppId}/instances/{instance.Id}/data/{dataElementId}/tags";
                var contentBody = new StringContent($"\"{Tags.DevelopmentTagValue}\"");

                try
                {                    
                    var requestMessage = new HttpRequestMessage(HttpMethod.Post, requestUri)
                    {
                        Content = contentBody
                    };
                    requestMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    using var response = await _altinnClient.SendRequestAsync(requestMessage);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Failed to add tag to form data type");
                }
            }
        }

        public async Task<Instance> GetAltinnInstanceAsync(AltinnEvent altinnEvent)
        {
            _logger.LogInformation("Fetching Altinn instance. Source: {altinnEvent}", altinnEvent.Source);
            var requestUri = _settings.IsLocal ? altinnEvent.Source.Replace("https", "http") : altinnEvent.Source;
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, requestUri);

            HttpResponseMessage response = null;
            try
            {
                response = await _altinnClient.SendRequestAsync(requestMessage);
                await response.Content.ReadAsStringAsync();

                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Instance>(result);
            }
            catch (Exception exception)
            {
                _logger.LogError(exception,
                    "Failed to fetch Altinn instance. StatusCode: {statusCode}. Source: {altinnEvent}",
                    response.StatusCode, altinnEvent);

                response?.Dispose();
                throw new CancelProcessException($"Failed to fetch Altinn instance. ResourceInstance: {altinnEvent.ResourceInstance}");
            }
        }

        public async Task<Stream> GetStreamFromAltinnInstanceAsync(DataElement dataItem)
        {
            _logger.LogInformation("Start fetching data from Altinn");
            var requestUri = _settings.IsLocal ? dataItem.SelfLinks.Apps.Replace("https", "http") : dataItem.SelfLinks.Platform;
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, requestUri)
            {
                Headers = { Accept = { new MediaTypeWithQualityHeaderValue(dataItem.ContentType) } }
            };

            HttpResponseMessage response = null;
            try
            {
                response = await _altinnClient.SendRequestAsync(requestMessage);

                _logger.LogInformation("Data fetched from Altinn. StatusCode: {statusCode}", response.StatusCode);
                return await response.Content.ReadAsStreamAsync();
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "StatusCode: {statusCode}", response?.StatusCode);
                response?.Dispose();
                throw;
            }
        }
    }
}