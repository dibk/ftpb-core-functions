﻿using FtB_Common.Enums;
using FtB_Common.TokenCache;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Altinn3.Adapters
{
    public class AltinnTokenExchanger
    {
        private HttpClient _httpClient { get; }
        private readonly TokenCache _tokenCache;

        private bool IsLocal => _httpClient.BaseAddress.ToString() == "http://local.altinn.cloud/";

        public AltinnTokenExchanger(HttpClient httpClient, TokenCache tokenCache)
        {
            _httpClient = httpClient;
            _tokenCache = tokenCache;
        }

        public async Task<string> ExchangeMaskinportenTokenToAltinnToken(string maskinportenToken)
        {
            if (_tokenCache.TryGetTokenValue(TokenTypeEnum.Altinn, null, maskinportenToken, out var altinnToken))
                return altinnToken;

            var apiEndpoint = "authentication/api/v1/exchange/maskinporten";

            if (IsLocal)
            {
                apiEndpoint = "Home/GetTestOrgToken/dibk";
            }

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", maskinportenToken);

            using var response = await _httpClient.GetAsync(apiEndpoint);

            altinnToken = await response.Content.ReadAsStringAsync();

            _tokenCache.SetToken(altinnToken, TokenTypeEnum.Altinn, null, maskinportenToken);

            return altinnToken;
        }
    }
}
