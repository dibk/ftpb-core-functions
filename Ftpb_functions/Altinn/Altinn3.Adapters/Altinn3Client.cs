﻿using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Threading.Tasks;

namespace Altinn3.Adapters
{
    public class Altinn3Client
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger<Altinn3Client> _logger;

        public Altinn3Client(IHttpClientFactory clientFactory, ILogger<Altinn3Client> logger)
        {
            _httpClient = clientFactory.CreateClient(Altinn3Settings.Altinn3HttpClientName);
            _logger = logger;
        }

        public async Task<HttpResponseMessage> SendRequestAsync(HttpRequestMessage request)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.SendAsync(request).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                return response;
            }
            catch (HttpRequestException hre)
            {
                _logger.LogError(hre, "Request to Altinn on {RequestUri} failed", request.RequestUri);
                throw;
            }
        }
    }
}