﻿namespace Altinn3.Adapters
{
    public class Altinn3Settings
    {
        public static readonly string SectionName = "Altinn3Settings";
        public static readonly string ConfigurationKeyBaseUrl = "Altinn3Settings:BaseUrl";
        public static readonly string ConfigurationKeyExchangeBaseUrl = "Altinn3Settings:ExchangeBaseUrl";
        public static readonly string Altinn3HttpClientName = "Altinn3HttpClient";

        public string BaseUrl { get; set; }
        public string ExchangeBaseUrl { get; set; }
        public string Scopes { get; set; }
        public string ConsumerOrg { get; set; }
        public bool IsLocal => BaseUrl == "http://altinn3local.no/" || BaseUrl == "http://local.altinn.cloud/";
        public readonly string LocalAuthUri = "Home/GetTestOrgToken/dibk";
    }
}