﻿using Altinn.Common.Interfaces;
using Maskinporten;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Altinn3.Adapters
{
    public static class ServicesConfiguration
    {
        public static IServiceCollection AddAltinn3Client(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddMaskinportenClient(configuration);
            services.Configure<Altinn3Settings>(configuration.GetSection(Altinn3Settings.SectionName));
            services.AddHttpClient(Altinn3Settings.Altinn3HttpClientName, client =>
            {
                client.BaseAddress = new Uri(configuration[Altinn3Settings.ConfigurationKeyBaseUrl]);
            }).AddHttpMessageHandler<AltinnBearerTokenMessageHandler>();

            services.AddHttpClient<AltinnTokenExchanger>(client =>
            {
                client.BaseAddress = new Uri(configuration[Altinn3Settings.ConfigurationKeyExchangeBaseUrl]);
            });

            services.AddScoped<AltinnBearerTokenMessageHandler>();
            services.AddScoped<Altinn3Client>();
            services.AddScoped<Altinn3Service>();

            return services;
        }

        public static IServiceCollection AddAltinn3PrefillService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAltinn3Client(configuration);
                        
            services.AddScoped<IPrefillAdapter, PrefillAdapter>();
            services.AddScoped<ICorrespondenceAdapter, CorrespondenceAdapter>();

            return services;
        }
    }
}