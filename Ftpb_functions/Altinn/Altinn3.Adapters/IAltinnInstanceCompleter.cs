﻿using System.Threading.Tasks;

namespace Altinn3.Adapters
{
    public interface IAltinnInstanceCompleter
    {
        public Task<bool> PerformCompleteInstanceAsync(string archiveReference);
    }
}