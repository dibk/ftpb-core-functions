﻿using Maskinporten;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Altinn3.Adapters
{
    public class AltinnBearerTokenMessageHandler : DelegatingHandler
    {
        private readonly ILogger<AltinnBearerTokenMessageHandler> _logger;
        private readonly MaskinportenClient _maskinportenClient;
        private readonly AltinnTokenExchanger _altinnTokenExchanger;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly Altinn3Settings _settings;

        public AltinnBearerTokenMessageHandler(ILogger<AltinnBearerTokenMessageHandler> logger,
                                               MaskinportenClient maskinportenClient,
                                               AltinnTokenExchanger altinnTokenExchanger,
                                               IHttpClientFactory httpClientFactory,
                                               IOptions<Altinn3Settings> settings)
        {
            _logger = logger;
            _maskinportenClient = maskinportenClient;
            _altinnTokenExchanger = altinnTokenExchanger;
            _httpClientFactory = httpClientFactory;
            _settings = settings.Value;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            var token = await GetToken();

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

            return await base.SendAsync(request, cancellationToken);
        }

        private async Task<string> GetToken()
        {
            return _settings.IsLocal ?
                await GetLocalToken() :
                await GetAltinnAccesToken();
        }

        private async Task<string> GetAltinnAccesToken()
        {
            string maskinportenToken = await _maskinportenClient.GetAccessToken(_settings.Scopes.Split(";"));
            return await _altinnTokenExchanger.ExchangeMaskinportenTokenToAltinnToken(maskinportenToken);
        }

        private async Task<string> GetLocalToken()
        {
            try
            {
                using var client = _httpClientFactory.CreateClient();
                using var response = await client.GetAsync(_settings.LocalAuthUri);
                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to request access token");
                throw;
            }
        }
    }
}