﻿using FtB_Common.Enums;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Altinn3.Adapters
{
    public class AltinnInstanceCompleter : IAltinnInstanceCompleter
    {
        private readonly ILogger<AltinnInstanceCompleter> _logger;
        private readonly IBlobOperations _blobOperations;
        private readonly Altinn3Client _altinn3Client;        

        public AltinnInstanceCompleter(ILogger<AltinnInstanceCompleter> logger,
                                       IBlobOperations blobOperations,
                                       Altinn3Client altinn3Client)
        {
            _logger = logger;
            _blobOperations = blobOperations;
            _altinn3Client = altinn3Client;
        }

        private async Task<bool> CompleteAltinn3Instance(string instanceSource)
        {
            HttpResponseMessage response = null;

            try
            {
                if (instanceSource.StartsWith("https://local.altinn.cloud/"))
                    instanceSource = instanceSource.Replace("https", "http");

                var requestUri = $"{instanceSource}/complete";

                var requestMessage = new HttpRequestMessage(HttpMethod.Post, requestUri);

                response = await _altinn3Client.SendRequestAsync(requestMessage);

                _logger.LogInformation($"Instance {instanceSource} completed");

                return true;
            }
            catch (HttpRequestException httpException)
            {
                if (httpException.StatusCode == System.Net.HttpStatusCode.Conflict
                    || httpException.StatusCode == System.Net.HttpStatusCode.Forbidden) //
                {
                    _logger.LogError(httpException, $"Instance {instanceSource} already completed, Altinn reported status: {httpException.StatusCode}");
                    return true;
                }

                if (httpException.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    _logger.LogError(httpException, $"Instance {instanceSource} not found");
                    return false;
                }

                _logger.LogError(httpException, $"Unable to complete instance {instanceSource}. Statuscode: {response.StatusCode}");
                response.Dispose();
                return false;
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, $"Unable to complete instance {instanceSource}. Statuscode: {response.StatusCode}");
                response.Dispose();
                throw;
            }
        }

        private static string GetAltinn3Source(BlobStreamItem formDataBlob)
        {
            return formDataBlob.Metadata
                .Where(x => x.Key.Equals(BlobStorageMetadataKeys.InstanceSource, StringComparison.OrdinalIgnoreCase))
                .Select(x => x.Value).ToList().FirstOrDefault();
        }

        private async Task<string> GetAltinn3SourceFromFormDataAsync(string archiveReference)
        {
            var blobStorageTypeFormdata = new List<string> { BlobStorageMetadataTypes.FormData };
            var blobs = await _blobOperations.GetAllBlobsByMetadataTypeAsync(BlobStorageEnum.Private, archiveReference, blobStorageTypeFormdata);

            if (!blobs.Any())
            {
                throw new KeyNotFoundException("No formdata found");
            }
            var formDataBlob = blobs.First();

            var altinn3Source = GetAltinn3Source(formDataBlob);
            if (!string.IsNullOrEmpty(altinn3Source))
            {
                return altinn3Source;
            }
            else
            {
                throw new Exception("Missing Altinn3Source in Formdata metadata.");
            }
        }

        public async Task<bool> PerformCompleteInstanceAsync(string archiveReference)
        {
            var instanceSource = await GetAltinn3SourceFromFormDataAsync(archiveReference);
            var altinn3CompleteSuccess = await CompleteAltinn3Instance(instanceSource);

            return altinn3CompleteSuccess;
        }
    }
}