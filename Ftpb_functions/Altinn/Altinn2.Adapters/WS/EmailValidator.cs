﻿namespace Altinn2.Adapters.WS
{
    public static class EmailValidator
    {
        public static bool IsValidEmailAddress(string email)
        {
            // https://stackoverflow.com/questions/1365407/c-sharp-code-to-validate-email-address
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
