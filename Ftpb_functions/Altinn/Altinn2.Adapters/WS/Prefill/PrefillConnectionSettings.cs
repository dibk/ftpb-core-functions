﻿namespace Altinn2.Adapters.WS.Prefill
{
    public class PrefillConnectionSettings
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string EndpointUrl { get; set; }
        public string ServiceOwnerCode { get; set; }
    }
}
