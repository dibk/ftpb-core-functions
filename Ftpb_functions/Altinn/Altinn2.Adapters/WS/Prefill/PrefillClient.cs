﻿using Altinn2.Adapters.Bindings;
using AltinnWebServices.WS.Prefill;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.ServiceModel;
using System.Threading.Tasks;

namespace Altinn2.Adapters.WS.Prefill
{
    public class PrefillClient : IPrefillClient
    {
        private readonly IBindingFactory bindingFactory;
        private readonly AltinnSettings _connectionOptions;
        private readonly ServiceOwnerSettings _serviceOwnerSettings;
        private readonly ILogger<PrefillClient> _log;
        //Move into separate class????
        private readonly string _externalBatchId = Guid.NewGuid().ToString();
        private readonly bool _doSaveFormTask = true;
        private readonly bool _doinstantiateFormTask = true;
        private readonly int? _caseId = null;
        private readonly string _instantitedOnBehalfOf = null;

        public PrefillClient(IBindingFactory bindingFactory, IOptions<AltinnSettings> connectionOptions, IOptions<ServiceOwnerSettings> serviceOwnerSettings, ILogger<PrefillClient> log)
        {
            this.bindingFactory = bindingFactory;
            _connectionOptions = connectionOptions.Value;
            _serviceOwnerSettings = serviceOwnerSettings.Value;
            _log = log;
        }

        public async Task<ReceiptExternal> SendPrefill(PrefillFormTask prefillFormTask, DateTime? dueDate)
        {
            var client = new PreFillExternalBasicClient(bindingFactory.GetBindingFor(BindingType.Mtom), new EndpointAddress(_connectionOptions.PrefillEndpointUrl));
            try
            {                
                prefillFormTask.ServiceOwnerCode = _serviceOwnerSettings.ServiceOwnerCode;

                var result = await client.SubmitAndInstantiatePrefilledFormTaskBasicAsync(_serviceOwnerSettings.UserName,
                    _serviceOwnerSettings.Password,
                    _externalBatchId,
                    prefillFormTask,
                    _doSaveFormTask,
                    _doinstantiateFormTask,
                    _caseId,
                    dueDate,
                    _instantitedOnBehalfOf).ConfigureAwait(true);
                
                client.Close();
                Altinn.Common.AltinnTelemetryLogger.LogTelemetry<PrefillClient>("Prefill", client.Endpoint.Address.Uri, prefillFormTask.ExternalServiceCode, prefillFormTask.ExternalServiceEditionCode.ToString());

                return result;
            }
            catch (FaultException<AltinnFault> af)
            {
                _log.LogError(af, af.Detail.ToStringExtended());
                client.Abort();
                throw;
            }
            catch (TimeoutException te)
            {
                _log.LogError(te, $"Timeout when communicating with Altinn 2 prefill service");
                client.Abort();
                throw;
            }            
            catch (CommunicationException ce)
            {
                _log.LogError(ce, $"Communication error occurred when communication with Altinn 2 prefill service");
                client.Abort();
                throw;
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Error occurred when sending prefill to Altinn");
                throw;
            }
        }        
    }
}
