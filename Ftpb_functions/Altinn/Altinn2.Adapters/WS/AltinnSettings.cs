﻿namespace Altinn2.Adapters.WS
{
    public class AltinnSettings
    {
        public const string ConfigSection = "AltinnSettings";
        public string PrefillEndpointUrl { get; set; }
        public string CorrespondenceEndpointUrl { get; set; }
        public string BaseAddress { get; set; }
        public string StandaloneNotificationEndpointUrl { get; set; }
    }

    public interface IServiceOwnerSettings
    {
        string UserName { get; set; }
        string Password { get; set; }
        string ServiceOwnerCode { get; set; }
        string MessageServiceCode { get; set; }
        string MessageServiceEdition { get; set; }
    }

    public class ServiceOwnerSettings : IServiceOwnerSettings
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ServiceOwnerCode { get; set; }
        public string MessageServiceCode { get; set; }
        public string MessageServiceEdition { get; set; }
    }
}
