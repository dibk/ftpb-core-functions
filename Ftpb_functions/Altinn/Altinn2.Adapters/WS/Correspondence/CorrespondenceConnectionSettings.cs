﻿namespace Altinn2.Adapters.WS.Correspondence
{
    public class CorrespondenceConnectionSettings
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ServiceOwnerCode { get; set; }
        public string BaseAddress { get; set; }
        public string EndpointUrl { get; set; }
    }
}
