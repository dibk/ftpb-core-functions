﻿using Altinn2.Adapters.Bindings;
using AltinnWebServices.WS.Correspondence;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.ServiceModel;
using System.Threading.Tasks;

namespace Altinn2.Adapters.WS.Correspondence
{
    public class CorrespondenceClient : ICorrespondenceClient 
    {
        private readonly ILogger<CorrespondenceClient> _logger;
        private readonly IBindingFactory _bindingFactory;
        private readonly AltinnSettings _connectionOptions;
        private readonly ServiceOwnerSettings _serviceOwnerSettings;

        public CorrespondenceClient(ILogger<CorrespondenceClient> logger, IBindingFactory bindingFactory, IOptions<AltinnSettings> connectionOptions, IOptions<ServiceOwnerSettings> serviceOwnerSettings)
        {            
            _logger = logger;
            _bindingFactory = bindingFactory;
            _connectionOptions = connectionOptions.Value;
            _serviceOwnerSettings = serviceOwnerSettings.Value;
        }

        public async Task<ReceiptExternal> SendCorrespondence(InsertCorrespondenceV2 correspondenceItem, string externalShipmentReference)
        {
            var client = new CorrespondenceAgencyExternalBasicClient(_bindingFactory.GetBindingFor(BindingType.Mtom), new EndpointAddress(_connectionOptions.CorrespondenceEndpointUrl));

            try
            {   
                var result = await client.InsertCorrespondenceBasicV2Async(_serviceOwnerSettings.UserName, _serviceOwnerSettings.Password, _serviceOwnerSettings.ServiceOwnerCode, externalShipmentReference, correspondenceItem);
                client.Close();
                Altinn.Common.AltinnTelemetryLogger.LogTelemetry<CorrespondenceClient>("Correspondence", client.Endpoint.Address.Uri, correspondenceItem.ServiceCode, correspondenceItem.ServiceEdition);

                return result.Body.InsertCorrespondenceBasicV2Result;

            }
            catch (FaultException<AltinnFault> af)
            {
                _logger.LogError(af, af.Detail.ToStringExtended());
                client.Abort();
                throw;
            }
            catch (TimeoutException te)
            {
                _logger.LogError(te, $"Timeout when communicating with Altinn 2 correspondence service");
                client.Abort();
                throw;
            }
            catch (CommunicationException ce)
            {
                _logger.LogError(ce, $"Communication error occurred when communication with Altinn 2 correspondence service");
                client.Abort();
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred when sending correspondence to Altinn");
                throw;
            }
        }
    }
}
