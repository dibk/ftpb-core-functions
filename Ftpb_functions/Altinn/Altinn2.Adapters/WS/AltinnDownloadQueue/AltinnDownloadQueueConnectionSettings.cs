﻿namespace Altinn2.Adapters.WS.AltinnDownloadQueue
{
    public class AltinnDownloadQueueConnectionSettings
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string EndpointUrl { get; set; }
    }
}
