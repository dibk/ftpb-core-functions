using Altinn.Common;
using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using Altinn2.Adapters.Bindings;
using Altinn2.Adapters.WS.Correspondence;
using AltinnWebServices.WS.Correspondence;
using AltinnWebServices.WS.Prefill.StandaloneNotification;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;

namespace Altinn2.Adapters.WS.AltinnStandaloneNotification
{
    public class StandAloneNotificationClient : IStandaloneNotificationClient
    {
        private readonly ServiceOwnerSettings _serviceOwnerSettings;
        private readonly IBindingFactory _bindingFactory;
        private readonly AltinnSettings _connectionOptions;
        private readonly ILogger<StandAloneNotificationClient> _log;

        public StandAloneNotificationClient(
            IBindingFactory bindingFactory,
            IOptions<AltinnSettings> connectionOptions,
            ILogger<StandAloneNotificationClient> log,
            IOptions<ServiceOwnerSettings> serviceOwnerSettings
            )
        {            
            _bindingFactory = bindingFactory;
            _connectionOptions = connectionOptions.Value;
            _log = log;
            _serviceOwnerSettings = serviceOwnerSettings.Value;
        }

        public async Task<IAltinnPrefilledDistributionResult> SendStandaloneNotification(string toEmail, string reportee, string emailContent, string notificationTemplate, string subject)
        {
            try
            {
                var request = new SendStandaloneNotificationBasicRequest();
                var standaloneNotificationBEList = new StandaloneNotificationBEList();
                AltinnWebServices.WS.Prefill.StandaloneNotification.ReceiverEndPointBEList receiverEndPoints = new AltinnWebServices.WS.Prefill.StandaloneNotification.ReceiverEndPointBEList();

                var emails = toEmail.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                var emailValidationResults = emails.Select(s => new Tuple<string, bool>(s?.Trim(), EmailValidator.IsValidEmailAddress(s)));
                if (emailValidationResults.Any(a => a.Item2 == true))
                    foreach (var email in emailValidationResults.Where(v => v.Item2))
                    {
                        if (EmailValidator.IsValidEmailAddress(email.Item1))
                            receiverEndPoints.Add(new AltinnWebServices.WS.Prefill.StandaloneNotification.ReceiverEndPoint
                            {
                                ReceiverAddress = email.Item1,
                                TransportType = AltinnWebServices.WS.Prefill.StandaloneNotification.TransportType.Email
                            });
                    }
                else
                    receiverEndPoints.Add(new AltinnWebServices.WS.Prefill.StandaloneNotification.ReceiverEndPoint
                    {
                        TransportType = AltinnWebServices.WS.Prefill.StandaloneNotification.TransportType.EmailPreferred
                    });

                string smsNotification = "Du har en melding fra Direktoratet for Byggkvalitet i Altinn"; //Resources.TextStrings.AltinnNotificationMessage;

                var emailContentLength = emailContent.Length;
                var emailContentPartOne = emailContent;
                var emailContentPartTwo = "";
                int messageMaxLength = 900;
                if (emailContentLength > messageMaxLength)
                {
                    emailContentPartOne = emailContent.Substring(0, messageMaxLength);
                    emailContentPartTwo = emailContent.Substring(messageMaxLength);
                }

                var standaloneNotification = new StandaloneNotification()
                {
                    FromAddress = "noreply@altinn.no",
                    IsReservable = true,
                    LanguageID = 1044,
                    NotificationType = notificationTemplate,
                    ReceiverEndPoints = receiverEndPoints,
                    ReporteeNumber = reportee,
                    Roles = null,
                    Service = null,
                    ShipmentDateTime = DateTime.Now,
                    TextTokens = new AltinnWebServices.WS.Prefill.StandaloneNotification.TextTokenSubstitutionBEList()
                {
                     new AltinnWebServices.WS.Prefill.StandaloneNotification.TextToken()
                    {
                        TokenNum = 0,
                        TokenValue = smsNotification
                    },
                     new AltinnWebServices.WS.Prefill.StandaloneNotification.TextToken()
                    {
                        TokenNum = 1,
                        TokenValue = subject
                    },
                     new AltinnWebServices.WS.Prefill.StandaloneNotification.TextToken()
                    {
                        TokenNum = 2,

                        TokenValue = emailContentPartOne
                    },
                     new AltinnWebServices.WS.Prefill.StandaloneNotification.TextToken()
                    {
                    TokenNum = 3,

                    TokenValue = emailContentPartTwo
                }
            },
                    UseServiceOwnerShortNameAsSenderOfSms = false,
                };

                standaloneNotificationBEList.Add(standaloneNotification);

                var client = new NotificationAgencyExternalBasicClient(_bindingFactory.GetBindingFor(BindingType.Mtom), new EndpointAddress(_connectionOptions.StandaloneNotificationEndpointUrl));

                try
                {
                    var result = await client.SendStandaloneNotificationBasicV3Async(_serviceOwnerSettings.UserName, _serviceOwnerSettings.Password, standaloneNotificationBEList);
                    Altinn.Common.AltinnTelemetryLogger.LogTelemetry<StandAloneNotificationClient>("Notification", client.Endpoint.Address.Uri, standaloneNotification.Service.ServiceCode, standaloneNotification.Service.ServiceEdition.ToString());
                    client.Close();
                }
                catch (FaultException<AltinnWebServices.WS.Prefill.StandaloneNotification.AltinnFault> af)
                {
                    _log.LogError(af, af.Detail.ToString());
                    client.Abort();
                    throw;
                }
                catch (TimeoutException te)
                {
                    _log.LogError(te, $"Timeout when communicating with Altinn 2 standalone notification service");
                    client.Abort();
                    throw;
                }
                catch (CommunicationException ce)
                {
                    _log.LogError(ce, $"Communication error occurred when communication with Altinn 2 standalone notification service");
                    client.Abort();
                    throw;
                }
                catch (Exception ex)
                {
                    _log.LogError(ex, "Error occurred when sending standalone notification request to Altinn");
                    throw;
                }
            }
            catch (Exception e)
            {
                _log.LogError(e.Message);
            }
            var altinResult = new AltinnPrefilledDistributionResult();

            return altinResult;
        }

        public async Task<AltinnCorrespondenceResult> SendRegularMessageWithContentTemporarySolution(
            string toEmail,
            string reportee,
            string emailContent,
            string smsContent,
            string notificationTemplate,
            string subject,
            string externalShipmentReference,
            string archiveReference,
            string messageTitle,
            string messageSummary,
            string messageBody,
            string linkToSchema)
        {
            var correspondenceResult = new AltinnCorrespondenceResult();
            CorrespondenceBuilder _correspondenceBuilder = new CorrespondenceBuilder(_serviceOwnerSettings.MessageServiceCode, _serviceOwnerSettings.MessageServiceEdition);

            _correspondenceBuilder.SetUpCorrespondence(reportee, archiveReference);
            messageBody = AddInstanceLinkToBody(messageBody, linkToSchema);
            messageBody = AddSchemaButtonToBody(messageBody, linkToSchema);

            _correspondenceBuilder.AddContent(messageTitle, messageSummary, messageBody);

            string smsNotification = smsContent;

            _correspondenceBuilder.AddEmailAndSmsNotification(NotificationEnums.NotificationCarrier.AltinnEmailPreferred,
                        "noreply@noreply.no",
                        toEmail,
                        subject,
                        emailContent,
                        notificationTemplate,
                        smsNotification
                        );

            InsertCorrespondenceV2 correspondenceItem = _correspondenceBuilder.Build();
            try
            {
                var client = new CorrespondenceAgencyExternalBasicClient(_bindingFactory.GetBindingFor(BindingType.Mtom), new EndpointAddress(_connectionOptions.CorrespondenceEndpointUrl));
                try
                {
                    var result = await client.InsertCorrespondenceBasicV2Async(
                        _serviceOwnerSettings.UserName,
                        _serviceOwnerSettings.Password,
                        _serviceOwnerSettings.ServiceOwnerCode,
                        externalShipmentReference,
                        correspondenceItem
                        );

                    AltinnTelemetryLogger.LogTelemetry<StandAloneNotificationClient>("Correspondence", client.Endpoint.Address.Uri, correspondenceItem.ServiceCode, correspondenceItem.ServiceEdition.ToString());
                    client.Close();

                    correspondenceResult.AltinnCommunicationResult = AltinnCommunicationResultType.Sent;
                    correspondenceResult.CorrespondenceAltinnReceiptId = result.Body.InsertCorrespondenceBasicV2Result.ReceiptId.ToString();
                }
                catch (FaultException<AltinnWebServices.WS.Prefill.StandaloneNotification.AltinnFault> af)
                {
                    correspondenceResult.AltinnCommunicationResult = AltinnCommunicationResultType.UnkownErrorOccurred;
                    correspondenceResult.Message = $"An error occurred when sending correspondence to Altinn - {af.Message}";
                    _log.LogError(af, af.Detail.ToString());
                    client.Abort();
                }
                catch (TimeoutException te)
                {
                    _log.LogError(te, $"Timeout when communicating with Altinn 2 correspondence service");
                    client.Abort();
                    throw;
                }
                catch (CommunicationException ce)
                {
                    correspondenceResult.AltinnCommunicationResult = AltinnCommunicationResultType.UnkownErrorOccurred;
                    correspondenceResult.Message = $"An error occurred when sending correspondence to Altinn - {ce.Message}";
                    _log.LogError(ce, $"Communication error occurred when communication with Altinn 2 correspondence service");
                    client.Abort();
                }
                catch (Exception ex)
                {
                    correspondenceResult.AltinnCommunicationResult = AltinnCommunicationResultType.UnkownErrorOccurred;
                    correspondenceResult.Message = $"An error occurred when sending correspondence to Altinn - {ex.Message}";
                    _log.LogError(ex, "Error occurred when sending correspondence request to Altinn");
                    client.Abort();

                }
            }
            catch (FaultException<AltinnWebServices.WS.Correspondence.AltinnFault> ex)
            {
                _log.LogError(ex, "Altinn Error Message occurred: {AltinnErrorMessage}", ex.Detail.AltinnErrorMessage);
                correspondenceResult.AltinnCommunicationResult = AltinnCommunicationResultType.UnkownErrorOccurred;
                correspondenceResult.Message = $"An error occurred when sending correspondence to Altinn - {ex.Message}";
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "An error occurred when sending correspondence to Altinn");
                correspondenceResult.AltinnCommunicationResult = AltinnCommunicationResultType.UnkownErrorOccurred;
                correspondenceResult.Message = $"An error occurred when sending correspondence to Altinn - {ex.Message}";
            }

            return correspondenceResult;
        }

        private string AddSchemaButtonToBody(string messageBody, string linkToSchema)
        {
            string buttonWithLinkToSchema = $"<br></br><a href='{linkToSchema}'><button type = 'button' class='a-btn mb-0 a-btn-no-textwrap'> Gå til skjema for uttalelse</button></a>";
            messageBody = messageBody.Insert(messageBody.Length - 1, buttonWithLinkToSchema);
            return messageBody;
        }

        private string AddInstanceLinkToBody(string messageBody, string linkToSchema)
        {
            return messageBody.Replace("dette svarskjemaet", $"<a href='{linkToSchema}'>dette svarskjemaet</a>");
        }
    }
}
