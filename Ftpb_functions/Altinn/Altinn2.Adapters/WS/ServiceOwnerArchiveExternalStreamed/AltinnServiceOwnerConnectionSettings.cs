﻿namespace Altinn2.Adapters.WS.ServiceOwnerArchiveExternalStreamed
{
    public class AltinnServiceOwnerConnectionSettings
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string EndpointUrl { get; set; }
    }
}
