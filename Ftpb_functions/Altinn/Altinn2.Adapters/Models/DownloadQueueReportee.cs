﻿namespace Altinn2.Adapters.Models
{
    public enum DownloadQueueReportee
    {
        Organisation,
        Person,
        SelfRegisteredUser
    }
}
