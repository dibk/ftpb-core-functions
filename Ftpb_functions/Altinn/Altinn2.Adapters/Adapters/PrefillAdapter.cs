﻿using Altinn.Common;
using Altinn.Common.Interfaces;
using Altinn.Common.Models;
using Altinn2.Adapters.WS.Prefill;
using AltinnWebServices.WS.Prefill;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Altinn2.Adapters
{
    public class PrefillAdapter : IPrefillAdapter
    {
        private readonly ILogger _logger;
        private readonly IPrefillFormTaskBuilder _prefillFormTaskBuilder;
        private readonly IPrefillClient _altinnPrefillClient;

        public PrefillAdapter(ILogger<PrefillAdapter> logger, IPrefillFormTaskBuilder prefillFormTaskBuilder, IPrefillClient altinnPrefillClient)
        {
            _logger = logger;
            _prefillFormTaskBuilder = prefillFormTaskBuilder;
            _altinnPrefillClient = altinnPrefillClient;
        }

        public async Task<AltinnPrefillResult> SendPrefill(IDistributionMessage altinnDistributionMessage)
        {   
            _prefillFormTaskBuilder.SetupPrefillFormTask(altinnDistributionMessage.PrefillServiceCode,
                    int.Parse(altinnDistributionMessage.PrefillServiceEditionCode),
                    altinnDistributionMessage.NotificationMessage.Receiver.Id,
                    altinnDistributionMessage.DistributionFormReferenceId.ToString(),
                    altinnDistributionMessage.DistributionFormReferenceId.ToString(),
                    altinnDistributionMessage.DistributionFormReferenceId.ToString(),
                    altinnDistributionMessage.DaysValid);

            _prefillFormTaskBuilder.AddPrefillForm(altinnDistributionMessage.PrefillDataFormatId,
                    int.Parse(altinnDistributionMessage.PrefillDataFormatVersion),
                    altinnDistributionMessage.PrefilledXmlDataString,
                    altinnDistributionMessage.DistributionFormReferenceId.ToString());

            if (altinnDistributionMessage.NotificationMessage.Notifications.Count() > 0)
            {
                var notification = altinnDistributionMessage.NotificationMessage.Notifications.First();

                _prefillFormTaskBuilder.AddEmailAndSmsNotification("noreply@noreply.no",
                    notification.Receiver,
                    notification.EmailSubject,
                    notification.EmailContent,
                    altinnDistributionMessage.NotificationMessage.NotificationTemplate,
                    notification.SmsContent);

            }

            var prefillFormTask = _prefillFormTaskBuilder.Build();

            // ********** Should have retry for communication errors  *********

            ReceiptExternal receiptExternal = null;
            try
            {
                receiptExternal = await _altinnPrefillClient.SendPrefill(prefillFormTask, altinnDistributionMessage.DueDate);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred when sending prefill to Altinn");
            }

            var prefillResult = new AltinnPrefillResult();
            
            if (receiptExternal?.ReceiptStatusCode == ReceiptStatusEnum.OK)
            {               
                prefillResult.Message = "Ok - Prefill sent";
                prefillResult.AltinnCommunicationResult = AltinnCommunicationResultType.Sent;
                prefillResult.PrefillAltinnReceiptId = receiptExternal.ReceiptId.ToString();
                prefillResult.PrefillAltinnReceivedTime = receiptExternal.LastChanged;
                prefillResult.PrefillReferenceId = receiptExternal.References.Where(r => r.ReferenceTypeName == ReferenceType.WorkFlowReference).First().ReferenceValue;
            }
            else if (receiptExternal != null && receiptExternal.ReceiptStatusCode != ReceiptStatusEnum.OK)
                if (receiptExternal.ReceiptText.Contains("Reportee is reserved against electronic communication"))
                {
                    prefillResult.Message = receiptExternal.ReceiptText;
                    prefillResult.AltinnCommunicationResult = AltinnCommunicationResultType.ReservedReportee;
                }
                else if (receiptExternal.ReceiptText.Contains("One or more notification endpoint addresses is missing.") ||
                         receiptExternal.ReceiptText.Contains("Unable to properly identify notification receivers") ||
                         receiptExternal.ReceiptText.Contains("no official (kofuvi) notification endpoints found"))
                {
                    prefillResult.Message = receiptExternal.ReceiptText;
                    prefillResult.AltinnCommunicationResult = AltinnCommunicationResultType.UnableToReachReceiver;
                }
                else if (receiptExternal.ReceiptText.Contains("Invalid Reportee"))
                {
                    prefillResult.Message = "Invalid Reportee";
                    prefillResult.AltinnCommunicationResult = AltinnCommunicationResultType.UnableToReachReceiver;
                }
                else
                {
                    _logger.LogWarning($"An error occurred when sending prefill to Altinn {receiptExternal.ReceiptStatusCode} - {receiptExternal.ReceiptText} - {receiptExternal.ReceiptHistory}");
                    prefillResult.Message = $"Unknown error occurred while sending prefill: {receiptExternal.ReceiptStatusCode} - {receiptExternal.ReceiptText} - {receiptExternal.ReceiptHistory}";
                    prefillResult.AltinnCommunicationResult = AltinnCommunicationResultType.UnkownErrorOccurred;
                }
            else
            {
                _logger.LogWarning($"An error occurred when sending prefill to Altinn");
                prefillResult.Message = "Unknown error occurred while sending prefill";
                prefillResult.AltinnCommunicationResult = AltinnCommunicationResultType.UnkownErrorOccurred;
            }

            return prefillResult;
        }
    }
}