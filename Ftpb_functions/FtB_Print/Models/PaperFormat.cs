﻿namespace FtB_Print.Models
{
    public class PaperFormat
    {
        public int Height { get; private set; }
        public int Width { get; private set; }

        private PaperFormat(int height, int width)
        {
            Height = height;
            Width = width;
        }

        public static PaperFormat A3 => new PaperFormat(420, 297);
        public static PaperFormat A4 => new PaperFormat(297, 210);
        public static PaperFormat A5 => new PaperFormat(210, 148);

        public static PaperFormat Parse(string paperFormat)
        {
            return paperFormat switch
            {
                "A3" => A3,
                "A4" => A4,
                "A5" => A5,
                _ => A4,
            };
        }
    }
}
