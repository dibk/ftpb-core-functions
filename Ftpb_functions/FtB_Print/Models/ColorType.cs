﻿namespace FtB_Print.Models
{
    public enum ColorType
    {
        BW,
        Grayscalish,
        Color
    }
}
