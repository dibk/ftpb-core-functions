﻿using Dibk.Ftpb.Integration.SvarUt.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace FtB_Print.Models
{
    public class PrintDocument : SvarUtDokument
    {
        [JsonIgnore]
        public string AttachmentType { get; set; }

        public PrintDocument(string attachmentType, string dokumentType, string filnavn, Stream documentContent) : 
            this(attachmentType, dokumentType, filnavn, documentContent, null, false, false)
        {
        }

        public PrintDocument(
            string attachmentType, string dokumentType, string filnavn, Stream documentContent, List<long> giroarkSider, bool? skalSigneres, bool? ekskluderesFraUtskrift)
        {
            AttachmentType = attachmentType;
            DokumentType = dokumentType;
            Filnavn = filnavn;
            DocumentContent = documentContent;
            GiroarkSider = giroarkSider;
            SkalSigneres = skalSigneres;
            EkskluderesFraUtskrift = ekskluderesFraUtskrift;
        }
    }
}
