﻿using System.IO;

namespace FtB_Print.Models
{
    public class PdfInputData
    {
        public string AttachmentTypeName { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public MemoryStream Content { get; set; }
        public string Title { get; set; }
    }
}
