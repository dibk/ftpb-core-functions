﻿namespace FtB_Print.Models
{
    public class CountryCode
    {
        public string Country { get; set; }
        public string ISO { get; set; }
        public string SSB { get; set; }
    }
}
