﻿using FtB_Print.Models;
using System.IO;
using System.Threading.Tasks;

namespace FtB_Print.HttpClients.Pdf
{
    public interface IPdfHttpClient
    {
        Task<MemoryStream> GeneratePdfDocumentAsync(string htmlString);
        Task<MemoryStream> GeneratePdfDocumentAsync(PdfInputData inputData);
    }
}
