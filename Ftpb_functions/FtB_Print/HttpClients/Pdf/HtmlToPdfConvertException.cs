﻿using System;

namespace FtB_Print.HttpClients.Pdf
{
    public class HtmlToPdfConvertException : Exception
    {
        public string Text { get; set; }
        public int HttpStatusCode { get; set; }

        public HtmlToPdfConvertException(string text, int httpStatusCode)
        {
            Text = text;
            HttpStatusCode = httpStatusCode;
        }
    }
}
