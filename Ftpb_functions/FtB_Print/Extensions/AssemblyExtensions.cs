﻿using System.IO;
using System.Linq;
using System.Reflection;

namespace FtB_Print.Extensions
{
    public static class AssemblyExtensions
    {
        public static Stream GetResourceStream(this Assembly assembly, string fileName)
        {
            var name = assembly.GetManifestResourceNames().SingleOrDefault(name => name.EndsWith(fileName));

            return name != null ? assembly.GetManifestResourceStream(name) : null;
        }
    }
}
