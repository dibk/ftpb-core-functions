﻿using Dibk.Ftpb.ModelToPdf.Common.Client;
using FtB_Print.HttpClients.Pdf;
using FtB_Print.Services.Image;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace FtB_Print.Config
{
    public static class PrintConfiguration
    {
        public static void AddPrinting(this IServiceCollection services, IConfiguration configuration, Action<PrintSettings> settings)
        {
            services.Configure(settings);

            var printSettings = new PrintSettings();
            settings.Invoke(printSettings);

            foreach (var printOptions in printSettings.Options)
                services.AddScoped(printOptions.ServiceType, printOptions.ImplementationType);

            services.AddScoped<IImageService, ImageService>();
            services.AddHttpClient<IPdfHttpClient, PdfHttpClient>();
            services.AddDatamodelToPdfService(new Uri(configuration["DatamodelToPdf:ApiUrl"]));

            services.Configure<PdfSettings>(configuration.GetSection(PdfSettings.SectionName));
            services.Configure<ImageSettings>(configuration.GetSection(ImageSettings.SectionName));
        }
    }
}