using Dibk.Ftpb.Common.Models;

namespace FtB_Print.Config
{
    public class DocumentSetting
    {
        public string AttachmentTypeName { get; set; }
        public string FriendlyName { get; set; }

        /// <summary>
        /// mo83Code
        /// </summary>
        public string DokumentType { get; set; }

        public DocumentSetting(DataTypeConfig settings)
        {
            AttachmentTypeName = settings.DataType;
            FriendlyName = settings.DisplayName;
            DokumentType = settings.Arkivlett2DokumentType;
        }
    }
}