﻿using FtB_Print.Services.Print;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FtB_Print.Config
{
    public class PrintSettings
    {
        public List<PrintOptions> Options { get; set; } = new List<PrintOptions>();

        public void AddPrintService<TServiceImplementation, TModel>(Action<PrintServiceSettings> settings = null)
            where TModel : class
            where TServiceImplementation : IPrintService
        {
            var printServiceSettings = new PrintServiceSettings();

            if (settings != null)
                settings.Invoke(printServiceSettings);

            Options.Add(
                new PrintOptions<TModel>(
                    typeof(IPrintService),
                    typeof(TServiceImplementation),
                    printServiceSettings.ShipmentTitle,
                    printServiceSettings.ShipmentType,
                    printServiceSettings.AccountingCode
                )
            );
        }

        public PrintOptions GetPrintOptions<T>()
        {
            var type = typeof(T);

            return Options.SingleOrDefault(options => options.ModelType == type);
        }
    }
}