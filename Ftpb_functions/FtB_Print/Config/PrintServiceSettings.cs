﻿namespace FtB_Print.Config
{
    public class PrintServiceSettings
    {
        public string ShipmentTitle { get; set; }
        public string ShipmentType { get; set; }
        public string AccountingCode { get; set; }
    }
}