﻿using System;

namespace FtB_Print.Config
{
    public abstract class PrintOptions
    {
        public Type ServiceType { get; set; }
        public Type ImplementationType { get; set; }
        public Type ModelType { get; set; }
        public string ShipmentTitle { get; set; }
        public string ShipmentType { get; set; }
        public string AccountingCode { get; set; }
    }

    public class PrintOptions<T> : PrintOptions where T : class
    {
        public PrintOptions(
            Type serviceType, Type implementationType, string shipmentTitle, string shipmentType, string accountingCode)
        {
            ServiceType = serviceType;
            ImplementationType = implementationType;
            ModelType = typeof(T);
            ShipmentTitle = shipmentTitle;
            ShipmentType = shipmentType;
            AccountingCode = accountingCode;
        }
    }
}