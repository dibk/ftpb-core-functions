using Dibk.Ftpb.Common;
using Dibk.Ftpb.ModelToPdf.Common.Client;
using FtB_Common.Encryption;
using FtB_Common.Storage;
using FtB_Print.Config;
using FtB_Print.HttpClients.Pdf;
using FtB_Print.Mapping.HoeringOgOffentligEttersyn;
using FtB_Print.Models;
using FtB_Print.Services.Image;
using Ftb_Repositories;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using no.kxml.skjema.dibk.offentligEttersyn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Dibk.Ftpb.Common.Constants.DataTypes;

namespace FtB_Print.Services.Print.Implementations
{
    public class HoeringOgOffentligEttersynPrintService : PrintServiceBase<OffentligEttersynType>, IPrintService
    {
        public HoeringOgOffentligEttersynPrintService(
            IBlobOperations blobOperations,
            IDecryptionFactory decryptionFactory,
            IImageService imageService,
            IPdfHttpClient pdfHttpClient,
            IServiceProvider serviceProvider,
            IDbUnitOfWork dbUnitOfWork,
            IOptions<PrintSettings> options,
            ILogger<VarselOppstartPlanarbeidPrintService> logger,
            IDatamodelToPdfHttpClient datamodelToPdf) :
            base(blobOperations, decryptionFactory, imageService, pdfHttpClient, serviceProvider, dbUnitOfWork, options, logger, datamodelToPdf)
        {
        }

        public async Task<string> SendToPrintAsync(string archiveReference, string receiverId, Guid distributionFormReferenceId)
        {
            var printOptions = GetPrintOptions();
            var documents = await GetDocumentsAsync(archiveReference);
            var formData = await GetFormDataAsync(archiveReference);
            var sender = await GetSender(archiveReference, formData);
            var følgebrevModel = formData.ToFølgebrevModel(receiverId, GetAttachmentList(documents), sender);
            var printCoverLetter = await GeneratePrintCoverLetterAsync(følgebrevModel, Foelgebrev, archiveReference);

            documents.Insert(0, printCoverLetter);

            return await SendShipmentAsync(
                distributionFormReferenceId,
                archiveReference,
                formData.GetSubmittingSystem(),
                formData.GetBerørtPart(receiverId).ToSvarUtAdresse(),
                sender.ToSvarUtAdresse(fødselsnummer => DecryptSocialServiceNumber(fødselsnummer)),
                documents
            );
        }

        public async Task<PartType> GetSender(string archiveReference, OffentligEttersynType offentligEttersyn)
        {
            var reportee = await _blobOperations.GetReporteeIdFromStoredBlobAsync(archiveReference);

            var decryptedReportee = _decryptionFactory.GetDecryptor().DecryptText(reportee);

            if (!string.IsNullOrEmpty(offentligEttersyn.planKonsulent?.organisasjonsnummer) && offentligEttersyn.planKonsulent.organisasjonsnummer.Equals(decryptedReportee))
                return offentligEttersyn.planKonsulent;

            if ((!string.IsNullOrEmpty(offentligEttersyn.forslagsstiller.organisasjonsnummer) && offentligEttersyn.forslagsstiller.organisasjonsnummer.Equals(decryptedReportee))
                    || (!string.IsNullOrEmpty(offentligEttersyn.forslagsstiller.foedselsnummer)
                        && _decryptionFactory.GetDecryptor().DecryptText(offentligEttersyn.forslagsstiller.foedselsnummer).Equals(decryptedReportee)))
                return offentligEttersyn.forslagsstiller;

            throw new Exception($"Unable to find receiver in XML for id {reportee}");
        }

        public override IEnumerable<DocumentSetting> DocumentSettings => new[]
        {
            new DocumentSetting(DataTypeMapper.Get(Foelgebrev)),
            new DocumentSetting(DataTypeMapper.Get(Hoeringsbrev)),
            new DocumentSetting(DataTypeMapper.Get(PlankartPdf)),
            new DocumentSetting(DataTypeMapper.Get(PlanbestemmelsePdf)),
            new DocumentSetting(DataTypeMapper.Get(Planbeskrivelse)),
            new DocumentSetting(DataTypeMapper.Get(Konsekvensutredning)),
            new DocumentSetting(DataTypeMapper.Get(Illustrasjon)),
            new DocumentSetting(DataTypeMapper.Get(ROSAnalyse)),
            new DocumentSetting(DataTypeMapper.Get(Annet)),
        };

        private IEnumerable<string> GetAttachmentList(IEnumerable<PrintDocument> documents)
        {
            return documents
                .GroupBy(document => document.AttachmentType)
                .Select(grouping => $"{GetFriendlyName(grouping.Key)}{(grouping.Count() > 1 ? $" ({grouping.Count()})" : "")}");
        }
    }
}