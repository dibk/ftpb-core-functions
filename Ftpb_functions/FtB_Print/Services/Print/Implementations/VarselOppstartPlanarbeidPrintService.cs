using Dibk.Ftpb.Common;
using Dibk.Ftpb.ModelToPdf.Common.Client;
using FtB_Common.Encryption;
using FtB_Common.Storage;
using FtB_Print.Config;
using FtB_Print.HttpClients.Pdf;
using FtB_Print.Mapping.VarselOppstartPlanarbeid;
using FtB_Print.Models;
using FtB_Print.Services.Image;
using Ftb_Repositories;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using no.kxml.skjema.dibk.nabovarselPlan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Dibk.Ftpb.Common.Constants.DataTypes;

namespace FtB_Print.Services.Print.Implementations
{
    public class VarselOppstartPlanarbeidPrintService : PrintServiceBase<NabovarselPlanType>, IPrintService
    {
        public VarselOppstartPlanarbeidPrintService(
            IBlobOperations blobOperations,
            IDecryptionFactory decryptionFactory,
            IImageService imageService,
            IPdfHttpClient pdfHttpClient,
            IServiceProvider serviceProvider,
            IDbUnitOfWork dbUnitOfWork,
            IOptions<PrintSettings> options,
            ILogger<VarselOppstartPlanarbeidPrintService> logger,
            IDatamodelToPdfHttpClient datamodelToPdf) :
            base(blobOperations, decryptionFactory, imageService, pdfHttpClient, serviceProvider, dbUnitOfWork, options, logger, datamodelToPdf)
        {
        }

        public async Task<string> SendToPrintAsync(string archiveReference, string receiverId, Guid distributionFormReferenceId)
        {
            var printOptions = GetPrintOptions();
            var documents = await GetDocumentsAsync(archiveReference);
            var formData = await GetFormDataAsync(archiveReference);
            var sender = await GetSender(archiveReference, formData);
            var følgebrevModel = formData.ToFølgebrevModel(receiverId, GetAttachmentList(documents), sender);
            var printCoverLetter = await GeneratePrintCoverLetterAsync(følgebrevModel, Foelgebrev, archiveReference);

            documents.Insert(0, printCoverLetter);

            return await SendShipmentAsync(
                distributionFormReferenceId,
                archiveReference,
                formData.GetSubmittingSystem(),
                formData.GetBerørtPart(receiverId).ToSvarUtAdresse(),
                sender.ToSvarUtAdresse(fødselsnummer => DecryptSocialServiceNumber(fødselsnummer)),
                documents
            );
        }

        public async Task<PartType> GetSender(string archiveReference, NabovarselPlanType nabovarselPlan)
        {
            var reportee = await _blobOperations.GetReporteeIdFromStoredBlobAsync(archiveReference);

            var decryptedReportee = _decryptionFactory.GetDecryptor().DecryptText(reportee);

            if ((!string.IsNullOrEmpty(nabovarselPlan.planKonsulent?.organisasjonsnummer) && nabovarselPlan.planKonsulent.organisasjonsnummer.Equals(decryptedReportee))
                || (!string.IsNullOrEmpty(nabovarselPlan.planKonsulent?.foedselsnummer)
                    && _decryptionFactory.GetDecryptor().DecryptText(nabovarselPlan.planKonsulent.foedselsnummer).Equals(decryptedReportee)))
                return nabovarselPlan.planKonsulent;

            if ((!string.IsNullOrEmpty(nabovarselPlan.forslagsstiller.organisasjonsnummer) && nabovarselPlan.forslagsstiller.organisasjonsnummer.Equals(decryptedReportee))
                    || (!string.IsNullOrEmpty(nabovarselPlan.forslagsstiller.foedselsnummer)
                        && _decryptionFactory.GetDecryptor().DecryptText(nabovarselPlan.forslagsstiller.foedselsnummer).Equals(decryptedReportee)))
                return nabovarselPlan.forslagsstiller;

            throw new Exception($"Unable to find receiver in XML for id {reportee}");
        }

        public override IEnumerable<DocumentSetting> DocumentSettings => new[]
        {
            new DocumentSetting(DataTypeMapper.Get(Foelgebrev)),
            new DocumentSetting(DataTypeMapper.Get(Varselbrev)),
            new DocumentSetting(DataTypeMapper.Get(Planinitiativ)),
            new DocumentSetting(DataTypeMapper.Get(Planprogram)),
            new DocumentSetting(DataTypeMapper.Get(KartPlanavgrensing)),
            new DocumentSetting(DataTypeMapper.Get(KartDetaljert)),
            new DocumentSetting(DataTypeMapper.Get(ReferatOppstartsmoete)),
            new DocumentSetting(DataTypeMapper.Get(Annet)),
        };

        private IEnumerable<string> GetAttachmentList(IEnumerable<PrintDocument> documents)
        {
            return documents
                .GroupBy(document => document.AttachmentType)
                .Select(grouping => $"{GetFriendlyName(grouping.Key)}{(grouping.Count() > 1 ? $" ({grouping.Count()})" : "")}");
        }
    }
}