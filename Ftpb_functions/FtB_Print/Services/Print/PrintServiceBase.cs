using Dibk.Ftpb.Common;
using Dibk.Ftpb.Integration.SvarUt;
using Dibk.Ftpb.Integration.SvarUt.Models;
using Dibk.Ftpb.ModelToPdf.Common.Client;
using Dibk.Ftpb.ModelToPdf.Common.Models;
using FtB_Common.Encryption;
using FtB_Common.Enums;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_Common.Utils;
using FtB_Print.Config;
using FtB_Print.HttpClients.Pdf;
using FtB_Print.Models;
using FtB_Print.Services.Image;
using Ftb_Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Adresse = Dibk.Ftpb.Integration.SvarUt.Models.Adresse;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace FtB_Print.Services.Print
{
    public abstract class PrintServiceBase<T> where T : class
    {
        private static readonly IEnumerable<string> _mimeTypesToConvert = new[] { "image/jpeg", "image/png", "image/tiff" };
        private static readonly Regex _attachmentTypeRegex = new Regex(@"(?<attachment_type>(.*?))(?<sequence_no>( \(\d+\)))?.pdf", RegexOptions.Compiled);
        private static readonly Regex _sequenceNoRegex = new Regex(@".*?( \((?<sequence_no>\d+)\)).pdf", RegexOptions.Compiled);

        private readonly PrintSettings _settings;
        protected readonly IBlobOperations _blobOperations;
        protected readonly IDecryptionFactory _decryptionFactory;
        private readonly IImageService _imageService;
        private readonly IPdfHttpClient _pdfHttpClient;
        private readonly IServiceProvider _serviceProvider;
        private readonly IDbUnitOfWork _dbUnitOfWork;
        private readonly ILogger _logger;
        private readonly IDatamodelToPdfHttpClient _datamodelToPdfHttpClient;

        public PrintServiceBase(
            IBlobOperations blobOperations,
            IDecryptionFactory decryptionFactory,
            IImageService imageService,
            IPdfHttpClient pdfHttpClient,
            IServiceProvider serviceProvider,
            IDbUnitOfWork dbUnitOfWork,
            IOptions<PrintSettings> options,
            ILogger logger,
            IDatamodelToPdfHttpClient datamodelToPdfHttpClient)
        {
            _blobOperations = blobOperations;
            _decryptionFactory = decryptionFactory;
            _imageService = imageService;
            _pdfHttpClient = pdfHttpClient;
            _serviceProvider = serviceProvider;
            _dbUnitOfWork = dbUnitOfWork;
            _settings = options.Value;
            _logger = logger;
            _datamodelToPdfHttpClient = datamodelToPdfHttpClient;
        }

        public abstract IEnumerable<DocumentSetting> DocumentSettings { get; }

        public async Task ConvertImagesToPdfAsync(string archiveReference)
        {
            var blobStreamItems = await DownloadAttachmentsFromBlobStorageAsync(archiveReference, _mimeTypesToConvert);

            if (!blobStreamItems.Any())
                return;

            var pdfInputData = new List<PdfInputData>();

            foreach (var blobStreamItem in blobStreamItems)
                pdfInputData.Add(await _imageService.GetPdfInputDataAsync(blobStreamItem));

            var orderedList = HandleAttachmentTypeDuplicates(pdfInputData);
            var pdfTasks = new List<(PdfInputData InputData, Task<MemoryStream> Task)>();

            foreach (var inputData in orderedList)
                pdfTasks.Add((inputData, _pdfHttpClient.GeneratePdfDocumentAsync(inputData)));

            using var zipArchive = await CreateZipArchiveAsync(pdfTasks);

            _ = await UploadPdfImagesToBlobStorageAsync(zipArchive, archiveReference);
        }

        protected async Task<string> SendShipmentAsync(
            Guid distributionFormReferenceId, string externalReference, string submittingSystem, Adresse recipientAddress, Adresse replyAddress, List<PrintDocument> documents)
        {
            ForsendelsesId shipmentId;

            try
            {
                var svarUtClient = _serviceProvider.GetRequiredService<ISvarUtClient>();
                var printOptions = GetPrintOptions();

                var shipment = CreateShipment(
                    printOptions.ShipmentTitle,
                    printOptions.ShipmentType,
                    submittingSystem,
                    printOptions.AccountingCode,
                    externalReference,
                    recipientAddress,
                    replyAddress,
                    documents
                );

                shipmentId = await svarUtClient.SendForsendelseAsync(shipment);
                var distibutionForm = await _dbUnitOfWork.DistributionForms.Get(distributionFormReferenceId.ToString());

                distibutionForm.SubmitAndInstantiatePrefilledFormTaskReceiptId = shipmentId.Id;
                distibutionForm.Printed = true;

                _dbUnitOfWork.LogEntries.AddInfo($"Forsendelse til print akseptert av SvarUt med {shipmentId.Id}");
            }
            catch (ArgumentOutOfRangeException ae)
            {
                _logger.LogError(ae, "SvarUt forsendelse is invalid");
                _dbUnitOfWork.LogEntries.AddError($"Data i SvarUt-forsendelsen er ugyldige: {ae}");
                return null;
            }
            catch (SvarUtRequestException sure)
            {
                _logger.LogError(sure, "SvarUt did not accept the shipment");
                _dbUnitOfWork.LogEntries.AddError($"Forsendelse til SvarUt feilet: {sure.ToString()}");

                if (sure.HttpStatusCode == System.Net.HttpStatusCode.BadRequest || sure.HttpStatusCode == System.Net.HttpStatusCode.InternalServerError)
                {
                    return null;
                }
                else
                    throw;
            }
            catch (Exception exception)
            {
                _dbUnitOfWork.LogEntries.AddInfo($"Feil oppstod ved sending av Forsendelse til SvarUt: {exception.Message}");
                _logger.LogError(exception, "Error occurred while sending forsendelse to SvarUt");
                throw;
            }

            await _dbUnitOfWork.SaveDistributionForms();
            await _dbUnitOfWork.SaveLogEntries();

            return shipmentId.Id;
        }

        protected async Task<T> GetFormDataAsync(string archiveReference)
        {
            var metadata = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.FormData)
            };

            var formDataBlob = await _blobOperations.GetAllBlobsByFilterAsync(BlobStorageEnum.Private, archiveReference, metadata);
            var xmlStream = formDataBlob.First().Content;

            return SerializeUtil.DeserializeXML<T>(xmlStream);
        }

        protected async Task<List<PrintDocument>> GetDocumentsAsync(string archiveReference)
        {
            var printImagesPdfArchive = await DownloadPrintImagesPdfArchiveFromBlobStorageAsync(archiveReference);
            var printImagePdfDocuments = ConvertZipArchiveToDocuments(printImagesPdfArchive);
            var pdfBlobStreamItems = await DownloadAttachmentsFromBlobStorageAsync(archiveReference, new[] { "application/pdf" });
            var pdfDocuments = ConvertBlobStreamItemsToDocuments(pdfBlobStreamItems);

            return OrderDocuments(printImagePdfDocuments.Concat(pdfDocuments));
        }

        private async Task<Stream> CreatePrintCoverLetterPdf(FølgebrevModel følgebrevModel, string archiveReference)
        {
            var modelToPdfRequest = CreateModelToPdfRequest(følgebrevModel, archiveReference);
            var result = await _datamodelToPdfHttpClient.CreatePdfAsync(modelToPdfRequest, CancellationToken.None);
            return result;
        }

        private static ModelToPdfRequest CreateModelToPdfRequest(FølgebrevModel følgebrevModel, string archiveReference)
        {
            string følgebrevModelJson = JsonSerializer.Serialize(følgebrevModel);
            var modelToPdfRequest = new ModelToPdfRequest()
            {
                Type = DataModelTypes.Følgebrev,
                ContentString = følgebrevModelJson,
                ArchiveReference = archiveReference,
            };

            return modelToPdfRequest;
        }

        protected async Task<PrintDocument> GeneratePrintCoverLetterAsync(FølgebrevModel følgebrevModel, string attachmentType, string archiveReference)
        {
            var pdfStream = await CreatePrintCoverLetterPdf(følgebrevModel, archiveReference);
            var setting = DataTypeMapper.Get(attachmentType);

            return new PrintDocument(setting.DataType, setting.Arkivlett1DokumentType, $"{setting.DisplayName}.pdf", pdfStream);
        }

        protected string DecryptSocialServiceNumber(string socialServiceNumber)
        {
            return _decryptionFactory.GetDecryptor().DecryptText(socialServiceNumber);
        }

        protected PrintOptions GetPrintOptions()
        {
            return _settings.GetPrintOptions<T>();
        }

        protected string GetFriendlyName(string attachmentType)
        {
            return DocumentSettings.SingleOrDefault(setting => setting.AttachmentTypeName == attachmentType)?.FriendlyName;
        }

        protected string GetMO83Code(string attachmentType)
        {
            return DocumentSettings.SingleOrDefault(setting => setting.AttachmentTypeName == attachmentType)?.DokumentType;
        }

        private async Task<string> UploadPdfImagesToBlobStorageAsync(MemoryStream memoryStream, string archiveReference)
        {
            return await _blobOperations.AddStreamToBlobStorageAsync(
                BlobStorageEnum.Private,
                archiveReference,
                $"PdfPrintImages_{archiveReference.ToUpper()}.zip",
                memoryStream,
                "application/zip",
                new List<KeyValuePair<string, string>> { new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.PdfPrintImages) }
            );
        }

        private async Task<ZipArchive> DownloadPrintImagesPdfArchiveFromBlobStorageAsync(string archiveReference)
        {
            var metadata = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>(BlobStorageMetadataKeys.Type, BlobStorageMetadataTypes.PdfPrintImages)
            };

            var blobStreamItems = await _blobOperations.GetAllBlobsByFilterAsync(BlobStorageEnum.Private, archiveReference, metadata);
            var blobStreamItem = blobStreamItems.FirstOrDefault();

            if (blobStreamItem == null)
                return null;

            return new ZipArchive(blobStreamItem.Content);
        }

        private async Task<IEnumerable<BlobStreamItem>> DownloadAttachmentsFromBlobStorageAsync(string archiveReference, IEnumerable<string> mimeTypes)
        {
            var blobStorageTypes = new List<string>
            {
                BlobStorageMetadataTypes.MainForm,
                BlobStorageMetadataTypes.SubmittalAttachment,
            };

            var containerName = _blobOperations.GetPublicBlobContainerName(archiveReference);

            return await _blobOperations.GetAllBlobsByMetadataTypeAsync(BlobStorageEnum.Public, containerName, blobStorageTypes, mimeTypes);
        }

        private static Forsendelse CreateShipment(
            string shipmentTitle, string shipmentType, string submittingSystem, string accountingCode, string externalReference, Adresse recipientAddress, Adresse replyAddress, List<PrintDocument> documents)
        {
            return new Forsendelse
            {
                Tittel = shipmentTitle,
                ForsendelsesType = shipmentType,
                AvgivendeSystem = submittingSystem,
                EksternReferanse = externalReference,
                KonteringsKode = accountingCode,
                Mottaker = recipientAddress,
                SvarSendesTil = replyAddress,
                Dokumenter = documents.ConvertAll(document => (IDokument)document),
                UtskriftsKonfigurasjon = new UtskriftsKonfigurasjon
                {
                    Tosidig = true,
                    UtskriftMedFarger = true
                },
                KrevNiva4Innlogging = false,
                Kryptert = false,
                KunDigitalLevering = false
            };
        }

        private static async Task<MemoryStream> CreateZipArchiveAsync(List<(PdfInputData InputData, Task<MemoryStream> Task)> pdfTasks)
        {
            _ = await Task.WhenAll(pdfTasks.Select(pdfTask => pdfTask.Task));

            var memoryStream = new MemoryStream();
            using (ZipArchive zipArchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
            {
                foreach (var (inputData, task) in pdfTasks)
                {
                    var pdfStream = task.Result;
                    var entry = zipArchive.CreateEntry($"{Path.GetFileNameWithoutExtension(inputData.FileName)}.pdf");

                    using (Stream str = entry.Open())
                    {
                        await pdfStream.CopyToAsync(str);
                    }
                }
            }

            memoryStream.Seek(0, SeekOrigin.Begin);

            return memoryStream;
        }

        private List<PrintDocument> OrderDocuments(IEnumerable<PrintDocument> attachments)
        {
            var orderedDocuments = new List<PrintDocument>();

            foreach (var setting in DocumentSettings)
            {
                var attachmentsOfType = attachments.Where(attachment => attachment.AttachmentType == setting.AttachmentTypeName);
                var count = attachmentsOfType.Count();

                if (count == 0)
                    continue;

                if (count == 1)
                {
                    orderedDocuments.Add(attachmentsOfType.Single());
                    continue;
                }

                var max = 0;
                var withSequenceNumber = new List<PrintDocument>();
                var withoutSequenceNumber = new List<PrintDocument>();

                foreach (var attachment in attachmentsOfType)
                {
                    var match = _sequenceNoRegex.Match(attachment.Filnavn);

                    if (match.Success)
                    {
                        var sequenceNo = int.Parse(match.Groups["sequence_no"].Value);

                        if (sequenceNo > max)
                            max = sequenceNo;

                        withSequenceNumber.Add(attachment);
                    }
                    else
                    {
                        withoutSequenceNumber.Add(attachment);
                    }
                }

                foreach (var attachment in withoutSequenceNumber)
                    attachment.Filnavn = $"{Path.GetFileNameWithoutExtension(attachment.Filnavn)} ({++max}){Path.GetExtension(attachment.Filnavn)}";

                orderedDocuments.AddRange(withSequenceNumber);
                orderedDocuments.AddRange(withoutSequenceNumber);
            }

            return orderedDocuments;
        }

        private List<PdfInputData> HandleAttachmentTypeDuplicates(List<PdfInputData> pdfInputData)
        {
            var groupedInputData = pdfInputData
                .GroupBy(data => data.AttachmentTypeName)
                .ToList();

            var pdfInputDataList = new List<PdfInputData>();

            foreach (var grouping in groupedInputData)
            {
                var count = grouping.Count();
                var first = grouping.First();
                var setting = DocumentSettings.Single(setting => setting.AttachmentTypeName == first.AttachmentTypeName);

                if (count == 1)
                {
                    first.FileName = setting.AttachmentTypeName + Path.GetExtension(first.FileName);
                    first.Title = setting.FriendlyName;
                    pdfInputDataList.Add(first);
                    continue;
                }

                for (int i = 0; i < count; i++)
                {
                    var inputData = grouping.ElementAt(i);
                    inputData.FileName = $"{inputData.AttachmentTypeName} ({i + 1}){Path.GetExtension(inputData.FileName)}";
                    inputData.Title = $"{setting.FriendlyName} ({i + 1})";
                    pdfInputDataList.Add(inputData);
                }
            }

            return pdfInputDataList;
        }

        private List<PrintDocument> ConvertBlobStreamItemsToDocuments(IEnumerable<BlobStreamItem> blobStreamItems)
        {
            var documents = new List<PrintDocument>();

            if (!blobStreamItems.Any())
                return documents;

            foreach (var blobStreamItem in blobStreamItems)
            {
                var attachmentType = blobStreamItem.Metadata.Single(metadata => metadata.Key == BlobStorageMetadataKeys.AttachmentTypeName).Value;
                var friendlyName = GetFriendlyName(attachmentType);
                var documentType = GetMO83Code(attachmentType);
                var fileName = $"{friendlyName}.pdf";
                var document = new PrintDocument(attachmentType, documentType, fileName, blobStreamItem.Content);

                documents.Add(document);
            }

            return documents;
        }

        private List<PrintDocument> ConvertZipArchiveToDocuments(ZipArchive zipArchive)
        {
            var documents = new List<PrintDocument>();

            if (zipArchive == null)
                return documents;

            foreach (var entry in zipArchive.Entries)
            {
                var match = _attachmentTypeRegex.Match(entry.Name);

                if (!match.Success)
                    throw new Exception($"Invalid attachment type {entry.Name}");

                var attachmentType = match.Groups["attachment_type"].Value;
                var sequenceNo = match.Groups["sequence_no"].Value;
                var friendlyName = GetFriendlyName(attachmentType);
                var documentType = GetMO83Code(attachmentType);
                var fileName = $"{friendlyName}{sequenceNo}.pdf";
                var document = new PrintDocument(attachmentType, documentType, fileName, entry.Open());

                documents.Add(document);
            }

            return documents;
        }
    }
}