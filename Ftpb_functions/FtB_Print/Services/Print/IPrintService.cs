﻿using System;
using System.Threading.Tasks;

namespace FtB_Print.Services.Print
{
    public interface IPrintService
    {
        Task<string> SendToPrintAsync(string archiveReference, string receiverId, Guid distributionFormReferenceId);
        Task ConvertImagesToPdfAsync(string archiveReference);
    }
}
