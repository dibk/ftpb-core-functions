﻿namespace FtB_Print.Services.Image
{
    public class ImageSettings
    {
        public static readonly string SectionName = "Image";
        public string Format { get; set; } = "A4";
        public DpiSettings DPI { get; set; } = new DpiSettings();

        public class DpiSettings
        {
            public int BW { get; set; } = 600;
            public int Grayscale { get; set; } = 300;
            public int Color { get; set; } = 150;
        }
    }
}
