﻿using FtB_Common.Storage;
using FtB_Print.Models;
using System.Threading.Tasks;

namespace FtB_Print.Services.Image
{
    public interface IImageService
    {
        Task<PdfInputData> GetPdfInputDataAsync(BlobStreamItem blobStreamItem);
    }
}
