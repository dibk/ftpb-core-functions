﻿using BitMiracle.LibTiff.Classic;
using FtB_Common.Enums;
using FtB_Common.Storage;
using FtB_Common.Storage.Metadata;
using FtB_Print.Models;
using Microsoft.Extensions.Options;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace FtB_Print.Services.Image
{
    public class ImageService : IImageService
    {
        private readonly ImageSettings _settings;
        private const double InchAsMillimeter = 25.4;
        private const double GrayscaleThreshold = 0.05;

        public ImageService(
            IOptions<ImageSettings> options)
        {
            _settings = options.Value;
        }

        public async Task<PdfInputData> GetPdfInputDataAsync(BlobStreamItem blobStreamItem)
        {
            if (blobStreamItem.MimeType == "image/tiff")
                return await GetPdfInputDataFromTiffImageAsync(blobStreamItem);

            return await GetPdfInputDataFromImageAsync(blobStreamItem);
        }

        private async Task<PdfInputData> GetPdfInputDataFromImageAsync(BlobStreamItem blobStreamItem)
        {
            var pdfInputData = new PdfInputData
            {
                AttachmentTypeName = GetAttachmentTypeName(blobStreamItem.Metadata),
                FileName = Guid.NewGuid() + Path.GetExtension(blobStreamItem.FileName),
                MimeType = blobStreamItem.MimeType
            };

            var memoryStream = await CopyToMemoryStreamAsync(blobStreamItem.Content);
            var codec = CreateSKCodec(memoryStream);
            var bitmap = SKBitmap.Decode(codec);
            var colorType = DetectBitmapColorType(bitmap);

            if (ShouldResize(bitmap, colorType))
            {
                pdfInputData.Content = await ResizeImageAsync(bitmap, codec.EncodedFormat, colorType);
                await memoryStream.DisposeAsync();
            }
            else
            {
                memoryStream.Position = 0;
                pdfInputData.Content = await CopyToMemoryStreamAsync(memoryStream);
            }

            codec.Dispose();
            bitmap.Dispose();

            return pdfInputData;
        }

        private async Task<PdfInputData> GetPdfInputDataFromTiffImageAsync(BlobStreamItem blobStreamItem)
        {
            var pdfInputData = new PdfInputData
            {
                AttachmentTypeName = GetAttachmentTypeName(blobStreamItem.Metadata),
                FileName = $"{Guid.NewGuid()}.jpg",
                MimeType = "image/jpeg",
            };

            using var memoryStream = await CopyToMemoryStreamAsync(blobStreamItem.Content);
            using var bitmap = TiffStreamToSKBitmap(memoryStream);
            var colorType = DetectBitmapColorType(bitmap);

            if (ShouldResize(bitmap, colorType))
                pdfInputData.Content = await ResizeImageAsync(bitmap, SKEncodedImageFormat.Jpeg, colorType);
            else
                pdfInputData.Content = await SKBitmapToImageStreamAsync(bitmap, SKEncodedImageFormat.Jpeg);

            return pdfInputData;
        }

        private async Task<MemoryStream> ResizeImageAsync(SKBitmap bitmap, SKEncodedImageFormat encodingFormat, ColorType colorType)
        {
            var ratio = GetImageMaxSize(colorType) / (float)Math.Max(bitmap.Width, bitmap.Height);
            var resizedWidth = bitmap.Width * ratio;
            var resizedHeight = bitmap.Height * ratio;
            var resized = bitmap.Resize(new SKImageInfo((int)resizedWidth, (int)resizedHeight), SKFilterQuality.High);

            return await SKBitmapToImageStreamAsync(resized, encodingFormat);
        }

        private bool ShouldResize(SKBitmap bitmap, ColorType colorType) => Math.Max(bitmap.Width, bitmap.Height) > GetImageMaxSize(colorType);

        private int GetImageMaxSize(ColorType colorType)
        {
            var paperFormat = PaperFormat.Parse(_settings.Format);
            var dpi = GetDpiFromColorType(colorType);

            return (int)Math.Round(dpi * paperFormat.Height / InchAsMillimeter);
        }

        private int GetDpiFromColorType(ColorType colorType)
        {
            return colorType switch
            {
                ColorType.BW => _settings.DPI.BW,
                ColorType.Grayscalish => _settings.DPI.Grayscale,
                ColorType.Color => _settings.DPI.Color,
                _ => _settings.DPI.Grayscale,
            };
        }

        private static async Task<MemoryStream> SKBitmapToImageStreamAsync(SKBitmap bitmap, SKEncodedImageFormat encodingFormat, int quality = 90)
        {
            using var image = SKImage.FromBitmap(bitmap);
            using var data = image.Encode(encodingFormat, quality);

            return await CopyToMemoryStreamAsync(data.AsStream());
        }

        private static SKCodec CreateSKCodec(MemoryStream memoryStream)
        {
            try
            {
                return SKCodec.Create(memoryStream);
            }
            catch
            {
                memoryStream.Position = 0;
                using var data = SKData.Create(memoryStream);

                return SKCodec.Create(data);
            }
        }

        private static SKBitmap TiffStreamToSKBitmap(MemoryStream tiffStream)
        {
            using var tiffImage = Tiff.ClientOpen("in-memory", "r", tiffStream, new TiffStream());

            var width = tiffImage.GetField(TiffTag.IMAGEWIDTH)[0].ToInt();
            var height = tiffImage.GetField(TiffTag.IMAGELENGTH)[0].ToInt();

            var bitmap = new SKBitmap();
            var info = new SKImageInfo(width, height);

            var raster = new int[width * height];
            var pointer = GCHandle.Alloc(raster, GCHandleType.Pinned);

            bitmap.InstallPixels(info, pointer.AddrOfPinnedObject(), info.RowBytes, (addr, ctx) => pointer.Free(), null);

            if (!tiffImage.ReadRGBAImageOriented(width, height, raster, Orientation.TOPLEFT))
                return null;

            if (SKImageInfo.PlatformColorType == SKColorType.Bgra8888)
                SKSwizzle.SwapRedBlue(pointer.AddrOfPinnedObject(), raster.Length);

            return bitmap;
        }

        private static async Task<MemoryStream> CopyToMemoryStreamAsync(Stream stream)
        {
            var memoryStream = new MemoryStream();
            await stream.CopyToAsync(memoryStream);
            await stream.DisposeAsync();
            memoryStream.Position = 0;

            return memoryStream; 
        }

        private static string GetAttachmentTypeName(IEnumerable<KeyValuePair<string, string>> metadata)
        {
            return metadata.Single(metadata => metadata.Key == BlobStorageMetadataKeys.AttachmentTypeName).Value;
        }

        private static ColorType DetectBitmapColorType(SKBitmap bitmap)
        {
            var pixels = bitmap.Pixels;
            long diff = 0;
            var colors = new HashSet<float>();

            foreach (var pixel in pixels)
            {
                colors.Add(pixel.Red + pixel.Green + pixel.Blue);

                var rg = Math.Abs(pixel.Red - pixel.Green);
                var rb = Math.Abs(pixel.Red - pixel.Blue);
                var gb = Math.Abs(pixel.Green - pixel.Blue);

                diff += rg + rb + gb;
            }

            if (colors.Count == 2 && colors.All(color => color == 0 || color == 255 * 3))
                return ColorType.BW;

            if (diff / (bitmap.Height * bitmap.Height) / (255f * 3f) < GrayscaleThreshold)
                return ColorType.Grayscalish;

            return ColorType.Color;
        }
    }
}
