using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using Dibk.Ftpb.Integration.SvarUt.Models;
using Dibk.Ftpb.ModelToPdf.Common.Enums;
using Dibk.Ftpb.ModelToPdf.Common.Models;
using Dibk.Ftpb.ModelToPdf.Common.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using Adresse = Dibk.Ftpb.Integration.SvarUt.Models.Adresse;

namespace FtB_Print.Mapping.VarselOppstartPlanarbeidPlussHoeringsmyndigheter
{
    public static class VarselOppstartPlanarbeidPlussHoeringsmyndigheterTypeMapper
    {
        public static FølgebrevModel ToFølgebrevModel(this PlanvarselV2 planvarsel, string receiverId, IEnumerable<string> vedlegg, AktoerPlan sender)
        {
            var berørtPart = GetBerørtPart(planvarsel, receiverId);
            var plannavn = planvarsel.Planforslag.Plannavn;
            var kommunenavn = planvarsel.Kommunenavn;
            var fristForInnspill = planvarsel.Planforslag.FristForInnspill.GetValueOrDefault();
            var kontaktEpost = sender.Epost;
            var kontaktTelefon = sender.Telefon;
            
            return new FølgebrevModel()
            {
                PlantjenesteType = PlantjenesteType.VORPAH.ToString(),
                Tittel = "Varsel om oppstart av planarbeid",
                Plannavn = plannavn,
                Kommunenavn = kommunenavn,
                Mottaker = ToKontaktinfoViewModel(berørtPart),
                Avsender = ToKontaktinfoViewModel(sender),
                FristForInnspill = fristForInnspill,
                KontaktEpost = kontaktEpost,
                KontaktTelefon = kontaktTelefon,
                Vedlegg = vedlegg.ToList()
            };
        }

        public static Adresse ToSvarUtAdresse(this Beroertpart berørtPart)
        {
            var adresselinjer = GetAdresselinjer(berørtPart.Adresse);

            return new Adresse
            {
                DigitalAdresse = new Digitaladresse(),
                PostAdresse = new PostAdresse
                {
                    Navn = berørtPart.Navn,
                    Adresse1 = adresselinjer.ElementAtOrDefault(0),
                    Adresse2 = adresselinjer.ElementAtOrDefault(1),
                    Adresse3 = adresselinjer.ElementAtOrDefault(2),
                    PostNummer = CountryCodeHandler.GetAdjustedPostalCode(berørtPart.Adresse.Postnr?.Trim(), berørtPart.Adresse.Landkode),                    
                    PostSted = berørtPart.Adresse.Poststed,
                    Land = CountryCodeHandler.GetCountryName(berørtPart.Adresse.Landkode)
                }
            };
        }

        public static Adresse ToSvarUtAdresse(this AktoerPlan part, Func<string, string> socialSecurityNumberResolver)
        {
            var adresselinjer = GetAdresselinjer(part.Adresse);

            var adresse = new Adresse
            {
                PostAdresse = new PostAdresse
                {
                    Navn = part.Navn,
                    Adresse1 = adresselinjer.ElementAtOrDefault(0),
                    Adresse2 = adresselinjer.ElementAtOrDefault(1),
                    Adresse3 = adresselinjer.ElementAtOrDefault(2),
                    PostNummer = CountryCodeHandler.GetAdjustedPostalCode(part.Adresse.Postnr?.Trim(), part.Adresse.Landkode),
                    PostSted = part.Adresse.Poststed,
                    Land = CountryCodeHandler.GetCountryName(part.Adresse.Landkode)
                },
                DigitalAdresse = new Digitaladresse()
            };

            if (part.Partstype.Kodeverdi.Equals("Privatperson", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrWhiteSpace(part.Foedselsnummer))
            {
                adresse.DigitalAdresse.FodselsNummer = socialSecurityNumberResolver.Invoke(part.Foedselsnummer);
            }
            else if (part.Partstype.Kodeverdi.Equals("Foretak", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrWhiteSpace(part.Organisasjonsnummer))
            {
                adresse.DigitalAdresse.OrganisasjonsNummer = part.Organisasjonsnummer;
            }

            return adresse;
        }

        public static Beroertpart GetBerørtPart(this PlanvarselV2 offentligEttersyn, string receiverId)
        {
            return offentligEttersyn.BeroerteParter
                .First(berørtPart => berørtPart.Foedselsnummer == receiverId || berørtPart.Organisasjonsnummer == receiverId);
        }

        public static string GetSubmittingSystem(this PlanvarselV2 offentligEttersyn)
        {
            return offentligEttersyn.Metadata?.FraSluttbrukersystem ?? "Fellestjenester PLAN - Altinn";
        }

        private static KontaktinfoModel ToKontaktinfoViewModel(Beroertpart berørtPart)
        {
            var navn = berørtPart.Navn;

            if (berørtPart.Partstype.Kodeverdi != "Privatperson")
                navn += $" (org.nr. {berørtPart.Organisasjonsnummer})";

            var adresselinjer = GetAdresselinjer(berørtPart.Adresse);

            return new KontaktinfoModel
            {
                Navn = navn,
                Adresselinje1 = adresselinjer.ElementAtOrDefault(0) ?? string.Empty,
                Adresselinje2 = adresselinjer.ElementAtOrDefault(1) ?? string.Empty,
                Adresselinje3 = adresselinjer.ElementAtOrDefault(2) ?? string.Empty,
                Postnummer = CountryCodeHandler.GetAdjustedPostalCode(berørtPart.Adresse.Postnr, berørtPart.Adresse.Landkode),
                Poststed = berørtPart.Adresse.Poststed,
                Landkode = CountryCodeHandler.GetCountryName(berørtPart.Adresse.Landkode)
            };
        }

        private static KontaktinfoModel ToKontaktinfoViewModel(AktoerPlan part)
        {
            var adresselinjer = GetAdresselinjer(part.Adresse);

            return new KontaktinfoModel
            {
                Navn = part.Navn,
                Adresselinje1 = adresselinjer.ElementAtOrDefault(0) ?? string.Empty,
                Adresselinje2 = adresselinjer.ElementAtOrDefault(1) ?? string.Empty,
                Adresselinje3 = adresselinjer.ElementAtOrDefault(2) ?? string.Empty,
                Postnummer = CountryCodeHandler.GetAdjustedPostalCode(part.Adresse.Postnr, part.Adresse.Landkode),
                Poststed = part.Adresse.Poststed,
                Landkode = CountryCodeHandler.GetCountryName(part.Adresse.Landkode)
            };
        }


        private static List<string> GetAdresselinjer(EnkelAdresse adresse)
        {
            var adresselinjer = new List<string>();

            if (!string.IsNullOrWhiteSpace(adresse.Adresselinje1))
                adresselinjer.Add(adresse.Adresselinje1);

            if (!string.IsNullOrWhiteSpace(adresse.Adresselinje2))
                adresselinjer.Add(adresse.Adresselinje2);

            if (!string.IsNullOrWhiteSpace(adresse.Adresselinje3))
                adresselinjer.Add(adresse.Adresselinje3);

            return adresselinjer;
        }
    }
}
