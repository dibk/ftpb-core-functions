using Dibk.Ftpb.Integration.SvarUt.Models;
using Dibk.Ftpb.ModelToPdf.Common.Enums;
using Dibk.Ftpb.ModelToPdf.Common.Models;
using Dibk.Ftpb.ModelToPdf.Common.Models.Shared;
using no.kxml.skjema.dibk.nabovarselPlan;
using System;
using System.Collections.Generic;
using System.Linq;
using Adresse = Dibk.Ftpb.Integration.SvarUt.Models.Adresse;

namespace FtB_Print.Mapping.VarselOppstartPlanarbeid
{
    public static class NabovarselPlanTypeMapper
    {
        public static FølgebrevModel ToFølgebrevModel(this NabovarselPlanType nabovarsel, string receiverId, IEnumerable<string> vedlegg, PartType sender)
        {
            var berørtPart = GetBerørtPart(nabovarsel, receiverId);
            var plannavn = nabovarsel.planforslag.plannavn;
            var kommunenavn = nabovarsel.kommunenavn;
            var fristForInnspill = nabovarsel.planforslag.fristForInnspill.GetValueOrDefault();
            var kontaktEpost = sender.epost;
            var kontaktTelefon = sender.telefon;
            
            return new FølgebrevModel()
            {
                PlantjenesteType = PlantjenesteType.VORPA.ToString(),
                Tittel = "Varsel om oppstart av planarbeid",
                Plannavn = plannavn,
                Kommunenavn = kommunenavn,
                Mottaker = ToKontaktinfoViewModel(berørtPart),
                Avsender = ToKontaktinfoViewModel(sender),
                FristForInnspill = fristForInnspill,
                KontaktEpost = kontaktEpost,
                KontaktTelefon = kontaktTelefon,
                Vedlegg = vedlegg.ToList()
            };
        }

        public static Adresse ToSvarUtAdresse(this BeroertPartType berørtPart)
        {
            var adresselinjer = GetAdresselinjer(berørtPart.adresse);

            return new Adresse
            {
                DigitalAdresse = new Digitaladresse(),
                PostAdresse = new PostAdresse
                {
                    Navn = berørtPart.navn,
                    Adresse1 = adresselinjer.ElementAtOrDefault(0),
                    Adresse2 = adresselinjer.ElementAtOrDefault(1),
                    Adresse3 = adresselinjer.ElementAtOrDefault(2),
                    PostNummer = CountryCodeHandler.GetAdjustedPostalCode(berørtPart.adresse.postnr?.Trim(), berørtPart.adresse.landkode),                    
                    PostSted = berørtPart.adresse.poststed,
                    Land = CountryCodeHandler.GetCountryName(berørtPart.adresse.landkode)
                }
            };
        }

        public static Adresse ToSvarUtAdresse(this PartType part, Func<string, string> socialSecurityNumberResolver)
        {
            var adresselinjer = GetAdresselinjer(part.adresse);

            var adresse = new Adresse
            {
                PostAdresse = new PostAdresse
                {
                    Navn = part.navn,
                    Adresse1 = adresselinjer.ElementAtOrDefault(0),
                    Adresse2 = adresselinjer.ElementAtOrDefault(1),
                    Adresse3 = adresselinjer.ElementAtOrDefault(2),
                    PostNummer = CountryCodeHandler.GetAdjustedPostalCode(part.adresse.postnr?.Trim(), part.adresse.landkode),
                    PostSted = part.adresse.poststed,
                    Land = CountryCodeHandler.GetCountryName(part.adresse.landkode)
                },
                DigitalAdresse = new Digitaladresse()
            };

            if (part.partstype.kodeverdi.Equals("Privatperson", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrWhiteSpace(part.foedselsnummer))
            {
                adresse.DigitalAdresse.FodselsNummer = socialSecurityNumberResolver.Invoke(part.foedselsnummer);
            }
            else if (part.partstype.kodeverdi.Equals("Foretak", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrWhiteSpace(part.organisasjonsnummer))
            {
                adresse.DigitalAdresse.OrganisasjonsNummer = part.organisasjonsnummer;
            }

            return adresse;
        }

        public static BeroertPartType GetBerørtPart(this NabovarselPlanType nabovarsel, string receiverId)
        {
            return nabovarsel.beroerteParter
                .First(berørtPart => berørtPart.foedselsnummer == receiverId || berørtPart.organisasjonsnummer == receiverId);
        }

        public static string GetSubmittingSystem(this NabovarselPlanType nabovarsel)
        {
            return nabovarsel.metadata?.fraSluttbrukersystem ?? "Fellestjenester PLAN - Altinn";
        }

        private static KontaktinfoModel ToKontaktinfoViewModel(BeroertPartType berørtPart)
        {
            var navn = berørtPart.navn;

            if (berørtPart.partstype.kodeverdi != "Privatperson")
                navn += $" (org.nr. {berørtPart.organisasjonsnummer})";

            var adresselinjer = GetAdresselinjer(berørtPart.adresse);

            return new KontaktinfoModel
            {
                Navn = navn,
                Adresselinje1 = adresselinjer.ElementAtOrDefault(0) ?? string.Empty,
                Adresselinje2 = adresselinjer.ElementAtOrDefault(1) ?? string.Empty,
                Adresselinje3 = adresselinjer.ElementAtOrDefault(2) ?? string.Empty,
                Postnummer = CountryCodeHandler.GetAdjustedPostalCode(berørtPart.adresse.postnr, berørtPart.adresse.landkode),
                Poststed = berørtPart.adresse.poststed,
                Landkode = CountryCodeHandler.GetCountryName(berørtPart.adresse.landkode)
            };
        }

        private static KontaktinfoModel ToKontaktinfoViewModel(PartType part)
        {
            var adresselinjer = GetAdresselinjer(part.adresse);

            return new KontaktinfoModel
            {
                Navn = part.navn,
                Adresselinje1 = adresselinjer.ElementAtOrDefault(0) ?? string.Empty,
                Adresselinje2 = adresselinjer.ElementAtOrDefault(1) ?? string.Empty,
                Adresselinje3 = adresselinjer.ElementAtOrDefault(2) ?? string.Empty,
                Postnummer = CountryCodeHandler.GetAdjustedPostalCode(part.adresse.postnr, part.adresse.landkode),
                Poststed = part.adresse.poststed,
                Landkode = CountryCodeHandler.GetCountryName(part.adresse.landkode)
            };
        }


        private static List<string> GetAdresselinjer(EnkelAdresseType adresse)
        {
            var adresselinjer = new List<string>();

            if (!string.IsNullOrWhiteSpace(adresse.adresselinje1))
                adresselinjer.Add(adresse.adresselinje1);

            if (!string.IsNullOrWhiteSpace(adresse.adresselinje2))
                adresselinjer.Add(adresse.adresselinje2);

            if (!string.IsNullOrWhiteSpace(adresse.adresselinje3))
                adresselinjer.Add(adresse.adresselinje3);

            return adresselinjer;
        }
    }
}
