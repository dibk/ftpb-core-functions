﻿using FtB_Print.Extensions;
using FtB_Print.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace FtB_Print.Mapping
{
    public class CountryCodeHandler
    {
        private static readonly string _jsonFile = "country-codes.json";
        private static List<CountryCode> _countryCodes;

        static CountryCodeHandler()
        {
            SetCountryCodes();
        }

        public static string GetCountryName(string countryCode)
        {
            if (string.IsNullOrEmpty(countryCode))
                return null;

            var cc = AdjustSSBCodeForNorway(countryCode);

            try
            {
                return _countryCodes.Single(code => code.ISO.Equals(cc, StringComparison.InvariantCultureIgnoreCase) || code.SSB == cc).Country;
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        public static string GetCountryISOCode(string countryCode)
        {
            if (string.IsNullOrEmpty(countryCode))
                return null;

            var cc = AdjustSSBCodeForNorway(countryCode);

            try
            {
                return _countryCodes.SingleOrDefault(code => code.ISO.Equals(cc, StringComparison.InvariantCultureIgnoreCase) || code.SSB == cc)?.ISO;
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        public static string GetAdjustedPostalCode(string postalCode, string countryCode)
        {
            if (countryCode == null || GetCountryISOCode(countryCode) == null || GetCountryISOCode(countryCode).Equals("NO", StringComparison.InvariantCultureIgnoreCase))
                return postalCode;

            var isoCode = GetCountryISOCode(countryCode);

            if (isoCode == null)
                return postalCode;

            return $"{isoCode}-{postalCode}";
        }

        private static void SetCountryCodes()
        {
            using var stream = Assembly.GetExecutingAssembly().GetResourceStream(_jsonFile);
            using var streamReader = new StreamReader(stream);
            var json = streamReader.ReadToEnd();

            _countryCodes = JsonConvert.DeserializeObject<List<CountryCode>>(json);
        }

        private static string AdjustSSBCodeForNorway(string countryCode)
        {
            if (countryCode == null)
                return null;

            if (countryCode == "0")
                return "000";

            return countryCode;
        }
    }
}