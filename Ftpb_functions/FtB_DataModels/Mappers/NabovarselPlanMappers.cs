﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FtB_DataModels.Mappers
{
    public static class NabovarselPlanMappers
    {
        public static IMapper GetNabovarselPartTypeMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<no.kxml.skjema.dibk.nabovarselPlan.PartType, no.kxml.skjema.dibk.nabovarselsvarPlan.PartType>();
                cfg.CreateMap<no.kxml.skjema.dibk.nabovarselPlan.EnkelAdresseType, no.kxml.skjema.dibk.nabovarselsvarPlan.EnkelAdresseType>();
                cfg.CreateMap<no.kxml.skjema.dibk.nabovarselPlan.KodeType, no.kxml.skjema.dibk.nabovarselsvarPlan.KodeType>();
                cfg.CreateMap<no.kxml.skjema.dibk.nabovarselPlan.KontaktpersonType, no.kxml.skjema.dibk.nabovarselsvarPlan.KontaktpersonType>();

            });
            return config.CreateMapper();
        }

        public static IMapper GetNabovarselBerortPartMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AllowNullCollections = true;
                cfg.CreateMap<no.kxml.skjema.dibk.nabovarselPlan.GjelderEiendomType, no.kxml.skjema.dibk.nabovarselsvarPlan.GjelderEiendomType>();
                cfg.CreateMap<no.kxml.skjema.dibk.nabovarselPlan.MatrikkelnummerType, no.kxml.skjema.dibk.nabovarselsvarPlan.MatrikkelnummerType>();
                cfg.CreateMap<no.kxml.skjema.dibk.nabovarselPlan.EiendommensAdresseType, no.kxml.skjema.dibk.nabovarselsvarPlan.EiendommensAdresseType>();
                cfg.CreateMap<no.kxml.skjema.dibk.nabovarselPlan.BeroertPartType, no.kxml.skjema.dibk.nabovarselsvarPlan.BeroertPartType>();
                cfg.CreateMap<no.kxml.skjema.dibk.nabovarselPlan.KodeType, no.kxml.skjema.dibk.nabovarselsvarPlan.KodeType>();
                cfg.CreateMap<no.kxml.skjema.dibk.nabovarselPlan.EnkelAdresseType, no.kxml.skjema.dibk.nabovarselsvarPlan.EnkelAdresseType>();
                cfg.CreateMap<no.kxml.skjema.dibk.nabovarselPlan.KontaktpersonType, no.kxml.skjema.dibk.nabovarselsvarPlan.KontaktpersonType>();

            });
            return config.CreateMapper();
        }

        public static IMapper GetNabovarselReceiverMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<no.kxml.skjema.dibk.nabovarselsvarPlan.BeroertPartType, DistributionReceiver>()
                .ForMember(dest => dest.Ssn, opt => opt.MapFrom(src => src.foedselsnummer))
                .ForMember(dest => dest.Orgnr, opt => opt.MapFrom(src => src.organisasjonsnummer))
                .ForMember(dest => dest.Epost, opt => opt.MapFrom(src => src.epost))
                .ForMember(dest => dest.Kommentar, opt => opt.MapFrom(src => src.kommentar))
                .ForMember(dest => dest.Navn, opt => opt.MapFrom(src => src.navn))
                .ForMember(dest => dest.SystemReferanse, opt => opt.MapFrom(src => src.systemReferanse))
                .ForMember(dest => dest.Telefon, opt => opt.MapFrom(src => src.telefon))
                .ForSourceMember(s => s.adresse, opt => opt.DoNotValidate())
                .ForSourceMember(s => s.kontaktperson, opt => opt.DoNotValidate())
                .ForSourceMember(s => s.gjelderEiendom, opt => opt.DoNotValidate())
                .ForSourceMember(s => s.partstype, opt => opt.DoNotValidate())
                ;
            });
            return config.CreateMapper();
        }
        public static IMapper GetNabovarselGjelderEiendomTypeMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<no.kxml.skjema.dibk.nabovarselPlan.GjelderEiendomType, no.kxml.skjema.dibk.nabovarselsvarPlan.GjelderEiendomType>();
                cfg.CreateMap<no.kxml.skjema.dibk.nabovarselPlan.MatrikkelnummerType, no.kxml.skjema.dibk.nabovarselsvarPlan.MatrikkelnummerType>();
                cfg.CreateMap<no.kxml.skjema.dibk.nabovarselPlan.EiendommensAdresseType, no.kxml.skjema.dibk.nabovarselsvarPlan.EiendommensAdresseType>();
            });
            return config.CreateMapper();
        }

        public static no.kxml.skjema.dibk.nabovarselPlan.BeroertPartType ToUniqueBeroertPartTypeWithPopulatedGjeldendeEiendom(this no.kxml.skjema.dibk.nabovarselPlan.NabovarselPlanType nabovarsel, string beroertPartId, Func<string, string> idDecryptor)
        {
            var decryptedReceiverId = idDecryptor(beroertPartId);
            var berortParter = nabovarsel.beroerteParter?.Where(b => (
                                                                              !string.IsNullOrEmpty(b.foedselsnummer) && idDecryptor(b.foedselsnummer).Equals(decryptedReceiverId))
                                                                              || (!string.IsNullOrEmpty(b.organisasjonsnummer) && b.organisasjonsnummer.Equals(decryptedReceiverId))
                                                                      ).ToList();

            var beroertPartType = berortParter.FirstOrDefault();

            List<no.kxml.skjema.dibk.nabovarselPlan.GjelderEiendomType> gjelderEiendom = new List<no.kxml.skjema.dibk.nabovarselPlan.GjelderEiendomType>();

            foreach (var beroertPart in berortParter)
            {
                foreach (var eiendom in beroertPart.gjelderEiendom)
                {
                    if (!gjelderEiendom.Contains(eiendom))
                    {
                        gjelderEiendom.Add(eiendom);
                    }
                }
            }
            var gjelderEiendomArray = gjelderEiendom.ToArray();
            beroertPartType.gjelderEiendom = gjelderEiendomArray;

            return beroertPartType;
        }

        public static List<no.kxml.skjema.dibk.nabovarselPlan.BeroertPartType> GetListOfBeroertPartWithEiendomFromGroups(List<IGrouping<string, no.kxml.skjema.dibk.nabovarselPlan.BeroertPartType>> groupsWithBeroerteParterWithDecryptedIDs)
        {
            var beroerteParterAsList = new List<no.kxml.skjema.dibk.nabovarselPlan.BeroertPartType>();

            foreach (var group in groupsWithBeroerteParterWithDecryptedIDs)
            {
                List<no.kxml.skjema.dibk.nabovarselPlan.GjelderEiendomType> gjelderEiendom = new List<no.kxml.skjema.dibk.nabovarselPlan.GjelderEiendomType>();
                foreach (var beroertPart in group)
                {
                    if (beroertPart.gjelderEiendom != null)
                    {
                        foreach (var eiendom in beroertPart.gjelderEiendom)
                        {
                            if (!gjelderEiendom.Contains(eiendom))
                            {
                                gjelderEiendom.Add(eiendom);
                            }
                        }
                    }
                }
                var beroertPartType = group.FirstOrDefault();
                beroertPartType.gjelderEiendom = gjelderEiendom.ToArray();
                beroerteParterAsList.Add(beroertPartType);
            }
            return beroerteParterAsList;
        }

        public static List<no.kxml.skjema.dibk.planvarsel.BeroertPartType> GetListOfPlanvarselTypeBeroertPartWithEiendomFromGroups(List<IGrouping<string, no.kxml.skjema.dibk.planvarsel.BeroertPartType>> groupsWithBeroerteParterWithDecryptedIDs)
        {

            var beroerteParterAsList = new List<no.kxml.skjema.dibk.planvarsel.BeroertPartType>();

            foreach (var group in groupsWithBeroerteParterWithDecryptedIDs)
            {
                List<no.kxml.skjema.dibk.planvarsel.GjelderEiendomType> gjelderEiendom = new List<no.kxml.skjema.dibk.planvarsel.GjelderEiendomType>();
                foreach (var beroertPart in group)
                {
                    if (beroertPart.gjelderEiendom != null)
                    {
                        foreach (var eiendom in beroertPart.gjelderEiendom)
                        {
                            if (!gjelderEiendom.Contains(eiendom))
                            {
                                gjelderEiendom.Add(eiendom);
                            }
                        }
                    }
                }
                var beroertPartType = group.FirstOrDefault();
                beroertPartType.gjelderEiendom = gjelderEiendom.ToArray();
                beroerteParterAsList.Add(beroertPartType);
            }
            return beroerteParterAsList;
        }
    }
}
