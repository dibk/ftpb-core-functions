﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace FtB_DataModels.Mappers
{
    public static class HoeringOgOffentligEttersynMappers
    {
        public static IMapper GetHoeringOgOffentligEttersynBerortPartMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AllowNullCollections = true;
                cfg.CreateMap<no.kxml.skjema.dibk.offentligEttersyn.BeroertPartType, no.kxml.skjema.dibk.uttalelseOffentligEttersyn.BeroertPartType>();
                cfg.CreateMap<no.kxml.skjema.dibk.offentligEttersyn.GjelderEiendomType[], no.kxml.skjema.dibk.uttalelseOffentligEttersyn.GjelderEiendomType >();
                cfg.CreateMap<no.kxml.skjema.dibk.offentligEttersyn.GjelderEiendomType, no.kxml.skjema.dibk.uttalelseOffentligEttersyn.GjelderEiendomType>();
                cfg.CreateMap<no.kxml.skjema.dibk.offentligEttersyn.MatrikkelnummerType, no.kxml.skjema.dibk.uttalelseOffentligEttersyn.MatrikkelnummerType>();
                cfg.CreateMap<no.kxml.skjema.dibk.offentligEttersyn.EiendommensAdresseType, no.kxml.skjema.dibk.uttalelseOffentligEttersyn.EiendommensAdresseType>();
                cfg.CreateMap<no.kxml.skjema.dibk.offentligEttersyn.KodeType, no.kxml.skjema.dibk.uttalelseOffentligEttersyn.KodeType>();
                cfg.CreateMap<no.kxml.skjema.dibk.offentligEttersyn.EnkelAdresseType, no.kxml.skjema.dibk.uttalelseOffentligEttersyn.EnkelAdresseType>();
            });
            return config.CreateMapper();
        }

        public static IMapper GetHoeringOgOffentligEttersynReceiverMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<no.kxml.skjema.dibk.uttalelseOffentligEttersyn.BeroertPartType, DistributionReceiver>()
                .ForMember(dest => dest.Ssn, opt => opt.MapFrom(src => src.foedselsnummer))
                .ForMember(dest => dest.Orgnr, opt => opt.MapFrom(src => src.organisasjonsnummer))
                .ForMember(dest => dest.Epost, opt => opt.MapFrom(src => src.epost))
                //.ForMember(dest => dest.Kommentar, opt => opt.MapFrom(src => src..kommentar))
                .ForMember(dest => dest.Navn, opt => opt.MapFrom(src => src.navn))
                .ForMember(dest => dest.SystemReferanse, opt => opt.MapFrom(src => src.systemReferanse))
                .ForMember(dest => dest.Telefon, opt => opt.MapFrom(src => src.telefon))
                .ForSourceMember(s => s.adresse, opt => opt.DoNotValidate())
                .ForSourceMember(s => s.gjelderEiendommer, opt => opt.DoNotValidate())
                .ForSourceMember(s => s.partstype, opt => opt.DoNotValidate())
                ;
            });
            return config.CreateMapper();
        }

        public static IMapper GetHoeringOgOffentligEttersynPartTypeMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<no.kxml.skjema.dibk.offentligEttersyn.PartType, no.kxml.skjema.dibk.uttalelseOffentligEttersyn.PartType>();
                cfg.CreateMap<no.kxml.skjema.dibk.offentligEttersyn.EnkelAdresseType, no.kxml.skjema.dibk.uttalelseOffentligEttersyn.EnkelAdresseType>();
                cfg.CreateMap<no.kxml.skjema.dibk.offentligEttersyn.KodeType, no.kxml.skjema.dibk.uttalelseOffentligEttersyn.KodeType>();
            });
            return config.CreateMapper();
        }

        public static List<no.kxml.skjema.dibk.offentligEttersyn.BeroertPartType> GetListOfHoeringOgOffentligEttersynVarselTypeBeroertPartWithEiendomFromGroups(List<IGrouping<string, no.kxml.skjema.dibk.offentligEttersyn.BeroertPartType>> groupsWithBeroerteParterWithDecryptedIDs)
        {

            var beroerteParterAsList = new List<no.kxml.skjema.dibk.offentligEttersyn.BeroertPartType>();

            foreach (var group in groupsWithBeroerteParterWithDecryptedIDs)
            {
                List<no.kxml.skjema.dibk.offentligEttersyn.GjelderEiendomType> gjelderEiendom = new List<no.kxml.skjema.dibk.offentligEttersyn.GjelderEiendomType>();
                foreach (var beroertPart in group)
                {
                    if (beroertPart.gjelderEiendommer != null)
                    {
                        foreach (var eiendom in beroertPart.gjelderEiendommer)
                        {
                            if (!gjelderEiendom.Contains(eiendom))
                            {
                                gjelderEiendom.Add(eiendom);
                            }
                        }
                    }
                }
                var beroertPartType = group.FirstOrDefault();
                beroertPartType.gjelderEiendommer = gjelderEiendom.ToArray();
                beroerteParterAsList.Add(beroertPartType);
            }
            return beroerteParterAsList;
        }
    }
}
