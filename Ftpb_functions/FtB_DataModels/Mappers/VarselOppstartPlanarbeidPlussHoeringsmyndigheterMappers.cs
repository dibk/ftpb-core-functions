﻿using AutoMapper;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using System.Collections.Generic;
using System.Linq;

namespace FtB_DataModels.Mappers
{
    public static class VarselOppstartPlanarbeidPlussHoeringsmyndigheterMappers
    {
        public static IMapper GetBerortPartMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AllowNullCollections = true;
                cfg.CreateMap<Beroertpart, no.kxml.skjema.dibk.planuttalelse.BeroertPartType>()
                    .ForMember(type => type.partstype, opt => opt.MapFrom(src => src.Partstype))
                    .ForMember(type => type.foedselsnummer, opt => opt.MapFrom(src => src.Foedselsnummer))
                    .ForMember(type => type.organisasjonsnummer, opt => opt.MapFrom(src => src.Organisasjonsnummer))
                    .ForMember(type => type.navn, opt => opt.MapFrom(src => src.Navn))
                    .ForMember(type => type.telefon, opt => opt.MapFrom(src => src.Telefon))
                    .ForMember(type => type.epost, opt => opt.MapFrom(src => src.Epost))
                    .ForMember(type => type.adresse, opt => opt.MapFrom(src => src.Adresse))
                    .ForMember(type => type.systemReferanse, opt => opt.MapFrom(src => src.Systemreferanse))
                    .ForMember(type => type.gjelderEiendom, opt => opt.MapFrom(src => src.GjelderEiendom))
                    .ForSourceMember(src => src.BeskrivelseForVarsel, opt => opt.DoNotValidate())
                    .ForSourceMember(src => src.ErHoeringsmyndighet, opt => opt.DoNotValidate());

                CreateMappingConfigForAktoer(cfg);

                cfg.CreateMap<Eiendom, no.kxml.skjema.dibk.planuttalelse.GjelderEiendomType>()
                    .ForMember(type => type.bolignummer, opt => opt.MapFrom(src => src.Bolignummer))
                    .ForMember(type => type.bygningsnummer, opt => opt.MapFrom(src => src.Bygningsnummer))
                    .ForMember(type => type.eiendomsidentifikasjon, opt => opt.MapFrom(src => src.Eiendomsidentifikasjon))
                    .ForMember(type => type.adresse, opt => opt.MapFrom(src => src.Adresse))
                    .ForMember(type => type.kommunenavn, opt => opt.MapFrom(src => src.Kommunenavn));

                cfg.CreateMap<Eiendomsidentifikasjon, no.kxml.skjema.dibk.planuttalelse.MatrikkelnummerType>()
                    .ForMember(type => type.kommunenummer, opt => opt.MapFrom(src => src.Kommunenummer))
                    .ForMember(type => type.gaardsnummer, opt => opt.MapFrom(src => src.Gaardsnummer))
                    .ForMember(type => type.bruksnummer, opt => opt.MapFrom(src => src.Bruksnummer))
                    .ForMember(type => type.festenummer, opt => opt.MapFrom(src => src.Festenummer))
                    .ForMember(type => type.seksjonsnummer, opt => opt.MapFrom(src => src.Seksjonsnummer));

                cfg.CreateMap<Adresse, no.kxml.skjema.dibk.planuttalelse.EiendommensAdresseType>()
                    .ForMember(type => type.adresselinje1, opt => opt.MapFrom(src => src.Adresselinje1))
                    .ForMember(type => type.adresselinje2, opt => opt.MapFrom(src => src.Adresselinje2))
                    .ForMember(type => type.adresselinje3, opt => opt.MapFrom(src => src.Adresselinje3))
                    .ForMember(type => type.postnr, opt => opt.MapFrom(src => src.Postnr))
                    .ForMember(type => type.poststed, opt => opt.MapFrom(src => src.Poststed))
                    .ForMember(type => type.landkode, opt => opt.MapFrom(src => src.Landkode))
                    .ForMember(type => type.gatenavn, opt => opt.MapFrom(src => src.Gatenavn))
                    .ForMember(type => type.husnr, opt => opt.MapFrom(src => src.Husnr))
                    .ForMember(type => type.bokstav, opt => opt.MapFrom(src => src.Bokstav));
            });
            return config.CreateMapper();
        }

        public static IMapper GetAktoerMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AllowNullCollections = true;
                CreateMappingConfigForAktoer(cfg);
            });
            return config.CreateMapper();
        }

        private static void CreateMappingConfigForAktoer(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<AktoerPlan, no.kxml.skjema.dibk.planuttalelse.PartType>()
                .ForMember(type => type.partstype, opt => opt.MapFrom(src => src.Partstype))
                .ForMember(type => type.foedselsnummer, opt => opt.MapFrom(src => src.Foedselsnummer))
                .ForMember(type => type.organisasjonsnummer, opt => opt.MapFrom(src => src.Organisasjonsnummer))
                .ForMember(type => type.navn, opt => opt.MapFrom(src => src.Navn))
                .ForMember(type => type.epost, opt => opt.MapFrom(src => src.Epost))
                .ForMember(type => type.adresse, opt => opt.MapFrom(src => src.Adresse))
                .ForMember(type => type.telefonnummer, opt => opt.MapFrom(src => src.Telefon));

            cfg.CreateMap<EnkelAdresse, no.kxml.skjema.dibk.planuttalelse.EnkelAdresseType>()
                .ForMember(type => type.adresselinje1, opt => opt.MapFrom(src => src.Adresselinje1))
                .ForMember(type => type.adresselinje2, opt => opt.MapFrom(src => src.Adresselinje2))
                .ForMember(type => type.adresselinje3, opt => opt.MapFrom(src => src.Adresselinje3))
                .ForMember(type => type.postnr, opt => opt.MapFrom(src => src.Postnr))
                .ForMember(type => type.poststed, opt => opt.MapFrom(src => src.Poststed))
                .ForMember(type => type.landkode, opt => opt.MapFrom(src => src.Landkode));

            cfg.CreateMap<Kodeliste, no.kxml.skjema.dibk.planuttalelse.KodeType>()
                .ForMember(type => type.kodeverdi, opt => opt.MapFrom(src => src.Kodeverdi))
                .ForMember(type => type.kodebeskrivelse, opt => opt.MapFrom(src => src.Kodebeskrivelse));
        }

        public static IMapper GetVorpaPlussHReceiverMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<no.kxml.skjema.dibk.planuttalelse.BeroertPartType, DistributionReceiver>()
                    .ForMember(dest => dest.Ssn, opt => opt.MapFrom(src => src.foedselsnummer))
                    .ForMember(dest => dest.Orgnr, opt => opt.MapFrom(src => src.organisasjonsnummer))
                    .ForMember(dest => dest.Epost, opt => opt.MapFrom(src => src.epost))
                    //.ForMember(dest => dest.Kommentar, opt => opt.MapFrom(src => src..kommentar))
                    .ForMember(dest => dest.Navn, opt => opt.MapFrom(src => src.navn))
                    .ForMember(dest => dest.SystemReferanse, opt => opt.MapFrom(src => src.systemReferanse))
                    .ForMember(dest => dest.Telefon, opt => opt.MapFrom(src => src.telefon))
                    .ForSourceMember(s => s.adresse, opt => opt.DoNotValidate())
                    .ForSourceMember(s => s.gjelderEiendom, opt => opt.DoNotValidate())
                    .ForSourceMember(s => s.partstype, opt => opt.DoNotValidate())
                    ;
            });
            return config.CreateMapper();
        }

        public static List<Beroertpart> GetListOfPlanvarselBeroertPartWithEiendomFromGroups(List<IGrouping<string, Beroertpart>> groupsWithBeroerteParterWithDecryptedIDs)
        {

            var beroerteParterAsList = new List<Beroertpart>();

            foreach (var group in groupsWithBeroerteParterWithDecryptedIDs)
            {
                List<Eiendom> gjelderEiendom = new List<Eiendom>();
                foreach (var beroertPart in group)
                {
                    if (beroertPart.GjelderEiendom != null)
                    {
                        foreach (var eiendom in beroertPart.GjelderEiendom)
                        {
                            if (!gjelderEiendom.Contains(eiendom))
                            {
                                gjelderEiendom.Add(eiendom);
                            }
                        }
                    }
                }
                var beroertPartType = group.FirstOrDefault();
                beroertPartType.GjelderEiendom = gjelderEiendom.ToArray();
                beroerteParterAsList.Add(beroertPartType);
            }
            return beroerteParterAsList;
        }
    }
}
