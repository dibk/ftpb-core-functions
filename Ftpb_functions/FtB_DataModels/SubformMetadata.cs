﻿using System.Collections.Generic;
using System.Linq;

namespace FtB_DataModels
{
    public class SubformMetadata
    {
        public string DataformatId { get; set; }
        public string DataformatVersion { get; set; }
        public string SubformName { get; set; }
        public string SvarUtPdfFilename { get; set; }
        public string SvarUtXmlFilename { get; set; }
    }

    public static class SubformMetadataProvider
    {
        private static List<SubformMetadata> _subformMetadata => new List<SubformMetadata>() { new SubformMetadata() { DataformatId = "7063", DataformatVersion = "47177", SubformName = "Sluttrapport for bygningsavfall", SvarUtXmlFilename = "Sluttrapport for bygningsavfall", SvarUtPdfFilename = "Kvittering for sluttrapport for bygningsavfall" } };

        public static SubformMetadata GetSubformMetadata(string dataformatId, string dataformatVersion, string subformName)
        {
            var subformMetadata = _subformMetadata.FirstOrDefault(x => x.DataformatId.Equals(dataformatId) && x.DataformatVersion.Equals(dataformatVersion));
            if (subformMetadata != null)
                return subformMetadata;
            else
                return new SubformMetadata()
                {
                    DataformatId = dataformatId,
                    DataformatVersion = dataformatVersion,
                    SubformName = subformName,
                    SvarUtPdfFilename = subformName,
                    SvarUtXmlFilename = subformName
                };
        }
    }
}