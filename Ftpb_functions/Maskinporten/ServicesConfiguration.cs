﻿using FtB_Common.TokenCache;
using Maskinporten.Certificate;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Maskinporten
{
    public static class ServicesConfiguration
    {
        public static IServiceCollection AddMaskinportenClient(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHttpClient<MaskinportenClient>();
            services.Configure<MaskinportenClientConfiguration>(configuration.GetSection(MaskinportenClientConfiguration.SectionName));
            services.AddSingleton<TokenCache>();
            services.Configure<CertificateSettings>(configuration.GetSection(CertificateSettings.ConfigSection));

            var certificateStoreType = configuration.GetValue<string>($"{CertificateSettings.ConfigSection}:StoreType");

            if (certificateStoreType == "AzureKeyVault")
                services.AddSingleton<ICertificateReader, KeyVaultCertificateReader>();
            else
                services.AddSingleton<ICertificateReader, LocalStoreCertificateReader>();

            return services;
        }
    }
}