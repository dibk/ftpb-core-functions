using FtB_Common.Enums;
using FtB_Common.TokenCache;
using Maskinporten.Certificate;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Maskinporten
{
    public class MaskinportenClient
    {
        private const string GrantType = "urn:ietf:params:oauth:grant-type:jwt-bearer";
        private const string MediaTypeFromUrl = "application/x-www-form-urlencoded";
        private const string CharsetUtf8 = "utf-8";
        private readonly MaskinportenClientConfiguration _configuration;
        private readonly HttpClient _httpClient;
        private readonly ILogger<MaskinportenClient> _logger;
        private readonly TokenCache _tokenCache;
        private readonly ICertificateReader _certificateReader;

        public MaskinportenClient(IOptions<MaskinportenClientConfiguration> configuration,
                                  HttpClient httpClient,
                                  ILogger<MaskinportenClient> logger,
                                  TokenCache tokenCache,
                                  ICertificateReader certificateReader)
        {
            _configuration = configuration.Value;
            _httpClient = httpClient;
            _logger = logger;
            _tokenCache = tokenCache;
            _certificateReader = certificateReader;
        }

        public async Task<string> GetAccessToken(IEnumerable<string> scopes, string consumerOrg = null)
        {
            if (_tokenCache.TryGetTokenValue(TokenTypeEnum.Maskinporten, scopes, consumerOrg, out var tokenValue))
                return tokenValue;

            var scopesAsString = ScopesAsString(scopes);
            var accessToken = await GetNewAccessToken(scopesAsString, consumerOrg);

            _tokenCache.SetToken(accessToken.Token, TokenTypeEnum.Maskinporten, scopes, consumerOrg, accessToken.GetExpiryTime());

            return accessToken.Token;
        }

        private async Task<MaskinportenToken> GetNewAccessToken(string scopes, string consumerOrg)
        {
            try
            {
                SetRequestHeaders();
                var requestContent = CreateRequestContent(scopes, consumerOrg);

                var tokenUri = new Uri(_configuration.TokenEndpoint);
                var response = await _httpClient.PostAsync(tokenUri, requestContent).ConfigureAwait(false);

                await ThrowIfResponseIsInvalid(response).ConfigureAwait(false);

                return await CreateTokenFromResponse(response).ConfigureAwait(false);
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, $"Unable to create AccessToken from {_configuration.TokenEndpoint}. Error: {exception.Message}");

                return null;
            }
        }

        private void SetRequestHeaders()
        {
            _httpClient.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue()
            {
                NoCache = true
            };
        }

        private FormUrlEncodedContent CreateRequestContent(string scopes, string consumerOrg)
        {
            var _tokenGenerator = new JwtRequestTokenGenerator(_certificateReader.GetCertificate());
            var content = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("grant_type", GrantType),
                new KeyValuePair<string, string>("assertion", _tokenGenerator.CreateEncodedJwt(scopes, _configuration, consumerOrg))
            });

            content.Headers.ContentType = new MediaTypeHeaderValue(MediaTypeFromUrl);
            content.Headers.Add("Charset", CharsetUtf8);

            return content;
        }

        private async Task ThrowIfResponseIsInvalid(HttpResponseMessage response)
        {
            if (response.StatusCode != HttpStatusCode.OK)
            {
                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                throw new UnexpectedResponseException($"Got unexpected HTTP Status code {response.StatusCode} from {_configuration.TokenEndpoint}. Content: {content}.");
            }
        }

        private async Task<MaskinportenToken> CreateTokenFromResponse(HttpResponseMessage response)
        {
            var maskinportenResponse = await ReadResponse(response).ConfigureAwait(false);

            return new MaskinportenToken(
                maskinportenResponse.AccessToken,
                ExpirationTimeInSeconds(maskinportenResponse.ExpiresIn));
        }

        private int ExpirationTimeInSeconds(int tokenExpiresIn)
        {
            return tokenExpiresIn - _configuration.NumberOfSecondsLeftBeforeExpire;
        }

        private static async Task<MaskinportenResponse> ReadResponse(HttpResponseMessage responseMessage)
        {
            var responseAsJson = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
            return JsonConvert.DeserializeObject<MaskinportenResponse>(responseAsJson);
        }

        private static string ScopesAsString(IEnumerable<string> scopes) => string.Join(" ", scopes);
    }
}