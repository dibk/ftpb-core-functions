﻿using JWT;
using JWT.Algorithms;
using JWT.Builder;
using JWT.Serializers;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace Maskinporten
{
    public class JwtRequestTokenGenerator : IJwtRequestTokenGenerator
    {
        private const int JwtExpireTimeInMinutes = 2;
        private const string DummyKey = ""; // Required by encoder, but not used with RS256Algorithm

        private readonly JwtEncoder encoder;
        private readonly X509Certificate2 certificate;

        private static IDictionary<string, object> CreateJwtPayload(string scope, MaskinportenClientConfiguration configuration, string consumerOrg)
        {
            var jwtData = new JwtData();

            jwtData.Payload.Add("aud", configuration.Audience);
            jwtData.Payload.Add("scope", scope);
            jwtData.Payload.Add("iss", configuration.Issuer);
            jwtData.Payload.Add("exp", UnixEpoch.GetSecondsSince(DateTime.UtcNow.AddMinutes(JwtExpireTimeInMinutes)));
            jwtData.Payload.Add("iat", UnixEpoch.GetSecondsSince(DateTime.UtcNow));
            jwtData.Payload.Add("jti", Guid.NewGuid());
            if (consumerOrg != null)
            {
                jwtData.Payload.Add("consumer_org", consumerOrg);
            }

            return jwtData.Payload;
        }

        public JwtRequestTokenGenerator(X509Certificate2 certificate)
        {
            this.certificate = certificate;
            this.encoder = new JwtEncoder(
                new RS256Algorithm(this.certificate.GetRSAPublicKey(), this.certificate.GetRSAPrivateKey()),
                new JsonNetSerializer(),
                new JwtBase64UrlEncoder());
        }

        public string CreateEncodedJwt(string scope, MaskinportenClientConfiguration configuration, string consumerOrg)
        {
            var payload = CreateJwtPayload(scope, configuration, consumerOrg);
            var header = CreateJwtHeader();
            header.Remove("typ");
            header.Remove("kid");
            var jwt = this.encoder.Encode(header, payload, DummyKey);

            return jwt;
        }

        private IDictionary<string, object> CreateJwtHeader()
        {
            return new Dictionary<string, object>
            {
                {
                    "x5c",
                    new List<string>() { Convert.ToBase64String(this.certificate.Export(X509ContentType.Cert)) }
                }
            };
        }
    }
}