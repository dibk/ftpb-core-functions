﻿namespace Maskinporten
{
    public interface IJwtRequestTokenGenerator
    {
        string CreateEncodedJwt(string scope, MaskinportenClientConfiguration configuration, string consumerOrg);
    }
}