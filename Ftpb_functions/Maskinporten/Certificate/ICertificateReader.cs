﻿using System.Security.Cryptography.X509Certificates;

namespace Maskinporten.Certificate
{
    public interface ICertificateReader
    {
        X509Certificate2 GetCertificate();
    }
}