﻿using Azure.Identity;
using Azure.Security.KeyVault.Certificates;
using Microsoft.Extensions.Options;
using System;
using System.Security.Cryptography.X509Certificates;

namespace Maskinporten.Certificate
{
    public class KeyVaultCertificateReader : ICertificateReader
    {
        private readonly X509Certificate2 _certificate;
        private readonly string _vaultUrl;
        private readonly string _certName;
        private readonly string _tenantId;

        public KeyVaultCertificateReader(IOptions<CertificateSettings> settings)
        {
            _vaultUrl = settings.Value.AzureKeyVault.VaultUrl;
            _certName = settings.Value.AzureKeyVault.CertificateName;
            _tenantId = settings.Value.AzureKeyVault.TenantId;

            if (string.IsNullOrEmpty(_certName))
                throw new ArgumentException($"Setting 'CertificateName' must be set");

            if (string.IsNullOrEmpty(_vaultUrl))
                throw new ArgumentException($"Setting 'VaultUrl' must be set");

            _certificate = LoadCertificate();
        }

        public X509Certificate2 GetCertificate() => _certificate;

        private X509Certificate2 LoadCertificate()
        {
            var client = new CertificateClient(vaultUri: new Uri(_vaultUrl), credential: GetAzureCredentials());

            var kvCert = client.DownloadCertificate(_certName);

            return kvCert.Value;
        }

        private DefaultAzureCredential GetAzureCredentials()
        {
            DefaultAzureCredential azureCredential = null;
            if (!string.IsNullOrWhiteSpace(_tenantId))
                azureCredential = new DefaultAzureCredential(new DefaultAzureCredentialOptions() { TenantId = _tenantId });
            else
                azureCredential = new DefaultAzureCredential();

            return azureCredential;
        }
    }
}