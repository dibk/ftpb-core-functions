﻿using Microsoft.Extensions.Options;
using System.Security.Cryptography.X509Certificates;

namespace Maskinporten.Certificate
{
    public class LocalStoreCertificateReader : ICertificateReader
    {
        private readonly string _certificateThumbprint;
        private readonly X509Certificate2 _certificate;

        public LocalStoreCertificateReader(IOptions<CertificateSettings> settings)
        {
            _certificateThumbprint = settings.Value.LocalStore.CetificateThumbprint;
            _certificate = LoadCertificate();
        }

        public X509Certificate2 GetCertificate() => _certificate;

        private X509Certificate2 LoadCertificate()
        {
            if (!string.IsNullOrEmpty(_certificateThumbprint))
            {
                var store = new X509Store(StoreLocation.CurrentUser);
                store.Open(OpenFlags.ReadOnly);
                var certificates = store.Certificates.Find(X509FindType.FindByThumbprint, _certificateThumbprint, false);
                store.Close();
                return certificates.Count > 0 ? certificates[0] : null;
            }
            return null;
        }
    }
}