﻿namespace Maskinporten
{
    public class MaskinportenClientConfiguration
    {
        public static readonly string SectionName = "MaskinportenClient";

        public string Audience { get; set; }

        public string TokenEndpoint { get; set; }

        public string Issuer { get; set; }

        public int NumberOfSecondsLeftBeforeExpire { get; set; }
        public string CertificateThumbprint { get; set; }
    }
}