﻿using Altinn3.Adapters;
using Dibk.Ftpb.ModelToPdf.Common.Client;
using Elastic.Apm.Azure.Functions;
using FtB_Common;
using FtB_FuncPrepareSending;
using FtB_Print.Config;
using FtB_Print.Services.Print.Implementations;
using FtB_ProcessStrategies;
using FtB_Validation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using no.kxml.skjema.dibk.nabovarselPlan;
using Serilog;
using Serilog.Debugging;
using System;

var host = new HostBuilder()
    .ConfigureAppConfiguration(builder =>
    {
        builder
            .SetBasePath(Environment.CurrentDirectory)
            .AddJsonFile("local.settings.json", optional: true, reloadOnChange: true)
            .AddEnvironmentVariables()
            .Build();
    })
    .ConfigureFunctionsWorkerDefaults((ctx, builder) =>
    {
        if (!string.IsNullOrEmpty(ctx.Configuration["ELASTIC_APM_SERVER_URL"]))
            builder.UseMiddleware<ApmMiddleware>();
    })
    .ConfigureLogging((ctx, logging) =>
    {
        Log.Logger = SerilogServiceCollectionExtension.ConfigureSerilog(ctx.Configuration).CreateLogger();
        logging.AddSerilog(Log.Logger, true);

        if (bool.TryParse(ctx.Configuration["Serilog:SelfLogEnabled"], out var selflogEnabled))
            if (selflogEnabled)
                SelfLog.Enable(msg => Console.WriteLine(msg));
    })
    .ConfigureServices(services =>
    {
        var configuration = services.BuildServiceProvider().GetService<IConfiguration>();

        services.AddScoped<SubmittalQueueProcessor>();
        services.AddScoped<AltinnEventProcessor>();
        services.AddScoped<AltinnEventPersister>();
        services.AddAltinn3PrefillService(configuration);
        services.AddPrepareServices(configuration);

        services.AddPrinting(configuration, settings =>
        {
            settings.AddPrintService<VarselOppstartPlanarbeidPrintService, NabovarselPlanType>();
        });

        services.AddValidation(validationUri: new Uri(configuration["Validation:ValidationUrl"]),
                               legacyValidationUri: new Uri(configuration["Validation:LegacyValidtionUrl"]));
        services.AddDatamodelToPdfService(new Uri(configuration["DatamodelToPdf:ApiUrl"]));
    })
    .Build();

host.Run();