﻿using FtB_Common;
using FtB_Common.Encryption;
using FtB_Common.FormDataRepositories;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.TokenCache;
using FtB_FormLogic;
using FtB_FormLogic.AltinnEventsLogic;
using FtB_FormLogic.Mappers;
using FtB_FormLogic.Shipments.ShipmentFormLogic.Ferdigattest;
using FtB_FormLogic.Shipments.ShipmentFormLogic.IgangsettingstillatelseV3;
using FtB_FormLogic.Shipments.ShipmentFormLogic.InnsendingAvReguleringsplanforslag;
using FtB_FormLogic.Shipments.ShipmentFormLogic.SoknadOmSamtykkeLogic;
using FtB_FormLogic.Shipments.ShipmentFormLogic.SuppleringAvSoeknad;
using FtB_FormLogic.Shipments.ShipmentFormLogic.SuppleringAvSoeknadATIL;
using Ftb_Repositories;
using KontaktOgReservasjonsregisteret;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FtB_FuncPrepareSending
{
    public static class ServicesConfiguration
    {
        public static IServiceCollection AddPrepareServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<EnvironmentProvider>();
            services.AddScoped<IgangsettingstillatelseProcessCompletedEventHandler>();
            services.AddScoped<InnsendingPlanforslagProcessCompletedEventHandler>();
            services.AddScoped<VarselHoffeProcessCompletedEventHandler>();
            services.AddScoped<UttalelseHoffeProcessCompletedEventHandler>();
            services.AddScoped<VarselPlanoppstartProcessCompletedEventHandler>();
            services.AddScoped<UttalelsePlanoppstartProcessCompletedEventHandler>();
            services.AddScoped<VarselOppstartPlanarbeidPrepareLogic>();
            services.AddScoped<VarselOppstartPlanarbeidPlussHoeringsmyndigheterPrepareLogic>();
            services.AddScoped<InnsendingAvReguleringsplanforslagPrepareLogic>();
            services.AddScoped<HoeringOgOffentligEttersynPrepareLogic>();
            services.AddScoped<SvarVarselOppstartPlanarbeidPrepareLogic>();
            services.AddScoped<SvarVarselOppstartPlanarbeidPlussHoeringsmyndigheterPrepareLogic>();
            services.AddScoped<SvarHoeringOgOffentligEttersynPrepareLogic>();
            services.AddScoped<SoknadOmSamtykkePrepareLogic>();
            services.AddScoped<SuppleringAvSoeknadPrepareLogic>();
            services.AddScoped<SuppleringAvSoeknadATILPrepareLogic>();
            services.AddScoped<FerdigattestPrepareLogic>();
            services.AddScoped<IgangsettingstillatelseV3PrepareLogic>();
            services.AddScoped<FormatIdToFormMapper>();
            services.AddScoped<AltinnResourceAndTypeToFormMapper>();
            services.AddScoped<IFormDataRepo, FormDataRepository>();

            services.AddStorageClients(configuration["PrivateAzureStorageConnectionString"], configuration["PublicAzureStorageConnectionString"]);
            services.AddDtoRepos();

            services.AddFtbRepositories(configuration);

            services.AddScoped<IDecryption, Decryption>();
            services.AddScoped<IDecryptionFactory, DecryptionFactory>();
            services.Configure<EncryptionSettings>(configuration.GetSection("EncryptionSettings"));

            services.AddHttpClient<KRRClient>();
            services.Configure<KrrClientConfiguration>(configuration.GetSection(KrrClientConfiguration.SectionName));

            services.AddSingleton<TokenCache>();

            return services;
        }
    }
}
