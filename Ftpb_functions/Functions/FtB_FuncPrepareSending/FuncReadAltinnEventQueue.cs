using Azure.Messaging.ServiceBus;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_ProcessStrategies;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;

namespace FtB_FuncPrepareSending
{
    public class FuncReadAltinnEventQueue
    {
        private readonly ILogger<FuncReadAltinnEventQueue> _logger;
        private readonly AltinnEventProcessor _queueMessageProcessor;

        public FuncReadAltinnEventQueue(ILogger<FuncReadAltinnEventQueue> logger, AltinnEventProcessor queueMessageProcessor)
        {
            _logger = logger;
            _queueMessageProcessor = queueMessageProcessor;
        }

        [Function("FuncReadAltinnEventQueue")]
        [ServiceBusOutput("%SubmittalQueueName%", Connection = "QueueConnectionString", EntityType = ServiceBusEntityType.Queue)]
        public async Task<SubmittalQueueItem> Run([ServiceBusTrigger("%AltinnEventQueueName%", Connection = "QueueConnectionString")] ServiceBusReceivedMessage message, ServiceBusMessageActions messageActions)
        {
            var altinnEventRawQueueMessage = System.Text.Encoding.Default.GetString(message.Body);
            AltinnEventQueueItem altinnEventQueueItem;
            try
            {
                var opts = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };
                altinnEventQueueItem = JsonSerializer.Deserialize<AltinnEventQueueItem>(altinnEventRawQueueMessage, opts);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to deserialize message to AltinnEventQueueItem: {AltinnEventRawQueueMessage}", altinnEventRawQueueMessage);
                await messageActions.DeadLetterMessageAsync(message);
                throw;
            }

            if (altinnEventQueueItem?.ArchiveReference != null)
            {
                using (var scope = _logger.BeginScope(new Dictionary<string, string> { { "ArchiveReference", altinnEventQueueItem.ArchiveReference }, { "AltinnEventType", altinnEventQueueItem.AltinnEventType } }))
                {
                    try
                    {
                        var result = await _queueMessageProcessor.ExecuteProcessingStrategy(altinnEventQueueItem);

                        await messageActions.CompleteMessageAsync(message);

                        return result;
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, "An error occurred when processing {AltinnEventRawQueueMessage}", altinnEventRawQueueMessage);
                        throw;
                    }
                }
            }
            else
            {
                _logger.LogError("AltinnEventQueueItem is null, unable to process altinnEventQueueMessage: {AltinnEventRawQueueMessage}", altinnEventRawQueueMessage);
                await messageActions.DeadLetterMessageAsync(message);
                throw new ArgumentNullException("altinnEventQueueItem", "AltinnEventQueueItem is null, unable to process SubmittalQueueMessage: {altinnEventRawQueueMessage}");
            }
        }
    }
}