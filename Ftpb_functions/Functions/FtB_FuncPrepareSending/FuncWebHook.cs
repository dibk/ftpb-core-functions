using FtB_Common.BusinessModels;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_ProcessStrategies;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace FtB_FuncPrepareSending
{
    public class FuncWebHook
    {
        private readonly ILogger<FuncWebHook> _logger;
        private readonly AltinnEventPersister _altinnEventPersister;

        public FuncWebHook(ILogger<FuncWebHook> logger, AltinnEventPersister altinnEventPersister)
        {
            _logger = logger;
            _altinnEventPersister = altinnEventPersister;
        }

        [Function("HandleAltinnEvents")]
        public async Task<FuncWebHookOutput> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "events")] HttpRequestData req,
            FunctionContext context)
        {
            var response = req.CreateResponse();

            string requestBody = null;
            AltinnEvent altinnEvent;
            try
            {
                requestBody = await new StreamReader(req.Body).ReadToEndAsync();
                var opts = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };
                altinnEvent = JsonSerializer.Deserialize<AltinnEvent>(requestBody, opts);
                _logger.LogDebug("AltinnEvent {AltinnEventType} received for instance: {AltinnInstance}", altinnEvent.Type, altinnEvent.ResourceInstance);
            }
            catch (Exception ex)
            {
                var errorMessage = $"Unable to deserialize message to AltinnEvent: {requestBody}";
                _logger.LogError(ex, errorMessage);

                response.StatusCode = System.Net.HttpStatusCode.BadRequest; //Bad Request
                await response.WriteAsJsonAsync(errorMessage);

                return new FuncWebHookOutput { HttpResponse = response };
            }

            if (!altinnEvent.Type.Equals(AltinnEventTypes.InstanceCompleted))
            {
                response.StatusCode = System.Net.HttpStatusCode.OK; //OK
                return new FuncWebHookOutput { HttpResponse = response };
            }

            AltinnEventQueueItem altinnEventQueueItem;
            try
            {
                altinnEventQueueItem = await _altinnEventPersister.HandleEvent(altinnEvent);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while processing AltinnStrategy for {altinnResource}, with the following request: {request}", altinnEvent.ResourceInstance, requestBody);

                response.StatusCode = System.Net.HttpStatusCode.NotImplemented; //Not Implemented
                return new FuncWebHookOutput { HttpResponse = response };
            }

            try
            {
                response.StatusCode = System.Net.HttpStatusCode.OK; //OK
                return new FuncWebHookOutput
                {
                    HttpResponse = response,
                    AltinnEventQueueItem = altinnEventQueueItem
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred when processing {SubmittalQueueMessage}", requestBody);
                throw;
            }
        }

        public class FuncWebHookOutput
        {
            [ServiceBusOutput("%AltinnEventQueueName%", Connection = "QueueConnectionString", EntityType = ServiceBusEntityType.Queue)]
            public AltinnEventQueueItem AltinnEventQueueItem { get; set; }

            public HttpResponseData HttpResponse { get; set; }
        }
    }
}