using Azure.Messaging.ServiceBus;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_ProcessStrategies;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;

namespace FtB_FuncPrepareSending
{
    public class FuncReadSubmittalQueue
    {
        private readonly ILogger<FuncReadSubmittalQueue> _logger;
        private readonly SubmittalQueueProcessor _queueProcessor;

        public FuncReadSubmittalQueue(ILogger<FuncReadSubmittalQueue> logger, SubmittalQueueProcessor queueProcessor)
        {
            _logger = logger;
            _queueProcessor = queueProcessor;
        }

        [Function("FuncReadSubmittalQueue")]
        [ServiceBusOutput("%SendingQueueName%", Connection = "QueueConnectionString", EntityType = ServiceBusEntityType.Queue)]
        public async Task<IEnumerable<SendQueueItem>> Run([ServiceBusTrigger("%SubmittalQueueName%", Connection = "QueueConnectionString")] ServiceBusReceivedMessage message, ServiceBusMessageActions messageActions)
        {
            var submittalQueueMessage = System.Text.Encoding.Default.GetString(message.Body);
            SubmittalQueueItem submittalQueueItem;
            try
            {
                var opts = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };
                submittalQueueItem = System.Text.Json.JsonSerializer.Deserialize<SubmittalQueueItem>(submittalQueueMessage, opts);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to deserialize message to SubmittalQueueMessage: {SubmittalQueueMessage}", submittalQueueMessage);
                await messageActions.DeadLetterMessageAsync(message);
                throw;
            }

            if (submittalQueueItem?.ArchiveReference != null)
            {
                using (var scope = _logger.BeginScope(new Dictionary<string, string> { { "ArchiveReference", submittalQueueItem.ArchiveReference } }))
                {
                    try
                    {
                        var results = await _queueProcessor.ExecuteProcessingStrategy(submittalQueueItem);

                        await messageActions.CompleteMessageAsync(message);

                        return results;
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, "An error occurred when processing {SubmittalQueueMessage}", submittalQueueMessage);
                        throw;
                    }
                }
            }
            else
            {
                _logger.LogError("SubmittalQueueItem is null, unable to process SubmittalQueueMessage: {SubmittalQueueMessage}", submittalQueueMessage);
                await messageActions.DeadLetterMessageAsync(message);
                throw new ArgumentNullException("submittalQueueItem", $"SubmittalQueueItem is null, unable to process SubmittalQueueMessage: {submittalQueueMessage}");
            }
        }
    }
}