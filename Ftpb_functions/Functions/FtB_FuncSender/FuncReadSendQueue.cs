﻿using Azure.Messaging.ServiceBus;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_Common.Exceptions;
using FtB_ProcessStrategies;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace FtB_FuncSender
{
    public class FuncReadSendQueue
    {
        private readonly ILogger<FuncReadSendQueue> _logger;
        private readonly SendQueueProcessor _queueProcessor;

        public FuncReadSendQueue(ILogger<FuncReadSendQueue> logger, SendQueueProcessor queueProcessor)
        {
            _logger = logger;
            _queueProcessor = queueProcessor;
        }

        [Function("FuncReadSendQueue")]
        [ServiceBusOutput("%ReportQueueName%", Connection = "QueueConnectionString", EntityType = ServiceBusEntityType.Queue)]
        public async Task<ReportQueueItem> Run([ServiceBusTrigger("%SendingQueueName%", Connection = "QueueConnectionString")] ServiceBusReceivedMessage message, ServiceBusMessageActions messageActions)
        {
            var sendQueueMessage = System.Text.Encoding.Default.GetString(message.Body);
            SendQueueItem sendQueueItem;
            try
            {
                sendQueueItem = JsonConvert.DeserializeObject<SendQueueItem>(sendQueueMessage);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to deserialize message to SendQueueItem: {SendQueueMessage}", sendQueueMessage);
                await messageActions.DeadLetterMessageAsync(message);
                throw;
            }

            if (sendQueueItem != null)
                using (var scope = _logger.BeginScope(new Dictionary<string, string> { { "ArchiveReference", sendQueueItem.ArchiveReference }, { "ReceiverId", sendQueueItem.Receiver.Id } }))
                {
                    try
                    {
                        var sw = new Stopwatch();
                        sw.Start();
                        var result = await _queueProcessor.ExecuteProcessingStrategyAsync(sendQueueItem);
                        sw.Stop();
                        _logger.LogInformation("Elapsed time for processing SendQueueItem: {elapsedMilliseconds} ms", sw.ElapsedMilliseconds);
                        await messageActions.CompleteMessageAsync(message);

                        return result;
                    }
                    catch (CancelProcessException cancelException)
                    {
                        await messageActions.DeadLetterMessageAsync(message, deadLetterReason: cancelException.Message);
                        _logger.LogError($"Unable to process SendQueueItem with reference: {sendQueueItem.ArchiveReference} due to error from SvarUt: {cancelException.Message}");
                        return null;
                    }
                    catch (Exception)
                    {
                        // for debugging purposes??
                        throw;
                    }
                }
            else
            {
                _logger.LogError("SendQueueItem is null, unable to process SendQueueMessage: {SendQueueMessage}", sendQueueMessage);
                await messageActions.DeadLetterMessageAsync(message);
                throw new Exception($"SendQueueItem is null, unable to process SendQueueMessage: {sendQueueMessage}");
            }
        }
    }
}