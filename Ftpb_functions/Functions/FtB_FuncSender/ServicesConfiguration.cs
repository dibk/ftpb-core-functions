﻿using System;
using Altinn.Distribution;
using Altinn.Distribution.ATIL;
using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Integration.SvarUt;
using FtB_Common;
using FtB_Common.Encryption;
using FtB_Common.FormDataRepositories;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.TokenCache;
using FtB_Common.Utils;
using FtB_FormLogic;
using FtB_FormLogic.Distributions.DistributionFormLogic.HoeringOgOffentligEttersynLogic.Send;
using FtB_FormLogic.Distributions.DistributionFormLogic.VarselOppstartPlanarbeidTilHøringsmyndigheterLogic.Send;
using FtB_FormLogic.Shipments.ShipmentFormLogic.Ferdigattest;
using FtB_FormLogic.Shipments.ShipmentFormLogic.IgangsettingstillatelseV3;
using FtB_FormLogic.Shipments.ShipmentFormLogic.InnsendingAvReguleringsplanforslag;
using FtB_FormLogic.Shipments.ShipmentFormLogic.SoknadOmSamtykkeLogic;
using FtB_FormLogic.Shipments.ShipmentFormLogic.SuppleringAvSoeknad;
using FtB_FormLogic.Shipments.SvarUt;
using FtB_Print.Config;
using FtB_Print.Services.Print.Implementations;
using FtB_ProcessStrategies;
using Ftb_Repositories;
using FtB_Validation;
using KontaktOgReservasjonsregisteret;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using no.kxml.skjema.dibk.nabovarselPlan;
using no.kxml.skjema.dibk.offentligEttersyn;
using no.kxml.skjema.dibk.planvarsel;

namespace FtB_FuncSender;

public static class ServicesConfiguration
{
    public static IServiceCollection AddSenderServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddScoped<EnvironmentProvider>();

        services.AddScoped<VarselOppstartPlanarbeidSendLogic>();
        services.AddScoped<SoknadOmSamtykkeSvarUtSendLogic>();
        services.AddScoped<VarselOppstartPlanarbeidPlussHoeringsmyndigheterSendLogic>();
        services.AddScoped<HoeringOgOffentligEttersynSendLogic>();
        services.AddScoped<SvarVarselOppstartPlanarbeidPlussHoeringsmyndigheterSendLogic>();
        services.AddScoped<SvarHoeringOgOffentligEttersynSendLogic>();
        services.AddScoped<SuppleringAvSoeknadSvarUtSendLogic>();
        services.AddScoped<FerdigattestSendLogic>();
        services.AddScoped<InnsendingAvReguleringsplanforslagSendLogic>();
        services.AddScoped<IgangsettingstillatelseV3SendLogic>();

        services.AddScoped<IDistributionDataMapper<NabovarselPlanType>, VarselOppstartPlanarbeidSendDataProvider>();
        services.AddScoped<IDistributionDataMapper<OffentligEttersynType>, HoeringOgOffentligEttersynSendDataProvider>();
        services.AddScoped<IDistributionDataMapper<PlanvarselV2>, VarselOppstartPlanarbeidPlussHoeringsmyndigheterSendDataProvider>();

        services.AddScoped<VarselOppstartPlanarbeidPrefillMapper>();
        services.AddScoped<HoeringOgOffentligEttersynSendPrefillMapper>();
        services.AddScoped<VarselOppstartPlanarbeidPlussHøringsmyndigheterSendPrefillMapper>();

        services.AddScoped<SendQueueProcessor>();

        services.AddStorageClients(configuration["PrivateAzureStorageConnectionString"], configuration["PublicAzureStorageConnectionString"]);
        services.AddDtoRepos();

        services.AddScoped<IHtmlUtils, HtmlUtils>();
        services.AddScoped<FormatIdToFormMapper>();
        services.AddScoped<IFormDataRepo, FormDataRepository>();
        
        services.AddScoped<IDecryption, Decryption>();
        services.AddScoped<IDecryptionFactory, DecryptionFactory>();
        services.Configure<EncryptionSettings>(configuration.GetSection("EncryptionSettings"));
        services.Configure<HtmlUtilSettings>(configuration.GetSection("HtmlUtilSettings"));

        services.AddFtbRepositories(configuration);

        services.AddAltinn2Distribution(configuration);
        services.AddAtilAltinn2Distribution(configuration);
        services.AddAltinnNotification(configuration);
        services.AddAltinn3Distribution(configuration);

        services.AddScoped<VorpahDistribution>();
        services.AddScoped<HoeringOgOffentligEttersynDistribution>();
        services.AddScoped<ISvarUtAdapter, SvarUtAdapter>();

        services.AddScoped<SvarUtShipmentBuilder>();

        services.AddHttpClient<KRRClient>();
        services.Configure<KrrClientConfiguration>(configuration.GetSection(KrrClientConfiguration.SectionName));

        services.AddSingleton<TokenCache>();

        services.AddSvarUtClient(
            new Uri(configuration["SvarUtConfigSettings:BaseAddress"]),
            configuration["SvarUtConfigSettings:UserName"],
            configuration["SvarUtConfigSettings:Password"]
        );

        services.AddPrinting(configuration, settings =>
        {
            settings.AddPrintService<VarselOppstartPlanarbeidPrintService, NabovarselPlanType>(settings =>
            {
                settings.ShipmentTitle = "Varsel om oppstart av planarbeid";
                settings.ShipmentType = "dibk.plan.varselomplanoppstart";
                settings.AccountingCode = "vorpa-print";
            });
            settings.AddPrintService<HoeringOgOffentligEttersynPrintService, OffentligEttersynType>(settings =>
            {
                settings.ShipmentTitle = "Høring og offentlig ettersyn";
                settings.ShipmentType = "dibk.plan.hoeringogoffentligettersyn";
                settings.AccountingCode = "hoffe-print";
            });
            settings.AddPrintService<VarselOppstartPlanarbeidPlussHoeringsmyndigheterPrintService, PlanvarselV2>(settings =>
            {
                settings.ShipmentTitle = "Varsel om oppstart av planarbeid";
                settings.ShipmentType = "dibk.plan.varselomplanoppstart";
                settings.AccountingCode = "vorpa-print";
            });
        });

        services.AddValidation(validationUri: new Uri(configuration["Validation:ValidationUrl"]),
                                       legacyValidationUri: new Uri(configuration["Validation:LegacyValidtionUrl"]));

        return services;
    }
}