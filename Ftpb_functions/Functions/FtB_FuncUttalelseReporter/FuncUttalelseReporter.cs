using FtB_Common.BusinessModels.TableStorage.Dtos;
using FtB_Common.BusinessModels.TableStorage.Entities;
using FtB_ProcessStrategies.Processors.AccumulatedReportProcessor;
using Ftb_Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace FtB_FuncUttalelseReporter
{
    public class FuncUttalelseReporter
    {
        private readonly ILogger<FuncUttalelseReporter> _logger;
        private readonly UttalelseProcessor _uttalelseProcessor;
        private readonly IDbUnitOfWork _dbUnitOfWork;

        public FuncUttalelseReporter(ILogger<FuncUttalelseReporter> logger,
                                                            UttalelseProcessor processor,
                                                            IDbUnitOfWork dbUnitOfWork)
        {
            _logger = logger;
            _uttalelseProcessor = processor;
            _dbUnitOfWork = dbUnitOfWork;
        }

        [Function("ReportUttalelse")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = "report")] HttpRequestData req)
        {
            try
            {
                string type = req.Query["type"];

                type = type != null ? type.ToUpper() : DistributionSubmittalTypes.VORPA;

                if (type.Equals(DistributionSubmittalTypes.VORPA) || type.Equals(DistributionSubmittalTypes.VORPAH) || type.Equals(DistributionSubmittalTypes.HOFFE))
                {
                    var processedArchiveReferences = await _uttalelseProcessor.ExecuteProcessingStrategyAsync(type);

                    string reportSubmittals = "";

                    if (processedArchiveReferences != null)
                    {
                        reportSubmittals = string.Concat(processedArchiveReferences.Select(ar => $"ArchiveReference: {ar}").ToList(), ", ");
                    }

                    string timeStamp = DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss");
                    string responseMessage = string.IsNullOrEmpty(reportSubmittals)
                        ? $"This HTTP triggered function executed successfully for {type}, but nothing to report."
                        : $"This HTTP triggered function executed successfully for {type}, and the following were reported for: {reportSubmittals}. ";

                    return new OkObjectResult(timeStamp + ": " + responseMessage);
                }
                else
                {
                    return new BadRequestObjectResult($"Invalid type: {type}");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unknown error occured in ReportUttalelse");
                return new StatusCodeResult(500);
            }
        }

        [Function("ReportUttalelseTimedTrigger")]
        public async Task RunTimer(
            [TimerTrigger("%ReportTriggerTimer%")] TimerInfo myTimer)
        {
            try
            {
                _logger.LogInformation("Starting ReportUttalelseTimedTrigger for type: VORPAH, VORPA og HOFFE");

                var types = new string[]
                {
                    DistributionSubmittalTypes.HOFFE,
                    DistributionSubmittalTypes.VORPAH,
                    DistributionSubmittalTypes.VORPA
                };

                foreach (var type in types)
                {
                    var processedArchiveReferences = await _uttalelseProcessor.ExecuteProcessingStrategyAsync(type);

                    string reportSubmittals = "";

                    if (processedArchiveReferences != null)
                    {
                        reportSubmittals = string.Join(", ", processedArchiveReferences.Select(ar => $"ArchiveReference:  {ar}").ToList());
                    }

                    string timeStamp = DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss");
                    string responseMessage = string.IsNullOrEmpty(reportSubmittals)
                        ? $"This timer triggered function executed successfully for {type}, but nothing to report."
                        : $"This timer triggered function executed successfully for {type}, and the following were reported for: {reportSubmittals}. ";

                    _logger.LogInformation(timeStamp + ": " + responseMessage);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unknown error occured in ReportUttalelseTimedTrigger");
            }
        }
    }
}