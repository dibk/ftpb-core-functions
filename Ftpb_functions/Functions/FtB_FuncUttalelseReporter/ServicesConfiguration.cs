﻿using Altinn.Distribution;
using FtB_Common.Encryption;
using FtB_Common.FormDataRepositories;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Utils;
using FtB_Print.HttpClients.Pdf;
using FtB_ProcessStrategies.Processors.AccumulatedReportProcessor;
using Ftb_Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FtB_FuncUttalelseReporter;

public static class ServicesConfiguration
{
    public static IServiceCollection AddUttalelseReporterServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddStorageClients(configuration["PrivateAzureStorageConnectionString"], configuration["PublicAzureStorageConnectionString"]);
        services.AddDtoRepos();

        services.AddScoped<IHtmlUtils, HtmlUtils>();
        services.AddScoped<IFormDataRepo, FormDataRepository>();        
        services.AddScoped<IDecryption, Decryption>();
        services.AddScoped<IDecryptionFactory, DecryptionFactory>();
        services.Configure<EncryptionSettings>(configuration.GetSection("EncryptionSettings"));
        services.Configure<HtmlUtilSettings>(configuration.GetSection("HtmlUtilSettings"));
        services.AddScoped<UttalelseProcessor>();
        services.AddFtbRepositories(configuration);
        services.AddAltinn2Distribution(configuration);
        services.AddAltinnNotification(configuration);

        services.AddHttpClient<IPdfHttpClient, PdfHttpClient>();
        services.Configure<PdfSettings>(configuration.GetSection(PdfSettings.SectionName));

        services.AddScoped<IReportMessageBuilder, VorpaReportMessageBuilder>();
        services.AddScoped<IReportMessageBuilder, HoffeReportMessageBuilder>();
        services.AddScoped<IReportMessageBuilder, VorpahReportMessageBuilder>();
        services.AddScoped<ReportMessageBuilderFactory>();

        return services;
    }
}