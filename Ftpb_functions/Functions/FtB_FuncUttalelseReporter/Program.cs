﻿using Elastic.Apm.Azure.Functions;
using FtB_Common;
using FtB_FuncUttalelseReporter;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Debugging;
using System;

var host = new HostBuilder()
    .ConfigureAppConfiguration(builder =>
    {
        builder
            .SetBasePath(Environment.CurrentDirectory)
            .AddJsonFile("local.settings.json", optional: true, reloadOnChange: true)
            .AddEnvironmentVariables()
            .Build();
    })
    .ConfigureFunctionsWorkerDefaults((ctx, builder) =>
    {
        if (!string.IsNullOrEmpty(ctx.Configuration["ELASTIC_APM_SERVER_URL"]))
            builder.UseMiddleware<ApmMiddleware>();
    })
    .ConfigureLogging((ctx, logging) =>
    {
        Log.Logger = SerilogServiceCollectionExtension.ConfigureSerilog(ctx.Configuration).CreateLogger();
        logging.AddSerilog(Log.Logger, true);

        if (bool.TryParse(ctx.Configuration["Serilog:SelfLogEnabled"], out var selflogEnabled))
            if (selflogEnabled)
                SelfLog.Enable(msg => Console.WriteLine(msg));
    })
    .ConfigureServices(services =>
    {
        var configuration = services.BuildServiceProvider().GetService<IConfiguration>();

        services.AddUttalelseReporterServices(configuration);
    })
    .Build();

host.Run();