using Altinn.Distribution;
using Altinn3.Adapters;
using Dibk.Ftpb.ModelToPdf.Common.Client;
using FtB_Common.Encryption;
using FtB_Common.FormDataRepositories;
using FtB_Common.Interfaces;
using FtB_Common.Storage;
using FtB_Common.Utils;
using FtB_FormLogic;
using FtB_FormLogic.Shipments.ShipmentFormLogic.Ferdigattest;
using FtB_FormLogic.Shipments.ShipmentFormLogic.IgangsettingstillatelseV3;
using FtB_FormLogic.Shipments.ShipmentFormLogic.InnsendingAvReguleringsplanforslag;
using FtB_Print.HttpClients.Pdf;
using FtB_ProcessStrategies;
using Ftb_Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace FtB_FuncReporter;

public static class ServicesConfiguration
{
    public static IServiceCollection AddReporterServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddScoped<VarselOppstartPlanarbeidReportLogic>();
        services.AddScoped<VarselOppstartPlanarbeidPlussHoeringsmyndigheterReportLogic>();
        services.AddScoped<HoeringOgOffentligEttersynReportLogic>();
        services.AddScoped<InnsendingAvReguleringsplanforslagReportLogic>();
        services.AddScoped<FerdigattestReportLogic>();
        services.AddScoped<IgangsettingstillatelseV3ReportLogic>();

        services.AddAltinn3Client(configuration);
        services.AddScoped<IAltinnInstanceCompleter, AltinnInstanceCompleter>();

        services.AddScoped<ReportQueueProcessor>();

        services.AddStorageClients(configuration["PrivateAzureStorageConnectionString"], configuration["PublicAzureStorageConnectionString"]);
        services.AddDtoRepos();

        services.AddScoped<FormatIdToFormMapper>();
        services.AddScoped<IFormDataRepo, FormDataRepository>();
        services.AddFtbRepositories(configuration);

        services.AddScoped<IDecryption, Decryption>();
        services.AddScoped<IDecryptionFactory, DecryptionFactory>();
        services.Configure<EncryptionSettings>(configuration.GetSection("EncryptionSettings"));

        services.AddScoped<IHtmlUtils, HtmlUtils>();
        services.Configure<HtmlUtilSettings>(configuration.GetSection("HtmlUtilSettings"));

        services.AddHttpClient<IPdfHttpClient, PdfHttpClient>();
        services.Configure<PdfSettings>(configuration.GetSection(PdfSettings.SectionName));

        services.AddAltinnNotification(configuration);
        services.AddDatamodelToPdfService(new Uri(configuration["DatamodelToPdf:ApiUrl"]));

        return services;
    }
}