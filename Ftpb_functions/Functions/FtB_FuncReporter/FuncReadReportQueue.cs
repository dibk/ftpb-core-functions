﻿using Azure.Messaging.ServiceBus;
using FtB_Common.BusinessModels.QueueMessageModels;
using FtB_ProcessStrategies;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FtB_FuncReporter
{
    public class FuncReadReportQueue
    {
        private readonly ILogger<FuncReadReportQueue> _logger;
        private readonly ReportQueueProcessor _queueProcessor;

        public FuncReadReportQueue(ILogger<FuncReadReportQueue> logger, ReportQueueProcessor queueProcessor)
        {
            _logger = logger;
            _queueProcessor = queueProcessor;
        }

        [Function("FuncReadReportQueue")]
        public async Task Run([ServiceBusTrigger("%ReportQueueName%", Connection = "QueueConnectionString")] ServiceBusReceivedMessage message, ServiceBusMessageActions messageActions)
        {
            var reportQueueMessage = System.Text.Encoding.Default.GetString(message.Body);
            ReportQueueItem reportQueueItem;
            try
            {
                reportQueueItem = JsonConvert.DeserializeObject<ReportQueueItem>(reportQueueMessage);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to deserialize message to ReportQueueMessage: {ReportQueueMessage}", reportQueueMessage);
                await messageActions.DeadLetterMessageAsync(message);
                throw;
            }
            
            if (reportQueueItem != null)
                using (var scope = _logger.BeginScope(new Dictionary<string, string> { { "ArchiveReference", reportQueueItem.ArchiveReference }, { "ReceiverId", reportQueueItem.Receiver.Id } })) 
                {
                    try
                    {
                        var result = await _queueProcessor.ExecuteProcessingStrategyAsync(reportQueueItem);
                        await messageActions.CompleteMessageAsync(message);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, "An error occurred when processing {ReportQueueItem}", reportQueueItem);
                        await messageActions.DeadLetterMessageAsync(message);
                        throw;
                    }
                }
            else
            {
                _logger.LogError("ReportQueueItem is null, unable to process ReportQueueMessage: {ReportQueueMessage}", reportQueueMessage);
                await messageActions.DeadLetterMessageAsync(message);
                throw new Exception($"ReportQueueMessage is null, unable to process ReportQueueMessage: {reportQueueMessage}");
            }
        }
    }
}